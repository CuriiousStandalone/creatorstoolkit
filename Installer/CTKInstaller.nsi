;--------------------------------
;Include Modern UI

  !include "MUI2.nsh"

;--------------------------------

!define CompanyName "Curiious"
!define AppName "Creator's Toolkit"

# define installer name
Name "${AppName}"

# Get the date string.
!define /date TIMESTAMP "%y%m%d"

!define OutFile "CreatorsToolkit_${TIMESTAMP}_Installer.exe"
OutFile "${OutFile}"

# set desktop as install directory
InstallDir "$PROGRAMFILES64\${CompanyName}\${AppName}"

;--------------------------------
;Interface Configuration

  !define MUI_ICON "ctk.ico"
  !define MUI_UNICON "ctk.ico"
  !define MUI_HEADERIMAGE
  !define MUI_HEADERIMAGE_BITMAP "Header.bmp" ; optional
  !define MUI_WELCOMEPAGE_TITLE "Welcome to ${AppName} Setup."
  !define MUI_WELCOMEPAGE_TEXT "Welcome to ${AppName}."
  !define MUI_ABORTWARNING "${AppName} Setup"
  !define MUI_ABORTWARNING_TEXT "Are you sure you want to quit ${AppName} setup?"
  !define MUI_WELCOMEFINISHPAGE_BITMAP "Panel.bmp"
  !define MUI_LICENSEPAGE_TEXT_TOP "Changelog of ${AppName}"
  !define MUI_LICENSEPAGE_TEXT_BOTTOM " "
  !define MUI_LICENSEPAGE_BUTTON "Next >"
  !define MUI_PAGE_HEADER_TEXT  "Changelog"
  !define MUI_PAGE_HEADER_SUBTEXT " "

;--------------------------------

;--------------------------------
;Pages

  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_LICENSE "Changelog.txt"
  !insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_INSTFILES
  !define MUI_FINISHPAGE_RUN_NOTCHECKED
  !define MUI_FINISHPAGE_RUN "$INSTDIR\${AppName}.exe" 
  !define MUI_FINISHPAGE_RUN_TEXT "Run ${AppName}"
  !insertmacro MUI_PAGE_FINISH

#  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
#  !insertmacro MUI_UNPAGE_FINISH

;--------------------------------

;Languages
 
  !insertmacro MUI_LANGUAGE "English"

;--------------------------------
;Installer Sections
 
# default section start
Section

# define output path
SetOutPath "$INSTDIR"
 
# specify file to go in output path
File /r "C:\Buildhere\*"
File "M:\Curiious\CuriiousIQ.Local.CreatorsToolkit\Installer\ctk.ico"
 
# define uninstaller name
WriteUninstaller $INSTDIR\uninstaller.exe

# Create an entry in the add remove software window
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName}" \
                 "DisplayName" "${AppName}"
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName}" \
                 "UninstallString" "$\"$INSTDIR\uninstaller.exe$\""
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName}" \
				 "DisplayIcon" "$\"$INSTDIR\ctk.ico$\""

 System::Call 'shell32.dll::SHChangeNotify(i, i, i, i) v (0x08000000, 0, 0, 0)'
 

# default section end
SectionEnd
#-------

; Optional section (can be disabled by the user)
Section "Start Menu Shortcuts"

CreateDirectory "$SMPROGRAMS\${CompanyName}\${AppName}"
CreateShortCut "$SMPROGRAMS\${CompanyName}\${AppName}\${AppName}.lnk" "$INSTDIR\${AppName}.exe" 
CreateShortCut "$SMPROGRAMS\${CompanyName}\${AppName}\Uninstall ${AppName}.lnk" "$INSTDIR\uninstaller.exe" 
  
SectionEnd

#-------

Section "Desktop Shortcut"

# Create Shortcuts

CreateShortcut "$DESKTOP\${AppName}.lnk" "$INSTDIR\${AppName}.exe" 
  
SectionEnd

#-------

 
# create a section to define what the uninstaller does.
# the section will always be named "Uninstall"
Section "Uninstall"
 
# Always delete uninstaller first
Delete $INSTDIR\uninstaller.exe
 
# now delete installed file
Delete "$INSTDIR\*.*"
RmDir /r "$INSTDIR"

# Remove the shortcuts
RmDir /r "$SMPROGRAMS\${CompanyName}\${AppName}"
Delete "$DESKTOP\${AppName}.lnk"

# Remove from add remove software
DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName}"

# Remove file assocation
DeleteRegKey HKCR ".ctk"
DeleteRegKey HKCR "${AppName}"

# Remove from HKCU 
DeleteRegKey HKCU "Software\Curiious\${CompanyName}\${AppName}"

System::Call 'shell32.dll::SHChangeNotify(i, i, i, i) v (0x08000000, 0, 0, 0)'

SectionEnd

Section "Associate *.ctk files with ${AppName}" FileAssociations
 ;register file extensions
 WriteRegStr HKCR ".ctk" ""  "${AppName}"
 WriteRegStr HKCR "${AppName}\shell\open\command" "" "$\"$INSTDIR\${AppName}.exe$\" -f $\"%1$\""
SectionEnd


# This requires the code signing dongle, sectigo safenet and the microsoft signtool installed.
!finalize '"C:\Program Files (x86)\Microsoft SDKs\ClickOnce\SignTool\signtool.exe" sign /n "Curiious Pty Ltd" /fd SHA256 /tr http://timestamp.globalsign.com/?signature=sha2 /td SHA256 /a ${OutFile}'

