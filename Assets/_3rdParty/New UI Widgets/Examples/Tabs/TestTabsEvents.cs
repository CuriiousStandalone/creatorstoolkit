﻿namespace UIWidgets.Examples
{
	using System;
	using UIWidgets;
	using UnityEngine;

	/// <summary>
	/// Test Tabs events.
	/// </summary>
	public class TestTabsEvents : MonoBehaviour
	{
		/// <summary>
		/// Tabs.
		/// </summary>
		[SerializeField]
		protected TabsIcons Tabs;

		int current_tab_index;

		/// <summary>
		/// Process start.
		/// </summary>
		protected void Start()
		{
			current_tab_index = Array.IndexOf(Tabs.TabObjects, Tabs.SelectedTab);

			Tabs.OnTabSelect.AddListener(TabChanged);
		}

		void TabChanged(int new_tab_index)
		{
			Debug.Log("deselected tab: " + GetTabName(current_tab_index) + "; index " + current_tab_index);
			Debug.Log("selected tab: " + GetTabName(new_tab_index) + "; index " + new_tab_index);
			
			current_tab_index = new_tab_index;
		}

		string GetTabName(int index)
		{
			if (index < 0 || index >= Tabs.TabObjects.Length)
			{
				return "none";
			}

			return Tabs.TabObjects[index].Name;
		}

		/// <summary>
		/// Process destroy.
		/// </summary>
		protected void OnDestroy()
		{
			Tabs.OnTabSelect.RemoveListener(TabChanged);
		}
	}
}