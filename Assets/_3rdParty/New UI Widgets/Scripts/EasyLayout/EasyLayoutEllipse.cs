﻿namespace EasyLayoutNS
{
	using System;
	using System.Collections.Generic;
	using UnityEngine;

	/// <summary>
	/// Ellipse layout group.
	/// </summary>
	public class EasyLayoutEllipse : EasyLayoutBaseType
	{
		List<LayoutElementInfo> EllipseGroup = new List<LayoutElementInfo>();

		/// <summary>
		/// Initializes a new instance of the <see cref="EasyLayoutEllipse"/> class.
		/// </summary>
		/// <param name="layout">Layout.</param>
		public EasyLayoutEllipse(EasyLayout layout)
				: base(layout)
		{
		}

		/// <summary>
		/// Calculate group size.
		/// </summary>
		/// <returns>Size.</returns>
		protected override Vector2 CalculateGroupSize()
		{
			var settings = Layout.EllipseSettings;
			var size = Layout.InternalSize;

			if (!settings.WidthAuto)
			{
				size.x = settings.Width;
			}

			if (!settings.HeightAuto)
			{
				size.y = settings.Height;
			}

			var max_size = FindElementsMaxSize();
			var align = GetAlignRate(Layout.EllipseSettings.Align);

			size.x -= max_size.x * (align + 1f);
			size.y -= max_size.x * (align + 1f);

			size.x = Mathf.Max(1f, size.x);
			size.y = Mathf.Max(1f, size.y);

			return size;
		}

		Vector2 FindElementsMaxSize()
		{
			var max_size = Vector2.zero;

			for (int i = 0; i < Elements.Count; i++)
			{
				max_size.x = Mathf.Max(max_size.x, Elements[i].Width);
				max_size.y = Mathf.Max(max_size.y, Elements[i].Height);
			}

			return max_size;
		}

		/// <summary>
		/// Calculate positions of the elements.
		/// </summary>
		/// <param name="size">Size.</param>
		protected override void CalculatePositions(Vector2 size)
		{
			if (EllipseGroup.Count == 0)
			{
				return;
			}

			var settings = Layout.EllipseSettings;

			var angle_auto = settings.Fill == EllipseFill.Closed
				? 360f / EllipseGroup.Count
				: settings.ArcLength / Mathf.Max(1, EllipseGroup.Count - 1);
			var angle_step = settings.AngleStepAuto ? angle_auto : settings.AngleStep;

			var angle = settings.AngleStart + settings.AngleFiller + settings.AngleScroll;

			var center = new Vector2(size.x / 2.0f, size.y / 2.0f);
			var align = GetAlignRate(settings.Align);

			for (int i = 0; i < EllipseGroup.Count; i++)
			{
				var element = EllipseGroup[i];

				var angle_rad = Mathf.Deg2Rad * angle;
				var cos = Mathf.Cos(angle_rad);
				var sin = Mathf.Sin(angle_rad);

				var ellipse_pos = new Vector2(center.x * cos, center.y * sin);

				// base position
				ellipse_pos.x += -element.Width * element.Rect.pivot.x;
				ellipse_pos.y += element.Height * (1f - element.Rect.pivot.y);

				// align
				ellipse_pos.x += element.Width * 0.5f * cos * align;
				ellipse_pos.y += element.Width * 0.5f * sin * align;

				// pivot.x
				ellipse_pos.x += element.Width * (element.Rect.pivot.x - 0.5f) * cos;
				ellipse_pos.y += element.Width * (element.Rect.pivot.x - 0.5f) * sin;

				// pivot.y
				ellipse_pos.x += element.Height * (0.5f - element.Rect.pivot.y) * sin;
				ellipse_pos.y += -element.Height * (0.5f - element.Rect.pivot.y) * cos;

				element.PositionTopLeft = ellipse_pos;
				element.NewEulerAnglesZ = angle;

				angle -= angle_step;
			}
		}

		static float GetAlignRate(EllipseAlign align)
		{
			switch (align)
			{
				case EllipseAlign.Outer:
					return -1f;
				case EllipseAlign.Center:
					return 0f;
				case EllipseAlign.Inner:
					return 1f;
				default:
					Debug.LogWarning("Unknown ellipse align: " + align);
					break;
			}

			return 0f;
		}

		/// <summary>
		/// Calculate sizes of the elements.
		/// </summary>
		protected override void CalculateSizes()
		{
		}

		/// <summary>
		/// Group elements.
		/// </summary>
		protected override void Group()
		{
			EllipseGroup.Clear();
			EllipseGroup.AddRange(Elements);

			if (Layout.RightToLeft)
			{
				EllipseGroup.Reverse();
			}
		}
	}
}