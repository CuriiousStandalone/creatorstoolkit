namespace EasyLayoutNS
{
	using System;
	using System.Collections.Generic;
	using System.Reflection;
	using UnityEditor;
	using UnityEngine;
	using UnityEngine.Events;

	/// <summary>
	/// Field info for conditional display.
	/// </summary>
	public class ConditionalFieldInfo
	{
		/// <summary>
		/// Indent level.
		/// </summary>
		public int Indent;

		/// <summary>
		/// Field name.
		/// </summary>
		public string Name;

		/// <summary>
		/// Conditions to display field.
		/// </summary>
		public Dictionary<string, Func<SerializedProperty, bool>> Conditions = new Dictionary<string, Func<SerializedProperty, bool>>();

		/// <summary>
		/// Initializes a new instance of the <see cref="ConditionalFieldInfo"/> class.
		/// </summary>
		/// <param name="fieldName">Field name.</param>
		/// <param name="indent">Indent level.</param>
		/// <param name="conditions">Conditions to display field.</param>
		public ConditionalFieldInfo(string fieldName, int indent = 0, Dictionary<string, Func<SerializedProperty, bool>> conditions = null)
		{
			Name = fieldName;
			Indent = indent;

			if (conditions != null)
			{
				Conditions = conditions;
			}
		}
	}

	/// <summary>
	/// Conditional editor.
	/// </summary>
	public abstract class ConditionalEditor : Editor
	{
		/// <summary>
		/// Not displayable fields.
		/// </summary>
		protected List<string> IgnoreFields;

		/// <summary>
		/// Fields to display.
		/// </summary>
		protected List<ConditionalFieldInfo> Fields;

		/// <summary>
		/// Serialized properties.
		/// </summary>
		protected Dictionary<string, SerializedProperty> SerizalizedProperties = new Dictionary<string, SerializedProperty>();

		/// <summary>
		/// Serialized events.
		/// </summary>
		protected Dictionary<string, SerializedProperty> SerializedEvents = new Dictionary<string, SerializedProperty>();

		/// <summary>
		/// Init.
		/// </summary>
		protected virtual void OnEnable()
		{
			Init();

			SerizalizedProperties.Clear();
			foreach (var field in Fields)
			{
				SerizalizedProperties[field.Name] = null;
			}

			GetSerizalizedProperties();
		}

		/// <summary>
		/// Init this instance.
		/// </summary>
		protected abstract void Init();

		/// <summary>
		/// Get serialized properties.
		/// </summary>
		protected void GetSerizalizedProperties()
		{
			var property = serializedObject.GetIterator();
			property.NextVisible(true);
			while (property.NextVisible(false))
			{
				if (IsEvent(property))
				{
					SerializedEvents[property.name] = serializedObject.FindProperty(property.name);
				}
				else
				{
					if (SerizalizedProperties.ContainsKey(property.name))
					{
						SerizalizedProperties[property.name] = serializedObject.FindProperty(property.name);
					}
					else if (!IgnoreFields.Contains(property.name))
					{
						Debug.LogWarning("Field info not found: " + property.name);
					}
				}
			}
		}

		/// <summary>
		/// Is property event?
		/// </summary>
		/// <param name="property">Property</param>
		/// <returns>true if property is event; otherwise false.</returns>
		protected virtual bool IsEvent(SerializedProperty property)
		{
			var object_type = property.serializedObject.targetObject.GetType();
			var property_type = object_type.GetField(property.propertyPath, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
			if (property_type == null)
			{
				return false;
			}

			return typeof(UnityEventBase).IsAssignableFrom(property_type.FieldType);
		}

		/// <summary>
		/// Check is all displayable fields exists.
		/// </summary>
		/// <returns>true if all displayable fields exists; otherwise false.</returns>
		protected bool AllFieldsExists()
		{
			var result = true;
			foreach (var kv in SerizalizedProperties)
			{
				if (kv.Value == null)
				{
					Debug.LogWarning("Field with name '" + kv.Key + "' not found");
					result = false;
				}
			}

			return result;
		}

		/// <summary>
		/// Check is field can be displayed.
		/// </summary>
		/// <param name="info">Field info.</param>
		/// <returns>true if field can be displayed; otherwise false.</returns>
		protected bool CanShow(ConditionalFieldInfo info)
		{
			foreach (var condition in info.Conditions)
			{
				var field = SerizalizedProperties[condition.Key];
				if (!condition.Value(field))
				{
					return false;
				}
			}

			return true;
		}

		/// <summary>
		/// Draw inspector GUI.
		/// </summary>
		public override void OnInspectorGUI()
		{
			if (!AllFieldsExists())
			{
				return;
			}

			serializedObject.Update();

			foreach (var field in Fields)
			{
				if (!CanShow(field))
				{
					continue;
				}

				EditorGUI.indentLevel += field.Indent;
				EditorGUILayout.PropertyField(SerizalizedProperties[field.Name], true);
				EditorGUI.indentLevel -= field.Indent;
			}

			foreach (var ev in SerializedEvents)
			{
				EditorGUILayout.PropertyField(ev.Value, true);
			}

			serializedObject.ApplyModifiedProperties();

			AdditionalGUI();
		}

		/// <summary>
		/// Display additional GUI.
		/// </summary>
		protected virtual void AdditionalGUI()
		{
		}
	}

	/// <summary>
	/// EasyLayout editor.
	/// </summary>
	[CustomEditor(typeof(EasyLayout), true)]
	public class EasyLayoutEditor : ConditionalEditor
	{
		void Upgrade()
		{
			Array.ForEach(targets, x =>
			{
				var l = x as EasyLayout;
				if (l != null)
				{
					l.Upgrade();
				}
			});
		}

		static Dictionary<string, Func<SerializedProperty, bool>> isCompactOrGrid = new Dictionary<string, Func<SerializedProperty, bool>>()
		{
			{ "layoutType", x => (LayoutTypes)x.enumValueIndex == LayoutTypes.Compact || (LayoutTypes)x.enumValueIndex == LayoutTypes.Grid },
		};

		static Dictionary<string, Func<SerializedProperty, bool>> isCompact = new Dictionary<string, Func<SerializedProperty, bool>>()
		{
			{ "layoutType", x => (LayoutTypes)x.enumValueIndex == LayoutTypes.Compact },
		};

		static Dictionary<string, Func<SerializedProperty, bool>> isCompactNotFlexible = new Dictionary<string, Func<SerializedProperty, bool>>()
		{
			{ "layoutType", x => (LayoutTypes)x.enumValueIndex == LayoutTypes.Compact },
			{ "compactConstraint", x => (CompactConstraints)x.enumValueIndex != CompactConstraints.Flexible },
		};

		static Dictionary<string, Func<SerializedProperty, bool>> isGrid = new Dictionary<string, Func<SerializedProperty, bool>>()
			{
				{ "layoutType", x => (LayoutTypes)x.enumValueIndex == LayoutTypes.Grid },
			};

		static Dictionary<string, Func<SerializedProperty, bool>> isGridNotFlexible = new Dictionary<string, Func<SerializedProperty, bool>>()
		{
			{ "layoutType", x => (LayoutTypes)x.enumValueIndex == LayoutTypes.Grid },
			{ "gridConstraint", x => (GridConstraints)x.enumValueIndex != GridConstraints.Flexible },
		};

		static Dictionary<string, Func<SerializedProperty, bool>> isFlex = new Dictionary<string, Func<SerializedProperty, bool>>()
		{
			{ "layoutType", x => (LayoutTypes)x.enumValueIndex == LayoutTypes.Flex },
		};

		static Dictionary<string, Func<SerializedProperty, bool>> isStaggered = new Dictionary<string, Func<SerializedProperty, bool>>()
		{
			{ "layoutType", x => (LayoutTypes)x.enumValueIndex == LayoutTypes.Staggered },
		};

		static Dictionary<string, Func<SerializedProperty, bool>> isEllipse = new Dictionary<string, Func<SerializedProperty, bool>>()
		{
			{ "layoutType", x => (LayoutTypes)x.enumValueIndex == LayoutTypes.Ellipse },
		};

		static Dictionary<string, Func<SerializedProperty, bool>> isNotEllipse = new Dictionary<string, Func<SerializedProperty, bool>>()
		{
			{ "layoutType", x => (LayoutTypes)x.enumValueIndex != LayoutTypes.Ellipse },
		};

		static Dictionary<string, Func<SerializedProperty, bool>> isSymmetric = new Dictionary<string, Func<SerializedProperty, bool>>()
		{
			{ "symmetric", x => x.boolValue },
		};

		static Dictionary<string, Func<SerializedProperty, bool>> isNotSymmetric = new Dictionary<string, Func<SerializedProperty, bool>>()
		{
			{ "symmetric", x => !x.boolValue },
		};

		/// <summary>
		/// Init this instance.
		/// </summary>
		protected override void Init()
		{
			Upgrade();

			Fields = new List<ConditionalFieldInfo>()
			{
				new ConditionalFieldInfo("mainAxis"),
				new ConditionalFieldInfo("layoutType"),

				new ConditionalFieldInfo("groupPosition", 1, isCompactOrGrid),
				new ConditionalFieldInfo("rowAlign", 1, isCompact),
				new ConditionalFieldInfo("innerAlign", 1, isCompact),
				new ConditionalFieldInfo("compactConstraint", 1, isCompact),
				new ConditionalFieldInfo("compactConstraintCount", 2, isCompactNotFlexible),

				new ConditionalFieldInfo("cellAlign", 1, isGrid),
				new ConditionalFieldInfo("gridConstraint", 1, isGrid),
				new ConditionalFieldInfo("gridConstraintCount", 2, isGridNotFlexible),

				new ConditionalFieldInfo("flexSettings", 1, isFlex),
				new ConditionalFieldInfo("staggeredSettings", 1, isStaggered),
				new ConditionalFieldInfo("ellipseSettings", 1, isEllipse),

				new ConditionalFieldInfo("spacing", 0, isNotEllipse),
				new ConditionalFieldInfo("symmetric"),

				new ConditionalFieldInfo("margin", 1, isSymmetric),
				new ConditionalFieldInfo("marginTop", 1, isNotSymmetric),
				new ConditionalFieldInfo("marginBottom", 1, isNotSymmetric),
				new ConditionalFieldInfo("marginLeft", 1, isNotSymmetric),
				new ConditionalFieldInfo("marginRight", 1, isNotSymmetric),

				new ConditionalFieldInfo("topToBottom", 0, isNotEllipse),

				new ConditionalFieldInfo("rightToLeft"),
				new ConditionalFieldInfo("skipInactive"),
				new ConditionalFieldInfo("resetRotation", 0, isNotEllipse),
				new ConditionalFieldInfo("childrenWidth"),
				new ConditionalFieldInfo("childrenHeight"),
			};

			IgnoreFields = new List<string>()
			{
				"m_Padding",
				"m_ChildAlignment",
			};
		}

		/// <summary>
		/// Display additional GUI.
		/// </summary>
		protected override void AdditionalGUI()
		{
			if (targets.Length == 1)
			{
				var script = (EasyLayout)target;

				EditorGUILayout.LabelField("Block size", string.Format("{0}x{1}", script.BlockSize.x, script.BlockSize.y));
				EditorGUILayout.LabelField("UI size", string.Format("{0}x{1}", script.UISize.x, script.UISize.y));
				EditorGUILayout.LabelField("Size", string.Format("{0}x{1}", script.Size.x, script.Size.y));
			}
		}
	}
}