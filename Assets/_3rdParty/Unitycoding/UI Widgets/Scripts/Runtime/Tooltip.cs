﻿using UnityEngine;
using UnityEngine.UI;

using TMPro;

namespace Unitycoding.UIWidgets{
	public class Tooltip : UIWidget {
		[Header("Reference")]
		/// <summary>
		/// The Text component to display tooltip text.
		/// </summary>
		public Text text;

        public TMP_Text textPro;
		/// <summary>
		/// The Image component to display the icon.
		/// </summary>
		public Image icon;
		/// <summary>
		/// The background image.
		/// </summary>
		public Image background;
		/// <summary>
		/// Update position to follow mouse 
		/// </summary>
		public bool updatePosition;

		private float width=300f;
		private Canvas canvas;
		private bool _updatePosition;
        public bool LockToPostion;

		protected override void OnStart ()
		{
			base.OnStart ();
			canvas = GetComponentInParent<Canvas> ();
			width = rectTransform.sizeDelta.x;
			if (IsVisible) {
				Close ();
			}
		}

		protected virtual void Update ()
		{
			if (updatePosition && canvasGroup.alpha > 0f && _updatePosition) {
				UpdatePosition ();
			}
		}
		
		private void UpdatePosition()
        {
            if (LockToPostion)
            {
                return;
            }
			Vector2 pos;
			RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform, Input.mousePosition, canvas.worldCamera, out pos);
			Vector2 offset=Vector2.zero;

            if(LockToPostion)
            {

            }

			if (Input.mousePosition.x < rectTransform.sizeDelta.x)
            {
				offset += new Vector2 (rectTransform.sizeDelta.x * 0.5f, 0);
			}
            else
            {
                offset += new Vector2(-rectTransform.sizeDelta.x*0.5f,0);
			}
			if(Screen.height- Input.mousePosition.y > rectTransform.sizeDelta.y)
            {
                offset += new Vector2 (0, rectTransform.sizeDelta.y * 0.5f);
			}
            else
            {
                offset += new Vector2 (0, -rectTransform.sizeDelta.y * 0.5f);
			}

			pos=pos+offset;
			
			transform.position = canvas.transform.TransformPoint(pos);
			Focus ();
		}

		/// <summary>
		/// Show this widget.
		/// </summary>
		public override void Show ()
		{
			base.Show ();
			canvasGroup.interactable = false;
			canvasGroup.blocksRaycasts = false;
		}

		/// <summary>
		/// Show this widget.
		/// </summary>
		/// <param name="tooltipText">Tooltip text.</param>
		public virtual void Show(string tooltipText){
			Show (tooltipText, this.width, true);
		}

		/// <summary>
		/// Show this widget.
		/// </summary>
		/// <param name="tooltipText">Tooltip text.</param>
		/// <param name="width">Width.</param>
		/// <param name="showBackground">If set to <c>true</c> show background.</param>
		public virtual void Show(string tooltipText, float width, bool showBackground){
			Show (tooltipText,null, width, showBackground);
		}

		/// <summary>
		/// Show this widget.
		/// </summary>
		/// <param name="tooltipText">Tooltip text.</param>
		/// <param name="icon">Icon.</param>
		/// <param name="width">Width.</param>
		/// <param name="showBackground">If set to <c>true</c> show background.</param>
		public virtual void Show(string tooltipText,Sprite icon, float width, bool showBackground, Transform lockToTransfrom = null){
			if (!string.IsNullOrEmpty (tooltipText))
            {
				this.text.text = tooltipText;
				if (icon != null)
                {
					this.icon.sprite = icon;
					this.icon.transform.parent.gameObject.SetActive (true);
				}
                else
                {
					this.icon.transform.parent.gameObject.SetActive (false);
				}



				rectTransform.sizeDelta = new Vector2 (width, rectTransform.sizeDelta.y);
				this.background.enabled = showBackground;
				this._updatePosition=true;
                if (lockToTransfrom != null)
                {
                    LockToPostion = true;
                    transform.position = lockToTransfrom.position;
                }
                else
                {
                    LockToPostion = false;
                    UpdatePosition();
                }
				Show ();
			} 
		}

        /// <summary>
		/// Show this widget.
		/// </summary>
		/// <param name="tooltipText">Tooltip text.</param>
		/// <param name="icon">Icon.</param>
		/// <param name="width">Width.</param>
		/// <param name="showBackground">If set to <c>true</c> show background.</param>
		public virtual void Show(string tooltipText, Vector3 lockToTransfrom)
        {
            if (!string.IsNullOrEmpty(tooltipText))
            {
                Show();

                textPro.text = tooltipText;

                textPro.ForceMeshUpdate();

                this.icon.transform.parent.gameObject.SetActive(false);

                rectTransform.sizeDelta = new Vector2(textPro.textBounds.size.x + 50, rectTransform.sizeDelta.y);

                this.background.enabled = true;

                this._updatePosition = true;

                if (lockToTransfrom != null)
                {
                    LockToPostion = true;

                    transform.position = new Vector3(lockToTransfrom.x + (rectTransform.sizeDelta.x / 2), lockToTransfrom.y - (rectTransform.sizeDelta.y / 2) - 10, lockToTransfrom.z);

                    if((transform.position.x + rectTransform.sizeDelta.x / 2) > Screen.width)
                    {
                        transform.position = new Vector3(lockToTransfrom.x - (rectTransform.sizeDelta.x / 2), transform.position.y, lockToTransfrom.z);
                    }

                    if((transform.position.y - rectTransform.sizeDelta.y / 2) < 0)
                    {
                        transform.position = new Vector3(transform.position.x, lockToTransfrom.y + (rectTransform.sizeDelta.y / 2) + 10, lockToTransfrom.z);
                    }
                }
                else
                {
                    LockToPostion = false;
                    UpdatePosition();
                }
            }
        }

        /// <summary>
        /// Close this widget.
        /// </summary>
        public override void Close ()
		{
			base.Close ();
			_updatePosition = false;
		}
	}
}