﻿using strange.extensions.context.api;
using strange.extensions.context.impl;

using UnityEngine;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public class ContextResource : MVCSContext
    {
        public ContextResource(MonoBehaviour view) : base(view)
        {
        }

        public ContextResource(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
        {
        }

        protected override void mapBindings()
        {
            #region Model Bindings

            injectionBinder.Bind<IResourceModel>().To<ResourceModel>().ToSingleton();
            injectionBinder.Bind<IServiceLocalFileSystemCommunication>().To<ServiceLocalFileSystemCommunication>().ToSingleton();

            #endregion

            #region View-MediatorBindings

            mediationBinder.Bind<ViewUploadDialog>().To<MediatorUploadDialog>();
            mediationBinder.Bind<ViewResourceManager>().To<MediatorResourceManager>();
            mediationBinder.Bind<ViewResourceTooltipManager>().To<MediatorResourceTooltipManager>();

            #endregion

            #region Command Binding

            //commandBinder.Bind(ContextEvent.START).To<CommandLoadResources>().Once();

            commandBinder.Bind(EEventToolkitCrossContext.HIDE_PANEL).To<CommandHidePanel>();
            commandBinder.Bind(EEventToolkitCrossContext.SHOW_PANEL).To<CommandShowPanel>();
            //commandBinder.Bind(EEventToolkitCrossContext.LAUNCH_BULK_RESOURCE_UPLOAD).To<CommandShowUploadDialog>();
            commandBinder.Bind(EEventToolkitCrossContext.WORLD_DOWNLOADED).To<CommandDownloadAllResroucesInWorld>();
            commandBinder.Bind(EEventToolkitCrossContext.SHOW_FILTERED_LIST).To<CommandShowFilteredList>();
            commandBinder.Bind(EEventToolkitCrossContext.TOOLTIP_TEXT_RESPONSE).To<CommandTooltipResponse>();
            commandBinder.Bind(EEventToolkitCrossContext.IMPORT_MEDIA_RESOURCE).To<CommandImportMediaResources>();

            commandBinder.Bind(EEventToolkitCrossContext.OPEN_WORLD_REQUESTED).To<CommandReloadResourceList>();
            commandBinder.Bind(EEventToolkitCrossContext.NEW_WORLD_CREATED).To<CommandReloadResourceList>();
            commandBinder.Bind(EEventToolkitCrossContext.RELOAD_RESOURCE_LIST).To<CommandReloadResourceList>();

            commandBinder.Bind(EResourceEvent.LOAD_LOCAL_RESOURCE_DATA).To<CommandSaveLocalPlatformResourceData>();
            //commandBinder.Bind(EResourceEvent.DOWNLOAD_ALL_THUMBNAILS).To<CommandFetchAllThumbnails>();
            commandBinder.Bind(EResourceEvent.REQUEST_PLATFORM_RESOURCE_DATA).To<CommandGetPlatformResources>();
            commandBinder.Bind(EResourceEvent.FETCH_REMOTE_RESOURCE_DATA).To<CommandSaveRemoteResourceData>();
            commandBinder.Bind(EResourceEvent.REQUEST_REMOTE_RESOURCE_DATA).To<CommandGetRemoteResources>();
            commandBinder.Bind(EResourceEvent.ALL_THUMBNAILS_DOWNLOADED).To<CommandLoadThumbnailsToModel>();
            commandBinder.Bind(EResourceEvent.REMOVE_LOCAL_RESOURCE).To<CommandRemoveLocalResource>();
            commandBinder.Bind(EResourceEvent.DOWNLOAD_REMOTE_RESOURCE).To<CommandFetchRemoteResource>();
            commandBinder.Bind(EResourceEvent.LOAD_HOTSPOT_ICONS).To<CommandSaveHotspotIcons>();
            commandBinder.Bind(EResourceEvent.ON_DRAG_DROP_FAILED).To<CommandDragDropFailed>();
            commandBinder.Bind(EResourceEvent.UPLOAD_MEDIA_RESOURCES).To<CommandUploadMediaResources>();
            commandBinder.Bind(EResourceEvent.REFRESH_RESOURCES_LIST).To<CommandLoadResources>();
            commandBinder.Bind(EResourceEvent.TOOLTIP_TEXT_REQUESTED).To<CommandTooltipRequest>();

            #endregion
        }
    }
}