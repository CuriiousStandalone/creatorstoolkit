﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;

using UnityEngine;

using strange.extensions.dispatcher.eventdispatcher.api;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;
using PulseIQ.Local.Common.Model.ResourceVersionControl;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public class ServiceLocalFileSystemCommunication : IServiceLocalFileSystemCommunication
    {
        [Inject]
        public IEventDispatcher dispatcher { get; set; }

        private static ManualResetEvent resetEvent = new ManualResetEvent(false);

        private string zipPath;

        private string tempDownPath;

        public ServiceLocalFileSystemCommunication()
        {
        }

        public void FetchAllLocal(ListCardResourceTypes resourceType)
        {
            GetAllLocalPlatformResourcesData();
        }

        private void GetAllLocalPlatformResourcesData()
        {
            List<string> pathList = new List<string>();

            List<string> filesList = new List<string>();

            pathList.Add(Settings.GetPathByItemType(ItemTypeCode.PANORAMA));
            pathList.Add(Settings.GetPathByItemType(ItemTypeCode.PANORAMA_VIDEO));
            pathList.Add(Settings.GetPathByItemType(ItemTypeCode.STEREO_PANORAMA_VIDEO));
            pathList.Add(Settings.GetPathByItemType(ItemTypeCode.AUDIO));
            pathList.Add(Settings.GetPathByItemType(ItemTypeCode.IMAGE));

            foreach (string path in pathList)
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                string[] files = Directory.GetFiles(path);

                foreach (var file in files)
                {
                    filesList.Add(file);
                }
            }

            List<SResourceData> resoureDataList = new List<SResourceData>();
            SResourceData resourceData;

            MessageData messageData = new MessageData();
            messageData.Parameters.Add((byte)EParameterCodeResource.STATUS, "success");

            if (filesList.Count > 0)
            {
                foreach (string file in filesList)
                {
                    resourceData = new SResourceData();
                    resourceData.ItemType = GetItemTypeFromPath(file);
                    resourceData.LocalFilePath = file;
                    resourceData.ResourceName = Path.GetFileName(file);
                    resourceData.ResourceType = ListCardResourceTypes.PLATFORM;
                    resourceData.ThumbnailPath = Path.Combine(Settings.GetThumbnailPathByItemType(resourceData.ItemType), Path.GetFileNameWithoutExtension(resourceData.ResourceName) + ".png");

                    if (resourceData.ItemType == ItemTypeCode.PANORAMA || resourceData.ItemType == ItemTypeCode.PANORAMA_VIDEO || resourceData.ItemType == ItemTypeCode.PLANAR_VIDEO || resourceData.ItemType == ItemTypeCode.STEREO_PANORAMA_VIDEO || resourceData.ItemType == ItemTypeCode.AUDIO || resourceData.ItemType == ItemTypeCode.IMAGE)
                    {
                        resourceData.MultimediaId = Path.GetFileName(file);
                    }

                    resoureDataList.Add(resourceData);
                }
            }

            resourceData = new SResourceData
            {
                ResourceName = "Puzzle",
                LocalFilePath = "",
                ItemType = ItemTypeCode.SLOT_GROUP,
                MultimediaId = ""
            };

            resoureDataList.Add(resourceData);

            messageData.Parameters.Add((byte)EParameterCodeResource.RESOURCE_LIST, resoureDataList);

            dispatcher.Dispatch(EResourceEvent.LOAD_LOCAL_RESOURCE_DATA_COMPLETED, messageData);
        }

        private ItemTypeCode GetItemTypeFromPath(string path)
        {
            string directoryName = Path.GetDirectoryName(path);

            ItemTypeCode itemType;

            if (directoryName.Contains("1_Item"))
            {
                itemType = ItemTypeCode.ITEM;
            }
            else if (directoryName.Contains("2_Interactive"))
            {
                itemType = ItemTypeCode.INTERACTIVE_ITEM;
            }
            else if (directoryName.Contains("3_Slot"))
            {
                itemType = ItemTypeCode.SLOT;
            }
            else if (directoryName.Contains("4_Panorama"))
            {
                itemType = ItemTypeCode.PANORAMA;
            }
            else if (directoryName.Contains("5_PanoramaVideo"))
            {
                itemType = ItemTypeCode.PANORAMA_VIDEO;
            }
            else if (directoryName.Contains("6_PlanarVideo"))
            {
                itemType = ItemTypeCode.PLANAR_VIDEO;
            }
            else if (directoryName.Contains("10_SyncTimer"))
            {
                itemType = ItemTypeCode.SYNCTIMER;
            }
            else if (directoryName.Contains("12_Tutorial"))
            {
                itemType = ItemTypeCode.TUTORIAL_ITEM;
            }
            else if (directoryName.Contains("13_Postprocessing"))
            {
                itemType = ItemTypeCode.POST_PROCESSING_PROFILE;
            }
            else if (directoryName.Contains("14_Skybox"))
            {
                itemType = ItemTypeCode.SKYBOX;
            }
            else if (directoryName.Contains("15_UI"))
            {
                itemType = ItemTypeCode.UI;
            }
            else if (directoryName.Contains("17_StereoPanoramaVideo"))
            {
                itemType = ItemTypeCode.STEREO_PANORAMA_VIDEO;
            }
            else if(directoryName.Contains("20_Audio"))
            {
                itemType = ItemTypeCode.AUDIO;
            }
            else if(directoryName.Contains("21_Image"))
            {
                itemType = ItemTypeCode.IMAGE;
            }
            else
            {
                itemType = ItemTypeCode.ITEM;
            }

            return itemType;
        }

        public void CopyMultimediaDeviceFileToLocalDirectory(MessageData messageData)
        {
            ItemTypeCode itemType = (ItemTypeCode)messageData[(byte)EParameterCodeResource.IMPORT_ITEM_TYPE];

            string itemPath = (string)messageData[(byte)EParameterCodeResource.IMPORT_ITEM_PATH];
            string itemThumbnailPath = (string)messageData[(byte)EParameterCodeResource.IMPORT_ITEM_THUMBNAIL_PATH];

            string destFilePath = "";
            string destThumbnailFilePath = "";

            string fileName = Path.GetFileName(itemPath);
            string thumbnailFileName = Path.GetFileNameWithoutExtension(itemPath) + ".png";

            MessageData resultMessageData = new MessageData();

            destFilePath = Settings.GetPathByItemType(itemType);
            destThumbnailFilePath = Settings.GetThumbnailPathByItemType(itemType);

            if (!Directory.Exists(destFilePath))
            {
                Directory.CreateDirectory(destFilePath);
            }

            if (!Directory.Exists(destThumbnailFilePath))
            {
                Directory.CreateDirectory(destThumbnailFilePath);
            }

            destFilePath = Path.Combine(Settings.GetPathByItemType(itemType), fileName);
            destThumbnailFilePath = Path.Combine(Settings.GetThumbnailPathByItemType(itemType), thumbnailFileName);

            SResourceData resourceData = new SResourceData
            {
                ResourceName = fileName,
                LocalFilePath = destFilePath,
                ItemType = itemType,
                MultimediaId = fileName,
                ThumbnailPath = destThumbnailFilePath,
            };

            resultMessageData.Parameters.Add((byte)EParameterCodeResource.IMPORT_ITEM_TYPE, itemType);
            resultMessageData.Parameters.Add((byte)EParameterCodeResource.MULTIMEDIA_ASSET, resourceData);

            try
            {
                if (File.Exists(destFilePath))
                {
                    File.Delete(destFilePath);
                }
                File.Copy(itemPath, destFilePath);

                if (itemThumbnailPath != "")
                {
                    if (File.Exists(destThumbnailFilePath))
                    {
                        File.Delete(destThumbnailFilePath);
                    }
                    File.Copy(itemThumbnailPath, destThumbnailFilePath);
                }

                resultMessageData.Parameters.Add((byte)EParameterCodeResource.STATUS, "success");

                dispatcher.Dispatch(EResourceEvent.FILE_IMPORT_COMPLETED, resultMessageData);
            }
            catch (IOException ex)
            {
                resultMessageData.Parameters.Add((byte)EParameterCodeResource.STATUS, "error");
                resultMessageData.Parameters.Add((byte)EParameterCodeResource.MESSAGE, ex.Message);

                dispatcher.Dispatch(EResourceEvent.FILE_IMPORT_ERROR, resultMessageData);
            }
        }

        public void CopyResourceDeviceFileToLocalDirectory(ItemTypeCode itemType, string windowsFilePath, string androidFilePath)
        {
            string fileName = Path.GetFileName(windowsFilePath);

            MessageData resultMessageData = new MessageData();

            try
            {
                string destFilePath = Path.Combine(Settings.GetPathByItemType(itemType, "windows"), fileName);
                if (File.Exists(destFilePath))
                {
                    File.Delete(destFilePath);
                }
                File.Copy(windowsFilePath, destFilePath);

                destFilePath = Path.Combine(Settings.GetPathByItemType(itemType, "android"), fileName);
                if (File.Exists(destFilePath))
                {
                    File.Delete(destFilePath);
                }
                File.Copy(androidFilePath, destFilePath);

                SResourceData platformResourceData = new SResourceData
                {
                    ResourceName = fileName,
                    LocalFilePath = Path.Combine(Settings.GetPathByItemType(itemType, "windows"), fileName),
                    ItemType = itemType
                };

                resultMessageData.Parameters.Add((byte)EParameterCodeResource.IMPORT_ITEM_TYPE, itemType);
                resultMessageData.Parameters.Add((byte)EParameterCodeResource.RESOURCE, platformResourceData);
                resultMessageData.Parameters.Add((byte)EParameterCodeResource.STATUS, "success");

                dispatcher.Dispatch(EResourceEvent.FILE_IMPORT_COMPLETED, resultMessageData);
            }
            catch (IOException ex)
            {
                resultMessageData.Parameters.Add((byte)EParameterCodeResource.STATUS, "error");
                resultMessageData.Parameters.Add((byte)EParameterCodeResource.MESSAGE, ex.Message);

                dispatcher.Dispatch(EResourceEvent.FILE_IMPORT_ERROR, resultMessageData);
            }
        }

       
        private void UpdateVersionControlXML(string path)
        {
            string[] files = Directory.GetFiles(path);
            ResourcesVersionDataSerializer.ReadData(files[0], out ResourcesVersionDataXML versionXMLNew);
            ResourcesVersionData versionDataNew = new ResourcesVersionData(versionXMLNew);

            if (versionDataNew.ListResourceItems.Count <= 0)
            {
                return;
            }

            if (File.Exists(Settings.ItemVersionPath))
            {
                ResourcesVersionDataXML versionXML = new ResourcesVersionDataXML();
                ResourcesVersionDataSerializer.ReadData(Settings.ItemVersionPath, out versionXML);
                ResourcesVersionData versionData = new ResourcesVersionData(versionXML);

                foreach (ResourceVersionItemData newItemData in versionDataNew.ListResourceItems)
                {
                    bool edited = false;

                    foreach (ResourceVersionItemData itemData in versionData.ListResourceItems)
                    {
                        if (itemData.ResourceId == newItemData.ResourceId)
                        {
                            itemData.VersionNumber = newItemData.VersionNumber;
                            edited = true;
                            break;
                        }
                    }

                    if (!edited)
                    {
                        versionData.ListResourceItems.Add(newItemData);
                    }
                }

                File.Delete(Settings.ItemVersionPath);
                versionData.DateChecked = DateTime.Now.ToString(CultureInfo.InvariantCulture);
                ResourcesVersionDataSerializer.WriteData(versionData, Settings.ItemVersionPath);
            }
            else
            {
                File.Copy(files[0], Settings.ItemVersionPath);
            }
        }
    }
}