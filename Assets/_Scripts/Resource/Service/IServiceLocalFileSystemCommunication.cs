﻿using System.Collections.Generic;

using strange.extensions.dispatcher.eventdispatcher.api;

using CuriiousIQ.Local.CreatorsToolkit.Resource;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public interface IServiceLocalFileSystemCommunication
    {
        IEventDispatcher dispatcher { get; set; }

        void FetchAllLocal(ListCardResourceTypes resourceType);

        void CopyMultimediaDeviceFileToLocalDirectory(MessageData messageData);

        void CopyResourceDeviceFileToLocalDirectory(ItemTypeCode itemType, string windowsFilePath, string androidFilePath);
    }
}