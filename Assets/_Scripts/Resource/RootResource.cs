﻿using strange.extensions.context.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public class RootResource : ContextView
    {
        private void Awake()
        {
            context = new ContextResource(this);
        }
    }
}
