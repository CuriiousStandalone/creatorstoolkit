﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using UnityEngine;

using strange.extensions.mediation.impl;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIPanels;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    /// <summary>
    /// Manages Display of resources for both home screen and editor.
    /// </summary>
    public class ViewResourceManager : EventView, IResponsivePanel
    {
        [SerializeField]
        private CItemList[] _collectionLists;

        [SerializeField]
        private CSearchBarTypeFilter[] _searchBars;

        [SerializeField]
        private GameObject _genericAlertDialog;

        private bool _downloadingDone;

        private ListItemCardData[] _currentCardItems;

        private bool _collectionsInited;

        public enum ResourceManagerEvent
        {
            REMOVE_ASSET_CLICKED,
            ADD_THUMBNAIL_CLICKED,
            DOWNLOAD_ASSET_CLICKED,
            ITEM_DROP_FAILED,
            VIEW_LOADED,
            PANEL_HIDDEN,
            PANEL_SHOWN,
        }

        internal void Init()
        {
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelId);

            dispatcher.Dispatch(ResourceManagerEvent.VIEW_LOADED, msgData);

            InitCollections();
        }

        private void OnRemoveAsset(ListItemCardData cardData)
        {
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeResource.CARD_DATA, cardData);
            dispatcher.Dispatch(ResourceManagerEvent.REMOVE_ASSET_CLICKED, msgData);
        }

        private void OnDownload(ListItemCardData cardData)
        {
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeResource.CARD_DATA, cardData);

            dispatcher.Dispatch(ResourceManagerEvent.DOWNLOAD_ASSET_CLICKED, msgData);
        }

        private void OnDragDropFailed(ListItemCardData cardData)
        {
            if(cardData.Type[0] == "Hotspot Icon")
            {
                return;
            }
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)CommonParameterCode.RESOURCEID, cardData.ResourceID);
            ItemTypeCode type = (ItemTypeCode)Enum.Parse(typeof(ItemTypeCode), cardData.Type[0]);
            msgData.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, type);
            msgData.Parameters.Add((byte)CommonParameterCode.MOUSE_COORDS, Input.mousePosition);
            dispatcher.Dispatch(ResourceManagerEvent.ITEM_DROP_FAILED, msgData);
        }

        private static IEnumerable<ListItemCardData> SortFunction(IEnumerable<ListItemCardData> card)
        {
            IEnumerable<ListItemCardData> returnData = card.OrderBy(data => data.ResourceType).ThenBy(data => data.Type[0]).ThenBy(data => data.Name);
            return returnData;
        }

        /// <summary>
        /// Restricting item types for initial 1.4 release
        /// this does not apply to hotspots.
        /// </summary>
        /// <returns>if Item type is allowed/supported</returns>
        /// 
        private bool ResourceTypeAllowed(ItemTypeCode type)
        {
            return type == ItemTypeCode.PANORAMA_VIDEO ||
                   type == ItemTypeCode.PANORAMA ||
                   type == ItemTypeCode.STEREO_PANORAMA_VIDEO ||
                   type == ItemTypeCode.PLANAR_VIDEO ||
                   type == ItemTypeCode.AUDIO ||
                   type == ItemTypeCode.IMAGE;
        }

        /// <summary>
        /// Called on local and remote resources finished loading. avoids duplicates.
        /// </summary>
        /// <param name="data">ResourceModels list of resource data.</param>
        public void FillData(List<SResourceData> data)
        {
            InitCollections();

            ClearItemCardData();
            ListItemCardData[] items = Array.ConvertAll(data.Where(resource => ResourceTypeAllowed(resource.ItemType)).ToArray(), ResourceToCardData);
            _currentCardItems = items;
            string[] types = items.SelectMany(cardData => cardData.Type).Distinct().ToArray();

            foreach (CSearchBarTypeFilter searchBar in _searchBars)
            {
                searchBar.ReplaceTypeCollection(types);
            }

            foreach (CItemList collectionList in _collectionLists)
            {
                collectionList.ReplaceCollection(items);
            }
        }

        public void ClearItemCardData()
        {
            foreach(CItemList list in _collectionLists)
            {
                list.ClearList();
            }
        }

        /// <summary>
        /// Adds Hotspot Icons to the library.
        /// </summary>
        /// <param name="icons">array of Icons.</param>
        public void AddResources(Sprite[] icons)
        {
            InitCollections();

            foreach (CSearchBarTypeFilter searchBar in _searchBars)
            {
                searchBar.AddType("Hotspot Icon");
            }

            ListItemCardData[] newItems = Array.ConvertAll(icons, SpriteToCardData);

            for (int index = 0; index < newItems.Length; index++)
            {
                ListItemCardData card = newItems[index];
                card.Name += index;
            }

            foreach (CItemList collectionList in _collectionLists)
            {
                collectionList.AddRange(newItems);
            }
        }

        private ListItemCardData SpriteToCardData(Sprite input)
        {
            ListItemCardData cardData = new ListItemCardData();
            cardData.Name = "Status Icon";
            cardData.Type = new[] {"Hotspot Icon"};
            cardData.Icon = input;
            cardData.ResourceType = ListCardResourceTypes.HOTSPOT;
            return cardData;
        }

        private void InitCollections()
        {
            if (_collectionsInited)
            {
                return;
            }

            foreach (CItemList collectionList in _collectionLists)
            {
                collectionList.InitNow();
                collectionList.OnRemove.AddListener(OnRemoveAsset);
                collectionList.OnDownload.AddListener(OnDownload);
                collectionList.OnDragDropFailed.AddListener(OnDragDropFailed);
                collectionList.ChangeSortFunction(SortFunction);
                collectionList.ShouldSort = true;
            }

            _collectionsInited = true;

            foreach (CSearchBarTypeFilter searchBar in _searchBars)
            {
                searchBar.InitNow();
            }
        }

        public void UpdateResource(SResourceData resource)
        {
            for (int index = 0; index < _currentCardItems.Length; index++)
            {
                if (string.IsNullOrEmpty(_currentCardItems[index].ResourceID))
                {
                    continue;
                }
                if (resource.Id.Equals(_currentCardItems[index].ResourceID))
                {
                    string typeString = Enum.GetName(typeof(ItemTypeCode), resource.ItemType);
                    _currentCardItems[index].ResourceID = resource.Id;
                    _currentCardItems[index].Name = resource.ResourceName;
                    if (resource.ResourceType == ListCardResourceTypes.PLATFORM)
                    {
                        FileInfo fileInfo = new FileInfo(resource.LocalFilePath);
                        //_currentCardItems[index].Meta = $"Size: {fileInfo.Length / 1024}MB / 00:00s";

                        if (resource.ItemType == ItemTypeCode.PANORAMA || resource.ItemType == ItemTypeCode.IMAGE)
                        {
                            _currentCardItems[index].Meta = $"Size: {GetFileSize(fileInfo.Length)}";
                        }
                        else
                        {
                            _currentCardItems[index].Meta = $"Size: {GetFileSize(fileInfo.Length)} / 00:00s";
                        }
                    }
                    else
                    {
                        _currentCardItems[index].Meta = "";
                    }
                    _currentCardItems[index].Type = new[] {typeString};
                    _currentCardItems[index].ResourceType = resource.ResourceType;
                }
            }
        }

        internal void ShowResourceImported()
        {
            _genericAlertDialog.SetActive(true);

            GenericPopupDialogTMPro dialogClass = _genericAlertDialog.GetComponent<GenericPopupDialogTMPro>();

            dialogClass.InitializeDialog("Success", "Resources imported successfully", "OK", null);
        }

        public void UpdateThumbnails(Dictionary<string, Sprite> thumbnailDictionary)
        {
            foreach (ListItemCardData cardData in _currentCardItems)
            {
                if (string.IsNullOrEmpty(cardData.ResourceID))
                {
                    continue;
                }
                if (thumbnailDictionary.ContainsKey(cardData.ResourceID))
                {
                    Sprite sprite = thumbnailDictionary[cardData.ResourceID];
                    if (sprite != null)
                    {
                        cardData.Icon = sprite;
                    }
                }
            }
        }

        /// <summary>
        /// Opens the window to a specific filter. used by Dropbox.
        /// </summary>
        /// <param name="filter">Filter(s) to use.</param>
        public void OpenToFilter(string filter)
        {
            foreach (CSearchBarTypeFilter searchBar in _searchBars)
            {
                searchBar.SetFilter(new[] {filter});
            }

            ShowPanel();
        }

        /// <summary>
        /// Tween the panel close and optionally clear the current filter to default.
        /// </summary>
        /// <param name="resetFilter"></param>
        public void ClearUnclearFilter(bool resetFilter)
        {
            if (resetFilter)
            {
                foreach (CSearchBarTypeFilter searchBar in _searchBars)
                {
                    searchBar.ResetFilter();
                }
            }
        }

        private ListItemCardData ResourceToCardData(SResourceData input)
        {
            ListItemCardData result = new ListItemCardData();
            string typeString = Enum.GetName(typeof(ItemTypeCode), input.ItemType);

            result.ResourceID = input.Id;
            result.Name = input.ResourceName;
            if (input.ResourceType == ListCardResourceTypes.PLATFORM)
            {
                FileInfo fileInfo = new FileInfo(input.LocalFilePath);

                if (input.ItemType == ItemTypeCode.PANORAMA || input.ItemType == ItemTypeCode.IMAGE)
                {
                    result.Meta = $"Size: {GetFileSize(fileInfo.Length)}";
                }
                else
                {
                    result.Meta = $"Size: {GetFileSize(fileInfo.Length)} / 00:00s";
                }
            }
            else
            {
                result.Meta = "";
            }

            result.Type = new[] {typeString};
            result.ResourceType = input.ResourceType;
            return result;
        }

        private bool IsMedia(ItemTypeCode itemType)
        {
            if(itemType == ItemTypeCode.PANORAMA ||
                itemType == ItemTypeCode.PANORAMA_VIDEO ||
                itemType == ItemTypeCode.STEREO_PANORAMA_VIDEO ||
                itemType == ItemTypeCode.AUDIO ||
                itemType == ItemTypeCode.IMAGE)
            {
                return true;
            }

            return false;
        }

        private bool IsMedia(int itemType)
        {
            if (itemType == 4 ||
                itemType == 6 ||
                itemType == 17 ||
                itemType == 20 ||
                itemType == 21)
            {
                return true;
            }

            return false;
        }

        private string GetFileSize(long bytes)
        {
            if(bytes / 1024 < 1000)
            {
                return (bytes / 1024) + " KB";
            }
            else if(bytes / 1024 / 1024 < 1000)
            {
                return (bytes / 1024 / 1024) + " MB";
            }
            else
            {
                return (bytes / 1024 / 1024 / 1024) + " GB";
            }
        }

        #region Panel

        public string PanelId
        {
            get { return gameObject.GetComponent<CPanel>()._panelId; }
        }

        public void HidePanel()
        {
            ClearUnclearFilter(false);

            gameObject.SetActive(false);

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelId);

            dispatcher.Dispatch(ResourceManagerEvent.PANEL_HIDDEN, msgData);
        }

        public void ShowPanel()
        {
            if (!gameObject.activeInHierarchy)
            {
                gameObject.SetActive(true);

                MessageData msgData = new MessageData();
                msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelId);

                dispatcher.Dispatch(ResourceManagerEvent.PANEL_SHOWN, msgData);
            }
        }

        public void ShowPanelWithTween()
        {
            Debug.Log("ViewResourceManager.ShowPanelWithTween is not yet implemented");
        }

        public void HidePanelWithTween()
        {
            Debug.Log("ViewResourceManager.HidePanelWithTween is not yet implemented");
        }

        public void IncreaseHeight(float height)
        {
            Debug.Log("ViewResourceManager.IncreaseHeight is not yet implemented");
        }

        public void IncreaseHeightFromBottom(float height)
        {
            Debug.Log("ViewResourceManager.IncreaseHeightFromBottom is not yet implemented");
        }

        public void IncreaseHeightFromTop(float height)
        {
            Debug.Log("ViewResourceManager.IncreaseHeightFromTop is not yet implemented");
        }

        public void DecreaseHeight(float height)
        {
            Debug.Log("ViewResourceManager.DecreaseHeight is not yet implemented");
        }

        public void DecreaseHeightFromBottom(float height)
        {
            Debug.Log("ViewResourceManager.DecreaseHeightFromBottom is not yet implemented");
        }

        public void DecreaseHeightFromTop(float height)
        {
            Debug.Log("ViewResourceManager.DecreaseHeightFromTop is not yet implemented");
        }

        public void IncreaseWidth(float width)
        {
            Debug.Log("ViewResourceManager.IncreaseWidth is not yet implemented");
        }

        public void IncreaseWidthFromRight(float width)
        {
            Debug.Log("ViewResourceManager.IncreaseWidthFromRight is not yet implemented");
        }

        public void IncreaseWidthFromLeft(float width)
        {
            Debug.Log("ViewResourceManager.IncreaseWidthFromLeft is not yet implemented");
        }

        public void DecreaseWidth(float width)
        {
            Debug.Log("ViewResourceManager.DecreaseWidth is not yet implemented");
        }

        public void DecreaseWidthFromRight(float width)
        {
            Debug.Log("ViewResourceManager.DecreaseWidthFromRight is not yet implemented");
        }

        public void DecreaseWidthFromLeft(float width)
        {
            Debug.Log("ViewResourceManager.DecreaseWidthFromLeft is not yet implemented");
        }

        public void RepositionToTop(float height)
        {
            gameObject.GetComponent<CPanel>().RepositionToTop(height);
        }

        public void RepositionToBottom(float height)
        {
            gameObject.GetComponent<CPanel>().RepositionToBottom(height);
        }

        public void RepositionToLeft(float width)
        {
            gameObject.GetComponent<CPanel>().RepositionToLeft(width);
        }

        public void RepositionToRight(float width)
        {
            gameObject.GetComponent<CPanel>().RepositionToRight(width);
        }

        #endregion

        public void ShowList(string filterName)
        {
            OpenToFilter(filterName);
        }
    }
}