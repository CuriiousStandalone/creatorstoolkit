﻿using System.Collections.Generic;
using System.Linq;

using UnityEngine;

using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Extensions;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public class MediatorResourceManager : EventMediator
    {
        [Inject]
        public ViewResourceManager view { get; set; }

        private List<SResourceData> _lastestResourceData = new List<SResourceData>();

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.Init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewResourceManager.ResourceManagerEvent.VIEW_LOADED, OnViewLoaded);
            view.dispatcher.UpdateListener(value, ViewResourceManager.ResourceManagerEvent.PANEL_HIDDEN, OnPanelHidden);
            view.dispatcher.UpdateListener(value, ViewResourceManager.ResourceManagerEvent.PANEL_SHOWN, OnPanelShown);
            view.dispatcher.UpdateListener(value, ViewResourceManager.ResourceManagerEvent.ITEM_DROP_FAILED, OnDragDropFailed);
            view.dispatcher.UpdateListener(value, ViewResourceManager.ResourceManagerEvent.REMOVE_ASSET_CLICKED, RemoveAssetClicked);
            view.dispatcher.UpdateListener(value, ViewResourceManager.ResourceManagerEvent.ADD_THUMBNAIL_CLICKED, AddThumbnailClicked);
            view.dispatcher.UpdateListener(value, ViewResourceManager.ResourceManagerEvent.DOWNLOAD_ASSET_CLICKED, OnDownload);

            dispatcher.UpdateListener(value, EResourceEvent.HIDE_PANEL, OnHidePanel);
            dispatcher.UpdateListener(value, EResourceEvent.SHOW_PANEL, OnShowPanel);
            dispatcher.UpdateListener(value, EResourceEvent.RESPONSE_PLATFORM_RESOURCE_DATA, OnReceivedPlatformResourceData);
            dispatcher.UpdateListener(value, EResourceEvent.ALL_THUMBNAILS_LOADED, ThumbnailsLoaded);
            dispatcher.UpdateListener(value, EResourceEvent.RESOURCE_UPDATED, ResourceUpdated);
            dispatcher.UpdateListener(value, EResourceEvent.REMOTE_RESOURCE_DOWNLOADED, ResourceFinishedDownloading);
            dispatcher.UpdateListener(value, EResourceEvent.HOTSPOT_ICONS_LOADED, HotspotIconsLoaded);
            dispatcher.UpdateListener(value, EResourceEvent.SHOW_FILTERED_LIST, OnShowHotspotIconList);
            dispatcher.UpdateListener(value, EResourceEvent.RESOURCE_IMPORTED, OnResourceImported);
        }

        private void OnShowHotspotIconList(IEvent payload)
        {
            MessageData data = (MessageData)payload.data;
            string filter = (string)data[(byte)EParameterCodeToolkit.RESOURCE_LIST_FILTER];

            view.ShowList(filter);
        }

        private void OnDragDropFailed(IEvent payload)
        {
            dispatcher.Dispatch(EResourceEvent.ON_DRAG_DROP_FAILED, payload.data);
        }

        private void ResourceFinishedDownloading(IEvent payload)
        {
            MessageData msgData = payload.MessageData();
            List<SResourceData> resources = (List<SResourceData>)msgData.Parameters[(byte)EParameterCodeResource.RESOURCE_LIST];
            foreach (SResourceData resourceData in resources)
            {
                view.UpdateResource(resourceData);
            }
        }

        #region IncomingEvents

        private void OnResourceImported()
        {
            dispatcher.Dispatch(EResourceEvent.REFRESH_RESOURCES_LIST);

            view.ShowResourceImported();
        }

        private void ResourceUpdated(IEvent payload)
        {
            MessageData msgData = payload.MessageData();
            SResourceData resource = (SResourceData)msgData.Parameters[(byte)EParameterCodeResource.RESOURCE];
            view.UpdateResource(resource);
        }

        private void ThumbnailsLoaded(IEvent payload)
        {
            MessageData msgData = payload.MessageData();
            Dictionary<string, Sprite> thumbnailDictionary = (Dictionary<string, Sprite>)msgData.Parameters[(byte)EParameterCodeResource.THUMBNAIL_DICTIONARY];
            view.UpdateThumbnails(thumbnailDictionary);
        }

        private void AddThumbnailClicked(IEvent payload)
        {
            MessageData messageData = payload.MessageData();
            ListItemCardData cardData = (ListItemCardData)messageData.Parameters[(byte)EParameterCodeResource.CARD_DATA];

            Debug.Log($"Thumbnail Clicked for {cardData.Name}");
        }

        private void OnHidePanel(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            string panelId = (string)msgData[(byte)EParameterCodeToolkit.PANEL_ID];

            if (view.PanelId == panelId)
            {
                view.HidePanel();
            }
        }

        private void OnShowPanel(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            string panelId = (string)msgData[(byte)EParameterCodeToolkit.PANEL_ID];

            if (view.PanelId == panelId)
            {
                view.ShowPanel();
            }
        }

        private void OnReceivedPlatformResourceData(IEvent payload)
        {
            MessageData msgData = (MessageData)payload.data;
            List<SResourceData> data = (List<SResourceData>)msgData[(byte)EParameterCodeResource.RESOURCE_LIST];
            _lastestResourceData = data;
            view.FillData(data);
        }

        private void HotspotIconsLoaded(IEvent payload)
        {
            MessageData msgData = (MessageData)payload.data;
            Sprite[] Icons = (Sprite[])msgData[(byte)EParameterCodeResource.HOTSPOT_ICONS];
            view.AddResources(Icons);
        }

        #endregion

        #region OutGoingEvents

        private void RemoveAssetClicked(IEvent payload)
        {
            MessageData messageData = payload.MessageData();
            ListItemCardData cardData = (ListItemCardData)messageData.Parameters[(byte)EParameterCodeResource.CARD_DATA];
            Debug.Log($"Remove Clicked for {cardData.Name}");

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeResource.RESOURCE_ID, cardData.ResourceID);
            dispatcher.Dispatch(EResourceEvent.REMOVE_LOCAL_RESOURCE, msgData);
        }

        private void OnDownload(IEvent payload)
        {
            MessageData messageData = payload.MessageData();

            ListItemCardData cardData = (ListItemCardData)messageData.Parameters[(byte)EParameterCodeResource.CARD_DATA];

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeResource.RESOURCE, _lastestResourceData.First(resource => resource.Id == cardData.ResourceID));

            dispatcher.Dispatch(EResourceEvent.DOWNLOAD_REMOTE_RESOURCE, msgData);
        }

        private void OnViewLoaded(IEvent evt)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.PANEL_LOADED, evt.data);
        }

        private void OnPanelHidden(IEvent evt)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.PANEL_HIDDEN, evt.data);
        }

        private void OnPanelShown(IEvent evt)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.PANEL_SHOWN, evt.data);
        }

        #endregion
    }
}