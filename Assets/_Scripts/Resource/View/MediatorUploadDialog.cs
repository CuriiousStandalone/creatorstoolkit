using System.Collections.Generic;
using System.Linq;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

using PulseIQ.Local.ClientCommon.Components.Common;

using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public class MediatorUploadDialog : EventMediator
    {
        [Inject]
        public ViewUploadDialog view { get; set; }

        private List<SResourceData> _remoteResources = new List<SResourceData>();

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.Init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewUploadDialog.Events.CHECK_RESOURCE_EXISTENCE, CheckForRemoteResource);
            view.dispatcher.UpdateListener(value, ViewUploadDialog.Events.REQUEST_RESOURCES_REFRESH, RequestResourcesUpdate);
            view.dispatcher.UpdateListener(value, ViewUploadDialog.Events.UPLOAD_FILES, UploadFiles);

            dispatcher.UpdateListener(value, EResourceEvent.SHOW_UPLOAD_DIALOG, ShowView);
            dispatcher.UpdateListener(value, EResourceEvent.HIDE_UPLOAD_DIALOG, HideView);
            dispatcher.UpdateListener(value, EResourceEvent.UPDATE_PROGRESS_BAR, UpdateProgressBar);
            //dispatcher.UpdateListener(value, EResourceEvent.RESOURCE_UPLOADED, OnResourceUploadCompleted);
            dispatcher.UpdateListener(value, EResourceEvent.RESPONSE_PLATFORM_RESOURCE_DATA, RemoteResourcesReceived);
        }

        private void CheckForRemoteResource(IEvent payload)
        {
            MessageData msgData = (MessageData)payload.data;
            string resourceId = (string)msgData.Parameters[(byte)EParameterCodeToolkit.RESOURCE_ID];
            if (_remoteResources.Any(resourceData => resourceData.Id.Equals(resourceId)))
            {
                view.RemoteResourceExists();
            }
            else
            {
                view.RemoteResourceIsUnique();
            }
        }

        #region Incoming

        private void RemoteResourcesReceived(IEvent payload)
        {
            MessageData msgData = (MessageData)payload.data;
            List<SResourceData> data = (List<SResourceData>)msgData[(byte)EParameterCodeResource.RESOURCE_LIST];
            _remoteResources = data;
        }

        private void UpdateProgressBar(IEvent payload)
        {
            MessageData msgData = (MessageData)payload.data;
            view.UpdateProgress((double)msgData.Parameters[(byte)EParameterCodeToolkit.PROGRESS_BAR_VALUE]);
        }

        private void OnResourceUploadCompleted(IEvent payload)
        {
            view.OnResourceUploadCompleted();
        }

        private void HideView()
        {
            view.Hide();
        }

        private void ShowView()
        {
            view.Show();
        }

        #endregion

        #region outgoing

        private void RequestResourcesUpdate(IEvent payload)
        {
            dispatcher.Dispatch(EResourceEvent.REFRESH_RESOURCES_LIST);
        }

        private void UploadFiles(IEvent payload)
        {
            MessageData msgData = (MessageData)payload.data;
            dispatcher.Dispatch(EResourceEvent.UPLOAD_MEDIA_RESOURCES, msgData);
        }

        private void RequestResourceData()
        {
            dispatcher.Dispatch(EResourceEvent.REQUEST_PLATFORM_RESOURCE_DATA);
        }

        #endregion
    }
}