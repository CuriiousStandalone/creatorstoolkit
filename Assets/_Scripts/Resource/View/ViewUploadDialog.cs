using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using Button = UnityEngine.UI.Button;

using SFB;

using strange.extensions.mediation.impl;

using TMPro;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents;
using PulseIQ.Local.ClientCommon.Extensions;
using PulseIQ.Local.Common;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    //    Flow of adding to the upload queue. 
    //                                                +------------------+
    //      +----------------------------------------->New Files to Queue|
    //      |                                         +--------+---------+
    //      |                                                  |
    //      |                                                  |
    //      |                                         +--------+---------+
    //      |                                         |                  |
    //      |                                         |   Check Queue    |
    //      |                                         |                  |
    //      |                                         +--------+---------+
    //      |                                                  |
    //      |                                                  |
    //      |                                                  v
    //      |                                       +----------+--------------+
    //      |                                       | Check CMS for collision <------------------------<-------+
    //      |                                   +---+                         +-----------------+              |
    //      |                                   |   +-------------------------+                 |              |
    //      |                                   |                                               |              |
    //      |                     +-------------+                                               |              |
    //      |                     |                                                             |              |
    //      |                     v                                                             v              |
    //      |            +--------+--------------------------+                    +-------------+-----------+  |
    //      |            | Check Items Extension and if it's |     Failed         |  Item is already on CMS |  |
    //      |            | in the upload list                +-------------+      |                         |  |
    //      |            |                                   |             |      +------------+------------+  ^
    //      |            +---------+-------------------------+             |                   |               |
    //      |                      |                                       |                   |               |
    //      |               +------v--------+                              |                   |               |
    //      |               |  Create Item  |                              |                   |               |
    //      +---------------+               |                              |                   v               |
    //                      +---------------+                              |         +---------+---------+     |
    //                                                                     +-------->+   Rename Dialog   +-----+
    //                                                                               +-------------------+

    public class ViewUploadDialog : EventView
    {
        [SerializeField]
        private TMP_Dropdown _dropdown;

        [SerializeField]
        private CItemList _itemList;

        [SerializeField]
        private Image _backPanel;

        //for covering the Upload dialog during other overlaying modals.
        [SerializeField]
        private Image _modalBackPanel;

        //for covering the Upload dialog during other overlaying modals.
        [SerializeField]
        private Image _progressBarFillImage;

        [SerializeField]
        private GameObject _progressBar;

        [SerializeField]
        private Button _uploadBtn;

        [SerializeField]
        private Button _cancelBtn;

        [SerializeField]
        private GenericPopupDialogTMPro _genericAlertPopupDialog;

        private string _currentTypeSelected;
        private string[] _newFilePaths;

        private GameObject _renameDialogInstance;
        private GenericRenameDialog _renameDialog;
        private Queue<string> _pathQueue = new Queue<string>();

        private readonly List<SResourceData> _newResourcesToUpload = new List<SResourceData>();

        private const int MAX_FILE_SIZE = 500; //MB
        private const long THUMBNAIL_MAX_SIZE = 50; //KB
        //the path it currently is.
        private string _currentName;
        //the path that was selected
        private string _currentPathOriginal;

        private Dictionary<ListItemCardData, string> _uploadList = new Dictionary<ListItemCardData, string>();
        private Dictionary<ListItemCardData, string> _uploadListThumbnails = new Dictionary<ListItemCardData, string>();

        public enum Events
        {
            CHECK_RESOURCE_EXISTENCE,
            UPLOAD_FILES,
            REQUEST_RESOURCES_REFRESH
        }

        private readonly string[] _options =
        {
            "First select a file type",
            "PANORAMA",
            "PANORAMA_VIDEO",
            "PLANAR_VIDEO",
            "STEREO_PANORAMA_VIDEO",
            "AUDIO",
            "IMAGE",
        };

        private string LastUsedPath
        {
            get
            {
                string dir = PlayerPrefs.GetString("LastUsedPath", @"C:\");
                return Directory.Exists(dir) ? dir : @"C:\";
            }
            set { PlayerPrefs.SetString("LastUsedPath", value); }
        }

        public void Init()
        {
            _dropdown.options.AddRange(Array.ConvertAll(_options, EnumStringToDisplay));
            var resourcePrefab = Resources.Load<GameObject>("Prefabs/UI/RenameDialog");
            if (resourcePrefab != null)
            {
                _renameDialogInstance = Instantiate(resourcePrefab, transform.parent);
                _renameDialogInstance.SetActive(false);
                _renameDialog = _renameDialogInstance.GetComponent<GenericRenameDialog>();
            }
        }

        private void UpdateListeners(bool value)
        {
            _dropdown.onValueChanged.UpdateListeners(value, OnDropdownValueChanged);
            _itemList.OnRemove.UpdateListeners(value, UploadListItemRemoved);
            _itemList.OnAddThumbnail.UpdateListeners(value, OnAddThumbnailClicked);
            _cancelBtn.onClick.UpdateListeners(value, OnCancelClicked);
            _uploadBtn.onClick.UpdateListeners(value, OnPublishClicked);
        }

        #region UploadListManagment

        private void OnDropdownValueChanged(int optionIndex)
        {
            _currentTypeSelected = _dropdown.options[optionIndex].text;
            switch (DisplayToEnumString(_dropdown.options[optionIndex].text))
            {
                case "PANORAMA":
                    _newFilePaths = StandaloneFileBrowser.OpenFilePanel("Open Panorama Images", LastUsedPath, "png", true);
                    break;
                case "PANORAMA_VIDEO":
                    _newFilePaths = StandaloneFileBrowser.OpenFilePanel("Open Panorama Videos", LastUsedPath, "mp4", true);
                    break;
                case "PLANAR_VIDEO":
                    _newFilePaths = StandaloneFileBrowser.OpenFilePanel("Open Videos", LastUsedPath, "mp4", true);
                    break;
                case "STEREO_PANORAMA_VIDEO":
                    _newFilePaths = StandaloneFileBrowser.OpenFilePanel("Open Stereo Videos", LastUsedPath, "mp4", true);
                    break;
                case "AUDIO":
                    ExtensionFilter[] extensions = new[]
                    {
                        new ExtensionFilter("Audio", new string[] { /*"oga", "mp3",*/ "wav" })
                    };
                    _newFilePaths = StandaloneFileBrowser.OpenFilePanel("Open Audio File", LastUsedPath, extensions, true);
                    break;
                case "IMAGE":
                    _newFilePaths = StandaloneFileBrowser.OpenFilePanel("Open Image File", LastUsedPath, "png", true);
                    break;
                default:
                    Debug.LogError("Selected Value Not Supported.");
                    return;
            }

            if (_newFilePaths != null)
            {
                _pathQueue = new Queue<string>(_newFilePaths);
                CheckQueue();
            }

            _dropdown.SetValueWithoutNotify(0);
        }

        /// <summary>
        /// checks to see if any other files need to be added.
        /// </summary>
        private void CheckQueue()
        {
            if (_pathQueue.Count > 0)
            {
                //Show Rename Dialog
                _currentPathOriginal = _pathQueue.Dequeue();
                _currentName = Path.GetFileName(_currentPathOriginal);
                CheckIfFileIsAlreadyOnCMS();
            }
        }

        /// <summary>
        /// Checks current Remote Resources to see if this file is already located on the CMS.
        /// </summary>
        private void CheckIfFileIsAlreadyOnCMS()
        {
            //os the file to be uploaded there.
            if (File.Exists(_currentPathOriginal))
            {
                if (new FileInfo(_currentPathOriginal).Length / 1024 / 1024 > MAX_FILE_SIZE)
                {
                    _genericAlertPopupDialog.InitializeDialog("FILE TOO LARGE",
                        $"The selected file ({Path.GetFileName(_currentPathOriginal)}) is too large. Files must be under {MAX_FILE_SIZE}KBs",
                        "Ok", null);
                    CheckQueue();
                }
                else
                {
                    //check if on cms already
                    MessageData msgData = new MessageData();
                    //is it's current name ok?
                    msgData.Parameters.Add((byte)EParameterCodeToolkit.RESOURCE_ID, _currentName);
                    dispatcher.Dispatch(Events.CHECK_RESOURCE_EXISTENCE, msgData);
                }
            }
        }

        /// <summary>
        /// Remote resource matched same id so user must rename.
        /// </summary>
        public void RemoteResourceExists()
        {
            //Show Rename Dialog.
            _modalBackPanel.enabled = false;
            string itemName = Path.GetFileName(_currentName);
            _renameDialog.Initialize(
                $"{itemName} already exists, you cannot overwrite resources with the same name. Please enter a different name below.",
                itemName, OnRenameComplete, OnRenameCanceled);
        }

        /// <summary>
        /// the user canceled the rename so ignore that file input.
        /// </summary>
        private void OnRenameCanceled()
        {
            _modalBackPanel.enabled = true;
            CheckQueue();
        }

        /// <summary>
        /// check that the new name is unique.
        /// </summary>
        private void OnRenameComplete(string newName)
        {
            _modalBackPanel.enabled = true;
            _currentName = newName;
            CheckIfFileIsAlreadyOnCMS();
        }

        /// <summary>
        /// when name is unique it can now be added to the upload list.
        /// need to check the rename queue in case renaming got us here.
        /// </summary>
        public void RemoteResourceIsUnique()
        {
            string ext = GetItemTypeFileExt((ItemTypeCode)Enum.Parse(typeof(ItemTypeCode), DisplayToEnumString(_currentTypeSelected)));
            bool inUploadList = _uploadList.Any(kv => kv.Key.Name.Equals(_currentName));
            string[] exts = ext.Split(',');
            bool hasCorrectExtension = ext == "";
            foreach (string extension in exts)
            {
                hasCorrectExtension |= _currentName.EndsWith(ext);

            } 

            if (!inUploadList && hasCorrectExtension)
            {
                CreateResourceItem();
                CheckQueue();
            }
            else if (inUploadList)
            {
                RemoteResourceExists();
            }
            else
            {
                _modalBackPanel.enabled = false;
                string itemName = Path.GetFileName(_currentName);
                _renameDialog.Initialize(
                    $"{itemName} has the wrong file type extension(\"{ext}\"). Please enter a different name below.",
                    itemName, OnRenameComplete, OnRenameCanceled);
            }
        }

        /// <summary>
        /// Adds a card to the upload list that represents the new Resource prior to uploading.
        /// Thumbnails can be added at this point.
        /// </summary>
        public void CreateResourceItem()
        {
            if (!File.Exists(_currentPathOriginal))
            {
                return;
            }
            var newResource = new ListItemCardData();
            newResource.Name = _currentName;
            newResource.ResourceID = _currentName;
            newResource.Meta = $"Size {new FileInfo(_currentPathOriginal).Length / 1024 / 1024}MB";
            newResource.Type = new[] {_currentTypeSelected};
            newResource.ResourceType = ListCardResourceTypes.PLATFORM;
            _itemList.AddItem(newResource);
            _uploadList.Add(newResource, _currentPathOriginal);
            _uploadBtn.interactable = true;

            if(!Directory.Exists(Settings.TempDownloadPath))
            {
                Directory.CreateDirectory(Settings.TempDownloadPath);
            }

            string thumbnailPath = Path.Combine(Settings.TempDownloadPath, Path.GetFileNameWithoutExtension(_currentName) + ".png");

            newResource.LoadIcon(_currentPathOriginal, thumbnailPath);

            _uploadListThumbnails.Add(newResource, thumbnailPath);
        }

        private void UploadListItemRemoved(ListItemCardData dataGettingRemoved)
        {
            _itemList.RemoveItem(dataGettingRemoved.ResourceID);
            _uploadList.Remove(dataGettingRemoved);
            if (_itemList.Count <= 0)
            {
                _uploadBtn.interactable = false;
            }
        }

        private void OnAddThumbnailClicked(ListItemCardData itemToAddThumbnailTo)
        {
            var paths = StandaloneFileBrowser.OpenFilePanel("Select Thumbnail", LastUsedPath, "png", false);
            if (paths == null || paths.Length <= 0)
            {
                return;
            }
            string thumbnailPath = paths[0];
            long fileSizeInKb = new FileInfo(thumbnailPath).Length / 1024;
            //Check Size here.
            if (fileSizeInKb <= THUMBNAIL_MAX_SIZE)
            {
                _uploadListThumbnails.Add(itemToAddThumbnailTo, thumbnailPath);
                byte[] fileData = File.ReadAllBytes(thumbnailPath);
                Texture2D tex = new Texture2D(2, 2);
                tex.LoadImage(fileData);
                itemToAddThumbnailTo.Icon = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2());
            }
            else
            {
                _genericAlertPopupDialog.InitializeDialog("FILE TOO LARGE",
                    $"The selected file is too large for a thumbnail. Thumbnails must be under {THUMBNAIL_MAX_SIZE}KBs",
                    "Ok", null);
            }
        }

        #endregion

        #region Upload

        private void OnPublishClicked()
        {
            foreach (var dictRef in _uploadList)
            {
                _newResourcesToUpload.Add(ConvertToResourceData(dictRef));
            }

            _uploadBtn.interactable = false;
            _cancelBtn.interactable = false;

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeResource.RESOURCE_LIST, _newResourcesToUpload);

            dispatcher.Dispatch(Events.UPLOAD_FILES, msgData);
        }

        public void UpdateProgress(double progress)
        {
            _progressBar.SetActive(true);
            _progressBarFillImage.fillAmount = (float)(progress);
            Debug.Log($"ProgressInc: {progress} || ProgressBarVal: {_progressBarFillImage.fillAmount}");
        }

        public void OnResourceUploadCompleted()
        {
            dispatcher.Dispatch(Events.REQUEST_RESOURCES_REFRESH);
            Hide();
        }

        #endregion

        #region Window Events

        private void OnCancelClicked()
        {
            Hide();
        }

        public void Show()
        {
            _cancelBtn.enabled = true;
            _cancelBtn.interactable = true;

            _backPanel.gameObject.SetActive(true);
            UpdateListeners(true);
        }

        public void Hide()
        {
            UpdateListeners(false);
            _itemList.Clear();
            _backPanel.gameObject.SetActive(false);
            _uploadBtn.interactable = false;
            _progressBar.SetActive(false);
            _uploadList.Clear();
            _uploadListThumbnails.Clear();

            if (Directory.Exists(Settings.TempDownloadPath))
            {
                Directory.Delete(Settings.TempDownloadPath, true);
            }
        }

        #endregion

        #region Utils

        private static TMP_Dropdown.OptionData EnumStringToDisplay(string input)
        {
            return new TMP_Dropdown.OptionData(input.First().ToString().ToUpper() + (input.Substring(1).ToLowerInvariant().Replace("_", " ")));
        }

        private static string DisplayToEnumString(string input)
        {
            return input.ToUpperInvariant().Replace(" ", "_");
        }

        private static string GetItemTypeFileExt(ItemTypeCode type)
        {
            switch (type)
            {
                case ItemTypeCode.PANORAMA_VIDEO:
                case ItemTypeCode.PLANAR_VIDEO:
                case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                    return ".mp4";
                case ItemTypeCode.AUDIO:
                    return ".wav";//.oga, .mp3, 
                case ItemTypeCode.PANORAMA:
                case ItemTypeCode.IMAGE:
                    return ".png";
                default:
                    return "";
            }
        }

        private SResourceData ConvertToResourceData(KeyValuePair<ListItemCardData, string> dictRef)
        {
            SResourceData resourceData = new SResourceData();
            resourceData.ResourceName = dictRef.Key.Name;
            resourceData.LocalFilePath = dictRef.Value;
            resourceData.ItemType = (ItemTypeCode)Enum.Parse(typeof(ItemTypeCode), DisplayToEnumString(dictRef.Key.Type[0]));
            resourceData.ResourceType = ListCardResourceTypes.PLATFORM;
            resourceData.LocalVersion = 1;
            resourceData.RemoteVersion = 1;
            resourceData.MultimediaId = dictRef.Key.Name;

            switch (resourceData.ItemType)
            {
                case ItemTypeCode.PANORAMA:
                    {
                        resourceData.ResourceId = "PanoramaImagePlayer";
                        break;
                    }

                case ItemTypeCode.PANORAMA_VIDEO:
                    {
                        resourceData.ResourceId = "PanoramaVideoPlayer";
                        break;
                    }

                case ItemTypeCode.PLANAR_VIDEO:
                    {
                        resourceData.ResourceId = "PlanarVideoPlayer";
                        break;
                    }

                case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                    {
                        resourceData.ResourceId = "StereoPanoramaVideoPlayer";
                        break;
                    }

                case ItemTypeCode.AUDIO:
                    {
                        resourceData.ResourceId = "AudioPlayer";
                        break;
                    }

                case ItemTypeCode.IMAGE:
                    {
                        resourceData.ResourceId = "ImagePlayer";
                        break;
                    }
            }

            if (null != _uploadListThumbnails && _uploadListThumbnails.ContainsKey(dictRef.Key))
            {
                resourceData.ThumbnailPath = _uploadListThumbnails[dictRef.Key];
            }
            else
            {
                resourceData.ThumbnailPath = "";
            }

            return resourceData;
        }

        #endregion
    }
}