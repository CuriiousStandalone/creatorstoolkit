﻿using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common;

using strange.extensions.command.impl;
using System.IO.Compression;
using PulseIQ.Local.ClientCommon.Components.Common;
using System.IO;
using PulseIQ.Local.Common.Model.PanoramaVideo;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;
using PulseIQ.Local.Common.Model.Panorama;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.Common.Model.Hotspot.HotspotElement;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using UnityEngine;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandImportContentPack : EventCommand
    {
        [Inject]
        public IModelActiveWorld worldModel { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;
            string packagePath = (string)data[(byte)EParameterCodeToolkit.PACKAGE_PATH];

            string savePath = Settings.editorRootPath;

            using (ZipArchive ciqPackage = ZipFile.Open(packagePath, ZipArchiveMode.Read))
            {
                foreach(ZipArchiveEntry entry in ciqPackage.Entries)
                {
                    if(entry.Name == "")
                    {
                        continue;
                    }
                    string internalPath = Path.Combine("file://", savePath, entry.FullName);

                    if(!Directory.Exists(Path.GetDirectoryName(internalPath)))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(internalPath));
                    }

                    entry.ExtractToFile(Path.Combine("file://", savePath, entry.FullName), true);
                }
            }

            dispatcher.Dispatch(EEventToolkitCrossContext.RELOAD_RESOURCE_LIST);
            dispatcher.Dispatch(EEventToolkitCrossContext.IMPORTING_FINISHED);
        }
    }
}