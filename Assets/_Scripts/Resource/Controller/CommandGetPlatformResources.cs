﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CuriiousIQ.Local.CreatorsToolkit.Resource;

using PulseIQ.Local.ClientCommon.Components.Common;

using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.api;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public class CommandGetPlatformResources : EventCommand
    {
        [Inject]
        public IResourceModel resourceModel { get; set; }

        public override void Execute()
        {
            MessageData messageData = new MessageData();
            messageData.Parameters.Add((byte)EParameterCodeResource.RESOURCE_LIST, resourceModel.ResourceDataList);

            dispatcher.Dispatch(EResourceEvent.RESPONSE_PLATFORM_RESOURCE_DATA, messageData);
        }
    }
}
