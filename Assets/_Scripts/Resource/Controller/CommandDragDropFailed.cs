using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

using PulseIQ.Local.ClientCommon.Components.Common;

using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public class CommandDragDropFailed : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.RESOURCE_DRAG_DROP_FAILED, evt.data);
        }
    }
}