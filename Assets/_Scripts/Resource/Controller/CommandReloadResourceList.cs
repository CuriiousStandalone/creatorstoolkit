﻿using System.Collections.Generic;

using strange.extensions.command.impl;

using PulseIQ.Local.ClientCommon.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    /// <summary>
    /// New reloading script to reload resources every time a world is opened for new workflow
    /// </summary>
    public class CommandReloadResourceList : EventCommand
    {
        [Inject]
        public IResourceModel ResourceModel { get; set; }

        public override void Execute()
        {
            Retain();

            UpdateListeners(true);

            if (null == ResourceModel.ResourceDataList)
            {
                ResourceModel.ResourceDataList = new List<SResourceData>();
            }

            dispatcher.Dispatch(EResourceEvent.LOAD_LOCAL_RESOURCE_DATA);
        }

        private void UpdateListeners(bool value)
        {
            dispatcher.UpdateListener(value, EResourceEvent.LOCAL_RESOURCE_DATA_LOADED, OnLocalResourceDataLoaded);
        }

        private void OnLocalResourceDataLoaded()
        {
            MessageData messageData = new MessageData();
            messageData.Parameters.Add((byte)EParameterCodeResource.RESOURCE_LIST, ResourceModel.ResourceDataList);

            dispatcher.Dispatch(EResourceEvent.RESPONSE_PLATFORM_RESOURCE_DATA, messageData);

            dispatcher.Dispatch(EResourceEvent.ALL_THUMBNAILS_DOWNLOADED);

            dispatcher.Dispatch(EResourceEvent.LOAD_HOTSPOT_ICONS);

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "4");

            dispatcher.Dispatch(EEventToolkitCrossContext.SHOW_PANEL, msgData);

            UpdateListeners(false);

            Release();
        }
    }
}