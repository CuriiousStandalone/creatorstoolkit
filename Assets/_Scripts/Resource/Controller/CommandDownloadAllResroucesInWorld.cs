﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

using UnityEngine;

using strange.extensions.command.impl;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.World;
using PulseIQ.Local.Common.Model.Panorama;
using PulseIQ.Local.Common.Model.PanoramaVideo;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.Common.Model.Hotspot.HotspotElement;
using PulseIQ.Local.Common;
using PulseIQ.Local.Service;
using PulseIQ.Local.Common.Model.Resource;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public class CommandDownloadAllResroucesInWorld : EventCommand
    {
        private ContentAccess _content;

        private Action<double> _downloadAction;

        private List<DownloadData> _listDownloadData;

        private GameObject _mainThreadGameObject;

        private UnityMainThread _unityMainThread;

        private double _donePercent = 0;

        private int _totalFileToDownload = 0;

        public override void Execute()
        {
            Retain();

            _mainThreadGameObject = new GameObject();
            _unityMainThread = _mainThreadGameObject.AddComponent<UnityMainThread>();

            _downloadAction += OnDownloadProgress;

            MessageData msgData = (MessageData)evt.data;

            WorldData worldData = (WorldData)msgData[(byte)EParameterCodeToolkit.WORLD_DATA];

            _content = new ContentAccess(GlobalSettings.CMSURL);

            _listDownloadData = new List<DownloadData>();

            foreach (var item in worldData.ListItems)
            {
                if(!IgnorableType(item.ItemType))
                {
                    if(IsMediaType(item.ItemType))
                    {
                        ResourceInfoData resourceInfo = new ResourceInfoData
                        {
                            GivenRID = item.ItemResourceId,
                            ItemType = Convert.ToInt32(item.ItemType),
                        };

                        List<CTHotspotData> listHotspot = new List<CTHotspotData>();

                        switch(item.ItemType)
                        {
                            case ItemTypeCode.PANORAMA:
                                {
                                    resourceInfo.MultimediaID = ((CTPanoramaData)item).PanoramaId;
                                    listHotspot = ((CTPanoramaData)item).ListHotspot;
                                    break;
                                }
                            case ItemTypeCode.PANORAMA_VIDEO:
                                {
                                    resourceInfo.MultimediaID = ((CTPanoramaVideoData)item).PanoramaVideoId;
                                    listHotspot = ((CTPanoramaVideoData)item).ListHotspot;
                                    break;
                                }
                            case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                                {
                                    resourceInfo.MultimediaID = ((CTStereoPanoramaVideoData)item).StereoPanoramaVideoId;
                                    listHotspot = ((CTStereoPanoramaVideoData)item).ListHotspot;
                                    break;
                                }
                        }

                        if (!_listDownloadData.Any(x => x.resourceInfoData.MultimediaID == resourceInfo.MultimediaID))
                        {
                            if(!Directory.Exists(Settings.GetPathByItemType(item.ItemType)))
                            {
                                Directory.CreateDirectory(Settings.GetPathByItemType(item.ItemType));
                            }

                            DownloadData downloadData = new DownloadData();
                            downloadData.resourceInfoData = resourceInfo;
                            downloadData.downloadPath = Path.Combine(Settings.GetPathByItemType(item.ItemType), resourceInfo.MultimediaID);

                            _listDownloadData.Add(downloadData);
                        }

                        if(listHotspot.Count > 0)
                        {
                            foreach(var hotspotItem in listHotspot)
                            {
                                if(hotspotItem.HotspotBoxes.Count > 0)
                                {
                                    foreach(var hotspotBoxItem in hotspotItem.HotspotBoxes)
                                    {
                                        if(hotspotBoxItem.Elements.Count > 0)
                                        {
                                            foreach(var hotspotElementItem in hotspotBoxItem.Elements)
                                            {
                                                ResourceInfoData infoData = null;

                                                if (hotspotElementItem.TypeCode == EHotspotElementTypeCode.AUDIO)
                                                {
                                                    infoData = new ResourceInfoData
                                                    {
                                                        MultimediaID = ((CTHotspotElementAudioData)hotspotElementItem).MultimediaId,
                                                        ItemType = 20,
                                                    };
                                                }
                                                else if (hotspotElementItem.TypeCode == EHotspotElementTypeCode.IMAGE)
                                                {
                                                    infoData = new ResourceInfoData
                                                    {
                                                        MultimediaID = ((CTHotspotElementImageData)hotspotElementItem).MultimediaId,
                                                        ItemType = 21,
                                                    };
                                                }

                                                if (null != infoData)
                                                {
                                                    if(!_listDownloadData.Any(x => x.resourceInfoData.MultimediaID == infoData.MultimediaID))
                                                    {
                                                        if(!Directory.Exists(Settings.GetPathByItemType((ItemTypeCode)infoData.ItemType)))
                                                        {
                                                            Directory.CreateDirectory(Settings.GetPathByItemType((ItemTypeCode)infoData.ItemType));
                                                        }

                                                        string path = Path.Combine(Settings.GetPathByItemType((ItemTypeCode)infoData.ItemType), infoData.MultimediaID);
                                                        DownloadData downloadData = new DownloadData();
                                                        downloadData.resourceInfoData = infoData;
                                                        downloadData.downloadPath = path;

                                                        _listDownloadData.Add(downloadData);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            _totalFileToDownload = _listDownloadData.Count;

            DownloadMediaResources();
        }

        private async void DownloadMediaResources()
        {
            if (_listDownloadData.Count > 0)
            {
                DownloadData downloadData = _listDownloadData[0];

                _listDownloadData.RemoveAt(0);

                if (!IsAvaialableLocally(downloadData.resourceInfoData.MultimediaID, (ItemTypeCode)downloadData.resourceInfoData.ItemType))
                {
                    await _content.Resource.DownloadMediaResourceAsync(downloadData.resourceInfoData, downloadData.downloadPath, _downloadAction);
                }

                _donePercent += 100;

                DownloadMediaResources();
            }
            else
            {
                dispatcher.Dispatch(EEventToolkitCrossContext.WORLD_RESOURCES_DOWNLOADED);

                _downloadAction -= OnDownloadProgress;

                Release();
            }
        }

        private void OnDownloadProgress(double progressValue)
        {
            UnityMainThread.wkr.AddJob(() =>
            {
                MessageData messageData = new MessageData();

                messageData.Parameters.Add((byte)EParameterCodeToolkit.PROGRESS_BAR_VALUE, ((progressValue + _donePercent) / _totalFileToDownload) / 100);

                dispatcher.Dispatch(EEventToolkitCrossContext.UPDATE_DOWNLOAD_PROGRESS, messageData);
            });
        }

        private bool IgnorableType(ItemTypeCode itemType)
        {
            if(itemType == ItemTypeCode.CUSTOM || itemType == ItemTypeCode.NOTATION_LINE || itemType == ItemTypeCode.NOTATION_POINT || itemType == ItemTypeCode.SLOT_GROUP || itemType == ItemTypeCode.SPAWN_POINT)
            {
                return true;
            }

            return false;
        }

        private bool IsMediaType(ItemTypeCode itemType)
        {
            if(itemType == ItemTypeCode.PANORAMA || itemType == ItemTypeCode.PANORAMA_VIDEO || itemType == ItemTypeCode.PLANAR_VIDEO || itemType == ItemTypeCode.STEREO_PANORAMA_VIDEO)
            {
                return true;
            }

            return false;
        }

        private bool IsAvaialableLocally(string fileName, ItemTypeCode itemType)
        {
            string filePath = Path.Combine(Settings.GetPathByItemType(itemType), fileName);

            if(File.Exists(filePath))
            {
                return true;
            }

            return false;
        }
    }

    public struct DownloadData
    {
        public ResourceInfoData resourceInfoData;
        public string downloadPath;
    }
}
