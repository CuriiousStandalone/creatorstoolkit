using UnityEngine;

using strange.extensions.command.impl;

using PulseIQ.Local.ClientCommon.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public class CommandSaveHotspotIcons : EventCommand
    {
        const int NUMBER_OF_ICONS = 69;

        [Inject]
        public IResourceModel Resource { get; set; }

        public override void Execute()
        {
            Resource.HotspotIcons = new Sprite[NUMBER_OF_ICONS];
            for (int index = 0; index < NUMBER_OF_ICONS; index++)
            {
                string imagePath = "Images/HotspotStatusIcons/" + "StatusIcon" + index;
                Resource.HotspotIcons[index] = Resources.Load<Sprite>(imagePath);
            }
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeResource.HOTSPOT_ICONS, Resource.HotspotIcons);
            dispatcher.Dispatch(EResourceEvent.HOTSPOT_ICONS_LOADED, msgData);
        }
    }
}