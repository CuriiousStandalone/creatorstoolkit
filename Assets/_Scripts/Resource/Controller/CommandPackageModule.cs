﻿using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common;

using strange.extensions.command.impl;
using System.IO.Compression;
using PulseIQ.Local.ClientCommon.Components.Common;
using System.IO;
using PulseIQ.Local.Common.Model.PanoramaVideo;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;
using PulseIQ.Local.Common.Model.Panorama;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.Common.Model.Hotspot.HotspotElement;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using UnityEngine;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandPackageModule : EventCommand
    {
        [Inject]
        public IModelActiveWorld worldModel { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;
            string saveFilePath = (string)data[(byte)EParameterCodeToolkit.PACKAGE_PATH];

            string savePath = Path.GetDirectoryName(saveFilePath);

            string CTKzipName = Path.GetFileName(saveFilePath);

            string zipPath = savePath + "/" + CTKzipName;

            if (File.Exists(zipPath))
            {
                File.Delete(zipPath);
            }
           
            using (ZipArchive ciqModule = ZipFile.Open(zipPath, ZipArchiveMode.Create))
            {
                string filePath;

                if (File.Exists(Path.Combine(Settings.editorRootPath, "Module.ctk")))
                {
                    ciqModule.CreateEntryFromFile((Path.Combine(Settings.editorRootPath, "Module.ctk")), worldModel.ActiveWorldData.WorldId + ".ctk", System.IO.Compression.CompressionLevel.NoCompression);
                }

                if (File.Exists(Path.Combine(Settings.WorldThumbnailPath, worldModel.ActiveWorldData.WorldThumbnailPath)))
                {
                    ciqModule.CreateEntryFromFile(Path.Combine(Settings.WorldThumbnailPath, worldModel.ActiveWorldData.WorldThumbnailPath), Path.Combine(Settings.internalImagePath, Path.GetFileName(worldModel.ActiveWorldData.WorldThumbnailPath)), System.IO.Compression.CompressionLevel.NoCompression);
                }

                foreach (CTItemData item in worldModel.ActiveWorldData.ListItems)
                {
                    switch (item.ItemType)
                    {
                        case ItemTypeCode.PANORAMA:
                            {
                                CTPanoramaData panoramaData = (CTPanoramaData)item;

                                filePath = Settings.GetThumbnailPathByItemType(ItemTypeCode.PANORAMA) + (Path.GetFileNameWithoutExtension(panoramaData.PanoramaId) + ".png");
                                Debug.Log("File Path is : " + filePath);

                                if (File.Exists(filePath))
                                {
                                    ciqModule.CreateEntryFromFile(filePath, Path.Combine(Settings.internalPanoramaThumbPath, (Path.GetFileNameWithoutExtension(panoramaData.PanoramaId) + ".png")), System.IO.Compression.CompressionLevel.NoCompression);                                   
                                }

                                filePath = Path.Combine(Settings.PanoramaPath, panoramaData.PanoramaId);

                                ciqModule.CreateEntryFromFile(filePath, Path.Combine(Settings.internalPanoramaPath, panoramaData.PanoramaId), System.IO.Compression.CompressionLevel.NoCompression);
                                Debug.Log("Asset local Path is : " + Path.Combine(Settings.internalPanoramaPath, panoramaData.PanoramaId));

                                foreach (CTHotspotData hotspotData in panoramaData.ListHotspot)
                                {
                                    if (hotspotData.HotspotBoxes.Count > 0)
                                    {
                                        foreach (CTHotspotElementBaseData elementData in hotspotData.HotspotBoxes[0].Elements)
                                        {
                                            switch (elementData.TypeCode)
                                            {
                                                case EHotspotElementTypeCode.AUDIO:
                                                    {
                                                        string id = ((CTHotspotElementAudioData)elementData).MultimediaId;

                                                        if (id == "")
                                                        {
                                                            continue;
                                                        }

                                                        filePath = Settings.GetThumbnailPathByItemType(ItemTypeCode.AUDIO) + (Path.GetFileNameWithoutExtension(id) + ".png");

                                                        if (File.Exists(filePath))
                                                        {
                                                            ciqModule.CreateEntryFromFile(filePath, Path.Combine(Settings.internalAudioThumbPath, (Path.GetFileNameWithoutExtension(id) + ".png")), System.IO.Compression.CompressionLevel.NoCompression);
                                                        }

                                                        filePath = Path.Combine(Settings.AudioPath, id);
                                                        ciqModule.CreateEntryFromFile(filePath, Path.Combine(Settings.internalAudioPath, id), System.IO.Compression.CompressionLevel.NoCompression);
                                                        break;
                                                    }

                                                case EHotspotElementTypeCode.IMAGE:
                                                    {
                                                        string id = ((CTHotspotElementImageData)elementData).MultimediaId;

                                                        if (id == "")
                                                        {
                                                            continue;
                                                        }

                                                        filePath = Settings.GetThumbnailPathByItemType(ItemTypeCode.IMAGE) + (Path.GetFileNameWithoutExtension(id) + ".png");

                                                        if(Path.GetFileName(id) == Path.GetFileName(worldModel.ActiveWorldData.WorldThumbnailPath))
                                                        {
                                                            continue;
                                                        }

                                                        if (File.Exists(filePath))
                                                        {
                                                            ciqModule.CreateEntryFromFile(filePath, Path.Combine(Settings.internalImageThumbPath, (Path.GetFileNameWithoutExtension(id) + ".png")), System.IO.Compression.CompressionLevel.NoCompression);
                                                        }

                                                        filePath = Path.Combine(Settings.ImagePath, id);
                                                        ciqModule.CreateEntryFromFile(filePath, Path.Combine(Settings.internalImagePath, id), System.IO.Compression.CompressionLevel.NoCompression);
                                                        break;
                                                    }
                                                default:
                                                    {
                                                        continue;
                                                    }
                                            }
                                        }
                                    }
                                }

                                break;
                            }

                        case ItemTypeCode.PANORAMA_VIDEO:
                            {
                                CTPanoramaVideoData panoramaData = (CTPanoramaVideoData)item;

                                filePath = Settings.GetThumbnailPathByItemType(ItemTypeCode.PANORAMA_VIDEO) + (Path.GetFileNameWithoutExtension(panoramaData.PanoramaVideoId) + ".png");

                                if (File.Exists(filePath))
                                {
                                    ciqModule.CreateEntryFromFile(filePath, Path.Combine(Settings.internalPanoramaVideoThumbPath, (Path.GetFileNameWithoutExtension(panoramaData.PanoramaVideoId) + ".png")), System.IO.Compression.CompressionLevel.NoCompression);
                                }

                                filePath = Path.Combine(Settings.PanoramaVideoPath, panoramaData.PanoramaVideoId);

                                ciqModule.CreateEntryFromFile(filePath, Path.Combine(Settings.internalPanoramaVideoPath, panoramaData.PanoramaVideoId), System.IO.Compression.CompressionLevel.NoCompression);

                                foreach (CTHotspotData hotspotData in panoramaData.ListHotspot)
                                {
                                    if (hotspotData.HotspotBoxes.Count > 0)
                                    {
                                        foreach (CTHotspotElementBaseData elementData in hotspotData.HotspotBoxes[0].Elements)
                                        {
                                            switch (elementData.TypeCode)
                                            {
                                                case EHotspotElementTypeCode.AUDIO:
                                                    {
                                                        string id = ((CTHotspotElementAudioData)elementData).MultimediaId;

                                                        if (id == "")
                                                        {
                                                            continue;
                                                        }


                                                        filePath = Settings.GetThumbnailPathByItemType(ItemTypeCode.AUDIO) + (Path.GetFileNameWithoutExtension(id) + ".png");

                                                        if (File.Exists(filePath))
                                                        {
                                                            ciqModule.CreateEntryFromFile(filePath, Path.Combine(Settings.internalAudioThumbPath, (Path.GetFileNameWithoutExtension(id) + ".png")), System.IO.Compression.CompressionLevel.NoCompression);
                                                        }

                                                        filePath = Path.Combine(Settings.AudioPath, id);
                                                        ciqModule.CreateEntryFromFile(filePath, Path.Combine(Settings.internalAudioPath, id), System.IO.Compression.CompressionLevel.NoCompression);
                                                        break;
                                                    }

                                                case EHotspotElementTypeCode.IMAGE:
                                                    {
                                                        string id = ((CTHotspotElementImageData)elementData).MultimediaId;

                                                        if (id == "")
                                                        {
                                                            continue;
                                                        }

                                                        filePath = Settings.GetThumbnailPathByItemType(ItemTypeCode.IMAGE) + (Path.GetFileNameWithoutExtension(id) + ".png");

                                                        if (Path.GetFileName(id) == Path.GetFileName(worldModel.ActiveWorldData.WorldThumbnailPath))
                                                        {
                                                            continue;
                                                        }

                                                        if (File.Exists(filePath))
                                                        {
                                                            ciqModule.CreateEntryFromFile(filePath, Path.Combine(Settings.internalImageThumbPath, (Path.GetFileNameWithoutExtension(id) + ".png")), System.IO.Compression.CompressionLevel.NoCompression);
                                                        }

                                                        filePath = Path.Combine(Settings.ImagePath, id);
                                                        ciqModule.CreateEntryFromFile(filePath, Path.Combine(Settings.internalImagePath, id), System.IO.Compression.CompressionLevel.NoCompression);
                                                        break;
                                                    }
                                                default:
                                                    {
                                                        continue;
                                                    }
                                            }
                                        }
                                    }
                                }

                                break;
                            }

                        case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                            {
                                CTStereoPanoramaVideoData panoramaData = (CTStereoPanoramaVideoData)item;

                                filePath = Settings.GetThumbnailPathByItemType(ItemTypeCode.STEREO_PANORAMA_VIDEO) + (Path.GetFileNameWithoutExtension(panoramaData.StereoPanoramaVideoId) + ".png");

                                if (File.Exists(filePath))
                                {
                                    ciqModule.CreateEntryFromFile(filePath, Path.Combine(Settings.internalStereoPanoramaVideoThumbPath, (Path.GetFileNameWithoutExtension(panoramaData.StereoPanoramaVideoId) + ".png")), System.IO.Compression.CompressionLevel.NoCompression);
                                }

                                filePath = Path.Combine(Settings.StereoPanoramaVideoPath, panoramaData.StereoPanoramaVideoId);

                                ciqModule.CreateEntryFromFile(filePath, Path.Combine(Settings.internalStereoPanoramaVideoPath, panoramaData.StereoPanoramaVideoId), System.IO.Compression.CompressionLevel.NoCompression);

                                foreach(CTHotspotData hotspotData in panoramaData.ListHotspot)
                                {
                                    if (hotspotData.HotspotBoxes.Count > 0)
                                    {
                                        foreach (CTHotspotElementBaseData elementData in hotspotData.HotspotBoxes[0].Elements)
                                        {
                                            switch (elementData.TypeCode)
                                            {
                                                case EHotspotElementTypeCode.AUDIO:
                                                    {
                                                        string id = ((CTHotspotElementAudioData)elementData).MultimediaId;

                                                        if (id == "")
                                                        {
                                                            continue;
                                                        }

                                                        filePath = Settings.GetThumbnailPathByItemType(ItemTypeCode.AUDIO) + (Path.GetFileNameWithoutExtension(id) + ".png");

                                                        if (File.Exists(filePath))
                                                        {
                                                            ciqModule.CreateEntryFromFile(filePath, Path.Combine(Settings.internalAudioThumbPath, (Path.GetFileNameWithoutExtension(id) + ".png")), System.IO.Compression.CompressionLevel.NoCompression);
                                                        }

                                                        filePath = Path.Combine(Settings.AudioPath, id);
                                                        ciqModule.CreateEntryFromFile(filePath, Path.Combine(Settings.internalAudioPath, id), System.IO.Compression.CompressionLevel.NoCompression);
                                                        break;
                                                    }

                                                case EHotspotElementTypeCode.IMAGE:
                                                    {
                                                        string id = ((CTHotspotElementImageData)elementData).MultimediaId;

                                                        if (id == "")
                                                        {
                                                            continue;
                                                        }

                                                        filePath = Settings.GetThumbnailPathByItemType(ItemTypeCode.IMAGE) + (Path.GetFileNameWithoutExtension(id) + ".png");

                                                        if (Path.GetFileName(id) == Path.GetFileName(worldModel.ActiveWorldData.WorldThumbnailPath))
                                                        {
                                                            continue;
                                                        }

                                                        if (File.Exists(filePath))
                                                        {
                                                            ciqModule.CreateEntryFromFile(filePath, Path.Combine(Settings.internalImageThumbPath, (Path.GetFileNameWithoutExtension(id) + ".png")), System.IO.Compression.CompressionLevel.NoCompression);
                                                        }

                                                        filePath = Path.Combine(Settings.ImagePath, id);
                                                        ciqModule.CreateEntryFromFile(filePath, Path.Combine(Settings.internalImagePath, id), System.IO.Compression.CompressionLevel.NoCompression);
                                                        break;
                                                    }
                                                default:
                                                    {
                                                        continue;
                                                    }
                                            }

                                        }
                                    }
                                }

                                break;
                            }

                        default:
                            {
                                continue;
                            }
                    }

                }

            }

            dispatcher.Dispatch(EEventToolkitCrossContext.PACKAGING_FINISHED);
        }
    }
}