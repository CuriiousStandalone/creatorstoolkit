﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

using UnityEngine;

using strange.extensions.command.impl;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.Resource;
using PulseIQ.Local.Service;
using PulseIQ.Local.Common;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public class CommandFetchRemoteResource : EventCommand
    {
        [Inject]
        public IResourceModel ResourceModel { get; set; }

        private bool _isResource;
        private bool _isMedia;

        private ContentAccess _content;

        private Action<double> _downloadProgress;

        private GameObject _mainThreadGameObject;

        private UnityMainThread _unityMainThread;

        public override void Execute()
        {
            Retain();

            _mainThreadGameObject = new GameObject();
            _unityMainThread = _mainThreadGameObject.AddComponent<UnityMainThread>();

            _downloadProgress += DownloadProgress;

            dispatcher.Dispatch(EEventToolkitCrossContext.SHOW_DOWNLOAD_PROGRESS_WINDOW);

            MessageData messageData = (MessageData)evt.data;

            SResourceData _remoteResourceData = (SResourceData)messageData[(byte)EParameterCodeResource.RESOURCE];

            _content = new ContentAccess(GlobalSettings.CMSURL);

            ResourceInfoData resourceData = new ResourceInfoData
            {
                GivenRID = _remoteResourceData.ResourceId,
                ItemType = (int)_remoteResourceData.ItemType,
                MultimediaID = _remoteResourceData.MultimediaId
            };

            if (_remoteResourceData.ItemType == ItemTypeCode.PANORAMA || _remoteResourceData.ItemType == ItemTypeCode.PANORAMA_VIDEO || _remoteResourceData.ItemType == ItemTypeCode.PLANAR_VIDEO || _remoteResourceData.ItemType == ItemTypeCode.STEREO_PANORAMA_VIDEO || _remoteResourceData.ItemType == ItemTypeCode.AUDIO || _remoteResourceData.ItemType == ItemTypeCode.IMAGE)
            {
                _isMedia = true;

                DownloadMediaFile(resourceData);
            }
            else
            {
                _isResource = true;
            }
        }

        private async void DownloadMediaFile(ResourceInfoData resourceData)
        {
            ItemTypeCode itemType = GetItemType(resourceData.ItemType);

            string downloadPath = Path.GetFullPath(Path.Combine(Settings.GetPathByItemType(itemType), resourceData.MultimediaID));

            await _content.Resource.DownloadMediaResourceAsync(resourceData, downloadPath, DownloadProgress);

            ResourceDownloaded(resourceData);
        }

        private void DownloadProgress(double progressVal)
        {
            UnityMainThread.wkr.AddJob(() =>
            {
                MessageData messageData = new MessageData();

                messageData.Parameters.Add((byte)EParameterCodeToolkit.PROGRESS_BAR_VALUE, progressVal / 100);

                dispatcher.Dispatch(EEventToolkitCrossContext.UPDATE_DOWNLOAD_PROGRESS, messageData);
            });
        }

        private void ResourceDownloaded(ResourceInfoData resourceData)
        {
            if (null == ResourceModel.ResourceDataList)
            {
                ResourceModel.ResourceDataList = new List<SResourceData>();
            }

            bool doesExist = ResourceModel.ResourceDataList.Any(i => i.MultimediaId == resourceData.MultimediaID);

            SResourceData platformResourceData = new SResourceData();

            if (doesExist)
            {
                for (int i = 0; i < ResourceModel.ResourceDataList.Count; i++)
                {
                    if (ResourceModel.ResourceDataList[i].MultimediaId != resourceData.MultimediaID)
                    {
                        continue;
                    }
                    platformResourceData = ResourceModel.ResourceDataList[i];
                    SResourceData remoteResourceData = ResourceModel.ResourceDataList.Find(j => j.MultimediaId == resourceData.MultimediaID);
                    platformResourceData.RemoteVersion = remoteResourceData.RemoteVersion;
                    platformResourceData.LocalVersion = remoteResourceData.LocalVersion;
                    platformResourceData.LocalFilePath = Path.Combine(Settings.GetPathByItemType(platformResourceData.ItemType), resourceData.MultimediaID);
                    platformResourceData.ResourceType = ListCardResourceTypes.PLATFORM;

                    ResourceModel.ResourceDataList[i] = platformResourceData;

                    break;
                }
            }
            else
            {
                platformResourceData.ResourceName = resourceData.MultimediaID;
                platformResourceData.ItemType = GetItemType(resourceData.ItemType);
                platformResourceData.MultimediaId = resourceData.MultimediaID;
                platformResourceData.LocalFilePath = Path.Combine(Settings.GetPathByItemType(platformResourceData.ItemType), resourceData.MultimediaID);
                platformResourceData.ResourceType = ListCardResourceTypes.PLATFORM;

                platformResourceData.ResourceName = Path.Combine(Settings.GetPathByItemType(platformResourceData.ItemType), resourceData.MultimediaID);

                SResourceData remoteResourceData = ResourceModel.ResourceDataList.Find(j => j.MultimediaId == resourceData.MultimediaID);

                ResourceModel.ResourceDataList.Add(platformResourceData);
            }

            List<SResourceData> updatedResources = new List<SResourceData>();
            updatedResources.Add(platformResourceData);

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeResource.RESOURCE_LIST, updatedResources);

            dispatcher.Dispatch(EResourceEvent.REMOTE_RESOURCE_DOWNLOADED, msgData);

            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_DOWNLOAD_PROGRESS_WINDOW);

            _downloadProgress -= DownloadProgress;

            GameObject.Destroy(_mainThreadGameObject);

            Release();
        }

        private ItemTypeCode GetItemType(int itemType)
        {
            switch(itemType)
            {
                default:
                case 4:
                    {
                        return ItemTypeCode.PANORAMA;
                    }

                case 5:
                    {
                        return ItemTypeCode.PANORAMA_VIDEO;
                    }

                case 6:
                    {
                        return ItemTypeCode.PLANAR_VIDEO;
                    }

                case 17:
                    {
                        return ItemTypeCode.STEREO_PANORAMA_VIDEO;
                    }

                case 20:
                    {
                        return ItemTypeCode.AUDIO;
                    }

                case 21:
                    {
                        return ItemTypeCode.IMAGE;
                    }
            }
        }
    }
}