﻿using PulseIQ.Local.ClientCommon.Components.Common;

using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public class CommandRequestPlatformResources : EventCommand
    {
        [Inject]
        public IResourceModel resourceModel { get; set; }

        public override void Execute()
        {
            MessageData messageData = new MessageData();
            messageData.Parameters.Add((byte)EParameterCodeResource.CURRENT_RESOURCE_LIST, resourceModel.ResourceDataList);

            dispatcher.Dispatch(EResourceEvent.GET_CURRENT_RESOURCE_LIST, messageData);
        }
    }
}
