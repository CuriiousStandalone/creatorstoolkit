using System.Collections;
using System.Collections.Generic;
using System.IO;

using UnityEngine;

using strange.extensions.command.impl;
using strange.extensions.context.api;
using strange.extensions.context.impl;

using PulseIQ.Local.ClientCommon.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public class CommandLoadThumbnailsToModel : EventCommand
    {
        [Inject(ContextKeys.CONTEXT_VIEW)]
        public GameObject ContextView { get; set; }

        [Inject]
        public IResourceModel ResourceModel { get; set; }

        public override void Execute()
        {
            ContextView component = ContextView.GetComponent<ContextView>();
            component.StartCoroutine(LoadAll());
            Retain();
        }

        private IEnumerator LoadAll()
        {
            if (ResourceModel.ResourceThumbnailDictionary == null)
            {
                ResourceModel.ResourceThumbnailDictionary = new Dictionary<string, Sprite>();
            }

            foreach (SResourceData resource in ResourceModel.ResourceDataList)
            {
                if (string.IsNullOrEmpty(resource.ThumbnailPath) || string.IsNullOrEmpty(resource.Id))
                {
                    continue;
                }
                if (ResourceModel.ResourceThumbnailDictionary.ContainsKey(resource.Id))
                {
                    continue;
                }

                ResourceModel.ResourceThumbnailDictionary.Add(resource.Id, LoadThumbnail(resource.ThumbnailPath));
                yield return null;
            }

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeResource.THUMBNAIL_DICTIONARY, ResourceModel.ResourceThumbnailDictionary);
            dispatcher.Dispatch(EResourceEvent.ALL_THUMBNAILS_LOADED, msgData);
        }

        private static Sprite LoadThumbnail(string imagePath)
        {
            imagePath = imagePath ?? "";

            if (imagePath == "" || !File.Exists(imagePath))
            {
                return null;
            }
            byte[] fileData = File.ReadAllBytes(imagePath);
            Texture2D tex = new Texture2D(2, 2);
            tex.LoadImage(fileData);
            return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2());
        }
    }
}