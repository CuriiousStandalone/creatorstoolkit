﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public class CommandHidePanel : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EResourceEvent.HIDE_PANEL, evt.data);
        }
    }
}
