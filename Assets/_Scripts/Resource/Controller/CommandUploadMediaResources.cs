﻿using System;
using System.IO;
using System.Collections.Generic;

using UnityEngine;

using strange.extensions.command.impl;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

using PulseIQ.Local.Common;
using PulseIQ.Local.Service;
using PulseIQ.Local.Service.Resource;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public class CommandUploadMediaResources : EventCommand
    {
        [Inject]
        public IResourceModel ResourceModel { get; set; }

        private List<SResourceData> _resourceDataList;

        private ContentAccess _content;

        private Action<double> downloadAction;

        private GameObject _mainThreadGameObject;

        private UnityMainThread _unityMainThread;

        private double _donePercent = 0;

        private int _totalFileToUpload = 0;

        public override void Execute()
        {
            Retain();

            downloadAction += OnUpdateProgressBar;

            _mainThreadGameObject = new GameObject();
            _unityMainThread = _mainThreadGameObject.AddComponent<UnityMainThread>();

            _content = new ContentAccess(GlobalSettings.CMSURL);

            MessageData messageData = (MessageData)evt.data;

            _resourceDataList = (List<SResourceData>)messageData[(byte)EParameterCodeResource.RESOURCE_LIST];

            foreach (var resource in _resourceDataList)
            {
                _totalFileToUpload++;

                if (!string.IsNullOrEmpty(resource.ThumbnailPath) && File.Exists(resource.ThumbnailPath))
                {
                    _totalFileToUpload++;
                }
            }

            UploadResource();
        }

        private void UploadResource()
        {
            if(_resourceDataList.Count > 0)
            {
                SResourceData resourceData = CopyToLocalPackageData(_resourceDataList[0]);

                _resourceDataList.RemoveAt(0);

                UploadThumbnail(resourceData);
            }
            else
            {
                dispatcher.Dispatch(EResourceEvent.RESOURCE_UPLOADED);

                if (Directory.Exists(Settings.TempDownloadPath))
                {
                    Directory.Delete(Settings.TempDownloadPath, true);
                }

                downloadAction -= OnUpdateProgressBar;

                Release();
            }
        }

        private async void UploadThumbnail(SResourceData resourceData)
        {
            if(!string.IsNullOrEmpty(resourceData.ThumbnailPath) && File.Exists(resourceData.ThumbnailPath))
            {
                await _content.Resource.UploadResourceAsync(resourceData.ThumbnailPath, resourceData.ResourceId, resourceData.ResourceName,
                resourceData.ItemType, Path.GetFileNameWithoutExtension(resourceData.MultimediaId) + ".png", EPlatformTypes.Thumbnail,
                downloadAction);

                _donePercent += 100;
            }
            
            UploadMediaFile(resourceData);
        }

        private async void UploadMediaFile(SResourceData resourceData)
        {
            await _content.Resource.UploadResourceAsync(resourceData.LocalFilePath, resourceData.ResourceId, resourceData.ResourceName,
                resourceData.ItemType, resourceData.MultimediaId, EPlatformTypes.Windows,
                downloadAction);

            _donePercent += 100;

            UploadResource();
        }

        private void OnUpdateProgressBar(double progressValue)
        {
            UnityMainThread.wkr.AddJob(() =>
            {
                MessageData messageData = new MessageData();

                messageData.Parameters.Add((byte)EParameterCodeToolkit.PROGRESS_BAR_VALUE, ((progressValue + _donePercent) / _totalFileToUpload) /100);

                dispatcher.Dispatch(EResourceEvent.UPDATE_PROGRESS_BAR, messageData);
            });
        }

        private SResourceData CopyToLocalPackageData(SResourceData resourceData)
        {
            string newLocalPath = Path.Combine(Settings.GetPathByItemType(resourceData.ItemType), resourceData.ResourceName);
            string newLocalThumbnailPath = Path.Combine(Settings.GetThumbnailPathByItemType(resourceData.ItemType), Path.GetFileNameWithoutExtension(resourceData.ResourceName) + ".png");

            string path = Path.GetDirectoryName(newLocalPath);

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            File.Copy(resourceData.LocalFilePath, newLocalPath);

            resourceData.LocalFilePath = newLocalPath;

            if (!string.IsNullOrEmpty(resourceData.ThumbnailPath))
            {
                path = Path.GetDirectoryName(newLocalThumbnailPath);

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                if (File.Exists(resourceData.ThumbnailPath))
                {
                    File.Copy(resourceData.ThumbnailPath, newLocalThumbnailPath);

                    resourceData.ThumbnailPath = newLocalThumbnailPath;
                }
                else
                {
                    resourceData.ThumbnailPath = "";
                }
            }

            return resourceData;
        }
    }
}
