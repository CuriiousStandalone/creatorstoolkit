﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

using UnityEngine;

using strange.extensions.command.impl;

using PulseIQ.Local.Common;
using PulseIQ.Local.Service;
using PulseIQ.Local.Common.Model.Resource;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public class CommandSaveRemoteResourceData : EventCommand
    {
        [Inject]
        public IResourceModel ResourceModel { get; set; }

        private ContentAccess _content;

        public override void Execute()
        {
            _content = new ContentAccess(GlobalSettings.CMSURL);

            List<ResourceInfoData> listResources = _content.Resource.GetAllResourceData();

            if (ResourceModel.ResourceDataList == null)
            {
                ResourceModel.ResourceDataList = new List<SResourceData>();
            }

            //FIX FOR REMOVING REMOTE RESOURCES
            if (null == listResources)
            {
                Debug.Log("listResoource == null, skipping if statement");
            }
            else if (listResources.Count > 0)
            {
                SResourceData remoteResourceData;

                bool exist;

                foreach (var resourceJsonData in listResources)
                {
                    exist = false;

                    if(IsMedia((ItemTypeCode)Enum.Parse(typeof(ItemTypeCode), resourceJsonData.ItemType.ToString())))
                    {
                        if(ResourceModel.ResourceDataList.Any(x => x.MultimediaId == resourceJsonData.MultimediaID))
                        {
                            exist = true; 
                        }
                    }
                    else
                    {
                        if (ResourceModel.ResourceDataList.Any(x => x.ResourceId == resourceJsonData.GivenRID))
                        {
                            exist = true;
                        }
                    }

                    if(!exist)
                    {
                        remoteResourceData = new SResourceData
                        {
                            ResourceId = resourceJsonData.GivenRID,
                            ItemType = (ItemTypeCode)Enum.Parse(typeof(ItemTypeCode), resourceJsonData.ItemType.ToString()),
                            ResourceName = resourceJsonData.ResourceName,
                            MultimediaId = resourceJsonData.MultimediaID,
                            ResourceType = ListCardResourceTypes.REMOTE,
                            ThumbnailPath = IsMedia((ItemTypeCode)Enum.Parse(typeof(ItemTypeCode), resourceJsonData.ItemType.ToString())) ? Path.Combine(Settings.GetThumbnailPathByItemType((ItemTypeCode)Enum.Parse(typeof(ItemTypeCode), resourceJsonData.ItemType.ToString())), Path.GetFileNameWithoutExtension(resourceJsonData.MultimediaID) + ".png") : Path.Combine(Settings.GetThumbnailPathByItemType((ItemTypeCode)Enum.Parse(typeof(ItemTypeCode), resourceJsonData.ItemType.ToString())), resourceJsonData.GivenRID + ".png")
                        };

                        ResourceModel.ResourceDataList.Add(remoteResourceData);
                    }
                }
            }

            dispatcher.Dispatch(EResourceEvent.REMOTE_RESOURCE_DATA_FETCHED);
        }

        private bool IsMedia(ItemTypeCode itemType)
        {
            if (itemType == ItemTypeCode.PANORAMA || itemType == ItemTypeCode.PANORAMA_VIDEO || itemType == ItemTypeCode.PLANAR_VIDEO || itemType == ItemTypeCode.STEREO_PANORAMA_VIDEO || itemType == ItemTypeCode.AUDIO || itemType == ItemTypeCode.IMAGE)
            {
                return true;
            }

            return false;
        }
    }
}