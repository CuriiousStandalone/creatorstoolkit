﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public class CommandShowPanel : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EResourceEvent.SHOW_PANEL, evt.data);
        }
    }
}
