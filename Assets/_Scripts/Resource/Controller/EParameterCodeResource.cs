﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public enum EParameterCodeResource
    {
        STATUS,
        MESSAGE,

        RESOURCE_LIST,

        RESOURCE,

        RESOURCE_JSON_STRING,

        IMPORT_ITEM_TYPE,
        IMPORT_ITEM_PATH,
        IMPORT_ITEM_THUMBNAIL_PATH,

        MULTIMEDIA_ASSET,

        RESOURCE_TYPE,
        ITEM_TYPE,

        DOWNLOADED_FILE_NAME,

        TO_UPLOAD_FILE_PATH,

        VIRTUAL_DIRECTORY_JSON_STRING,
        VIRTUAL_DIRECTORY_JSON_DATA,

        IS_DIRECTORY,
        DIRECTORY_ID,

        TO_OVERRIDE,

        OLD_VERSION_REF,

        THUMBNAIL_DICTIONARY,
        CARD_DATA,
        RESOURCE_ID,
        HOTSPOT_ICONS,
        CURRENT_RESOURCE_LIST
    }
}
