using System;
using System.IO;
using System.Linq;

using strange.extensions.command.impl;

using PulseIQ.Local.ClientCommon.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public class CommandRemoveLocalResource : EventCommand
    {
        [Inject]
        public IResourceModel ResourceModel { get; set; }

        public override void Execute()
        {
            MessageData msgData = (MessageData)evt.data;
            string id = (string)msgData.Parameters[(byte)EParameterCodeResource.RESOURCE_ID];
            if (string.IsNullOrEmpty(id))
            {
                return;
            }

            Func<SResourceData, bool> predicate = dataItem => dataItem.Id.Equals(id);
            if (!ResourceModel.ResourceDataList.Any(predicate))
            {
                return;
            }

            SResourceData resource = ResourceModel.ResourceDataList.First(predicate);
            if (File.Exists(resource.LocalFilePath))
            {
                File.Delete(resource.LocalFilePath);
            }

            resource.LocalFilePath = string.Empty;
            resource.ResourceType = ListCardResourceTypes.REMOTE;
            MessageData outMsgData = new MessageData();
            outMsgData.Parameters.Add((byte)EParameterCodeResource.RESOURCE, resource);
            dispatcher.Dispatch(EResourceEvent.RESOURCE_UPDATED, outMsgData);
        }
    }
}