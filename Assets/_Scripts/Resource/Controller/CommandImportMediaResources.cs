﻿using System.IO;
using System.Collections.Generic;

using strange.extensions.command.impl;

using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public class CommandImportMediaResources : EventCommand
    {
        private List<string> _pathList;

        private ItemTypeCode _itemType;

        public override void Execute()
        {
            Retain();

            MessageData msgData = (MessageData)evt.data;

            _itemType = (ItemTypeCode)msgData[(byte)EParameterCodeToolkit.ITEM_TYPE];

            string[] paths = (string[])msgData[(byte)EParameterCodeToolkit.IMPORTED_ITEMS_PATH_LIST];

            _pathList = new List<string>(paths);

            ProcessMediaResources();
        }

        private async void ProcessMediaResources()
        {
            if(_pathList.Count <= 0)
            {
                dispatcher.Dispatch(EResourceEvent.RESOURCE_IMPORTED);
                return;
            }

            string path = _pathList[0];

            string fileNameWithExt = Path.GetFileName(path);
            string fileName = fileNameWithExt.Split('.')[0];
            string fileExt = fileNameWithExt.Split('.')[1];

            string destinationPath = Path.Combine(Settings.GetPathByItemType(_itemType), fileNameWithExt);

            int i = 1;

            string[] fileNum = fileName.Split('(');

            string actualFileName = fileNum[0];

            while (File.Exists(destinationPath))
            {
                fileName = actualFileName + "(" + i + ")";

                if(!Directory.Exists(destinationPath))
                {
                    Directory.CreateDirectory(destinationPath);
                }

                destinationPath = Path.Combine(Settings.GetPathByItemType(_itemType), fileName + "." + fileExt);

                i++;
            }

            if(!Directory.Exists(Settings.GetPathByItemType(_itemType)))
            {
                Directory.CreateDirectory(Settings.GetPathByItemType(_itemType));
            }

            if (_itemType == ItemTypeCode.PANORAMA || _itemType == ItemTypeCode.IMAGE)
            {
                if (fileExt == "png")
                {
                    File.Copy(path, destinationPath);
                }
                else
                {
                    destinationPath = Path.Combine(Settings.GetPathByItemType(_itemType), Path.GetFileNameWithoutExtension(path) + ".png");
                    await FFMpegUtility.ConvertImagetoPNG(path, destinationPath);
                }
            }
            else if(_itemType == ItemTypeCode.AUDIO)
            {
                if (fileExt == "wav")
                {
                    File.Copy(path, destinationPath);
                }
                else
                {
                    destinationPath = Path.Combine(Settings.GetPathByItemType(_itemType), Path.GetFileNameWithoutExtension(path) + ".wav");
                    await FFMpegUtility.ConvertAudioToMP4(path, destinationPath);
                }
            }
            else
            {
                File.Copy(path, destinationPath);
            }

            string thumbnailPath = Path.Combine(Settings.GetThumbnailPathByItemType(_itemType), fileName) + ".png";

            if (!Directory.Exists(Settings.GetThumbnailPathByItemType(_itemType)))
            {
                Directory.CreateDirectory(Settings.GetThumbnailPathByItemType(_itemType));
            }

            if (_itemType == ItemTypeCode.PANORAMA_VIDEO || _itemType == ItemTypeCode.STEREO_PANORAMA_VIDEO)
            {
                thumbnailPath = await FFMpegUtility.GenerateThumbnailOfVideo(path, thumbnailPath);
            }
            else if (_itemType == ItemTypeCode.PANORAMA || _itemType == ItemTypeCode.IMAGE)
            {
                thumbnailPath = await FFMpegUtility.GenerateThumbnailOfImage(path, thumbnailPath);
            }

            _pathList.RemoveAt(0);

            ProcessMediaResources();
        }
    }
}
