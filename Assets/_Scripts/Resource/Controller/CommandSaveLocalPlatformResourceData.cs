﻿using System.Linq;
using System.Collections.Generic;

using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.api;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public class CommandSaveLocalPlatformResourceData : EventCommand
    {
        [Inject]
        public IServiceLocalFileSystemCommunication Service { get; set; }

        [Inject]
        public IResourceModel ResourceModel { get; set; }

        private List<SResourceData> _resourceDataList;

        public override void Execute()
        {
            Retain();

            UpdateListeners(true);

            Service.FetchAllLocal(ListCardResourceTypes.PLATFORM);
        }

        private void UpdateListeners(bool val)
        {
            Service.dispatcher.UpdateListener(val, EResourceEvent.LOAD_LOCAL_RESOURCE_DATA_COMPLETED, LocalDataFetched);
            Service.dispatcher.UpdateListener(val, EResourceEvent.LOAD_LOCAL_RESOURCE_DATA_ERROR, LocalDataFetched);
        }

        private void LocalDataFetched(IEvent evt)
        {
            MessageData messageData = (MessageData)evt.data;

            if ((string)messageData.Parameters[(byte)EParameterCodeResource.STATUS] == "error")
            {
                return;
            }

            _resourceDataList = (List<SResourceData>)messageData.Parameters[(byte)EParameterCodeResource.RESOURCE_LIST];

            if (ResourceModel.ResourceDataList == null)
            {
                ResourceModel.ResourceDataList = new List<SResourceData>();
            }

            for (int i = ResourceModel.ResourceDataList.Count - 1; i >= 0; i--)
            {
                if (ResourceModel.ResourceDataList[i].ItemType == ItemTypeCode.PANORAMA ||
                    ResourceModel.ResourceDataList[i].ItemType == ItemTypeCode.PANORAMA_VIDEO ||
                    ResourceModel.ResourceDataList[i].ItemType == ItemTypeCode.STEREO_PANORAMA_VIDEO ||
                    ResourceModel.ResourceDataList[i].ItemType == ItemTypeCode.IMAGE ||
                    ResourceModel.ResourceDataList[i].ItemType == ItemTypeCode.AUDIO)

                {
                    ResourceModel.ResourceDataList.RemoveAt(i);
                }
            }

            foreach (var resource in _resourceDataList)
            {
                if((IsMedia(resource.ItemType) && ResourceModel.ResourceDataList.Any(x => x.MultimediaId == resource.MultimediaId)) ||
                    (!IsMedia(resource.ItemType) && ResourceModel.ResourceDataList.Any(x => x.ResourceId == resource.ResourceId)))
                {
                    continue;
                }
                
                ResourceModel.ResourceDataList.Add(resource);
            }

            dispatcher.Dispatch(EResourceEvent.LOCAL_RESOURCE_DATA_LOADED);

            UpdateListeners(false);

            Release();
        }

        private bool IsMedia(ItemTypeCode itemType)
        {
            if(itemType == ItemTypeCode.PANORAMA || itemType == ItemTypeCode.PANORAMA_VIDEO || itemType == ItemTypeCode.PLANAR_VIDEO || itemType == ItemTypeCode.STEREO_PANORAMA_VIDEO || itemType == ItemTypeCode.AUDIO || itemType == ItemTypeCode.IMAGE)
            {
                return true;
            }

            return false;
        }
    }
}