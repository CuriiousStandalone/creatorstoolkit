﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public class CommandTooltipResponse : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EResourceEvent.TOOLTIP_TEXT_RESPONSE, evt.data);
        }
    }
}
