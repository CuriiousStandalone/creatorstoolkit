﻿using System.IO;
using System.Collections.Generic;

using strange.extensions.command.impl;

using PulseIQ.Local.Common;
using PulseIQ.Local.Service;
using PulseIQ.Local.Common.Model.Thumbnail;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using UnityEngine;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    /// <summary>
    /// This command should be used to download thumbnails from CMS in future
    /// </summary>
    public class CommandFetchAllThumbnails : EventCommand
    {
        private ContentAccess _content;

        private List<ThumbnailInfoData> _thumbnailList;

        public override void Execute()
        {
            Retain();

            _content = new ContentAccess(GlobalSettings.CMSURL);

            _thumbnailList = _content.Resource.GetAllThumbnailData();

            DownloadThumbnail();
        }

        private async void DownloadThumbnail()
        {
            //FIX FOR REMOVING REMOTE RESOURCES
            if (null == _thumbnailList)
            {
                Debug.Log("_thumbnailList == null. Skipping download thumbnail");
                Release();
                return;
            }

            if (_thumbnailList.Count > 0)
            {
                ThumbnailInfoData thumbnailInfoData = _thumbnailList[0];

                _thumbnailList.RemoveAt(0);

                if(!Directory.Exists(Settings.GetThumbnailPathByItemType(thumbnailInfoData.ItemType)))
                {
                    Directory.CreateDirectory(Settings.GetThumbnailPathByItemType(thumbnailInfoData.ItemType));
                }

                string destinationPath = Path.Combine(Settings.GetThumbnailPathByItemType(thumbnailInfoData.ItemType), Path.GetFileName(thumbnailInfoData.Path));

                if (!File.Exists(destinationPath))
                {
                    await _content.Resource.DownloadThumbnailAsync(thumbnailInfoData.Path, destinationPath);
                }

                DownloadThumbnail();
            }
            else
            {
                dispatcher.Dispatch(EResourceEvent.ALL_THUMBNAILS_DOWNLOADED);

                Release();
            }
        }
    }
}
