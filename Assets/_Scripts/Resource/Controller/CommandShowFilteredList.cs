﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public class CommandShowFilteredList : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EResourceEvent.SHOW_FILTERED_LIST, evt.data);
        }
    }
}