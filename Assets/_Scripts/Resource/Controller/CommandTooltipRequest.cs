﻿using strange.extensions.command.impl;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public class CommandTooltipRequest : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.TOOLTIP_TEXT_REQUESTED, evt.data);
        }
    }
}
