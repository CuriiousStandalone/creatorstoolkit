﻿using strange.extensions.command.impl;

using PulseIQ.Local.ClientCommon.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public class CommandGetRemoteResources : EventCommand
    {
        [Inject]
        public IResourceModel resourceModel { get; set; }

        public override void Execute()
        {
            MessageData messageData = new MessageData();
            messageData.Parameters.Add((byte)EParameterCodeResource.RESOURCE_LIST, resourceModel.ResourceDataList);

            dispatcher.Dispatch(EResourceEvent.RESPONSE_REMOTE_RESOURCE_DATA, messageData);
        }
    }
}