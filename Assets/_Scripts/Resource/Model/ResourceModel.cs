﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public class ResourceModel : IResourceModel
    {
        public List<SResourceData> ResourceData;
        private Dictionary<string, Sprite> _resourceThumbnailDictionary;

        public List<SResourceData> ResourceDataList
        {
            get { return ResourceData; }

            set { ResourceData = value; }
        }

        public Dictionary<string, Sprite> ResourceThumbnailDictionary
        {
            get { return _resourceThumbnailDictionary; }
            set { _resourceThumbnailDictionary = value; }
        }

        public Sprite[] HotspotIcons { get; set; }
    }
}