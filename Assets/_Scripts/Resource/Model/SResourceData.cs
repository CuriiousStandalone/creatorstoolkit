﻿using PulseIQ.Local.Common;

using UnityEngine;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    /// <summary>
    /// Contains relevant resource information for local and remote resources.
    /// </summary>
    public struct SResourceData
    {
        public string ResourceId;
        public string ResourceName;
        public string LocalFilePath;
        public string ThumbnailPath;
        public ItemTypeCode ItemType;
        public ListCardResourceTypes ResourceType;
        public float LocalVersion;
        public float RemoteVersion;
        public string MultimediaId;

        public string Id
        {
            get
            {
                if (IsMedia)
                {
                    if (!string.IsNullOrEmpty(MultimediaId))
                    {
                        return MultimediaId;
                    }
                }
                if (!string.IsNullOrEmpty(ResourceId))
                {
                    return ResourceId;
                }
                if (!string.IsNullOrEmpty(ResourceName))
                {
                    return ResourceName;
                }
                return "";
            }
        }

        public bool IsMedia
        {
            get
            {
                return ItemType == ItemTypeCode.PANORAMA ||
                       ItemType == ItemTypeCode.PLANAR_VIDEO ||
                       ItemType == ItemTypeCode.PANORAMA_VIDEO ||
                       ItemType == ItemTypeCode.STEREO_PANORAMA_VIDEO ||
                       ItemType == ItemTypeCode.AUDIO ||
                       ItemType == ItemTypeCode.IMAGE;
            }
        }
    }
}