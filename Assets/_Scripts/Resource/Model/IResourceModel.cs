﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace CuriiousIQ.Local.CreatorsToolkit.Resource
{
    public interface IResourceModel
    {
        List<SResourceData> ResourceDataList { get; set; }

        Dictionary<string, Sprite> ResourceThumbnailDictionary { get; set; }

        Sprite[] HotspotIcons { get; set; }
    }
}