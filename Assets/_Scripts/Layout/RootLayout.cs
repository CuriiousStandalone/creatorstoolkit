﻿using strange.extensions.context.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class RootLayout : ContextView
    {
        private void Awake()
        {
            context = new ContextLayout(this);
        }
    }
}
