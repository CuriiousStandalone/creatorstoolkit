﻿using strange.extensions.context.api;
using strange.extensions.context.impl;

using UnityEngine;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class ContextLayout : MVCSContext
    {
        public ContextLayout(MonoBehaviour view) : base(view)
        {

        }

        public ContextLayout(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
        {

        }

        protected override void mapBindings()
        {
            #region Model Bindings

            injectionBinder.Bind<IUILayoutModel>().To<UILayoutModel>().ToSingleton();

            #endregion

            #region View-MediatorBindings

            mediationBinder.Bind<ViewButtonEnabler>().To<MediatorButtonEnabler>();
            mediationBinder.Bind<ViewLayoutManager>().To<MediatorLayoutManager>();
            mediationBinder.Bind<ViewBoundaryBarManager>().To<MediatorBoundaryBarManager>();
            mediationBinder.Bind<ViewLayoutTooltipManager>().To<MediatorLayoutTooltipManager>();

            #endregion

            #region Command Binding

            commandBinder.Bind(EEventToolkitCrossContext.PANEL_LOADED).To<CommandPanelLoaded>();
            commandBinder.Bind(EEventToolkitCrossContext.HIDE_PANEL).To<CommandHidePanel>();
            commandBinder.Bind(EEventToolkitCrossContext.SHOW_PANEL).To<CommandShowPanel>();
            commandBinder.Bind(EEventToolkitCrossContext.PANEL_HIDDEN).To<CommandPanelHidden>();
            commandBinder.Bind(EEventToolkitCrossContext.PANEL_SHOWN).To<CommandPanelShown>();
            commandBinder.Bind(EEventToolkitCrossContext.ENABLE_ADD_HOTSPOT_BUTTON).To<CommandEnableHotspotButton>();
            commandBinder.Bind(EEventToolkitCrossContext.DISABLE_ADD_HOTSPOT_BUTTON).To<CommandDisableHotspotButton>();
            commandBinder.Bind(EEventToolkitCrossContext.ENABLE_DELETE_ITEM_BUTTON).To<CommandEnableDeleteButton>();
            commandBinder.Bind(EEventToolkitCrossContext.DISABLE_DELETE_ITEM_BUTTON).To<CommandDisableDeleteButton>();
            commandBinder.Bind(EEventToolkitCrossContext.TOOLTIP_TEXT_RESPONSE).To<CommandTooltipResponse>();

            commandBinder.Bind(ELayoutEvent.LOAD_UI_LAYOUT_CONFIG).To<CommandLoadUILayoutConfig>();
            commandBinder.Bind(ELayoutEvent.PREVIEW_MODE_ENABLED).To<CommandPreviewModeEnabled>();
            commandBinder.Bind(ELayoutEvent.PREVIEW_MODE_DISABLED).To<CommandPreviewModeDisabled>();
            commandBinder.Bind(ELayoutEvent.TOOLTIP_TEXT_REQUESTED).To<CommandTooltipRequest>();

            #endregion
        }
    }
}
