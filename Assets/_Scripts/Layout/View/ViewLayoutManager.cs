﻿using UnityEngine;
using UnityEngine.UI;

using strange.extensions.mediation.impl;
using SFB;
using PulseIQ.Local.ClientCommon.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.Common;
using TMPro;
using System.Collections;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class ViewLayoutManager : EventView
    {
        private UILayoutData _uiLayoutData;

        [SerializeField]
        private Toggle _resourceManagerToggleButton;
        [SerializeField]
        private Toggle _worldHierarchyToggleButton;
        [SerializeField]
        private Toggle _mainMenuToggleButton;
        [SerializeField]
        private Toggle _previewModeToggleButton;

        [SerializeField]
        private Button _uploadPanoramaImageButton;
        [SerializeField]
        private Button _uploadPanoramaVideoButton;
        [SerializeField]
        private Button _uploadStereoPanoramaVideoButton;
        [SerializeField]
        private Button _uploadImageButton;
        [SerializeField]
        private Button _uploadAudioButton;
        [SerializeField]
        private Button _pubishButton;
        [SerializeField]
        private Button _saveButton;

        [SerializeField]
        private Button _deleteButton;
        [SerializeField]
        private Button _hotspotButton;

        [SerializeField]
        private TMP_Text _savingText;

        internal bool _resourcePanelPreviouslyActive = false;

        private bool _isResourceManagerToggleActive = false;
        private bool _isWorldHierarchyToggleActive = false;
        private bool _isMainMenuToggleActive = false;
        private bool _isPreviewModeToggleActive = false;

        internal enum LayoutManagerEvents
        {
            VIEW_LOADED,
            RESORUCE_MANAGER_SHOW_CLICKED,
            RESORUCE_MANAGER_HIDE_CLICKED,
            WORLD_HIERARCHY_SHOW_CLICKED,
            WORLD_HIERARCHY_HIDE_CLICKED,
            MAIN_MENU_SHOW_CLICKED,
            MAIN_MENU_HIDE_CLICKED,
            PREVIEW_MODE_ENABLE_CLICKED,
            PREVIEW_MODE_DISABLE_CLICKED,
            DELETE_RESOURCE_CLICKED,
            ADD_HOTSPOT_CLICKED,
            UPLOAD_PANORAMA_IMAGE_CLICKED,
            UPLOAD_PANORAMA_VIDEO_CLICKED,
            UPLOAD_STEREO_PANORAMA_VIDEO_CLICKED,
            UPLOAD_IMAGE_CLICKED,
            UPLOAD_AUDIO_CLICKED,
            RESET_CAMERA_CLICKED,
            PUBLISH_TO_CMS_CLICKED,
            SAVE_LOCALLY_CLICKED,
        }

        internal void Init()
        {
            dispatcher.Dispatch(LayoutManagerEvents.VIEW_LOADED);
        }

        public void SaveUILayoutData(UILayoutData uiLayoutData)
        {
            _uiLayoutData = uiLayoutData;
        }

        public void ToggleResourceManagerPanel_ClickHandler()
        {   
            if (_isResourceManagerToggleActive)
            {
                _resourcePanelPreviouslyActive = false;
                dispatcher.Dispatch(LayoutManagerEvents.RESORUCE_MANAGER_HIDE_CLICKED);
            }
            else
            {
                _resourcePanelPreviouslyActive = true;
                dispatcher.Dispatch(LayoutManagerEvents.RESORUCE_MANAGER_SHOW_CLICKED);
            }
        }

        public void ToggleWorldHierarhcyPanel_ClickHandler()
        {
            if (_isWorldHierarchyToggleActive)
            {
                dispatcher.Dispatch(LayoutManagerEvents.WORLD_HIERARCHY_HIDE_CLICKED);
            }
            else
            {
                dispatcher.Dispatch(LayoutManagerEvents.WORLD_HIERARCHY_SHOW_CLICKED);
            }
        }

        public void ToggleMainMenuPanel_ClickHandler()
        {
            if (_isMainMenuToggleActive)
            {
                dispatcher.Dispatch(LayoutManagerEvents.MAIN_MENU_HIDE_CLICKED);
            }
            else
            {
                dispatcher.Dispatch(LayoutManagerEvents.MAIN_MENU_SHOW_CLICKED);
            }
        }

        public void TogglePreviewMode_ClickHandler()
        {
            if(_isPreviewModeToggleActive)
            {
                EnableDisableButtons(true);
                dispatcher.Dispatch(LayoutManagerEvents.PREVIEW_MODE_DISABLE_CLICKED);
            }
            else
            {
                EnableDisableButtons(false);
                dispatcher.Dispatch(LayoutManagerEvents.PREVIEW_MODE_ENABLE_CLICKED);
            }

            _isPreviewModeToggleActive = !_isPreviewModeToggleActive;
        }

        private void EnableDisableButtons(bool flag)
        {
            _mainMenuToggleButton.enabled = flag;
            _mainMenuToggleButton.interactable = flag;

            _resourceManagerToggleButton.enabled = flag;
            _resourceManagerToggleButton.interactable = flag;

            _worldHierarchyToggleButton.enabled = flag;
            _worldHierarchyToggleButton.interactable = flag;

            _uploadPanoramaImageButton.enabled = flag;
            _uploadPanoramaImageButton.interactable = flag;

            _uploadPanoramaVideoButton.enabled = flag;
            _uploadPanoramaVideoButton.interactable = flag;

            _uploadStereoPanoramaVideoButton.enabled = flag;
            _uploadStereoPanoramaVideoButton.interactable = flag;

            _uploadImageButton.enabled = flag;
            _uploadImageButton.interactable = flag;

            _uploadAudioButton.enabled = flag;
            _uploadAudioButton.interactable = flag;

            _pubishButton.enabled = flag;
            _pubishButton.interactable = flag;

            _saveButton.enabled = flag;
            _saveButton.interactable = flag;
        }

        public void OnUpdatePanelHiddenButton(string panelId)
        {
            switch(panelId)
            {
                case "-1": //PreviewMode
                    {
                        _isPreviewModeToggleActive = false;
                        _previewModeToggleButton.SetIsOnWithoutNotify(false);
                        break;
                    }
                case "4": //ResourceManager
                    {
                        _isResourceManagerToggleActive = false;
                        _resourceManagerToggleButton.SetIsOnWithoutNotify(false);

                        break;
                    }
                case "5": //WorldHierarchy
                    {
                        _isWorldHierarchyToggleActive = false;
                        _worldHierarchyToggleButton.SetIsOnWithoutNotify(false);

                        break;
                    }
                case "7": //MainMenu
                    {
                        _isMainMenuToggleActive = false;
                        _mainMenuToggleButton.SetIsOnWithoutNotify(false);

                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        public void OnUpdatePanelShownButton(string panelId)
        {
            switch (panelId)
            {
                case "-1": //PreviewMode
                    {
                        _isPreviewModeToggleActive = true;
                        _previewModeToggleButton.SetIsOnWithoutNotify(true);
                        break;
                    }
                case "4": //ResourceManager
                    {
                        _isResourceManagerToggleActive = true;
                        _resourceManagerToggleButton.SetIsOnWithoutNotify(true);
                        break;
                    }
                case "5": //WorldHierarchy
                    {
                        _isWorldHierarchyToggleActive = true;
                        _worldHierarchyToggleButton.SetIsOnWithoutNotify(true);

                        break;
                    }
                case "7": //MainMenu
                    {
                        _isMainMenuToggleActive = true;
                        _mainMenuToggleButton.SetIsOnWithoutNotify(true);

                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        public void EnableDisableHotspotButton(bool value)
        {
            _hotspotButton.enabled = value;
            _hotspotButton.interactable = value;
        }

        public void EnableDisableDeleteButton(bool value)
        {
            _deleteButton.enabled = value;
            _deleteButton.interactable = value;
        }

        public void DeleteWorldItem_ClickedHandler()
        {
            dispatcher.Dispatch(LayoutManagerEvents.DELETE_RESOURCE_CLICKED);
        }

        public void AddHotspot_ClickedHandler()
        {
            dispatcher.Dispatch(LayoutManagerEvents.ADD_HOTSPOT_CLICKED);
        }

        public void UploadPanoramaImage_ClickHandler()
        {
            ExtensionFilter[] extensions = new[]
            {
                new ExtensionFilter("Panorama Images", "png", "jpeg", "jpg", "bmp", "tga", "tif", "tiff")
            };

            MessageData message = new MessageData();

            string[] selectedFiles = StandaloneFileBrowser.OpenFilePanel("Select Panorama Images", Settings.PanoramaPath, extensions, true);

            if (selectedFiles.Length > 0)
            {
                message.Parameters.Add((byte)EParameterCodeToolkit.ITEM_TYPE, ItemTypeCode.PANORAMA);
                message.Parameters.Add((byte)EParameterCodeToolkit.IMPORTED_ITEMS_PATH_LIST, selectedFiles);

                dispatcher.Dispatch(LayoutManagerEvents.UPLOAD_PANORAMA_IMAGE_CLICKED, message);
            }
        }

        public void UploadPanoramaVideo_ClickHandler()
        {
            ExtensionFilter[] extensions = new[]
            {
                new ExtensionFilter("Panorama Videos", "mp4")
            };

            MessageData message = new MessageData();

            string[] selectedFiles = StandaloneFileBrowser.OpenFilePanel("Select Panorama Videos", Settings.PanoramaVideoPath, extensions, true);

            if (selectedFiles.Length > 0)
            {
                message.Parameters.Add((byte)EParameterCodeToolkit.ITEM_TYPE, ItemTypeCode.PANORAMA_VIDEO);
                message.Parameters.Add((byte)EParameterCodeToolkit.IMPORTED_ITEMS_PATH_LIST, selectedFiles);

                dispatcher.Dispatch(LayoutManagerEvents.UPLOAD_PANORAMA_VIDEO_CLICKED, message);
            }
        }

        public void UploadStereoPanoramaVideo_ClickHandler()
        {
            ExtensionFilter[] extensions = new[]
            {
                new ExtensionFilter("Stereo Panorama Videos", "mp4")
            };

            MessageData message = new MessageData();

            string[] selectedFiles = StandaloneFileBrowser.OpenFilePanel("Select Stereo Panorama Videos", Settings.StereoPanoramaVideoPath, extensions, true);

            if (selectedFiles.Length > 0)
            {
                message.Parameters.Add((byte)EParameterCodeToolkit.ITEM_TYPE, ItemTypeCode.STEREO_PANORAMA_VIDEO);
                message.Parameters.Add((byte)EParameterCodeToolkit.IMPORTED_ITEMS_PATH_LIST, selectedFiles);

                dispatcher.Dispatch(LayoutManagerEvents.UPLOAD_STEREO_PANORAMA_VIDEO_CLICKED, message);
            }
        }

        public void UploadImage_ClickHandler()
        {
            ExtensionFilter[] extensions = new[]
            {
                new ExtensionFilter("Images", "png", "jpeg", "jpg", "bmp", "tga", "tif", "tiff")
            };

            MessageData message = new MessageData();

            string[] selectedFiles = StandaloneFileBrowser.OpenFilePanel("Select Images", Settings.ImagePath, extensions, true);

            if (selectedFiles.Length > 0)
            {
                message.Parameters.Add((byte)EParameterCodeToolkit.ITEM_TYPE, ItemTypeCode.IMAGE);
                message.Parameters.Add((byte)EParameterCodeToolkit.IMPORTED_ITEMS_PATH_LIST, selectedFiles);

                dispatcher.Dispatch(LayoutManagerEvents.UPLOAD_IMAGE_CLICKED, message);
            }
        }

        public void UploadAudioImage_ClickHandler()
        {
            ExtensionFilter[] extensions = new[]
            {
                new ExtensionFilter("Audio", new string[] { /*"oga", "mp3",*/ "wav", "aac", "aiff", "mp3", "m4a", "ogg" })
            };

            MessageData message = new MessageData();

            string[] selectedFiles = StandaloneFileBrowser.OpenFilePanel("Select Audio", Settings.AudioPath, extensions, true);

            if (selectedFiles.Length > 0)
            {
                message.Parameters.Add((byte)EParameterCodeToolkit.ITEM_TYPE, ItemTypeCode.AUDIO);
                message.Parameters.Add((byte)EParameterCodeToolkit.IMPORTED_ITEMS_PATH_LIST, selectedFiles);

                dispatcher.Dispatch(LayoutManagerEvents.UPLOAD_AUDIO_CLICKED, message);
            }
        }

        public void ResetCamera_ClickHandler()
        {
            dispatcher.Dispatch(LayoutManagerEvents.RESET_CAMERA_CLICKED);
        }

        public void PublishCMS_ClickHandler()
        {
            dispatcher.Dispatch(LayoutManagerEvents.PUBLISH_TO_CMS_CLICKED);
        }

        public void SaveLocally_ClickHandler()
        {
            dispatcher.Dispatch(LayoutManagerEvents.SAVE_LOCALLY_CLICKED);
        }

        public void OnSaveRequested()
        {
            _savingText.text = "Saving...";
            Invoke("OnSaveCompleted", 0.5f);
            Invoke("EmptyText", 1.5f);
        }

        public void OnSaveCompleted()
        {
            _savingText.text = "Saved!";
        }

        public void EmptyText()
        {
            _savingText.text = "";
        }
    }
}