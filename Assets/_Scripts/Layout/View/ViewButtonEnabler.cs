﻿using strange.extensions.mediation.impl;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class ViewButtonEnabler : EventView
    {
        [SerializeField]
        private List<Toggle> toggleList = new List<Toggle>();

        [SerializeField]
        private List<Button> buttonList = new List<Button>();

        internal void Init()
        {
            foreach (Toggle toggle in toggleList)
            {
                toggle.enabled = false;
            }

            foreach (Button button in buttonList)
            {
                button.enabled = false;
            }
        }

        public void EnableButtons()
        {
            foreach (Toggle toggle in toggleList)
            {
                toggle.enabled = true;
            }

            foreach (Button button in buttonList)
            {
                button.enabled = true;
            }
        }
    }
}