﻿using UnityEngine;

using strange.extensions.mediation.impl;

using PulseIQ.Local.ClientCommon.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common.Tooltip;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class ViewLayoutTooltipManager : EventView, ITooltipManager
    {
        internal enum TooltipEvents
        {
            VIEW_LOADED,
            GET_TOOLTIP_TEXT,
        }

        [SerializeField]
        private int _id;

        internal void Init()
        {
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.TOOLTIP_ID, _id);

            dispatcher.Dispatch(TooltipEvents.VIEW_LOADED, msgData);
        }

        internal void SetTooltipText(string text)
        {
            TooltipText = text;
        }

        public int ID
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }

        public string TooltipText { get; set; }
    }
}
