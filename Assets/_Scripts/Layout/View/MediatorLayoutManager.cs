﻿using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class MediatorLayoutManager : EventMediator
    {
        [Inject]
        public ViewLayoutManager view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.Init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewLayoutManager.LayoutManagerEvents.VIEW_LOADED, OnViewLoaded);

            view.dispatcher.UpdateListener(value, ViewLayoutManager.LayoutManagerEvents.RESORUCE_MANAGER_HIDE_CLICKED, OnResourceManagerHideClicked);
            view.dispatcher.UpdateListener(value, ViewLayoutManager.LayoutManagerEvents.RESORUCE_MANAGER_SHOW_CLICKED, OnResourceManagerShowClicked);
            view.dispatcher.UpdateListener(value, ViewLayoutManager.LayoutManagerEvents.WORLD_HIERARCHY_HIDE_CLICKED, OnWorldHierarchyHideClicked);
            view.dispatcher.UpdateListener(value, ViewLayoutManager.LayoutManagerEvents.WORLD_HIERARCHY_SHOW_CLICKED, OnWorldHierarchyShowClicked);
            view.dispatcher.UpdateListener(value, ViewLayoutManager.LayoutManagerEvents.MAIN_MENU_HIDE_CLICKED, OnMainMenuHideClicked);
            view.dispatcher.UpdateListener(value, ViewLayoutManager.LayoutManagerEvents.MAIN_MENU_SHOW_CLICKED, OnMainMenuShowClicked);
            view.dispatcher.UpdateListener(value, ViewLayoutManager.LayoutManagerEvents.PREVIEW_MODE_DISABLE_CLICKED, OnPreviewModeDisableClicked);
            view.dispatcher.UpdateListener(value, ViewLayoutManager.LayoutManagerEvents.PREVIEW_MODE_ENABLE_CLICKED, OnPreviewModeEnableClicked);

            view.dispatcher.UpdateListener(value, ViewLayoutManager.LayoutManagerEvents.DELETE_RESOURCE_CLICKED, OnDeleteResrouceClicked);
            view.dispatcher.UpdateListener(value, ViewLayoutManager.LayoutManagerEvents.ADD_HOTSPOT_CLICKED, OnAddHotspotClicked);
            view.dispatcher.UpdateListener(value, ViewLayoutManager.LayoutManagerEvents.UPLOAD_PANORAMA_IMAGE_CLICKED, OnUploadPanoramaImageClicked);
            view.dispatcher.UpdateListener(value, ViewLayoutManager.LayoutManagerEvents.UPLOAD_PANORAMA_VIDEO_CLICKED, OnUploadPanoramaVideoClicked);
            view.dispatcher.UpdateListener(value, ViewLayoutManager.LayoutManagerEvents.UPLOAD_STEREO_PANORAMA_VIDEO_CLICKED, OnUploadStereoPanoramaVideoClicked);
            view.dispatcher.UpdateListener(value, ViewLayoutManager.LayoutManagerEvents.UPLOAD_IMAGE_CLICKED, OnUploadImageClicked);
            view.dispatcher.UpdateListener(value, ViewLayoutManager.LayoutManagerEvents.UPLOAD_AUDIO_CLICKED, OnUploadAudioClicked);
            view.dispatcher.UpdateListener(value, ViewLayoutManager.LayoutManagerEvents.RESET_CAMERA_CLICKED, OnResetCameraClicked);
            view.dispatcher.UpdateListener(value, ViewLayoutManager.LayoutManagerEvents.PUBLISH_TO_CMS_CLICKED, OnPublishClicked);
            view.dispatcher.UpdateListener(value, ViewLayoutManager.LayoutManagerEvents.SAVE_LOCALLY_CLICKED, OnSaveLocallyClicked);

            dispatcher.UpdateListener(value, ELayoutEvent.UI_LAYOUT_CONFIG_LOADED, OnLayoutConfigLoaded);
            dispatcher.UpdateListener(value, ELayoutEvent.UPDATE_PANEL_HIDDEN_BUTTON, OnUpdatePanelHiddenButton);
            dispatcher.UpdateListener(value, ELayoutEvent.UPDATE_PANEL_SHOWN_BUTTON, OnUpdatePanelShownButton);
            dispatcher.UpdateListener(value, ELayoutEvent.ENABLE_ADD_HOTSPOT_BUTTON, OnEnableHotspotButton);
            dispatcher.UpdateListener(value, ELayoutEvent.DISABLE_ADD_HOTSPOT_BUTTON, OnDisableHotspotButton);
            dispatcher.UpdateListener(value, ELayoutEvent.ENABLE_DELETE_ITEM_BUTTON, OnEnableDeleteButton);
            dispatcher.UpdateListener(value, ELayoutEvent.DISABLE_DELETE_ITEM_BUTTON, OnDisableDeleteButton);
            dispatcher.UpdateListener(value, EEventToolkitCrossContext.SAVE_MODULE_REQUESTED, OnSaveRequested);
        }

        private void OnSaveRequested()
        {
            view.OnSaveRequested();
        }

        private void OnViewLoaded()
        {
            dispatcher.Dispatch(ELayoutEvent.LOAD_UI_LAYOUT_CONFIG);
        }

        private void OnLayoutConfigLoaded(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            UILayoutData uiLayoutData = (UILayoutData)msgData[(byte)EParameterCodeLayout.UI_LAYOUT_DATA];

            view.SaveUILayoutData(uiLayoutData);

            dispatcher.Dispatch(EEventToolkitCrossContext.UI_CONFIG_LOADED);
        }

        private void OnResourceManagerHideClicked()
        {
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "4");

            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, msgData);
        }

        private void OnResourceManagerShowClicked()
        {
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "4");

            dispatcher.Dispatch(EEventToolkitCrossContext.SHOW_PANEL, msgData);
        }

        private void OnWorldHierarchyHideClicked()
        {
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "5");

            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, msgData);
        }

        private void OnWorldHierarchyShowClicked()
        {
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "5");

            dispatcher.Dispatch(EEventToolkitCrossContext.SHOW_PANEL, msgData);
        }

        private void OnMainMenuHideClicked()
        {
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "7");

            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, msgData);

            msgData.Parameters.Clear();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "8");

            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, msgData);
        }

        private void OnMainMenuShowClicked()
        {
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "7");

            dispatcher.Dispatch(EEventToolkitCrossContext.SHOW_PANEL, msgData);
        }

        private void OnPreviewModeDisableClicked()
        {
            MessageData msgData = new MessageData();

            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "2");
            dispatcher.Dispatch(EEventToolkitCrossContext.SHOW_PANEL, msgData);

            MessageData msgData2 = new MessageData();

            msgData2.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "3");
            dispatcher.Dispatch(EEventToolkitCrossContext.SHOW_PANEL, msgData2);

            if (view._resourcePanelPreviouslyActive)
            {
                MessageData msgData3 = new MessageData();

                msgData3.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "4");
                dispatcher.Dispatch(EEventToolkitCrossContext.SHOW_PANEL, msgData3);
            }

            MessageData msgData4 = new MessageData();

            msgData4.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "5");
            dispatcher.Dispatch(EEventToolkitCrossContext.SHOW_PANEL, msgData4);

            MessageData msgData5 = new MessageData();

            msgData5.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "6");
            dispatcher.Dispatch(EEventToolkitCrossContext.SHOW_PANEL, msgData5);

            dispatcher.Dispatch(ELayoutEvent.PREVIEW_MODE_DISABLED);
        }

        private void OnPreviewModeEnableClicked()
        {
            MessageData msgData = new MessageData();

            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "2");
            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, msgData);

            MessageData msgData2 = new MessageData();

            msgData2.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "3");
            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, msgData2);

            MessageData msgData3 = new MessageData();

            msgData3.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "4");
            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, msgData3);

            MessageData msgData4 = new MessageData();

            msgData4.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "5");
            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, msgData4);

            MessageData msgData5 = new MessageData();

            msgData5.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "6");
            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, msgData5);

            MessageData msgData6 = new MessageData();

            msgData6.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "7");
            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, msgData6);

            MessageData msgData7 = new MessageData();

            msgData7.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "8");
            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, msgData7);

            dispatcher.Dispatch(ELayoutEvent.PREVIEW_MODE_ENABLED);
        }

        private void OnUpdatePanelHiddenButton(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            string panelId = (string)msgData[(byte)EParameterCodeToolkit.PANEL_ID];

            view.OnUpdatePanelHiddenButton(panelId);
        }

        private void OnUpdatePanelShownButton(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            string panelId = (string)msgData[(byte)EParameterCodeToolkit.PANEL_ID];

            view.OnUpdatePanelShownButton(panelId);
        }

        private void OnEnableDeleteButton()
        {
            view.EnableDisableDeleteButton(true);
        }

        private void OnDisableDeleteButton()
        {
            view.EnableDisableDeleteButton(false);
        }

        private void OnEnableHotspotButton()
        {
            view.EnableDisableHotspotButton(true);
        }

        private void OnDisableHotspotButton()
        {
            view.EnableDisableHotspotButton(false);
        }

        private void OnDeleteResrouceClicked()
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.DELETE_ITEM_REQUESTED);
        }

        private void OnAddHotspotClicked()
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.SHOW_HOTSPOT_OPTIONS);
        }

        //Call this event to show Upload Dialog - EEventToolkitCrossContextLAUNCH_BULK_RESOURCE_UPLOAD

        public void OnUploadPanoramaImageClicked(IEvent evt)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.IMPORT_MEDIA_RESOURCE, evt.data);
        }

        public void OnUploadPanoramaVideoClicked(IEvent evt)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.IMPORT_MEDIA_RESOURCE, evt.data);
        }

        public void OnUploadStereoPanoramaVideoClicked(IEvent evt)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.IMPORT_MEDIA_RESOURCE, evt.data);
        }

        public void OnUploadImageClicked(IEvent evt)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.IMPORT_MEDIA_RESOURCE, evt.data);
        }

        public void OnUploadAudioClicked(IEvent evt)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.IMPORT_MEDIA_RESOURCE, evt.data);
        }

        public void OnResetCameraClicked()
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.RESET_CAMERA);
        }

        public void OnPublishClicked()
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.PUBLISH_MODULE_REQUESTED);
        }

        public void OnSaveLocallyClicked()
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.SAVE_MODULE_REQUESTED);
        }
    }
}
