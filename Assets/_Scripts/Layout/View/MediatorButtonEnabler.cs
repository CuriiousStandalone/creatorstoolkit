﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using strange.extensions.mediation.impl;


namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class MediatorButtonEnabler : EventMediator
    {
        [Inject]
        public ViewButtonEnabler view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.Init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            dispatcher.UpdateListener(value, EEventToolkitCrossContext.OPEN_WORLD_REQUESTED, OnEnableButtons);
            dispatcher.UpdateListener(value, EEventToolkitCrossContext.NEW_WORLD_CREATED, OnEnableButtons);
        }

        private void OnEnableButtons()
        {
            view.EnableButtons();
        }
    }
}
