﻿using strange.extensions.mediation.impl;

using UnityEngine;

using PulseIQ.Local.ClientCommon.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIPanels;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class ViewBoundaryBarManager : EventView
    {
        internal enum BoundaryBarEvents
        {
            VIEW_LOADED,
            PANEL_SHOWN,
            PANEL_HIDDEN,
        }

        internal void Init()
        {
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelId);

            dispatcher.Dispatch(BoundaryBarEvents.VIEW_LOADED, msgData);
        }

        public void HideView()
        {
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelId);

            dispatcher.Dispatch(BoundaryBarEvents.PANEL_HIDDEN, msgData);

            gameObject.SetActive(false);
        }

        public void ShowView()
        {
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelId);

            dispatcher.Dispatch(BoundaryBarEvents.PANEL_SHOWN, msgData);

            gameObject.SetActive(true);
        }

        public string PanelId
        {
            get
            {
                return gameObject.GetComponent<CPanel>()._panelId;
            }
        }
    }
}
