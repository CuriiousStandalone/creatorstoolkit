﻿using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class MediatorBoundaryBarManager : EventMediator
    {
        [Inject]
        public ViewBoundaryBarManager view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.Init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewBoundaryBarManager.BoundaryBarEvents.VIEW_LOADED, OnViewLoaded);
            view.dispatcher.UpdateListener(value, ViewBoundaryBarManager.BoundaryBarEvents.PANEL_SHOWN, OnPanelShown);
            view.dispatcher.UpdateListener(value, ViewBoundaryBarManager.BoundaryBarEvents.PANEL_HIDDEN, OnPanelHidden);

            dispatcher.UpdateListener(value, ELayoutEvent.HIDE_PANEL, OnHidePanel);
            dispatcher.UpdateListener(value, ELayoutEvent.SHOW_PANEL, OnShowPanel);
        }

        private void OnViewLoaded(IEvent evt)
        {
            dispatcher.Dispatch(ELayoutEvent.PANEL_LOADED, evt.data);
        }

        private void OnPanelHidden(IEvent evt)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.PANEL_HIDDEN, evt.data);
        }

        private void OnPanelShown(IEvent evt)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.PANEL_SHOWN, evt.data);
        }

        private void OnHidePanel(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            string panelId = (string)msgData[(byte)EParameterCodeToolkit.PANEL_ID];

            if (view.PanelId == panelId)
            {
                view.HideView();
            }
        }

        private void OnShowPanel(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            string panelId = (string)msgData[(byte)EParameterCodeToolkit.PANEL_ID];

            if (view.PanelId == panelId)
            {
                view.ShowView();
            }
        }
    }
}
