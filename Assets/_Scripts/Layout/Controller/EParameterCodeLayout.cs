﻿namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public enum EParameterCodeLayout
    {
        UI_LAYOUT_DATA,

        SCREEN_WIDTH,
        SCREEN_HEIGHT,
        WIDTH_PERCENTAGE,
        HEIGHT_PERCENTAGE,
    }
}
