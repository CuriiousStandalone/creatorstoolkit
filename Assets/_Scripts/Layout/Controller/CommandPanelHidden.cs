﻿using System.Collections.Generic;

using strange.extensions.command.impl;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class CommandPanelHidden : EventCommand
    {
        [Inject]
        public IUILayoutModel layoutModel { get; set; }

        public override void Execute()
        {
            MessageData msgData = (MessageData)evt.data;

            string panelId = (string)msgData[(byte)EParameterCodeToolkit.PANEL_ID];

            UpdateActivePanelList(panelId);

            dispatcher.Dispatch(ELayoutEvent.UPDATE_PANEL_HIDDEN_BUTTON, msgData);

            PanelData panelData = layoutModel.LayoutData.PanelList.Find(x => x.PanelId == panelId);

            List<RulesData> rules = panelData.Rules.FindAll(x => x.Cause == EEffect.VISIBILITY);

            MessageData resultData = new MessageData();

            foreach (var ruleData in rules)
            {
                resultData.Parameters.Clear();

                string effectedPanelId = ruleData.PanelId;

                if (ruleData.Effect == EEffect.RESIZE)
                {
                    if(ruleData.Axis == EAxis.Y)
                    {
                        resultData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, effectedPanelId);
                        resultData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_HEIGHT, panelData.PanelDimension.Y);
                        resultData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ANCHOR, ruleData.Anchor);

                        dispatcher.Dispatch(EEventToolkitCrossContext.PANEL_INCREASE_HEIGHT, resultData);
                    }
                }
            }
        }

        private void UpdateActivePanelList(string panelId)
        {
            SPanelData sPanelData = layoutModel.ActivePanels.Find(x => x.panelId == panelId);

            List<SPanelData> listSPanelData = new List<SPanelData>(layoutModel.ActivePanels);
            listSPanelData.Remove(sPanelData);

            sPanelData.isCurrentlyActive = false;

            listSPanelData.Add(sPanelData);
        }
    }
}
