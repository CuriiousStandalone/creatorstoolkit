﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class CommandHidePanel : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(ELayoutEvent.HIDE_PANEL, evt.data);
        }
    }
}
