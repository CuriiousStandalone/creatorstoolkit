﻿using System.Linq;
using System.Collections.Generic;

using strange.extensions.command.impl;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class CommandPanelLoaded : EventCommand
    {
        [Inject]
        public IUILayoutModel LayoutModel { get; set; }

        public override void Execute()
        {
            MessageData msgData = (MessageData)evt.data;

            string panelId = (string)msgData[(byte)EParameterCodeToolkit.PANEL_ID];

            bool isActiveOnLaunch = false;

            if (LayoutModel.LayoutData.PanelList.Any(x => x.PanelId == panelId))
            {
                isActiveOnLaunch = LayoutModel.LayoutData.PanelList.Find(y => y.PanelId == panelId).IsActiveOnAppLaunch;
            }

            SPanelData sPanelData = new SPanelData
            {
                panelId = panelId,
                isCurrentlyActive = isActiveOnLaunch,
            };

            List<SPanelData> sPanelDataList = new List<SPanelData>(LayoutModel.ActivePanels);
            sPanelDataList.Add(sPanelData);
            LayoutModel.ActivePanels = sPanelDataList;

            if (CheckAllPanelsLoaded())
            {
                MessageData messageData = new MessageData();

                foreach (var item in LayoutModel.ActivePanels)
                {
                    messageData.Parameters.Clear();
                    messageData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, item.panelId);

                    if (item.isCurrentlyActive)
                    {
                        dispatcher.Dispatch(EEventToolkitCrossContext.SHOW_PANEL, messageData);
                    }
                    else
                    {
                        dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, messageData);
                    }
                }
            }
        }

        private bool CheckAllPanelsLoaded()
        {
            if (LayoutModel.ActivePanels.Count < LayoutModel.LayoutData.PanelList.Count)
            {
                return false;
            }

            return true;
        }
    }
}
