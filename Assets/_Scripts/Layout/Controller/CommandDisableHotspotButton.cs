﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class CommandDisableHotspotButton : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(ELayoutEvent.DISABLE_ADD_HOTSPOT_BUTTON);
        }
    }
}
