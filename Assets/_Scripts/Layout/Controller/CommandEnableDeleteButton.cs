﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class CommandEnableDeleteButton : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(ELayoutEvent.ENABLE_DELETE_ITEM_BUTTON);
        }
    }
}
