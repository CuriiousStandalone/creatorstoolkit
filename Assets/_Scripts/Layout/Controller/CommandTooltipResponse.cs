﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class CommandTooltipResponse : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(ELayoutEvent.TOOLTIP_TEXT_RESPONSE, evt.data);
        }
    }
}
