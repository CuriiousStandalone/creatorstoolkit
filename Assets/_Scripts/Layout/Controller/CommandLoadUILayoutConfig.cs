﻿using System.Collections.Generic;

using strange.extensions.command.impl;

using PulseIQ.Local.ClientCommon.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class CommandLoadUILayoutConfig : EventCommand
    {
        [Inject]
        public IUILayoutModel LayoutModel { get; set; }

        public override void Execute()
        {
            UILayoutDataXML layoutDataXml = UILayoutDataSerializer.ReadData();

            LayoutModel.LayoutData = new UILayoutData(layoutDataXml);

            InitBoundaryBars();

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeLayout.UI_LAYOUT_DATA, LayoutModel.LayoutData);

            dispatcher.Dispatch(ELayoutEvent.UI_LAYOUT_CONFIG_LOADED, msgData);
        }

        private void InitBoundaryBars()
        {
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "1");

            dispatcher.Dispatch(EEventToolkitCrossContext.PANEL_LOADED, msgData);

            msgData.Parameters.Clear();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "2");

            dispatcher.Dispatch(EEventToolkitCrossContext.PANEL_LOADED, msgData);

            msgData.Parameters.Clear();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "3");

            dispatcher.Dispatch(EEventToolkitCrossContext.PANEL_LOADED, msgData);

            msgData.Parameters.Clear();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "7");

            dispatcher.Dispatch(EEventToolkitCrossContext.PANEL_LOADED, msgData);
        }
    }
}
