﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class CommandDisableDeleteButton : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(ELayoutEvent.DISABLE_DELETE_ITEM_BUTTON);
        }
    }
}
