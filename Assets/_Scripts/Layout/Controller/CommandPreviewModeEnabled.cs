﻿using strange.extensions.command.impl;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class CommandPreviewModeEnabled : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.PREVIEW_MODE_ENABLED);
        }
    }
}
