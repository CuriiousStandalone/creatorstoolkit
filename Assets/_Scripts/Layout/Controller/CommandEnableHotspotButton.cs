﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class CommandEnableHotspotButton : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(ELayoutEvent.ENABLE_ADD_HOTSPOT_BUTTON);
        }
    }
}
