﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class CommandShowPanel : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(ELayoutEvent.SHOW_PANEL, evt.data);
        }
    }
}
