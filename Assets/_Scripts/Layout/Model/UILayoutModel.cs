﻿using System.Collections.Generic;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class UILayoutModel : IUILayoutModel
    {
        private UILayoutData _layoutData;
        private List<SPanelData> _activePanels;

        public UILayoutData LayoutData
        {
            get
            {
                return _layoutData;
            }

            set
            {
                _layoutData = value;
            }
        }

        public List<SPanelData> ActivePanels
        {
            get
            {
                if(_activePanels == null)
                {
                    _activePanels = new List<SPanelData>();
                }

                return _activePanels;
            }

            set
            {
                _activePanels = value;
            }
        }
    }
}
