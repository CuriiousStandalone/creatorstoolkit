﻿using System.Xml.Serialization;

using PulseIQ.Local.Common.CustomType;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class Dimension
    {
        [XmlElement("Width")]
        public float Width;

        [XmlElement("Height")]
        public float Height;

        public Dimension() { }

        public Dimension(CTVector2 vector2)
        {
            Width = vector2.X;
            Height = vector2.Y;
        }

        public CTVector2 ToCTVector2() => new CTVector2
        {
            X = Width,
            Y = Height
        };
    }
}
