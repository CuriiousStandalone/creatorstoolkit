﻿using System.Xml.Serialization;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class RulesDataXML
    {
        [XmlElement("PanelId")]
        public string PanelId;

        [XmlElement("Anchor")]
        public EAnchor Anchor;

        [XmlElement("Cause")]
        public EEffect Cause;

        [XmlElement("Effect")]
        public EEffect Effect;

        [XmlElement("Axis")]
        public EAxis Axis;
    }
}
