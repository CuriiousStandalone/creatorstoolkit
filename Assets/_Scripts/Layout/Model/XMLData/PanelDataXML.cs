﻿using System.Collections.Generic;
using System.Xml.Serialization;

using PulseIQ.Local.Common.Model.Xml;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class PanelDataXML
    {
        [XmlElement("PanelId")]
        public string PanelId;

        [XmlElement("IsActiveOnAppLaunch")]
        public bool IsActiveOnAppLaunch;

        [XmlElement("PanelPosition")]
        public Position PanelPosition;

        [XmlElement("PanelDimension")]
        public Dimension PanelDimension;

        [XmlArray("Rules")]
        [XmlArrayItem("RuleData", typeof(RulesDataXML))]
        public List<RulesDataXML> Rules;
    }
}
