﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

using UnityEngine;

using PulseIQ.Local.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public static class UILayoutDataSerializer
    {
        public static XmlSerializer XmlSerializer = new XmlSerializer(typeof(UILayoutDataXML));

        public static UILayoutDataXML ReadData()
        {
            TextAsset textAsset = (TextAsset)Resources.Load("UILayoutConfig", typeof(TextAsset));

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(textAsset.text);

            UILayoutDataXML uiLayoutDataXML;

            using (XmlReader reader = new XmlNodeReader(xmlDoc))
            {
                uiLayoutDataXML = (UILayoutDataXML)XmlSerializer.Deserialize(reader);
            }

            return uiLayoutDataXML;
        }

        public static void WriteData(UILayoutData uiLayoutData)
        {
            #region Seeding Data
            //UILayoutData uiLayoutData = new UILayoutData();
            //uiLayoutData.PanelList = new List<PanelData>();

            //PanelData panelData = new PanelData
            //{
            //    PanelId = "1",
            //    IsScreenAffected = true,
            //    Effect = ELayoutEffect.SIZE,
            //    Axis = ELayoutAxis.X,
            //    PanelPosition = new CTVector3(0.0f, 0.0f, 0.0f),
            //    PanelDimension = new CTVector2(0.0f, 0.0f)
            //};
            //panelData.AffectedPanelsList = new List<AffectedPanelData>();

            //AffectedPanelData affectedPanel = new AffectedPanelData
            //{
            //    PanelId = "2",
            //    Anchor = ELayoutAnchor.BOTTOM,
            //    Effect = ELayoutEffect.SIZE,
            //    Axis = ELayoutAxis.Y,
            //};
            //panelData.AffectedPanelsList.Add(affectedPanel);
            //affectedPanel = new AffectedPanelData
            //{
            //    PanelId = "3",
            //    Anchor = ELayoutAnchor.BOTTOM,
            //    Effect = ELayoutEffect.SIZE,
            //    Axis = ELayoutAxis.Y,
            //};
            //panelData.AffectedPanelsList.Add(affectedPanel);
            //affectedPanel = new AffectedPanelData
            //{
            //    PanelId = "5",
            //    Anchor = ELayoutAnchor.BOTTOM,
            //    Effect = ELayoutEffect.SIZE,
            //    Axis = ELayoutAxis.Y,
            //};
            //panelData.AffectedPanelsList.Add(affectedPanel);
            //affectedPanel = new AffectedPanelData
            //{
            //    PanelId = "6",
            //    Anchor = ELayoutAnchor.BOTTOM,
            //    Effect = ELayoutEffect.SIZE,
            //    Axis = ELayoutAxis.Y,
            //};
            //panelData.AffectedPanelsList.Add(affectedPanel);

            //uiLayoutData.PanelList.Add(panelData);

            //panelData = new PanelData
            //{
            //    PanelId = "2",
            //    IsScreenAffected = true,
            //    Effect = ELayoutEffect.SIZE,
            //    Axis = ELayoutAxis.Y,
            //    PanelPosition = new CTVector3(0.0f, 0.0f, 0.0f),
            //    PanelDimension = new CTVector2(0.0f, 0.0f)
            //};
            //panelData.AffectedPanelsList = new List<AffectedPanelData>();

            //affectedPanel = new AffectedPanelData
            //{
            //    PanelId = "4",
            //    Anchor = ELayoutAnchor.LEFT,
            //    Effect = ELayoutEffect.SIZE,
            //    Axis = ELayoutAxis.X,
            //};
            //panelData.AffectedPanelsList.Add(affectedPanel);
            //affectedPanel = new AffectedPanelData
            //{
            //    PanelId = "6",
            //    Anchor = ELayoutAnchor.LEFT,
            //    Effect = ELayoutEffect.POSITION,
            //    Axis = ELayoutAxis.X,
            //};
            //panelData.AffectedPanelsList.Add(affectedPanel);

            //uiLayoutData.PanelList.Add(panelData);

            //panelData = new PanelData
            //{
            //    PanelId = "3",
            //    IsScreenAffected = true,
            //    Effect = ELayoutEffect.SIZE,
            //    Axis = ELayoutAxis.Y,
            //    PanelPosition = new CTVector3(0.0f, 0.0f, 0.0f),
            //    PanelDimension = new CTVector2(0.0f, 0.0f)
            //};
            //panelData.AffectedPanelsList = new List<AffectedPanelData>();

            //affectedPanel = new AffectedPanelData
            //{
            //    PanelId = "4",
            //    Anchor = ELayoutAnchor.RIGHT,
            //    Effect = ELayoutEffect.SIZE,
            //    Axis = ELayoutAxis.X,
            //};
            //panelData.AffectedPanelsList.Add(affectedPanel);
            //affectedPanel = new AffectedPanelData
            //{
            //    PanelId = "5",
            //    Anchor = ELayoutAnchor.RIGHT,
            //    Effect = ELayoutEffect.POSITION,
            //    Axis = ELayoutAxis.X,
            //};
            //panelData.AffectedPanelsList.Add(affectedPanel);

            //uiLayoutData.PanelList.Add(panelData);

            //panelData = new PanelData
            //{
            //    PanelId = "4",
            //    IsScreenAffected = true,
            //    Effect = ELayoutEffect.SIZE,
            //    Axis = ELayoutAxis.X,
            //    PanelPosition = new CTVector3(0.0f, 0.0f, 0.0f),
            //    PanelDimension = new CTVector2(0.0f, 0.0f)
            //};
            //panelData.AffectedPanelsList = new List<AffectedPanelData>();

            //affectedPanel = new AffectedPanelData
            //{
            //    PanelId = "5",
            //    Anchor = ELayoutAnchor.TOP,
            //    Effect = ELayoutEffect.SIZE,
            //    Axis = ELayoutAxis.Y,
            //};
            //panelData.AffectedPanelsList.Add(affectedPanel);
            //affectedPanel = new AffectedPanelData
            //{
            //    PanelId = "6",
            //    Anchor = ELayoutAnchor.TOP,
            //    Effect = ELayoutEffect.SIZE,
            //    Axis = ELayoutAxis.Y,
            //};
            //panelData.AffectedPanelsList.Add(affectedPanel);

            //uiLayoutData.PanelList.Add(panelData);

            //panelData = new PanelData
            //{
            //    PanelId = "5",
            //    IsScreenAffected = true,
            //    Effect = ELayoutEffect.SIZE,
            //    Axis = ELayoutAxis.Y,
            //    PanelPosition = new CTVector3(0.0f, 0.0f, 0.0f),
            //    PanelDimension = new CTVector2(0.0f, 0.0f)
            //};
            //panelData.AffectedPanelsList = new List<AffectedPanelData>();

            //uiLayoutData.PanelList.Add(panelData);

            //panelData = new PanelData
            //{
            //    PanelId = "6",
            //    IsScreenAffected = true,
            //    Effect = ELayoutEffect.SIZE,
            //    Axis = ELayoutAxis.Y,
            //    PanelPosition = new CTVector3(0.0f, 0.0f, 0.0f),
            //    PanelDimension = new CTVector2(0.0f, 0.0f)
            //};
            //panelData.AffectedPanelsList = new List<AffectedPanelData>();

            //uiLayoutData.PanelList.Add(panelData);
            #endregion

            UILayoutDataXML uILayoutDataXML = uiLayoutData.ToXML();

            string fulPath = Path.Combine(Application.dataPath, "Resources/UILayoutConfig.xml");

            using (StringWriterUtf8 textWriter = new StringWriterUtf8())
            {
                XmlSerializer.Serialize(textWriter, uILayoutDataXML);
                File.WriteAllText(fulPath, string.Empty);
                File.WriteAllText(fulPath, textWriter.ToString());
            }
        }
    }
}
