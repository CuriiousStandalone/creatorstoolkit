﻿using System.Collections.Generic;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class UILayoutData
    {
        public List<PanelData> PanelList { get; set; }

        public UILayoutDataXML ToXML()
        {
            UILayoutDataXML layoutDataXml = new UILayoutDataXML
            {
                PanelList = new List<PanelDataXML>()
            };

            foreach (PanelData panelData in PanelList)
            {
                layoutDataXml.PanelList.Add(panelData.ToXML());
            }

            return layoutDataXml;
        }

        public UILayoutData()
        {
            PanelList = new List<PanelData>();
        }

        public UILayoutData(UILayoutDataXML uiLayoutDataXml)
        {
            PanelList = new List<PanelData>();

            foreach (PanelDataXML panelDataXml in uiLayoutDataXml.PanelList)
            {
                PanelData panelData = new PanelData(panelDataXml);
                PanelList.Add(panelData);
            }
        }
    }
}
