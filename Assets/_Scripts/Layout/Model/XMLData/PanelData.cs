﻿using System.Collections.Generic;

using PulseIQ.Local.Common.CustomType;
using PulseIQ.Local.Common.Model.Xml;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class PanelData
    {
        public string PanelId { get; set; }

        public bool IsActiveOnAppLaunch { get; set; }

        public CTVector3 PanelPosition { get; set; }

        public CTVector2 PanelDimension { get; set; }

        protected List<RulesData> _rules;

        public List<RulesData> Rules
        {
            get
            {
                return _rules;
            }
            set
            {
                if (null == _rules)
                {
                    _rules = new List<RulesData>();
                }
                else
                {
                    _rules.Clear();
                }

                _rules.AddRange(value);
            }
        }

        public PanelData()
        {
            _rules = new List<RulesData>();
        }

        public PanelData(PanelDataXML panelDataXml)
        {
            PanelId = panelDataXml.PanelId;
            IsActiveOnAppLaunch = panelDataXml.IsActiveOnAppLaunch;

            PanelPosition = panelDataXml.PanelPosition.ToCTVector3();
            PanelDimension = panelDataXml.PanelDimension.ToCTVector2();

            _rules = new List<RulesData>();

            foreach (RulesDataXML ruleDataXML in panelDataXml.Rules)
                _rules.Add(new RulesData(ruleDataXML));
        }

        public PanelDataXML ToXML()
        {
            var data = new PanelDataXML();
            FillXmlData(data);
            return data;
        }

        protected void FillXmlData(PanelDataXML data)
        {
            data.PanelId = PanelId;
            data.IsActiveOnAppLaunch = IsActiveOnAppLaunch;

            data.PanelPosition = new Position
            {
                PositionX = PanelPosition.X,
                PositionY = PanelPosition.Y,
                PositionZ = PanelPosition.Z
            };

            data.PanelDimension = new Dimension
            {
                Width = PanelDimension.X,
                Height = PanelDimension.Y
            };

            var rulesDataXMLs = new List<RulesDataXML>(Rules.Count);

            foreach (RulesData ruleData in Rules)
            {
                rulesDataXMLs.Add(ruleData.ToXML());
            }

            data.Rules = rulesDataXMLs;
        }
    }
}
