﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    [XmlRoot("UILayoutData")]
    public class UILayoutDataXML
    {
        [XmlArray("PanelList")]
        [XmlArrayItem("PanelData", typeof(PanelDataXML))]
        public List<PanelDataXML> PanelList { get; set; }
    }
}
