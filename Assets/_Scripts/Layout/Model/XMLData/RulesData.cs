﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public class RulesData
    {
        public string PanelId { get; set; }

        public EAnchor Anchor { get; set; }

        public EEffect Cause { get; set; }

        public EEffect Effect { get; set; }

        public EAxis Axis { get; set; }

        public RulesData()
        {

        }

        public RulesData(RulesDataXML rulesDataXML)
        {
            PanelId = rulesDataXML.PanelId;
            Anchor = rulesDataXML.Anchor;
            Cause = rulesDataXML.Cause;
            Effect = rulesDataXML.Effect;
            Axis = rulesDataXML.Axis;
        }

        public RulesData(RulesData rulesData)
        {
            PanelId = rulesData.PanelId;
            Anchor = rulesData.Anchor;
            Cause = rulesData.Cause;
            Effect = rulesData.Effect;
            Axis = rulesData.Axis;
        }

        public RulesDataXML ToXML()
        {
            RulesDataXML rulesDataXML = new RulesDataXML
            {
                PanelId = PanelId,
                Anchor = Anchor,
                Cause = Cause,
                Effect = Effect,
                Axis = Axis,
            };

            return rulesDataXML;
        }
    }
}
