﻿using System.Collections.Generic;

namespace CuriiousIQ.Local.CreatorsToolkit.Layout
{
    public interface IUILayoutModel
    {
        UILayoutData LayoutData { get; set; }

        List<SPanelData> ActivePanels { get; set; }
    }
}
