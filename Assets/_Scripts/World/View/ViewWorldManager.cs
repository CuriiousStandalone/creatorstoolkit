﻿using System.Linq;
using System.Collections.Generic;

using UnityEngine;

using strange.extensions.mediation.impl;

using PulseIQ.Local.Common;
using PulseIQ.Local.Common.Model.World;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.Common.Model.Panorama;
using PulseIQ.Local.Common.Model.PanoramaVideo;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;
using PulseIQ.Local.Common.CustomType;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.Multimedia;

using CuriiousIQ.Local.CreatorsToolkit.World.Static;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class ViewWorldManager : EventView
    {
        private GameObject _itemsParent;
        private GameObject _currentMediaPlayer;

        [SerializeField]
        private Camera _mainCamera;

        private Vector3 _mainCameraInitPosition;
        private Quaternion _mainCameraInitRotation;

        private WorldData _currentWorldData;
        private ViewStaticItem _selectedItem;
        private ViewStaticItem _lastActiveItem; // last active item while in preview mode - to get the correct item for end behaviour stuff
        private ViewStaticHotspot _selectedHotspot;
        private ViewStaticItem _lastSelectedItem; // last selected item before going to preview mode so that we can come back to it

        private Dictionary<int, ViewStaticItem> _staticItems;

        private List<ViewStaticHotspot> _currentItemHotspots;

        private bool _isModuleSelected, _isItemSelected, _isHotspotSelected, _previewModeEnabled = false;

        private float _hotspotDrawDistance = 0f;

        internal enum WorldManagerEvents
        {
            VIEW_LOADED,
            ITEM_CREATED,
            ITEM_DELETED,
            HOTSPOT_CREATED,
            HOTSPOT_DELETED,
        }

        internal void Init()
        {
            _mainCameraInitPosition = new Vector3(_mainCamera.transform.position.x, _mainCamera.transform.position.y, _mainCamera.transform.position.z);
            _mainCameraInitRotation = new Quaternion(_mainCamera.transform.rotation.x, _mainCamera.transform.rotation.y, _mainCamera.transform.rotation.z, _mainCamera.transform.rotation.w);

            _itemsParent = new GameObject("ItemsParent");
            _itemsParent.transform.parent = transform;
            _itemsParent.transform.position = Vector3.zero;

            _currentItemHotspots = new List<ViewStaticHotspot>();
            _staticItems = new Dictionary<int, ViewStaticItem>();

            dispatcher.Dispatch(WorldManagerEvents.VIEW_LOADED);

            
        }

        internal void DestroyCurrentWorldItems()
        {
            _currentWorldData = null;
            _selectedItem = null;
            _lastSelectedItem = null;

            Transform trans = _itemsParent.transform;

            int size = _currentItemHotspots.Count;

            for(int i = 0; i < size; i++)
            {
                Destroy(_currentItemHotspots[0].gameObject);
            }

            _currentItemHotspots.Clear();

            while (trans.childCount != 0)
            {
                Transform child = trans.GetChild(0);

                child.parent = null;
                Destroy(child.gameObject);
            }

            Destroy(_currentMediaPlayer);

            _isModuleSelected = false;
            _isItemSelected = false;
            _isHotspotSelected = false;

            _staticItems.Clear();
        }

        internal void WorldCreated(WorldData worldData)
        {
            _currentWorldData = worldData;

            foreach (CTItemData item in worldData.ListItems)
            {
                if (IsMediaItem(item.ItemType))
                {
                    CreateMediaItem(item, true);
                }
            }
        }

        internal void ModuleSelected()
        {
            _isModuleSelected = true;
            _isItemSelected = false;
            _isHotspotSelected = false;

            int size = _currentItemHotspots.Count;

            for (int i = 0; i < size; i++)
            {
                if (null != _currentItemHotspots && null != _currentItemHotspots[0].gameObject)
                {
                    Destroy(_currentItemHotspots[0].gameObject);
                }
            }

            _currentItemHotspots.Clear();

            _selectedItem = null;
            _selectedHotspot = null;
            _lastSelectedItem = null;

            Destroy(_currentMediaPlayer);
        }

        public void ItemDroppedIntoWorld(CTItemData itemData, bool isTeleportItem = false)
        {
            if (IsMediaItem(itemData.ItemType))
            {
                CreateMediaItem(itemData, isTeleportItem);
            }
        }

        private void CreateMediaItem(CTItemData data, bool isTeleportItem = false)
        {
            Vector3 position = new Vector3(data.Position.X, data.Position.Y, data.Position.Z);
            Quaternion rotation = new Quaternion(data.Rotation.X, data.Rotation.Y, data.Rotation.Z, data.Rotation.W);
            Vector3 scale = new Vector3(data.Scale.X, data.Scale.Y, data.Scale.Z);

            GameObject objSpawnedItem;

            objSpawnedItem = new GameObject();

            MediaPreview mediaPreview = objSpawnedItem.AddComponent<MediaPreview>();
            mediaPreview.SetLookAtTranform(_mainCamera.transform);

            objSpawnedItem.transform.position = position;
            objSpawnedItem.transform.rotation = rotation;
            objSpawnedItem.transform.localScale = scale;
            objSpawnedItem.transform.parent = _itemsParent.transform;
            objSpawnedItem.name = data.ItemResourceId;

            ViewStaticItem view = objSpawnedItem.AddComponent<ViewStaticItem>();
            view.ItemIndex = data.ItemIndex;
            view.SetItemResourceID(data.ItemResourceId);
            view.SetItemName(data.ItemName);
            view.SetItemData(data);
            view.SetItemPosition(new CTVector3(objSpawnedItem.transform.position.x, objSpawnedItem.transform.position.y, objSpawnedItem.transform.position.z));
            view.SetItemRotation(new CTQuaternion(objSpawnedItem.transform.rotation.x, objSpawnedItem.transform.rotation.y, objSpawnedItem.transform.rotation.z, objSpawnedItem.transform.rotation.w));
            view.SetItemScale(new CTVector3(objSpawnedItem.transform.localScale.x, objSpawnedItem.transform.localScale.y, objSpawnedItem.transform.localScale.z));

            _staticItems.Add(data.ItemIndex, view);

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeWorld.ITEM_DATA, data);
            msgData.Parameters.Add((byte)EParameterCodeWorld.IS_TELEPORT_ITEM, isTeleportItem);

            dispatcher.Dispatch(WorldManagerEvents.ITEM_CREATED, msgData);
        }

        public void ItemSelected(CTItemData itemData)
        {
            bool isCurrentItem = false;

            _isModuleSelected = false;
            _isItemSelected = true;
            _isHotspotSelected = false;

            foreach (var hotspotItem in _currentItemHotspots)
            {
                hotspotItem.ShowHideHotspotBoxes(false);
                hotspotItem.SetFirstSelection(true);
            }

            if (null != _selectedItem)
            {
                isCurrentItem = _selectedItem.ItemIndex == itemData.ItemIndex ? true : false;
            }

            if (isCurrentItem)
            {
                return;
            }

            ViewStaticItem view = _staticItems.FirstOrDefault(x => x.Key == itemData.ItemIndex).Value;

            _currentItemHotspots = new List<ViewStaticHotspot>();

            if (null != view)
            {
                _selectedItem = view;
                _lastSelectedItem = view;
            }
            else
            {
                return;
            }

            Destroy(_currentMediaPlayer);

            if (IsMediaItem(itemData.ItemType))
            {
                switch (itemData.ItemType)
                {
                    case ItemTypeCode.PANORAMA:
                        {
                            _currentMediaPlayer = view.LoadPanoramaImage(_mainCamera, transform);
                            foreach(CTHotspotData hotspot in ((CTPanoramaData)itemData).ListHotspot)
                            {
                                CreateHotspot(itemData, hotspot);
                            }
                            break;
                        }

                    case ItemTypeCode.PANORAMA_VIDEO:
                        {
                            _currentMediaPlayer = view.LoadPanoramaVideo(_mainCamera, transform);
                            foreach (CTHotspotData hotspot in ((CTPanoramaVideoData)itemData).ListHotspot)
                            {
                                CreateHotspot(itemData, hotspot);
                            }
                            break;
                        }

                    case ItemTypeCode.PLANAR_VIDEO:
                        {
                            break;
                        }

                    case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                        {
                            _currentMediaPlayer = view.LoadStereoPanoramaVideo(_mainCamera, transform);
                            foreach (CTHotspotData hotspot in ((CTStereoPanoramaVideoData)itemData).ListHotspot)
                            {
                                CreateHotspot(itemData, hotspot);
                            }
                            break;
                        }
                }
            }
        }

        public void DeleteCurrentItem()
        {
            if (_isItemSelected && _selectedItem != null)
            {
                int itemIndex = _selectedItem.ItemIndex;

                Destroy(_currentMediaPlayer);

                _staticItems.Remove(itemIndex);

                Destroy(_selectedItem.gameObject);

                _selectedItem = null;
                _lastSelectedItem = null;

                _currentItemHotspots.Clear();

                MessageData msgData = new MessageData();
                msgData.Parameters.Add((byte)EParameterCodeWorld.ITEM_INDEX, itemIndex);

                dispatcher.Dispatch(WorldManagerEvents.ITEM_DELETED, msgData);
            }
            else if(_isHotspotSelected && _selectedHotspot != null && _selectedItem != null)
            {
                int itemIndex = _selectedItem.ItemIndex;
                string hotspotId = _selectedHotspot.HotspotID;

                _currentItemHotspots.Remove(_selectedHotspot);

                Destroy(_selectedHotspot.gameObject);

                _selectedHotspot = null;

                MessageData msgData = new MessageData();
                msgData.Parameters.Add((byte)EParameterCodeWorld.ITEM_INDEX, itemIndex);
                msgData.Parameters.Add((byte)EParameterCodeWorld.HOTSPOT_ID, hotspotId);

                dispatcher.Dispatch(WorldManagerEvents.HOTSPOT_DELETED, msgData);
            }
        }

        private bool IsMediaItem(ItemTypeCode itemType)
        {
            if (itemType == ItemTypeCode.PANORAMA || itemType == ItemTypeCode.PANORAMA_VIDEO || itemType == ItemTypeCode.PLANAR_VIDEO || itemType == ItemTypeCode.STEREO_PANORAMA_VIDEO /* || itemType == ItemTypeCode.AUDIO || itemType == ItemTypeCode.IMAGE*/)
            {
                return true;
            }

            return false;
        }

        internal void ResetCamera()
        {
            if (null == _mainCamera)
            {
                return;
            }

            _mainCamera.transform.position = _mainCameraInitPosition;
            _mainCamera.transform.rotation = _mainCameraInitRotation;

            if(null != _mainCamera.GetComponent<CustomCameraRotation>())
            {
                _mainCamera.GetComponent<CustomCameraRotation>().ResetTransform();
            }
        }

        #region Hotspot

        public void GetHotspotPositionDistance()
        {
            if (null != _currentMediaPlayer)
            {
                float distance = _currentMediaPlayer.transform.Find("Sphere").GetComponent<SphereCollider>().bounds.extents.x;

                Vector3 rightPos = transform.position + ((_mainCamera.transform.forward + _mainCamera.transform.right) / 2f).normalized * distance;
                Vector3 leftPos = transform.position + ((_mainCamera.transform.forward - _mainCamera.transform.right) / 2f).normalized * distance;

                _hotspotDrawDistance = Vector3.Distance(transform.position, (rightPos + leftPos) / 2f);
            }
        }

        public void CreateHotspot(CTItemData itemData, CTHotspotData hotspotData, bool isNewCreation = false, bool isPreviewModeOn = false, bool isLastLoadedMedia = false)
        {
            GetHotspotPositionDistance();

            GameObject hotspot = Instantiate(Resources.Load(PrefabNameConstants.HOTSPOT) as GameObject, _currentMediaPlayer.transform);
            hotspot.GetComponent<ViewStaticHotspot>().HotspotID = hotspotData.HotspotId;
            hotspot.GetComponent<ViewStaticHotspot>().IsInPreviewMode = isPreviewModeOn;

            if (isNewCreation)
            {
                hotspot.transform.position = _mainCamera.transform.position + (_mainCamera.transform.forward * _hotspotDrawDistance);

                Vector3 direction = hotspot.transform.position - _mainCamera.transform.position;
                Quaternion lookAtRotation = Quaternion.LookRotation(direction, Vector3.forward);
                hotspotData.HotspotLocalPostionX = lookAtRotation.eulerAngles.x;
                hotspotData.HotspotLocalPostionY = lookAtRotation.eulerAngles.y;
            }

            _currentItemHotspots.Add(hotspot.GetComponent<ViewStaticHotspot>());

            MessageData msgData = new MessageData();

            msgData.Parameters.Add((byte)EParameterCodeStatic.MAIN_CAMERA, _mainCamera);
            msgData.Parameters.Add((byte)EParameterCodeStatic.HOTSPOT_DRAW_DISTANCE, _hotspotDrawDistance);
            msgData.Parameters.Add((byte)EParameterCodeStatic.ITEM_DATA, itemData);
            msgData.Parameters.Add((byte)EParameterCodeStatic.HOTSPOT_DATA, hotspotData);
            msgData.Parameters.Add((byte)EParameterCodeStatic.NEW_CREATION, isNewCreation);
            msgData.Parameters.Add((byte)EParameterCodeStatic.IS_LAST_LOADED_MEDIA, isLastLoadedMedia);

            dispatcher.Dispatch(WorldManagerEvents.HOTSPOT_CREATED, msgData);
        }

        public void CreateNewHotspot(CTHotspotData hotspotData)
        {
            CTItemData selectedItemData = _selectedItem.GetItemData();

            CreateHotspot(selectedItemData, hotspotData, true);
        }

        public void HotspotSelected(CTHotspotData hotspotData)
        {
            foreach(var hotspotItem in _currentItemHotspots)
            {
                //if (_previewModeEnabled)
                //{
                //
                //}
                //else
                //{
                    hotspotItem.ShowHideHotspotBoxes(hotspotItem.HotspotID == hotspotData.HotspotId);
                //}

                if(hotspotItem.HotspotID == hotspotData.HotspotId)
                {
                    _selectedHotspot = hotspotItem;
                }
            }

            _isHotspotSelected = true;
            _isItemSelected = false;
            _isModuleSelected = false;
        }

        #endregion

        #region Preview Mode related

        internal void LoadFirstOrSelectedMedia()
        {
            int size = _currentItemHotspots.Count;

            for (int i = 0; i < size; i++)
            {
                Destroy(_currentItemHotspots[0].gameObject);
            }

            _currentItemHotspots.Clear();

            Destroy(_currentMediaPlayer);

            ViewStaticItem view;

            _lastActiveItem = _selectedItem;

            if (_selectedItem != null)
            {
                view = _selectedItem;
            }
            else
            {
                view = _staticItems.FirstOrDefault().Value;
            }

            LoadMediaForPreview(view, true);
        }

        internal void LoadMediaWithItemIndex(int itemIndex)
        {
            if(null != _staticItems[itemIndex])
            {
                _selectedItem = _staticItems[itemIndex];

                LoadFirstOrSelectedMedia();
            }
        }

        internal void LoadLastActiveMedia()
        {
            int size = _currentItemHotspots.Count;

            for (int i = 0; i < size; i++)
            {
                Destroy(_currentItemHotspots[0].gameObject);
            }

            _currentItemHotspots.Clear();

            Destroy(_currentMediaPlayer);

            ViewStaticItem view;

            if(null != _lastSelectedItem)
            {
                _selectedItem = _lastSelectedItem;
            }
            else
            {
                ModuleSelected();
                return;
            }

            if (_selectedItem != null)
            {
                view = _selectedItem;
            }
            else
            {
                view = _staticItems.FirstOrDefault().Value;
            }

            LoadMediaForPreview(view, false, true);
        }

        private void LoadMediaForPreview(ViewStaticItem view, bool isPreviewMode, bool isLastLoadedMedia = false)
        {
            CTItemData itemData = view.GetItemData();

            _currentItemHotspots = new List<ViewStaticHotspot>();

            if (null != view)
            {
                _selectedItem = view;
            }
            else
            {
                return;
            }

            Destroy(_currentMediaPlayer);

            if (IsMediaItem(itemData.ItemType))
            {
                switch (itemData.ItemType)
                {
                    case ItemTypeCode.PANORAMA:
                        {
                            _currentMediaPlayer = view.LoadPanoramaImage(_mainCamera, transform, isPreviewMode);
                            foreach (CTHotspotData hotspot in ((CTPanoramaData)itemData).ListHotspot)
                            {
                                CreateHotspot(itemData, hotspot, false, isPreviewMode, isLastLoadedMedia);
                            }
                            break;
                        }

                    case ItemTypeCode.PANORAMA_VIDEO:
                        {
                            _currentMediaPlayer = view.LoadPanoramaVideo(_mainCamera, transform, isPreviewMode);
                            foreach (CTHotspotData hotspot in ((CTPanoramaVideoData)itemData).ListHotspot)
                            {
                                CreateHotspot(itemData, hotspot, false, isPreviewMode, isLastLoadedMedia);
                            }
                            break;
                        }

                    case ItemTypeCode.PLANAR_VIDEO:
                        {
                            break;
                        }

                    case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                        {
                            _currentMediaPlayer = view.LoadStereoPanoramaVideo(_mainCamera, transform, isPreviewMode);
                            foreach (CTHotspotData hotspot in ((CTStereoPanoramaVideoData)itemData).ListHotspot)
                            {
                                CreateHotspot(itemData, hotspot, false, isPreviewMode, isLastLoadedMedia);
                            }
                            break;
                        }
                }
            }
        }

        public void TiggerEndBehaviour()
        {
            if(null != _lastActiveItem)
            {
                CTItemData itemData = _lastActiveItem.GetItemData();

                switch(itemData.ItemType)
                {
                    case ItemTypeCode.PANORAMA:
                        {
                            if (((CTPanoramaData)itemData).EndBehavior == EMultimediaEndBehaviorCode.TELEPORT)
                            {
                                int itemIndex = ((CTPanoramaData)itemData).TargetMultimediaItemIndex;

                                _selectedItem = _staticItems[itemIndex];

                                LoadFirstOrSelectedMedia();
                            }
                            break;
                        }
                    case ItemTypeCode.PANORAMA_VIDEO:
                        {
                            if(((CTPanoramaVideoData)itemData).EndBehavior == EMultimediaEndBehaviorCode.TELEPORT)
                            {
                                int itemIndex = ((CTPanoramaVideoData)itemData).TargetMultimediaItemIndex;

                                _selectedItem = _staticItems[itemIndex];

                                LoadFirstOrSelectedMedia();
                            }
                            break;
                        }

                    case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                        {
                            if (((CTStereoPanoramaVideoData)itemData).EndBehavior == EMultimediaEndBehaviorCode.TELEPORT)
                            {
                                int itemIndex = ((CTStereoPanoramaVideoData)itemData).TargetMultimediaItemIndex;

                                _selectedItem = _staticItems[itemIndex];

                                LoadFirstOrSelectedMedia();
                            }
                            break;
                        }
                }
            }
        }

        internal void MediaTimeReceived(float mediaTime)
        {
            CTItemData itemData = _selectedItem.GetItemData();

            List<CTHotspotData> hotspotList = new List<CTHotspotData>();

            switch(itemData.ItemType)
            {
                case ItemTypeCode.PANORAMA:
                    {
                        hotspotList = ((CTPanoramaData)itemData).ListHotspot;
                        break;
                    }

                case ItemTypeCode.PANORAMA_VIDEO:
                    {
                        hotspotList = ((CTPanoramaVideoData)itemData).ListHotspot;
                        break;
                    }

                case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                    {
                        hotspotList = ((CTStereoPanoramaVideoData)itemData).ListHotspot;
                        break;
                    }
            }

            foreach(var hotspotData in hotspotList)
            {
                if(hotspotData.ActiveTimeInMillisec > 0 && hotspotData.DeactiveTimeInMillisec > 0)
                {
                    if(_currentItemHotspots.Any(x => x.HotspotID == hotspotData.HotspotId))
                    {
                        if (mediaTime >= hotspotData.ActiveTimeInMillisec)
                        {
                            ViewStaticHotspot hotspot = _currentItemHotspots.FirstOrDefault(x => x.HotspotID == hotspotData.HotspotId);

                            hotspot.EnableDefaultHotspot();
                        }

                        if(mediaTime >= hotspotData.DeactiveTimeInMillisec)
                        {
                            ViewStaticHotspot hotspot = _currentItemHotspots.FirstOrDefault(x => x.HotspotID == hotspotData.HotspotId);

                            hotspot.DisableDefaultHotspot();
                        }
                    }
                }
            }
        }

        #endregion

        public void PreviewModeEnabled()
        {
            _previewModeEnabled = true;
        }

        public void PreviewModeDisabled()
        {
            _previewModeEnabled = false;
        }
    }
}