﻿using System.Collections.Generic;

using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.Directory;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class MediatorPublishModuleDialog : EventMediator
    {
        [Inject]
        public ViewPublishModuleDialog View { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
            View.Init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            View.dispatcher.UpdateListener(value, ViewPublishModuleDialog.PublishModuleEvents.PUBLISH_CLICKED, OnPublishClicked);
            View.dispatcher.UpdateListener(value, ViewPublishModuleDialog.PublishModuleEvents.UPDATED_NAME_PUBLISH_CLICKED, OnUpdatedNamePublishClicked);

            dispatcher.UpdateListener(value, EWorldEvent.SHOW_PUBLISH_MODULE_DIALOG, OnShowView);
            dispatcher.UpdateListener(value, EWorldEvent.PUBLISH_MODULE_SUCCESS, OnPublishSuccess);
            dispatcher.UpdateListener(value, EWorldEvent.PUBLISH_MODULE_NAME_EXIST_LOCALLY, OnPublishModuleNameExist);
        }

        private void OnPublishClicked(IEvent evt)
        {
            dispatcher.Dispatch(EWorldEvent.PUBLISH_MODULE, evt.data);
        }

        private void OnUpdatedNamePublishClicked(IEvent evt)
        {
            dispatcher.Dispatch(EWorldEvent.PUBLISH_UPDATED_MODULE, evt.data);
        }

        private void OnShowView(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            List<DirectoryInfoData> directories = (List<DirectoryInfoData>)msgData[(byte)EParameterCodeWorld.DIRECTORY_INFO_DATA_LIST];

            View.PopulateDropdown(directories);

            View.ShowView();
        }

        private void OnPublishSuccess()
        {
            View.HideView();
        }

        private void OnPublishModuleNameExist()
        {
            View.ShowFileConflitDialog();
        }
    }
}
