﻿using strange.extensions.mediation.impl;
using strange.extensions.dispatcher.eventdispatcher.api;

using PulseIQ.Local.ClientCommon.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class MediatorWorldTooltipManager : EventMediator
    {
        [Inject]
        public ViewWorldTooltipManager view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.Init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewWorldTooltipManager.TooltipEvents.VIEW_LOADED, OnViewLoaded);

            dispatcher.UpdateListener(value, EWorldEvent.TOOLTIP_TEXT_RESPONSE, OnTooltipTextReceived);
        }

        private void OnViewLoaded(IEvent evt)
        {
            dispatcher.Dispatch(EWorldEvent.TOOLTIP_TEXT_REQUESTED, evt.data);
        }

        private void OnTooltipTextReceived(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            int id = (int)msgData[(byte)EParameterCodeToolkit.TOOLTIP_ID];

            if (id == view.ID)
            {
                string text = (string)msgData[(byte)EParameterCodeToolkit.TOOLTIP_TEXT];

                view.SetTooltipText(text);
            }
        }
    }
}
