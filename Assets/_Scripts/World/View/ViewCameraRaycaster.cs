﻿using PulseIQ.Local.ClientCommon.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.World.Inspector;
using strange.extensions.mediation.impl;
using UnityEngine;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.World.Static;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class ViewCameraRaycaster : EventView
    {
        internal enum CAMERA_RAYCASTER_EVENTS
        {
            UPDATE_HOTSPOT_POSITION,
            UPDATE_HOTSPOT_ELEMENT_POSITION,
        }

        [SerializeField]
        private Camera _mainCamera;

        private float _hotspotDrawDistance = 0f;

        private Ray _hotspotRay = new Ray();

        private bool _hotspotSelected = false;

        private bool _isPreviewModeOn = false;

        private Transform _hotspotObjTransform = null;


        internal void init()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                int layerMask = LayerMask.GetMask("MediaPreview");

                _hotspotRay = _mainCamera.ScreenPointToRay(Input.mousePosition);

                RaycastHit hit;

                if (Physics.Raycast(_hotspotRay, out hit, Mathf.Infinity, layerMask))
                {
                    if (null != hit.transform.GetComponent<ViewStaticHotspot>())
                    {
                        _hotspotSelected = true;
                        _isPreviewModeOn = hit.transform.GetComponent<ViewStaticHotspot>().IsInPreviewMode;
                        _hotspotObjTransform = hit.transform;
                        hit.transform.GetComponent<ViewStaticHotspot>().HotspotSelected();
                        _hotspotDrawDistance = hit.transform.GetComponent<ViewStaticHotspot>().DrawDistance;
                    }
                }
            }

            // This is where you move the hotspot item in world
            if (_hotspotSelected && Input.GetMouseButton(0) && !_isPreviewModeOn)
            {
                _hotspotRay = _mainCamera.ScreenPointToRay(Input.mousePosition);
                _hotspotObjTransform.position = _mainCamera.transform.position + (_hotspotRay.direction * _hotspotDrawDistance);
            }

            if (Input.GetMouseButtonUp(0) && _hotspotSelected)
            {
                if (null != _hotspotObjTransform)
                {
                    MessageData datta = new MessageData();

                    datta.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _hotspotObjTransform.GetComponent<ViewStaticHotspot>().HotspotItemIndex);
                    datta.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ID, _hotspotObjTransform.GetComponent<ViewStaticHotspot>().HotspotID);
                    datta.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_XY_POSITION, GetHotspotXyPosition(_hotspotObjTransform.position - _mainCamera.transform.position));

                    dispatcher.Dispatch(CAMERA_RAYCASTER_EVENTS.UPDATE_HOTSPOT_POSITION, datta);
                }

                _hotspotObjTransform = null;
                _hotspotSelected = false;
            }
        }

        public Vector2 GetHotspotXyPosition(Vector3 direction)
        {
            Vector2 hotspotPos = new Vector2();
            Quaternion lookAtRotation = Quaternion.LookRotation(direction, Vector3.forward);
            hotspotPos.x = lookAtRotation.eulerAngles.x;
            hotspotPos.y = lookAtRotation.eulerAngles.y;

            return hotspotPos;
        }
    }
}