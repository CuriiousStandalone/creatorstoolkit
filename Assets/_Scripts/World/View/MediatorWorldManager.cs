﻿using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.World;
using PulseIQ.Local.Common.Model.Hotspot;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class MediatorWorldManager : EventMediator
    {
        [Inject]
        public ViewWorldManager view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.Init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewWorldManager.WorldManagerEvents.VIEW_LOADED, OnViewLoaded);
            view.dispatcher.UpdateListener(value, ViewWorldManager.WorldManagerEvents.ITEM_CREATED, OnItemCreated);
            view.dispatcher.UpdateListener(value, ViewWorldManager.WorldManagerEvents.ITEM_DELETED, OnItemDeleted);
            view.dispatcher.UpdateListener(value, ViewWorldManager.WorldManagerEvents.HOTSPOT_CREATED, OnHotspotCreated);
            view.dispatcher.UpdateListener(value, ViewWorldManager.WorldManagerEvents.HOTSPOT_DELETED, OnHotspotDeleted);

            dispatcher.UpdateListener(value, EWorldEvent.RESET_WORLD, OnResetWorld);
            dispatcher.UpdateListener(value, EWorldEvent.WORLD_CREATED, OnNewWorldCreated);

            dispatcher.UpdateListener(value, EWorldEvent.RESOURCE_ITEM_DROPPED_INTO_WORLD, OnResourceItemDroppedIntoWorld);
            dispatcher.UpdateListener(value, EWorldEvent.TELEPORT_ITEM_CREATED, OnTeleportItemCreated);
            dispatcher.UpdateListener(value, EWorldEvent.DELETE_ITEM, OnDeleteItem);

            dispatcher.UpdateListener(value, EWorldEvent.MODULE_SELECTED, OnModuleSelected);
            dispatcher.UpdateListener(value, EWorldEvent.SELECTED_ITEM_PROCESSED, OnItemSelected);
            dispatcher.UpdateListener(value, EWorldEvent.HOTSPOT_SELECTED, OnHotspotSelected);

            dispatcher.UpdateListener(value, EWorldEvent.CREATE_NEW_HOTSPOT, OnNewHotspotInitialized);

            dispatcher.UpdateListener(value, EWorldEvent.RESET_CAMERA, OnResetCamera);

            dispatcher.UpdateListener(value, EWorldEvent.LOAD_FIRST_OR_SELECTED_MEDIA, OnLoadFirstOrSelectedMedia);
            dispatcher.UpdateListener(value, EWorldEvent.LOAD_LAST_ACTIVE_MEDIA, OnLoadLastActiveMedia);
            dispatcher.UpdateListener(value, EWorldEvent.LOAD_MEDIA_WITH_ITEM_INDEX, OnLoadMediaWithItemIndex);
            dispatcher.UpdateListener(value, EWorldEvent.PANORAMA_VIDEO_ENDED, OnMediaEnded);
            dispatcher.UpdateListener(value, EWorldEvent.STEREO_PANORAMA_VIDEO_ENDED, OnMediaEnded);
            dispatcher.UpdateListener(value, EWorldEvent.PANORAMA_IMAGE_TELEPORT_TIME_TRIGGERED, OnMediaEnded);

            dispatcher.UpdateListener(value, EWorldEvent.MEDIA_VIDEO_CURRENT_TIME, OnMediaCurrentTimeReceived);

            dispatcher.UpdateListener(value, EEventToolkitCrossContext.PREVIEW_MODE_ENABLED, OnPreviewModeEnabled);
            dispatcher.UpdateListener(value, EEventToolkitCrossContext.PREVIEW_MODE_DISABLED, OnPreviewModeDisabled);
        }

        private void OnPreviewModeEnabled()
        {
            view.PreviewModeEnabled();
        }

        private void OnPreviewModeDisabled()
        {
            view.PreviewModeDisabled();
        }

        private void OnViewLoaded()
        {
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.APP_START_EVENT, true);

            dispatcher.Dispatch(EWorldEvent.CREATE_NEW_WORLD, msgData);
        }

        private void OnItemCreated(IEvent evt)
        {
            dispatcher.Dispatch(EWorldEvent.ITEM_CREATED, evt.data);
        }

        private void OnHotspotCreated(IEvent evt)
        {
            dispatcher.Dispatch(EWorldEvent.HOTSPOT_CREATED, evt.data);
        }

        private void OnItemSelected(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            CTItemData itemData = (CTItemData)msgData[(byte)EParameterCodeToolkit.ITEM_DATA];

            view.ItemSelected(itemData);
        }

        private void OnItemDeleted(IEvent evt)
        {
            dispatcher.Dispatch(EWorldEvent.ITEM_DELETED, evt.data);
        }

        private void OnHotspotDeleted(IEvent evt)
        {
            dispatcher.Dispatch(EWorldEvent.HOTSPOT_DELETED, evt.data);
        }

        private void OnHotspotSelected(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            CTHotspotData hotspotData = (CTHotspotData)msgData[(byte)EParameterCodeToolkit.HOTSPOT_DATA];
            
            view.HotspotSelected(hotspotData);
        }

        private void OnNewHotspotInitialized(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            CTHotspotData hotspotData = (CTHotspotData)msgData[(byte)EParameterCodeToolkit.HOTSPOT_DATA];

            view.CreateNewHotspot(hotspotData);
        }

        private void OnResetWorld()
        {
            view.DestroyCurrentWorldItems();

            dispatcher.Dispatch(EWorldEvent.RESET_WORLD_COMPLETED);
        }

        private void OnNewWorldCreated(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            WorldData worldData = (WorldData)msgData[(byte)EParameterCodeWorld.WORLD_DATA];

            view.WorldCreated(worldData);
        }

        private void OnModuleSelected(IEvent evt)
        {
            view.ModuleSelected();
        }

        private void OnResourceItemDroppedIntoWorld(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            CTItemData itemData = (CTItemData)msgData[(byte)EParameterCodeWorld.ITEM_DATA];

            view.ItemDroppedIntoWorld(itemData);
        }

        private void OnTeleportItemCreated(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            CTItemData itemData = (CTItemData)msgData[(byte)EParameterCodeToolkit.ITEM_DATA];

            view.ItemDroppedIntoWorld(itemData, true);
        }

        private void OnDeleteItem()
        {
            view.DeleteCurrentItem();
        }

        private void OnResetCamera()
        {
            view.ResetCamera();
        }

        #region Preview Mode related

        private void OnLoadFirstOrSelectedMedia()
        {
            view.LoadFirstOrSelectedMedia();
        }

        private void OnLoadLastActiveMedia()
        {
            view.LoadLastActiveMedia();
        }

        private void OnLoadMediaWithItemIndex(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            int itemIndex = (int)msgData[(byte)EParameterCodeWorld.ITEM_INDEX];

            view.LoadMediaWithItemIndex(itemIndex);
        }

        private void OnMediaEnded()
        {
            view.TiggerEndBehaviour();
        }

        private void OnMediaCurrentTimeReceived(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;
            float videoTime = (float)msgData[(byte)EParameterCodeWorld.MEDIA_CURRENT_TIME];

            view.MediaTimeReceived(videoTime);
        }

        #endregion
    }
}
