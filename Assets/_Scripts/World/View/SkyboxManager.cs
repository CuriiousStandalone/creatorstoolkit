﻿using UnityEngine;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class SkyboxManager : MonoBehaviour
    {
        [SerializeField]
        private Material _skyboxMaterial;

        void Awake()
        {
            RenderSettings.skybox = _skyboxMaterial;
        }
    }
}
