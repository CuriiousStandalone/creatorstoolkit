﻿using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using TMPro;

using strange.extensions.mediation.impl;

using PulseIQ.Local.Common.Model.Directory;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class ViewPublishModuleDialog : EventView
    {
        internal enum PublishModuleEvents
        {
            PUBLISH_CLICKED,
            UPDATED_NAME_PUBLISH_CLICKED,
        }

        [SerializeField]
        private Button _saveButton;
        [SerializeField]
        private Button _cancelButton;

        [SerializeField]
        private TMP_Dropdown _dropdown;

        [SerializeField]
        private Image _progressBar;

        [SerializeField]
        private GameObject _fileConflictDialog;

        [SerializeField]
        private Toggle _publicToggle;
        [SerializeField]
        private Toggle _ourVRToggle;

        private bool _isDirectory = false;

        private List<DirectoryInfoData> _directories;

        internal void Init()
        {
            _dropdown.enabled = false;
            _dropdown.interactable = false;
            _dropdown.captionText.color = new Color(0.44f, 0.44f, 0.44f);

            _publicToggle.SetIsOnWithoutNotify(true);
            _ourVRToggle.SetIsOnWithoutNotify(false);

            _directories = new List<DirectoryInfoData>();
        }

        internal void ShowView()
        {
            _dropdown.enabled = false;
            _dropdown.interactable = false;
            _dropdown.captionText.color = new Color(0.44f, 0.44f, 0.44f);

            _publicToggle.SetIsOnWithoutNotify(true);
            _ourVRToggle.SetIsOnWithoutNotify(false);

            gameObject.SetActive(true);
        }

        internal void HideView()
        {
            gameObject.SetActive(false);
            HideFileConflitDialog();
        }

        internal void PopulateDropdown(List<DirectoryInfoData> directories)
        {
            _directories = directories;

            List<string> directoryIds = new List<string>();

            foreach(var directory in _directories)
            {
                directoryIds.Add(directory.DirectoryName);
            }

            _dropdown.ClearOptions();
            _dropdown.AddOptions(directoryIds);
        }

        public void PublicSelected()
        {
            _dropdown.enabled = false;
            _dropdown.interactable = false;
            _dropdown.captionText.color = new Color(0.44f, 0.44f, 0.44f);
            _isDirectory = false;
        }

        public void OurVRSelected()
        {
            _dropdown.enabled = true;
            _dropdown.interactable = true;
            _dropdown.captionText.color = new Color(1f, 1f, 1f);
            _isDirectory = true;
        }

        public void PublishClicked()
        {
            MessageData msgData = new MessageData();

            msgData.Parameters.Add((byte)EParameterCodeWorld.IS_DIRECTORY, _isDirectory);
            msgData.Parameters.Add((byte)EParameterCodeWorld.DIRECTORY_ID, _directories.Count > 0 ? _directories[_dropdown.value].VirtualDirectoryId : "-1");

            dispatcher.Dispatch(PublishModuleEvents.PUBLISH_CLICKED, msgData);
        }

        public void CancelClicked()
        {
            gameObject.SetActive(false);
        }

        #region File Conflict Dialog

        [SerializeField]
        private Button _fileConflictPublishButton;

        [SerializeField]
        private TMP_InputField _fileConflictName;

        public void ShowFileConflitDialog()
        {
            _fileConflictPublishButton.enabled = false;
            _fileConflictPublishButton.interactable = false;

            _fileConflictName.text = "";

            _fileConflictDialog.SetActive(true);
        }

        public void HideFileConflitDialog()
        {
            _fileConflictDialog.SetActive(false);
        }

        public void FileConflictNameChanged()
        {
            if(_fileConflictName.text != "")
            {
                _fileConflictPublishButton.enabled = true;
                _fileConflictPublishButton.interactable = true;
            }
            else
            {
                _fileConflictPublishButton.enabled = false;
                _fileConflictPublishButton.interactable = false;
            }
        }

        public void FileConflictPublishClicked()
        {
            MessageData msgData = new MessageData();

            msgData.Parameters.Add((byte)EParameterCodeWorld.IS_DIRECTORY, _isDirectory);
            msgData.Parameters.Add((byte)EParameterCodeWorld.DIRECTORY_ID, _directories[_dropdown.value].VirtualDirectoryId);
            msgData.Parameters.Add((byte)EParameterCodeWorld.WORLD_NAME, _fileConflictName.text);

            dispatcher.Dispatch(PublishModuleEvents.UPDATED_NAME_PUBLISH_CLICKED, msgData);
        }

        #endregion
    }
}
