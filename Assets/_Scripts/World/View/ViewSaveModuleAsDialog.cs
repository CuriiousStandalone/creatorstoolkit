﻿using UnityEngine;
using UnityEngine.UI;

using TMPro;

using strange.extensions.mediation.impl;

using PulseIQ.Local.ClientCommon.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class ViewSaveModuleAsDialog : EventView
    {
        internal enum SaveModuleEvents
        {
            SAVE_CLICKED,
        }

        [SerializeField]
        private Button _saveButton;
        [SerializeField]
        private Button _cancelButton;

        [SerializeField]
        private TMP_InputField _inputField;

        [SerializeField]
        private GameObject _genericDialog;

        internal void Init()
        {
            _saveButton.enabled = false;
            _saveButton.interactable = false;
        }

        internal void ShowView()
        {
            _inputField.text = "";
            gameObject.SetActive(true);
        }

        internal void HideView()
        {
            _inputField.text = "";
            gameObject.SetActive(false);
        }

        internal void NameExistLocally()
        {
            GenericPopupDialogTMPro dialogClass = _genericDialog.GetComponent<GenericPopupDialogTMPro>();

            dialogClass.InitializeDialog("Name Exist Locally", "Module name you provided exist already in your local file system. Chose another name.", "OK", null);
        }

        public void SaveClicked()
        {
            MessageData msgData = new MessageData();

            msgData.Parameters.Add((byte)EParameterCodeWorld.WORLD_NAME, _inputField.text);

            dispatcher.Dispatch(SaveModuleEvents.SAVE_CLICKED, msgData);
        }

        public void CancelClicked()
        {
            gameObject.SetActive(false);
        }

        public void NameUpdated()
        {
            if(_inputField.text != "")
            {
                _saveButton.enabled = true;
                _saveButton.interactable = true;
            }
            else
            {
                _saveButton.enabled = false;
                _saveButton.interactable = false;
            }
        }
    }
}
