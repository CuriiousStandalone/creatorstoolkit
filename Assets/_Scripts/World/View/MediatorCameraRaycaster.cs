﻿using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class MediatorCameraRaycaster : EventMediator
    {
        [Inject]
        public ViewCameraRaycaster view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewCameraRaycaster.CAMERA_RAYCASTER_EVENTS.UPDATE_HOTSPOT_ELEMENT_POSITION, UpdateHotspotElementPosition);
            view.dispatcher.UpdateListener(value, ViewCameraRaycaster.CAMERA_RAYCASTER_EVENTS.UPDATE_HOTSPOT_POSITION, UpdateHotspotPosition);
        }

        private void UpdateHotspotElementPosition(IEvent evt)
        {
            dispatcher.Dispatch(EWorldEvent.UPDATE_HOTSPOT_ELEMENT_POSITION, evt.data);
        }

        private void UpdateHotspotPosition(IEvent evt)
        {
            dispatcher.Dispatch(EWorldEvent.UPDATE_HOTSPOT_POSITION, evt.data);
        }
    }
}