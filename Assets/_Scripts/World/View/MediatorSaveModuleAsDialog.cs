﻿using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class MediatorSaveModuleAsDialog : EventMediator
    {
        [Inject]
        public ViewSaveModuleAsDialog View { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
            View.Init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            View.dispatcher.UpdateListener(value, ViewSaveModuleAsDialog.SaveModuleEvents.SAVE_CLICKED, OnSaveClicked);

            dispatcher.UpdateListener(value, EWorldEvent.SHOW_SAVE_MODULE_AS_DIALOG, OnShowView);
            dispatcher.UpdateListener(value, EWorldEvent.SAVE_MODULE_AS_SUCCESS, OnHideView);
            dispatcher.UpdateListener(value, EWorldEvent.SAVE_MODULE_AS_NAME_EXIST_LOCALLY, OnNameExistLocally);
        }

        private void OnShowView()
        {
            View.ShowView();
        }

        private void OnSaveClicked(IEvent evt)
        {
            dispatcher.Dispatch(EWorldEvent.SAVE_MODULE_AS, evt.data);
        }

        private void OnHideView()
        {
            View.HideView();
        }

        private void OnNameExistLocally()
        {
            View.ShowView();
            View.NameExistLocally();
        }
    }
}
