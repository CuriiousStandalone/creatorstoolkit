﻿using System;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

using strange.extensions.mediation.impl;

using UIWidgets;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIPanels;
using PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents;
using PulseIQ.Local.Common;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;
using PulseIQ.Local.Common.Model.World;
using PulseIQ.Local.Common.Model.Panorama;
using PulseIQ.Local.Common.Model.PanoramaVideo;

namespace CuriiousIQ.Local.CreatorsToolkit.World.WorldHierarchy
{
    public class ViewWorldHierarchyManager : EventView, IResponsivePanel
    {
        public enum ViewWorldHierarchyManagerEvents
        {
            VIEW_LOADED,
            PANEL_HIDDEN,
            PANEL_SHOWN,
            RESOURCE_DROPPED,
            ITEM_SELECTED,
            MODULE_SELECTED,
            HOTSPOT_SELECTED,
        }

        public CTreeView TreeView;
        public CSearchBarTypeFilter TreeSearchBar;
        private WorldData _currentWorldData;
        private List<CTHotspotData> _additionalHotspots = new List<CTHotspotData>();

        const string HOTSPOT_TYPE = "HOTSPOT";
        const string MODULE_TYPE = "MODULE";

        private static bool IsSupportedType(ItemTypeCode type)
        {
            switch (type)
            {
                case ItemTypeCode.AUDIO:
                case ItemTypeCode.IMAGE:
                case ItemTypeCode.PANORAMA:
                case ItemTypeCode.PANORAMA_VIDEO:
                case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                case ItemTypeCode.HOTSPOT:
                    return true;
                default:
                    return false;
            }
        }

        private static bool MightHaveChildren(ItemTypeCode type)
        {
            switch (type)
            {
                case ItemTypeCode.IMAGE:
                case ItemTypeCode.PANORAMA:
                case ItemTypeCode.PANORAMA_VIDEO:
                case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                case ItemTypeCode.HOTSPOT:
                    return true;
                default:
                    return false;
            }
        }

        public string PanelId
        {
            get { return gameObject.GetComponent<CPanel>()._panelId; }
        }

        internal void Init()
        {
            TreeView.InitNow();
            TreeView.ResourceDropped.AddListener(OnResourceDropped);
            TreeView.OnItemSelected.AddListener(OnItemSelected);

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelId);

            dispatcher.Dispatch(ViewWorldHierarchyManagerEvents.VIEW_LOADED, msgData);

            HidePanel();
        }

        private void OnItemSelected(int index, ListViewItem listViewItem)
        {
            MessageData msgData = new MessageData();
            string itemTypeString = TreeView.SelectedItem.Node.Item.Types[0];
            if (itemTypeString.Equals(HOTSPOT_TYPE))
            {
                CTItemData selectedItem = _currentWorldData.ListItems.FirstOrDefault(item => item.ItemIndex.ToString().Equals(TreeView.SelectedItem.Node.Parent.Item.ItemID));

                if (selectedItem != null)
                {
                    MessageData addedMessageData = new MessageData();
                    addedMessageData.Parameters.Add((byte)EParameterCodeToolkit.ITEM_DATA, selectedItem);

                    dispatcher.Dispatch(ViewWorldHierarchyManagerEvents.ITEM_SELECTED, addedMessageData);
                }

                CTHotspotData hotspot = _additionalHotspots.FirstOrDefault(spot => spot.HotspotId.Equals(TreeView.SelectedItem.Node.Item.ItemID));
                if (hotspot != null)
                {
                    if(hotspot.TeleportMultimediaItemIndex > 0)
                    {
                        foreach(CTItemData iData in _currentWorldData.ListItems)
                        {
                            if (iData.ItemIndex == hotspot.TeleportMultimediaItemIndex)
                            {
                                msgData.Parameters.Add((byte)EParameterCodeToolkit.MEDIA_TELEPORT_ITEM, iData.ItemName);
                            }
                        }
                    }

                    else if (!msgData.Parameters.ContainsKey((byte)EParameterCodeToolkit.MEDIA_TELEPORT_ITEM))
                    {
                        msgData.Parameters.Add((byte)EParameterCodeToolkit.MEDIA_TELEPORT_ITEM, "");
                    }

                    msgData.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_DATA, hotspot);
                    msgData.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, selectedItem.ItemIndex);
                    msgData.Parameters.Add((byte)EParameterCodeToolkit.ITEM_TYPE, selectedItem.ItemType);
                    dispatcher.Dispatch(ViewWorldHierarchyManagerEvents.HOTSPOT_SELECTED, msgData);
                }
            }
            else
            {
                if (itemTypeString.Equals(MODULE_TYPE))
                {
                    msgData.Parameters.Add((byte)EParameterCodeWorld.WORLD_DATA, _currentWorldData);
                    dispatcher.Dispatch(ViewWorldHierarchyManagerEvents.MODULE_SELECTED, msgData);
                }
                else
                {
                    CTItemData selectedItem = _currentWorldData.ListItems.FirstOrDefault(item => item.ItemIndex.ToString().Equals(TreeView.SelectedItem.Node.Item.ItemID));
                    
                    msgData.Parameters.Add((byte)EParameterCodeToolkit.ITEM_DATA, selectedItem);
                    dispatcher.Dispatch(ViewWorldHierarchyManagerEvents.ITEM_SELECTED, msgData);
                }
            }
        }

        protected override void OnDestroy()
        {
            TreeView.ResourceDropped.RemoveListener(OnResourceDropped);
            TreeView.OnItemSelected.RemoveListener(OnItemSelected);

            base.OnDestroy();
        }

        private void OnResourceDropped(ListItemCardData item)
        {
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)CommonParameterCode.RESOURCEID, item.ResourceID);

            ItemTypeCode type = (ItemTypeCode)Enum.Parse(typeof(ItemTypeCode), item.Type[0]);
            msgData.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, type);

            dispatcher.Dispatch(ViewWorldHierarchyManagerEvents.RESOURCE_DROPPED, msgData);
        }

        public void ResetHierarchy()
        {
            TreeView.Reset();
        }

        public void AddItem(CTItemData itemData)
        {
            Canvas.ForceUpdateCanvases();

            if (!IsSupportedType(itemData.ItemType))
            {
                return;
            }

            string itemTypeString = Enum.GetName(itemData.ItemType.GetType(), itemData.ItemType);
            TreeSearchBar.AddType(itemTypeString);
            TreeView.AddItem(itemData.ItemName, itemData.ItemIndex.ToString(), new[] {itemTypeString});

            if (MightHaveChildren(itemData.ItemType))
            {
                switch (itemData.ItemType)
                {
                    case ItemTypeCode.PANORAMA:
                        CTPanoramaData panoramaData = (CTPanoramaData)itemData;

                        foreach (CTHotspotData hotspot in panoramaData.ListHotspot)
                        {
                            AddHotspotItem(hotspot, itemData.ItemIndex.ToString());
                        }
                        break;
                    case ItemTypeCode.PANORAMA_VIDEO:
                        CTPanoramaVideoData panoramaVideoData = (CTPanoramaVideoData)itemData;

                        foreach (CTHotspotData hotspot in panoramaVideoData.ListHotspot)
                        {
                            AddHotspotItem(hotspot, itemData.ItemIndex.ToString());
                        }
                        break;
                    case ItemTypeCode.PLANAR_VIDEO:
                        break;
                    case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                        CTStereoPanoramaVideoData stereoPanoramaVideoData = (CTStereoPanoramaVideoData)itemData;

                        foreach (CTHotspotData hotspot in stereoPanoramaVideoData.ListHotspot)
                        {
                            AddHotspotItem(hotspot, itemData.ItemIndex.ToString());
                        }
                        break;
                    case ItemTypeCode.HOTSPOT:
                        break;
                    case ItemTypeCode.AUDIO:
                        break;
                    case ItemTypeCode.IMAGE:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public void ItemDataChanged(CTItemData itemData)
        {
            for(int i = 0; i < _currentWorldData.ListItems.Count; ++i)
            {
                if (_currentWorldData.ListItems[i].ItemIndex == itemData.ItemIndex)
                {
                    _currentWorldData.ListItems[i] = itemData;
                }
            }
        }


        public void HotspotDataChanged(CTHotspotData itemData)
        {
            for (int i = 0; i < _additionalHotspots.Count; ++i)
            {
                if (_additionalHotspots[i].HotspotId == itemData.HotspotId)
                {
                    _additionalHotspots[i] = itemData;
                }
            }
        }

        internal void AddHotspotItem(CTHotspotData hotspot, string parentID)
        {
            Canvas.ForceUpdateCanvases();

            TreeView.AddItem(hotspot.HotspotName, hotspot.HotspotId, new[] {HOTSPOT_TYPE}, parentID);
            _additionalHotspots.Add(hotspot);
            //TODO also add any video, audio, or panel information.
        }

        public void SetupHierarchy(WorldData worldData)
        {
            TreeView.AddItem("Module", "Module", new[] {MODULE_TYPE});
            _currentWorldData = worldData;
            //move to on item created individually.
            TreeView.SelectItem(0);
        }

        public void UpdateItemName(string itemId, string itemName)
        {
            TreeView.FindNode(itemId).Item.Name = itemName;
        }

        public void RemoveItem(string itemNameOrID)
        {
            TreeView.RemoveItem(itemNameOrID);
        }

        public void RemoveItemWithID(string itemID)
        {
            TreeView.RemoveItemWithID(itemID);
        }

        public void RemoveItem(CTItemData itemData)
        {
            TreeView.RemoveItem(itemData.ItemName);
        }

        public void SelectItemOverride(int itemId)
        {
            TreeView.SelectItem(itemId.ToString());

            CTItemData selectedItem = _currentWorldData.ListItems.FirstOrDefault(x => x.ItemIndex == itemId);

            if (selectedItem != null)
            {
                MessageData addedMessageData = new MessageData();
                addedMessageData.Parameters.Add((byte)EParameterCodeToolkit.ITEM_DATA, selectedItem);

                dispatcher.Dispatch(ViewWorldHierarchyManagerEvents.ITEM_SELECTED, addedMessageData);
            }
        }

        public void SelectHotspotOverride(string hotspotId, string parentId, int itemIndex, ItemTypeCode type)
        {
            TreeView.ExpandItem(parentId);

            TreeView.SelectItem(hotspotId);

            CTHotspotData hotspot = _additionalHotspots.FirstOrDefault(spot => spot.HotspotId.Equals(hotspotId));
            if (hotspot != null)
            {
                MessageData msgData = new MessageData();

                if (hotspot.TeleportMultimediaItemIndex > 0)
                {
                    foreach (CTItemData iData in _currentWorldData.ListItems)
                    {
                        if (iData.ItemIndex == hotspot.TeleportMultimediaItemIndex)
                        {
                            msgData.Parameters.Add((byte)EParameterCodeToolkit.MEDIA_TELEPORT_ITEM, iData.ItemName);
                        }
                    }
                }
                else
                {
                    if (!msgData.Parameters.ContainsKey((byte)EParameterCodeToolkit.MEDIA_TELEPORT_ITEM))
                    {
                        msgData.Parameters.Add((byte)EParameterCodeToolkit.MEDIA_TELEPORT_ITEM, "");
                    }
                }

                msgData.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_DATA, hotspot);
                msgData.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, itemIndex);
                msgData.Parameters.Add((byte)EParameterCodeToolkit.ITEM_TYPE, type);
                dispatcher.Dispatch(ViewWorldHierarchyManagerEvents.HOTSPOT_SELECTED, msgData);
            }
        }

        #region Panel

        public void HidePanel()
        {
            gameObject.SetActive(false);

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelId);

            dispatcher.Dispatch(ViewWorldHierarchyManagerEvents.PANEL_HIDDEN, msgData);
        }

        public void ShowPanel()
        {
            gameObject.SetActive(true);

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelId);

            dispatcher.Dispatch(ViewWorldHierarchyManagerEvents.PANEL_SHOWN, msgData);
        }

        public void ShowPanelWithTween()
        {
            Debug.Log("ViewWorldHierarchyManager.ShowPanelWithTween is not yet implemented");
        }

        public void HidePanelWithTween()
        {
            Debug.Log("ViewWorldHierarchyManager.HidePanelWithTween is not yet implemented");
        }

        public void IncreaseHeight(float height)
        {
            gameObject.GetComponent<CPanel>().IncreaseHeightEqually(height);
        }

        public void IncreaseHeightFromBottom(float height)
        {
            gameObject.GetComponent<CPanel>().IncreaseHeightFromBottom(height);
        }

        public void IncreaseHeightFromTop(float height)
        {
            gameObject.GetComponent<CPanel>().IncreaseHeightFromTop(height);
        }

        public void DecreaseHeight(float height)
        {
            gameObject.GetComponent<CPanel>().DecreaseHeightEqually(height);
        }

        public void DecreaseHeightFromBottom(float height)
        {
            gameObject.GetComponent<CPanel>().DecreaseHeightFromBottom(height);
        }

        public void DecreaseHeightFromTop(float height)
        {
            gameObject.GetComponent<CPanel>().DecreaseHeightFromTop(height);
        }

        public void IncreaseWidth(float width)
        {
            Debug.Log("ViewWorldHierarchyManager.IncreaseWidth is not yet implemented");
        }

        public void IncreaseWidthFromRight(float width)
        {
            Debug.Log("ViewWorldHierarchyManager.IncreaseWidthFromRight is not yet implemented");
        }

        public void IncreaseWidthFromLeft(float width)
        {
            Debug.Log("ViewWorldHierarchyManager.IncreaseWidthFromLeft is not yet implemented");
        }

        public void DecreaseWidth(float width)
        {
            Debug.Log("ViewWorldHierarchyManager.DecreaseWidth is not yet implemented");
        }

        public void DecreaseWidthFromRight(float width)
        {
            Debug.Log("ViewWorldHierarchyManager.DecreaseWidthFromRight is not yet implemented");
        }

        public void DecreaseWidthFromLeft(float width)
        {
            Debug.Log("ViewWorldHierarchyManager.DecreaseWidthFromLeft is not yet implemented");
        }

        public void RepositionToTop(float height)
        {
            gameObject.GetComponent<CPanel>().RepositionToTop(height);
        }

        public void RepositionToBottom(float height)
        {
            gameObject.GetComponent<CPanel>().RepositionToBottom(height);
        }

        public void RepositionToLeft(float width)
        {
            gameObject.GetComponent<CPanel>().RepositionToLeft(width);
        }

        public void RepositionToRight(float width)
        {
            gameObject.GetComponent<CPanel>().RepositionToRight(width);
        }

        #endregion
    }
}