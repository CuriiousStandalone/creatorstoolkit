﻿using UnityEngine;

using strange.extensions.mediation.impl;
using strange.extensions.dispatcher.eventdispatcher.api;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.World;
using PulseIQ.Local.Common.Model.Hotspot;
using CuriiousIQ.Local.CreatorsToolkit.World.Static;
using PulseIQ.Local.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.World.WorldHierarchy
{
    public class MediatorWorldHierarchyManager : EventMediator
    {
        [Inject]
        public ViewWorldHierarchyManager view { get; set; }

        [Inject]
        public IModelActiveWorld ActiveWorldModel { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.Init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewWorldHierarchyManager.ViewWorldHierarchyManagerEvents.VIEW_LOADED, OnViewLoaded);
            view.dispatcher.UpdateListener(value, ViewWorldHierarchyManager.ViewWorldHierarchyManagerEvents.PANEL_HIDDEN, OnPanelHidden);
            view.dispatcher.UpdateListener(value, ViewWorldHierarchyManager.ViewWorldHierarchyManagerEvents.PANEL_SHOWN, OnPanelShown);

            view.dispatcher.UpdateListener(value, ViewWorldHierarchyManager.ViewWorldHierarchyManagerEvents.RESOURCE_DROPPED, OnResourceDropped);
            view.dispatcher.UpdateListener(value, ViewWorldHierarchyManager.ViewWorldHierarchyManagerEvents.ITEM_SELECTED, OnItemSelected);
            view.dispatcher.UpdateListener(value, ViewWorldHierarchyManager.ViewWorldHierarchyManagerEvents.HOTSPOT_SELECTED, OnHotspotSelected);
            view.dispatcher.UpdateListener(value, ViewWorldHierarchyManager.ViewWorldHierarchyManagerEvents.MODULE_SELECTED, OnModuleSelected);

            dispatcher.UpdateListener(value, EWorldHierarchyEvent.HIDE_PANEL, OnHidePanel);
            dispatcher.UpdateListener(value, EWorldHierarchyEvent.SHOW_PANEL, OnShowPanel);

            dispatcher.UpdateListener(value, EWorldHierarchyEvent.INCREASE_PANEL_HEIGHT, OnIncreasePanelHeight);
            dispatcher.UpdateListener(value, EWorldHierarchyEvent.DECREASE_PANEL_HEIGHT, OnDecreasePanelHeight);

            dispatcher.UpdateListener(value, EWorldHierarchyEvent.REPOSITION_PANEL_LEFT, OnRepositionPanelToLeft);
            dispatcher.UpdateListener(value, EWorldHierarchyEvent.REPOSITION_PANEL_RIGHT, OnRepositionPanelToRight);

            dispatcher.UpdateListener(value, EWorldHierarchyEvent.RESET_WORLD_HIERARCHY, OnResetWorldHierarchy);
            dispatcher.UpdateListener(value, EWorldHierarchyEvent.WORLD_CREATED, OnNewWorldGenerated);
            dispatcher.UpdateListener(value, EWorldHierarchyEvent.REMOVE_ITEM, OnItemRemoved);

            dispatcher.UpdateListener(value, EWorldHierarchyEvent.HOTSPOT_WORLD_ITEM_SELECTED, OnHotspotWorldItemSelected);
            dispatcher.UpdateListener(value, EWorldHierarchyEvent.ITEM_CREATED, OnItemCreated);
            dispatcher.UpdateListener(value, EWorldHierarchyEvent.HOTSPOT_CREATED, OnHotspotCreated);
            dispatcher.UpdateListener(value, EWorldHierarchyEvent.REMOVE_HOTSPOT, OnHotspotRemoved);

            dispatcher.UpdateListener(value, EWorldEvent.ITEM_NAME_CHANGED, OnItemNameChange);
            dispatcher.UpdateListener(value, EWorldEvent.SELECT_ITEM, OnSelectItemOverride);
            dispatcher.UpdateListener(value, EWorldEvent.ITEM_DATA_CHANGED, OnItemDataChanged);
            dispatcher.UpdateListener(value, EWorldEvent.HOTSPOT_DATA_CHANGED, OnHotspotDataChanged);
        }

        #region Incoming

        private void OnResetWorldHierarchy()
        {
            view.ResetHierarchy();
            dispatcher.Dispatch(EWorldHierarchyEvent.RESET_WORLD_HIERARCHY_COMPLETED);
        }

        private void OnNewWorldGenerated(IEvent payload)
        {
            MessageData msgData = (MessageData)payload.data;
            WorldData worldData = (WorldData)msgData.Parameters[(byte)EParameterCodeWorld.WORLD_DATA];
            view.SetupHierarchy(worldData);
        }

        private void OnItemCreated(IEvent payload)
        {
            MessageData msgData = (MessageData)payload.data;
            CTItemData itemData = (CTItemData)msgData[(byte)EParameterCodeWorld.ITEM_DATA];
            view.AddItem(itemData);
        }

        private void OnHotspotCreated(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            CTHotspotData hotspotData = (CTHotspotData)msgData[(byte)EParameterCodeStatic.HOTSPOT_DATA];
            CTItemData itemData = (CTItemData)msgData[(byte)EParameterCodeStatic.ITEM_DATA];

            view.AddHotspotItem(hotspotData, itemData.ItemIndex.ToString());
        }

        private void OnItemRemoved(IEvent payload)
        {
            MessageData msgData = (MessageData)payload.data;
            if (msgData.Parameters.ContainsKey((byte)EParameterCodeToolkit.ITEM_DATA))
            {
                CTItemData itemData = (CTItemData)msgData.Parameters[(byte)EParameterCodeToolkit.ITEM_DATA];
                view.RemoveItemWithID(itemData.ItemIndex.ToString());
            }
            else if (msgData.Parameters.ContainsKey((byte)EParameterCodeToolkit.ITEM_INDEX))
            {
                string itemID = (string)msgData.Parameters[(byte)EParameterCodeToolkit.ITEM_INDEX];
                view.RemoveItem(itemID);
            }
        }

        private void OnHotspotRemoved(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            string hotspotID = (string)msgData[(byte)EParameterCodeToolkit.HOTSPOT_ID];
            string hotspotName = (string)msgData[(byte)EParameterCodeToolkit.HOTSPOT_NAME];

            view.RemoveItemWithID(hotspotID);
        }

        private void OnItemNameChange(IEvent payload)
        {
            MessageData msgData = (MessageData)payload.data;
            string itemID = (string)msgData.Parameters[(byte)EParameterCodeToolkit.ITEM_INDEX];
            string itemName = (string)msgData.Parameters[(byte)EParameterCodeToolkit.ITEM_NAME];
            view.UpdateItemName(itemID, itemName);
        }

        private void OnSelectItemOverride(IEvent payload)
        {
            MessageData msgData = (MessageData)payload.data;

            int itemIndex = (int)msgData.Parameters[(byte)EParameterCodeToolkit.ITEM_INDEX];

            view.SelectItemOverride(itemIndex);
        }


        private void OnItemDataChanged(IEvent payload)
        {
            MessageData data = (MessageData)payload.data;
            CTItemData itemData = (CTItemData)data[(byte)EParameterCodeToolkit.ITEM_DATA];

            view.ItemDataChanged(itemData);
        }

        private void OnHotspotDataChanged(IEvent payload)
        {
            MessageData data = (MessageData)payload.data;
            CTHotspotData itemData = (CTHotspotData)data[(byte)EParameterCodeToolkit.HOTSPOT_DATA];

            view.HotspotDataChanged(itemData);
        }

        private void OnHotspotWorldItemSelected(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            string hotspotId = (string)msgData[(byte)EParameterCodeToolkit.HOTSPOT_ID];
            string parentId = (string)msgData[(byte)EParameterCodeToolkit.ITEM_ID];
            ItemTypeCode type = (ItemTypeCode)msgData[(byte)EParameterCodeToolkit.ITEM_TYPE];
            int itemIndex = (int)msgData[(byte)EParameterCodeToolkit.ITEM_INDEX];

            view.SelectHotspotOverride(hotspotId, parentId, itemIndex, type);
        }

        #endregion

        #region Outgoing

        private void OnModuleSelected(IEvent payload)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.DISABLE_ADD_HOTSPOT_BUTTON);
            dispatcher.Dispatch(EEventToolkitCrossContext.DISABLE_DELETE_ITEM_BUTTON);
            dispatcher.Dispatch(EEventToolkitCrossContext.MODULE_SELECTED);
            dispatcher.Dispatch(EWorldHierarchyEvent.MODULE_SELECTED, payload.data);
        }

        private void OnHotspotSelected(IEvent payload)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.ENABLE_ADD_HOTSPOT_BUTTON);
            dispatcher.Dispatch(EEventToolkitCrossContext.ENABLE_DELETE_ITEM_BUTTON);
            dispatcher.Dispatch(EWorldEvent.HOTSPOT_SELECTED, payload.data);
        }

        private void OnItemSelected(IEvent payload)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.ENABLE_ADD_HOTSPOT_BUTTON);
            dispatcher.Dispatch(EEventToolkitCrossContext.ENABLE_DELETE_ITEM_BUTTON);
            dispatcher.Dispatch(EEventToolkitCrossContext.ITEM_SELECTED);
            dispatcher.Dispatch(EWorldEvent.ITEM_SELECTED, payload.data);
        }

        private void OnResourceDropped(IEvent payload)
        {
            dispatcher.Dispatch(EWorldHierarchyEvent.RESOURCE_ITEM_DROPPED_INTO_HIERARCHY, payload.data);
        }

        #endregion

        #region Panel

        private void OnViewLoaded(IEvent evt)
        {
            if(null != ActiveWorldModel.ActiveWorldData)
            {
                view.SetupHierarchy(ActiveWorldModel.ActiveWorldData);
            }

            dispatcher.Dispatch(EEventToolkitCrossContext.PANEL_LOADED, evt.data);
        }

        private void OnHidePanel(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;
            string panelId = (string)msgData[(byte)EParameterCodeToolkit.PANEL_ID];

            if (panelId == view.PanelId)
            {
                view.HidePanel();
            }
        }

        private void OnShowPanel(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;
            string panelId = (string)msgData[(byte)EParameterCodeToolkit.PANEL_ID];

            if (panelId == view.PanelId)
            {
                view.ShowPanel();
            }
        }

        private void OnPanelHidden(IEvent evt)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.PANEL_HIDDEN, evt.data);
        }

        private void OnPanelShown(IEvent evt)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.PANEL_SHOWN, evt.data);
        }

        private void OnIncreasePanelHeight(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            string panelId = (string)msgData[(byte)EParameterCodeToolkit.PANEL_ID];

            if (panelId == view.PanelId)
            {
                float height = (float)msgData[(byte)EParameterCodeToolkit.PANEL_HEIGHT];
                EAnchor anchor = (EAnchor)msgData[(byte)EParameterCodeToolkit.PANEL_ANCHOR];

                if (anchor == EAnchor.TOP)
                {
                    view.IncreaseHeightFromBottom(height);
                }
                else if (anchor == EAnchor.BOTTOM)
                {
                    view.IncreaseHeightFromTop(height);
                }
                else
                {
                    view.IncreaseHeight(height);
                }
            }
        }

        private void OnDecreasePanelHeight(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            string panelId = (string)msgData[(byte)EParameterCodeToolkit.PANEL_ID];

            if (panelId == view.PanelId)
            {
                float height = (float)msgData[(byte)EParameterCodeToolkit.PANEL_HEIGHT];
                EAnchor anchor = (EAnchor)msgData[(byte)EParameterCodeToolkit.PANEL_ANCHOR];

                if (anchor == EAnchor.TOP)
                {
                    view.DecreaseHeightFromBottom(height);
                }
                else if (anchor == EAnchor.BOTTOM)
                {
                    view.DecreaseHeightFromTop(height);
                }
                else
                {
                    view.DecreaseHeight(height);
                }
            }
        }

        private void OnRepositionPanelToLeft(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            string panelId = (string)msgData[(byte)EParameterCodeToolkit.PANEL_ID];

            if (panelId == view.PanelId)
            {
                float width = (float)msgData[(byte)EParameterCodeToolkit.PANEL_WIDTH];
                view.RepositionToLeft(width);
            }
        }

        private void OnRepositionPanelToRight(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            string panelId = (string)msgData[(byte)EParameterCodeToolkit.PANEL_ID];

            if (panelId == view.PanelId)
            {
                float width = (float)msgData[(byte)EParameterCodeToolkit.PANEL_WIDTH];
                view.RepositionToRight(width);
            }
        }

        #endregion
    }
}