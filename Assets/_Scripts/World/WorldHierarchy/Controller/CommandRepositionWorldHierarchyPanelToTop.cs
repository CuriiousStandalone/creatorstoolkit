﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.WorldHierarchy
{
    public class CommandRepositionWorldHierarchyPanelToTop : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EWorldHierarchyEvent.REPOSITION_PANEL_TOP, evt.data);
        }
    }
}
