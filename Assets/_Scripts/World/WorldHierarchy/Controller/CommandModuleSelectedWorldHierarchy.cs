﻿using PulseIQ.Local.ClientCommon.Components.Common;
using strange.extensions.command.impl;
using System.IO;

namespace CuriiousIQ.Local.CreatorsToolkit.World.WorldHierarchy
{
    public class CommandModuleSelectedWorldHierarchy : EventCommand
    {
        [Inject]
        public IModelActiveWorld WorldModel { get; set; }

        public override void Execute()
        {
            // OLD LOGIC - DISCONNECTING AT MEDIATOR
            MessageData msgData = (MessageData)evt.data;

            msgData.Parameters.Add((byte)EParameterCodeWorld.MODULE_THUMBNAIL_PATH, Path.Combine(Settings.WorldThumbnailPath, "Module.png"));

            dispatcher.Dispatch(EWorldEvent.MODULE_SELECTED, msgData);
        }
    }
}
