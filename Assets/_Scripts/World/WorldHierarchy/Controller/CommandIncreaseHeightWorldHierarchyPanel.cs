﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.WorldHierarchy
{
    public class CommandIncreaseHeightWorldHierarchyPanel : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EWorldHierarchyEvent.INCREASE_PANEL_HEIGHT, evt.data);
        }
    }
}
