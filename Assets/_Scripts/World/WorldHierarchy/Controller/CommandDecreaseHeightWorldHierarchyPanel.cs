﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.WorldHierarchy
{
    public class CommandDecreaseHeightWorldHierarchyPanel : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EWorldHierarchyEvent.DECREASE_PANEL_HEIGHT, evt.data);
        }
    }
}
