﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.WorldHierarchy
{
    public class CommandRepositionWorldHierarchyPanelToRight : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EWorldHierarchyEvent.REPOSITION_PANEL_RIGHT, evt.data);
        }
    }
}
