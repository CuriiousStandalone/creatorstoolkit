﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.WorldHierarchy
{
    public class CommandRepositionWorldHierarchyPanelToBottom : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EWorldHierarchyEvent.REPOSITION_PANEL_BOTTOM, evt.data);
        }
    }
}
