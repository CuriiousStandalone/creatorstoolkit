﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.WorldHierarchy
{
    public class CommandShowWorldHierarchyPanel : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EWorldHierarchyEvent.SHOW_PANEL, evt.data);
        }
    }
}
