﻿namespace CuriiousIQ.Local.CreatorsToolkit.World.WorldHierarchy
{
    public enum EWorldHierarchyEvent
    {
        HIDE_PANEL,
        SHOW_PANEL,

        INCREASE_PANEL_HEIGHT,
        DECREASE_PANEL_HEIGHT,

        REPOSITION_PANEL_RIGHT,
        REPOSITION_PANEL_LEFT,
        REPOSITION_PANEL_TOP,
        REPOSITION_PANEL_BOTTOM,

        RESOURCE_ITEM_DROPPED_INTO_HIERARCHY,

        RESET_WORLD_HIERARCHY,
        RESET_WORLD_HIERARCHY_COMPLETED,
        WORLD_CREATED,

        ITEM_CREATED,
        HOTSPOT_CREATED,

        MODULE_SELECTED,

        //Called when an item is deleted with:
        //EParameterCodeToolkit.ITEM_DATA
        // or
        //EParameterCodeToolkit.ITEM_INDEX
        REMOVE_ITEM,
        REMOVE_HOTSPOT,

        HOTSPOT_WORLD_ITEM_SELECTED,
    }
}
