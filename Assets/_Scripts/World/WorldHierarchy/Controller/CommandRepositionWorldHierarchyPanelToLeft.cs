﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.WorldHierarchy
{
    public class CommandRepositionWorldHierarchyPanelToLeft : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EWorldHierarchyEvent.REPOSITION_PANEL_LEFT, evt.data);
        }
    }
}
