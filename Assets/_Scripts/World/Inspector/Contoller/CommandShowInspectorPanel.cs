﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class CommandShowInspectorPanel : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EInspectorEvent.SHOW_PANEL, evt.data);
        }
    }
}
