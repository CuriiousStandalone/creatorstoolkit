﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class CommandRepositionInspectorPanelToTop : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EInspectorEvent.REPOSITION_PANEL_TOP, evt.data);
        }
    }
}
