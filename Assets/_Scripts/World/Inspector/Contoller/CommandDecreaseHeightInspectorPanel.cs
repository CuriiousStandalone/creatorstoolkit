﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class CommandDecreaseHeightInspectorPanel : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EInspectorEvent.DECREASE_PANEL_HEIGHT, evt.data);
        }
    }
}
