﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.Panorama;
using PulseIQ.Local.Common.Model.PanoramaVideo;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;
using PulseIQ.Local.Common.Model.World;
using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class CommandModuleMultiplayerChanged : EventCommand
    {
        [Inject]
        public IModelActiveWorld model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;
            bool isMultiplayer = (bool)data[(byte)EParameterCodeToolkit.MODULE_IS_MULTIPLAYER];
            if(isMultiplayer)
            {
                model.ActiveWorldData.WorldInteractiveMode = EInteractiveModeCode.TEACHER_LEAD;
            }
            else
            {
                model.ActiveWorldData.WorldInteractiveMode = EInteractiveModeCode.PLAYER_LEAD;
            }

            foreach (CTItemData itemData in model.ActiveWorldData.ListItems)
            {
                switch (itemData.ItemType)
                {
                    case PulseIQ.Local.Common.ItemTypeCode.PANORAMA:
                        {
                            ((CTPanoramaData)itemData).IsMultiplayer = isMultiplayer;
                            break;
                        }

                    case PulseIQ.Local.Common.ItemTypeCode.PANORAMA_VIDEO:
                        {
                            ((CTPanoramaVideoData)itemData).IsMultiplayer = isMultiplayer;
                            break;
                        }

                    case PulseIQ.Local.Common.ItemTypeCode.STEREO_PANORAMA_VIDEO:
                        {
                            ((CTStereoPanoramaVideoData)itemData).IsMultiplayer = isMultiplayer;
                            break;                        
                        }
                }
            }
        }
    }
}