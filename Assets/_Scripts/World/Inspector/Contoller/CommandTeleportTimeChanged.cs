﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.Panorama;
using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class CommandTeleportTimeChanged : EventCommand
    {
        [Inject]
        public IModelActiveWorld model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            int itemIndex = (int)data.Parameters[(byte)EParameterCodeToolkit.ITEM_INDEX];
            int time = (int)data.Parameters[(byte)EParameterCodeToolkit.PANORAMA_TELEPORT_TIME];

            MessageData updatedItemData = new MessageData();

            foreach (CTItemData itemData in model.ActiveWorldData.ListItems)
            {
                if(itemData.ItemIndex == itemIndex)
                {
                    ((CTPanoramaData)itemData).PanoramaTeleportTime = time;
                    updatedItemData.Parameters.Add((byte)EParameterCodeToolkit.ITEM_DATA, itemData);
                    break;
                }
            }

            if (updatedItemData.Parameters.Count > 0)
            {
                dispatcher.Dispatch(EWorldEvent.ITEM_DATA_CHANGED, updatedItemData);
            }
        }
    }
}
