﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class CommandRepositionInspectorPanelToRight : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EInspectorEvent.REPOSITION_PANEL_RIGHT, evt.data);
        }
    }
}
