﻿using strange.extensions.command.impl;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.Panorama;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class CommandAutoTeleportChanged : EventCommand
    {
        [Inject]
        public IModelActiveWorld model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            int itemIndex = (int)data.Parameters[(byte)EParameterCodeToolkit.ITEM_INDEX];

            foreach (CTItemData itemData in model.ActiveWorldData.ListItems)
            {
                if (itemData.ItemIndex == itemIndex)
                {
                    ((CTPanoramaData)itemData).EndBehavior = PulseIQ.Local.Common.Model.Multimedia.EMultimediaEndBehaviorCode.TELEPORT;
                    ((CTPanoramaData)itemData).PanoramaTeleportTime = 0f;
                    break;
                }
            }
        }
    }
}