﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class CommandIncreaseHeightInspectorPanel : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EInspectorEvent.INCREASE_PANEL_HEIGHT, evt.data);
        }
    }
}
