﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.Panorama;
using PulseIQ.Local.Common.Model.PanoramaVideo;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;
using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class CommandTeleportItemChanged : EventCommand
    {
        [Inject]
        public IModelActiveWorld model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            int itemIndex = (int)data.Parameters[(byte)EParameterCodeToolkit.ITEM_INDEX];
            string targetIndexString = (string)data.Parameters[(byte)EParameterCodeToolkit.MEDIA_TELEPORT_ITEM];

            int targetIndex = targetIndexString == "" ? -1 : int.Parse(targetIndexString);

            MessageData updatedItemData = new MessageData();

            foreach (CTItemData itemData in model.ActiveWorldData.ListItems)
            {
                if (itemData.ItemIndex == itemIndex)
                {
                    switch (itemData.ItemType)
                    {
                        case ItemTypeCode.PANORAMA:
                            {
                                ((CTPanoramaData)itemData).TargetMultimediaItemIndex = targetIndex;
                                updatedItemData.Parameters.Add((byte)EParameterCodeToolkit.ITEM_DATA, itemData);
                                break;
                            }

                        case ItemTypeCode.PANORAMA_VIDEO:
                            {
                                ((CTPanoramaVideoData)itemData).TargetMultimediaItemIndex = targetIndex;
                                updatedItemData.Parameters.Add((byte)EParameterCodeToolkit.ITEM_DATA, itemData);
                                break;
                            }

                        case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                            {
                                ((CTStereoPanoramaVideoData)itemData).TargetMultimediaItemIndex = targetIndex;
                                updatedItemData.Parameters.Add((byte)EParameterCodeToolkit.ITEM_DATA, itemData);
                                break;
                            }
                    }
                    break;
                }
            }

            if (updatedItemData.Parameters.Count > 0)
            {
                dispatcher.Dispatch(EWorldEvent.ITEM_DATA_CHANGED, updatedItemData);
            }
        }
    }
}
