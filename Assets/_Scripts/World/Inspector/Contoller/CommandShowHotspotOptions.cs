﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class CommandShowHotspotOptions : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EInspectorEvent.SHOW_HOTSPOT_OPTIONS);
        }
    }
}
