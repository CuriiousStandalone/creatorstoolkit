﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.Item;
using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class CommandItemNameChanged : EventCommand
    {
        [Inject]
        public IModelActiveWorld model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;
            MessageData updatedItemData = new MessageData();

            int itemIndex = (int)data.Parameters[(byte)EParameterCodeToolkit.ITEM_INDEX];
            string itemName = (string)data.Parameters[(byte)EParameterCodeToolkit.ITEM_NAME];

            foreach (CTItemData itemData in model.ActiveWorldData.ListItems)
            {
                if (itemData.ItemIndex == itemIndex)
                {
                    itemData.ItemName = itemName;
                    updatedItemData.Parameters.Add((byte)EParameterCodeToolkit.ITEM_DATA, itemData);
                    break;
                }
            }

            if (updatedItemData.Parameters.Count > 0)
            {
                dispatcher.Dispatch(EWorldEvent.ITEM_DATA_CHANGED, updatedItemData);
            }
        }
    }
}