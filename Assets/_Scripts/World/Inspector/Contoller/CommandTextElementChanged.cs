﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.World.Static;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.Common.Model.Hotspot.HotspotElement;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.Panorama;
using PulseIQ.Local.Common.Model.PanoramaVideo;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;
using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class CommandTextElementChanged : EventCommand
    {
        [Inject]
        public IModelActiveWorld model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;
            int itemIndex = (int)data[(byte)EParameterCodeToolkit.ITEM_INDEX];
            string hotspotID = (string)data[(byte)EParameterCodeToolkit.HOTSPOT_ID];
            int elementID = (int)data[(byte)EParameterCodeToolkit.HOTSPOT_ELEMENT_ID];

            string text = (string)data[(byte)EParameterCodeToolkit.ELEMENT_TEXT];

            foreach (CTItemData itemData in model.ActiveWorldData.ListItems)
            {
                if (itemIndex == itemData.ItemIndex)
                {
                    switch (itemData.ItemType)
                    {
                        case PulseIQ.Local.Common.ItemTypeCode.PANORAMA:
                            {
                                foreach (CTHotspotData hotspotData in ((CTPanoramaData)itemData).ListHotspot)
                                {
                                    if (hotspotID == hotspotData.HotspotId)
                                    {
                                        ((CTHotspotElementTextData)hotspotData.HotspotBoxes[0].Elements[elementID]).Text = text;
                                        data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ELEMENT_DATA, hotspotData.HotspotBoxes[0].Elements[elementID]);
                                    }
                                }

                                break;
                            }

                        case PulseIQ.Local.Common.ItemTypeCode.PANORAMA_VIDEO:
                            {
                                foreach (CTHotspotData hotspotData in ((CTPanoramaVideoData)itemData).ListHotspot)
                                {
                                    if (hotspotID == hotspotData.HotspotId)
                                    {
                                        ((CTHotspotElementTextData)hotspotData.HotspotBoxes[0].Elements[elementID]).Text = text;
                                        data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ELEMENT_DATA, hotspotData.HotspotBoxes[0].Elements[elementID]);
                                    }
                                }

                                break;
                            }

                        case PulseIQ.Local.Common.ItemTypeCode.STEREO_PANORAMA_VIDEO:
                            {
                                foreach (CTHotspotData hotspotData in ((CTStereoPanoramaVideoData)itemData).ListHotspot)
                                {
                                    if (hotspotID == hotspotData.HotspotId)
                                    {
                                        ((CTHotspotElementTextData)hotspotData.HotspotBoxes[0].Elements[elementID]).Text = text;
                                        data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ELEMENT_DATA, hotspotData.HotspotBoxes[0].Elements[elementID]);
                                    }
                                }

                                break;
                            }
                    }
                }
            }

            dispatcher.Dispatch(EStaticEvent.UPDATE_HOTSPOT_ELEMENT, data);
        }
    }
}