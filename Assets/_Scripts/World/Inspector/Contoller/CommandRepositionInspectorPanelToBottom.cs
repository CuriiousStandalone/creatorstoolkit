﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class CommandRepositionInspectorPanelToBottom : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EInspectorEvent.REPOSITION_PANEL_BOTTOM, evt.data);
        }
    }
}
