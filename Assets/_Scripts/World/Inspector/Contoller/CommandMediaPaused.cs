﻿using CuriiousIQ.Local.CreatorsToolkit.World.Static;
using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class CommandMediaPaused : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EStaticEvent.MEDIA_PAUSE, evt.data);
        }
    }
}