﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.Common.Model.Hotspot.HotspotElement;
using PulseIQ.Local.Common.Model.Hotspot.HotspotTemplate;
using strange.extensions.command.impl;
using System.Collections.Generic;
using UnityEngine;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandFindDefaultIconIds : EventCommand
    {
        private enum ELEMENT_TYPE
        {
            TELEPORT,
            AUDIO,
            TEXT,
            IMAGE,
            INFO_PANEL,
        }

        List<HotspotTemplateData> hotspotTemplateDataList = HotspotTemplateManager.Instance.HotspotTemplateTypes.Templates;
        string defaultID = "";
        string hoverID = "";
        string triggerID = "";


        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;
            CTHotspotData hotspotData = (CTHotspotData)data[(byte)EParameterCodeToolkit.HOTSPOT_DATA];

            ELEMENT_TYPE elementType = ELEMENT_TYPE.AUDIO;

            if (hotspotData.HotspotBoxes.Count == 0)
            {
                elementType = ELEMENT_TYPE.TELEPORT;
            }
            else if (hotspotData.HotspotBoxes[0].Elements.Count == 1)
            {
                switch (hotspotData.HotspotBoxes[0].Elements[0].TypeCode)
                {
                    case EHotspotElementTypeCode.AUDIO:
                        {
                            elementType = ELEMENT_TYPE.AUDIO;
                            break;
                        }

                    case EHotspotElementTypeCode.IMAGE:
                        {
                            elementType = ELEMENT_TYPE.IMAGE;
                            break;
                        }

                    case EHotspotElementTypeCode.TEXT:
                        {
                            elementType = ELEMENT_TYPE.TEXT;
                            break;
                        }
                }
            }
            else
            {
                elementType = ELEMENT_TYPE.INFO_PANEL;
            }

            foreach(HotspotTemplateData template in hotspotTemplateDataList)
            {
                if (template.HotspotData.HotspotBoxes.Count == 0)
                {
                    if(elementType == ELEMENT_TYPE.TELEPORT)
                    {
                        List<string> defaultIconList = new List<string>();

                        foreach (CTHotspotStatusData status in template.HotspotData.ListHotspotStatus)
                        {
                            if (status.StatusTypeCode == EHotspotStatusTypeCode.DEFAULT)
                            {
                                defaultID = status.StatusIconId;
                            }
                            else if (status.StatusTypeCode == EHotspotStatusTypeCode.HOVER)
                            {
                                hoverID = status.StatusIconId;
                            }
                            else if (status.StatusTypeCode == EHotspotStatusTypeCode.TRIGGER)
                            {
                                triggerID = status.StatusIconId;
                            }
                        }

                        defaultIconList.Add(defaultID);
                        defaultIconList.Add(hoverID);
                        defaultIconList.Add(triggerID);

                        MessageData sendData = new MessageData();
                        sendData.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ICON_ID, defaultIconList);
                        dispatcher.Dispatch(EWorldEvent.DEFAULT_STATUS_ICON_IDS, sendData);

                        return;
                    }
                }
                else if (template.HotspotData.HotspotBoxes[0].Elements.Count == 1)
                {
                    switch (template.HotspotData.HotspotBoxes[0].Elements[0].TypeCode)
                    {
                        case EHotspotElementTypeCode.AUDIO:
                            {
                                if(elementType == ELEMENT_TYPE.AUDIO)
                                {
                                    List<string> defaultIconList = new List<string>();

                                    foreach(CTHotspotStatusData status in template.HotspotData.ListHotspotStatus)
                                    {
                                        if(status.StatusTypeCode == EHotspotStatusTypeCode.DEFAULT)
                                        {
                                            defaultID = status.StatusIconId;
                                        }
                                        else if (status.StatusTypeCode == EHotspotStatusTypeCode.HOVER)
                                        {
                                            hoverID = status.StatusIconId;
                                        }
                                        else if (status.StatusTypeCode == EHotspotStatusTypeCode.TRIGGER)
                                        {
                                            triggerID = status.StatusIconId;
                                        }
                                    }

                                    defaultIconList.Add(defaultID);
                                    defaultIconList.Add(hoverID);
                                    defaultIconList.Add(triggerID);

                                    MessageData sendData = new MessageData();
                                    sendData.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ICON_ID, defaultIconList);
                                    dispatcher.Dispatch(EWorldEvent.DEFAULT_STATUS_ICON_IDS, sendData);

                                    return;
                                }
                                break;
                            }

                        case EHotspotElementTypeCode.IMAGE:
                            {
                                if (elementType == ELEMENT_TYPE.IMAGE)
                                {
                                    List<string> defaultIconList = new List<string>();

                                    foreach (CTHotspotStatusData status in template.HotspotData.ListHotspotStatus)
                                    {
                                        if (status.StatusTypeCode == EHotspotStatusTypeCode.DEFAULT)
                                        {
                                            defaultID = status.StatusIconId;
                                        }
                                        else if (status.StatusTypeCode == EHotspotStatusTypeCode.HOVER)
                                        {
                                            hoverID = status.StatusIconId;
                                        }
                                        else if (status.StatusTypeCode == EHotspotStatusTypeCode.TRIGGER)
                                        {
                                            triggerID = status.StatusIconId;
                                        }
                                    }

                                    defaultIconList.Add(defaultID);
                                    defaultIconList.Add(hoverID);
                                    defaultIconList.Add(triggerID);

                                    MessageData sendData = new MessageData();
                                    sendData.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ICON_ID, defaultIconList);
                                    dispatcher.Dispatch(EWorldEvent.DEFAULT_STATUS_ICON_IDS, sendData);

                                    return;
                                }
                                break;
                            }

                        case EHotspotElementTypeCode.TEXT:
                            {
                                if (elementType == ELEMENT_TYPE.TEXT)
                                {
                                    List<string> defaultIconList = new List<string>();

                                    foreach (CTHotspotStatusData status in template.HotspotData.ListHotspotStatus)
                                    {
                                        if (status.StatusTypeCode == EHotspotStatusTypeCode.DEFAULT)
                                        {
                                            defaultID = status.StatusIconId;
                                        }
                                        else if (status.StatusTypeCode == EHotspotStatusTypeCode.HOVER)
                                        {
                                            hoverID = status.StatusIconId;
                                        }
                                        else if (status.StatusTypeCode == EHotspotStatusTypeCode.TRIGGER)
                                        {
                                            triggerID = status.StatusIconId;
                                        }
                                    }

                                    defaultIconList.Add(defaultID);
                                    defaultIconList.Add(hoverID);
                                    defaultIconList.Add(triggerID);

                                    MessageData sendData = new MessageData();
                                    sendData.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ICON_ID, defaultIconList);
                                    dispatcher.Dispatch(EWorldEvent.DEFAULT_STATUS_ICON_IDS, sendData);

                                    return;
                                }
                                break;
                            }
                    }
                }
                else
                {
                    if (elementType == ELEMENT_TYPE.INFO_PANEL)
                    {
                        List<string> defaultIconList = new List<string>();

                        foreach (CTHotspotStatusData status in template.HotspotData.ListHotspotStatus)
                        {
                            if (status.StatusTypeCode == EHotspotStatusTypeCode.DEFAULT)
                            {
                                defaultID = status.StatusIconId;
                            }
                            else if (status.StatusTypeCode == EHotspotStatusTypeCode.HOVER)
                            {
                                hoverID = status.StatusIconId;
                            }
                            else if (status.StatusTypeCode == EHotspotStatusTypeCode.TRIGGER)
                            {
                                triggerID = status.StatusIconId;
                            }
                        }

                        defaultIconList.Add(defaultID);
                        defaultIconList.Add(hoverID);
                        defaultIconList.Add(triggerID);

                        MessageData sendData = new MessageData();
                        sendData.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ICON_ID, defaultIconList);
                        dispatcher.Dispatch(EWorldEvent.DEFAULT_STATUS_ICON_IDS, sendData);

                        return;
                    }
                }
            }
        }
    }
}