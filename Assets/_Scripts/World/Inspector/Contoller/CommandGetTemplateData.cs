﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.World.Inspector;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.PostProcessing;
using PulseIQ.Local.Common.Model.Skybox;
using PulseIQ.Local.Common.Model.World;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.api;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandGetTemplateData : EventCommand
    {
        [Inject]
        public IModelActiveWorld ActiveWorldModel { get; set; }

        [Inject]
        public ILocalDirService LocalDirService { get; set; }

        [Inject]
        public IModuleTemplateData ModuleTemplateModel { get; set; }

        private List<WorldData> _templateWorldDataList;

        public override void Execute()
        {
            Retain();

            UpdateListeners(true);

            _templateWorldDataList = new List<WorldData>(ModuleTemplateModel.TemplateList);

            LocalDirService.GetTemplateIds();
        }

        private void UpdateListeners(bool value)
        {
            LocalDirService.dispatcher.UpdateListener(value, EWorldEvent.TEMPLATE_PATHS_LOADED, OnTemplatePathsLoaded);
        }

        private void OnTemplatePathsLoaded(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            List<string> pathList = (List<string>)msgData[(byte)EParameterCodeWorld.TEMPLATE_PATH_LIST];

            // Hard coded stream list to load template data from streaming assets folder

            List<FileStream> streamList = new List<FileStream>();

            for(int i = 1; i < 4; ++i)
            {
                FileStream stream = new FileStream(Settings.StreamingAssetsPath + "/Template_" + i + ".ctk", FileMode.Open, FileAccess.Read);
                streamList.Add(stream);
            }

            foreach (var path in streamList)
            {
                WorldDataSerializer.ReadDataByFullPath(path, out WorldDataXML worldDataXml);
                WorldData worldData = new WorldData(worldDataXml);

                _templateWorldDataList.Add(worldData);
            }

            ModuleTemplateModel.TemplateList = _templateWorldDataList;

            MessageData resultData = new MessageData();
            resultData.Parameters.Add((byte)EParameterCodeWorld.TEMPLATE_DATA_LIST, _templateWorldDataList);

            dispatcher.Dispatch(EInspectorEvent.TEMPLATE_DATA_LOADED, resultData);
            
            //////////////////////////////////////////////////////////////////////////////////////////////////
            // Temp for setting template data into current active world. Will need to move with world creation
            ///////////////////////////////////////////////////////////////////////////
            foreach (WorldData wData in ModuleTemplateModel.TemplateList)
            {
                if (wData.WorldId == ActiveWorldModel.ActiveWorldData.WorldId)
                {
                    ActiveWorldModel.ActiveWorldData.ListItems = new List<CTItemData>(wData.ListItems);
                    ActiveWorldModel.ActiveWorldData.ListPostProcessing = new List<PostProcessingData>(wData.ListPostProcessing);
                    ActiveWorldModel.ActiveWorldData.ListSkybox = new List<SkyboxData>(wData.ListSkybox);

                    foreach (CTItemData mediaData in ActiveWorldModel.MediaItemsInCurrentWorld)
                    {
                        int currentMediaIndex = mediaData.ItemIndex;
                        int newMediaIndex = ActiveWorldModel.ActiveWorldData.ListItems.Max(x => x.ItemIndex) + 1;

                        mediaData.ItemIndex = newMediaIndex;

                        ActiveWorldModel.ActiveWorldData.ListItems.Add(mediaData);
                    }

                    break;
                }
            }
        }
    }
}
