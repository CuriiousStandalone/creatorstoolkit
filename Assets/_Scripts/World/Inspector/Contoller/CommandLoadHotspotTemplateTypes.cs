﻿using System.Collections.Generic;

using UnityEngine;

using strange.extensions.command.impl;

using PulseIQ.Local.Common.Model.Hotspot.HotspotTemplate;
using PulseIQ.Local.ClientCommon.Components.Common;
using System.IO;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class CommandLoadHotspotTemplateTypes : EventCommand
    {
        public override void Execute()
        { 
            List<HotspotTemplateData> hotspotTemplateDataList = HotspotTemplateManager.Instance.HotspotTemplateTypes.Templates;
            FileStream stream = new FileStream(Settings.StreamingAssetsPath + "/HotspotTemplateTypes.ctk", FileMode.Open, FileAccess.Read);
            HotspotTemplateManager.Instance.UpdateTemplateTypes(stream);
            hotspotTemplateDataList = HotspotTemplateManager.Instance.HotspotTemplateTypes.Templates;

            MessageData msgData = new MessageData();

            msgData.Parameters.Add((byte)EParameterCodeWorld.HOTSPOT_TEMPLATE_DATA_LIST, hotspotTemplateDataList);

            dispatcher.Dispatch(EInspectorEvent.HOTSPOT_TEMPLATE_TYPES_LOADED, msgData);
        }
    }
}
