﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;
using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class CommandVideoTypeChanged : EventCommand
    {
        [Inject]
        public IModelActiveWorld model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            int itemIndex = (int)data.Parameters[(byte)EParameterCodeToolkit.ITEM_INDEX];
            EStereoPanoramaVideoTypeCode videoType = (EStereoPanoramaVideoTypeCode)data.Parameters[(byte)EParameterCodeToolkit.STEREO_VIDEO_TYPE];

            MessageData updatedItemData = new MessageData();

            foreach (CTItemData itemData in model.ActiveWorldData.ListItems)
            {
                if (itemData.ItemIndex == itemIndex)
                {
                    if (itemData.ItemType == ItemTypeCode.STEREO_PANORAMA_VIDEO)
                    {
                        ((CTStereoPanoramaVideoData)itemData).StereoPanoramaVideoType = videoType;
                        updatedItemData.Parameters.Add((byte)EParameterCodeToolkit.ITEM_DATA, itemData);
                    }
                }
            }

            if (updatedItemData.Parameters.Count > 0)
            {
                dispatcher.Dispatch(EWorldEvent.ITEM_DATA_CHANGED, updatedItemData);
            }
        }
    }
}
