﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class CommandHideInspectorPanel : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EInspectorEvent.HIDE_PANEL, evt.data);
        }
    }
}
