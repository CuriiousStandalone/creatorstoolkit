﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.Multimedia;
using PulseIQ.Local.Common.Model.Panorama;
using PulseIQ.Local.Common.Model.PanoramaVideo;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;
using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class CommandHotspotDeletePressed : EventCommand
    {
        [Inject]
        public IModelActiveWorld model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;
            int index = (int)data[(byte)EParameterCodeToolkit.ITEM_INDEX];
            string hotspotID = (string)data[(byte)EParameterCodeToolkit.HOTSPOT_ID];

            MessageData newData = new MessageData();
            newData.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, index);
            newData.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ID, hotspotID);

            foreach (CTItemData item in model.ActiveWorldData.ListItems)
            {
                if (item.ItemIndex == index)
                {
                    //newData.Parameters.Add((byte)EParameterCodeToolkit.MULTIMEDIA_ID, item.GetMultimediaID(item.GetItemType()));

                    switch (item.ItemType)
                    {
                        case ItemTypeCode.PANORAMA:
                            {
                                for (int i = 0; i < ((CTPanoramaData)item).ListHotspot.Count; ++i)
                                {
                                    if (((CTPanoramaData)item).ListHotspot[i].HotspotId == hotspotID)
                                    {
                                        ((CTPanoramaData)item).ListHotspot.RemoveAt(i);
                                    }
                                }
                                break;
                            }

                        case ItemTypeCode.PANORAMA_VIDEO:
                            {
                                for (int i = 0; i < ((CTPanoramaVideoData)item).ListHotspot.Count; ++i)
                                {
                                    if (((CTPanoramaVideoData)item).ListHotspot[i].HotspotId == hotspotID)
                                    {
                                        ((CTPanoramaVideoData)item).ListHotspot.RemoveAt(i);
                                    }
                                }
                                break;
                            }

                        case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                            {
                                for (int i = 0; i < ((CTStereoPanoramaVideoData)item).ListHotspot.Count; ++i)
                                {
                                    if (((CTStereoPanoramaVideoData)item).ListHotspot[i].HotspotId == hotspotID)
                                    {
                                        ((CTStereoPanoramaVideoData)item).ListHotspot.RemoveAt(i);
                                    }
                                }
                                break;
                            }
                    }
                }
            }

            //dispatcher.Dispatch(EEventToolkitCrossContext.MEDIA_HIERARCHY_HOTSPOT_ITEM_REMOVED, newData);
            //dispatcher.Dispatch(EEventStatic.HOTSPOT_DELETED, evt.data);
        }
    }
}

