﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class CommandModuleNameUpdated : EventCommand
    {
        [Inject]
        public IModelActiveWorld model { get; set; }
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;
            string name = (string)data[(byte)EParameterCodeToolkit.MODULE_NAME];

            model.ActiveWorldData.WorldName = name;

            dispatcher.Dispatch(EEventToolkitCrossContext.UPDATED_MODULE_NAME, data);
        }
    }
}