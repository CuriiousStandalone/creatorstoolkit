﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class CommandModuleDescriptionUpdated : EventCommand
    {
        [Inject]
        public IModelActiveWorld model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;
            string desc = (string)data[(byte)EParameterCodeToolkit.MODULE_DESCRIPTION];

            model.ActiveWorldData.WorldDescription = desc;
        }
    }
}