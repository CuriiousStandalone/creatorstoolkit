﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.Panorama;
using PulseIQ.Local.Common.Model.PanoramaVideo;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;
using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class CommandHotspotNameChanged : EventCommand
    {
        [Inject]
        public IModelActiveWorld model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            int itemIndex = (int)data.Parameters[(byte)EParameterCodeToolkit.ITEM_INDEX];
            string hotspotID = (string)data.Parameters[(byte)EParameterCodeToolkit.HOTSPOT_INDEX];

            string name = (string)data.Parameters[(byte)EParameterCodeToolkit.HOTSPOT_NAME];

            MessageData updatedItemData = new MessageData();
            MessageData updatedHotspotData = new MessageData();

            foreach (CTItemData itemData in model.ActiveWorldData.ListItems)
            {
                if (itemData.ItemIndex == itemIndex)
                {
                    switch (itemData.ItemType)
                    {
                        case ItemTypeCode.PANORAMA:
                            {
                                foreach (CTHotspotData hotspotData in ((CTPanoramaData)itemData).ListHotspot)
                                {
                                    if (hotspotData.HotspotId == hotspotID)
                                    {
                                        hotspotData.HotspotName = name;
                                        updatedItemData.Parameters.Add((byte)EParameterCodeToolkit.ITEM_DATA, itemData);
                                        updatedHotspotData.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_DATA, hotspotData);
                                        break;
                                    }
                                }
                                break;
                            }

                        case ItemTypeCode.PANORAMA_VIDEO:
                            {
                                foreach (CTHotspotData hotspotData in ((CTPanoramaVideoData)itemData).ListHotspot)
                                {
                                    if (hotspotData.HotspotId == hotspotID)
                                    {
                                        hotspotData.HotspotName = name;
                                        updatedItemData.Parameters.Add((byte)EParameterCodeToolkit.ITEM_DATA, itemData);
                                        updatedHotspotData.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_DATA, hotspotData);
                                        break;
                                    }
                                }
                                break;
                            }

                        case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                            {
                                foreach (CTHotspotData hotspotData in ((CTStereoPanoramaVideoData)itemData).ListHotspot)
                                {
                                    if (hotspotData.HotspotId == hotspotID)
                                    {
                                        hotspotData.HotspotName = name;
                                        updatedItemData.Parameters.Add((byte)EParameterCodeToolkit.ITEM_DATA, itemData);
                                        updatedHotspotData.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_DATA, hotspotData);
                                        break;
                                    }
                                }
                                break;
                            }
                    }
                    break;
                }
            }

            if (updatedItemData.Parameters.Count > 0 && updatedHotspotData.Parameters.Count > 0)
            {
                dispatcher.Dispatch(EWorldEvent.ITEM_DATA_CHANGED, updatedItemData);
                dispatcher.Dispatch(EWorldEvent.HOTSPOT_DATA_CHANGED, updatedHotspotData);
            }
        }
    }
}