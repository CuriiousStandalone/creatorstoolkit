﻿using CuriiousIQ.Local.CreatorsToolkit.World.Static;
using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class CommandMediaSeeked : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EStaticEvent.MEDIA_SEEK, evt.data);
        }
    }
}