﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class CommandRepositionInspectorPanelToLeft : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EInspectorEvent.REPOSITION_PANEL_LEFT, evt.data);
        }
    }
}
