﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class CommandShowFilteredList : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.SHOW_FILTERED_LIST, evt.data);
        }
    }
}