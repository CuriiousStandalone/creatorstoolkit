﻿using PulseIQ.Local.ClientCommon.Components.Common;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class MediatorPanoramaManager : EventMediator
    {
        [Inject]
        public ViewPanoramaManager view { get; set; }

        public override void OnRegister()
        {
            view.init();
            UpdateListeners(true);
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewPanoramaManager.PANORAMA_EVENTS.AUTO_TELEPORT_CHANGED, OnAutoTeleportChanged);
            view.dispatcher.UpdateListener(value, ViewPanoramaManager.PANORAMA_EVENTS.NAME_CHANGED, OnNameChanged);
            view.dispatcher.UpdateListener(value, ViewPanoramaManager.PANORAMA_EVENTS.TELEPORT_ITEM_CHANGED, OnTeleportItemChanged);
            view.dispatcher.UpdateListener(value, ViewPanoramaManager.PANORAMA_EVENTS.TELEPORT_TIME_CHANGED, OnTeleportTimeChanged);
            view.dispatcher.UpdateListener(value, ViewPanoramaManager.PANORAMA_EVENTS.TELEPORT_ITEM_CREATED, OnCreateTeleportItem);

            dispatcher.UpdateListener(value, EWorldEvent.TELEPORT_ITEM_CREATED, OnTeleportItemCreated);
        }

        private void OnCreateTeleportItem(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.CREATE_TELEPORT_ITEM, evt.data);
        }

        private void OnAutoTeleportChanged(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.PANORAMA_AUTO_TELEPORT_CHANGED, evt.data);
        }

        private void OnNameChanged(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.ITEM_NAME_CHANGED, evt.data);
        }

        private void OnTeleportItemChanged(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.MEDIA_TELEPORT_ITEM_CHANGED, evt.data);
        }

        private void OnTeleportTimeChanged(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.PANORAMA_TELEPORT_TIME_CHANGED, evt.data);
        }

        private void OnTeleportItemCreated(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;
            view.TeleportItemCreated(data);
        }
    }
}