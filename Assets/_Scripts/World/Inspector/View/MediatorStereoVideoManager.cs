﻿using PulseIQ.Local.ClientCommon.Components.Common;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class MediatorStereoVideoManager : EventMediator
    {
        [Inject]
        public ViewStereoVideoManager view { get; set; }

        public override void OnRegister()
        {
            view.init();
            UpdateListeners(true);
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewStereoVideoManager.STEREO_EVENTS.END_BEHAVIOUR_CHANGED, OnEndBehaviourChanged);
            view.dispatcher.UpdateListener(value, ViewStereoVideoManager.STEREO_EVENTS.NAME_CHANGED, OnNameChanged);
            view.dispatcher.UpdateListener(value, ViewStereoVideoManager.STEREO_EVENTS.PAUSE_PRESSED, OnPausePressed);
            view.dispatcher.UpdateListener(value, ViewStereoVideoManager.STEREO_EVENTS.PLAY_PRESSED, OnPlayPressed);
            view.dispatcher.UpdateListener(value, ViewStereoVideoManager.STEREO_EVENTS.REPLAY_PRESSED, OnReplayPressed);
            view.dispatcher.UpdateListener(value, ViewStereoVideoManager.STEREO_EVENTS.STOP_PRESSED, OnStopPressed);
            view.dispatcher.UpdateListener(value, ViewStereoVideoManager.STEREO_EVENTS.TELEPORT_ITEM_CHANGED, OnTeleportItemChanged);
            view.dispatcher.UpdateListener(value, ViewStereoVideoManager.STEREO_EVENTS.VIDEO_TIME_CHANGED, OnVideoTimeChanged);
            view.dispatcher.UpdateListener(value, ViewStereoVideoManager.STEREO_EVENTS.VIDEO_TYPE_CHANGED, OnVideoTypeChanged);
            view.dispatcher.UpdateListener(value, ViewStereoVideoManager.STEREO_EVENTS.TELEPORT_ITEM_CREATED, OnCreateTeleportItem);

            dispatcher.UpdateListener(value, EInspectorEvent.MEDIA_VIDEO_LOADED, OnMediaVideoLoaded);
            dispatcher.UpdateListener(value, EInspectorEvent.MEDIA_VIDEO_CURRENT_TIME, OnUpdateMediaCurrentTime);
            dispatcher.UpdateListener(value, EWorldEvent.TELEPORT_ITEM_CREATED, OnTeleportItemCreated);
        }

        #region Outbound
        private void OnEndBehaviourChanged(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.MEDIA_END_BEHAVIOUR_CHANGED, evt.data);
        }

        private void OnNameChanged(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.ITEM_NAME_CHANGED, evt.data);
        }

        private void OnPausePressed(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.MEDIA_PAUSE_PRESSED, evt.data);
        }

        private void OnPlayPressed(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.MEDIA_PLAY_PRESSED, evt.data);
        }

        private void OnReplayPressed(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.MEDIA_REPLAY_PRESSED, evt.data);
        }

        private void OnStopPressed(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.MEDIA_STOP_PRESSED, evt.data);
        }

        private void OnTeleportItemChanged(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.MEDIA_TELEPORT_ITEM_CHANGED, evt.data);
        }

        private void OnVideoTimeChanged(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.MEDIA_VIDEO_FRAME_CHANGED, evt.data);
        }

        private void OnVideoTypeChanged(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.STEREO_VIDEO_TYPE_CHANGED, evt.data);
        }

        private void OnCreateTeleportItem(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.CREATE_TELEPORT_ITEM, evt.data);
        }
        #endregion

        #region Inbound

        private void OnMediaVideoLoaded(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;

            view.SetVideoDuriation(data);
        }

        private void OnUpdateMediaCurrentTime(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;

            view.SetCurrentVideoTime(data);
        }

        private void OnTeleportItemCreated(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;
            view.TeleportItemCreated(data);
        }

        #endregion
    }
}