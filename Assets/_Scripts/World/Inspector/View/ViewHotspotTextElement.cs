﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using strange.extensions.mediation.impl;
using PulseIQ.Local.Common.Model.Hotspot;
using TMPro;
using UnityEngine;
using PulseIQ.Local.Common.Model.Hotspot.HotspotElement;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class ViewHotspotTextElement : EventView
    {
        internal enum HOTSPOT_TEXT_EVENTS
        {
            TITLE_TEXT_CHANGED,
        }

        [SerializeField]
        private TMP_InputField _titleText;

        private int _itemIndex;
        private string _hotspotID;
        private string _elementID;

        internal void init()
        {

        }

        public void SetElementData(CTHotspotElementTextData data, int itemIndex, string hotspotID)
        {
            _itemIndex = itemIndex;
            _hotspotID = hotspotID;
            _titleText.SetTextWithoutNotify(data.Text);
        }

        public void HotspotTitleTextChanged()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ID, _hotspotID);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ELEMENT_ID, 0);
            data.Parameters.Add((byte)EParameterCodeToolkit.ELEMENT_TEXT, _titleText.text);
            data.Parameters.Add((byte)EParameterCodeToolkit.ELEMENT_TYPE, EHotspotElementTypeCode.TEXT);

            dispatcher.Dispatch(HOTSPOT_TEXT_EVENTS.TITLE_TEXT_CHANGED, data);
        }
    }
}