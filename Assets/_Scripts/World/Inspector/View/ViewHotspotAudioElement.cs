﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.Common.Model.Hotspot.HotspotElement;
using strange.extensions.mediation.impl;
using System.IO;
using TMPro;
using UnityEngine;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class ViewHotspotAudioElement : EventView
    {
        internal enum HOTSPOT_AUDIO_EVENT
        {
            AUDIO_CHANGED,
            OPEN_ICON_RESOURCE_LIST,
        }

        [SerializeField]
        private CDropBox _audioDropBox;

        private int _itemIndex;
        private string _hotspotID;

        internal void init()
        {

        }

        public void SetElementData(CTHotspotElementAudioData data, int itemIndex, string hotspotID)
        {           
            _itemIndex = itemIndex;
            _hotspotID = hotspotID;

            if (data.MultimediaId != "")
            {
                _audioDropBox.FillData(data.MultimediaId, Path.GetFileNameWithoutExtension(data.MultimediaId));
            }
        }

        public void HotspotAudioElementChanged(string mediaID)
        {
            if (gameObject.activeInHierarchy)
            {
                MessageData data = new MessageData();
                data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
                data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ID, _hotspotID);
                data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ELEMENT_ID, 0);
                data.Parameters.Add((byte)EParameterCodeToolkit.ELEMENT_MEDIA_ID, mediaID);
                data.Parameters.Add((byte)EParameterCodeToolkit.ELEMENT_TYPE, EHotspotElementTypeCode.AUDIO);

                dispatcher.Dispatch(HOTSPOT_AUDIO_EVENT.AUDIO_CHANGED, data);
            }
        }

        public void HotspotAudioElementCleared()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ID, _hotspotID);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ELEMENT_ID, 0);
            data.Parameters.Add((byte)EParameterCodeToolkit.ELEMENT_MEDIA_ID, "");
            data.Parameters.Add((byte)EParameterCodeToolkit.ELEMENT_TYPE, EHotspotElementTypeCode.AUDIO);

            dispatcher.Dispatch(HOTSPOT_AUDIO_EVENT.AUDIO_CHANGED, data);
        }

        public void OpenHotspotIconList()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.RESOURCE_LIST_FILTER, "Audio");
            dispatcher.Dispatch(HOTSPOT_AUDIO_EVENT.OPEN_ICON_RESOURCE_LIST, data);
        }
    }
}