﻿using strange.extensions.mediation.impl;
using strange.extensions.dispatcher.eventdispatcher.api;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.World;
using PulseIQ.Local.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class MediatorInspectorManager : EventMediator
    {
        [Inject]
        public ViewInspectorManager view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.Init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewInspectorManager.VIEW_LOADED, OnViewLoaded);
            view.dispatcher.UpdateListener(value, ViewInspectorManager.PANEL_HIDDEN, OnPanelHidden);
            view.dispatcher.UpdateListener(value, ViewInspectorManager.PANEL_SHOWN, OnPanelShown);

            dispatcher.UpdateListener(value, EInspectorEvent.HIDE_PANEL, OnHidePanel);
            dispatcher.UpdateListener(value, EInspectorEvent.SHOW_PANEL, OnShowPanel);

            dispatcher.UpdateListener(value, EInspectorEvent.INCREASE_PANEL_HEIGHT, OnIncreasePanelHeight);
            dispatcher.UpdateListener(value, EInspectorEvent.DECREASE_PANEL_HEIGHT, OnDecreasePanelHeight);

            dispatcher.UpdateListener(value, EInspectorEvent.SHOW_HOTSPOT_OPTIONS, OnShowHotspotOptions);
            dispatcher.UpdateListener(value, EInspectorEvent.RESET_INSPECTOR, OnResetInspector);
            
            dispatcher.UpdateListener(value, EWorldEvent.SELECTED_ITEM_PROCESSED, OnItemSelected);
            dispatcher.UpdateListener(value, EWorldEvent.HOTSPOT_SELECTED, OnHotSpotSelected);
            dispatcher.UpdateListener(value, EWorldEvent.MODULE_SELECTED, OnModuleSelected);
        }

        private void OnResetInspector()
        {
            view.ResetInspector();

            dispatcher.Dispatch(EInspectorEvent.RESET_INSPECTOR_COMPLETED);
        }

        private void OnModuleSelected(IEvent payload)
        {
            MessageData msgData = (MessageData)payload.data;
            WorldData worldData = (WorldData)msgData.Parameters[(byte)EParameterCodeWorld.WORLD_DATA];
            view.ModuleSelected(worldData);
        }

        private void OnHotSpotSelected(IEvent payload)
        {
            MessageData msgData = (MessageData)payload.data;
            CTHotspotData itemData = (CTHotspotData)msgData.Parameters[(byte)EParameterCodeToolkit.HOTSPOT_DATA];
            int itemIndex = (int)msgData.Parameters[(byte)EParameterCodeToolkit.ITEM_INDEX];
            ItemTypeCode itemType = (ItemTypeCode)msgData.Parameters[(byte)EParameterCodeToolkit.ITEM_TYPE];
            string teleportItemName = (string)msgData.Parameters[(byte)EParameterCodeToolkit.MEDIA_TELEPORT_ITEM];
            view.HotspotSelected(itemData, itemIndex, itemType, teleportItemName);
        }

        private void OnItemSelected(IEvent payload)
        {
            MessageData msgData = (MessageData)payload.data;

            view.ItemSelected(msgData);
        }

        private void OnShowHotspotOptions()
        {
            view.ActivateHotspotOptions();
        }

        #region Panel

        private void OnViewLoaded(IEvent evt)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.PANEL_LOADED, evt.data);
        }

        private void OnHidePanel(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;
            string panelId = (string)msgData[(byte)EParameterCodeToolkit.PANEL_ID];

            if (panelId == view.PanelId)
            {
                view.HidePanel();
            }
        }

        private void OnShowPanel(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;
            string panelId = (string)msgData[(byte)EParameterCodeToolkit.PANEL_ID];

            if (panelId == view.PanelId)
            {
                view.ShowPanel();
            }
        }

        private void OnPanelHidden(IEvent evt)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.PANEL_HIDDEN, evt.data);
        }

        private void OnPanelShown(IEvent evt)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.PANEL_SHOWN, evt.data);
        }

        private void OnIncreasePanelHeight(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            string panelId = (string)msgData[(byte)EParameterCodeToolkit.PANEL_ID];

            if(panelId == view.PanelId)
            {
                float height = (float)msgData[(byte)EParameterCodeToolkit.PANEL_HEIGHT];
                EAnchor anchor = (EAnchor)msgData[(byte)EParameterCodeToolkit.PANEL_ANCHOR];

                if (anchor == EAnchor.TOP)
                {
                    view.IncreaseHeightFromBottom(height);
                }
                else if(anchor == EAnchor.BOTTOM)
                {
                    view.IncreaseHeightFromTop(height);
                }
                else
                {
                    view.IncreaseHeight(height);
                }
            }
        }

        private void OnDecreasePanelHeight(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            string panelId = (string)msgData[(byte)EParameterCodeToolkit.PANEL_ID];

            if (panelId == view.PanelId)
            {
                float height = (float)msgData[(byte)EParameterCodeToolkit.PANEL_HEIGHT];
                EAnchor anchor = (EAnchor)msgData[(byte)EParameterCodeToolkit.PANEL_ANCHOR];

                if (anchor == EAnchor.TOP)
                {
                    view.DecreaseHeightFromBottom(height);
                }
                else if (anchor == EAnchor.BOTTOM)
                {
                    view.DecreaseHeightFromTop(height);
                }
                else
                {
                    view.DecreaseHeight(height);
                }
            }
        }

        #endregion
    }
}
