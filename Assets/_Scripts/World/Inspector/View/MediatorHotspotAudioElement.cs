﻿using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class MediatorHotspotAudioElement : EventMediator
    {
        [Inject]
        public ViewHotspotAudioElement view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewHotspotAudioElement.HOTSPOT_AUDIO_EVENT.AUDIO_CHANGED, OnAudioChanged);
            view.dispatcher.UpdateListener(value, ViewHotspotAudioElement.HOTSPOT_AUDIO_EVENT.OPEN_ICON_RESOURCE_LIST, OnOpenIconResourceList);
        }

        private void OnAudioChanged(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.HOTSPOT_AUDIO_ELEMENT_UPDATED, evt.data);
        }

        private void OnOpenIconResourceList(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.SHOW_FILTERED_LIST, evt.data);
        }

    }
}