﻿using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class MediatorHotspotPanelElement : EventMediator
    {
        [Inject]
        public ViewHotspotPanelElement view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewHotspotPanelElement.HOTSPOT_PANEL_EVENT.IMAGE_CHANGED, OnImageChanged);
            view.dispatcher.UpdateListener(value, ViewHotspotPanelElement.HOTSPOT_PANEL_EVENT.DESCRIPTION_TEXT_CHANGED, OnDescriptionChanged);
            view.dispatcher.UpdateListener(value, ViewHotspotPanelElement.HOTSPOT_PANEL_EVENT.TITLE_TEXT_CHANGED, OnTitleChanged);
            view.dispatcher.UpdateListener(value, ViewHotspotPanelElement.HOTSPOT_PANEL_EVENT.OPEN_ICON_RESOURCE_LIST, OnOpenIconResourceList);
        }

        private void OnImageChanged(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.HOTSPOT_IMAGE_ELEMENT_UPDATED, evt.data);
        }

        private void OnDescriptionChanged(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.HOTSPOT_TEXT_ELEMENT_UPDATED, evt.data);
        }

        private void OnTitleChanged(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.HOTSPOT_TEXT_ELEMENT_UPDATED, evt.data);
        }

        private void OnOpenIconResourceList(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.SHOW_FILTERED_LIST, evt.data);
        }
    }
}