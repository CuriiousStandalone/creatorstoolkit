﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.World.Static;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common.UIElements;
using PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.Multimedia;
using PulseIQ.Local.Common.Model.PanoramaVideo;
using strange.extensions.mediation.impl;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class ViewMonoVideoManager : EventView
    {
        [SerializeField]
        private TMP_InputField _itemName;
    
        [SerializeField]
        private CDropdown _endBehaviourDropdown;
    
        [SerializeField]
        private TMP_Text _teleportItemText;
    
        [SerializeField]
        private Slider _videoSlider;
    
        [SerializeField]
        private TMP_Text _currentVideoTime;
    
        [SerializeField]
        private TMP_Text _videoDuration;
    
        private int _itemIndex = -1;
        private int _teleportItemIndex = -1;
        private EMultimediaEndBehaviorCode _endBehaviour;
        private float videoDuration = 0;
    
        internal enum PANORAMA_EVENTS
        {
            NAME_CHANGED,
            END_BEHAVIOUR_CHANGED,
            TELEPORT_ITEM_CHANGED,
            PLAY_PRESSED,
            PAUSE_PRESSED,
            STOP_PRESSED,
            REPLAY_PRESSED,
            VIDEO_TIME_CHANGED,
            TELEPORT_ITEM_CREATED,
        }
    
        internal void init()
        {
    
        }
    
        public void PanoramaVideoItemSelected(CTPanoramaVideoData panoramaData, string teleportItemName)
        {
            if(_itemIndex != panoramaData.ItemIndex)
            {
                _videoSlider.value = 0;
            }

            _itemIndex = panoramaData.ItemIndex;
            _teleportItemIndex = panoramaData.TargetMultimediaItemIndex;
            _endBehaviour = panoramaData.EndBehavior;

            if (!string.IsNullOrEmpty(teleportItemName))
            {
                _teleportItemText.text = teleportItemName;
            }
            else
            {
                _teleportItemText.text = "";
            }

            _itemName.SetTextWithoutNotify(panoramaData.ItemName);
            _endBehaviourDropdown.GetDropdown().SetValueWithoutNotify(_endBehaviourDropdown.GetIndexByName(ReturnEndBehaviourText(_endBehaviour)));
            _currentVideoTime.text = "0:00 /";
            _videoDuration.text = panoramaData.Duration.ToString();

        }
    
        private EMultimediaEndBehaviorCode ReturnEndBehaviour(string endBehaviour)
        {
            switch (endBehaviour)
            {
                case "Close":
                    {
                        return EMultimediaEndBehaviorCode.CLOSE;
                    }
    
                case "Loop":
                    {
                        return EMultimediaEndBehaviorCode.LOOP;
                    }
    
                case "Stop":
                    {
                        return EMultimediaEndBehaviorCode.STOP;
                    }
    
                case "Teleport":
                    {
                        return EMultimediaEndBehaviorCode.TELEPORT;
                    }
    
                default:
                    {
                        return EMultimediaEndBehaviorCode.CLOSE;
                    }
            }
        }
    
        private string ReturnEndBehaviourText(EMultimediaEndBehaviorCode endBehaviour)
        {
            switch (endBehaviour)
            {
                case EMultimediaEndBehaviorCode.CLOSE:
                    {
                        return "Close";
                    }
    
                case EMultimediaEndBehaviorCode.LOOP:
                    {
                        return "Loop";
                    }
    
                case EMultimediaEndBehaviorCode.STOP:
                    {
                        return "Stop";
                    }
    
                case EMultimediaEndBehaviorCode.TELEPORT:
                    {
                        return "Teleport";
                    }
    
                default:
                    {
                        return "Close";
                    }
            }  
        }
    
        public void ItemNameChanged()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_NAME, _itemName.text);
    
            dispatcher.Dispatch(PANORAMA_EVENTS.NAME_CHANGED, data);
        }
  
        public void EndBehaviourChanged()
        {
            _endBehaviour = ReturnEndBehaviour(_endBehaviourDropdown.GetDropdownStringValue());

            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.MEDIA_END_BEHAVIOUR, _endBehaviour);
    
            dispatcher.Dispatch(PANORAMA_EVENTS.END_BEHAVIOUR_CHANGED, data);
        }

        public void TeleportItemChanged(CDropBox dropBox)
        {
            if (dropBox.IsNewDroppedItem())
            {
                MessageData data = new MessageData();
                data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
                data.Parameters.Add((byte)EParameterCodeToolkit.MEDIA_TELEPORT_ITEM, dropBox.CurrentDataId);
                data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_TYPE, dropBox.GetDroppedItemType());

                dispatcher.Dispatch(PANORAMA_EVENTS.TELEPORT_ITEM_CREATED, data);
            }
            else
            {
                MessageData data = new MessageData();
                data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
                data.Parameters.Add((byte)EParameterCodeToolkit.MEDIA_TELEPORT_ITEM, dropBox.CurrentDataId);

                dispatcher.Dispatch(PANORAMA_EVENTS.TELEPORT_ITEM_CHANGED, data);
            }
        }

        public void TeleportItemCleared()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.MEDIA_TELEPORT_ITEM, "");

            dispatcher.Dispatch(PANORAMA_EVENTS.TELEPORT_ITEM_CHANGED, data);
        }

        public void PlayPressed()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
    
            dispatcher.Dispatch(PANORAMA_EVENTS.PLAY_PRESSED, data);
        }
    
        public void PausePressed()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
    
            dispatcher.Dispatch(PANORAMA_EVENTS.PAUSE_PRESSED, data);
        }
    
        public void StopPressed()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            _videoSlider.SetValueWithoutNotify(0);

            dispatcher.Dispatch(PANORAMA_EVENTS.STOP_PRESSED, data);
        }
    
        public void ReplayPressed()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
    
            dispatcher.Dispatch(PANORAMA_EVENTS.REPLAY_PRESSED, data);
        }
    
        public void SeekPressed()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.MEDIA_VIDEO_CURRENT_TIME, _videoSlider.value);
    
            dispatcher.Dispatch(PANORAMA_EVENTS.VIDEO_TIME_CHANGED, data);
        }

        public void SetCurrentVideoTime(MessageData data)
        {
            if (null == _currentVideoTime)
            {
                return;
            }

            float time = (float)data[(byte)EParameterCodeStatic.VIDEO_CURRENT_TIME];
            int totalTime = (int)time / 1000;
            int hours = totalTime / 3600;
            int minutes = (totalTime % 3600) / 60;
            int seconds = (totalTime % 3600) % 60;
            string secs = seconds.ToString();

            if (seconds < 10)
            {
                secs = "0" + seconds.ToString();
            }

            if (hours > 0)
            {
                _currentVideoTime.text = string.Format("{0}:{1}:{2} /", hours, minutes, secs);
            }
            else
            {
                _currentVideoTime.text = string.Format("{0}:{1} /", minutes, secs);
            }

            _videoSlider.SetValueWithoutNotify(totalTime / videoDuration);
        }

        public void SetVideoDuriation(MessageData data)
        {
            if (null == _videoDuration)
            {
                return;
            }

            float time = (float)data[(byte)EParameterCodeStatic.VIDEO_DURATION];

            int totalTime = (int)time / 1000;
            videoDuration = totalTime;
            int hours = totalTime / 3600;
            int minutes = (totalTime % 3600) / 60;
            int seconds = (totalTime % 3600) % 60;
            string secs = seconds.ToString();

            if (seconds < 10)
            {
                secs = "0" + seconds.ToString();
            }

            if (hours > 0)
            {
                _videoDuration.text = string.Format("{0}:{1}:{2}", hours, minutes, secs);
            }
            else
            {
                _videoDuration.text = string.Format("{0}:{1}", minutes, secs);
            }
        }

        public void TeleportItemCreated(MessageData msgData)
        {
            int itemIndex = (int)msgData[(byte)EParameterCodeToolkit.ITEM_INDEX];

            if (msgData.Parameters.ContainsKey((byte)EParameterCodeToolkit.HOTSPOT_ID) || itemIndex != _itemIndex)
            {
                return;
            }

            CTItemData itemData = (CTItemData)msgData[(byte)EParameterCodeToolkit.ITEM_DATA];

            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.MEDIA_TELEPORT_ITEM, itemData.ItemIndex.ToString());

            dispatcher.Dispatch(PANORAMA_EVENTS.TELEPORT_ITEM_CHANGED, data);
        }
    }
}