﻿using UnityEngine;

using strange.extensions.mediation.impl;

using TMPro;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common.UIElements;
using PulseIQ.Local.Common.Model.Panorama;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents;
using PulseIQ.Local.Common.Model.Item;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class ViewPanoramaManager : EventView
    {
        [SerializeField]
        private TMP_InputField _itemName;

        [SerializeField]
        private CCheckbox _autoTeleport;

        [SerializeField]
        private TMP_Text _teleportTarget;

        [SerializeField]
        private GameObject _teleportTimeObj;

        [SerializeField]
        private TMP_InputField _teleportTime;

        private int _itemIndex = -1;
        private int _teleportItemIndex = -1;

        private int _currentTeleportTime = 0;

        internal enum PANORAMA_EVENTS
        {
            NAME_CHANGED,
            AUTO_TELEPORT_CHANGED,
            TELEPORT_TIME_CHANGED,
            TELEPORT_ITEM_CHANGED,
            TELEPORT_ITEM_CREATED,
        }

        internal void init()
        {

        }

        public void PanoramaItemSelected(CTPanoramaData panoramaData, string teleportItemName)
        {
            _itemIndex = panoramaData.ItemIndex;
            _itemName.text = panoramaData.ItemName;
            _teleportItemIndex = panoramaData.TargetMultimediaItemIndex;

            if (!string.IsNullOrEmpty(teleportItemName))
            {
                _teleportTarget.text = teleportItemName;
            }
            else
            {
                _teleportTarget.text = "";
            }

            if (panoramaData.TargetMultimediaItemIndex > 0)
            {
                _autoTeleport.GetCheckbox().SetIsOnWithoutNotify(true);
                _teleportTimeObj.SetActive(true);
            }
            else
            {
                _autoTeleport.GetCheckbox().SetIsOnWithoutNotify(false);
                _teleportTimeObj.SetActive(false);
            }
        }

        public void ItemNameChanged()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_NAME, _itemName.text);

            dispatcher.Dispatch(PANORAMA_EVENTS.NAME_CHANGED, data);
        }

        public void AutoTeleportChanged()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.PANORAMA_AUTO_TELEPORT, _autoTeleport.GetCheckbox().isOn);

            dispatcher.Dispatch(PANORAMA_EVENTS.AUTO_TELEPORT_CHANGED, data);

            _teleportTimeObj.SetActive(_autoTeleport.GetCheckbox().isOn == true ? true : false);

        }

        public void TeleportTimeChanged()
        {
            int time = int.Parse(_teleportTime.text);

            if(time < 0)
            {
                _teleportTime.text = "0";
                _currentTeleportTime = 0;
            }
            else if(time > 999)
            {
                _teleportTime.text = "999";
                _currentTeleportTime = 999;
            }
            else
            {
                _currentTeleportTime = time;
            }

            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.PANORAMA_TELEPORT_TIME, _currentTeleportTime);

            dispatcher.Dispatch(PANORAMA_EVENTS.TELEPORT_TIME_CHANGED, data);
        }

        public void IncreaseTeleportTime()
        {
            ++_currentTeleportTime;

            if(_currentTeleportTime > 999)
            {
                _currentTeleportTime = 999;
            }

            _teleportTime.SetTextWithoutNotify(_currentTeleportTime.ToString());

            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.PANORAMA_TELEPORT_TIME, _currentTeleportTime);

            dispatcher.Dispatch(PANORAMA_EVENTS.TELEPORT_TIME_CHANGED, data);
        }


        public void DecreaseTeleportTime()
        {
            --_currentTeleportTime;

            if (_currentTeleportTime < 0)
            {
                _currentTeleportTime = 0;
            }

            _teleportTime.SetTextWithoutNotify(_currentTeleportTime.ToString());

            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.PANORAMA_TELEPORT_TIME, _currentTeleportTime);

            dispatcher.Dispatch(PANORAMA_EVENTS.TELEPORT_TIME_CHANGED, data);
        }

        public void TeleportItemChanged(CDropBox dropBox)
        {
            if (dropBox.IsNewDroppedItem())
            {
                MessageData data = new MessageData();
                data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
                data.Parameters.Add((byte)EParameterCodeToolkit.MEDIA_TELEPORT_ITEM, dropBox.CurrentDataId);
                data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_TYPE, dropBox.GetDroppedItemType());

                dispatcher.Dispatch(PANORAMA_EVENTS.TELEPORT_ITEM_CREATED, data);
            }
            else
            {
                MessageData data = new MessageData();
                data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
                data.Parameters.Add((byte)EParameterCodeToolkit.MEDIA_TELEPORT_ITEM, dropBox.CurrentDataId);

                dispatcher.Dispatch(PANORAMA_EVENTS.TELEPORT_ITEM_CHANGED, data);
            }
        }

        public void TeleportItemCleared()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.MEDIA_TELEPORT_ITEM, "");

            dispatcher.Dispatch(PANORAMA_EVENTS.TELEPORT_ITEM_CHANGED, data);
        }

        public void TeleportItemCreated(MessageData msgData)
        {
            int itemIndex = (int)msgData[(byte)EParameterCodeToolkit.ITEM_INDEX];

            if(msgData.Parameters.ContainsKey((byte)EParameterCodeToolkit.HOTSPOT_ID) || itemIndex != _itemIndex)
            {
                return;
            }

            CTItemData itemData = (CTItemData)msgData[(byte)EParameterCodeToolkit.ITEM_DATA];

            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.MEDIA_TELEPORT_ITEM, itemData.ItemIndex.ToString());

            dispatcher.Dispatch(PANORAMA_EVENTS.TELEPORT_ITEM_CHANGED, data);
        }
    }
}