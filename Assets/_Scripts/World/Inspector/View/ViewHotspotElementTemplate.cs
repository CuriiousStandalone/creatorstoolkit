﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.Common.Model.Hotspot.HotspotTemplate;
using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class ViewHotspotElementTemplate : EventView
    {
        internal enum HOTSPOT_ELEMENT_EVENTS
        {
            ELEMENT_CLICKED,
        }

        [SerializeField]
        private Image _hotspotElementImage;

        [SerializeField]
        private TMP_Text _hotspotElementTitle;

        [SerializeField]
        private TMP_Text _hotspotElementDesc;

        private HotspotTemplateData elementData;

        internal void init()
        {
            
        }

        public void SetElemenetData(HotspotTemplateData data)
        {
            elementData = data;
            _hotspotElementTitle.text = data.TemplateName;
            _hotspotElementDesc.text = data.TemplateDescription;
            _hotspotElementImage.sprite = Resources.Load<Sprite>("Images/HotspotTemplateIcons/hotspot_template_" + data.TemplateIndex);
        }

        public void HotspotElementSelected()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ELEMENT_DATA, elementData);

            dispatcher.Dispatch(HOTSPOT_ELEMENT_EVENTS.ELEMENT_CLICKED, data);
        }
    }
}