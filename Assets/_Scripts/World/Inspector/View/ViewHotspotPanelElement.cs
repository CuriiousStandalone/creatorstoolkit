﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using strange.extensions.mediation.impl;
using PulseIQ.Local.Common.Model.Hotspot;
using TMPro;
using UnityEngine;
using PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents;
using PulseIQ.Local.Common.Model.Hotspot.HotspotElement;
using System.IO;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class ViewHotspotPanelElement : EventView
    {
        internal enum HOTSPOT_PANEL_EVENT
        {
            TITLE_TEXT_CHANGED,
            DESCRIPTION_TEXT_CHANGED,
            IMAGE_CHANGED,
            OPEN_ICON_RESOURCE_LIST,
        }

        [SerializeField]
        private TMP_InputField _titleText;

        [SerializeField]
        private TMP_InputField _descriptionText;

        [SerializeField]
        private CDropBox _imageDropbox;

        private int _itemIndex;
        private string _hotspotID;
        private string _elementID;

        private void Start()
        {
            // Needed to do this as the rect would always self scale back to a smaller size, stopping drop actions to dropbox.
            GetComponent<RectTransform>().sizeDelta = new Vector2(100, 270);
        }

        internal void init()
        {

        }

        public void SetElementData(CTHotspotBoxData data, int itemIndex, string hotspotID)
        {
            _itemIndex = itemIndex;
            _hotspotID = hotspotID;

            CTHotspotElementTextData titleData = (CTHotspotElementTextData)data.Elements[0];
            CTHotspotElementTextData descriptionData = (CTHotspotElementTextData)data.Elements[1];
            CTHotspotElementImageData imageData = (CTHotspotElementImageData)data.Elements[2];

            _titleText.SetTextWithoutNotify(titleData.Text);
            _descriptionText.SetTextWithoutNotify(descriptionData.Text);

            Sprite currentSprite = null;

            if (imageData.MultimediaId != "")
            {
                string imagePath = Path.Combine(Settings.ImagePath, imageData.MultimediaId);

                if (File.Exists(imagePath))

                {
                    currentSprite = LoadImage(imagePath, imageData.Width / 100, imageData.Height / 100);

                    FileInfo fileInfo = new FileInfo(imagePath);

                    _imageDropbox.FillData(imageData.MultimediaId, Path.GetFileNameWithoutExtension(imageData.MultimediaId), currentSprite, "PLATFORM", GetFileSize(fileInfo.Length));
                }
                else
                {
                    _imageDropbox.ClearCurrentData();
                }
            }
        }

        private string GetFileSize(long bytes)
        {
            if (bytes / 1024 < 1000)
            {
                return (bytes / 1024) + " KB";
            }
            else if (bytes / 1024 / 1024 < 1000)
            {
                return (bytes / 1024 / 1024) + " MB";
            }
            else
            {
                return (bytes / 1024 / 1024 / 1024) + " GB";
            }
        }

        public void ImageChanged(string imageName)
        {
            if (gameObject.activeInHierarchy == true)
            {
                MessageData data = new MessageData();
                data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
                data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ID, _hotspotID);
                data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ELEMENT_ID, 2);
                data.Parameters.Add((byte)EParameterCodeToolkit.ELEMENT_MEDIA_ID, imageName);
                data.Parameters.Add((byte)EParameterCodeToolkit.IS_HOTSPOT_PANEL, true);
                data.Parameters.Add((byte)EParameterCodeToolkit.ELEMENT_TYPE, EHotspotElementTypeCode.IMAGE);

                dispatcher.Dispatch(HOTSPOT_PANEL_EVENT.IMAGE_CHANGED, data);
            }
        }

        public void HotspotImageCleared()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ID, _hotspotID);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ELEMENT_ID, 2);
            data.Parameters.Add((byte)EParameterCodeToolkit.ELEMENT_MEDIA_ID, "");
            data.Parameters.Add((byte)EParameterCodeToolkit.IS_HOTSPOT_PANEL, true);
            data.Parameters.Add((byte)EParameterCodeToolkit.ELEMENT_TYPE, EHotspotElementTypeCode.IMAGE);

            dispatcher.Dispatch(HOTSPOT_PANEL_EVENT.IMAGE_CHANGED, data);
        }

        public void HotspotTitleTextChanged()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ID, _hotspotID);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ELEMENT_ID, 0);
            data.Parameters.Add((byte)EParameterCodeToolkit.ELEMENT_TEXT, _titleText.text);
            data.Parameters.Add((byte)EParameterCodeToolkit.IS_HOTSPOT_PANEL, true);
            data.Parameters.Add((byte)EParameterCodeToolkit.ELEMENT_TYPE, EHotspotElementTypeCode.TEXT);

            dispatcher.Dispatch(HOTSPOT_PANEL_EVENT.TITLE_TEXT_CHANGED, data);
        }

        public void HotspotDescriptionTextChanged()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ID, _hotspotID);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ELEMENT_ID, 1);
            data.Parameters.Add((byte)EParameterCodeToolkit.ELEMENT_TEXT, _descriptionText.text);
            data.Parameters.Add((byte)EParameterCodeToolkit.IS_HOTSPOT_PANEL, true);
            data.Parameters.Add((byte)EParameterCodeToolkit.ELEMENT_TYPE, EHotspotElementTypeCode.TEXT);

            dispatcher.Dispatch(HOTSPOT_PANEL_EVENT.DESCRIPTION_TEXT_CHANGED, data);
        }

        private Sprite LoadImage(string imagePath, float canvasWidth, float canvasHeight)
        {
            if (imagePath != "" && File.Exists(imagePath))
            {
                Texture2D tex = null;
                byte[] fileData;

                fileData = File.ReadAllBytes(imagePath);
                tex = new Texture2D(2, 2);
                tex.LoadImage(fileData);

                int originalWidth = tex.width;
                int originalHeight = tex.height;

                float ratioX = (float)canvasWidth / (float)originalWidth;
                float ratioY = (float)canvasHeight / (float)originalHeight;

                float ratio = ratioX < ratioY ? ratioX : ratioY;

                int newHeight = System.Convert.ToInt32(originalHeight * ratio);
                int newWidth = System.Convert.ToInt32(originalWidth * ratio);

                return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2());
            }

            return null;
        }

        public void OpenHotspotIconList()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.RESOURCE_LIST_FILTER, "Image");
            dispatcher.Dispatch(HOTSPOT_PANEL_EVENT.OPEN_ICON_RESOURCE_LIST, data);
        }
    }
}