﻿using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class MediatorHotspotTextElement : EventMediator
    {
        [Inject]
        public ViewHotspotTextElement view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewHotspotTextElement.HOTSPOT_TEXT_EVENTS.TITLE_TEXT_CHANGED, OnTitleChanged);
        }

        private void OnTitleChanged(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.HOTSPOT_TEXT_ELEMENT_UPDATED, evt.data);
        }
    }
}