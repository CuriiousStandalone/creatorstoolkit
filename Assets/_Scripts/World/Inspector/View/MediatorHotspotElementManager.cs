﻿using System.Collections.Generic;

using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.Hotspot.HotspotTemplate;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class MediatorHotspotElementManager : EventMediator
    {
        [Inject]
        public ViewHotspotElementManager view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewHotspotElementManager.HotspotElementManagerEvents.VIEW_LOADED, OnViewLoaded);

            dispatcher.UpdateListener(value, EInspectorEvent.HOTSPOT_TEMPLATE_TYPES_LOADED, OnTemplatesLoaded);
        }

        private void OnViewLoaded()
        {
            dispatcher.Dispatch(EInspectorEvent.LOAD_HOTSPOT_TEMPLATE_TYPES);
        }

        private void OnTemplatesLoaded(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            List<HotspotTemplateData> list = (List<HotspotTemplateData>)msgData[(byte)EParameterCodeWorld.HOTSPOT_TEMPLATE_DATA_LIST];

            view.CreateHotspotElementOptions(list);
        }
    }
}