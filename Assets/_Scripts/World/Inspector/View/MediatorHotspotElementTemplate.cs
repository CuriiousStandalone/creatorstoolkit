﻿using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class MediatorHotspotElementTemplate : EventMediator
    {
        [Inject]
        public ViewHotspotElementTemplate view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewHotspotElementTemplate.HOTSPOT_ELEMENT_EVENTS.ELEMENT_CLICKED, HotspotElelementTemlateSelected);
        }

        private void HotspotElelementTemlateSelected(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.HOTSPOT_ELEMENT_TEMPLATE_SELECTED, evt.data);
        }
    }
}