﻿using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;


namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class MediatorHotspotImageElement : EventMediator
    {
        [Inject]
        public ViewHotspotImageElement view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewHotspotImageElement.HOTSPOT_IMAGE_EVENTS.IMAGE_CHANGED, OnImageChanged);
            view.dispatcher.UpdateListener(value, ViewHotspotImageElement.HOTSPOT_IMAGE_EVENTS.OPEN_ICON_RESOURCE_LIST, OnOpenIconResourceList);
        }

        private void OnImageChanged(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.HOTSPOT_IMAGE_ELEMENT_UPDATED, evt.data);
        }

        private void OnOpenIconResourceList(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.SHOW_FILTERED_LIST, evt.data);
        }
    }
}