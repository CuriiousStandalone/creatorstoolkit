﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.World;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using System.Collections.Generic;
using System.IO;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class MediatorInspectorModuleManager : EventMediator
    {
        [Inject]
        public ViewInspectorModuleManager view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.Init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewInspectorModuleManager.InspectorModuleManagerEvents.VIEW_LOADED, OnViewLoaded);
            view.dispatcher.UpdateListener(value, ViewInspectorModuleManager.InspectorModuleManagerEvents.UPDATED_DESCRIPTION, OnDescriptionUpdated);
            view.dispatcher.UpdateListener(value, ViewInspectorModuleManager.InspectorModuleManagerEvents.UPDATED_NAME, OnNameUpdated);
            view.dispatcher.UpdateListener(value, ViewInspectorModuleManager.InspectorModuleManagerEvents.UPDATED_IS_MULTIPLAYER, OnIsMultiplayerUpdated);
            view.dispatcher.UpdateListener(value, ViewInspectorModuleManager.InspectorModuleManagerEvents.TEMPLATE_CHANGED, OnTemplateChanged);
            view.dispatcher.UpdateListener(value, ViewInspectorModuleManager.InspectorModuleManagerEvents.THUMBNAIL_UPDATED, OnModuleThumbnailUpdated);
            view.dispatcher.UpdateListener(value, ViewInspectorModuleManager.InspectorModuleManagerEvents.OPEN_IMAGE_LIST, OnOpenImageList);
            view.dispatcher.UpdateListener(value, ViewInspectorModuleManager.InspectorModuleManagerEvents.THUMBNAIL_CHANGED, OnThumbnailChanged);
            view.dispatcher.UpdateListener(value, ViewInspectorModuleManager.InspectorModuleManagerEvents.THUMBNAIL_CLEARED, OnThumbnailCleared);

            dispatcher.UpdateListener(value, EInspectorEvent.TEMPLATE_DATA_LOADED, OnTemplateDataLoaded);
            dispatcher.UpdateListener(value, EInspectorEvent.WORLD_CREATED, OnWorldCreated);
            dispatcher.UpdateListener(value, EWorldEvent.WORLD_CREATED, OnWorldCreated);

            dispatcher.UpdateListener(value, EWorldEvent.MODULE_SELECTED, OnModuleSelected);

            dispatcher.UpdateListener(value, EEventToolkitCrossContext.OPEN_WORLD_REQUESTED, OnEnableAccordion);
            dispatcher.UpdateListener(value, EEventToolkitCrossContext.NEW_WORLD_CREATED, OnEnableAccordion);
        }

        private void OnThumbnailChanged(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.MODULE_THUMBNAIL_CHANGED, evt.data);
        }

        private void OnThumbnailCleared(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.MODULE_THUMBNAIL_CLEARED, evt.data);
        }

        private void OnOpenImageList(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.SHOW_FILTERED_LIST, evt.data);
        }

        private void OnEnableAccordion()
        {
            view.EnableModuleAccordion();
        }

        private void OnViewLoaded()
        {
            dispatcher.Dispatch(EInspectorEvent.GET_TEMPLATE_DATA);
        }

        private void OnDescriptionUpdated(IEvent payload)
        {
            dispatcher.Dispatch(EInspectorEvent.MODULE_DESCRIPTION_UPDATED, payload.data);
        }

        private void OnNameUpdated(IEvent payload)
        {
            dispatcher.Dispatch(EInspectorEvent.MODULE_NAME_UPDATED, payload.data);
        }

        private void OnIsMultiplayerUpdated(IEvent payload)
        {
            dispatcher.Dispatch(EInspectorEvent.MODULE_IS_MULTIPLAYER_UPDATED, payload.data);
        }

        private void OnTemplateChanged(IEvent payload)
        {
            dispatcher.Dispatch(EInspectorEvent.TEMPLATE_CHANGED, payload.data);
        }

        private void OnWorldCreated(IEvent payload)
        {
            MessageData msgData = (MessageData)payload.data;

            WorldData worldData = (WorldData)msgData.Parameters[(byte)EParameterCodeWorld.WORLD_DATA];
            string thumbnailPath = Path.Combine(Settings.WorldThumbnailPath, worldData.WorldThumbnailPath);

            view.SetModuleData(worldData, thumbnailPath);
        }

        private void OnTemplateDataLoaded(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            List<WorldData> templateList = (List<WorldData>)msgData[(byte)EParameterCodeWorld.TEMPLATE_DATA_LIST];

            view.SetTemplates(templateList);
        }

        private void OnModuleSelected(IEvent payload)
        {
            MessageData msgData = (MessageData)payload.data;

            WorldData worldData = (WorldData)msgData.Parameters[(byte)EParameterCodeWorld.WORLD_DATA];
            string thumbnailPath = Path.Combine(Settings.WorldThumbnailPath, worldData.WorldThumbnailPath);

            view.SetModuleData(worldData, thumbnailPath);
        }

        private void OnModuleThumbnailUpdated(IEvent evt)
        {
            dispatcher.Dispatch(EWorldEvent.MODULE_THUMBNAIL_UPDATED, evt.data);
        }
    }
}
