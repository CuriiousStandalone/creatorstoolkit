﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents;
using PulseIQ.Local.Common.Model.Hotspot.HotspotElement;
using strange.extensions.mediation.impl;
using System.IO;
using UnityEngine;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class ViewHotspotImageElement : EventView
    {
        internal enum HOTSPOT_IMAGE_EVENTS
        {
            IMAGE_CHANGED,
            OPEN_ICON_RESOURCE_LIST,
        }

        private int _itemIndex;
        private string _hotspotID;

        [SerializeField]
        private CDropBox _imageDropbox;

        internal void init()
        {

        }

        public void SetElementData(CTHotspotElementImageData data, int itemIndex, string hotspotID)
        {
            _itemIndex = itemIndex;
            _hotspotID = hotspotID;

            Sprite currentSprite = null;

            if (data.MultimediaId != "")
            {
                string imagePath = Path.Combine(Settings.ImagePath, data.MultimediaId);

                if (File.Exists(imagePath))
                {
                    currentSprite = LoadImage(imagePath, data.Width / 100, data.Height / 100);

                    FileInfo fileInfo = new FileInfo(imagePath);

                    _imageDropbox.FillData(data.MultimediaId, Path.GetFileNameWithoutExtension(data.MultimediaId), currentSprite, "PLATFORM", GetFileSize(fileInfo.Length));
                }
                else
                {
                    _imageDropbox.ClearCurrentData();
                }
            }          
        }

        private string GetFileSize(long bytes)
        {
            if (bytes / 1024 < 1000)
            {
                return (bytes / 1024) + " KB";
            }
            else if (bytes / 1024 / 1024 < 1000)
            {
                return (bytes / 1024 / 1024) + " MB";
            }
            else
            {
                return (bytes / 1024 / 1024 / 1024) + " GB";
            }
        }

        public void HotspotImageChanged(string mediaID)
        {
            if(mediaID.Contains("Status Icon"))
            {
                Debug.Log("!!!!!!!!!!!!!!!!!!!");
                return;
            }

            if (gameObject.activeInHierarchy)
            {
                MessageData data = new MessageData();
                data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
                data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ID, _hotspotID);
                data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ELEMENT_ID, 0);
                data.Parameters.Add((byte)EParameterCodeToolkit.ELEMENT_MEDIA_ID, mediaID);
                data.Parameters.Add((byte)EParameterCodeToolkit.ELEMENT_TYPE, EHotspotElementTypeCode.IMAGE);

                dispatcher.Dispatch(HOTSPOT_IMAGE_EVENTS.IMAGE_CHANGED, data);
            }
        }

        public void HotspotImageCleared()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ID, _hotspotID);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ELEMENT_ID, 0);
            data.Parameters.Add((byte)EParameterCodeToolkit.ELEMENT_MEDIA_ID, "");
            data.Parameters.Add((byte)EParameterCodeToolkit.ELEMENT_TYPE, EHotspotElementTypeCode.IMAGE);

            dispatcher.Dispatch(HOTSPOT_IMAGE_EVENTS.IMAGE_CHANGED, data);
        }

        private Sprite LoadImage(string imagePath, float canvasWidth, float canvasHeight)
        {
            if (imagePath != "" && File.Exists(imagePath))
            {
                Texture2D tex = null;
                byte[] fileData;

                fileData = File.ReadAllBytes(imagePath);
                tex = new Texture2D(2, 2);
                tex.LoadImage(fileData);

                int originalWidth = tex.width;
                int originalHeight = tex.height;

                float ratioX = (float)canvasWidth / (float)originalWidth;
                float ratioY = (float)canvasHeight / (float)originalHeight;

                float ratio = ratioX < ratioY ? ratioX : ratioY;

                int newHeight = System.Convert.ToInt32(originalHeight * ratio);
                int newWidth = System.Convert.ToInt32(originalWidth * ratio);

                return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2());
            }

            return null;
        }

        public void OpenHotspotIconList()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.RESOURCE_LIST_FILTER, "Image");
            dispatcher.Dispatch(HOTSPOT_IMAGE_EVENTS.OPEN_ICON_RESOURCE_LIST, data);
        }
    }
}