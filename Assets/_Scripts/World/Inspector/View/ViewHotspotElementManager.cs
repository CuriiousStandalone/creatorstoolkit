﻿using UnityEngine;

using System.Collections.Generic;

using strange.extensions.mediation.impl;

using PulseIQ.Local.Common.Model.Hotspot.HotspotTemplate;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class ViewHotspotElementManager : EventView
    {
        [SerializeField]
        private Transform _scrollListContent;

        internal enum HotspotElementManagerEvents
        {
            VIEW_LOADED,
        }

        internal void init()
        {
            dispatcher.Dispatch(HotspotElementManagerEvents.VIEW_LOADED);
        }

        public void CreateHotspotElementOptions(List<HotspotTemplateData> listHotspotData)
        {
            foreach (HotspotTemplateData template in listHotspotData)
            {
                GameObject obj = Instantiate(Resources.Load("Prefabs/UI/HotspotElementSelection") as GameObject, _scrollListContent);
                obj.GetComponent<ViewHotspotElementTemplate>().SetElemenetData(template);
            }
        }
    }
}