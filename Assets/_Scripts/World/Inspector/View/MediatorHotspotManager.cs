﻿using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

using PulseIQ.Local.ClientCommon.Components.Common;

using CuriiousIQ.Local.CreatorsToolkit.World.Static;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class MediatorHotspotManager : EventMediator
    {
        [Inject]
        public ViewHotspotManager view { get; set; }

        public override void OnRegister()
        {
            view.init();
            UpdateListeners(true);
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewHotspotManager.HOTSPOT_EVENTS.ACTIVATE_TIME_CHANGED, OnActivateTimeChanged);
            view.dispatcher.UpdateListener(value, ViewHotspotManager.HOTSPOT_EVENTS.DEACTIVATE_TIME_CHANGED, OnDectivateTimeChanged);
            view.dispatcher.UpdateListener(value, ViewHotspotManager.HOTSPOT_EVENTS.IS_ACTIVE_CHANGED, OnIsActiveChanged);
            view.dispatcher.UpdateListener(value, ViewHotspotManager.HOTSPOT_EVENTS.DELETE_PRESSED, OnDeletePressed);
            view.dispatcher.UpdateListener(value, ViewHotspotManager.HOTSPOT_EVENTS.NAME_CHANGED, OnNameChanged);
            view.dispatcher.UpdateListener(value, ViewHotspotManager.HOTSPOT_EVENTS.TRIGGER_EVENT_CHANGED, OnTriggerEventChanged);
            view.dispatcher.UpdateListener(value, ViewHotspotManager.HOTSPOT_EVENTS.TRIGGER_STATE_CHANGED, OnTriggerStateChanged);
            view.dispatcher.UpdateListener(value, ViewHotspotManager.HOTSPOT_EVENTS.STATUS_ICON_CHANGED, OnStatusIconChanged);
            view.dispatcher.UpdateListener(value, ViewHotspotManager.HOTSPOT_EVENTS.REQUEST_DEFAULT_ICON_IDS, OnRequestDefaultIconIds);
            view.dispatcher.UpdateListener(value, ViewHotspotManager.HOTSPOT_EVENTS.OPEN_ICON_RESOURCE_LIST, OnOpenIconResourceList);
            view.dispatcher.UpdateListener(value, ViewHotspotManager.HOTSPOT_EVENTS.TELEPORT_ITEM_CHANGED, OnTeleportItemChanged);
            view.dispatcher.UpdateListener(value, ViewHotspotManager.HOTSPOT_EVENTS.TELEPORT_ITEM_CREATED, OnCreateTeleportItem);

            dispatcher.UpdateListener(value, EInspectorEvent.MEDIA_VIDEO_LOADED, OnVideoLoaded);
            dispatcher.UpdateListener(value, EWorldEvent.DEFAULT_STATUS_ICON_IDS, OnSetDefaultIconIDs);
            dispatcher.UpdateListener(value, EWorldEvent.TELEPORT_ITEM_CREATED, OnTeleportItemCreated);
        }

        private void OnOpenIconResourceList(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.SHOW_FILTERED_LIST, evt.data);
        }

        private void OnRequestDefaultIconIds(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.REQUEST_DEFAULT_ICON_IDS, evt.data);
        }

        private void OnStatusIconChanged(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.HOTSPOT_STATUS_ICON_CHANGED, evt.data);
        }

        private void OnActivateTimeChanged(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.HOTSPOT_ACTIVATE_TIME_CHANGED, evt.data);
        }

        private void OnDectivateTimeChanged(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.HOTSPOT_DEACTIVATE_TIME_CHANGED, evt.data);
        }

        private void OnIsActiveChanged(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.HOTSPOT_IS_ACTIVE_CHANGED, evt.data);
        }

        private void OnDeletePressed(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.HOTSPOT_DELETE_PRESSED, evt.data);
        }

        private void OnNameChanged(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.HOTSPOT_NAME_CHANGED, evt.data);
        }

        private void OnTriggerEventChanged(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.HOTSPOT_TRIGGER_EVENT_CHANGED, evt.data);
        }

        private void OnTriggerStateChanged(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.HOTSPOT_TRIGGER_STATE_CHANGED, evt.data);
        }

        private void OnVideoLoaded(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;

            int videoDuration = (int)((float)data.Parameters[(byte)EParameterCodeStatic.VIDEO_DURATION]);
            view.SetVideoDuration(videoDuration);
        }

        private void OnSetDefaultIconIDs(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;

            view.SetDefaultIconIDs(data);
        }

        public void OnTeleportItemChanged(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.HOTSPOT_TELEPORT_ITEM_CHANGED, evt.data);
        }

        public void OnCreateTeleportItem(IEvent evt)
        {
            dispatcher.Dispatch(EInspectorEvent.CREATE_TELEPORT_ITEM, evt.data);
        }

        private void OnTeleportItemCreated(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;
            view.TeleportItemCreated(data);
        }
    }
}