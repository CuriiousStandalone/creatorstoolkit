﻿
using UnityEngine;
using UnityEngine.UI;
using TMPro;

using PulseIQ.Local.Common.Model.StereoPanoramaVideo;

using strange.extensions.mediation.impl;
using PulseIQ.Local.Common.Model.Multimedia;
using PulseIQ.Local.ClientCommon.Components.Common.UIElements;
using PulseIQ.Local.ClientCommon.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.World.Static;
using PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents;
using PulseIQ.Local.Common.Model.Item;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class ViewStereoVideoManager : EventView
    {
        [SerializeField]
        private TMP_InputField _itemName;

        [SerializeField]
        private CDropdown _videoTypeDropdown;

        [SerializeField]
        private CDropdown _endBehaviourDropdown;

        [SerializeField]
        private TMP_Text _teleportItemText;

        [SerializeField]
        private Slider _videoSlider;

        [SerializeField]
        private TMP_Text _currentVideoTime;

        [SerializeField]
        private TMP_Text _videoDuration;

        private int _itemIndex = -1;
        private int _teleportItemIndex = -1;
        private EMultimediaEndBehaviorCode _endBehaviour;
        private EStereoPanoramaVideoTypeCode _videoType;
        private float videoDuration = 0;

        internal enum STEREO_EVENTS
        {
            NAME_CHANGED,
            VIDEO_TYPE_CHANGED,
            END_BEHAVIOUR_CHANGED,
            TELEPORT_ITEM_CHANGED,
            PLAY_PRESSED,
            PAUSE_PRESSED,
            STOP_PRESSED,
            REPLAY_PRESSED,
            VIDEO_TIME_CHANGED,
            TELEPORT_ITEM_CREATED,
        }

        internal void init()
        {

        }

        public void StereoVideoItemSelected(CTStereoPanoramaVideoData stereoData, string teleportItemName)
        {
            _itemIndex = stereoData.ItemIndex;
            _teleportItemIndex = stereoData.TargetMultimediaItemIndex;
            _videoType = stereoData.StereoPanoramaVideoType;
            _endBehaviour = stereoData.EndBehavior;

            if(!string.IsNullOrEmpty(teleportItemName))
            {
                _teleportItemText.text = teleportItemName;
            }
            else
            {
                _teleportItemText.text = "";
            }

            _itemName.SetTextWithoutNotify(stereoData.ItemName);
            _endBehaviourDropdown.GetDropdown().SetValueWithoutNotify(_endBehaviourDropdown.GetIndexByName(ReturnEndBehaviourText(_endBehaviour)));
            _videoTypeDropdown.GetDropdown().value = _videoTypeDropdown.GetIndexByName(ReturnStereoTypeText(_videoType));
            _currentVideoTime.text = "0:00 /";
            _videoDuration.text = stereoData.Duration.ToString();
            _videoSlider.value = 0;
        }

        EStereoPanoramaVideoTypeCode ReturnStereoVideoType(string videoType)
        {
            switch (videoType)
            {
                case "Left Right":
                    {
                        return EStereoPanoramaVideoTypeCode.LEFT_RIGHT;
                    }

                case "Top Bottom":
                    {
                        return EStereoPanoramaVideoTypeCode.TOP_BOTTOM;
                    }

                case "Right Left":
                    {
                        return EStereoPanoramaVideoTypeCode.RIGHT_LEFT;
                    }

                case "Bottom Top":
                    {
                        return EStereoPanoramaVideoTypeCode.BOTTOM_TOP;
                    }
                default:
                    {
                        return EStereoPanoramaVideoTypeCode.LEFT_RIGHT;
                    }
            }
        }

        public string ReturnStereoTypeText(EStereoPanoramaVideoTypeCode type)
        {
            switch (type)
            {
                case EStereoPanoramaVideoTypeCode.BOTTOM_TOP:
                    {
                        return "Bottom Top";
                    }

                case EStereoPanoramaVideoTypeCode.LEFT_RIGHT:
                    {
                        return "Left Right";
                    }

                case EStereoPanoramaVideoTypeCode.RIGHT_LEFT:
                    {
                        return "Right Left";
                    }

                case EStereoPanoramaVideoTypeCode.TOP_BOTTOM:
                    {
                        return "Top Bottom";
                    }

                default:
                    {
                        return "Left Right";
                    }
            }
        }

        private EMultimediaEndBehaviorCode ReturnEndBehaviour(string endBehaviour)
        {
            switch (endBehaviour)
            {
                case "Close":
                    {
                        return EMultimediaEndBehaviorCode.CLOSE;
                    }

                case "Loop":
                    {
                        return EMultimediaEndBehaviorCode.LOOP;
                    }

                case "Stop":
                    {
                        return EMultimediaEndBehaviorCode.STOP;
                    }

                case "Teleport":
                    {
                        return EMultimediaEndBehaviorCode.TELEPORT;
                    }

                default:
                    {
                        return EMultimediaEndBehaviorCode.CLOSE;
                    }
            }
        }

        private string ReturnEndBehaviourText(EMultimediaEndBehaviorCode endBehaviour)
        {
            switch (endBehaviour)
            {
                case EMultimediaEndBehaviorCode.CLOSE:
                    {
                        return "Close";
                    }

                case EMultimediaEndBehaviorCode.LOOP:
                    {
                        return "Loop";
                    }

                case EMultimediaEndBehaviorCode.STOP:
                    {
                        return "Stop";
                    }

                case EMultimediaEndBehaviorCode.TELEPORT:
                    {
                        return "Teleport";
                    }

                default:
                    {
                        return "Close";
                    }
            }

        }

        public void ItemNameChanged()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_NAME, _itemName.text);

            dispatcher.Dispatch(STEREO_EVENTS.NAME_CHANGED, data);
        }

        public void VideoTypeChanged()
        {
            _videoType = ReturnStereoVideoType(_videoTypeDropdown.GetDropdownStringValue());

            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.STEREO_VIDEO_TYPE, _videoType);

            dispatcher.Dispatch(STEREO_EVENTS.VIDEO_TYPE_CHANGED, data);
        }

        public void EndBehaviourChanged()
        {
            _endBehaviour = ReturnEndBehaviour(_endBehaviourDropdown.GetDropdownStringValue());

            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.MEDIA_END_BEHAVIOUR, _endBehaviour);

            dispatcher.Dispatch(STEREO_EVENTS.END_BEHAVIOUR_CHANGED, data);
        }

        // change to read MessageData
        public void TeleportItemChanged(CDropBox dropBox)
        {
            if (dropBox.IsNewDroppedItem())
            {
                MessageData data = new MessageData();
                data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
                data.Parameters.Add((byte)EParameterCodeToolkit.MEDIA_TELEPORT_ITEM, dropBox.CurrentDataId);
                data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_TYPE, dropBox.GetDroppedItemType());

                dispatcher.Dispatch(STEREO_EVENTS.TELEPORT_ITEM_CREATED, data);
            }
            else
            {
                MessageData data = new MessageData();
                data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
                data.Parameters.Add((byte)EParameterCodeToolkit.MEDIA_TELEPORT_ITEM, dropBox.CurrentDataId);

                dispatcher.Dispatch(STEREO_EVENTS.TELEPORT_ITEM_CHANGED, data);
            }
        }

        public void TeleportItemCleared()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.MEDIA_TELEPORT_ITEM, "");

            dispatcher.Dispatch(STEREO_EVENTS.TELEPORT_ITEM_CHANGED, data);
        }

        public void PlayPressed()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);

            dispatcher.Dispatch(STEREO_EVENTS.PLAY_PRESSED, data);
        }

        public void PausePressed()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);

            dispatcher.Dispatch(STEREO_EVENTS.PAUSE_PRESSED, data);
        }

        public void StopPressed()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            _videoSlider.SetValueWithoutNotify(0);

            dispatcher.Dispatch(STEREO_EVENTS.STOP_PRESSED, data);
        }

        public void ReplayPressed()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);

            dispatcher.Dispatch(STEREO_EVENTS.REPLAY_PRESSED, data);
        }

        public void SeekPressed()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.MEDIA_VIDEO_CURRENT_TIME, _videoSlider.value);

            dispatcher.Dispatch(STEREO_EVENTS.VIDEO_TIME_CHANGED, data);
        }

        public void SetCurrentVideoTime(MessageData data)
        {
            if (null == _currentVideoTime)
            {
                return;
            }

            float time = (float)data[(byte)EParameterCodeStatic.VIDEO_CURRENT_TIME];
            int totalTime = (int)time / 1000;
            int hours = totalTime / 3600;
            int minutes = (totalTime % 3600) / 60;
            int seconds = (totalTime % 3600) % 60;
            string secs = seconds.ToString();

            if (seconds < 10)
            {
                secs = "0" + seconds.ToString();
            }

            if (hours > 0)
            {
                _currentVideoTime.text = string.Format("{0}:{1}:{2} /", hours, minutes, secs);
            }
            else
            {
                _currentVideoTime.text = string.Format("{0}:{1} /", minutes, secs);
            }

            _videoSlider.SetValueWithoutNotify(totalTime / videoDuration);
        }

        public void SetVideoDuriation(MessageData data)
        {
            if (null == _videoDuration)
            {
                return;
            }

            float time = (float)data[(byte)EParameterCodeStatic.VIDEO_DURATION];

            int totalTime = (int)time / 1000;
            videoDuration = totalTime;
            int hours = totalTime / 3600;
            int minutes = (totalTime % 3600) / 60;
            int seconds = (totalTime % 3600) % 60;
            string secs = seconds.ToString();

            if (seconds < 10)
            {
                secs = "0" + seconds.ToString();
            }

            if (hours > 0)
            {
                _videoDuration.text = string.Format("{0}:{1}:{2}", hours, minutes, secs);
            }
            else
            {
                _videoDuration.text = string.Format("{0}:{1}", minutes, secs);
            }
        }

        public void TeleportItemCreated(MessageData msgData)
        {
            int itemIndex = (int)msgData[(byte)EParameterCodeToolkit.ITEM_INDEX];

            if (msgData.Parameters.ContainsKey((byte)EParameterCodeToolkit.HOTSPOT_ID) || itemIndex != _itemIndex)
            {
                return;
            }

            CTItemData itemData = (CTItemData)msgData[(byte)EParameterCodeToolkit.ITEM_DATA];

            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.MEDIA_TELEPORT_ITEM, itemData.ItemIndex.ToString());

            dispatcher.Dispatch(STEREO_EVENTS.TELEPORT_ITEM_CHANGED, data);
        }
    }
}