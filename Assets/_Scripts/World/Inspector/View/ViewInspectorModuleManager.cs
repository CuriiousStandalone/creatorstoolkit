﻿using System.IO;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using strange.extensions.mediation.impl;

using TMPro;

using SFB;

using PulseIQ.Local.ClientCommon.Components.Common.UIElements;
using PulseIQ.Local.Common.Model.World;
using PulseIQ.Local.ClientCommon.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using UIWidgets;
using PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class ViewInspectorModuleManager : EventView
    {
        [SerializeField]
        private TMP_InputField _nameTextField;
        [SerializeField]
        private TMP_InputField _descriptionTextField;
        [SerializeField]
        private TMP_InputField _maxPlayersTextField;

        [SerializeField]
        private Button _uploadButton;

        [SerializeField]
        private List<CImageToggle> _templateList;

        private readonly int _defaultImageWidth = 100;
        private readonly int _defaultImageHeight = 100;

        [SerializeField]
        private Sprite btnUploadDefaultImage;
        [SerializeField]
        private Sprite btnUploadHoverImage;
        [SerializeField]
        private Sprite btnUploadPressedImage;
        [SerializeField]
        private Sprite btnUploadDeleteImage;

        [SerializeField]
        private Toggle _playerToggle;

        [SerializeField]
        private Toggle _teacherToggle;

        [SerializeField]
        private CToggleManager _toggleManager;

        [SerializeField]
        private CImageToggle _template1Toggle;
        [SerializeField]
        private CImageToggle _template2Toggle;
        [SerializeField]
        private CImageToggle _template3Toggle;

        [SerializeField]
        private Accordion _moduleAccordion;

        [SerializeField]
        private CDropBox _thumbnailDropbox;

        internal enum InspectorModuleManagerEvents
        {
            VIEW_LOADED,
            UPDATED_NAME,
            UPDATED_DESCRIPTION,
            UPDATED_IS_MULTIPLAYER,
            TEMPLATE_CHANGED,
            THUMBNAIL_UPDATED,
            OPEN_IMAGE_LIST,
            THUMBNAIL_CHANGED,
            THUMBNAIL_CLEARED,
        }

        internal void Init()
        {
            dispatcher.Dispatch(InspectorModuleManagerEvents.VIEW_LOADED);
        }

        public void ResetModuleInspector()
        {
            SetModuleName("", true);
            SetModuleDescription("", true);
            SetModuleThumbnail("");
        }

        internal void SetModuleData(WorldData moduleData, string thumbnailPath)
        {
            switch (moduleData.WorldTemplateId)
            {
                case "Template_1":
                    {
                        _toggleManager.SelectWithoutNotify(_template1Toggle);
                        _template1Toggle.SetSelectedState();
                        _template1Toggle.IsSelected = true;
                        break;
                    }

                case "Template_2":
                    {
                        _toggleManager.SelectWithoutNotify(_template2Toggle);
                        _template2Toggle.SetSelectedState();
                        _template2Toggle.IsSelected = true;
                        break;
                    }

                case "Template_3":
                    {
                        _toggleManager.SelectWithoutNotify(_template3Toggle);
                        _template3Toggle.SetSelectedState();
                        _template3Toggle.IsSelected = true;
                        break;
                    }
            }

            _uploadButton.onClick.RemoveListener(SelectThumbnail);
            _uploadButton.onClick.RemoveListener(DeleteThumbnail);

            SetModuleName(moduleData.WorldName);
            SetModuleDescription(moduleData.WorldDescription);
            //SetModuleThumbnail(thumbnailPath);

            if (File.Exists(thumbnailPath))
            {
                Sprite currentSprite = null;

                currentSprite = LoadImage(thumbnailPath);

                FileInfo fileInfo = new FileInfo(thumbnailPath);

                _thumbnailDropbox.FillData(Path.GetFileName(thumbnailPath), Path.GetFileNameWithoutExtension(thumbnailPath), currentSprite, "IMAGE", GetFileSize(fileInfo.Length));
            }
            else
            {
                _thumbnailDropbox.ClearCurrentData();
            }

            SetModuleLeadType(moduleData.WorldInteractiveMode);
        }

        public void ThumbnailChanged(string mediaID)
        {
            if (gameObject.activeInHierarchy)
            {
                MessageData data = new MessageData();

                data.Parameters.Add((byte)EParameterCodeWorld.MODULE_THUMBNAIL_FILENAME, mediaID);

                dispatcher.Dispatch(InspectorModuleManagerEvents.THUMBNAIL_CHANGED, data);
            }
        }

        public void ThumbnailCleared()
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)EParameterCodeWorld.MODULE_THUMBNAIL_FILENAME, "");

            dispatcher.Dispatch(InspectorModuleManagerEvents.THUMBNAIL_CLEARED, data);
        }

        private void SetModuleName(string name, bool isReset = false)
        {
            if (isReset)
            {
                _nameTextField.SetTextWithoutNotify(name);
            }
            else
            {
                _nameTextField.text = name;
            }
        }

        private void SetModuleDescription(string description, bool isReset = false)
        {
            if(isReset)
            {
                _descriptionTextField.SetTextWithoutNotify(description);
            }
            else
            {
                _descriptionTextField.text = description;
            }
        }

        private void SetModuleThumbnail(string imagePath)
        {
            //if (imagePath != "" && File.Exists(imagePath))
            //{
            //    Sprite sp = LoadImage(imagePath);
            //
            //    _uploadButton.GetComponent<Image>().sprite = sp;
            //
            //    SpriteState spriteState = new SpriteState();
            //    spriteState = _uploadButton.spriteState;
            //    spriteState.pressedSprite = btnUploadDeleteImage;
            //    spriteState.highlightedSprite = btnUploadDeleteImage;
            //
            //    _uploadButton.spriteState = spriteState;
            //
            //    _uploadButton.onClick.RemoveListener(SelectThumbnail);
            //    _uploadButton.onClick.AddListener(DeleteThumbnail);
            //}
            //else
            //{
            //    _uploadButton.GetComponent<Image>().sprite = btnUploadDefaultImage;
            //
            //    SpriteState spriteState = new SpriteState();
            //    spriteState = _uploadButton.spriteState;
            //    spriteState.pressedSprite = btnUploadPressedImage;
            //    spriteState.highlightedSprite = btnUploadHoverImage;
            //
            //    _uploadButton.spriteState = spriteState;
            //
            //    _uploadButton.onClick.RemoveListener(DeleteThumbnail);
            //    _uploadButton.onClick.AddListener(SelectThumbnail);
            //}
        }

        internal void SetTemplates(List<WorldData> templateList)
        {
            WorldData templateData;
            List<Sprite> listImages = new List<Sprite>();

            //FIX FOR REMOVING REMOTE RESOURCES
            if (null == templateList || templateList.Count == 0)
            {
                Debug.Log("templateList == null, returning");
                return;
            }

            for (int i = 0; i < 3; i++)
            {
                templateData = templateList[i];

                listImages.Clear();
                listImages = LoadTemplateImage(templateData.WorldId);

                _templateList[i].Name = templateData.WorldId;
                _templateList[i].SetText(templateData.WorldName);
                _templateList[i].SetImages(listImages[0], listImages[1], listImages[2]);
            }
        }

        private List<Sprite> LoadTemplateImage(string templateId)
        {
            List<Sprite> listImages = new List<Sprite>();

            string imagePath = Path.Combine(Application.streamingAssetsPath, "Thumbnail/" + templateId + "_normal.png");

            listImages.Add(LoadImage(imagePath));

            imagePath = Path.Combine(Application.streamingAssetsPath, "Thumbnail/" + templateId + "_selected.png");

            listImages.Add(LoadImage(imagePath));

            imagePath = Path.Combine(Application.streamingAssetsPath, "Thumbnail/" + templateId + "_hover.png");

            listImages.Add(LoadImage(imagePath));

            return listImages;
        }

        private Sprite LoadImage(string imagePath)
        {
            if (imagePath != "" && File.Exists(imagePath))
            {
                Texture2D tex = null;
                byte[] fileData;

                fileData = File.ReadAllBytes(imagePath);
                tex = new Texture2D(2, 2);
                tex.LoadImage(fileData);

                return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2());
            }

            return null;
        }

        public void ModuleNameUpdated()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.MODULE_NAME, _nameTextField.text);

            dispatcher.Dispatch(InspectorModuleManagerEvents.UPDATED_NAME, data);
        }

        public void ModuleDescriptionUpdated()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.MODULE_DESCRIPTION, _descriptionTextField.text);

            dispatcher.Dispatch(InspectorModuleManagerEvents.UPDATED_DESCRIPTION, data);
        }

        public void UpdateTemplate(string templateId)
        {
            Debug.Log("UpdateTemplate: " + templateId);

            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.TEMPLATE_ID, templateId);

            dispatcher.Dispatch(InspectorModuleManagerEvents.TEMPLATE_CHANGED, data);
        }

        public void SelectThumbnail()
        {
            ExtensionFilter[] extensions = new[]
            {
                new ExtensionFilter("Images", "png")
            };

            MessageData message = new MessageData();

            string selectedFile = OpenFileBrowser(extensions);

            if (selectedFile != "")
            {
                Sprite sp = LoadImage(selectedFile);

                _uploadButton.GetComponent<Image>().sprite = sp;

                SpriteState spriteState = new SpriteState();
                spriteState = _uploadButton.spriteState;
                spriteState.pressedSprite = btnUploadDeleteImage;
                spriteState.highlightedSprite = btnUploadDeleteImage;

                _uploadButton.spriteState = spriteState;

                _uploadButton.onClick.RemoveListener(SelectThumbnail);
                _uploadButton.onClick.AddListener(DeleteThumbnail);

                MessageData msgData = new MessageData();
                msgData.Parameters.Add((byte)EParameterCodeWorld.MODULE_THUMBNAIL_PATH, selectedFile);

                dispatcher.Dispatch(InspectorModuleManagerEvents.THUMBNAIL_UPDATED, msgData);
            }
        }

        private string OpenFileBrowser(ExtensionFilter[] filters)
        {
            string[] selectedFiles = StandaloneFileBrowser.OpenFilePanel("Open File", "", filters, false);

            string selectedFile = "";

            if (selectedFiles.Length > 0)
            {
                selectedFile = selectedFiles[0];
            }

            return selectedFile;
        }

        private void DeleteThumbnail()
        {
            _uploadButton.GetComponent<Image>().sprite = btnUploadDefaultImage;

            SpriteState spriteState = new SpriteState();
            spriteState = _uploadButton.spriteState;
            spriteState.pressedSprite = btnUploadPressedImage;
            spriteState.highlightedSprite = btnUploadHoverImage;

            _uploadButton.spriteState = spriteState;

            _uploadButton.onClick.RemoveListener(DeleteThumbnail);
            _uploadButton.onClick.AddListener(SelectThumbnail);

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeWorld.MODULE_THUMBNAIL_PATH, "");

            dispatcher.Dispatch(InspectorModuleManagerEvents.THUMBNAIL_UPDATED, msgData);
        }

        public void UpdateThumbnail()
        {

        }

        public void ModuleLeadChanged(string args)
        {
            MessageData data = new MessageData();
            bool value = args == "Teacher Lead" ? true : false;
            data.Parameters.Add((byte)EParameterCodeToolkit.MODULE_IS_MULTIPLAYER, value);

            dispatcher.Dispatch(InspectorModuleManagerEvents.UPDATED_IS_MULTIPLAYER, data);
        }

        private void SetModuleLeadType(EInteractiveModeCode type)
        {
            switch (type)
            {
                case EInteractiveModeCode.PLAYER_LEAD:
                    {
                        _playerToggle.isOn = true;
                        break;
                    }

                case EInteractiveModeCode.TEACHER_LEAD:
                    {
                        _teacherToggle.isOn = true;
                        break;
                    }
            }

        }

        public void EnableModuleAccordion()
        {
            _moduleAccordion.enabled = true;
            _moduleAccordion.DataSource[0].Open = true;
        }

        public void OpenImageList()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.RESOURCE_LIST_FILTER, "Image");
            dispatcher.Dispatch(InspectorModuleManagerEvents.OPEN_IMAGE_LIST, data);
        }

        private string GetFileSize(long bytes)
        {
            if (bytes / 1024 < 1000)
            {
                return (bytes / 1024) + " KB";
            }
            else if (bytes / 1024 / 1024 < 1000)
            {
                return (bytes / 1024 / 1024) + " MB";
            }
            else
            {
                return (bytes / 1024 / 1024 / 1024) + " GB";
            }
        }
    }
}
