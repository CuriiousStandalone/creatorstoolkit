﻿using UnityEngine;

using strange.extensions.mediation.impl;

using PulseIQ.Local.Common;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIPanels;
using PulseIQ.Local.Common.Model.World;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.Common.Model.PanoramaVideo;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;
using PulseIQ.Local.Common.Model.Panorama;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class ViewInspectorManager : EventView, IResponsivePanel
    {
        internal const string VIEW_LOADED = "VIEW_LOADED";
        internal const string PANEL_HIDDEN = "PANEL_HIDDEN";
        internal const string PANEL_SHOWN = "PANEL_SHOWN";

        [SerializeField]
        private GameObject _InspectorModuleAccordion;

        [SerializeField]
        private GameObject _stereoVideoAccordion;

        [SerializeField]
        private GameObject _panoramaVideoAccordion;

        [SerializeField]
        private GameObject _panoramaAccordion;

        [SerializeField]
        private GameObject _hotspotAccordion;

        [SerializeField]
        private GameObject _moduleAccordion;

        [SerializeField]
        private GameObject _hotspotOptionsAccordian;

        private string _itemName;
        private string _itemResourceID;
        private ItemTypeCode _itemType;

        public string PanelId
        {
            get { return gameObject.GetComponent<CPanel>()._panelId; }
        }

        internal void Init()
        {
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelId);

            dispatcher.Dispatch(VIEW_LOADED, msgData);
        }

        public void ModuleSelected(WorldData worldData)
        {
            ActivateModuleItem(worldData);
        }

        public void ItemSelected(MessageData msgData)
        {
            CTItemData itemData = (CTItemData)msgData.Parameters[(byte)EParameterCodeToolkit.ITEM_DATA];

            _itemName = itemData.ItemName;
            _itemResourceID = itemData.ItemResourceId;
            _itemType = itemData.ItemType;

            switch (_itemType)
            {
                case ItemTypeCode.PANORAMA_VIDEO:
                    {
                        string targetItemName = msgData.Parameters.ContainsKey((byte)EParameterCodeToolkit.ITEM_NAME) ? (string)msgData.Parameters[(byte)EParameterCodeToolkit.ITEM_NAME] : "";
                        ActivatePanoramaVideoItem((CTPanoramaVideoData)itemData, targetItemName);
                        break;
                    }

                case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                    {
                        string targetItemName = msgData.Parameters.ContainsKey((byte)EParameterCodeToolkit.ITEM_NAME) ? (string)msgData.Parameters[(byte)EParameterCodeToolkit.ITEM_NAME] : "";
                        ActivateStereoPanoramaItem((CTStereoPanoramaVideoData)itemData, targetItemName);
                        break;
                    }

                case ItemTypeCode.PANORAMA:
                    {
                        string targetItemName = msgData.Parameters.ContainsKey((byte)EParameterCodeToolkit.ITEM_NAME) ? (string)msgData.Parameters[(byte)EParameterCodeToolkit.ITEM_NAME] : "";
                        ActivatePanoramaItem((CTPanoramaData)itemData, targetItemName);
                        break;
                    }
            }
        }

        public void HotspotSelected(CTHotspotData hotspotData, int itemIndex, ItemTypeCode type, string teleportItemName = null)
        {
            ActivateHotspotItem(hotspotData, itemIndex, type, teleportItemName);
        }

        internal void ResetInspector()
        {
            _moduleAccordion.GetComponent<ViewInspectorModuleManager>().ResetModuleInspector();
        }

        private void ResetAccordions()
        {
            _stereoVideoAccordion.SetActive(false);
            _panoramaVideoAccordion.SetActive(false);
            _panoramaAccordion.SetActive(false);
            _hotspotAccordion.SetActive(false);
            _moduleAccordion.SetActive(false);
            _hotspotOptionsAccordian.SetActive(false);
        }

        internal void ActivateHotspotOptions()
        {
            ResetAccordions();
            _hotspotOptionsAccordian.SetActive(true);
        }

        private void ActivateModuleItem(WorldData worldData)
        {
            ResetAccordions();
            _moduleAccordion.SetActive(true);
        }

        private void ActivatePanoramaItem(CTPanoramaData data, string teleportItemName)
        {
            ResetAccordions();
            _panoramaAccordion.GetComponent<ViewPanoramaManager>().PanoramaItemSelected(data, teleportItemName);
            _panoramaAccordion.SetActive(true);
        }

        private void ActivatePanoramaVideoItem(CTPanoramaVideoData data, string teleportItemName)
        {
            ResetAccordions();
            _panoramaVideoAccordion.GetComponent<ViewMonoVideoManager>().PanoramaVideoItemSelected(data, teleportItemName);
            _panoramaVideoAccordion.SetActive(true);
        }

        private void ActivateStereoPanoramaItem(CTStereoPanoramaVideoData data, string teleportItemName)
        {
            ResetAccordions();
            _stereoVideoAccordion.GetComponent<ViewStereoVideoManager>().StereoVideoItemSelected(data, teleportItemName);
            _stereoVideoAccordion.SetActive(true);
        }

        private void ActivateHotspotItem(CTHotspotData data, int itemIndex, ItemTypeCode type, string teleportItemName = null)
        {
            ResetAccordions();
            _hotspotAccordion.GetComponent<ViewHotspotManager>().HotspotSelected(data, itemIndex, type, teleportItemName);
            _hotspotAccordion.SetActive(true);
        }

        #region Panel

        public void HidePanel()
        {
            gameObject.SetActive(false);

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelId);

            dispatcher.Dispatch(PANEL_HIDDEN, msgData);
        }

        public void ShowPanel()
        {
            gameObject.SetActive(true);

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelId);

            dispatcher.Dispatch(PANEL_SHOWN, msgData);
        }

        public void ShowPanelWithTween()
        {
            Debug.Log("ViewInspectorManager.ShowPanelWithTween is not yet implemented");
        }

        public void HidePanelWithTween()
        {
            Debug.Log("ViewInspectorManager.HidePanelWithTween is not yet implemented");
        }

        public void IncreaseHeight(float height)
        {
            gameObject.GetComponent<CPanel>().IncreaseHeightEqually(height);
        }

        public void IncreaseHeightFromBottom(float height)
        {
            gameObject.GetComponent<CPanel>().IncreaseHeightFromBottom(height);
        }

        public void IncreaseHeightFromTop(float height)
        {
            gameObject.GetComponent<CPanel>().IncreaseHeightFromTop(height);
        }

        public void DecreaseHeight(float height)
        {
            gameObject.GetComponent<CPanel>().DecreaseHeightEqually(height);
        }

        public void DecreaseHeightFromBottom(float height)
        {
            gameObject.GetComponent<CPanel>().DecreaseHeightFromBottom(height);
        }

        public void DecreaseHeightFromTop(float height)
        {
            gameObject.GetComponent<CPanel>().DecreaseHeightFromTop(height);
        }

        public void IncreaseWidth(float width)
        {
            Debug.Log("ViewInspectorManager.IncreaseWidth is not yet implemented");
        }

        public void IncreaseWidthFromRight(float width)
        {
            Debug.Log("ViewInspectorManager.IncreaseWidthFromRight is not yet implemented");
        }

        public void IncreaseWidthFromLeft(float width)
        {
            Debug.Log("ViewInspectorManager.IncreaseWidthFromLeft is not yet implemented");
        }

        public void DecreaseWidth(float width)
        {
            Debug.Log("ViewInspectorManager.DecreaseWidth is not yet implemented");
        }

        public void DecreaseWidthFromRight(float width)
        {
            Debug.Log("ViewInspectorManager.DecreaseWidthFromRight is not yet implemented");
        }

        public void DecreaseWidthFromLeft(float width)
        {
            Debug.Log("ViewInspectorManager.DecreaseWidthFromLeft is not yet implemented");
        }

        public void RepositionToTop(float height)
        {
            gameObject.GetComponent<CPanel>().RepositionToTop(height);
        }

        public void RepositionToBottom(float height)
        {
            gameObject.GetComponent<CPanel>().RepositionToBottom(height);
        }

        public void RepositionToLeft(float width)
        {
            gameObject.GetComponent<CPanel>().RepositionToLeft(width);
        }

        public void RepositionToRight(float width)
        {
            gameObject.GetComponent<CPanel>().RepositionToRight(width);
        }

        #endregion
    }
}