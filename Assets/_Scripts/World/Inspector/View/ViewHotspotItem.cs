﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;
using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class ViewHotspotItem : EventView
    {
        internal enum HOTSPOT_EVENTS
        {
            HOTSPOT_SELECTED,
        }

        private int _hotspotIndex = -1;
        private int _itemIndex = -1;
        private Transform _lookAtTransform;
        private float _drawDistance = 0f;

        private float _xRotation;
        private float _yRotation;

        internal void init()
        {

        }

        private void Update()
        {

        }

        public void SetHotspotElementData(Transform camera, Vector3 elementPosition, int itemIndex, int hotspotIndex, int elementIndex, ItemTypeCode code, float hsDrawDistance)
        {
            _itemIndex = itemIndex;
            _hotspotIndex = hotspotIndex;
            transform.position = elementPosition;
            _lookAtTransform = camera;
            _drawDistance = hsDrawDistance;

            if (code == ItemTypeCode.PANORAMA)
            {
                transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
            }

            Vector3 direction = elementPosition - camera.position;
            SetXYRotations(direction);
        }

        public void HotspotDeleted(MessageData data)
        {
            int hotspotIndex = (int)data[(byte)EParameterCodeToolkit.HOTSPOT_ID];

            if (hotspotIndex == _hotspotIndex)
            {
                Destroy(gameObject);
            }
        }

        public void ClosedEditing()
        {
            Destroy(gameObject);
        }

        public void UpdatePosition(MessageData data)
        {
            int hotspotID = (int)data[(byte)EParameterCodeToolkit.HOTSPOT_ID];
            Vector2 xy = (Vector2)data[(byte)EParameterCodeToolkit.HOTSPOT_XY_POSITION];

            if (_hotspotIndex == hotspotID)
            {
                _xRotation += xy.x;
                _yRotation += xy.y;

                Quaternion quater = Quaternion.Euler(_xRotation, _yRotation, _lookAtTransform.eulerAngles.z);
                Vector3 elelmentPosition = quater * (Vector3.forward * _drawDistance);
                transform.position = _lookAtTransform.position + elelmentPosition;
            }
        }

        public void SetPosition(MessageData data)
        {
            int hotspotID = (int)data[(byte)EParameterCodeToolkit.HOTSPOT_ID];
            Vector2 xy = (Vector2)data[(byte)EParameterCodeToolkit.HOTSPOT_XY_POSITION];

            if (_hotspotIndex == hotspotID)
            {
                _xRotation = xy.x;
                _yRotation = xy.y;

                Quaternion quater = Quaternion.Euler(_xRotation, _yRotation, _lookAtTransform.eulerAngles.z);
                Vector3 hotspotPosition = quater * (Vector3.forward * _drawDistance);
                transform.position = _lookAtTransform.position + hotspotPosition;
            }
        }

        public void HotspotSelected()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ID, _hotspotIndex);

            dispatcher.Dispatch(HOTSPOT_EVENTS.HOTSPOT_SELECTED, data);
        }

        public Vector2 GetXYPosition()
        {
            return new Vector2(_xRotation, _yRotation);
        }

        public void SetXYRotations(Vector3 direction)
        {
            Quaternion lookAtRotation = Quaternion.LookRotation(direction, Vector3.forward);
            _xRotation = lookAtRotation.eulerAngles.x;
            _yRotation = lookAtRotation.eulerAngles.y;
        }

        public int GetHotspotIndex()
        {
            return _hotspotIndex;
        }

        public int GetItemIndex()
        {
            return _itemIndex;
        }
    }
}