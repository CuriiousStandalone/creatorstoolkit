﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

using strange.extensions.mediation.impl;

using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.ClientCommon.Components.Common;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common.UIElements;
using PulseIQ.Local.Common.Model.Hotspot.HotspotElement;
using UIWidgets;
using PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents;
using System.Text.RegularExpressions;
using System.IO;
using System;
using PulseIQ.Local.Common;
using PulseIQ.Local.Common.Model.Item;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Inspector
{
    public class ViewHotspotManager : EventView
    {
        internal enum HOTSPOT_EVENTS
        {
            NAME_CHANGED,
            IS_ACTIVE_CHANGED,
            DELETE_PRESSED,
            DEACTIVATE_TIME_CHANGED,
            ACTIVATE_TIME_CHANGED,
            TRIGGER_STATE_CHANGED,
            TRIGGER_EVENT_CHANGED,
            STATUS_ICON_CHANGED,
            REQUEST_DEFAULT_ICON_IDS,
            OPEN_ICON_RESOURCE_LIST,
            TELEPORT_ITEM_CHANGED,
            TELEPORT_ITEM_CREATED,
        }

        private string _hotspotID;
        private int _itemIndex;
        EHotspotTriggerEventCode _triggerEventCode;
        EHotspotTriggerCode _triggerCode;

        [SerializeField]
        private TMP_InputField _hotspotName;

        [SerializeField]
        private Toggle _hideHotspotToggle;

        private int _videoDuration;

        [SerializeField]
        private TMP_InputField _deactivateTime;
        private int _currentDeactiveTime;

        [SerializeField]
        private TMP_InputField _activateTime;
        private int _currentActiveTime;

        [SerializeField]
        private CDropdown _triggerState;

        [SerializeField]
        private CDropdown _triggerEvent;

        [Space]
        [SerializeField]
        private Accordion _hotspotAccordion;

        [Space]
        [SerializeField]
        private CDropBox _defaultStatusDropbox;

        [SerializeField]
        private CDropBox _hoverStatusDropbox;

        [SerializeField]
        private CDropBox _triggerStatusDropbox;

        [SerializeField]
        private TMP_Text _teleportItemText;

        private List<GameObject> _accordionGameobjects = new List<GameObject>();

        private Dictionary<string, int> hotspotTypeDictionary = new Dictionary<string, int>();

        private string _defaultDefaultIconID = "";
        private string _defaultHoverIconID = "";
        private string _defaultTriggerIconID = "";

        internal void init()
        {

        }

        public void HotspotSelected(CTHotspotData hotspotData, int itemIndex, ItemTypeCode type, string teleportItemName = null)
        {
            if (_hotspotID == hotspotData.HotspotId)
            {
                return;
            }

            if (_hotspotAccordion.DataSource.Count > 1)
            {
                for (int i = 0; i < _hotspotAccordion.DataSource.Count; ++i)
                {
                    if (i == 0)
                    {
                        continue;
                    }

                    _hotspotAccordion.DataSource.RemoveAt(i);
                }

                for (int i = 0; i < _accordionGameobjects.Count; ++i)
                {
                    Destroy(_accordionGameobjects[i]);
                }

                _accordionGameobjects.Clear();
            }

            MessageData dataa = new MessageData();
            dataa.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_DATA, hotspotData);

            dispatcher.Dispatch(HOTSPOT_EVENTS.REQUEST_DEFAULT_ICON_IDS, dataa);

            _itemIndex = itemIndex;
            _hotspotID = hotspotData.HotspotId;
            _hotspotName.SetTextWithoutNotify(hotspotData.HotspotName);
            _hideHotspotToggle.SetIsOnWithoutNotify(!hotspotData.IsEnabledByDefault);
            _currentDeactiveTime = hotspotData.DeactiveTimeInMillisec / 1000;
            _deactivateTime.SetTextWithoutNotify(_currentDeactiveTime.ToString());
            _currentActiveTime = hotspotData.ActiveTimeInMillisec / 1000;
            _activateTime.SetTextWithoutNotify(_currentActiveTime.ToString());
            _triggerCode = hotspotData.HotspotTriggerCode;
            _triggerEventCode = hotspotData.HotspotTriggerEventCode;

            if(!string.IsNullOrEmpty(teleportItemName))
            {
                _teleportItemText.text = teleportItemName;
            }
            else
            {
                _teleportItemText.transform.parent.GetComponent<CDropBox>().ClearCurrentData();
            }

            int index = -1;
            index = GetIndexByName(_triggerState.GetDropdown(), ReturnTriggerCode(_triggerCode));
            _triggerState.GetDropdown().SetValueWithoutNotify(index);

            index = -1;
            index = GetIndexByName(_triggerEvent.GetDropdown(), ReturnTriggerEventCode(_triggerEventCode));
            _triggerEvent.GetDropdown().SetValueWithoutNotify(index);

            if (type == ItemTypeCode.PANORAMA)
            {
                _activateTime.enabled = false;
                _deactivateTime.enabled = false;
            }
            else
            {
                _activateTime.enabled = true;
                _deactivateTime.enabled = true;
            }

            foreach (CTHotspotStatusData statusData in hotspotData.ListHotspotStatus)
            {
                if (statusData.StatusIconId != "")
                {
                    switch (statusData.StatusTypeCode)
                    {
                        case EHotspotStatusTypeCode.DEFAULT:
                            {
                                string imagePath = PrefabNameConstants.HOTSPOT_ICON + statusData.StatusIconId;
                                _defaultStatusDropbox.FillData(statusData.StatusIconId, statusData.StatusId, LoadImage(imagePath, 52 / 100, 52 / 100));
                                break;
                            }

                        case EHotspotStatusTypeCode.HOVER:
                            {
                                string imagePath = PrefabNameConstants.HOTSPOT_ICON + statusData.StatusIconId;

                                _hoverStatusDropbox.FillData(statusData.StatusIconId, statusData.StatusId, LoadImage(imagePath, 52 / 100, 52 / 100));
                                break;
                            }

                        case EHotspotStatusTypeCode.TRIGGER:
                            {
                                string imagePath = PrefabNameConstants.HOTSPOT_ICON + statusData.StatusIconId;

                                _triggerStatusDropbox.FillData(statusData.StatusIconId, statusData.StatusId, LoadImage(imagePath, 52 / 100, 52 / 100));
                                break;
                            }
                    }
                }
                else
                {
                    switch (statusData.StatusTypeCode)
                    {
                        case EHotspotStatusTypeCode.DEFAULT:
                            {
                                _defaultStatusDropbox.ClearCurrentData();
                                break;
                            }

                        case EHotspotStatusTypeCode.HOVER:
                            {
                                _hoverStatusDropbox.ClearCurrentData();
                                break;
                            }

                        case EHotspotStatusTypeCode.TRIGGER:
                            {
                                _triggerStatusDropbox.ClearCurrentData();
                                break;
                            }
                    }
                }
            }

            if (hotspotData.HotspotBoxes.Count > 0)
            {
                SpawnHotspotElementPrefabs(hotspotData.HotspotBoxes[0]);
            }
        }

        public int GetIndexByName(TMP_Dropdown dropDown, string name)
        {
            if (dropDown == null)
            {
                return -1;
            }

            if (string.IsNullOrEmpty(name) == true)
            {
                return -1;
            }

            List<TMP_Dropdown.OptionData> list = dropDown.options;

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].text.Equals(name)) { return i; }
            }

            return -1;
        }

        public void HotspotNameChanged()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_NAME, _hotspotName.text);
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_INDEX, _hotspotID);

            dispatcher.Dispatch(HOTSPOT_EVENTS.NAME_CHANGED, data);
        }

        public void HotspotIsActiveChanged()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_IS_ACTIVE, !_hideHotspotToggle.isOn);
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_INDEX, _hotspotID);

            dispatcher.Dispatch(HOTSPOT_EVENTS.IS_ACTIVE_CHANGED, data);
        }

        public void DeleteHotspot()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ID, _hotspotID);
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);

            dispatcher.Dispatch(HOTSPOT_EVENTS.DELETE_PRESSED, data);
        }

        public void HotspotDeactivateTimeChanged()
        {
            if (_deactivateTime.text == "" || _deactivateTime.text == "-")
            {
                _currentDeactiveTime = 0;
            }
            else
            {
                _currentDeactiveTime = int.Parse(_deactivateTime.text);
            }

            if (_currentDeactiveTime < _currentActiveTime)
            {
                _currentDeactiveTime = _currentActiveTime;
            }

            if (_currentDeactiveTime < 0)
            {
                _currentDeactiveTime = 0;
            }

            if (_currentDeactiveTime > _videoDuration)
            {
                _currentDeactiveTime = _videoDuration;
            }

            _deactivateTime.text = _currentDeactiveTime.ToString();

            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_DEACTIVATE_TIME, _currentDeactiveTime * 1000);
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_INDEX, _hotspotID);

            dispatcher.Dispatch(HOTSPOT_EVENTS.DEACTIVATE_TIME_CHANGED, data);
        }

        public void IncreaseDeactiveTime()
        {
            if (!_deactivateTime.enabled)
            {
                return;
            }

            ++_currentDeactiveTime;

            if (_currentDeactiveTime > _videoDuration)
            {
                _currentDeactiveTime = _videoDuration;
            }

            _deactivateTime.text = _currentDeactiveTime.ToString();
        }

        public void DecreaseDeactiveTime()
        {
            if (!_deactivateTime.enabled)
            {
                return;
            }

            --_currentDeactiveTime;

            if (_currentDeactiveTime < 0)
            {
                _currentDeactiveTime = 0;
            }

            if (_currentDeactiveTime < _currentActiveTime)
            {
                _currentDeactiveTime = _currentActiveTime;
            }

            _deactivateTime.text = _currentDeactiveTime.ToString();
        }

        public void IncreaseActiveTime()
        {
            if (!_activateTime.enabled)
            {
                return;
            }

            ++_currentActiveTime;

            if (_currentActiveTime > _currentDeactiveTime)
            {
                _currentActiveTime = _currentDeactiveTime;
            }

            _activateTime.text = _currentActiveTime.ToString();
        }

        public void DecreaseActiveTime()
        {
            if (!_activateTime.enabled)
            {
                return;

            }
            --_currentActiveTime;

            if (_currentActiveTime < 0)
            {
                _currentActiveTime = 0;
            }

            _activateTime.text = _currentActiveTime.ToString();
        }

        public void HotspotActivateTimeChanged()
        {
            if (_activateTime.text == "" || _activateTime.text == "-")
            {
                _currentActiveTime = 0;
            }
            else
            {
                _currentActiveTime = int.Parse(_activateTime.text);
            }

            _currentActiveTime = int.Parse(_activateTime.text);

            if (_currentActiveTime < 0)
            {
                _currentActiveTime = 0;
            }

            if (_currentActiveTime > _currentDeactiveTime)
            {
                _currentActiveTime = _currentDeactiveTime;
            }

            if (_currentActiveTime > _videoDuration)
            {
                _currentActiveTime = _videoDuration;
            }

            _activateTime.SetTextWithoutNotify(_currentActiveTime.ToString());

            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ACTIVATE_TIME, _currentActiveTime * 1000);
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_INDEX, _hotspotID);

            dispatcher.Dispatch(HOTSPOT_EVENTS.ACTIVATE_TIME_CHANGED, data);
        }

        public void HotspotTriggerStateChanged()
        {
            _triggerCode = ReturnTriggerCode(_triggerState.GetDropdownStringValue());

            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_TRIGGER_STATE, ReturnTriggerCode(_triggerState.GetDropdownStringValue()));
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_INDEX, _hotspotID);

            dispatcher.Dispatch(HOTSPOT_EVENTS.TRIGGER_STATE_CHANGED, data);
        }

        public void HotspotTriggerEventChanged()
        {
            _triggerEventCode = ReturnTriggerEventCode(_triggerEvent.GetDropdownStringValue());

            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_TRIGGER_EVENT, _triggerEventCode);
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_INDEX, _hotspotID);

            dispatcher.Dispatch(HOTSPOT_EVENTS.TRIGGER_EVENT_CHANGED, data);
        }

        private EHotspotTriggerEventCode ReturnTriggerEventCode(string triggerEventCode)
        {
            switch (triggerEventCode)
            {
                case "Enable Trigger State":
                    {
                        return EHotspotTriggerEventCode.ENABLE_TRIGGER_STATE;
                    }

                case "Teleport":
                    {
                        return EHotspotTriggerEventCode.TELEPORT;
                    }

                default:
                    {
                        return EHotspotTriggerEventCode.TELEPORT;
                    }
            }
        }

        private string ReturnTriggerEventCode(EHotspotTriggerEventCode triggerEventCode)
        {
            switch (triggerEventCode)
            {
                case EHotspotTriggerEventCode.ENABLE_TRIGGER_STATE:
                    {
                        return "Enable Trigger State";
                    }

                case EHotspotTriggerEventCode.TELEPORT:
                    {
                        return "Teleport";
                    }

                default:
                    {
                        return "Teleport";
                    }
            }
        }

        private EHotspotTriggerCode ReturnTriggerCode(string triggerCode)
        {
            switch (triggerCode)
            {
                case "Gaze":
                    {
                        return EHotspotTriggerCode.GAZE;
                    }

                case "Hover":
                    {
                        return EHotspotTriggerCode.HOVER;
                    }

                case "Select":
                    {
                        return EHotspotTriggerCode.SELECT;
                    }

                case "Vote":
                    {
                        return EHotspotTriggerCode.VOTE;
                    }

                default:
                    {
                        return EHotspotTriggerCode.GAZE;
                    }
            }
        }

        private string ReturnTriggerCode(EHotspotTriggerCode triggerCode)
        {
            switch (triggerCode)
            {
                case EHotspotTriggerCode.GAZE:
                    {
                        return "Gaze";
                    }

                case EHotspotTriggerCode.HOVER:
                    {
                        return "Hover";
                    }

                case EHotspotTriggerCode.SELECT:
                    {
                        return "Select";
                    }

                case EHotspotTriggerCode.VOTE:
                    {
                        return "Vote";
                    }

                default:
                    {
                        return "Gaze";
                    }
            }
        }

        private void SpawnHotspotElementPrefabs(CTHotspotBoxData boxData)
        {
            if (boxData.Elements.Count == 1)
            {
                switch (boxData.Elements[0].TypeCode)
                {
                    case EHotspotElementTypeCode.AUDIO:
                        {
                            CTHotspotElementAudioData audioData = (CTHotspotElementAudioData)boxData.Elements[0];

                            GameObject header = GameObject.Instantiate(Resources.Load("Prefabs/UI/HotspotElements/TempHeader"), transform) as GameObject;
                            header.transform.GetChild(0).GetComponent<TMP_Text>().text = "AUDIO";
                            header.GetComponentInChildren<RotateOnToggle>().Accordion = _hotspotAccordion;

                            GameObject body = GameObject.Instantiate(Resources.Load("Prefabs/UI/HotspotElements/Audio"), transform) as GameObject;
                            body.GetComponent<ViewHotspotAudioElement>().SetElementData(audioData, _itemIndex, _hotspotID);

                            AccordionItem newItem = new AccordionItem()
                            {
                                ToggleObject = header,
                                ContentObject = body,
                                Open = true,
                            };

                            _hotspotAccordion.DataSource.Add(newItem);
                            _accordionGameobjects.Add(header);
                            _accordionGameobjects.Add(body);

                            break;
                        }

                    case EHotspotElementTypeCode.IMAGE:
                        {
                            CTHotspotElementImageData imageData = (CTHotspotElementImageData)boxData.Elements[0];

                            GameObject header = GameObject.Instantiate(Resources.Load("Prefabs/UI/HotspotElements/TempHeader"), transform) as GameObject;
                            header.transform.GetChild(0).GetComponent<TMP_Text>().text = "IMAGE";
                            header.GetComponentInChildren<RotateOnToggle>().Accordion = _hotspotAccordion;

                            GameObject body = GameObject.Instantiate(Resources.Load("Prefabs/UI/HotspotElements/Image"), transform) as GameObject;
                            body.GetComponent<ViewHotspotImageElement>().SetElementData(imageData, _itemIndex, _hotspotID);

                            AccordionItem newItem = new AccordionItem()
                            {
                                ToggleObject = header,
                                ContentObject = body,
                                Open = true,
                            };

                            _hotspotAccordion.DataSource.Add(newItem);
                            _accordionGameobjects.Add(header);
                            _accordionGameobjects.Add(body);

                            break;
                        }

                    case EHotspotElementTypeCode.TEXT:
                        {
                            CTHotspotElementTextData textData = (CTHotspotElementTextData)boxData.Elements[0];

                            GameObject header = GameObject.Instantiate(Resources.Load("Prefabs/UI/HotspotElements/TempHeader"), transform) as GameObject;
                            header.transform.GetChild(0).GetComponent<TMP_Text>().text = "TEXT";
                            header.GetComponentInChildren<RotateOnToggle>().Accordion = _hotspotAccordion;

                            GameObject body = GameObject.Instantiate(Resources.Load("Prefabs/UI/HotspotElements/Text"), transform) as GameObject;
                            body.GetComponent<ViewHotspotTextElement>().SetElementData(textData, _itemIndex, _hotspotID);

                            AccordionItem newItem = new AccordionItem()
                            {
                                ToggleObject = header,
                                ContentObject = body,
                                Open = true,
                            };

                            _hotspotAccordion.DataSource.Add(newItem);
                            _accordionGameobjects.Add(header);
                            _accordionGameobjects.Add(body);

                            break;
                        }
                }
            }
            else
            {
                GameObject header = Instantiate(Resources.Load("Prefabs/UI/HotspotElements/TempHeader"), transform) as GameObject;
                header.transform.GetChild(0).GetComponent<TMP_Text>().text = "INFO PANEL";
                header.GetComponentInChildren<RotateOnToggle>().Accordion = _hotspotAccordion;

                GameObject body = Instantiate(Resources.Load("Prefabs/UI/HotspotElements/InfoPanel"), transform) as GameObject;
                body.GetComponent<ViewHotspotPanelElement>().SetElementData(boxData, _itemIndex, _hotspotID);

                AccordionItem newItem = new AccordionItem()
                {
                    ToggleObject = header,
                    ContentObject = body,
                    Open = true,
                };

                _hotspotAccordion.DataSource.Add(newItem);
                _accordionGameobjects.Add(header);
                _accordionGameobjects.Add(body);
            }
        }

        public void DefaultStatusChanged(string iconID)
        {
            string id = string.Empty;

            for (int i = 0; i < iconID.Length; i++)
            {
                if (Char.IsDigit(iconID[i]))
                    id += iconID[i];
            }

            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_INDEX, _hotspotID);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ICON_ID, id);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_STATUS_TYPE, EHotspotStatusTypeCode.DEFAULT);

            dispatcher.Dispatch(HOTSPOT_EVENTS.STATUS_ICON_CHANGED, data);
        }

        public void HoverStatusChanged(string iconID)
        {
            string id = string.Empty;

            for (int i = 0; i < iconID.Length; i++)
            {
                if (Char.IsDigit(iconID[i]))
                    id += iconID[i];
            }

            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_INDEX, _hotspotID);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ICON_ID, id);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_STATUS_TYPE, EHotspotStatusTypeCode.HOVER);

            dispatcher.Dispatch(HOTSPOT_EVENTS.STATUS_ICON_CHANGED, data);
        }

        public void TriggerStatusChanged(string iconID)
        {
            string id = string.Empty;

            for (int i = 0; i < iconID.Length; i++)
            {
                if (Char.IsDigit(iconID[i]))
                    id += iconID[i];
            }

            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_INDEX, _hotspotID);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ICON_ID, id);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_STATUS_TYPE, EHotspotStatusTypeCode.TRIGGER);

            dispatcher.Dispatch(HOTSPOT_EVENTS.STATUS_ICON_CHANGED, data);
        }

        public void DefaultStatusCleared()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_INDEX, _hotspotID);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ICON_ID, _defaultDefaultIconID);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_STATUS_TYPE, EHotspotStatusTypeCode.DEFAULT);

            dispatcher.Dispatch(HOTSPOT_EVENTS.STATUS_ICON_CHANGED, data);
        }

        public void HoverStatusCleared()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_INDEX, _hotspotID);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ICON_ID, _defaultHoverIconID);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_STATUS_TYPE, EHotspotStatusTypeCode.HOVER);

            dispatcher.Dispatch(HOTSPOT_EVENTS.STATUS_ICON_CHANGED, data);
        }

        public void TriggerStatusCleared()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_INDEX, _hotspotID);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ICON_ID, _defaultTriggerIconID);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_STATUS_TYPE, EHotspotStatusTypeCode.TRIGGER);

            dispatcher.Dispatch(HOTSPOT_EVENTS.STATUS_ICON_CHANGED, data);
        }

        private Sprite LoadImage(string imagePath, float canvasWidth, float canvasHeight)
        {
            if (imagePath != "")
            {
                Sprite theSprite = Resources.Load<Sprite>(imagePath);
                return theSprite;
            }

            return null;
        }

        public void SetVideoDuration(int duration)
        {
            _videoDuration = duration / 1000;
        }

        /// <summary>
        /// Sets default Icon ID's. List is hard coded to be in order of default, hover and then trigger.
        /// </summary>
        /// <param name="data"></param>
        public void SetDefaultIconIDs(MessageData data)
        {
            List<string> iconIDs = (List<string>)data[(byte)EParameterCodeToolkit.HOTSPOT_ICON_ID];

            _defaultDefaultIconID = iconIDs[0];
            _defaultHoverIconID = iconIDs[1];
            _defaultTriggerIconID = iconIDs[2];
        }

        public void OpenHotspotIconList()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.RESOURCE_LIST_FILTER, "Hotspot Icon");
            dispatcher.Dispatch(HOTSPOT_EVENTS.OPEN_ICON_RESOURCE_LIST, data);
        }

        public void ItemTeleportAdded(CDropBox dropBox)
        { 
            if (dropBox.IsNewDroppedItem())
            {
                MessageData data = new MessageData();
                data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
                data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ID, _hotspotID);
                data.Parameters.Add((byte)EParameterCodeToolkit.MEDIA_TELEPORT_ITEM, dropBox.CurrentDataId);
                data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_TYPE, dropBox.GetDroppedItemType());

                dispatcher.Dispatch(HOTSPOT_EVENTS.TELEPORT_ITEM_CREATED, data);
            }
            else
            {
                MessageData data = new MessageData();
                data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
                data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_INDEX, _hotspotID);
                data.Parameters.Add((byte)EParameterCodeToolkit.MEDIA_TELEPORT_ITEM, dropBox.CurrentDataId);

                dispatcher.Dispatch(HOTSPOT_EVENTS.TELEPORT_ITEM_CHANGED, data);
            }
        }

        public void ItemTeleportCleared()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_INDEX, _hotspotID);
            data.Parameters.Add((byte)EParameterCodeToolkit.MEDIA_TELEPORT_ITEM, "");

            dispatcher.Dispatch(HOTSPOT_EVENTS.TELEPORT_ITEM_CHANGED, data);
        }

        public void TeleportItemCreated(MessageData msgData)
        {
            int itemIndex = (int)msgData[(byte)EParameterCodeToolkit.ITEM_INDEX];
            string hotspotID = (string)msgData[(byte)EParameterCodeToolkit.HOTSPOT_ID];

            if (hotspotID != _hotspotID || itemIndex != _itemIndex)
            {
                return;
            }

            CTItemData itemData = (CTItemData)msgData[(byte)EParameterCodeToolkit.ITEM_DATA];

            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _itemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_INDEX, _hotspotID);
            data.Parameters.Add((byte)EParameterCodeToolkit.MEDIA_TELEPORT_ITEM, itemData.ItemIndex.ToString());

            dispatcher.Dispatch(HOTSPOT_EVENTS.TELEPORT_ITEM_CHANGED, data);
        }
    }
}