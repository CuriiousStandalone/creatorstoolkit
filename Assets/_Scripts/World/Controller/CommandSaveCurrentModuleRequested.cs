﻿using System.IO;

using strange.extensions.command.impl;

using PulseIQ.Local.Common.Model.World;
using PulseIQ.Local.ClientCommon.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandSaveCurrentModuleRequested : EventCommand
    {
        [Inject]
        public IModelActiveWorld WorldModel { get; set; }

        public override void Execute()
        {
            string parentPath = Directory.GetParent(Settings.WorldPath).Parent.FullName;
            string filePath = Path.Combine(parentPath, "Module.ctk");

            if (File.Exists(filePath))
            {
                WorldDataSerializer.ReadDataByFullPath(filePath, out WorldDataXML worldDataXml);

                WorldData worldData = new WorldData(worldDataXml);

                if(worldData.WorldId == WorldModel.ActiveWorldData.WorldId)
                {
                    File.Delete(filePath);

                    WorldDataSerializer.WriteByData(WorldModel.ActiveWorldData, filePath);

                    string localThumbnailPath = Path.GetFullPath(Path.Combine(Settings.WorldThumbnailPath, "Module.png"));
                    string savedThumbnailPath = string.IsNullOrEmpty(WorldModel.ThumbnailPath) ? "" : Path.GetFullPath(WorldModel.ThumbnailPath);

                    if(savedThumbnailPath == "")
                    {
                        if (File.Exists(localThumbnailPath))
                        {
                            File.Delete(localThumbnailPath);
                        }
                    }
                    else if(localThumbnailPath != savedThumbnailPath)
                    {
                        if(File.Exists(localThumbnailPath))
                        {
                            File.Delete(localThumbnailPath);
                        }

                        if (File.Exists(savedThumbnailPath))
                        {
                            if (!Directory.Exists(Settings.WorldThumbnailPath))
                            {
                                Directory.CreateDirectory(Settings.WorldThumbnailPath);
                            }

                            File.Copy(savedThumbnailPath, localThumbnailPath);
                        }
                    }

                    dispatcher.Dispatch(EEventToolkitCrossContext.REFRESH_MODULE_LIST);
                }
                else
                {
                    MessageData msgData = new MessageData();

                    msgData.Parameters.Add((byte)EParameterCodeWorld.WORLD_NAME, WorldModel.ActiveWorldData.WorldName);

                    dispatcher.Dispatch(EWorldEvent.SAVE_MODULE_AS, msgData);
                }
            }
            else
            {
                MessageData msgData = new MessageData();

                msgData.Parameters.Add((byte)EParameterCodeWorld.WORLD_NAME, WorldModel.ActiveWorldData.WorldName);

                dispatcher.Dispatch(EWorldEvent.SAVE_MODULE_AS, msgData);
            }

            HideMainMenuPanel();
        }

        private void HideMainMenuPanel()
        {
            MessageData hideMainMenuPanelData = new MessageData();
            hideMainMenuPanelData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelNameConstants.MAIN_MENU);

            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, hideMainMenuPanelData);

            MessageData hideModulePanelData = new MessageData();
            hideModulePanelData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelNameConstants.MODULES_PANEL);

            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, hideModulePanelData);
        }
    }
}
