﻿using strange.extensions.command.impl;

using PulseIQ.Local.ClientCommon.Components.Common;

using PulseIQ.Local.Common.Model.Panorama;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.Common.Model.PanoramaVideo;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;
using UnityEngine;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.Common.Model.Item;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandSetHotspotXY : EventCommand
    {
        [Inject]
        public IModelActiveWorld model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;
            int index = (int)data[(byte)EParameterCodeToolkit.ITEM_INDEX];
            string hotspotID = (string)data[(byte)EParameterCodeToolkit.HOTSPOT_ID];
            Vector2 xyPos = (Vector2)data[(byte)EParameterCodeToolkit.HOTSPOT_XY_POSITION];

            foreach (CTItemData item in model.ActiveWorldData.ListItems)
            {
                if (item.ItemIndex == index)
                {
                    switch (item.ItemType)
                    {
                        case PulseIQ.Local.Common.ItemTypeCode.PANORAMA:
                            {
                                foreach (CTHotspotData hsData in ((CTPanoramaData)item).ListHotspot)
                                {
                                    if (hsData.HotspotId == hotspotID)
                                    {
                                        hsData.HotspotLocalPostionX = xyPos.x;
                                        hsData.HotspotLocalPostionY = xyPos.y;
                                    }
                                }
                                break;
                            }

                        case PulseIQ.Local.Common.ItemTypeCode.PANORAMA_VIDEO:
                            {
                                foreach (CTHotspotData hsData in ((CTPanoramaVideoData)item).ListHotspot)
                                {
                                    if (hsData.HotspotId == hotspotID)
                                    {
                                        hsData.HotspotLocalPostionX = xyPos.x;
                                        hsData.HotspotLocalPostionY = xyPos.y;
                                    }
                                }
                                break;
                            }

                        case PulseIQ.Local.Common.ItemTypeCode.STEREO_PANORAMA_VIDEO:
                            {
                                foreach (CTHotspotData hsData in ((CTStereoPanoramaVideoData)item).ListHotspot)
                                {
                                    if (hsData.HotspotId == hotspotID)
                                    {
                                        hsData.HotspotLocalPostionX = xyPos.x;
                                        hsData.HotspotLocalPostionY = xyPos.y;
                                    }
                                }
                                break;
                            }
                    }
                }
            }
        }
    }
}
