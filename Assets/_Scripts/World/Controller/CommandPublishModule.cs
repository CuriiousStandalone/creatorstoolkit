﻿using System.IO;
using System.Collections.Generic;

using strange.extensions.command.impl;

using PulseIQ.Local.Service;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.World;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandPublishModule : EventCommand
    {
        [Inject]
        public IModelActiveWorld WorldModel { get; set; }

        private ContentAccess _content;

        public override void Execute()
        {
            Retain();

            _content = new ContentAccess(GlobalSettings.CMSURL);

            List<WorldInfoData> remoteWorlds = _content.World.GetAll();

            foreach(var remoteWorld in remoteWorlds)
            {
                if(remoteWorld.WorldId == WorldModel.ActiveWorldData.WorldId || remoteWorld.WorldName == WorldModel.ActiveWorldData.WorldName)
                {
                    dispatcher.Dispatch(EWorldEvent.PUBLISH_MODULE_NAME_EXIST_LOCALLY);

                    Release();

                    return;
                }
            }

            string parentPath = Directory.GetParent(Settings.WorldPath).Parent.FullName;
            string worldPath = Path.Combine(parentPath, "Module.ctk");

            if (File.Exists(worldPath))
            {
                File.Delete(worldPath);
            }

            WorldDataSerializer.WriteByData(WorldModel.ActiveWorldData, worldPath);

            string localThumbnailPath = Path.GetFullPath(Path.Combine(Settings.WorldThumbnailPath, "Module.png"));
            string savedThumbnailPath = WorldModel.ThumbnailPath != "" ? Path.GetFullPath(WorldModel.ThumbnailPath) : WorldModel.ThumbnailPath;

            if (savedThumbnailPath == "")
            {
                if (File.Exists(localThumbnailPath))
                {
                    File.Delete(localThumbnailPath);
                }
            }
            else if (localThumbnailPath != savedThumbnailPath)
            {
                if (File.Exists(localThumbnailPath))
                {
                    File.Delete(localThumbnailPath);
                }

                if (File.Exists(savedThumbnailPath))
                {
                    File.Copy(savedThumbnailPath, localThumbnailPath);
                }
            }

            dispatcher.Dispatch(EEventToolkitCrossContext.REFRESH_MODULE_LIST);

            WorldDataSerializer.ReadDataByFullPath(worldPath, out WorldDataXML worldDataXml);

            MessageData msgData = (MessageData)evt.data;

            bool isDirectory = (bool)msgData[(byte)EParameterCodeWorld.IS_DIRECTORY];
            string directoryId = (string)msgData[(byte)EParameterCodeWorld.DIRECTORY_ID];

            UploadWorld(worldDataXml, isDirectory, directoryId);
        }

        private async void UploadWorld(WorldDataXML worldDataXml, bool isDirectory, string directoryId)
        {
            await _content.World.UploadWorldAsync(worldDataXml, isDirectory, directoryId);

            UploadThumbnail();
        }

        private async void UploadThumbnail()
        {
            if(WorldModel.ThumbnailPath != "" && File.Exists(WorldModel.ThumbnailPath))
            {
                await _content.World.UploadThumbnailAsync(WorldModel.ActiveWorldData.WorldId, WorldModel.ThumbnailPath);
            }

            dispatcher.Dispatch(EWorldEvent.PUBLISH_MODULE_SUCCESS);

            Release();
        }
    }
}
