﻿using strange.extensions.command.impl;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandSaveCurrentModuleAsRequested : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EWorldEvent.SHOW_SAVE_MODULE_AS_DIALOG);

            HideMainMenuPanel();
        }

        private void HideMainMenuPanel()
        {
            MessageData hideMainMenuPanelData = new MessageData();
            hideMainMenuPanelData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelNameConstants.MAIN_MENU);

            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, hideMainMenuPanelData);

            MessageData hideModulePanelData = new MessageData();
            hideModulePanelData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelNameConstants.MODULES_PANEL);

            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, hideModulePanelData);
        }
    }
}
