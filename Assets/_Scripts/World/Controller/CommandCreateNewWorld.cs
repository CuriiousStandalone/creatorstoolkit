﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

using strange.extensions.command.impl;

using SFB;

using PulseIQ.Local.Common.Model.World;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.SpawnPoint;
using PulseIQ.Local.Common.Model.LineNotation;
using PulseIQ.Local.Common.CustomType;
using PulseIQ.Local.ClientCommon.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.World.Inspector;
using CuriiousIQ.Local.CreatorsToolkit.World.WorldHierarchy;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandCreateNewWorld : EventCommand
    {
        [Inject]
        public IModelActiveWorld ActiveWorldModel { get; set; }

        private bool _worldReset = false;
        private bool _inspectorPanelReset = false;
        private bool _worldHierarchyReset = false;

        private MessageData msgData;

        public override void Execute()
        {
            MessageData incomingData = (MessageData)evt.data;

            bool appStartLevel = (bool)incomingData[(byte)EParameterCodeToolkit.APP_START_EVENT];

            if (!appStartLevel)
            {
                Retain();

                UpdateListeners(true);

                GetWorkingDirectory();
            }

            ActiveWorldModel.ResetWorld();

            WorldData worldData = new WorldData
            {
                WorldId = new Guid().ToString(),
                Version = "1.0",
                WorldName = "Untitled Module",
                WorldDescription = "",
                WorldThumbnailPath = "",
                IsNameTagVisible = true,
                IsGazeVisible = false,
                MinCapacity = 1,
                MaxCapacity = 40,
            };

            worldData.ListItems = new List<CTItemData>();

            CTSpawnPointData cTSpawn;

            for (int i = 0; i < 40; i++)
            {
                cTSpawn = new CTSpawnPointData
                {
                    ItemIndex = i + 1,
                    ItemResourceId = "SpawnPoint",
                    ItemName = "SpawnPoint" + (i + 1),
                    IsVisibleByDefault = true,
                    Position = new CTVector3(0f, 0f, 0f),
                    Rotation = new CTQuaternion(0f, 0f, 0f, 1f),
                    Scale = new CTVector3(1f, 1f, 1f)
                };

                worldData.ListItems.Add(cTSpawn);
            }

            CTLineNotationData lineNotationData = new CTLineNotationData
            {
                ItemIndex = worldData.ListItems.Count + 1,
                ItemResourceId = "",
                ItemName = "",
                IsVisibleByDefault = false,
                Lines = new List<CTLine>(),
            };

            worldData.ListItems.Add(lineNotationData);
            
            ActiveWorldModel.ActiveWorldData = worldData;

            msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeWorld.WORLD_DATA, worldData);
            msgData.Parameters.Add((byte)EParameterCodeWorld.MODULE_THUMBNAIL_PATH, Pathy.Combine(Settings.WorldThumbnailPath, "Module.png"));

            dispatcher.Dispatch(EWorldHierarchyEvent.RESET_WORLD_HIERARCHY);
            dispatcher.Dispatch(EInspectorEvent.RESET_INSPECTOR);
            dispatcher.Dispatch(EWorldEvent.RESET_WORLD);

            HideMainMenuPanel();
        }

        private void UpdateListeners(bool value)
        {
            dispatcher.UpdateListener(value, EWorldEvent.RESET_WORLD_COMPLETED, OnResetWorldCompleted);
            dispatcher.UpdateListener(value, EInspectorEvent.RESET_INSPECTOR_COMPLETED, OnResetInspectorCompleted);
            dispatcher.UpdateListener(value, EWorldHierarchyEvent.RESET_WORLD_HIERARCHY_COMPLETED, OnResetWorldHierarchyCompleted);
        }

        private void OnResetWorldCompleted()
        {
            _worldReset = true;

            dispatcher.Dispatch(EWorldEvent.WORLD_CREATED, msgData);

            CheckAllReset();
        }

        private void OnResetInspectorCompleted()
        {
            _inspectorPanelReset = true;

            dispatcher.Dispatch(EInspectorEvent.WORLD_CREATED, msgData);

            CheckAllReset();
        }

        private void OnResetWorldHierarchyCompleted()
        {
            _worldHierarchyReset = true;

            dispatcher.Dispatch(EWorldHierarchyEvent.WORLD_CREATED, msgData);

            CheckAllReset();
        }

        private void CheckAllReset()
        {
            if (_worldReset && _inspectorPanelReset && _worldHierarchyReset)
            {
                Release();

                UpdateListeners(false);
            }
        }

        private void HideMainMenuPanel()
        {
            //MessageData hideMainMenuPanelData = new MessageData();
            //hideMainMenuPanelData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelNameConstants.MAIN_MENU);
            //
            //dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, hideMainMenuPanelData);

            MessageData hideModulePanelData = new MessageData();
            hideModulePanelData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelNameConstants.MODULES_PANEL);

            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, hideModulePanelData);
        }

        private void GetWorkingDirectory()
        {
            ExtensionFilter[] extensions = new[]
            {
                new ExtensionFilter()
            };

            MessageData message = new MessageData();

            string[] selectedFiles = StandaloneFileBrowser.OpenFolderPanel("Folder Selection", "", false);

            if (selectedFiles.Length > 0)
            {
                List<string> filesFolders = Directory.GetDirectories(selectedFiles[0]).ToList();

                bool hasPackageData = false;

                foreach (var str in filesFolders)
                {
                    if (Path.GetFileName(str) == "PackageData")
                    {
                        hasPackageData = true;

                        break;
                    }
                }

                if (!hasPackageData)
                {
                    string newPDPath = Path.Combine(selectedFiles[0], "PackageData");

                    Directory.CreateDirectory(newPDPath);
                }

                Settings.editorRootPath = selectedFiles[0];
            }
            else
            {
                //Settings.clientPath = "CreatorToolkit";
                Settings.editorRootPath = Settings.defaultEditorRootPath;

                if (!Directory.Exists(Settings.editorRootPath))
                {
                    Directory.CreateDirectory(Settings.editorRootPath);
                }
            }
        }
    }
}
