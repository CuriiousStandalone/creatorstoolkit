﻿namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public enum EParameterCodeWorld
    {
        WORLD_DATA,
        WORLD_ID,
        WORLD_NAME,
        MODULE_THUMBNAIL_FILENAME,
        MODULE_THUMBNAIL_PATH,

        ITEM_DATA,
        ITEM_INDEX,

        HOTSPOT_DATA,
        HOTSPOT_ID,

        HOTSPOT_TEMPLATE_DATA_LIST,

        TEMPLATE_PATH_LIST,
        TEMPLATE_DATA_LIST,

        DIRECTORY_INFO_DATA_LIST,
        IS_DIRECTORY,
        DIRECTORY_ID,

        MEDIA_CURRENT_TIME,

        IS_TELEPORT_ITEM,
    }
}
