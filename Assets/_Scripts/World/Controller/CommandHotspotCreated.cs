﻿using System.Linq;

using strange.extensions.command.impl;

using CuriiousIQ.Local.CreatorsToolkit.World.Static;
using CuriiousIQ.Local.CreatorsToolkit.World.WorldHierarchy;
using PulseIQ.Local.ClientCommon.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common;
using PulseIQ.Local.Common.Model.Panorama;
using PulseIQ.Local.Common.Model.PanoramaVideo;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandHotspotCreated : EventCommand
    {
        [Inject]
        public IModelActiveWorld WorldModel { get; set; }

        public override void Execute()
        {
            MessageData msgData = (MessageData)evt.data;

            bool isNewCreation = (bool)msgData[(byte)EParameterCodeStatic.NEW_CREATION];

            dispatcher.Dispatch(EStaticEvent.HOTSPOT_CREATED, evt.data);

            if (isNewCreation)
            {
                dispatcher.Dispatch(EWorldHierarchyEvent.HOTSPOT_CREATED, evt.data);

                CTHotspotData hotspotData = (CTHotspotData)msgData[(byte)EParameterCodeStatic.HOTSPOT_DATA];
                CTItemData itemData = (CTItemData)msgData[(byte)EParameterCodeStatic.ITEM_DATA];

                MessageData newMessageData = new MessageData();
                newMessageData.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ID, hotspotData.HotspotId);
                newMessageData.Parameters.Add((byte)EParameterCodeToolkit.ITEM_ID, itemData.ItemName);
                newMessageData.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, itemData.ItemIndex);
                newMessageData.Parameters.Add((byte)EParameterCodeToolkit.ITEM_TYPE, itemData.ItemType);

                dispatcher.Dispatch(EEventToolkitCrossContext.ENABLE_ADD_HOTSPOT_BUTTON);
                dispatcher.Dispatch(EEventToolkitCrossContext.ENABLE_DELETE_ITEM_BUTTON);
                dispatcher.Dispatch(EWorldHierarchyEvent.HOTSPOT_WORLD_ITEM_SELECTED, newMessageData);

                switch(itemData.ItemType)
                {
                    case ItemTypeCode.PANORAMA:
                        {
                            ((CTPanoramaData)WorldModel.ActiveWorldData.ListItems.FirstOrDefault(x => x.ItemIndex == itemData.ItemIndex)).ListHotspot.Add(hotspotData);
                            break;
                        }

                    case ItemTypeCode.PANORAMA_VIDEO:
                        {
                            ((CTPanoramaVideoData)WorldModel.ActiveWorldData.ListItems.FirstOrDefault(x => x.ItemIndex == itemData.ItemIndex)).ListHotspot.Add(hotspotData);
                            break;
                        }

                    case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                        {
                            ((CTStereoPanoramaVideoData)WorldModel.ActiveWorldData.ListItems.FirstOrDefault(x => x.ItemIndex == itemData.ItemIndex)).ListHotspot.Add(hotspotData);
                            break;
                        }
                }
            }
        }
    }
}
