﻿using strange.extensions.command.impl;

using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.ClientCommon.Components.Common;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.World.WorldHierarchy;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandItemCreated : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EWorldHierarchyEvent.ITEM_CREATED, evt.data);

            MessageData msgData = (MessageData)evt.data;

            bool isTeleportItem = msgData.Parameters.ContainsKey((byte)EParameterCodeWorld.IS_TELEPORT_ITEM) && (bool)msgData[(byte)EParameterCodeWorld.IS_TELEPORT_ITEM];

            if (!isTeleportItem)
            {
                CTItemData itemData = (CTItemData)msgData[(byte)EParameterCodeWorld.ITEM_DATA];

                MessageData resultMsgData = new MessageData();
                resultMsgData.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, itemData.ItemIndex);

                dispatcher.Dispatch(EWorldEvent.SELECT_ITEM, resultMsgData);
            }
        }
    }
}
