﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandPreviewModeEnabled : EventCommand
    {
        [Inject]
        public IModelActiveWorld WorldModel { get; set; }

        public override void Execute()
        {
            WorldModel.IsPreviewModeOn = true;

            if (WorldModel.MediaItemsInCurrentWorld.Count > 0)
            {
                dispatcher.Dispatch(EWorldEvent.LOAD_FIRST_OR_SELECTED_MEDIA);
            }
        }
    }
}
