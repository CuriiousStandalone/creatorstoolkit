﻿using System;
using System.IO;
using System.Collections.Generic;

using strange.extensions.command.impl;

using PulseIQ.Local.Common.Model.World;
using PulseIQ.Local.ClientCommon.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandSaveModuleAs : EventCommand
    {
        [Inject]
        public IModelActiveWorld WorldModel { get; set; }

        public override void Execute()
        {
            MessageData msgData = (MessageData)evt.data;

            string newWorldName = (string)msgData[(byte)EParameterCodeWorld.WORLD_NAME];

            List<string> localWorldIds = new List<string>(Directory.GetFiles(Settings.WorldPath));

            bool alreadyExist = false;

            foreach(string worldPath in localWorldIds)
            {
                WorldDataSerializer.ReadDataByFullPath(worldPath, out WorldDataXML worldXML);

                WorldData worldData = new WorldData(worldXML);

                if(worldData.WorldName == newWorldName)
                {
                    alreadyExist = true;

                    break;
                }
            }

            if(alreadyExist)
            {
                dispatcher.Dispatch(EWorldEvent.SAVE_MODULE_AS_NAME_EXIST_LOCALLY);
                return;
            }

            WorldModel.ActiveWorldData.WorldId = Guid.NewGuid().ToString();
            WorldModel.ActiveWorldData.WorldName = newWorldName;

            string parentPath = Directory.GetParent(Settings.WorldPath).Parent.FullName;

            WorldDataSerializer.WriteByData(WorldModel.ActiveWorldData, Path.Combine(parentPath, "Module.ctk"));

            //update name on inspector

            string localThumbnailPath = Path.GetFullPath(Path.Combine(Settings.WorldThumbnailPath, "Module.png"));
            string savedThumbnailPath = WorldModel.ThumbnailPath != "" ? Path.GetFullPath(WorldModel.ThumbnailPath) : WorldModel.ThumbnailPath;

            if (savedThumbnailPath == "")
            {
                if (File.Exists(localThumbnailPath))
                {
                    File.Delete(localThumbnailPath);
                }
            }
            else if (localThumbnailPath != savedThumbnailPath)
            {

                if (File.Exists(localThumbnailPath))
                {
                    File.Delete(localThumbnailPath);
                }

                if (File.Exists(savedThumbnailPath))
                {
                    if (!Directory.Exists(Settings.WorldThumbnailPath))
                    {
                        Directory.CreateDirectory(Settings.WorldThumbnailPath);
                    }

                    File.Copy(savedThumbnailPath, localThumbnailPath);
                }
            }

            dispatcher.Dispatch(EWorldEvent.SAVE_MODULE_AS_SUCCESS);

            dispatcher.Dispatch(EEventToolkitCrossContext.REFRESH_MODULE_LIST);
        }
    }
}
