﻿using System.Linq;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

using strange.extensions.command.impl;

using CuriiousIQ.Local.CreatorsToolkit.World.WorldHierarchy;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandItemDeleted : EventCommand
    {
        [Inject]
        public IModelActiveWorld ActiveWorldModel { get; set; }

        public override void Execute()
        {
            MessageData msg = (MessageData)evt.data;

            int itemIndex = (int)msg[(byte)EParameterCodeWorld.ITEM_INDEX];

            CTItemData itemData = new CTItemData();

            foreach (var item in ActiveWorldModel.ActiveWorldData.ListItems)
            {
                if (item.ItemIndex == itemIndex)
                {
                    itemData = item;
                    ActiveWorldModel.ActiveWorldData.ListItems.Remove(item);
                    break;
                }
            }

            if (IsMedia(itemData.ItemType))
            {
                foreach (var item in ActiveWorldModel.MediaItemsInCurrentWorld)
                {
                    if (item.ItemIndex == itemIndex)
                    {
                        ActiveWorldModel.MediaItemsInCurrentWorld.Remove(item);
                        break;
                    }
                }
            }

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.ITEM_DATA, itemData);
            dispatcher.Dispatch(EWorldHierarchyEvent.REMOVE_ITEM, msgData);
        }

        private bool IsMedia(ItemTypeCode itemType)
        {
            if (itemType == ItemTypeCode.PANORAMA || itemType == ItemTypeCode.PANORAMA_VIDEO || itemType == ItemTypeCode.PLANAR_VIDEO || itemType == ItemTypeCode.STEREO_PANORAMA_VIDEO)
            {
                return true;
            }

            return false;
        }
    }
}