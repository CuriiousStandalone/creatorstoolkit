﻿namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public enum EWorldEvent : byte
    {
        UPDATE_HOTSPOT_POSITION,
        UPDATE_HOTSPOT_ELEMENT_POSITION,

        RESOURCE_ITEM_DROPPED_INTO_WORLD,

        CREATE_NEW_WORLD,
        WORLD_CREATED,
        RESET_WORLD,
        RESET_WORLD_COMPLETED,
        TEMPLATE_PATHS_LOADED,

        ITEM_CREATED,
        DELETE_ITEM,
        ITEM_DELETED,

        ITEM_SELECTED,//Clicked on item in hierarchy
        SELECTED_ITEM_PROCESSED, // 
        HOTSPOT_SELECTED,//Clicked on item in hierarchy
        MODULE_SELECTED,//Clicked on item in hierarchy
        SELECT_ITEM,//Clicked on item in world etc.
        
        //sent when inspector changes an items name with:
        //EParameterCodeToolkit.ITEM_INDEX (string)
        //EParameterCodeToolkit.ITEM_NAME (string)
        ITEM_NAME_CHANGED,
        ITEM_DATA_CHANGED,

        MODULE_THUMBNAIL_UPDATED,

        MEDIA_VIDEO_CURRENT_TIME,
        MEDIA_VIDEO_LOADED,

        CREATE_NEW_HOTSPOT,
        HOTSPOT_CREATED,
        SELECT_HOTSPOT,
        HOTSPOT_DELETED,
        HOTSPOT_DATA_CHANGED,

        SHOW_SAVE_MODULE_AS_DIALOG,
        SAVE_MODULE_AS,
        SAVE_MODULE_AS_SUCCESS,
        SAVE_MODULE_AS_NAME_EXIST_LOCALLY,

        SHOW_PUBLISH_MODULE_DIALOG,
        PUBLISH_MODULE,
        PUBLISH_UPDATED_MODULE,
        PUBLISH_MODULE_SUCCESS,
        PUBLISH_MODULE_NAME_EXIST_LOCALLY,

        RESET_CAMERA,
        DEFAULT_STATUS_ICON_IDS,

        LOAD_FIRST_OR_SELECTED_MEDIA,
        LOAD_MEDIA_WITH_ITEM_INDEX,
        LOAD_LAST_ACTIVE_MEDIA,

        PANORAMA_IMAGE_TELEPORT_TIME_TRIGGERED,
        PANORAMA_VIDEO_ENDED,
        STEREO_PANORAMA_VIDEO_ENDED,
        TELEPORT_ITEM_CREATED,

        TOOLTIP_TEXT_RESPONSE,
        TOOLTIP_TEXT_REQUESTED,
    }
}