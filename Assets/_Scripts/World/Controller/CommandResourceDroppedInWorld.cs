using System.Linq;
using System.Collections.Generic;

using UnityEngine;

using strange.extensions.command.impl;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;
using PulseIQ.Local.Common.CustomType;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.Common.Model.Panorama;
using PulseIQ.Local.Common.Model.Multimedia;
using PulseIQ.Local.Common.Model.PanoramaVideo;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandResourceDroppedInWorld : EventCommand
    {
        [Inject]
        public IModelActiveWorld ActiveWorldModel { get; set; }

        private string resourceId;

        private ItemTypeCode itemType;

        private bool isMultiplayer = false;

        public override void Execute()
        {
            MessageData msgData = (MessageData)evt.data;

            resourceId = (string)msgData[(byte)CommonParameterCode.RESOURCEID];

            isMultiplayer = ActiveWorldModel.ActiveWorldData.WorldInteractiveMode == PulseIQ.Local.Common.Model.World.EInteractiveModeCode.PLAYER_LEAD ? false : true;

            itemType = (ItemTypeCode)msgData[(byte)CommonParameterCode.ITEM_TYPE];

            if (msgData.Parameters.ContainsKey((byte)CommonParameterCode.MOUSE_COORDS))
            {
                Vector3 mouseCoords = (Vector3)msgData[(byte)CommonParameterCode.MOUSE_COORDS];

                if (IsInWorldViewPort(mouseCoords))
                {
                    CreateMediaItemData();
                }
            }
            else
            {
                CreateMediaItemData();
            }
        }

        private bool IsInWorldViewPort(Vector3 mouseCoord)
        {
            Vector3 newVect = new Vector3(mouseCoord.x, mouseCoord.y, mouseCoord.z - 20);

            int layerMask = LayerMask.GetMask("UI");

            if (Physics.Raycast(newVect, Vector3.forward, Mathf.Infinity, layerMask))
            {
                return false;
            }

            return true;
        }

        private void CreateMediaItemData()
        {
            bool isNonSupportedItem = false;

            CTItemData itemData = null;
            
            switch(itemType)
            {
                case ItemTypeCode.PANORAMA:
                    {
                        itemData = new CTPanoramaData
                        {
                            ItemIndex = ActiveWorldModel.ActiveWorldData.ListItems.Max(x => x.ItemIndex) + 1,
                            ItemResourceId = "PanoramaImagePlayer",
                            PanoramaId = resourceId,
                            ItemName = resourceId,
                            Position = new CTVector3(0f, 0f, 0f),
                            Rotation = new CTQuaternion(0f, 0f, 0f, 1f),
                            Scale = new CTVector3(0f, 0f, 0f),
                            IsVisibleByDefault = true,
                            IsMultiplayer = isMultiplayer,
                            ListHotspot = new List<CTHotspotData>(),
                            EndBehavior = EMultimediaEndBehaviorCode.CLOSE,
                            TargetMultimediaItemIndex = -1,
                            PanoramaTeleportTime = -1,
                        };
                        break;
                    }
                case ItemTypeCode.PANORAMA_VIDEO:
                    {
                        itemData = new CTPanoramaVideoData
                        {
                            ItemIndex = ActiveWorldModel.ActiveWorldData.ListItems.Max(x => x.ItemIndex) + 1,
                            ItemResourceId = "PanoramaVideoPlayer",
                            PanoramaVideoId = resourceId,
                            ItemName = resourceId,
                            Position = new CTVector3(0f, 0f, 0f),
                            Rotation = new CTQuaternion(0f, 0f, 0f, 1f),
                            Scale = new CTVector3(0f, 0f, 0f),
                            IsVisibleByDefault = true,
                            IsMultiplayer = isMultiplayer,
                            ListHotspot = new List<CTHotspotData>(),
                            EndBehavior = EMultimediaEndBehaviorCode.CLOSE,
                            TargetMultimediaItemIndex = -1,
                        };
                        break;
                    }
                case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                    {
                        itemData = new CTStereoPanoramaVideoData
                        {
                            ItemIndex = ActiveWorldModel.ActiveWorldData.ListItems.Max(x => x.ItemIndex) + 1,
                            ItemResourceId = "StereoPanoramaVideoPlayer",
                            StereoPanoramaVideoId = resourceId,
                            ItemName = resourceId,
                            Position = new CTVector3(0f, 0f, 0f),
                            Rotation = new CTQuaternion(0f, 0f, 0f, 1f),
                            Scale = new CTVector3(0f, 0f, 0f),
                            IsVisibleByDefault = true,
                            IsMultiplayer = isMultiplayer,
                            ListHotspot = new List<CTHotspotData>(),
                            EndBehavior = EMultimediaEndBehaviorCode.CLOSE,
                            TargetMultimediaItemIndex = -1,
                            StereoPanoramaVideoType = EStereoPanoramaVideoTypeCode.TOP_BOTTOM,
                        };
                        break;
                    }
                default:
                    {
                        isNonSupportedItem = true;
                        break;
                    }
            }

            if(isNonSupportedItem || itemData == null)
            {
                return;
            }

            ActiveWorldModel.ActiveWorldData.ListItems.Add(itemData);

            ActiveWorldModel.MediaItemsInCurrentWorld.Add(itemData);

            MessageData resultData = new MessageData();
            resultData.Parameters.Add((byte)EParameterCodeWorld.ITEM_DATA, itemData);

            dispatcher.Dispatch(EWorldEvent.RESOURCE_ITEM_DROPPED_INTO_WORLD, resultData);
        }
    }
}