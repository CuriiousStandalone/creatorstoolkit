﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandTooltipResponse : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EWorldEvent.TOOLTIP_TEXT_RESPONSE, evt.data);
        }
    }
}
