﻿using System.Linq;
using System.Collections.Generic;

using UnityEngine;

using strange.extensions.command.impl;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;
using PulseIQ.Local.Common.CustomType;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.Common.Model.Panorama;
using PulseIQ.Local.Common.Model.Multimedia;
using PulseIQ.Local.Common.Model.PanoramaVideo;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CreateNewTeleportItem : EventCommand
    {
        [Inject]
        public IModelActiveWorld ActiveWorldModel { get; set; }

        private string resourceId;

        private ItemTypeCode itemType;

        MessageData msgData;

        public override void Execute()
        {
            msgData = (MessageData)evt.data;

            resourceId = (string)msgData[(byte)EParameterCodeToolkit.MEDIA_TELEPORT_ITEM];

            itemType = (ItemTypeCode)msgData[(byte)EParameterCodeToolkit.ITEM_TYPE];

            CreateMediaItemData();
        }

        private void CreateMediaItemData()
        {
            bool isNonSupportedItem = false;

            CTItemData itemData = null;

            switch (itemType)
            {
                case ItemTypeCode.PANORAMA:
                    {
                        itemData = new CTPanoramaData
                        {
                            ItemIndex = ActiveWorldModel.ActiveWorldData.ListItems.Max(x => x.ItemIndex) + 1,
                            ItemResourceId = "PanoramaImagePlayer",
                            PanoramaId = resourceId,
                            ItemName = resourceId,
                            Position = new CTVector3(0f, 0f, 0f),
                            Rotation = new CTQuaternion(0f, 0f, 0f, 1f),
                            Scale = new CTVector3(0f, 0f, 0f),
                            IsVisibleByDefault = true,
                            IsMultiplayer = true,
                            ListHotspot = new List<CTHotspotData>(),
                            EndBehavior = EMultimediaEndBehaviorCode.CLOSE,
                            TargetMultimediaItemIndex = -1,
                            PanoramaTeleportTime = -1,
                        };
                        break;
                    }
                case ItemTypeCode.PANORAMA_VIDEO:
                    {
                        itemData = new CTPanoramaVideoData
                        {
                            ItemIndex = ActiveWorldModel.ActiveWorldData.ListItems.Max(x => x.ItemIndex) + 1,
                            ItemResourceId = "PanoramaVideoPlayer",
                            PanoramaVideoId = resourceId,
                            ItemName = resourceId,
                            Position = new CTVector3(0f, 0f, 0f),
                            Rotation = new CTQuaternion(0f, 0f, 0f, 1f),
                            Scale = new CTVector3(0f, 0f, 0f),
                            IsVisibleByDefault = true,
                            IsMultiplayer = true,
                            ListHotspot = new List<CTHotspotData>(),
                            EndBehavior = EMultimediaEndBehaviorCode.CLOSE,
                            TargetMultimediaItemIndex = -1,
                        };
                        break;
                    }
                case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                    {
                        itemData = new CTStereoPanoramaVideoData
                        {
                            ItemIndex = ActiveWorldModel.ActiveWorldData.ListItems.Max(x => x.ItemIndex) + 1,
                            ItemResourceId = "StereoPanoramaVideoPlayer",
                            StereoPanoramaVideoId = resourceId,
                            ItemName = resourceId,
                            Position = new CTVector3(0f, 0f, 0f),
                            Rotation = new CTQuaternion(0f, 0f, 0f, 1f),
                            Scale = new CTVector3(0f, 0f, 0f),
                            IsVisibleByDefault = true,
                            IsMultiplayer = true,
                            ListHotspot = new List<CTHotspotData>(),
                            EndBehavior = EMultimediaEndBehaviorCode.CLOSE,
                            TargetMultimediaItemIndex = -1,
                            StereoPanoramaVideoType = EStereoPanoramaVideoTypeCode.TOP_BOTTOM,
                        };
                        break;
                    }
                default:
                    {
                        isNonSupportedItem = true;
                        break;
                    }
            }

            if (isNonSupportedItem || itemData == null)
            {
                return;
            }

            ActiveWorldModel.ActiveWorldData.ListItems.Add(itemData);

            ActiveWorldModel.MediaItemsInCurrentWorld.Add(itemData);

            msgData.Parameters.Add((byte)EParameterCodeToolkit.ITEM_DATA, itemData);

            dispatcher.Dispatch(EWorldEvent.TELEPORT_ITEM_CREATED, msgData);
        }
    }
}