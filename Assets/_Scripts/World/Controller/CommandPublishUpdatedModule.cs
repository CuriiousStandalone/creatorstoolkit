﻿using System;
using System.IO;
using System.Collections.Generic;

using strange.extensions.command.impl;

using PulseIQ.Local.Service;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.World;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandPublishUpdatedModule : EventCommand
    {
        [Inject]
        public IModelActiveWorld WorldModel { get; set; }

        private ContentAccess _content;

        public override void Execute()
        {
            Retain();

            MessageData msgData = (MessageData)evt.data;

            bool isDirectory = (bool)msgData[(byte)EParameterCodeWorld.IS_DIRECTORY];

            string directoryId = (string)msgData[(byte)EParameterCodeWorld.DIRECTORY_ID];

            string newWorldName = (string)msgData[(byte)EParameterCodeWorld.WORLD_NAME];

            string newWorldId = "";

            _content = new ContentAccess(GlobalSettings.CMSURL);

            List<WorldInfoData> remoteWorlds = _content.World.GetAll();

            foreach (var remoteWorld in remoteWorlds)
            {
                if (remoteWorld.WorldName == newWorldName)
                {
                    dispatcher.Dispatch(EWorldEvent.PUBLISH_MODULE_NAME_EXIST_LOCALLY);

                    Release();

                    return;
                }
            }

            foreach(var remoteWorld in remoteWorlds)
            {
                if(remoteWorld.WorldId == WorldModel.ActiveWorldData.WorldId)
                {
                    newWorldId = Guid.NewGuid().ToString();
                    break;
                }
            }

            if(string.IsNullOrEmpty(newWorldId))
            {
                newWorldId = WorldModel.ActiveWorldData.WorldId;
            }

            WorldModel.ActiveWorldData.WorldId = newWorldId;
            WorldModel.ActiveWorldData.WorldName = newWorldName;

            string parentPath = Directory.GetParent(Settings.WorldPath).Parent.FullName;
            string worldPath = Path.Combine(parentPath, "Module.ctk");

            if(File.Exists(worldPath))
            {
                File.Delete(worldPath);
            }

            WorldDataSerializer.WriteByData(WorldModel.ActiveWorldData, worldPath);

            WorldDataSerializer.ReadDataByFullPath(worldPath, out WorldDataXML worldDataXml);

            UploadWorld(worldDataXml, isDirectory, directoryId);
        }

        private async void UploadWorld(WorldDataXML worldDataXml, bool isDirectory, string directoryId)
        {
            await _content.World.UploadWorldAsync(worldDataXml, isDirectory, directoryId);

            dispatcher.Dispatch(EWorldEvent.PUBLISH_MODULE_SUCCESS);

            Release();
        }
    }
}
