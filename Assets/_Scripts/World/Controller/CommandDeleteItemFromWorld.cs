﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandDeleteItemFromWorld : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EWorldEvent.DELETE_ITEM);
        }
    }
}
