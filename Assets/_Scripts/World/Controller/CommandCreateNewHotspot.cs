﻿using System;
using System.Linq;
using System.Collections.Generic;

using strange.extensions.command.impl;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.Hotspot.HotspotTemplate;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.Common.Model.Hotspot.HotspotElement;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandCreateNewHotspot : EventCommand
    {
        public override void Execute()
        {
            MessageData msgData = (MessageData)evt.data;

            HotspotTemplateData selectedHotspotTemplate = (HotspotTemplateData)msgData[(byte)EParameterCodeToolkit.HOTSPOT_ELEMENT_DATA];

            List<HotspotTemplateData> hotspotTemplateDataList = HotspotTemplateManager.Instance.HotspotTemplateTypes.Templates;

            CTHotspotData hotspotData = new CTHotspotData
            {
                HotspotId = Guid.NewGuid().ToString(),
                HotspotName = selectedHotspotTemplate.TemplateName,
                ListHotspotStatus = new List<CTHotspotStatusData>(),
                HotspotBoxes = new List<CTHotspotBoxData>(1),
                HotspotTriggerEventCode = selectedHotspotTemplate.HotspotData.HotspotTriggerEventCode,
            };

            foreach (var hotspotStatusData in selectedHotspotTemplate.HotspotData.ListHotspotStatus)
            {
                CTHotspotStatusData hotspotStatus = new CTHotspotStatusData
                {
                    StatusId = Guid.NewGuid().ToString(),
                    StatusName = hotspotStatusData.StatusName,
                    StatusTypeCode = hotspotStatusData.StatusTypeCode,
                    StatusIconId = hotspotStatusData.StatusIconId,
                };

                hotspotData.ListHotspotStatus.Add(hotspotStatus);
            }

            CTHotspotData hsData = hotspotTemplateDataList.FirstOrDefault(x => x.TemplateIndex == selectedHotspotTemplate.TemplateIndex).HotspotData;

            if (null != hsData.HotspotBoxes && hsData.HotspotBoxes.Count > 0)
            {
                CTHotspotBoxData templateBox = hsData.HotspotBoxes[0];

                List<CTHotspotElementBaseData> elementList = new List<CTHotspotElementBaseData>();

                foreach (CTHotspotElementBaseData eleData in templateBox.Elements)
                {
                    switch (eleData.TypeCode)
                    {
                        case EHotspotElementTypeCode.AUDIO:
                            {
                                CTHotspotElementAudioData newele = new CTHotspotElementAudioData();
                                newele.Height = ((CTHotspotElementAudioData)eleData).Height;
                                newele.MultimediaId = ((CTHotspotElementAudioData)eleData).MultimediaId;
                                newele.Width = ((CTHotspotElementAudioData)eleData).Width;
                                newele.X = ((CTHotspotElementAudioData)eleData).X;
                                newele.Y = ((CTHotspotElementAudioData)eleData).Y;

                                elementList.Add(newele);
                                break;
                            }

                        case EHotspotElementTypeCode.IMAGE:
                            {
                                CTHotspotElementImageData newele = new CTHotspotElementImageData();
                                newele.Height = ((CTHotspotElementImageData)eleData).Height;
                                newele.MultimediaId = ((CTHotspotElementImageData)eleData).MultimediaId;
                                newele.Width = ((CTHotspotElementImageData)eleData).Width;
                                newele.X = ((CTHotspotElementImageData)eleData).X;
                                newele.Y = ((CTHotspotElementImageData)eleData).Y;

                                elementList.Add(newele);

                                break;
                            }

                        case EHotspotElementTypeCode.TEXT:
                            {
                                CTHotspotElementTextData newele = new CTHotspotElementTextData();
                                newele.FontName = ((CTHotspotElementTextData)eleData).FontName;
                                newele.FontSize = ((CTHotspotElementTextData)eleData).FontSize;
                                newele.Text = ((CTHotspotElementTextData)eleData).Text;
                                newele.Height = ((CTHotspotElementTextData)eleData).Height;
                                newele.Width = ((CTHotspotElementTextData)eleData).Width;
                                newele.X = ((CTHotspotElementTextData)eleData).X;
                                newele.Y = ((CTHotspotElementTextData)eleData).Y;

                                elementList.Add(newele);
                                break;
                            }

                        case EHotspotElementTypeCode.VIDEO:
                            {
                                CTHotspotElementVideoData newele = new CTHotspotElementVideoData();
                                newele.Height = ((CTHotspotElementVideoData)eleData).Height;
                                newele.MultimediaId = ((CTHotspotElementVideoData)eleData).MultimediaId;
                                newele.Width = ((CTHotspotElementVideoData)eleData).Width;
                                newele.X = ((CTHotspotElementVideoData)eleData).X;
                                newele.Y = ((CTHotspotElementVideoData)eleData).Y;

                                elementList.Add(newele);
                                break;
                            }
                    }

                }

                CTHotspotBoxData newBox = new CTHotspotBoxData
                {
                    Elements = elementList,
                    Height = templateBox.Height,
                    Width = templateBox.Width,
                    X = templateBox.X,
                    Y = templateBox.Y,
                };

                hotspotData.HotspotBoxes.Add(newBox);
            }

            MessageData responseData = new MessageData();

            responseData.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_DATA, hotspotData);

            dispatcher.Dispatch(EWorldEvent.CREATE_NEW_HOTSPOT, responseData);
        }
    }
}
