﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandResetCameraRequested : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EWorldEvent.RESET_CAMERA);
        }
    }
}
