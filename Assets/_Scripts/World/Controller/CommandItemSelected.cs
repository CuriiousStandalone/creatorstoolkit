﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.Panorama;
using PulseIQ.Local.Common.Model.PanoramaVideo;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;
using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandItemSelected : EventCommand
    {
        [Inject]
        public IModelActiveWorld model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;
            CTItemData itemData = (CTItemData)data.Parameters[(byte)EParameterCodeToolkit.ITEM_DATA];

            switch (itemData.ItemType)
            {
                case PulseIQ.Local.Common.ItemTypeCode.PANORAMA:
                    {
                        CTPanoramaData panoramaData = (CTPanoramaData)itemData;

                        foreach (CTItemData items in model.ActiveWorldData.ListItems)
                        {
                            if (panoramaData.TargetMultimediaItemIndex == items.ItemIndex)
                            {
                                data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_NAME, items.ItemName);
                            }
                        }

                        break;
                    }

                case PulseIQ.Local.Common.ItemTypeCode.PANORAMA_VIDEO:
                    {
                        CTPanoramaVideoData panoramaData = (CTPanoramaVideoData)itemData;

                        foreach (CTItemData items in model.ActiveWorldData.ListItems)
                        {
                            if (panoramaData.TargetMultimediaItemIndex == items.ItemIndex)
                            {
                                data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_NAME, items.ItemName);
                            }
                        }

                        break;
                    }

                case PulseIQ.Local.Common.ItemTypeCode.STEREO_PANORAMA_VIDEO:
                    {
                        CTStereoPanoramaVideoData panoramaData = (CTStereoPanoramaVideoData)itemData;

                        foreach (CTItemData items in model.ActiveWorldData.ListItems)
                        {
                            if (panoramaData.TargetMultimediaItemIndex == items.ItemIndex)
                            {
                                data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_NAME, items.ItemName);
                            }
                        }

                        break;
                    }

                default:
                    break;
            }

            dispatcher.Dispatch(EWorldEvent.SELECTED_ITEM_PROCESSED, data);
        }
    }
}