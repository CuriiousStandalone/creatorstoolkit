﻿using System.Collections.Generic;

using strange.extensions.command.impl;

using PulseIQ.Local.Common;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.Common.Model.Panorama;
using PulseIQ.Local.Common.Model.PanoramaVideo;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;
using PulseIQ.Local.ClientCommon.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.World.WorldHierarchy;
using CuriiousIQ.Local.CreatorsToolkit.World.Static;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandHotspotDeleted : EventCommand
    {
        [Inject]
        public IModelActiveWorld ActiveWorldModel { get; set; }

        public override void Execute()
        {
            MessageData msg = (MessageData)evt.data;

            int itemIndex = (int)msg[(byte)EParameterCodeToolkit.ITEM_INDEX];
            string hotspotId = (string)msg[(byte)EParameterCodeToolkit.HOTSPOT_ID];

            string hotspotName = "";

            CTItemData itemData = new CTItemData();

            for(int i = 0; i < ActiveWorldModel.ActiveWorldData.ListItems.Count; i++)
            {
                if (ActiveWorldModel.ActiveWorldData.ListItems[i].ItemIndex == itemIndex)
                {
                    switch(ActiveWorldModel.ActiveWorldData.ListItems[i].ItemType)
                    {
                        case ItemTypeCode.PANORAMA:
                            {
                                List<CTHotspotData> hotspots = ((CTPanoramaData)ActiveWorldModel.ActiveWorldData.ListItems[i]).ListHotspot;
                                foreach(var hotspot in hotspots)
                                {
                                    if(hotspot.HotspotId == hotspotId)
                                    {
                                        hotspots.Remove(hotspot);
                                        hotspotName = hotspot.HotspotName;
                                        break;
                                    }
                                }

                                ((CTPanoramaData)ActiveWorldModel.ActiveWorldData.ListItems[i]).ListHotspot = hotspots;

                                break;
                            }

                        case ItemTypeCode.PANORAMA_VIDEO:
                            {
                                List<CTHotspotData> hotspots = ((CTPanoramaVideoData)ActiveWorldModel.ActiveWorldData.ListItems[i]).ListHotspot;
                                foreach (var hotspot in hotspots)
                                {
                                    if (hotspot.HotspotId == hotspotId)
                                    {
                                        hotspots.Remove(hotspot);
                                        hotspotName = hotspot.HotspotName;

                                        break;
                                    }
                                }

                                ((CTPanoramaVideoData)ActiveWorldModel.ActiveWorldData.ListItems[i]).ListHotspot = hotspots;

                                break;
                            }

                        case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                            {
                                List<CTHotspotData> hotspots = ((CTStereoPanoramaVideoData)ActiveWorldModel.ActiveWorldData.ListItems[i]).ListHotspot;
                                foreach (var hotspot in hotspots)
                                {
                                    if (hotspot.HotspotId == hotspotId)
                                    {
                                        hotspots.Remove(hotspot);
                                        hotspotName = hotspot.HotspotName;

                                        break;
                                    }
                                }

                                ((CTStereoPanoramaVideoData)ActiveWorldModel.ActiveWorldData.ListItems[i]).ListHotspot = hotspots;

                                break;
                            }
                    }
                    break;
                }
            }

            foreach (var item in ActiveWorldModel.MediaItemsInCurrentWorld)
            {
                if (item.ItemIndex == itemIndex)
                {
                    switch (item.ItemType)
                    {
                        case ItemTypeCode.PANORAMA:
                            {
                                List<CTHotspotData> hotspots = ((CTPanoramaData)item).ListHotspot;
                                foreach (var hotspot in hotspots)
                                {
                                    if (hotspot.HotspotId == hotspotId)
                                    {
                                        hotspots.Remove(hotspot);
                                        break;
                                    }
                                }

                                ((CTPanoramaData)item).ListHotspot = hotspots;

                                break;
                            }

                        case ItemTypeCode.PANORAMA_VIDEO:
                            {
                                List<CTHotspotData> hotspots = ((CTPanoramaVideoData)item).ListHotspot;
                                foreach (var hotspot in hotspots)
                                {
                                    if (hotspot.HotspotId == hotspotId)
                                    {
                                        hotspots.Remove(hotspot);

                                        break;
                                    }
                                }

                                ((CTPanoramaVideoData)item).ListHotspot = hotspots;

                                break;
                            }

                        case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                            {
                                List<CTHotspotData> hotspots = ((CTStereoPanoramaVideoData)item).ListHotspot;
                                foreach (var hotspot in hotspots)
                                {
                                    if (hotspot.HotspotId == hotspotId)
                                    {
                                        hotspots.Remove(hotspot);

                                        break;
                                    }
                                }

                                ((CTStereoPanoramaVideoData)item).ListHotspot = hotspots;

                                break;
                            }
                    }
                    break;
                }
            }
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ID, hotspotId);
            msgData.Parameters.Add((byte)EParameterCodeStatic.HOTSPOT_ID, hotspotId);
            msgData.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_NAME, hotspotName);

            dispatcher.Dispatch(EWorldHierarchyEvent.REMOVE_HOTSPOT, msgData);
        }
    }
}
