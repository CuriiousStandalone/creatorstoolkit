﻿using strange.extensions.command.impl;

using PulseIQ.Local.ClientCommon.Components.Common;
using System.IO;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandModuleThumbnailCleared : EventCommand
    {
        [Inject]
        public IModelActiveWorld WorldModel { get; set; }

        public override void Execute()
        {
             if (File.Exists(Path.Combine(Settings.ImagePath, "Module.png")))
             {
                 File.Delete(Path.Combine(Settings.ImagePath, "Module.png"));
             }
        }
    }
}
