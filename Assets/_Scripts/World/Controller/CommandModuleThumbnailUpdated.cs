﻿using strange.extensions.command.impl;

using PulseIQ.Local.ClientCommon.Components.Common;
using System.IO;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandModuleThumbnailUpdated : EventCommand
    {
        [Inject]
        public IModelActiveWorld WorldModel { get; set; }

        public override void Execute()
        {
            MessageData msgData = (MessageData)evt.data;

            string thumbnailPath = (string)msgData[(byte)EParameterCodeWorld.MODULE_THUMBNAIL_FILENAME];

            if(string.IsNullOrEmpty(thumbnailPath))
            {
                thumbnailPath = "";
            }

            WorldModel.ActiveWorldData.WorldThumbnailPath = thumbnailPath;
        }
    }
}
