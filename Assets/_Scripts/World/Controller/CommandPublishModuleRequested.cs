﻿using System.Collections.Generic;

using strange.extensions.command.impl;

using PulseIQ.Local.Service;
using PulseIQ.Local.Common;
using PulseIQ.Local.Common.Model.Directory;
using PulseIQ.Local.ClientCommon.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandPublishModuleRequested : EventCommand
    {
        private ContentAccess _content;

        public override void Execute()
        {
            _content = new ContentAccess(GlobalSettings.CMSURL);

            List<DirectoryInfoData> directories = _content.Directory.GetAll();

            MessageData msgData = new MessageData();

            msgData.Parameters.Add((byte)EParameterCodeWorld.DIRECTORY_INFO_DATA_LIST, directories);

            dispatcher.Dispatch(EWorldEvent.SHOW_PUBLISH_MODULE_DIALOG, msgData);

            HideMainMenuPanel();
        }

        private void HideMainMenuPanel()
        {
            MessageData hideMainMenuPanelData = new MessageData();
            hideMainMenuPanelData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelNameConstants.MAIN_MENU);

            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, hideMainMenuPanelData);

            MessageData hideModulePanelData = new MessageData();
            hideModulePanelData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelNameConstants.MODULES_PANEL);

            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, hideModulePanelData);
        }
    }
}
