﻿using System.Linq;
using System.Collections.Generic;

using strange.extensions.command.impl;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.Panorama;
using PulseIQ.Local.Common.Model.PanoramaVideo;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;
using PulseIQ.Local.Common.Model.World;
using PulseIQ.Local.Common.Model.Skybox;
using PulseIQ.Local.Common.Model.PostProcessing;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.World.Inspector;
using CuriiousIQ.Local.CreatorsToolkit.World.WorldHierarchy;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandTemplateChanged : EventCommand
    {
        [Inject]
        public IModelActiveWorld Model { get; set; }

        [Inject]
        public IModuleTemplateData ModuleTemplateModel { get; set; }

        private bool _worldReset = false;
        private bool _inspectorPanelReset = false;
        private bool _worldHierarchyReset = false;

        private MessageData _resultMsgData;

        public override void Execute()
        {
            Retain();

            UpdateListeners(true);

            MessageData data = (MessageData)evt.data;
            string templateID = (string)data[(byte)EParameterCodeToolkit.TEMPLATE_ID];

            Dictionary<int, int> idPairs = new Dictionary<int, int>();

            foreach(CTItemData mediaData in Model.MediaItemsInCurrentWorld)
            {
                idPairs.Add(mediaData.ItemIndex, -1);
            }

            Model.ActiveWorldData.ListItems.Clear();
            Model.ActiveWorldData.ListAssociation.Clear();
            Model.ActiveWorldData.ListPostProcessing.Clear();
            Model.ActiveWorldData.ListSkybox.Clear();

            foreach (WorldData worldData in ModuleTemplateModel.TemplateList)
            {
                if(worldData.WorldId == templateID)
                {
                    Model.ActiveWorldData.ListItems = new List<CTItemData>(worldData.ListItems);
                    Model.ActiveWorldData.ListPostProcessing = new List<PostProcessingData>(worldData.ListPostProcessing);
                    Model.ActiveWorldData.ListSkybox = new List<SkyboxData>(worldData.ListSkybox);

                    foreach(CTItemData mediaData in Model.MediaItemsInCurrentWorld)
                    {
                        int currentMediaIndex = mediaData.ItemIndex;
                        int newMediaIndex = Model.ActiveWorldData.ListItems.Max(x => x.ItemIndex) + 1;

                        ChangeMediaIndexReferences(currentMediaIndex, newMediaIndex);

                        mediaData.ItemIndex = newMediaIndex;

                        Model.ActiveWorldData.ListItems.Add(mediaData);
                    }

                    break;
                }
            }

            Model.ActiveWorldData.WorldTemplateId = templateID;

            _resultMsgData = new MessageData();
            _resultMsgData.Parameters.Add((byte)EParameterCodeWorld.WORLD_DATA, Model.ActiveWorldData);
            _resultMsgData.Parameters.Add((byte)EParameterCodeWorld.MODULE_THUMBNAIL_PATH, Model.ThumbnailPath);

            dispatcher.Dispatch(EWorldHierarchyEvent.RESET_WORLD_HIERARCHY);
            dispatcher.Dispatch(EInspectorEvent.RESET_INSPECTOR);
            dispatcher.Dispatch(EWorldEvent.RESET_WORLD);
        }

        private void UpdateListeners(bool value)
        {
            dispatcher.UpdateListener(value, EWorldEvent.RESET_WORLD_COMPLETED, OnResetWorldCompleted);
            dispatcher.UpdateListener(value, EInspectorEvent.RESET_INSPECTOR_COMPLETED, OnResetInspectorCompleted);
            dispatcher.UpdateListener(value, EWorldHierarchyEvent.RESET_WORLD_HIERARCHY_COMPLETED, OnResetWorldHierarchyCompleted);
        }

        private void OnResetWorldCompleted()
        {
            _worldReset = true;

            dispatcher.Dispatch(EWorldEvent.WORLD_CREATED, _resultMsgData);

            CheckAllReset();
        }

        private void OnResetInspectorCompleted()
        {
            _inspectorPanelReset = true;

            dispatcher.Dispatch(EInspectorEvent.WORLD_CREATED, _resultMsgData);

            CheckAllReset();
        }

        private void OnResetWorldHierarchyCompleted()
        {
            _worldHierarchyReset = true;

            dispatcher.Dispatch(EWorldHierarchyEvent.WORLD_CREATED, _resultMsgData);

            CheckAllReset();
        }

        private void CheckAllReset()
        {
            if (_worldReset && _inspectorPanelReset && _worldHierarchyReset)
            {
                Release();

                UpdateListeners(false);
            }
        }

        private void ChangeMediaIndexReferences(int oldIndex, int newIndex)
        {
            foreach(CTItemData data in Model.MediaItemsInCurrentWorld)
            {
                switch(data.ItemType)
                {
                    case PulseIQ.Local.Common.ItemTypeCode.PANORAMA:
                        {
                            CTPanoramaData panoramaData = (CTPanoramaData)data;

                            if(panoramaData.TargetMultimediaItemIndex == oldIndex)
                            {
                                panoramaData.TargetMultimediaItemIndex = newIndex;
                            }

                            foreach(CTHotspotData hotspotData in panoramaData.ListHotspot)
                            {
                                if(hotspotData.TeleportMultimediaItemIndex == oldIndex)
                                {
                                    hotspotData.TeleportMultimediaItemIndex = newIndex;
                                }
                            }

                            break;
                        }

                    case PulseIQ.Local.Common.ItemTypeCode.PANORAMA_VIDEO:
                        {
                            CTPanoramaVideoData panoramaData = (CTPanoramaVideoData)data;

                            if (panoramaData.TargetMultimediaItemIndex == oldIndex)
                            {
                                panoramaData.TargetMultimediaItemIndex = newIndex;
                            }

                            foreach (CTHotspotData hotspotData in panoramaData.ListHotspot)
                            {
                                if (hotspotData.TeleportMultimediaItemIndex == oldIndex)
                                {
                                    hotspotData.TeleportMultimediaItemIndex = newIndex;
                                }
                            }
                            break;
                        }

                    case PulseIQ.Local.Common.ItemTypeCode.STEREO_PANORAMA_VIDEO:
                        {
                            CTStereoPanoramaVideoData panoramaData = (CTStereoPanoramaVideoData)data;

                            if (panoramaData.TargetMultimediaItemIndex == oldIndex)
                            {
                                panoramaData.TargetMultimediaItemIndex = newIndex;
                            }

                            foreach (CTHotspotData hotspotData in panoramaData.ListHotspot)
                            {
                                if (hotspotData.TeleportMultimediaItemIndex == oldIndex)
                                {
                                    hotspotData.TeleportMultimediaItemIndex = newIndex;
                                }
                            }
                            break;
                        }
                }
            }
        }
    }
}