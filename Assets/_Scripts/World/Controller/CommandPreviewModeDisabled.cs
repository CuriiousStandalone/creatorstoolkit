﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandPreviewModeDisabled : EventCommand
    {
        [Inject]
        public IModelActiveWorld WorldModel { get; set; }

        public override void Execute()
        {
            WorldModel.IsPreviewModeOn = false;

            dispatcher.Dispatch(EWorldEvent.LOAD_LAST_ACTIVE_MEDIA);
        }
    }
}
