﻿using System.IO;

using strange.extensions.command.impl;

using PulseIQ.Local.Common;
using PulseIQ.Local.Common.Model.World;
using PulseIQ.Local.ClientCommon.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.World.Inspector;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.World.WorldHierarchy;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class CommandOpenWorld : EventCommand
    {
        [Inject]
        public IModelActiveWorld ActiveWorldModel { get; set; }

        private bool _worldReset = false;
        private bool _inspectorPanelReset = false;
        private bool _worldHierarchyReset = false;

        private MessageData messageData;

        public override void Execute()
        {
            Retain();

            UpdateListeners(true);

            MessageData msgData = (MessageData)evt.data;

            string worldId = (string)msgData[(byte)EParameterCodeToolkit.WORLD_ID];

            ActiveWorldModel.ResetWorld();

            WorldDataSerializer.ReadDataByWorldId(worldId, out WorldDataXML worldDataXml);

            WorldData worldData = new WorldData(worldDataXml);

            ActiveWorldModel.ActiveWorldData = worldData;
            ActiveWorldModel.ThumbnailPath = Path.Combine(Settings.WorldThumbnailPath, "Module.png");

            foreach (var itemData in worldData.ListItems)
            {
                if(IsMedia(itemData.ItemType))
                {
                    ActiveWorldModel.MediaItemsInCurrentWorld.Add(itemData);
                }
            }

            messageData = new MessageData();
            messageData.Parameters.Add((byte)EParameterCodeWorld.WORLD_DATA, worldData);
            messageData.Parameters.Add((byte)EParameterCodeWorld.MODULE_THUMBNAIL_PATH, ActiveWorldModel.ThumbnailPath);

            dispatcher.Dispatch(EWorldHierarchyEvent.RESET_WORLD_HIERARCHY);
            dispatcher.Dispatch(EInspectorEvent.RESET_INSPECTOR);
            dispatcher.Dispatch(EWorldEvent.RESET_WORLD);
        }

        private void UpdateListeners(bool value)
        {
            dispatcher.UpdateListener(value, EWorldEvent.RESET_WORLD_COMPLETED, OnResetWorldCompleted);
            dispatcher.UpdateListener(value, EInspectorEvent.RESET_INSPECTOR_COMPLETED, OnResetInspectorCompleted);
            dispatcher.UpdateListener(value, EWorldHierarchyEvent.RESET_WORLD_HIERARCHY_COMPLETED, OnResetWorldHierarchyCompleted);
        }

        private void OnResetWorldCompleted()
        {
            _worldReset = true;

            dispatcher.Dispatch(EWorldEvent.WORLD_CREATED, messageData);

            CheckAllReset();
        }

        private void OnResetInspectorCompleted()
        {
            _inspectorPanelReset = true;

            dispatcher.Dispatch(EInspectorEvent.WORLD_CREATED, messageData);

            CheckAllReset();
        }

        private void OnResetWorldHierarchyCompleted()
        {
            _worldHierarchyReset = true;

            dispatcher.Dispatch(EWorldHierarchyEvent.WORLD_CREATED, messageData);

            CheckAllReset();
        }

        private void CheckAllReset()
        {
            if(_worldReset && _inspectorPanelReset && _worldHierarchyReset)
            {
                Release();

                UpdateListeners(false);
            }
        }

        private bool IsMedia(ItemTypeCode itmType)
        {
            if(itmType == ItemTypeCode.PANORAMA || itmType == ItemTypeCode.PANORAMA_VIDEO || itmType == ItemTypeCode.PLANAR_VIDEO || itmType == ItemTypeCode.STEREO_PANORAMA_VIDEO)
            {
                return true;
            }

            return false;
        }
    }
}
