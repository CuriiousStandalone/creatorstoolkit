﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using strange.extensions.dispatcher.eventdispatcher.api;
using System.Collections.Generic;
using System.IO;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class LocalDirService : ILocalDirService
    {
        [Inject]
        public IEventDispatcher dispatcher { get; set; }

        public void GetTemplateIds()
        {
            List<string> filesList = new List<string>();

            //string path = CreatorToolkitSettings.LocalTemplateDirectory;

            //if (Directory.Exists(path))
            //{
            //    List<string> files = new List<string>(Directory.GetFiles(path));

            //    foreach (var file in files)
            //    {
            //        filesList.Add(file);
            //    }
            //}

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeWorld.TEMPLATE_PATH_LIST, filesList);

            dispatcher.Dispatch(EWorldEvent.TEMPLATE_PATHS_LOADED, msgData);
        }
    }
}
