﻿using strange.extensions.dispatcher.eventdispatcher.api;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public interface ILocalDirService
    {
        IEventDispatcher dispatcher { get; set; }

        void GetTemplateIds();
    }
}
