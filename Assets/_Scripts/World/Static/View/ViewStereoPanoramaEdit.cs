﻿using UnityEngine;

using strange.extensions.mediation.impl;

using RenderHeads.Media.AVProVideo;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Static
{
    public class ViewStereoPanoramaEdit : EventView
    {
        internal const string UPDATE_SLIDER = "UPDATE_SLIDER";
        internal const string SEND_MAX_VIDEO_TIME = "SEND_MAX_VIDEO_TIME";
        internal const string VIDEO_ENDED = "VIDEO_ENDED";

        [SerializeField]
        MediaPlayer mediaPlayer;

        private float currentVideoFrame = 0f;
        private bool updatingTime = false;
        private bool reloadPlayer = false;
        private string filePath = "";
        private bool invoking = false;
        MessageData data = new MessageData();

        internal void init()
        {
            mediaPlayer.Events.AddListener(OnVideoEvent);
        }

        private void OnDestroy()
        {
            mediaPlayer.CloseVideo();
            CancelInvoke("UpdateVideoTime");
            base.OnDestroy();
        }

        public void OnClosed()
        {
            Destroy(gameObject);
        }

        public void LoadVideo(string path, bool inPreviewMode = false)
        {
            filePath = path;

            if (mediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, path, inPreviewMode))
            {
                if (inPreviewMode)
                {
                    if (!invoking)
                    {
                        InvokeRepeating("UpdateVideoTime", 0.2f, 0.2f);
                        invoking = true;
                    }
                }

                Debug.Log("Loaded 360 Video File");
            }
            else
            {
                Debug.Log("Failed to load 360 Video File");
            }
        }

        public void PauseMedia()
        {
            mediaPlayer.Pause();
        }

        public void PlayMedia()
        {
            mediaPlayer.Play();

            if (!invoking)
            {
                InvokeRepeating("UpdateVideoTime", 0.2f, 0.2f);
                invoking = true;
            }
        }

        public void RestartMedia()
        {
            mediaPlayer.Rewind(true);
        }

        public void SeekMedia(float value)
        {
            float frame = mediaPlayer.Info.GetDurationMs() * value;
            mediaPlayer.Control.Seek(frame);
        }

        public void StopMedia()
        {
            mediaPlayer.Rewind(true);
            mediaPlayer.Stop();
            CancelInvoke("UpdateVideoTime");
            invoking = false;
        }

        public void UpdateVideoTime()
        {
            float value = mediaPlayer.Control.GetCurrentTimeMs();
            data.Parameters.Clear();
            data.Parameters.Add((byte)EParameterCodeStatic.VIDEO_CURRENT_TIME, value);

            dispatcher.Dispatch(UPDATE_SLIDER, data);
        }

        public void SetStereoType(EStereoPanoramaVideoTypeCode videoType)
        {
            switch (videoType)
            {
                default:
                case EStereoPanoramaVideoTypeCode.LEFT_RIGHT:
                    mediaPlayer.m_StereoPacking = StereoPacking.LeftRight;
                    break;
                case EStereoPanoramaVideoTypeCode.TOP_BOTTOM:
                    mediaPlayer.m_StereoPacking = StereoPacking.TopBottom;
                    break;

            }
        }

        public void UpdateStereoType(MessageData data)
        {
            EStereoPanoramaVideoTypeCode videoType = (EStereoPanoramaVideoTypeCode)data[(byte)EParameterCodeStatic.STEREO_PANORAMA_VIDEO_VIDEO_TYPE];

            switch (videoType)
            {
                default:
                case EStereoPanoramaVideoTypeCode.LEFT_RIGHT:
                    mediaPlayer.m_StereoPacking = StereoPacking.LeftRight;
                    reloadPlayer = true;
                    mediaPlayer.CloseVideo();
                    break;
                //case "RIGHT_LEFT":
                //
                //    break;
                case EStereoPanoramaVideoTypeCode.TOP_BOTTOM:
                    mediaPlayer.m_StereoPacking = StereoPacking.TopBottom;
                    reloadPlayer = true;
                    mediaPlayer.CloseVideo();
                    break;
                    //case "BOTTOM_TOP":
                    //    videoType = EStereoPanoramaVideoTypeCode.BOTTOM_TOP;
                    //    break;

            }
        }
        public void OnVideoEvent(MediaPlayer mp, MediaPlayerEvent.EventType et, ErrorCode errorCode)
        {
            switch (et)
            {
                case MediaPlayerEvent.EventType.ReadyToPlay:
                    {
                        data.Parameters.Clear();
                        data.Parameters.Add((byte)EParameterCodeStatic.VIDEO_DURATION, mediaPlayer.Info.GetDurationMs());

                        dispatcher.Dispatch(SEND_MAX_VIDEO_TIME, data);

                        break;
                    }

                case MediaPlayerEvent.EventType.FinishedPlaying:
                    {
                        dispatcher.Dispatch(VIDEO_ENDED);
                        break;
                    }

                case MediaPlayerEvent.EventType.Closing:
                    {
                        if (reloadPlayer)
                        {
                            reloadPlayer = false;
                            Invoke("DelayReloadVideo", 1f);
                        }
                        break;
                    }
            }
        }

        private void DelayReloadVideo()
        {
            if (mediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, filePath, false))
            {
                Debug.Log("Loaded 360 Video File");
            }
            else
            {
                Debug.Log("Failed to load 360 Video File");
            }
        }
    }
}
