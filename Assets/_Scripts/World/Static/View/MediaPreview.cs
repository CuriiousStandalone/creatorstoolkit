﻿using UnityEngine;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Static
{
    public class MediaPreview : MonoBehaviour
    {
        private Transform _billboardTransform = null;

        private float _distanceScaleFactor = 0.05f;
        private float _translationSize = 0f;
        private float _minDistance = 20;
        private float _maxDistance = 55;

        private void Update()
        {
            transform.LookAt(_billboardTransform);

            _translationSize = (_billboardTransform.position - transform.position).magnitude;

            if (_translationSize > _maxDistance || _translationSize < _minDistance)
            {
                return;
            }

            transform.localScale = new Vector3(_translationSize, _translationSize, _translationSize) * _distanceScaleFactor;

        }

        public void SetLookAtTranform(Transform lookAt)
        {
            _billboardTransform = lookAt;
        }
    }
}
