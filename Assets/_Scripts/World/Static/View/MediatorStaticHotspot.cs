﻿using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

using PulseIQ.Local.ClientCommon.Components.Common;
using UnityEngine;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.Common.Model.Item;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.World.Inspector;
using CuriiousIQ.Local.CreatorsToolkit.World.WorldHierarchy;
using System.Collections.Generic;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Static
{
    public class MediatorStaticHotspot : EventMediator
    {
        [Inject]
        public ViewStaticHotspot view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            dispatcher.UpdateListener(value, EStaticEvent.HOTSPOT_CREATED, OnCreatedHotspotProcessed);
            dispatcher.UpdateListener(value, EStaticEvent.HOTSPOT_DELETED, OnHotspotDeleted);
            dispatcher.UpdateListener(value, EStaticEvent.HOTSPOT_XY_POS_UPDATED, OnXYUpdated);
            dispatcher.UpdateListener(value, EStaticEvent.UPDATE_HOTSPOT_POSITION, OnUpdateHotspotPosition);
            dispatcher.UpdateListener(value, EInspectorEvent.UPDATE_STATIC_HOTSPOT_ICON, OnUpdateHotspotIcon);
            dispatcher.UpdateListener(value, EWorldEvent.DEFAULT_STATUS_ICON_IDS, OnSetDefaultIconID);
            dispatcher.UpdateListener(value, EWorldHierarchyEvent.REMOVE_HOTSPOT, OnHotspotDeleted);
            dispatcher.UpdateListener(value, EStaticEvent.MEDIA_VIDEO_CURRENT_TIME, OnCurrentTimeRecieved);

            view.dispatcher.UpdateListener(value, ViewStaticHotspot.StaticHotspotEvents.HOTSPOT_SELECTED, OnHotspotSelected);
        }

        private void OnCurrentTimeRecieved(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;
            float time = (float)data[(byte)EParameterCodeStatic.VIDEO_CURRENT_TIME];

            view.VideoTimeReceived(time);
        }

        private void OnUpdateHotspotIcon(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;

            view.UpdateHotspotIcon(data);
        }

        private void OnSetDefaultIconID(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;
            List<string> list = (List<string>)data[(byte)EParameterCodeToolkit.HOTSPOT_ICON_ID];

            view.SetDefaultStatusIcons(list[0]);
        }

        private void OnCreatedHotspotProcessed(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            CTHotspotData hotspotData = (CTHotspotData)msgData[(byte)EParameterCodeStatic.HOTSPOT_DATA];
            bool isLastLoadedMedia = (bool)msgData[(byte)EParameterCodeStatic.IS_LAST_LOADED_MEDIA];

            if (view.HotspotID == hotspotData.HotspotId)
            {
                Camera camera = (Camera)msgData[(byte)EParameterCodeStatic.MAIN_CAMERA];

                CTItemData itemData = (CTItemData)msgData[(byte)EParameterCodeStatic.ITEM_DATA];

                float hotspotDrawDistance = (float)msgData[(byte)EParameterCodeStatic.HOTSPOT_DRAW_DISTANCE];

                view.SetHotspotData(camera.transform, itemData.ItemIndex, itemData.ItemType, hotspotData, hotspotDrawDistance, isLastLoadedMedia);
            }
        }

        private void OnHotspotDeleted(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;

            view.HotspotDeleted(data);
        }

        private void OnXYUpdated(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;

            //view.SetPosition(data);
        }

        private void OnSetXYPosition(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;

            //view.SetPosition(data);
        }

        private void OnHotspotSelected(IEvent evt)
        {
            dispatcher.Dispatch(EStaticEvent.HOTSPOT_ITEM_SELECTED, evt.data);
        }

        private void OnUpdateHotspotPosition(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;

            int index = (int)data[(byte)EParameterCodeToolkit.ITEM_INDEX];
            string hotspotID = (string)data[(byte)EParameterCodeToolkit.HOTSPOT_ID];
            Vector2 xyPos = (Vector2)data[(byte)EParameterCodeToolkit.HOTSPOT_XY_POSITION];

            if (view.HotspotItemIndex == index && view.HotspotID == hotspotID)
            {
                view.SetPosition(xyPos);
            }
        }
    }
}
