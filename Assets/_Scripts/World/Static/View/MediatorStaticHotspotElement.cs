﻿using PulseIQ.Local.ClientCommon.Components.Common;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Static
{
    public class MediatorStaticHotspotElement : EventMediator
    {
        [Inject]
        public ViewStaticHotspotElement view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            dispatcher.UpdateListener(value, EStaticEvent.UPDATE_HOTSPOT_ELEMENT, UpdateHotspotElement);
        }

        private void UpdateHotspotElement(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;
            view.UpdateHotspotElement(data);
        }
    }
}