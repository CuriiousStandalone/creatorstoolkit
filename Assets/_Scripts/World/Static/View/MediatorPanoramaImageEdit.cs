﻿using strange.extensions.mediation.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Static
{
    public class MediatorPanoramaImageEdit : EventMediator
    {
        [Inject]
        public ViewPanoramaImageEdit view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewPanoramaImageEdit.END_BEHAVIOUR_TRIGGERED, OnEndBehaviourTriggered);
        }

        private void OnEndBehaviourTriggered()
        {
            dispatcher.Dispatch(EStaticEvent.PANORAMA_IMAGE_TELEPORT_TIME_TRIGGERED);
        }
    }
}
