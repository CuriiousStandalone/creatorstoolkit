﻿using UnityEngine;

using strange.extensions.mediation.impl;

using RenderHeads.Media.AVProVideo;

using PulseIQ.Local.ClientCommon.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Static
{
    public class ViewVideoPanoramaEdit : EventView
    {
        internal const string UPDATE_SLIDER = "UPDATE_SLIDER";
        internal const string SEND_MAX_VIDEO_TIME = "SEND_MAX_VIDEO_TIME";
        internal const string VIDEO_ENDED = "VIDEO_ENDED";

        [SerializeField]
        MediaPlayer mediaPlayer;

        MessageData data = new MessageData();

        private bool invoking = false;

        private float currentVideoFrame = 0f;

        internal void init()
        {
            mediaPlayer.Events.AddListener(OnVideoEvent);
        }

        private void OnDestroy()
        {
            mediaPlayer.CloseVideo();
            CancelInvoke("UpdateVideoTime");
            base.OnDestroy();
        }

        public void OnClosed()
        {
            Destroy(gameObject);
        }

        public void LoadVideo(string path, bool inPreviewMode = false)
        {
            if (mediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, path, inPreviewMode))
            {
                Debug.Log("Loaded 360 Video File");

                if(inPreviewMode)
                {
                    if (!invoking)
                    {
                        InvokeRepeating("UpdateVideoTime", 0.2f, 0.2f);
                        invoking = true;
                    }
                }
            }
            else
            {
                Debug.Log("Failed to load 360 Video File");
            }
        }
         
        public void PauseMedia()
        {
            mediaPlayer.Pause();
        }

        public void PlayMedia()
        {
            mediaPlayer.Play();

            if (!invoking)
            {
                InvokeRepeating("UpdateVideoTime", 0.2f, 0.2f);
                invoking = true;
            }
        }

        public void RestartMedia()
        {
            mediaPlayer.Rewind(true);
        }

        public void SeekMedia(float value)
        {
            float frame = mediaPlayer.Info.GetDurationMs() * value;
            mediaPlayer.Control.Seek(frame);
        }

        public void StopMedia()
        {
            mediaPlayer.Rewind(true);
            mediaPlayer.Stop();
            CancelInvoke("UpdateVideoTime");
            invoking = false;
        }

        public void UpdateVideoTime()
        {
            float value = mediaPlayer.Control.GetCurrentTimeMs();
            data.Parameters.Clear();
            data.Parameters.Add((byte)EParameterCodeStatic.VIDEO_CURRENT_TIME, value);

            dispatcher.Dispatch(UPDATE_SLIDER, data);
        }

        public void OnVideoEvent(MediaPlayer mp, MediaPlayerEvent.EventType et, ErrorCode errorCode)
        {
            switch (et)
            {
                case MediaPlayerEvent.EventType.ReadyToPlay:
                    {
                        data.Parameters.Clear();
                        data.Parameters.Add((byte)EParameterCodeStatic.VIDEO_DURATION, mediaPlayer.Info.GetDurationMs());

                        dispatcher.Dispatch(SEND_MAX_VIDEO_TIME, data);

                        break;
                    }

                case MediaPlayerEvent.EventType.FinishedPlaying:
                    {
                        dispatcher.Dispatch(VIDEO_ENDED);
                        break;
                    }
            }
        }
    }
}
