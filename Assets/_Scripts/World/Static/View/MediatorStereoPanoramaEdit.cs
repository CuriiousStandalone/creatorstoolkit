﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Static
{
    public class MediatorStereoPanoramaEdit : EventMediator
    {
        [Inject]
        public ViewStereoPanoramaEdit view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewStereoPanoramaEdit.UPDATE_SLIDER, OnUpdateSlider);
            view.dispatcher.UpdateListener(value, ViewStereoPanoramaEdit.SEND_MAX_VIDEO_TIME, OnSendMaxVideoTime);
            view.dispatcher.UpdateListener(value, ViewStereoPanoramaEdit.VIDEO_ENDED, OnViewEnded);

            dispatcher.UpdateListener(value, EStaticEvent.MEDIA_PLAY, OnPlayMedia);
            dispatcher.UpdateListener(value, EStaticEvent.MEDIA_STOP, OnStopMedia);
            dispatcher.UpdateListener(value, EStaticEvent.MEDIA_PAUSE, OnPauseMedia);
            dispatcher.UpdateListener(value, EStaticEvent.MEDIA_REPLAY, OnReplayMedia);
            dispatcher.UpdateListener(value, EStaticEvent.MEDIA_SEEK, OnSeekMedia);
        }

        private void OnPlayMedia()
        {
            view.PlayMedia();
        }

        private void OnStopMedia()
        {
            view.StopMedia();
        }

        private void OnPauseMedia()
        {
            view.PauseMedia();
        }

        private void OnReplayMedia()
        {
            view.RestartMedia();
        }

        private void OnSeekMedia(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;
            float value = (float)data[(byte)EParameterCodeToolkit.MEDIA_VIDEO_CURRENT_TIME];
            view.SeekMedia(value);
        }

        private void OnUpdateSlider(IEvent evt)
        {
            dispatcher.Dispatch(EStaticEvent.MEDIA_VIDEO_CURRENT_TIME, evt.data);
        }

        private void OnSendMaxVideoTime(IEvent evt)
        {
            dispatcher.Dispatch(EStaticEvent.MEDIA_VIDEO_LOADED, evt.data);
        }

        private void OnViewEnded()
        {
            dispatcher.Dispatch(EStaticEvent.STEREO_PANORAMA_VIDEO_ENDED);
        }
    }
}
