﻿using PulseIQ.Local.ClientCommon.Components.Common;
using strange.extensions.mediation.impl;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Static
{
    public class ViewPanoramaImageEdit : EventView
    {
        internal const string END_BEHAVIOUR_TRIGGERED = "END_BEHAVIOUR_TRIGGERED";

        private Texture2D imageTexture = null;
        public Renderer imageRenderer = null;
        private Coroutine teleportCoroutine;

        public Material monoPanoramaImageMaterial;

        internal void init()
        {

        }

        private void OnDestroy()
        {
            if (null != teleportCoroutine)
            {
                StopCoroutine(teleportCoroutine);
                teleportCoroutine = null;
            }
            imageRenderer.material.mainTexture = null;
            Destroy(imageRenderer.material);
            imageTexture = null;
            base.OnDestroy();
        }

        public void OnClosed()
        {
            Destroy(gameObject);
        }

        public void LoadImage(string path, bool inPreviewMode = false, float triggerTime = 0f)
        {
            StartCoroutine(LoadMediaImage(Settings.PanoramaPath + path, inPreviewMode, triggerTime));
        }

        IEnumerator LoadMediaImage(string path, bool inPreviewMode = false, float triggerTime = 0f)
        {
            using (UnityWebRequest webRequest = UnityWebRequestTexture.GetTexture("file://" + path))
            {
                yield return webRequest.SendWebRequest();
                if (webRequest.isNetworkError || webRequest.isHttpError)
                {
                    Debug.Log(webRequest.error);
                }
                else
                {
                    // Get downloaded asset bundle
                    var panoTexture = DownloadHandlerTexture.GetContent(webRequest);

                    Material panoImageMat = new Material(monoPanoramaImageMaterial);
                    panoImageMat.SetTexture("_Tex", panoTexture);
            
                    imageRenderer.material = panoImageMat;
                }
            }

            // Old loading code
            //using (WWW www = new WWW("file://" + path))
            //{
            //    yield return www;
            //
            //    if (null != www.error)
            //    {
            //        Debug.Log("WWW Error. CODE:  " + www.error);
            //    }
            //
            //    imageTexture = new Texture2D(2, 2);
            //
            //    www.LoadImageIntoTexture(imageTexture);
            //    imageRenderer.material.SetTexture("_Tex", imageTexture);
            //
            //    //RenderSettings.skybox = imageRenderer.material;
            //
            //    www.Dispose();
            //
            //    if(inPreviewMode && triggerTime > 0)
            //    {
            //        teleportCoroutine = StartCoroutine(StartTeleportTime(triggerTime));
            //    }
            //}
        }

        IEnumerator StartTeleportTime(float time)
        {
            yield return new WaitForSeconds(time);

            teleportCoroutine = null;

            dispatcher.Dispatch(END_BEHAVIOUR_TRIGGERED);
        }
    }
}
