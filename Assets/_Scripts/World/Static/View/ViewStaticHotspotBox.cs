﻿using UnityEngine;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Hotspot;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.Common.Model.Hotspot.HotspotElement;
using TMPro;
using UnityEngine.UI;
using System.IO;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Static
{
    public class ViewStaticHotspotBox : MonoBehaviour
    {
        [SerializeField]
        GameObject textObj;

        [SerializeField]
        GameObject imageObj;

        [SerializeField]
        GameObject infoObj;


        [SerializeField]
        SpriteRenderer backgroundBoxRenderer;

        private Vector3 lookAtPos = new Vector3();

        private string _hotspotId = "";

        //public void SetHotspotBoxData(CTHotspotBoxData data, Vector3 lookAt, string hotspotID)
        //{
        //    _hotspotId = hotspotID;
        //    backgroundBoxRenderer.size = new Vector2(data.Width / 100, data.Height / 100);
        //
        //    int elementID = 0;
        //
        //    Debug.Log("11111111111111111111111         " + data.Elements.Count);
        //
        //    foreach (var item in data.Elements)
        //    {
        //        switch (item.TypeCode)
        //        {
        //            case EHotspotElementTypeCode.TEXT:
        //                {
        //                    GameObject textElement = Instantiate((Resources.Load(PrefabNameConstants.HOTSPOT_TEXT_ELEMENT) as GameObject), transform);
        //
        //                    ViewStaticHotspotElement element = textElement.GetComponent<ViewStaticHotspotElement>();
        //                    element.SetHotspotElementData(item, data.Width / 100, data.Height / 100, elementID, _hotspotId);
        //                    break;
        //                }
        //
        //            case EHotspotElementTypeCode.IMAGE:
        //                {
        //                    GameObject imageElement = Instantiate((Resources.Load(PrefabNameConstants.HOTSPOT_IMAGE_ELEMENT) as GameObject), transform);
        //
        //                    ViewStaticHotspotElement element = imageElement.GetComponent<ViewStaticHotspotElement>();
        //                    element.SetHotspotElementData(item, data.Width / 100, data.Height / 100, elementID, _hotspotId);
        //                    break;
        //                }
        //
        //            case EHotspotElementTypeCode.AUDIO:
        //                {
        //                    GameObject audioElement = Instantiate(Resources.Load(PrefabNameConstants.HOTSPOT_AUDIO_ELEMENT) as GameObject, transform);
        //
        //                    ViewStaticHotspotElement element = audioElement.GetComponent<ViewStaticHotspotElement>();
        //                    element.SetHotspotElementData(item, data.Width / 100, data.Height / 100, elementID, _hotspotId);
        //                    break;
        //                }
        //
        //            default:
        //                {
        //                    GameObject go = new GameObject("hotspot element");
        //                    break;
        //                }
        //        }
        //        elementID++;
        //    }
        //    lookAtPos = lookAt;
        //
        //    transform.localPosition = new Vector3(data.X, data.Y);
        //
        //    //transform.LookAt(lookAtPos);
        //
        //    transform.Rotate(new Vector3(0, 180, 0));
        //}

        public void SetHotspotBoxData(CTHotspotBoxData data, Vector3 lookAt, string hotspotID)
        {
            _hotspotId = hotspotID;

            int elementID = 0;

            if (data.Elements.Count > 1)
            {
                infoObj.SetActive(true);

                int count = 0;

                foreach (var item in data.Elements)
                {
                    _hotspotId = hotspotID;

                    switch (item.TypeCode)
                    {
                        case EHotspotElementTypeCode.TEXT:
                            {
                                ++count;

                                if (count == 1)
                                {
                                    ViewStaticHotspotElement element = infoObj.GetComponent<ViewStaticHotspotElement>();
                                    element.SetHotspotElementData(item, data.Width / 100, data.Height / 100, elementID, _hotspotId, true);
                                }
                                else
                                {
                                    ViewStaticHotspotElement element = infoObj.GetComponent<ViewStaticHotspotElement>();
                                    element.SetHotspotElementData(item, data.Width / 100, data.Height / 100, elementID, _hotspotId, true);
                                }

                                break;
                            }

                        case EHotspotElementTypeCode.IMAGE:
                            {
                                ViewStaticHotspotElement element = infoObj.GetComponent<ViewStaticHotspotElement>();

                                element.SetHotspotElementData(item, data.Width / 100, data.Height / 100, elementID, _hotspotId, true);
                                break;
                            }

                        case EHotspotElementTypeCode.AUDIO:
                            {
                                GameObject audioElement = Instantiate(Resources.Load(PrefabNameConstants.HOTSPOT_AUDIO_ELEMENT) as GameObject, transform);

                                ViewStaticHotspotElement element = audioElement.GetComponent<ViewStaticHotspotElement>();
                                element.SetHotspotElementData(item, data.Width / 100, data.Height / 100, elementID, _hotspotId);
                                break;
                            }

                        default:
                            {
                                GameObject go = new GameObject("hotspot element");
                                break;
                            }
                    }
                    elementID++;
                }
                lookAtPos = lookAt;

                //transform.localPosition = new Vector3(data.X, data.Y);

                //transform.LookAt(lookAtPos);

                transform.Rotate(new Vector3(0, 180, 0));
            }
            else
            {
                foreach (var item in data.Elements)
                {
                    switch (item.TypeCode)
                    {
                        case EHotspotElementTypeCode.TEXT:
                            {
                                textObj.SetActive(true);
                                ViewStaticHotspotElement element = textObj.GetComponent<ViewStaticHotspotElement>();
                                element.SetHotspotElementData(item, data.Width / 100, data.Height / 100, elementID, _hotspotId);
                                break;
                            }

                        case EHotspotElementTypeCode.IMAGE:
                            {
                                imageObj.SetActive(true);
                                ViewStaticHotspotElement element = imageObj.GetComponent<ViewStaticHotspotElement>();

                                element.SetHotspotElementData(item, data.Width / 100, data.Height / 100, elementID, _hotspotId);
                                break;
                            }

                        case EHotspotElementTypeCode.AUDIO:
                            {
                                GameObject audioElement = Instantiate(Resources.Load(PrefabNameConstants.HOTSPOT_AUDIO_ELEMENT) as GameObject, transform);

                                ViewStaticHotspotElement element = audioElement.GetComponent<ViewStaticHotspotElement>();
                                element.SetHotspotElementData(item, data.Width / 100, data.Height / 100, elementID, _hotspotId);
                                break;
                            }

                        default:
                            {
                                GameObject go = new GameObject("hotspot element");
                                break;
                            }
                    }
                    elementID++;
                }
                lookAtPos = lookAt;

                transform.localPosition = new Vector3(data.X, data.Y);

                //transform.LookAt(lookAtPos);

                transform.Rotate(new Vector3(0, 180, 0));
            }
            //backgroundBoxRenderer.size = new Vector2(data.Width / 100, data.Height / 100);

            //int elementID = 0;            
        }

        private Sprite LoadImage(string imagePath, float canvasWidth, float canvasHeight)
        {
            if (imagePath != "" && File.Exists(imagePath))
            {
                Texture2D tex = null;
                byte[] fileData;

                fileData = File.ReadAllBytes(imagePath);
                tex = new Texture2D(2, 2);
                tex.LoadImage(fileData);

                int originalWidth = tex.width;
                int originalHeight = tex.height;

                float ratioX = (float)canvasWidth / (float)originalWidth;
                float ratioY = (float)canvasHeight / (float)originalHeight;

                float ratio = ratioX < ratioY ? ratioX : ratioY;

                int newHeight = System.Convert.ToInt32(originalHeight * ratio);
                int newWidth = System.Convert.ToInt32(originalWidth * ratio);

                return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2());
            }

            return null;
        }
    }
}
