﻿using System.IO;

using UnityEngine;

using strange.extensions.mediation.impl;

using PulseIQ.Local.Common;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.CustomType;
using PulseIQ.Local.Common.Model.PanoramaVideo;
using PulseIQ.Local.Common.Model.Multimedia;
using PulseIQ.Local.Common.Model.Panorama;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;
using PulseIQ.Local.Common.Model.PlanarVideo;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Static
{
    public class ViewStaticItem : EventView
    {
        private int _itemIndex;

        private CTItemData _itemData = new CTItemData();

        public int ItemIndex
        {
            get
            {
                return _itemIndex;
            }

            set
            {
                _itemData.ItemIndex = value;
                _itemIndex = value;
            }
        }

        public void SetItemResourceID(string resourceID)
        {
            _itemData.ItemResourceId = resourceID;
        }

        public void SetItemName(string name)
        {
            _itemData.ItemName = name;
        }

        public string GetItemName()
        {
            return _itemData.ItemName;
        }

        public ItemTypeCode GetItemType()
        {
            return _itemData.ItemType;
        }

        public void SetItemPosition(CTVector3 itemPosition)
        {
            _itemData.Position = itemPosition;
            Vector3 itemPos = new Vector3(itemPosition.X, itemPosition.Y, itemPosition.Z);
            transform.position = itemPos;
        }

        public void SetItemRotation(CTQuaternion itemRotation)
        {
            _itemData.Rotation = itemRotation;
            Quaternion itemRot = new Quaternion(itemRotation.X, itemRotation.Y, itemRotation.Z, itemRotation.W);
            transform.rotation = itemRot;
        }

        public void SetItemScale(CTVector3 itemScale)
        {
            _itemData.Scale = itemScale;
            Vector3 scale = new Vector3(itemScale.X, itemScale.Y, itemScale.Z);
            transform.localScale = scale;
        }

        public CTVector3 GetItemScale()
        {
            CTVector3 localScale = new CTVector3(transform.localScale.x, transform.localScale.y, transform.localScale.z);
            return localScale;
        }

        public void SetItemData(CTItemData data)
        {
            _itemData = data;
        }

        public CTItemData GetItemData()
        {
            return _itemData;
        }

        public string GetMediaExtension(ItemTypeCode itemType)
        {
            switch (itemType)
            {
                case ItemTypeCode.PANORAMA:
                    {
                        return Path.GetExtension(((CTPanoramaData)_itemData).PanoramaId);
                    }

                case ItemTypeCode.PANORAMA_VIDEO:
                    {
                        return Path.GetExtension(((CTPanoramaVideoData)_itemData).PanoramaVideoId);
                    }

                case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                    {
                        return Path.GetExtension(((CTStereoPanoramaVideoData)_itemData).StereoPanoramaVideoId);
                    }

                case ItemTypeCode.PLANAR_VIDEO:
                    {
                        return Path.GetExtension(((CTPlanarVideoData)_itemData).PlanarVideoId);
                    }
                default:
                    {
                        return "null string";
                    }
            }
        }

        public string GetMultimediaID(ItemTypeCode itemType)
        {
            switch (itemType)
            {
                case ItemTypeCode.PANORAMA:
                    {
                        return ((CTPanoramaData)_itemData).PanoramaId;
                    }

                case ItemTypeCode.PANORAMA_VIDEO:
                    {
                        return ((CTPanoramaVideoData)_itemData).PanoramaVideoId;
                    }

                case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                    {
                        return ((CTStereoPanoramaVideoData)_itemData).StereoPanoramaVideoId;
                    }

                case ItemTypeCode.PLANAR_VIDEO:
                    {
                        return ((CTPlanarVideoData)_itemData).PlanarVideoId;
                    }
                default:
                    {
                        return "null string";
                    }
            }
        }

        public void SetIsMultiplayer(ItemTypeCode itemType, bool value)
        {
            switch (itemType)
            {
                case ItemTypeCode.PANORAMA:
                    {
                        ((CTPanoramaData)_itemData).IsMultiplayer = value;
                        break;
                    }

                case ItemTypeCode.PANORAMA_VIDEO:
                    {
                        ((CTPanoramaVideoData)_itemData).IsMultiplayer = value;
                        break;
                    }

                case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                    {
                        ((CTStereoPanoramaVideoData)_itemData).IsMultiplayer = value;
                        break;
                    }

                case ItemTypeCode.PLANAR_VIDEO:
                    {
                        break;
                    }
                default:
                    {
                        return;
                    }
            }
        }

        public bool GetIsMultiplayer(ItemTypeCode itemType)
        {
            switch (itemType)
            {
                case ItemTypeCode.PANORAMA:
                    {
                        return ((CTPanoramaData)_itemData).IsMultiplayer;
                    }

                case ItemTypeCode.PANORAMA_VIDEO:
                    {
                        return ((CTPanoramaVideoData)_itemData).IsMultiplayer;
                    }

                case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                    {
                        return ((CTStereoPanoramaVideoData)_itemData).IsMultiplayer;
                    }

                case ItemTypeCode.PLANAR_VIDEO:
                    {
                        return false;
                    }
                default:
                    {
                        return false;
                    }
            }
        }

        #region Stereo Panorama Video Functions

        public void UpdateStereoPanoramaVideoVideoType(EStereoPanoramaVideoTypeCode videoType)
        {
            ((CTStereoPanoramaVideoData)_itemData).StereoPanoramaVideoType = videoType;
        }

        public EStereoPanoramaVideoTypeCode GetStereoPanoramaVideoVideoType()
        {
            return ((CTStereoPanoramaVideoData)_itemData).StereoPanoramaVideoType;
        }

        public void UpdateStereoPanoramaVideoEndBehaviour(EMultimediaEndBehaviorCode endBehaviorCode)
        {
            ((CTStereoPanoramaVideoData)_itemData).EndBehavior = endBehaviorCode;
        }

        public EMultimediaEndBehaviorCode GetStereoPanoramaVideoEndBehaviour()
        {
            return ((CTStereoPanoramaVideoData)_itemData).EndBehavior;
        }

        public void UpdateStereoPanoramaVideoTeleportItemIndex(int value)
        {
            ((CTStereoPanoramaVideoData)_itemData).TargetMultimediaItemIndex = value;
        }

        public int GetStereoPanoramaVideoTeleportItemIndex()
        {
            return ((CTStereoPanoramaVideoData)_itemData).TargetMultimediaItemIndex;
        }

        public GameObject LoadStereoPanoramaVideo(Camera mainCamera, Transform parent, bool inPreviewMode = false)
        {
            GameObject mediaPlayer = Instantiate(Resources.Load("Prefabs/MediaPlayers/StereoPanoramaVideoPlayer") as GameObject, parent);
            mediaPlayer.transform.position = mainCamera.transform.position;
            mediaPlayer.GetComponent<ViewStereoPanoramaEdit>().SetStereoType(((CTStereoPanoramaVideoData)_itemData).StereoPanoramaVideoType);
            mediaPlayer.GetComponent<ViewStereoPanoramaEdit>().LoadVideo(Settings.StereoPanoramaVideoPath + ((CTStereoPanoramaVideoData)_itemData).StereoPanoramaVideoId, inPreviewMode);

            return mediaPlayer;
        }

        #endregion

        #region Panorama Video

        public void UpdatePanoramaVideoEndBehaviour(EMultimediaEndBehaviorCode endBehaviorCode)
        {
            ((CTPanoramaVideoData)_itemData).EndBehavior = endBehaviorCode;
        }

        public EMultimediaEndBehaviorCode GetPanoramaVideoEndBehaviour()
        {
            return ((CTPanoramaVideoData)_itemData).EndBehavior;
        }

        public int GetPanoramaVideoTeleportItemIndex()
        {
            return ((CTPanoramaVideoData)_itemData).TargetMultimediaItemIndex;
        }

        public void UpdatePanoramaVideoTeleportItemIndex(int value)
        {
            ((CTPanoramaVideoData)_itemData).TargetMultimediaItemIndex = value;
        }

        public GameObject LoadPanoramaVideo(Camera mainCamera, Transform parent, bool inPreviewMode = false)
        {
            GameObject mediaPlayer = Instantiate(Resources.Load("Prefabs/MediaPlayers/PanoramaVideoPlayer") as GameObject, parent);
            mediaPlayer.transform.position = mainCamera.transform.position;
            mediaPlayer.GetComponent<ViewVideoPanoramaEdit>().LoadVideo(Settings.PanoramaVideoPath + ((CTPanoramaVideoData)_itemData).PanoramaVideoId, inPreviewMode);

            return mediaPlayer;
        }

        #endregion

        #region Panorama Image

        public void UpdatePanoramaImageEndBehaviour(EMultimediaEndBehaviorCode endBehaviorCode)
        {
            ((CTPanoramaData)_itemData).EndBehavior = endBehaviorCode;
        }

        public EMultimediaEndBehaviorCode GetPanoramaImageEndBehaviour()
        {
            return ((CTPanoramaData)_itemData).EndBehavior;
        }

        public int GetPanoramaImageTeleportItemIndex()
        {
            return ((CTPanoramaData)_itemData).TargetMultimediaItemIndex;
        }

        public void UpdatePanoramaImageTeleportItemIndex(int value)
        {
            ((CTPanoramaData)_itemData).TargetMultimediaItemIndex = value;
        }

        public float GetPanoramaImageTeleportTime()
        {
            return ((CTPanoramaData)_itemData).PanoramaTeleportTime;
        }

        public void UpdatePanoramaImageTeleportTime(float value)
        {
            ((CTPanoramaData)_itemData).PanoramaTeleportTime = value;
        }

        public GameObject LoadPanoramaImage(Camera mainCamera, Transform parent, bool inPreviewMode = false)
        {
            GameObject mediaPlayer = Instantiate(Resources.Load("Prefabs/MediaPlayers/PanoramaImagePlayer") as GameObject, parent);
            mediaPlayer.transform.position = mainCamera.transform.position;
            mediaPlayer.GetComponent<ViewPanoramaImageEdit>().LoadImage(((CTPanoramaData)_itemData).PanoramaId, inPreviewMode, ((CTPanoramaData)_itemData).PanoramaTeleportTime);

            return mediaPlayer;
        }

        #endregion

        #region Planar Video

        public void UpdatePlanarVideoEndBehaviour(EMultimediaEndBehaviorCode endBehaviorCode)
        {
            //((CTPlanarVideoData)_itemData).EndBehavior = endBehaviorCode;
        }

        public EMultimediaEndBehaviorCode GetPlanarVideoEndBehaviour()
        {
            //return ((CTPlanarVideoData)_itemData).EndBehavior;
            return EMultimediaEndBehaviorCode.CLOSE;
        }

        #endregion
    }
}
