﻿using System.Collections.Generic;

using UnityEngine;

using strange.extensions.mediation.impl;

using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.ClientCommon.Hotspot;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using TMPro;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Static
{
    public class ViewStaticHotspot : EventView
    {
        internal enum StaticHotspotEvents
        {
            HOTSPOT_SELECTED,
        }

        [SerializeField]
        private SpriteRenderer _hotspotSprite;

        private Transform _lookAtTransform = null;

        private int _hotspotItemIndex = 0;

        private string _hotspotId = "";
        private string _defaultHotspotIconID = "";

        private ItemTypeCode _hotspotItemType;

        private float _xRot = 0;
        private float _yRot = 0;
        private float _drawDistance = 0;

        private bool _isInPreviewMode = false;
        private bool _firstSelection = false;

        private List<GameObject> hotspotBoxList;

        private float activeTime = 0f;
        private float deactiveTime = 0f;
        private float currentVideoTime = 0f;

        List<SpriteRenderer> hotspotSprites = new List<SpriteRenderer>();
        List<TMP_Text> hotspotTexts = new List<TMP_Text>();
        private float currentTimer = 0f;
        private float fadeTime = 0.5f;
        private float fadeOutTime = 0.3f;
        private float fadeInTime = 0.5f;
        private bool isFading = false;
        private bool fadeIn = false;

        public float DrawDistance
        {
            get
            {
                return _drawDistance;
            }
        }

        public string HotspotID
        {
            get
            {
                return _hotspotId;
            }
            set
            {
                _hotspotId = value;
            }
        }

        public bool IsInPreviewMode
        {
            get
            {
                return _isInPreviewMode;
            }

            set
            {
                _isInPreviewMode = value;
            }
        }

        public int HotspotItemIndex
        {
            get
            {
                return _hotspotItemIndex;
            }
        }

        private void Update()
        {
            if (null != _lookAtTransform)
            {
                transform.LookAt(_lookAtTransform);
            }

            if(isFading)
            {
                currentTimer += Time.deltaTime;

                if (fadeIn)
                {
                    FadeHotspotBox(currentTimer / fadeTime);
                }
                else
                {
                    FadeHotspotBox(1f - (currentTimer / fadeTime));
                }

                if(currentTimer > fadeTime)
                {
                    isFading = false;
                    currentTimer = 0f;

                    if (fadeIn)
                    {
                        FadeHotspotBox(1f);
                    }
                    else
                    {
                        FadeHotspotBox(0);

                        foreach (var hotspotBox in hotspotBoxList)
                        {
                            hotspotBox.SetActive(false);
                        }
                    }
                }
            }
        }

        internal void SetHotspotData(Transform camera, int index, ItemTypeCode code, CTHotspotData hotspotData, float hsDrawDistance, bool isLastLoadedMedia = false)
        {
            _hotspotItemIndex = index;

            activeTime = hotspotData.ActiveTimeInMillisec;
            deactiveTime = hotspotData.DeactiveTimeInMillisec;

            Vector3 hotspotPosition = camera.position + (camera.forward * hsDrawDistance);

            transform.position = hotspotPosition;

            _lookAtTransform = camera;

            _drawDistance = hsDrawDistance;

            _hotspotItemType = code;

            if (code == ItemTypeCode.PANORAMA)
            {
                transform.localScale = new Vector3(1.75f, 1.75f, 1.75f);
            }
            else
            {
                transform.localScale = new Vector3(0.25f, 0.25f, 0.25f);
            }

            Vector3 direction = hotspotPosition - camera.position;

            SetXYRotations(direction);

            SetPosition(new Vector2(hotspotData.HotspotLocalPostionX, hotspotData.HotspotLocalPostionY));

            CreateHotspotBoxes(hotspotData, isLastLoadedMedia);

            foreach (CTHotspotStatusData status in hotspotData.ListHotspotStatus)
            {
                if (status.StatusTypeCode == EHotspotStatusTypeCode.DEFAULT)
                {
                    _hotspotSprite.sprite = LoadImage(PrefabNameConstants.HOTSPOT_ICON + status.StatusIconId);
                }
            }

            if (_isInPreviewMode)
            {
                if (hotspotData.IsEnabledByDefault && (hotspotData.ActiveTimeInMillisec <= 0 || hotspotData.DeactiveTimeInMillisec <= 0))
                {
                    EnableDefaultHotspot();
                }
                else
                {
                    DisableDefaultHotspot();
                }
            }
        }

        public void EnableDefaultHotspot()
        {
            if (_isInPreviewMode)
            {
                _hotspotSprite.gameObject.SetActive(true);
            }
        }

        public void DisableDefaultHotspot()
        {
            if (_isInPreviewMode)
            {
                _hotspotSprite.gameObject.SetActive(false);
            }
        }

        public void SetFirstSelection(bool value)
        {
            _firstSelection = value;
        }

        private void CreateHotspotBoxes(CTHotspotData hotspotData, bool isLastLoadedMedia)
        {
            hotspotBoxList = new List<GameObject>();

            foreach (CTHotspotBoxData boxData in hotspotData.HotspotBoxes)
            {
                GameObject boxObject = Instantiate(Resources.Load(PrefabNameConstants.HOTSPOT_BOX) as GameObject, this.transform);
                ViewStaticHotspotBox hotspotBoxScript = boxObject.GetComponent<ViewStaticHotspotBox>();

                hotspotBoxScript.SetHotspotBoxData(boxData, _lookAtTransform.position, hotspotData.HotspotId);

                if(isLastLoadedMedia)
                {
                    boxObject.SetActive(true);
                }
                else
                {
                    boxObject.SetActive(false);
                }
                
                hotspotBoxList.Add(boxObject);

                hotspotSprites.AddRange(boxObject.GetComponentsInChildren<SpriteRenderer>());
                hotspotTexts.AddRange(boxObject.GetComponentsInChildren<TMP_Text>());
            }
        }

        public void ShowHideHotspotBoxes(bool flag)
        {
            if(!flag)
            {
                if (!_isInPreviewMode)
                {
                    foreach (var hotspotBox in hotspotBoxList)
                    {
                        hotspotBox.SetActive(true);
                    }
                }
        
                return;
            }
        
            foreach (var hotspotBox in hotspotBoxList)
            {
                if (null != hotspotBox)
                {
                    if(_firstSelection)
                    {
                        _firstSelection = false;
        
                        return;
                    }
        
                    if (_isInPreviewMode)
                    {
                        if (flag)
                        {
                            if (!hotspotBox.activeSelf)
                            {
                                hotspotBox.SetActive(true);
                            }
                            else
                            {
                                hotspotBox.SetActive(false);
                            }
                        }
                        else
                        {
                            if (!hotspotBox.activeSelf)
                            {
                                hotspotBox.SetActive(true);
                            }
                            else
                            {
                                hotspotBox.SetActive(false);
                            }
                        }
                    }
                    else
                    {
                        hotspotBox.SetActive(true);
                    }
                }
            }
        }

        ///// <summary>
        /// Old Code
        /// </summary>
        /// <param name="flag"></param>
        //public void ShowHideHotspotBoxes(bool flag)
        //{
        //    foreach(var hotspotBox in hotspotBoxList)
        //    {
        //        if (null != hotspotBox)
        //        {
        //            if(flag && _firstSelection)
        //            {
        //                _firstSelection = false;
        //
        //                if (hotspotBox.activeSelf)
        //                {
        //                    hotspotBox.SetActive(false);
        //                }
        //
        //                return;
        //            }
        //
        //            if (flag == true && hotspotBox.activeSelf == true && _isInPreviewMode)
        //            {
        //                if (hotspotBox.activeSelf)
        //                {
        //                    hotspotBox.SetActive(false);
        //                }
        //            }
        //            else
        //            {
        //                if (_isInPreviewMode)
        //                {
        //                    if (flag)
        //                    {
        //                        if (!hotspotBox.activeSelf)
        //                        {
        //                            hotspotBox.SetActive(flag);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (hotspotBox.activeSelf)
        //                        {
        //                            hotspotBox.SetActive(false);
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    hotspotBox.SetActive(flag);
        //                }
        //            }
        //        }
        //    }
        //}

        internal void HotspotDeleted(MessageData data)
        {
            string hotspotID = (string)data[(byte)EParameterCodeStatic.HOTSPOT_ID];

            if (hotspotID == _hotspotId)
            {
                int count = hotspotBoxList.Count;

                for(int i = 0; i < count; i++)
                {
                    Destroy(hotspotBoxList[0]);
                    hotspotBoxList.RemoveAt(0);
                }

                hotspotBoxList.Clear();

                Destroy(gameObject);
            }
        }

        public void UpdatePosition(MessageData data)
        {
            string hotspotID = (string)data[(byte)EParameterCodeStatic.HOTSPOT_ID];
            Vector2 xy = (Vector2)data[(byte)EParameterCodeStatic.HOTSPOT_XY_POSITION];

            if (hotspotID == _hotspotId)
            {
                _xRot += xy.x;
                _yRot += xy.y;

                Quaternion quater = Quaternion.Euler(_xRot, _yRot, _lookAtTransform.eulerAngles.z);
                Vector3 hotspotPosition = quater * (Vector3.forward * _drawDistance);
                transform.position = _lookAtTransform.position + hotspotPosition;
            }
        }

        public void SetPosition(Vector2 xyPosition)
        {
            Vector2 xy = xyPosition;

            _xRot = xy.x;
            _yRot = xy.y;

            Quaternion quater = Quaternion.Euler(_xRot, _yRot, _lookAtTransform.eulerAngles.z);

            Vector3 hotspotPosition = quater * (Vector3.forward * _drawDistance);

            transform.position = _lookAtTransform.position + hotspotPosition;
        }

        public void HotspotSelected()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_INDEX, _hotspotItemIndex);
            data.Parameters.Add((byte)EParameterCodeToolkit.HOTSPOT_ID, HotspotID);
            data.Parameters.Add((byte)EParameterCodeToolkit.ITEM_TYPE, _hotspotItemType);

            dispatcher.Dispatch(StaticHotspotEvents.HOTSPOT_SELECTED, data);
        }

        public Vector2 GetXYPosition()
        {
            return new Vector2(_xRot, _yRot);
        }

        public void SetXYRotations(Vector3 direction)
        {
            Quaternion lookAtRotation = Quaternion.LookRotation(direction, Vector3.forward);
            _xRot = lookAtRotation.eulerAngles.x;
            _yRot = lookAtRotation.eulerAngles.y;
        }

        public void UpdateHotspotIcon(MessageData data)
        {
            string hotspotID = (string)data.Parameters[(byte)EParameterCodeToolkit.HOTSPOT_INDEX];
            string iconID = (string)data.Parameters[(byte)EParameterCodeToolkit.HOTSPOT_ICON_ID];

            if (_hotspotId == hotspotID)
            {
                _hotspotSprite.sprite = iconID == "" ? LoadImage(PrefabNameConstants.HOTSPOT_ICON + _defaultHotspotIconID) : LoadImage(PrefabNameConstants.HOTSPOT_ICON + iconID);
            }
        }

        private Sprite LoadImage(string imagePath)
        {
            if (imagePath != "")
            {
                Sprite theSprite = Resources.Load<Sprite>(imagePath);
                return theSprite;
            }

            return null;
        }

        public void SetDefaultStatusIcons(string iconID)
        {
            _defaultHotspotIconID = iconID;
        }

        public void VideoTimeReceived(float videoTime)
        {
            if (_isInPreviewMode)
            {
                currentVideoTime = videoTime;

                if (activeTime <= 0 && deactiveTime <= 0)
                {
                    return;
                }

                if (videoTime >= activeTime && videoTime <= deactiveTime)
                {
                    EnableDefaultHotspot();
                }
                else
                {
                    if ((videoTime < activeTime || videoTime > deactiveTime))
                    {
                        DisableDefaultHotspot();
                    }
                }
            }
        }

        public void FadeHotspotBox(float value)
        {
            foreach(TMP_Text text in hotspotTexts)
            {
                text.color = new Color(text.color.r, text.color.g, text.color.b, value);
            }

            foreach(SpriteRenderer rend in hotspotSprites)
            {
                rend.color = new Color(rend.color.r, rend.color.g, rend.color.b, value);
            }
        }
    }
}
