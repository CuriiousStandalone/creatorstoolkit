﻿using System.IO;

using UnityEngine;

using TMPro;

using PulseIQ.Local.Common.Model.Hotspot.HotspotElement;
using PulseIQ.Local.ClientCommon.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using strange.extensions.mediation.impl;
using UnityEngine.UI;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Static
{
    public class ViewStaticHotspotElement : EventView
    {
        private EHotspotElementTypeCode elementType;

        private int _elementIndex = -1;

        private string _hotspotID = "";

        private float _parentWidth;
        private float _parentHeight;

        [SerializeField]
        TMP_Text textObjText = null;

        [SerializeField]
        RawImage imageObjImage = null;

        [SerializeField]
        TMP_Text infoObjHeader = null;
        [SerializeField]
        TextMeshProUGUI infoObjBody = null;
        [SerializeField]
        RawImage infoObjImage = null;

        private int textCount = 0;

        private float maxImagePanelSize = 3000;

        public void SetHotspotElementData(CTHotspotElementBaseData hotspotElementData, float parentWidth, float parentHeight, int elementIndex, string hotspotID, bool isInfoPanel = false)
        {
            elementType = hotspotElementData.TypeCode;

            _hotspotID = hotspotID;

            _elementIndex = elementIndex;

            _parentWidth = parentWidth;
            _parentHeight = parentHeight;

            switch (elementType)
            {
                case EHotspotElementTypeCode.TEXT:
                    {
                        if (isInfoPanel)
                        {
                            ++textCount;

                            if (textCount == 1)
                            {
                                infoObjHeader.text = ((CTHotspotElementTextData)hotspotElementData).Text;
                            }
                            else
                            {
                                infoObjBody.text = ((CTHotspotElementTextData)hotspotElementData).Text;
                            }
                        }
                        else
                        {
                            textObjText.text = ((CTHotspotElementTextData)hotspotElementData).Text;
                            Canvas.ForceUpdateCanvases();
                        }

                        if (textCount >= 2)
                        {
                            textCount = 0;
                        }

                        break;
                    }

                case EHotspotElementTypeCode.IMAGE:
                    {
                        if (isInfoPanel)
                        {
                            if (string.IsNullOrEmpty(((CTHotspotElementImageData)hotspotElementData).MultimediaId))
                            {
                                imageObjImage.transform.parent.gameObject.SetActive(false);
                                return;
                            }

                            string imagePath = Path.Combine(Settings.ImagePath, ((CTHotspotElementImageData)hotspotElementData).MultimediaId);

                            imageObjImage.texture = LoadImage(imagePath, ((CTHotspotElementImageData)hotspotElementData).Width / 100, ((CTHotspotElementImageData)hotspotElementData).Height / 100);

                            Vector2 rectTransformVector;

                            if (imageObjImage.texture.width >= imageObjImage.texture.height)
                            {
                                float sizeMultiplier = 1800f / imageObjImage.texture.height;
                                rectTransformVector = new Vector2(imageObjImage.texture.width * sizeMultiplier, 1800f);

                            }
                            else
                            {
                                float sizeMultiplier = 1800f / imageObjImage.texture.width;

                                rectTransformVector = new Vector2(1800f, imageObjImage.texture.height * sizeMultiplier);
                            }

                            imageObjImage.GetComponent<RectTransform>().sizeDelta = rectTransformVector;

                            imageObjImage.transform.parent.gameObject.SetActive(true);
                            break;
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(((CTHotspotElementImageData)hotspotElementData).MultimediaId))
                            {
                                imageObjImage.transform.parent.gameObject.SetActive(false);
                                return;
                            }

                            string imagePath = Path.Combine(Settings.ImagePath, ((CTHotspotElementImageData)hotspotElementData).MultimediaId);

                            imageObjImage.texture = LoadImage(imagePath, ((CTHotspotElementImageData)hotspotElementData).Width / 100, ((CTHotspotElementImageData)hotspotElementData).Height / 100);

                            Vector2 rectTransformVector;

                            if (imageObjImage.texture.width >= imageObjImage.texture.height)
                            {
                                float sizeMultiplier = maxImagePanelSize / imageObjImage.texture.width;
                                rectTransformVector = new Vector2(maxImagePanelSize, imageObjImage.texture.height * sizeMultiplier);

                            }
                            else
                            {
                                float sizeMultiplier = maxImagePanelSize / imageObjImage.texture.height;
                                rectTransformVector = new Vector2(imageObjImage.texture.width * sizeMultiplier, maxImagePanelSize);
                            }

                            imageObjImage.GetComponent<RectTransform>().sizeDelta = rectTransformVector;
                            imageObjImage.transform.parent.GetComponent<RectTransform>().sizeDelta = rectTransformVector;

                            imageObjImage.transform.parent.gameObject.SetActive(true);
                            break;
                        }
                    }

                case EHotspotElementTypeCode.AUDIO:
                    {
                        if (string.IsNullOrEmpty(((CTHotspotElementAudioData)hotspotElementData).MultimediaId))
                        {
                            return;
                        }

                        AudioSource audioSource = gameObject.GetComponent<AudioSource>();

                        string filePath = Path.Combine(Settings.AudioFilePath, ((CTHotspotElementAudioData)hotspotElementData).MultimediaId);

                        WWW www = new WWW(filePath);

                        audioSource.clip = www.GetAudioClip();

                        break;
                    }
            }
        }

        private Texture2D LoadImage(string imagePath, float canvasWidth, float canvasHeight)
        {
            if (imagePath != "" && File.Exists(imagePath))
            {
                Texture2D tex = null;
                byte[] fileData;

                fileData = File.ReadAllBytes(imagePath);
                tex = new Texture2D(2, 2);
                tex.LoadImage(fileData);

                return tex;

                //int originalWidth = tex.width;
                //int originalHeight = tex.height;
                //
                //float ratioX = (float)canvasWidth / (float)originalWidth;
                //float ratioY = (float)canvasHeight / (float)originalHeight;
                //
                //float ratio = ratioX < ratioY ? ratioX : ratioY;
                //
                //int newHeight = System.Convert.ToInt32(originalHeight * ratio);
                //int newWidth = System.Convert.ToInt32(originalWidth * ratio);
                //
                //return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2());
            }

            return null;
        }

        public void UpdateHotspotElement(MessageData msgData)
        {
            CTHotspotElementBaseData baseData = (CTHotspotElementBaseData)msgData[(byte)EParameterCodeToolkit.HOTSPOT_ELEMENT_DATA];
            string hotspotID = (string)msgData[(byte)EParameterCodeToolkit.HOTSPOT_ID];
            int elementIndex = (int)msgData[(byte)EParameterCodeToolkit.HOTSPOT_ELEMENT_ID];
            bool isInfoPanel = false;
            EHotspotElementTypeCode type = (EHotspotElementTypeCode)msgData[(byte)EParameterCodeToolkit.ELEMENT_TYPE];

            if (msgData.Parameters.ContainsKey((byte)(EParameterCodeToolkit.IS_HOTSPOT_PANEL)))
            {
                isInfoPanel = (bool)msgData[(byte)EParameterCodeToolkit.IS_HOTSPOT_PANEL];
            }


            if (hotspotID != _hotspotID)
            {
                return;
            }

            switch (type)
            {
                case EHotspotElementTypeCode.TEXT:
                    {
                        if (isInfoPanel)
                        {
                            if (elementIndex == 0)
                            {
                                infoObjHeader.text = ((CTHotspotElementTextData)baseData).Text;
                            }
                            else if (elementIndex == 1)
                            {
                                infoObjBody.text = ((CTHotspotElementTextData)baseData).Text;
                            }
                        }
                        else
                        {
                            textObjText.text = ((CTHotspotElementTextData)baseData).Text;

                            LayoutRebuilder.ForceRebuildLayoutImmediate(this.transform.GetComponent<RectTransform>());
                        }

                        break;
                    }

                case EHotspotElementTypeCode.IMAGE:
                    {

                        if (isInfoPanel)
                        {
                            if (string.IsNullOrEmpty(((CTHotspotElementImageData)baseData).MultimediaId))
                            {
                                imageObjImage.texture = null;
                                imageObjImage.transform.parent.gameObject.SetActive(false);
                                return;
                            }

                            string imagePath = Path.Combine(Settings.ImagePath, ((CTHotspotElementImageData)baseData).MultimediaId);

                            imageObjImage.texture = LoadImage(imagePath, ((CTHotspotElementImageData)baseData).Width / 100, ((CTHotspotElementImageData)baseData).Height / 100);

                            Vector2 rectTransformVector;

                            if (imageObjImage.texture.width >= imageObjImage.texture.height)
                            {
                                float sizeMultiplier = 1800f / imageObjImage.texture.height;
                                rectTransformVector = new Vector2(imageObjImage.texture.width * sizeMultiplier, 1800f);

                            }
                            else
                            {
                                float sizeMultiplier = 1800f / imageObjImage.texture.width;

                                rectTransformVector = new Vector2(1800f, imageObjImage.texture.height * sizeMultiplier);
                            }

                            imageObjImage.GetComponent<RectTransform>().sizeDelta = rectTransformVector;

                            imageObjImage.transform.parent.gameObject.SetActive(true);
                            break;
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(((CTHotspotElementImageData)baseData).MultimediaId))
                            {
                                imageObjImage.texture = null;
                                imageObjImage.transform.parent.gameObject.SetActive(false);
                                return;
                            }

                            string imagePath = Path.Combine(Settings.ImagePath, ((CTHotspotElementImageData)baseData).MultimediaId);

                            imageObjImage.texture = LoadImage(imagePath, ((CTHotspotElementImageData)baseData).Width / 100, ((CTHotspotElementImageData)baseData).Height / 100);

                            Vector2 rectTransformVector;

                            if (imageObjImage.texture.width >= imageObjImage.texture.height)
                            {
                                float sizeMultiplier = maxImagePanelSize / imageObjImage.texture.width;
                                rectTransformVector = new Vector2(maxImagePanelSize, imageObjImage.texture.height * sizeMultiplier);

                            }
                            else
                            {
                                float sizeMultiplier = maxImagePanelSize / imageObjImage.texture.height;
                                rectTransformVector = new Vector2(imageObjImage.texture.width * sizeMultiplier, maxImagePanelSize);
                            }

                            imageObjImage.GetComponent<RectTransform>().sizeDelta = rectTransformVector;
                            imageObjImage.transform.parent.GetComponent<RectTransform>().sizeDelta = rectTransformVector;

                            imageObjImage.transform.parent.gameObject.SetActive(true);

                            break;
                        }
                    }

                case EHotspotElementTypeCode.AUDIO:
                    {
                        if (string.IsNullOrEmpty(((CTHotspotElementAudioData)baseData).MultimediaId))
                        {
                            return;
                        }

                        AudioSource audioSource = gameObject.GetComponent<AudioSource>();

                        string filePath = Path.Combine(Settings.AudioFilePath, ((CTHotspotElementAudioData)baseData).MultimediaId);

                        WWW www = new WWW(filePath);

                        audioSource.clip = www.GetAudioClip();

                        break;
                    }
            }
        }
    }
}