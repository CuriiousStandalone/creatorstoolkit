﻿using CuriiousIQ.Local.CreatorsToolkit.World.Inspector;
using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Static
{
    public class CommandMediaVideoLoaded : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EInspectorEvent.MEDIA_VIDEO_LOADED, evt.data);
        }
    }
}