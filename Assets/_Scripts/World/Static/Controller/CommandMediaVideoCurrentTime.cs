﻿using strange.extensions.command.impl;

using CuriiousIQ.Local.CreatorsToolkit.World.Inspector;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Static
{
    public class CommandMediaVideoCurrentTime : EventCommand
    {
        [Inject]
        public IModelActiveWorld WorldModel { get; set; }

        public override void Execute()
        {
            dispatcher.Dispatch(EInspectorEvent.MEDIA_VIDEO_CURRENT_TIME, evt.data);

            if (WorldModel.IsPreviewModeOn)
            {
                MessageData receivedMsgData = (MessageData)evt.data;
                float mediaTime = (float)receivedMsgData[(byte)EParameterCodeStatic.VIDEO_CURRENT_TIME];

                MessageData responseMsgData = new MessageData();
                responseMsgData.Parameters.Add((byte)EParameterCodeWorld.MEDIA_CURRENT_TIME, mediaTime);

                dispatcher.Dispatch(EWorldEvent.MEDIA_VIDEO_CURRENT_TIME, responseMsgData);
            }
        }
    }
}