﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Static
{
    public class CommandUpdateHotspotPosition : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EStaticEvent.UPDATE_HOTSPOT_POSITION, evt.data);
        }
    }
}
