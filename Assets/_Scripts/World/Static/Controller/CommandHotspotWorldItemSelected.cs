﻿using System.Linq;
using System.Collections.Generic;

using strange.extensions.command.impl;

using PulseIQ.Local.Common;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.Common.Model.Panorama;
using PulseIQ.Local.Common.Model.PanoramaVideo;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;
using PulseIQ.Local.ClientCommon.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.World.WorldHierarchy;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Static
{
    public class CommandHotspotWorldItemSelected : EventCommand
    {
        [Inject]
        public IModelActiveWorld WorldModel { get; set; }

        public override void Execute()
        {
            MessageData msgData = (MessageData)evt.data;

            int index = (int)msgData[(byte)EParameterCodeToolkit.ITEM_INDEX];

            string hotspotId = (string)msgData[(byte)EParameterCodeToolkit.HOTSPOT_ID];

            msgData.Parameters.Add((byte)EParameterCodeToolkit.ITEM_ID, WorldModel.ActiveWorldData.ListItems.FirstOrDefault(x => x.ItemIndex == index).ItemName);

            if (WorldModel.IsPreviewModeOn)
            {
                foreach(var item in WorldModel.ActiveWorldData.ListItems)
                {
                    if(item.ItemIndex == index && IsMedia(item.ItemType))
                    {
                        List<CTHotspotData> hotspots;

                        switch(item.ItemType)
                        {
                            case ItemTypeCode.PANORAMA:
                                {
                                    hotspots = new List<CTHotspotData>(((CTPanoramaData)item).ListHotspot);
                                    break;
                                }

                            case ItemTypeCode.PANORAMA_VIDEO:
                                {
                                    hotspots = new List<CTHotspotData>(((CTPanoramaVideoData)item).ListHotspot);
                                    break;
                                }

                            case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                                {
                                    hotspots = new List<CTHotspotData>(((CTStereoPanoramaVideoData)item).ListHotspot);
                                    break;
                                }

                            default:
                                {
                                    hotspots = new List<CTHotspotData>();
                                    break;
                                }
                        }

                        if(null != hotspots && hotspots.Count > 0)
                        {
                            if (hotspots.Any(x => x.HotspotId == hotspotId))
                            {
                                CTHotspotData hotspotData = hotspots.First(x => x.HotspotId == hotspotId);

                                if (null != hotspotData && hotspotData.HotspotTriggerEventCode == EHotspotTriggerEventCode.TELEPORT && hotspotData.TeleportMultimediaItemIndex > 0)
                                {
                                    MessageData mData = new MessageData();
                                    mData.Parameters.Add((byte)EParameterCodeWorld.ITEM_INDEX, hotspotData.TeleportMultimediaItemIndex);

                                    dispatcher.Dispatch(EWorldEvent.LOAD_MEDIA_WITH_ITEM_INDEX, mData);
                                }
                                else
                                {
                                    dispatcher.Dispatch(EWorldHierarchyEvent.HOTSPOT_WORLD_ITEM_SELECTED, evt.data);
                                }
                            }
                        }

                        break;
                    }
                }
            }
            else
            {
                dispatcher.Dispatch(EWorldHierarchyEvent.HOTSPOT_WORLD_ITEM_SELECTED, evt.data);
            }
        }

        private bool IsMedia(ItemTypeCode itemType)
        {
            if(itemType == ItemTypeCode.PANORAMA ||
                itemType == ItemTypeCode.PANORAMA_VIDEO ||
                itemType == ItemTypeCode.STEREO_PANORAMA_VIDEO)
            {
                return true;
            }

            return false;
        }
    }
}
