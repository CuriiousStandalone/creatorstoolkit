﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Static
{
    public class CommandPanoramaVideoEnded : EventCommand
    {
        [Inject]
        public IModelActiveWorld WorldModel { get; set; }

        public override void Execute()
        {
            if(WorldModel.IsPreviewModeOn)
            {
                dispatcher.Dispatch(EWorldEvent.PANORAMA_VIDEO_ENDED);
            }
        }
    }
}
