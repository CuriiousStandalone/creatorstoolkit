﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Static
{
    public class CommandPanoramaImageTeleportTimeTiggered : EventCommand
    {
        [Inject]
        public IModelActiveWorld WorldModel { get; set; }

        public override void Execute()
        {
            if (WorldModel.IsPreviewModeOn)
            {
                dispatcher.Dispatch(EWorldEvent.PANORAMA_IMAGE_TELEPORT_TIME_TRIGGERED);
            }
        }
    }
}
