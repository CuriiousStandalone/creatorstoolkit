﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World.Static
{
    public class CommandStereoPanoramaVideoEnded : EventCommand
    {
        [Inject]
        public IModelActiveWorld WorldModel { get; set; }

        public override void Execute()
        {
            if (WorldModel.IsPreviewModeOn)
            {
                dispatcher.Dispatch(EWorldEvent.STEREO_PANORAMA_VIDEO_ENDED);
            }
        }
    }
}
