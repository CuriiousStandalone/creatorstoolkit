﻿using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.World;
using System.Collections.Generic;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class ModelActiveWorld : IModelActiveWorld
    {
        private WorldData _activeWorldData;

        private List<CTItemData> _mediaItemsInWorld;

        private bool _isPreviewModeOn = false;

        private string _thumbnailPath = "";

        public WorldData ActiveWorldData
        {
            get
            {
                return _activeWorldData;
            }
            set
            {
                _activeWorldData = value;
            }
        }

        public List<CTItemData> MediaItemsInCurrentWorld
        {
            get
            {
                if(_mediaItemsInWorld == null)
                {
                    _mediaItemsInWorld = new List<CTItemData>();
                }

                return _mediaItemsInWorld;
            }

            set
            {
                _mediaItemsInWorld = value;
            }
        }

        public string ThumbnailPath
        {
            get
            {
                return _thumbnailPath;
            }

            set
            {
                _thumbnailPath = value;
            }
        }

        public bool IsPreviewModeOn
        {
            get
            {
                return _isPreviewModeOn;
            }

            set
            {
                _isPreviewModeOn = value;
            }
        }

        public void ResetWorld()
        {
            if (_activeWorldData != null && _activeWorldData.ListItems != null)
            {
                _activeWorldData.ListItems.Clear();
            }

            if (_activeWorldData != null && _activeWorldData.ListPostProcessing != null)
            {
                _activeWorldData.ListPostProcessing.Clear();
            }

            if (_activeWorldData != null && _activeWorldData.ListSkybox != null)
            {
                _activeWorldData.ListSkybox.Clear();
            }

            _activeWorldData = null;

            if(_mediaItemsInWorld != null)
            {
                _mediaItemsInWorld.Clear();
            }
        }
    }
}
