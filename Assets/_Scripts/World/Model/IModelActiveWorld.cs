﻿using System.Collections.Generic;

using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.World;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public interface IModelActiveWorld
    {
        WorldData ActiveWorldData { get; set; }

        List<CTItemData> MediaItemsInCurrentWorld { get; set; }

        string ThumbnailPath { get; set; }

        bool IsPreviewModeOn { get; set; }

        void ResetWorld();
    }
}
