﻿using System.Collections.Generic;

using PulseIQ.Local.Common.Model.World;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public interface IModuleTemplateData
    {
        List<WorldData> TemplateList { get; set; }
    }
}
