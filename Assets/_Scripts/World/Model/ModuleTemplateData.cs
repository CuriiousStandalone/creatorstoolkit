﻿using System.Collections.Generic;

using PulseIQ.Local.Common.Model.World;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class ModuleTemplateData : IModuleTemplateData
    {
        private List<WorldData> _templateList;

        public List<WorldData> TemplateList
        {
            get
            {
                if(null == _templateList)
                {
                    _templateList = new List<WorldData>();
                }

                return _templateList;
            }

            set
            {
                _templateList = value;
            }
        }
    }
}
