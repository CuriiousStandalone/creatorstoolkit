﻿using strange.extensions.context.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class RootWorld : ContextView
    {
        private void Awake()
        {
            context = new ContextWorld(this);
        }
    }
}
