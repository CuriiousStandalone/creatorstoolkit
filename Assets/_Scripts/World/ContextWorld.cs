﻿using strange.extensions.context.api;
using strange.extensions.context.impl;

using UnityEngine;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.World.Inspector;
using CuriiousIQ.Local.CreatorsToolkit.World.WorldHierarchy;
using CuriiousIQ.Local.CreatorsToolkit.World.Static;

using PulseIQ.Local.ClientCommon.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.World
{
    public class ContextWorld : MVCSContext
    {
        public ContextWorld(MonoBehaviour view) : base(view)
        {
        }

        public ContextWorld(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
        {
        }

        protected override void mapBindings()
        {
            #region Model Bindings

            injectionBinder.Bind<IModelActiveWorld>().To<ModelActiveWorld>().ToSingleton();
            injectionBinder.Bind<IModuleTemplateData>().To<ModuleTemplateData>().ToSingleton();
            injectionBinder.Bind<ILocalDirService>().To<LocalDirService>().ToSingleton();

            #endregion

            #region View-MediatorBindings

            mediationBinder.Bind<ViewInspectorManager>().To<MediatorInspectorManager>();
            mediationBinder.Bind<ViewHotspotElementManager>().To<MediatorHotspotElementManager>();
            mediationBinder.Bind<ViewInspectorModuleManager>().To<MediatorInspectorModuleManager>();
            mediationBinder.Bind<ViewPanoramaManager>().To<MediatorPanoramaManager>();
            mediationBinder.Bind<ViewMonoVideoManager>().To<MediatorMonoVideoManager>();
            mediationBinder.Bind<ViewStereoVideoManager>().To<MediatorStereoVideoManager>();
            mediationBinder.Bind<ViewHotspotManager>().To<MediatorHotspotManager>();
            mediationBinder.Bind<ViewHotspotElementTemplate>().To<MediatorHotspotElementTemplate>();
            mediationBinder.Bind<ViewCameraRaycaster>().To<MediatorCameraRaycaster>();

            mediationBinder.Bind<ViewWorldHierarchyManager>().To<MediatorWorldHierarchyManager>();

            mediationBinder.Bind<ViewStaticItem>().To<MediatorStaticItem>();
            mediationBinder.Bind<ViewStaticHotspot>().To<MediatorStaticHotspot>();

            mediationBinder.Bind<ViewPanoramaImageEdit>().To<MediatorPanoramaImageEdit>();
            mediationBinder.Bind<ViewVideoPanoramaEdit>().To<MediatorVideoPanoramaEdit>();
            mediationBinder.Bind<ViewStereoPanoramaEdit>().To<MediatorStereoPanoramaEdit>();

            mediationBinder.Bind<ViewHotspotAudioElement>().To<MediatorHotspotAudioElement>();
            mediationBinder.Bind<ViewHotspotImageElement>().To<MediatorHotspotImageElement>();
            mediationBinder.Bind<ViewHotspotPanelElement>().To<MediatorHotspotPanelElement>();
            mediationBinder.Bind<ViewHotspotTextElement>().To<MediatorHotspotTextElement>();
            mediationBinder.Bind<ViewStaticHotspotElement>().To<MediatorStaticHotspotElement>();

            mediationBinder.Bind<ViewWorldManager>().To<MediatorWorldManager>();

            mediationBinder.Bind<ViewSaveModuleAsDialog>().To<MediatorSaveModuleAsDialog>();
            mediationBinder.Bind<ViewPublishModuleDialog>().To<MediatorPublishModuleDialog>();

            mediationBinder.Bind<ViewWorldTooltipManager>().To<MediatorWorldTooltipManager>();

            #endregion

            #region Command Binding

            commandBinder.Bind(EEventToolkitCrossContext.HIDE_PANEL).To<CommandHideInspectorPanel>().To<CommandHideWorldHierarchyPanel>();
            commandBinder.Bind(EEventToolkitCrossContext.SHOW_PANEL).To<CommandShowInspectorPanel>().To<CommandShowWorldHierarchyPanel>();
            commandBinder.Bind(EEventToolkitCrossContext.PANEL_INCREASE_HEIGHT).To<CommandIncreaseHeightInspectorPanel>().To<CommandIncreaseHeightWorldHierarchyPanel>();
            commandBinder.Bind(EEventToolkitCrossContext.PANEL_DECREASE_HEIGHT).To<CommandDecreaseHeightInspectorPanel>().To<CommandDecreaseHeightWorldHierarchyPanel>();
            commandBinder.Bind(EEventToolkitCrossContext.PANEL_REPOSITION_TO_RIGHT).To<CommandRepositionInspectorPanelToRight>().To<CommandRepositionWorldHierarchyPanelToRight>();
            commandBinder.Bind(EEventToolkitCrossContext.PANEL_REPOSITION_TO_LEFT).To<CommandRepositionInspectorPanelToLeft>().To<CommandRepositionWorldHierarchyPanelToLeft>();
            commandBinder.Bind(EEventToolkitCrossContext.PANEL_REPOSITION_TO_TOP).To<CommandRepositionInspectorPanelToTop>().To<CommandRepositionWorldHierarchyPanelToTop>();
            commandBinder.Bind(EEventToolkitCrossContext.PANEL_REPOSITION_TO_BOTTOM).To<CommandRepositionInspectorPanelToBottom>().To<CommandRepositionWorldHierarchyPanelToBottom>();
            //Attach this command if you want to open Module from Module Panel
            //commandBinder.Bind(EEventToolkitCrossContext.OPEN_WORLD_REQUESTED).To<CommandOpenWorld>();
            commandBinder.Bind(EEventToolkitCrossContext.OPEN_WORLD_REQUESTED).To<CommandOpenWorldByPath>();
            commandBinder.Bind(EEventToolkitCrossContext.CREATE_NEW_WORLD_REQUESTED).To<CommandCreateNewWorldRequested>();
            commandBinder.Bind(EEventToolkitCrossContext.DELETE_ITEM_REQUESTED).To<CommandDeleteItemFromWorld>();
            commandBinder.Bind(EEventToolkitCrossContext.SHOW_HOTSPOT_OPTIONS).To<CommandShowHotspotOptions>();
            commandBinder.Bind(EEventToolkitCrossContext.RESET_CAMERA).To<CommandResetCameraRequested>();
            commandBinder.Bind(EEventToolkitCrossContext.RESOURCE_DRAG_DROP_FAILED).To<CommandResourceDroppedInWorld>();

            commandBinder.Bind(EEventToolkitCrossContext.SAVE_MODULE_REQUESTED).To<CommandSaveCurrentModuleRequested>();
            commandBinder.Bind(EEventToolkitCrossContext.SAVE_MODULE_AS_REQUESTED).To<CommandSaveCurrentModuleAsRequested>();
            commandBinder.Bind(EEventToolkitCrossContext.PUBLISH_MODULE_REQUESTED).To<CommandPublishModuleRequested>();

            commandBinder.Bind(EEventToolkitCrossContext.PREVIEW_MODE_ENABLED).To<CommandPreviewModeEnabled>();
            commandBinder.Bind(EEventToolkitCrossContext.PREVIEW_MODE_DISABLED).To<CommandPreviewModeDisabled>();

            commandBinder.Bind(EEventToolkitCrossContext.TOOLTIP_TEXT_RESPONSE).To<CommandTooltipResponse>();
            commandBinder.Bind(EEventToolkitCrossContext.PACKAGE_MODULE).To<CommandPackageModule>();
            commandBinder.Bind(EEventToolkitCrossContext.IMPORT_PACKAGE).To<CommandImportContentPack>();

            commandBinder.Bind(EWorldHierarchyEvent.RESOURCE_ITEM_DROPPED_INTO_HIERARCHY).To<CommandResourceDroppedInWorld>();
            commandBinder.Bind(EWorldHierarchyEvent.MODULE_SELECTED).To<CommandModuleSelectedWorldHierarchy>();


            commandBinder.Bind(EWorldEvent.CREATE_NEW_WORLD).To<CommandCreateNewWorld>();
            commandBinder.Bind(EWorldEvent.ITEM_DELETED).To<CommandItemDeleted>();
            commandBinder.Bind(EWorldEvent.ITEM_SELECTED).To<CommandItemSelected>();
            commandBinder.Bind(EWorldEvent.ITEM_CREATED).To<CommandItemCreated>();
            commandBinder.Bind(EWorldEvent.HOTSPOT_CREATED).To<CommandHotspotCreated>();
            commandBinder.Bind(EWorldEvent.HOTSPOT_DELETED).To<CommandHotspotDeleted>();
            commandBinder.Bind(EWorldEvent.UPDATE_HOTSPOT_POSITION).To<CommandSetHotspotXY>();
            commandBinder.Bind(EWorldEvent.MODULE_THUMBNAIL_UPDATED).To<CommandModuleThumbnailUpdated>(); 
            commandBinder.Bind(EInspectorEvent.MODULE_THUMBNAIL_CHANGED).To<CommandModuleThumbnailUpdated>();
            commandBinder.Bind(EInspectorEvent.MODULE_THUMBNAIL_CLEARED).To<CommandModuleThumbnailUpdated>();

            commandBinder.Bind(EWorldEvent.SAVE_MODULE_AS).To<CommandSaveModuleAs>();
            commandBinder.Bind(EWorldEvent.PUBLISH_MODULE).To<CommandPublishModule>();
            commandBinder.Bind(EWorldEvent.PUBLISH_UPDATED_MODULE).To<CommandPublishUpdatedModule>();
            commandBinder.Bind(EWorldEvent.TOOLTIP_TEXT_REQUESTED).To<CommandTooltipRequest>();

            commandBinder.Bind(EInspectorEvent.LOAD_HOTSPOT_TEMPLATE_TYPES).To<CommandLoadHotspotTemplateTypes>();
            commandBinder.Bind(EInspectorEvent.GET_TEMPLATE_DATA).To<CommandGetTemplateData>();
            commandBinder.Bind(EInspectorEvent.HOTSPOT_ACTIVATE_TIME_CHANGED).To<CommandHotspotActiveTimeChanged>();
            commandBinder.Bind(EInspectorEvent.HOTSPOT_DEACTIVATE_TIME_CHANGED).To<CommandHotspotDeactiveTimeChanged>();
            commandBinder.Bind(EInspectorEvent.HOTSPOT_DELETE_PRESSED).To<CommandHotspotDeleted>();
            commandBinder.Bind(EInspectorEvent.HOTSPOT_IS_ACTIVE_CHANGED).To<CommandHotspotIsActiveChanged>();
            commandBinder.Bind(EInspectorEvent.HOTSPOT_NAME_CHANGED).To<CommandHotspotNameChanged>();
            commandBinder.Bind(EInspectorEvent.HOTSPOT_TRIGGER_EVENT_CHANGED).To<CommandHotspotTriggerEventChanged>();
            commandBinder.Bind(EInspectorEvent.HOTSPOT_TRIGGER_STATE_CHANGED).To<CommandHotspotTriggerStateChanged>();
            commandBinder.Bind(EInspectorEvent.HOTSPOT_ELEMENT_TEMPLATE_SELECTED).To<CommandCreateNewHotspot>();
            commandBinder.Bind(EInspectorEvent.HOTSPOT_STATUS_ICON_CHANGED).To<CommandHotspotStatusChanged>();
            commandBinder.Bind(EInspectorEvent.HOTSPOT_IMAGE_ELEMENT_UPDATED).To<CommandImageElementChanged>();
            commandBinder.Bind(EInspectorEvent.HOTSPOT_TEXT_ELEMENT_UPDATED).To<CommandTextElementChanged>();
            commandBinder.Bind(EInspectorEvent.HOTSPOT_AUDIO_ELEMENT_UPDATED).To<CommandAudioElementChanged>();
            commandBinder.Bind(EInspectorEvent.HOTSPOT_VIDEO_ELEMENT_UPDATED).To<CommandVideoElementChanged>();
            commandBinder.Bind(EInspectorEvent.HOTSPOT_TELEPORT_ITEM_CHANGED).To<CommandHotspotTeleportItemChanged>();

            commandBinder.Bind(EInspectorEvent.ITEM_NAME_CHANGED).To<CommandItemNameChanged>();
            commandBinder.Bind(EInspectorEvent.MEDIA_END_BEHAVIOUR_CHANGED).To<CommandEndBehaviourChanged>();
            commandBinder.Bind(EInspectorEvent.MEDIA_PAUSE_PRESSED).To<CommandMediaPaused>();
            commandBinder.Bind(EInspectorEvent.MEDIA_PLAY_PRESSED).To<CommandMediaPlayed>();
            commandBinder.Bind(EInspectorEvent.MEDIA_REPLAY_PRESSED).To<CommandMediaReplayed>();
            commandBinder.Bind(EInspectorEvent.MEDIA_STOP_PRESSED).To<CommandMediaStopped>();
            commandBinder.Bind(EInspectorEvent.MEDIA_TELEPORT_ITEM_CHANGED).To<CommandTeleportItemChanged>();
            commandBinder.Bind(EInspectorEvent.PANORAMA_TELEPORT_TIME_CHANGED).To<CommandTeleportTimeChanged>();
            commandBinder.Bind(EInspectorEvent.MEDIA_VIDEO_FRAME_CHANGED).To<CommandMediaSeeked>();
            commandBinder.Bind(EInspectorEvent.PANORAMA_AUTO_TELEPORT_CHANGED).To<CommandAutoTeleportChanged>();
            commandBinder.Bind(EInspectorEvent.STEREO_VIDEO_TYPE_CHANGED).To<CommandVideoTypeChanged>();

            commandBinder.Bind(EInspectorEvent.MODULE_DESCRIPTION_UPDATED).To<CommandModuleDescriptionUpdated>();
            commandBinder.Bind(EInspectorEvent.MODULE_NAME_UPDATED).To<CommandModuleNameUpdated>();
            commandBinder.Bind(EInspectorEvent.MODULE_IS_MULTIPLAYER_UPDATED).To<CommandModuleMultiplayerChanged>();
            commandBinder.Bind(EInspectorEvent.TEMPLATE_CHANGED).To<CommandTemplateChanged>();
            commandBinder.Bind(EInspectorEvent.REQUEST_DEFAULT_ICON_IDS).To<CommandFindDefaultIconIds>();
            commandBinder.Bind(EInspectorEvent.SHOW_FILTERED_LIST).To<CommandShowFilteredList>();
            commandBinder.Bind(EInspectorEvent.CREATE_TELEPORT_ITEM).To<CreateNewTeleportItem>();

            commandBinder.Bind(EStaticEvent.MEDIA_VIDEO_LOADED).To<CommandMediaVideoLoaded>();
            commandBinder.Bind(EStaticEvent.MEDIA_VIDEO_CURRENT_TIME).To<CommandMediaVideoCurrentTime>();
            commandBinder.Bind(EStaticEvent.HOTSPOT_ITEM_SELECTED).To<CommandHotspotWorldItemSelected>();
            commandBinder.Bind(EStaticEvent.UPDATE_HOTSPOT_POSITION).To<CommandUpdateHotspotPosition>();
            commandBinder.Bind(EStaticEvent.PANORAMA_IMAGE_TELEPORT_TIME_TRIGGERED).To<CommandPanoramaImageTeleportTimeTiggered>();
            commandBinder.Bind(EStaticEvent.PANORAMA_VIDEO_ENDED).To<CommandPanoramaVideoEnded>();
            commandBinder.Bind(EStaticEvent.STEREO_PANORAMA_VIDEO_ENDED).To<CommandStereoPanoramaVideoEnded>();
            #endregion
        }
    }
}