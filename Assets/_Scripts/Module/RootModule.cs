﻿using strange.extensions.context.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Module
{
    public class RootModule : ContextView
    {
        private void Awake()
        {
            context = new ContextModule(this);
        }
    }
}
