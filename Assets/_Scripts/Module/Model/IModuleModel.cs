﻿using System.Collections.Generic;

using UnityEngine;

namespace CuriiousIQ.Local.CreatorsToolkit.Module
{
    public interface IModuleModel
    {
        List<SModuleData> ListModuleData { get; set; }

        Dictionary<string, Sprite> ModuleThumbnailDictionary { get; set; }
    }
}
