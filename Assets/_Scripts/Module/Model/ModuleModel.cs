﻿using System.Collections.Generic;
using System.Linq;

using UnityEngine;

namespace CuriiousIQ.Local.CreatorsToolkit.Module
{
    public class ModuleModel : IModuleModel
    {
        private List<SModuleData> _listModuleData;
        private Dictionary<string, Sprite> _moduleThumbnailDictionary;

        public List<SModuleData> ListModuleData
        {
            get
            {
                if(_listModuleData == null)
                {
                    _listModuleData = new List<SModuleData>();
                }

                return _listModuleData;
            }

            set
            {
                _listModuleData = value;
            }
        }

        public Dictionary<string, Sprite> ModuleThumbnailDictionary
        {
            get
            {
                if(_moduleThumbnailDictionary == null)
                {
                    _moduleThumbnailDictionary = new Dictionary<string, Sprite>();
                }

                return _moduleThumbnailDictionary;
            }

            set
            {
                _moduleThumbnailDictionary = value;
            }
        }
    }
}
