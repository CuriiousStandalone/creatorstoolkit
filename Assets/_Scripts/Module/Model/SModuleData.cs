﻿namespace CuriiousIQ.Local.CreatorsToolkit.Module
{
    public struct SModuleData
    {
        public string ModuleId;
        public string ModuleName;
        public string ModuleDescription;
        public string DateTimeModified;
        public string ThumbnailPath;

        public int MaxPlayers;

        public bool IsLocal;
    }
}
