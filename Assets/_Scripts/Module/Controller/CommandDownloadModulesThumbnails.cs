﻿using System;
using System.IO;
using System.Collections.Generic;

using UnityEngine;

using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.command.impl;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;
using PulseIQ.Local.Service;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Module
{
    public class CommandDownloadModulesThumbnails : EventCommand
    {
        [Inject]
        public IModuleModel ModuleModel { get; set; }

        [Inject]
        public ILocalFileSystemComService FileSystemService { get; set; }

        private List<string> _thumbnailIdsToDownload;

        private ContentAccess _content;

        public override void Execute()
        {
            Retain();

            FileSystemService.dispatcher.AddListener(EModuleEvents.GET_ALL_LOCAL_THUMBNAILS_DATA_COMPLETED, OnLocalThumbnailsDataLoaded);

            FileSystemService.GetAllLocalModulesThumbnailData();
        }

        private void OnLocalThumbnailsDataLoaded(IEvent evt)
        {
            FileSystemService.dispatcher.RemoveListener(EModuleEvents.GET_ALL_LOCAL_THUMBNAILS_DATA_COMPLETED, OnLocalThumbnailsDataLoaded);

            MessageData msgData = (MessageData)evt.data;

            List<string> listLocalFiles = (List<string>)msgData[(byte)EParameterCodeModule.LOCAL_MODULES_THUMBNAIL_LIST];

            List<string> remoteThumbnailIds = new List<string>();

            //_content = new ContentAccess(GlobalSettings.CMSURL);

            //remoteThumbnailIds = _content.World.GetAllThumbnailIds();

            _thumbnailIdsToDownload = new List<string>();

            foreach (var id in remoteThumbnailIds)
            {
                if (!listLocalFiles.Contains(id))
                {
                    _thumbnailIdsToDownload.Add(id);
                }
            }

            if (_thumbnailIdsToDownload.Count > 0)
            {
                DownloadThumbnails();
            }
            else
            {
                LoadSpritesToModel();
            }
        }

        private async void DownloadThumbnails()
        {
            foreach(var id in _thumbnailIdsToDownload)
            {
                await _content.World.DownloadThumbnailAsync(id, Path.Combine(Settings.WorldThumbnailPath, id + ".png"));
            }

            LoadSpritesToModel();
        }

        private void LoadSpritesToModel()
        {
            FileSystemService.dispatcher.AddListener(EModuleEvents.GET_ALL_LOCAL_THUMBNAILS_DATA_COMPLETED, OnFinalLocalThumbnailsDataLoaded);

            FileSystemService.GetAllLocalModulesThumbnailData();
        }

        private void OnFinalLocalThumbnailsDataLoaded(IEvent evt)
        {
            FileSystemService.dispatcher.RemoveListener(EModuleEvents.GET_ALL_LOCAL_THUMBNAILS_DATA_COMPLETED, OnFinalLocalThumbnailsDataLoaded);

            MessageData msgData = (MessageData)evt.data;

            List<string> listLocalThumbIDs = (List<string>)msgData[(byte)EParameterCodeModule.LOCAL_MODULES_THUMBNAIL_LIST];

            Dictionary<string, Sprite> localDictionary = new Dictionary<string, Sprite>(ModuleModel.ModuleThumbnailDictionary);

            foreach(var id in listLocalThumbIDs)
            {
                if (!localDictionary.ContainsKey(id))
                {
                    localDictionary.Add(id, LoadSprite(id));
                }
            }

            ModuleModel.ModuleThumbnailDictionary = localDictionary;

            MessageData message = new MessageData();
            message.Parameters.Add((byte)EParameterCodeModule.THUMBNAIL_DICTIONARY, ModuleModel.ModuleThumbnailDictionary);

            dispatcher.Dispatch(EModuleEvents.ALL_THUMBNAILS_DOWNLOADED, message);
        }

        private Sprite LoadSprite(string moduleId)
        {
            string imagePath = Path.Combine(Settings.WorldThumbnailPath, moduleId + ".png");

            if (!File.Exists(imagePath))
            {
                return null;
            }

            byte[] fileData = File.ReadAllBytes(imagePath);

            Texture2D tex = new Texture2D(2, 2);

            tex.LoadImage(fileData);

            return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2());
        }
    }
}
