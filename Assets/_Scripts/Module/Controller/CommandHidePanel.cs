﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Module
{
    public class CommandHidePanel : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EModuleEvents.HIDE_PANEL, evt.data);
        }
    }
}
