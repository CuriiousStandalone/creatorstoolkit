﻿using System.Collections.Generic;
using System.Linq;
using System.IO;

using strange.extensions.command.impl;

using PulseIQ.Local.Common.Model.World;
using PulseIQ.Local.Service;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Module
{
    public class CommandGetRemoteModulesData : EventCommand
    {
        [Inject]
        public IModuleModel ModuleModel { get; set; }

        public override void Execute()
        {
            ContentAccess content = new ContentAccess(GlobalSettings.CMSURL);

            List<WorldInfoData> worldInfoDataList = content.World.GetAll();

            List<SModuleData> modulesList = new List<SModuleData>(ModuleModel.ListModuleData);

            foreach (var worldInfo in worldInfoDataList)
            {
                SModuleData moduleData;

                if(!ModuleModel.ListModuleData.Any(x => x.ModuleId == worldInfo.WorldId))
                {
                    moduleData = new SModuleData
                    {
                        ModuleId = worldInfo.WorldId,
                        ModuleName = worldInfo.WorldName,
                        IsLocal = false,
                        ThumbnailPath = Path.Combine(Settings.WorldThumbnailPath, "Module.png"),
                    };

                    modulesList.Add(moduleData);
                }
            }

            ModuleModel.ListModuleData = modulesList;

            dispatcher.Dispatch(EModuleEvents.REMOTE_MODULE_DATA_FETCHED);
        }
    }
}
