﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Module
{
    public class CommandTooltipResponse : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EModuleEvents.TOOLTIP_TEXT_RESPONSE, evt.data);
        }
    }
}
