﻿using PulseIQ.Local.ClientCommon.Components.Common;
using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Module
{
    public class CommandLoadAllModules : EventCommand
    {
        [Inject]
        public IModuleModel ModuleModel { get; set; }

        public override void Execute()
        {
            Retain();

            UpdateListeners(true);

            dispatcher.Dispatch(EModuleEvents.LOAD_LOCAL_MODULE_DATA);
        }

        private void UpdateListeners(bool value)
        {
            dispatcher.UpdateListener(value, EModuleEvents.LOCAL_MODULE_DATA_LOADED, OnLocalModuleDataLoaded);
            dispatcher.UpdateListener(value, EModuleEvents.REMOTE_MODULE_DATA_FETCHED, OnRemoteModuleDataLoaded);
        }

        private void OnLocalModuleDataLoaded()
        {
            // Uncomment this event to get worlds on CMS
            //dispatcher.Dispatch(EModuleEvents.FETCH_REMOTE_MODULE_DATA);

            OnRemoteModuleDataLoaded();
        }
        
        private void OnRemoteModuleDataLoaded()
        {
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeModule.ALL_MODULES_LIST, ModuleModel.ListModuleData);

            dispatcher.Dispatch(EModuleEvents.ALL_MODULES_LOADED, msgData);

            UpdateListeners(false);

            Release();
        }
    }
}
