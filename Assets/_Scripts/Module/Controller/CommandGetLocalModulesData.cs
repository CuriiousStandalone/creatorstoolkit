﻿using System.Linq;
using System.Collections.Generic;

using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.api;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.World;
using System;
using System.IO;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Module
{
    public class CommandGetLocalModulesData : EventCommand
    {
        [Inject]
        public IModuleModel ModuleModel { get; set; }

        [Inject]
        public ILocalFileSystemComService LocalFileService { get; set; }

        private List<SModuleData> modulesList;

        public override void Execute()
        {
            Retain();

            UpdateListeners(true);

            modulesList = new List<SModuleData>(ModuleModel.ListModuleData);

            LocalFileService.GetAllLocalModulesData();
        }

        private void UpdateListeners(bool value)
        {
            LocalFileService.dispatcher.UpdateListener(value, EModuleEvents.LOADING_LOCAL_MODULE_DATA_COMPLETE, OnLocalModulesLoaded);
        }

        private void OnLocalModulesLoaded(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            List<string> files = (List<string>)msgData[(byte)EParameterCodeModule.LOCAL_MODULES_LIST];

            foreach(var file in files)
            {
                WorldDataSerializer.ReadDataByFullPath(file, out WorldDataXML worldDataXML);
                WorldData worldData = new WorldData(worldDataXML);
                DateTime dateTime = File.GetLastWriteTime(file);

                if(modulesList.Any(x => x.ModuleId == worldData.WorldId))
                {
                    continue;
                }

                SModuleData moduleData = new SModuleData
                {
                    IsLocal = true,
                    ModuleId = worldData.WorldId,
                    ModuleName = worldData.WorldName,
                    ModuleDescription = worldData.WorldDescription,
                    DateTimeModified = dateTime.ToString("dd MMMM yyyy hh:mm"),
                    ThumbnailPath = Path.Combine(Settings.WorldThumbnailPath, "Module.png"),
                    MaxPlayers = worldData.MaxCapacity,
                };

                modulesList.Add(moduleData);
            }

            ModuleModel.ListModuleData = modulesList;

            dispatcher.Dispatch(EModuleEvents.LOCAL_MODULE_DATA_LOADED);

            UpdateListeners(false);

            Release();
        }
    }
}
