﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Module
{
    public class CommandOpenModuleRequested : EventCommand
    {
        public override void Execute()
        {
            MessageData msgData = (MessageData)evt.data;

            string moduleId = (string)msgData[(byte)EParameterCodeModule.MODULE_ID];

            MessageData resultData = new MessageData();

            resultData.Parameters.Add((byte)EParameterCodeToolkit.WORLD_ID, moduleId);

            dispatcher.Dispatch(EEventToolkitCrossContext.OPEN_WORLD_REQUESTED, resultData);

            HideMainMenuPanel();
        }

        private void HideMainMenuPanel()
        {
            MessageData hideMainMenuPanelData = new MessageData();
            hideMainMenuPanelData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelNameConstants.MAIN_MENU);

            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, hideMainMenuPanelData);

            MessageData hideModulePanelData = new MessageData();
            hideModulePanelData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelNameConstants.MODULES_PANEL);

            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, hideModulePanelData);
        }
    }
}
