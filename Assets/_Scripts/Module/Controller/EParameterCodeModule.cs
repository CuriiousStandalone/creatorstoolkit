﻿namespace CuriiousIQ.Local.CreatorsToolkit.Module
{
    public enum EParameterCodeModule
    {
        MODULE_ID,
        MODULE_DATA,
        LOCAL_MODULES_LIST,
        REMOTE_MODULES_LIST,
        LOCAL_MODULES_THUMBNAIL_LIST,
        ALL_MODULES_LIST,
        THUMBNAIL_DICTIONARY,
    }
}
