﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Module
{
    public class CommandShowPanel : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EModuleEvents.SHOW_PANEL, evt.data);
        }
    }
}
