﻿using System.IO;

using strange.extensions.command.impl;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;
using PulseIQ.Local.Common.Model.World;
using PulseIQ.Local.Service;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using strange.extensions.dispatcher.eventdispatcher.api;

namespace CuriiousIQ.Local.CreatorsToolkit.Module
{
    public class CommandDownloadModule //: EventCommand
    {
        //[Inject]
        //public IModuleModel ModuleModel { get; set; }

        //private SModuleData moduleData;

        //public override void Execute()
        //{
        //    Retain();

        //    UpdateListeners(true);

        //    dispatcher.Dispatch(EEventToolkitCrossContext.SHOW_DOWNLOAD_PROGRESS_WINDOW);

        //    MessageData msgData = (MessageData)evt.data;

        //    string moduleId = (string)msgData[(byte)EParameterCodeModule.MODULE_ID];

        //    ContentAccess contentAccess = new ContentAccess(GlobalSettings.CMSURL);

        //    WorldDataXML worldDataXml = contentAccess.World.GetWorldDataXml(moduleId);

        //    WorldData worldData = new WorldData(worldDataXml);

        //    WorldDataSerializer.WriteByData(worldData, Path.Combine(CreatorToolkitSettings.LocalWorldDirectory, moduleId + ".ctk"));

        //    moduleData = new SModuleData();

        //    for(int i = 0; i < ModuleModel.ListModuleData.Count; i++)
        //    {
        //        if(ModuleModel.ListModuleData[i].ModuleId == moduleId)
        //        {
        //            moduleData.ModuleId = worldData.WorldId;
        //            moduleData.ModuleName = worldData.WorldName;
        //            moduleData.ModuleDescription = worldData.WorldDescription;
        //            moduleData.IsLocal = true;
        //            moduleData.MaxPlayers = worldData.MaxCapacity;
        //            moduleData.DateTimeModified = File.GetLastWriteTime(Path.Combine(CreatorToolkitSettings.LocalWorldDirectory, moduleId + ".ctk")).ToString("dd MMMM yyyy hh:mm");
        //            moduleData.ThumbnailPath = Path.Combine(CreatorToolkitSettings.LocalWorldThumbnailDirectory, worldData.WorldId + ".png");

        //            ModuleModel.ListModuleData[i] = moduleData;

        //            break;
        //        }
        //    }

        //    MessageData messageData = new MessageData();
        //    messageData.Parameters.Add((byte)EParameterCodeToolkit.WORLD_DATA, worldData);

        //    dispatcher.Dispatch(EEventToolkitCrossContext.WORLD_DOWNLOADED, messageData);
        //}

        //private void UpdateListeners(bool value)
        //{
        //    dispatcher.UpdateListener(value, EEventToolkitCrossContext.WORLD_RESOURCES_DOWNLOADED, OnWorldResorucesDownloaded);
        //}

        //private void OnWorldResorucesDownloaded(IEvent evt)
        //{
        //    MessageData resultMD = new MessageData();
        //    resultMD.Parameters.Add((byte)EParameterCodeModule.MODULE_DATA, moduleData);

        //    dispatcher.Dispatch(EModuleEvents.MODULE_DOWNLOADED, resultMD);

        //    dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_DOWNLOAD_PROGRESS_WINDOW);

        //    UpdateListeners(false);

        //    Release();
        //}
    }
}
