﻿using strange.extensions.dispatcher.eventdispatcher.api;

namespace CuriiousIQ.Local.CreatorsToolkit.Module
{
    public interface ILocalFileSystemComService
    {
        IEventDispatcher dispatcher { get; set; }

        void GetAllLocalModulesData();

        void GetAllLocalModulesThumbnailData();
    }
}
