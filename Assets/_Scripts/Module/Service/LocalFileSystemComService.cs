﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using strange.extensions.dispatcher.eventdispatcher.api;
using System.Collections.Generic;
using System.IO;

namespace CuriiousIQ.Local.CreatorsToolkit.Module
{
    public class LocalFileSystemComService : ILocalFileSystemComService
    {
        [Inject]
        public IEventDispatcher dispatcher { get; set; }

        public void GetAllLocalModulesData()
        {
            List<string> filesList = new List<string>();

            string path = Settings.WorldPath;

            if (Directory.Exists(path))
            {
                List<string> files = new List<string>(Directory.GetFiles(path));

                foreach (var file in files)
                {
                    filesList.Add(file);
                }
            }

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeModule.LOCAL_MODULES_LIST, filesList);

            dispatcher.Dispatch(EModuleEvents.LOADING_LOCAL_MODULE_DATA_COMPLETE, msgData);
        }

        public void GetAllLocalModulesThumbnailData()
        {
            List<string> filesList = new List<string>();

            string path = Settings.WorldThumbnailPath;

            if(Directory.Exists(path))
            {
                List<string> files = new List<string>(Directory.GetFiles(path));

                foreach(var file in files)
                {
                    string id = Path.GetFileNameWithoutExtension(file);
                    if (!string.IsNullOrEmpty(id))
                    {
                        filesList.Add(id);
                    }
                }
            }

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeModule.LOCAL_MODULES_THUMBNAIL_LIST, filesList);

            dispatcher.Dispatch(EModuleEvents.GET_ALL_LOCAL_THUMBNAILS_DATA_COMPLETED, msgData);
        }
    }
}
