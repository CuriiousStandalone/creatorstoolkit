﻿using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Module
{
    public class MediatorModuleItem : EventMediator
    {
        [Inject]
        public ViewModuleItem view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            if (view != null)
            {
                view.dispatcher.UpdateListener(value, ViewModuleItem.ModuleItemEvents.ITEM_CLICKED, OnItemClicked);
            }
        }

        public void OnItemClicked(IEvent evt)
        {
            dispatcher.Dispatch(EModuleEvents.OPEN_MODULE_REQUESTED, evt.data);
        }
    }
}
