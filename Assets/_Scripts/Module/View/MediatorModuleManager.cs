﻿using System.Collections.Generic;

using UnityEngine;

using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Module
{
    public class MediatorModuleManager : EventMediator
    {
        [Inject]
        public ViewModuleManager view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.Init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewModuleManager.ModuleManagerEvents.VIEW_LOADED, OnViewLoaded);
            view.dispatcher.UpdateListener(value, ViewModuleManager.ModuleManagerEvents.PANEL_HIDDEN, OnPanelHidden);
            view.dispatcher.UpdateListener(value, ViewModuleManager.ModuleManagerEvents.PANEL_SHOWN, OnPanelShown);
            view.dispatcher.UpdateListener(value, ViewModuleManager.ModuleManagerEvents.DOWNLOAD_MODULE, OnDownloadModuleClicked);

            dispatcher.UpdateListener(value, EModuleEvents.HIDE_PANEL, OnHidePanel);
            dispatcher.UpdateListener(value, EModuleEvents.SHOW_PANEL, OnShowPanel);
            dispatcher.UpdateListener(value, EModuleEvents.ALL_MODULES_LOADED, OnAllModulesLoaded);
            dispatcher.UpdateListener(value, EModuleEvents.ALL_THUMBNAILS_DOWNLOADED, OnThumbnailsDownloaded);
            dispatcher.UpdateListener(value, EModuleEvents.MODULE_DOWNLOADED, OnModuleDownloaded);
        }

        private void OnViewLoaded(IEvent evt)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.PANEL_LOADED, evt.data);
        }

        private void OnPanelShown(IEvent evt)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.PANEL_SHOWN, evt.data);
        }

        private void OnPanelHidden(IEvent evt)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.PANEL_HIDDEN, evt.data);
        }

        private void OnHidePanel(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            string panelId = (string)msgData[(byte)EParameterCodeToolkit.PANEL_ID];

            if (view.PanelId == panelId)
            {
                view.HidePanel();
            }
        }

        private void OnShowPanel(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            string panelId = (string)msgData[(byte)EParameterCodeToolkit.PANEL_ID];

            if (view.PanelId == panelId)
            {
                view.ShowPanel();
            }
        }

        private void OnAllModulesLoaded(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            List<SModuleData> modulesList = (List<SModuleData>)msgData[(byte)EParameterCodeModule.ALL_MODULES_LIST];

            view.FillData(modulesList);
        }

        private void OnThumbnailsDownloaded(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            Dictionary<string, Sprite> thumbnailDictionary = (Dictionary<string, Sprite>)msgData.Parameters[(byte)EParameterCodeModule.THUMBNAIL_DICTIONARY];

            view.UpdateThumbnails(thumbnailDictionary);
        }

        private void OnModuleDownloaded(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            SModuleData moduleData = (SModuleData)msgData[(byte)EParameterCodeModule.MODULE_DATA];

            view.UpdateModule(moduleData);
        }

        private void OnDownloadModuleClicked(IEvent evt)
        {
            dispatcher.Dispatch(EModuleEvents.DOWNLOAD_MODULE, evt.data);
        }
    }
}
