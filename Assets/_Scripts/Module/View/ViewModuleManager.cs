﻿using System;
using System.Linq;
using System.Collections.Generic;

using UnityEngine;

using strange.extensions.mediation.impl;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents;
using PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIPanels;
using static PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents.CItemList;

namespace CuriiousIQ.Local.CreatorsToolkit.Module
{
    public class ViewModuleManager : EventView, IResponsivePanel
    {
        [SerializeField]
        private CItemList[] _collectionLists;
        [SerializeField]
        private CSearchBarTypeFilter[] _searchBars;

        private ListItemCardData[] _currentCardItems;

        private bool _downloadingDone;
        private bool _collectionsInited;

        internal enum ModuleManagerEvents
        {
            VIEW_LOADED,
            PANEL_SHOWN,
            PANEL_HIDDEN,
            DOWNLOAD_MODULE,
        }

        internal void Init()
        {
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelId);

            dispatcher.Dispatch(ModuleManagerEvents.VIEW_LOADED, msgData);

            InitCollections();
        }

        private void InitCollections()
        {
            if (_collectionsInited)
            {
                return;
            }
            foreach (CItemList collectionList in _collectionLists)
            {
                collectionList.InitNow();
                collectionList.OnRemove.AddListener(OnRemoveAsset);
                collectionList.OnDownload.AddListener(OnDownload);
                collectionList.ChangeSortFunction(SortFunction);
                collectionList.SortFilterFunction += ApplyCustomFilter;
                collectionList.ShouldSort = true;
            }
            _collectionsInited = true;
            foreach (CSearchBarTypeFilter searchBar in _searchBars)
            {
                searchBar.InitNow();
            }
        }

        private void OnRemoveAsset(ListItemCardData cardData)
        {
            //MessageData msgData = new MessageData();
            //msgData.Parameters.Add((byte)EParameterCodeResource.CARD_DATA, cardData);
            //dispatcher.Dispatch(ResourceManagerEvent.REMOVE_ASSET_CLICKED, msgData);
        }

        private void OnDownload(ListItemCardData cardData)
        {
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeModule.MODULE_ID, cardData.ResourceID);

            dispatcher.Dispatch(ModuleManagerEvents.DOWNLOAD_MODULE, msgData);
        }

        private static IEnumerable<ListItemCardData> SortFunction(IEnumerable<ListItemCardData> card)
        {
            return card.OrderBy(data => data.Value);
        }

        public void ApplyCustomFilter(string filterString, string[] filterType)
        {
            SortTypes sortType = SortTypes.NONE;
            int val = 0;

            switch(filterType[0])
            {
                case "Local modules recent":
                    {
                        val = 2;
                        sortType = SortTypes.META;
                        break;
                    }
                case "Local modules alphabetical":
                    {
                        val = 2;
                        sortType = SortTypes.DATA;
                        break;
                    }
                case "CMS modules":
                    {
                        val = 1;
                        sortType = SortTypes.NONE;
                        break;
                    }
            }

            foreach(CItemList collectionList in _collectionLists)
            {
                collectionList.ApplyCustomFilter(filterString, val, sortType);
            }
        }

        public void FillData(List<SModuleData> data)
        {
            InitCollections();

            ListItemCardData[] items = Array.ConvertAll(data.ToArray(), ResourceToCardData);

            _currentCardItems = items;

            //string[] types = items.SelectMany(cardData => cardData.Type).Distinct().ToArray();

            string[] types = new string[] { "Local modules recent", "Local modules alphabetical", "CMS modules" };

            foreach (CSearchBarTypeFilter searchBar in _searchBars)
            {
                searchBar.ReplaceTypeCollection(types);
            }

            foreach (CItemList collectionList in _collectionLists)
            {
                collectionList.ReplaceCollection(items);
            }
        }

        private ListItemCardData ResourceToCardData(SModuleData input)
        {
            ListItemCardData result = new ListItemCardData();

            string typeString = input.IsLocal ? "Local" : "Remote";

            result.ResourceID = input.ModuleId;
            result.Name = input.ModuleName;
            if (input.IsLocal)
            {
                result.Meta = input.DateTimeModified;
            }
            else
            {
                result.Meta = "";
            }

            if (!input.IsLocal)
            {
                result.Type = new[] { "Download to open" };
                result.Value = 2;
            }
            else
            {
                result.Type = new[] { "Team up to " + input.MaxPlayers + " students" };
                result.Value = 1;
            }
            result.ResourceType = input.IsLocal ? ListCardResourceTypes.PLATFORM : ListCardResourceTypes.REMOTE;

            return result;
        }

        public void UpdateThumbnails(Dictionary<string, Sprite> thumbnailDictionary)
        {
            foreach (ListItemCardData cardData in _currentCardItems)
            {
                if (string.IsNullOrEmpty(cardData.ResourceID))
                {
                    continue;
                }
                if (thumbnailDictionary.ContainsKey(cardData.ResourceID))
                {
                    Sprite sprite = thumbnailDictionary[cardData.ResourceID];
                    if (sprite != null)
                    {
                        cardData.Icon = sprite;
                    }
                }
            }
        }

        public void UpdateModule(SModuleData moduleData)
        {
            for (int index = 0; index < _currentCardItems.Length; index++)
            {
                if (string.IsNullOrEmpty(_currentCardItems[index].ResourceID))
                {
                    continue;
                }
                if (moduleData.ModuleId.Equals(_currentCardItems[index].ResourceID))
                {
                    //string typeString = Enum.GetName(typeof(ItemTypeCode), resource.ItemType);
                    _currentCardItems[index].ResourceID = moduleData.ModuleId;
                    _currentCardItems[index].Name = moduleData.ModuleName;

                    if (moduleData.IsLocal)
                    {
                        _currentCardItems[index].Meta = moduleData.DateTimeModified;
                    }
                    else
                    {
                        _currentCardItems[index].Meta = "";
                    }

                    if (!moduleData.IsLocal)
                    {
                        _currentCardItems[index].Type = new[] { "Download to open" };
                        _currentCardItems[index].Value = 2;
                    }
                    else
                    {
                        _currentCardItems[index].Type = new[] { "Team up to " + moduleData.MaxPlayers + " students" };
                        _currentCardItems[index].Value = 1;
                    }
                    _currentCardItems[index].ResourceType = moduleData.IsLocal ? ListCardResourceTypes.PLATFORM : ListCardResourceTypes.REMOTE;
                }
            }
        }

        #region Interface functions

        public string PanelId
        {
            get
            {
                return gameObject.GetComponent<CPanel>()._panelId;
            }
        }

        public void HidePanel()
        {
            gameObject.SetActive(false);

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelId);

            dispatcher.Dispatch(ModuleManagerEvents.PANEL_HIDDEN, msgData);
        }

        public void ShowPanel()
        {
            gameObject.SetActive(true);

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelId);

            dispatcher.Dispatch(ModuleManagerEvents.PANEL_SHOWN, msgData);
        }

        public void ShowPanelWithTween()
        {
            throw new System.NotImplementedException();
        }

        public void HidePanelWithTween()
        {
            throw new System.NotImplementedException();
        }

        public void IncreaseHeight(float height)
        {
            throw new System.NotImplementedException();
        }

        public void IncreaseHeightFromBottom(float height)
        {
            throw new System.NotImplementedException();
        }

        public void IncreaseHeightFromTop(float height)
        {
            throw new System.NotImplementedException();
        }

        public void DecreaseHeight(float height)
        {
            throw new System.NotImplementedException();
        }

        public void DecreaseHeightFromBottom(float height)
        {
            throw new System.NotImplementedException();
        }

        public void DecreaseHeightFromTop(float height)
        {
            throw new System.NotImplementedException();
        }

        public void IncreaseWidth(float width)
        {
            throw new System.NotImplementedException();
        }

        public void IncreaseWidthFromRight(float width)
        {
            throw new System.NotImplementedException();
        }

        public void IncreaseWidthFromLeft(float width)
        {
            throw new System.NotImplementedException();
        }

        public void DecreaseWidth(float width)
        {
            throw new System.NotImplementedException();
        }

        public void DecreaseWidthFromRight(float width)
        {
            throw new System.NotImplementedException();
        }

        public void DecreaseWidthFromLeft(float width)
        {
            throw new System.NotImplementedException();
        }

        public void RepositionToTop(float height)
        {
            gameObject.GetComponent<CPanel>().RepositionToTop(height);
        }

        public void RepositionToBottom(float height)
        {
            gameObject.GetComponent<CPanel>().RepositionToBottom(height);
        }

        public void RepositionToLeft(float width)
        {
            gameObject.GetComponent<CPanel>().RepositionToLeft(width);
        }

        public void RepositionToRight(float width)
        {
            gameObject.GetComponent<CPanel>().RepositionToRight(width);
        }

        #endregion
    }
}
