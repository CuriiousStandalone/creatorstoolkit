﻿using strange.extensions.mediation.impl;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents;

namespace CuriiousIQ.Local.CreatorsToolkit.Module
{
    public class ViewModuleItem : EventView
    {
        internal enum ModuleItemEvents
        {
            ITEM_CLICKED,
        }

        public void ModuleClick_Handler()
        {
            if (GetComponent<ListItemCardComponent>().Item.ResourceType == ListCardResourceTypes.PLATFORM)
            {
                MessageData msgData = new MessageData();
                msgData.Parameters.Add((byte)EParameterCodeModule.MODULE_ID, gameObject.GetComponent<ListItemCardComponent>().Item.ResourceID);

                dispatcher.Dispatch(ModuleItemEvents.ITEM_CLICKED, msgData);
            }
        }
    }
}
