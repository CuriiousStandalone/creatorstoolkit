﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using strange.extensions.context.api;
using strange.extensions.context.impl;

using UnityEngine;

namespace CuriiousIQ.Local.CreatorsToolkit.Module
{
    public class ContextModule : MVCSContext
    {
        public ContextModule(MonoBehaviour view) : base(view)
        {
        }

        public ContextModule(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
        {
        }

        protected override void mapBindings()
        {
            #region Model Bindings

            injectionBinder.Bind<IModuleModel>().To<ModuleModel>().ToSingleton();
            injectionBinder.Bind<ILocalFileSystemComService>().To<LocalFileSystemComService>().ToSingleton();

            #endregion

            #region View-MediatorBindings

            mediationBinder.Bind<ViewModuleManager>().To<MediatorModuleManager>();
            mediationBinder.Bind<ViewModuleItem>().To<MediatorModuleItem>();
            mediationBinder.Bind<ViewModuleTooltipManager>().To<MediatorModuleTooltipManager>();

            #endregion

            #region Command Binding

            commandBinder.Bind(EModuleEvents.LOAD_ALL_MODULES).To<CommandLoadAllModules>();
            commandBinder.Bind(EModuleEvents.LOAD_LOCAL_MODULE_DATA).To<CommandGetLocalModulesData>();
            commandBinder.Bind(EModuleEvents.FETCH_REMOTE_MODULE_DATA).To<CommandGetRemoteModulesData>();
            commandBinder.Bind(EModuleEvents.DOWNLOAD_MODULE).To<CommandDownloadModule>();
            commandBinder.Bind(EModuleEvents.OPEN_MODULE_REQUESTED).To<CommandOpenModuleRequested>();
            commandBinder.Bind(EModuleEvents.TOOLTIP_TEXT_REQUESTED).To<CommandTooltipRequest>();

            commandBinder.Bind(EEventToolkitCrossContext.SHOW_PANEL).To<CommandShowPanel>();
            commandBinder.Bind(EEventToolkitCrossContext.HIDE_PANEL).To<CommandHidePanel>();

            commandBinder.Bind(EEventToolkitCrossContext.TOOLTIP_TEXT_RESPONSE).To<CommandTooltipResponse>();

            commandBinder.Bind(EEventToolkitCrossContext.REFRESH_MODULE_LIST).To<CommandLoadAllModules>().To<CommandDownloadModulesThumbnails>();

            #endregion
        }
    }
}
