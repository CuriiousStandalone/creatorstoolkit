﻿using strange.extensions.context.impl;
using strange.extensions.context.api;

using UnityEngine;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Main
{
    public class ContextMain : MVCSContext
    {
        public ContextMain(MonoBehaviour view) : base(view)
        {

        }

        public ContextMain(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
        {

        }

        protected override void mapBindings()
        {
            #region CrossContextCommand Bindings

            foreach(var crossContextEnum in System.Enum.GetValues(typeof(EEventToolkitCrossContext)))
            {
                crossContextBridge.Bind(crossContextEnum);
            }

            #endregion

            #region Model Bindings

            injectionBinder.Bind<ITooltipModel>().To<TooltipModel>().ToSingleton();

            #endregion

            #region View-MediatorBindings

            mediationBinder.Bind<ViewMainMenuManager>().To<MediatorMainMenuManager>();
            mediationBinder.Bind<ViewDownloadProgressDialog>().To<MediatorDownloadProgressDialog>();
            mediationBinder.Bind<ViewTipsManager>().To<MediatorTipsManager>();

            #endregion

            #region Command Binding

            commandBinder.Bind(ContextEvent.START).To<CommandStart>().Once().To<CommandLoadTooltips>().Once();

            commandBinder.Bind(EMainEvent.LOAD_SCENES).To<CommandLoadScene>();

            commandBinder.Bind(EEventToolkitCrossContext.UI_CONFIG_LOADED).To<CommandLoadAllScenes>();
            commandBinder.Bind(EEventToolkitCrossContext.SHOW_PANEL).To<CommandShowPanel>();
            commandBinder.Bind(EEventToolkitCrossContext.HIDE_PANEL).To<CommandHidePanel>();
            commandBinder.Bind(EEventToolkitCrossContext.SHOW_DOWNLOAD_PROGRESS_WINDOW).To<CommandShowProgressWindow>();
            commandBinder.Bind(EEventToolkitCrossContext.HIDE_DOWNLOAD_PROGRESS_WINDOW).To<CommandHideProgressWindow>();
            commandBinder.Bind(EEventToolkitCrossContext.UPDATE_DOWNLOAD_PROGRESS).To<CommandUpdateDownloadProgress>();

            commandBinder.Bind(EEventToolkitCrossContext.TOOLTIP_TEXT_REQUESTED).To<CommandGetTooltip>();

            #endregion
        }
    }
}
