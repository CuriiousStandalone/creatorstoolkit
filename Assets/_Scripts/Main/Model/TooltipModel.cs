﻿using System.Collections.Generic;

namespace CuriiousIQ.Local.CreatorsToolkit.Main
{
    public class TooltipModel : ITooltipModel
    {
        private Dictionary<int, string> _tooltipDictionary;

        public Dictionary<int, string> TooltipDictionary
        {
            get
            {
                if(null == _tooltipDictionary)
                {
                    _tooltipDictionary = new Dictionary<int, string>();
                }

                return _tooltipDictionary;
            }

            set
            {
                _tooltipDictionary = value;
            }
        }
    }
}
