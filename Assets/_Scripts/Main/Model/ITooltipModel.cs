﻿using System.Collections.Generic;

namespace CuriiousIQ.Local.CreatorsToolkit.Main
{
    public interface ITooltipModel
    {
        Dictionary<int, string> TooltipDictionary { get; set; }
    }
}
