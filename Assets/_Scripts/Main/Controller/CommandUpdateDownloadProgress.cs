﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Main
{
    public class CommandUpdateDownloadProgress : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EMainEvent.UPDATE_DOWNLOAD_PROGRESS, evt.data);
        }
    }
}
