﻿using System;
using System.Collections.Generic;

using strange.extensions.command.impl;

using UnityEngine;

using PulseIQ.Local.ClientCommon.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Main
{
    public class CommandLoadScene : EventCommand
    {
        public override void Execute()
        {
            MessageData msgData = (MessageData)evt.data;

            List<string> scenes = (List<string>)msgData[(byte)EParameterCodeMain.LIST_SCENE_NAMES];

            foreach (string filepath in scenes)
            {
                //Load the component
                if (String.IsNullOrEmpty(filepath))
                {
                    throw new Exception("Can't load a module with a null or empty filepath.");
                }

                Application.LoadLevelAdditive(filepath);
            }
        }
    }
}
