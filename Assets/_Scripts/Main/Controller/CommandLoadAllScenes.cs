﻿using System.Collections.Generic;

using strange.extensions.command.impl;

using PulseIQ.Local.ClientCommon.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Main
{
    public class CommandLoadAllScenes : EventCommand
    {
        public override void Execute()
        {
            MessageData msgData = new MessageData();

            List<string> scenes = new List<string>();

            scenes.Add("Resource");
            scenes.Add("Module");
            scenes.Add("World");

            msgData.Parameters.Add((byte)EParameterCodeMain.LIST_SCENE_NAMES, scenes);

            dispatcher.Dispatch(EMainEvent.LOAD_SCENES, msgData);
        }
    }
}
