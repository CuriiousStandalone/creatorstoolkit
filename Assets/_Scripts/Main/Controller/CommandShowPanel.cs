﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Main
{
    public class CommandShowPanel : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EMainEvent.SHOW_PANEL, evt.data);
        }
    }
}
