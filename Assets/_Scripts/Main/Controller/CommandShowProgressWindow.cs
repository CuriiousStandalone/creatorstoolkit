﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Main
{
    public class CommandShowProgressWindow : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EMainEvent.SHOW_PROGRESS_WINDOW);
        }
    }
}
