﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Main
{
    public class CommandHideProgressWindow : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EMainEvent.HIDE_PROGRESS_WINDOW);
        }
    }
}
