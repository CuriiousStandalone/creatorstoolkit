﻿using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Main
{
    public class CommandHidePanel : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(EMainEvent.HIDE_PANEL, evt.data);
        }
    }
}
