﻿using System;
using System.Collections.Generic;

using UnityEngine;

using strange.extensions.command.impl;

using PulseIQ.Local.ClientCommon.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Main
{
    public class CommandGetTooltip : EventCommand
    {
        [Inject]
        public ITooltipModel TooltipModel { get; set; }

        public override void Execute()
        {
            MessageData msgData = (MessageData)evt.data;

            int id = (int)msgData[(byte)EParameterCodeToolkit.TOOLTIP_ID];

            msgData.Parameters.Add((byte)EParameterCodeToolkit.TOOLTIP_TEXT, TooltipModel.TooltipDictionary[id]);

            dispatcher.Dispatch(EEventToolkitCrossContext.TOOLTIP_TEXT_RESPONSE, msgData);
        }
    }
}
