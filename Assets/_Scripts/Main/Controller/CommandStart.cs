﻿using System.IO;
using System.Linq;
using System.Collections.Generic;

using UnityEngine;

using strange.extensions.command.impl;

using SFB;

using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Main
{
    public class CommandStart : EventCommand
    {
        public override void Execute()
        {
            MessageData msgData = new MessageData();

            List<string> scenes = new List<string>();
            scenes.Add("Layout");

            Settings.applicationType = EApplicationType.CREATOR_TOOL_KIT;
            
            // Removing for new start up logic with Phil
            //GetWorkingDirectory();

            TextAsset _xml = Resources.Load<TextAsset>("PulseIQConfig");
            StringReader reader = new StringReader(_xml.ToString());

            GlobalSettings.ReadData(reader);

            msgData.Parameters.Add((byte)EParameterCodeMain.LIST_SCENE_NAMES, scenes);

            dispatcher.Dispatch(EMainEvent.LOAD_SCENES, msgData);
        }

        private void GetWorkingDirectory()
        {
            ExtensionFilter[] extensions = new[]
            {
                new ExtensionFilter()
            };

            MessageData message = new MessageData();

            string[] selectedFiles = StandaloneFileBrowser.OpenFolderPanel("Module Folder Selection", "", false);

            if (selectedFiles.Length > 0)
            {
                List<string> filesFolders = Directory.GetDirectories(selectedFiles[0]).ToList();

                bool hasPackageData = false;

                foreach (var str in filesFolders)
                {
                    if (Path.GetFileName(str) == "PackageData")
                    {
                        hasPackageData = true;

                        break;
                    }
                }

                if (!hasPackageData)
                {
                    string newPDPath = Path.Combine(selectedFiles[0], "PackageData");

                    Directory.CreateDirectory(newPDPath);
                }

                Settings.editorRootPath = selectedFiles[0];
            }
            else
            {
                //Settings.clientPath = "CreatorToolkit";
                Settings.editorRootPath = Settings.defaultEditorRootPath;

                if(!Directory.Exists(Settings.editorRootPath))
                {
                    Directory.CreateDirectory(Settings.editorRootPath);
                }
            }
        }
    }
}