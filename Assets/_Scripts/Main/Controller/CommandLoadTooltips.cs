﻿using System;
using System.Collections.Generic;

using UnityEngine;

using strange.extensions.command.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Main
{
    public class CommandLoadTooltips : EventCommand
    {
        [Inject]
        public ITooltipModel TooltipModel { get; set; }

        public override void Execute()
        {
            TextAsset textAsset = (TextAsset)Resources.Load("TooltipList", typeof(TextAsset));

            string[] listOptions = textAsset.text.Split(new string[] { "\r\n" }, StringSplitOptions.None);

            Dictionary<int, string> dictionary = new Dictionary<int, string>(TooltipModel.TooltipDictionary);

            foreach(string option in listOptions)
            {
                string[] combination = option.Split(',');

                if(combination.Length <= 1 || string.IsNullOrEmpty(combination[0]))
                {
                    break;
                }

                try
                {
                    dictionary.Add(Convert.ToInt32(combination[0]), combination[1]);
                }
                catch
                {
                    Debug.Log("Exception at key: " + combination[0]);
                }
            }

            TooltipModel.TooltipDictionary = dictionary;
        }
    }
}
