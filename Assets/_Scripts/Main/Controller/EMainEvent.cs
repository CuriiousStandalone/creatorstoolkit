﻿namespace CuriiousIQ.Local.CreatorsToolkit.Main
{
    public enum EMainEvent
    {
        LOAD_SCENES,
        HIDE_PANEL,
        SHOW_PANEL,

        SHOW_PROGRESS_WINDOW,
        HIDE_PROGRESS_WINDOW,
        UPDATE_DOWNLOAD_PROGRESS,
    }
}
