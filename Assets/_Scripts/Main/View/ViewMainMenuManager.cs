﻿using strange.extensions.mediation.impl;

using UnityEngine;
using UnityEngine.UI;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIPanels;
using SFB;
using System.IO;
using System.Collections.Generic;
using PulseIQ.Local.ClientCommon.Components.Common.UIElements;
using TMPro;
using PortableDevices;

namespace CuriiousIQ.Local.CreatorsToolkit.Main
{
    public class ViewMainMenuManager : EventView, IResponsivePanel
    {
        [SerializeField]
        private List<Button> buttonList = new List<Button>();

        [SerializeField]
        private GameObject newOpenWindow;

        private string moduleName;

        private PortableDevice currentDevice;
        private PortableDeviceFolder currentDeviceModulePath;
        private string[] currentModulePaths;

        [SerializeField]
        private GameObject genericPopupDialog;

        [SerializeField]
        private GameObject TransferPopup;
        [SerializeField]
        private Button TransferButton;
        [SerializeField]
        private TMP_Text TrasnferText;

        [SerializeField]
        private Button transferToDeviceButton;
        [SerializeField]
        private Button UpdateToolkitButton;

        internal enum MainMenuEvents
        {
            PANEL_SHOWN,
            PANEL_HIDDEN,
            OPEN_MODULE,
            MODULE_SELECTED,
            NEW_MODULE,
            SAVE_MODULE,
            SAVE_MODULE_AS,
            PUBLISH_MODULE_CMS,
            UPLOAD_FILES,
            QUIT_APP,
            PACKAGE_MODULE,
            IMPORT_PACKAGE,
            PUBLISH_PACKAGE_TO_HEADSET,
        }

        internal void Init()
        {
            foreach(Button button in buttonList)
            {
                button.enabled = false;
                button.GetComponent<CTextImageButton>().enabled = false;
            }
        }

        public void UpdateModuleName(string name)
        {
            moduleName = name;
        }

        override protected void Start()
        {
            base.Start();

            string[] args = System.Environment.GetCommandLineArgs();

            if (args.Length > 0)
            {
                foreach (string argument in args)
                {
                    string extension = Path.GetExtension(argument);

                    if (extension == ".ctk")
                    {
                        MessageData message = new MessageData();
                        message.Parameters.Add((byte)EParameterCodeToolkit.WORLD_PATH, argument);
                        message.Parameters.Add((byte)EParameterCodeToolkit.WORLD_ID, Path.GetFileName(argument));
                        Settings.editorRootPath = Path.GetDirectoryName(argument);

                        dispatcher.Dispatch(MainMenuEvents.MODULE_SELECTED, message);
                    }
                }
            }

            UpdateToolkitButton.gameObject.GetComponent<CTextImageButton>().enabled = false;

            if (Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor)
            {
                transferToDeviceButton.gameObject.GetComponent<CTextImageButton>().enabled = false;
                transferToDeviceButton.interactable = false;
            }
        }

        public void OpenModule_ClickHandler()
        {
            //This is how you can show Module List Panel.
            //dispatcher.Dispatch(MainMenuEvents.OPEN_MODULE);

            ExtensionFilter[] extensions = new[]
            {
                new ExtensionFilter("Creators Toolkit file", "ctk")
            };

            MessageData message = new MessageData();

            string[] selectedFiles = StandaloneFileBrowser.OpenFilePanel("Select CTK file", "", extensions, false);

            if (selectedFiles.Length > 0)
            {
                message.Parameters.Add((byte)EParameterCodeToolkit.WORLD_PATH, selectedFiles[0]);
                message.Parameters.Add((byte)EParameterCodeToolkit.WORLD_ID, Path.GetFileName(selectedFiles[0]));
                Settings.editorRootPath = Path.GetDirectoryName(selectedFiles[0]);

                dispatcher.Dispatch(MainMenuEvents.MODULE_SELECTED, message);
            }
        }

        public void NewModule_ClickHandler()
        {
            dispatcher.Dispatch(MainMenuEvents.NEW_MODULE);
        }

        public void SaveModule_ClickHandler()
        {
            dispatcher.Dispatch(MainMenuEvents.SAVE_MODULE);
        }

        public void SaveModuleAs_ClickHandler()
        {
            dispatcher.Dispatch(MainMenuEvents.SAVE_MODULE_AS);
        }

        public void PublishModule_ClickHandler()
        {
            dispatcher.Dispatch(MainMenuEvents.PUBLISH_MODULE_CMS);
        }

        public void UploadFiles_ClickHandler()
        {
            dispatcher.Dispatch(MainMenuEvents.UPLOAD_FILES);
        }

        public void PackageModule_ClickHandler()
        {
            ExtensionFilter[] extensions = new[]
            {
                new ExtensionFilter("Creators Toolkit module package", "ciqmodule")
            };

            //string[] selectedFiles = StandaloneFileBrowser.Open("Select CTK file", "", extensions, false);

            string selectedFiles = StandaloneFileBrowser.SaveFilePanel("Module package name", "", moduleName, extensions);

            if (selectedFiles.Length > 0)
            {
                MessageData message = new MessageData();
                message.Parameters.Add((byte)EParameterCodeToolkit.PACKAGE_PATH, selectedFiles);

                dispatcher.Dispatch(MainMenuEvents.PACKAGE_MODULE, message);
            }
        }

        public void PublishPackageToHeadset_ClickHandler()
        {
            PortableDeviceCollection devices = new PortableDeviceCollection();
            devices.Refresh();

            if(devices.Count > 0)
            {
                ExtensionFilter[] extensions = new[]
                {
                    new ExtensionFilter("Creators Toolkit module package", "ciqmodule")
                };

                string[] selectedFiles;

                if (Directory.Exists("Documents/CuriiousIQ/Modules"))
                {
                    selectedFiles = StandaloneFileBrowser.OpenFilePanel("Select Curiious IQ Modules", "Documents/CuriiousIQ/Modules", extensions, true);
                }
                else
                {
                    selectedFiles = StandaloneFileBrowser.OpenFilePanel("Select Curiious IQ Modules", "", extensions, true);
                }
                

                if (selectedFiles.Length > 0)
                {
                    bool deviceHasFolder = false;

                    foreach (PortableDevice device in devices)
                    {
                        if(deviceHasFolder)
                        {
                            break;
                        }
                        
                        device.Connect();

                        PortableDeviceFolder root = device.GetContents();

                        if(null == root || null == root.Files || root.Files.Count == 0)
                        {
                            continue;
                        }

                        PortableDeviceFolder internalStorage = root.Files[0] as PortableDeviceFolder;

                        if(null == internalStorage)
                        {
                            continue;
                        }

                        PortableDeviceFolder curiiousFolder = GetSubFolder(internalStorage, "CuriiousIQ");

                        if(null == curiiousFolder)
                        {
                            continue;
                        }

                        PortableDeviceFolder modulesFolder = GetSubFolder(curiiousFolder, "Modules");

                        if(null == modulesFolder)
                        {
                            continue;
                        }

                        currentModulePaths = selectedFiles;

                        currentDevice = device;

                        currentDeviceModulePath = modulesFolder;

                        long byteSize = 0;

                        string fileNames = "";
                        bool addedFirstFile = false;
                        List<string> nameList = new List<string>();

                        foreach (string files in selectedFiles)
                        {
                            FileInfo info = new FileInfo(files);
                            byteSize += info.Length;
                            nameList.Add(Path.GetFileNameWithoutExtension(files));

                            if (addedFirstFile)
                            {
                                fileNames += ",";
                            }

                            if (!addedFirstFile)
                            {
                                addedFirstFile = true;
                            }                          
                        }

                        if(nameList.Count == 1)
                        {
                            fileNames += Path.GetFileNameWithoutExtension(nameList[0]);
                        }
                        else if(nameList.Count == 2)
                        {
                            fileNames = nameList[0] + " and " + nameList[1];
                        }
                        else
                        {
                            for(int i = 0; i < nameList.Count; ++i)
                            {
                                if (i == 0)
                                {
                                    fileNames = nameList[i];
                                }
                                else if (i + 1  == nameList.Count)
                                {
                                    fileNames += " and " + nameList[i];
                                }
                                else
                                {
                                    fileNames += ", " + nameList[i];
                                }
                            }
                        }

                        byteSize = byteSize / 1000000;

                        genericPopupDialog.SetActive(true);

                        GenericPopupDialogTMPro dialogClass = genericPopupDialog.GetComponent<GenericPopupDialogTMPro>();

                        dialogClass.InitializeDialog("Publish to " + device.FriendlyName, "Would you like to publish the module " + fileNames + " to " + device.FriendlyName + "? This will require " + byteSize + "mb of available storage.", "No", "Yes", null, PublishToDevice);

                        deviceHasFolder = true;
 
                        device.Disconnect();
                    }
                    
                    if(!deviceHasFolder)
                    {
                        foreach (PortableDevice device in devices)
                        {
                            currentModulePaths = selectedFiles;

                            currentDevice = device;

                            genericPopupDialog.SetActive(true);

                            GenericPopupDialogTMPro dialogClass = genericPopupDialog.GetComponent<GenericPopupDialogTMPro>();

                            dialogClass.InitializeDialog("CURIIOUS IQ MODULES FOLDER NOT FOUND.", "The device is not a VR headset or Curiious has never been run before. Would you like to publish anyway?", "No", "Publish", null, CreateAndPublishToDevice);

                            break;
                        }
                    }

                    //MessageData message = new MessageData();
                    //message.Parameters.Add((byte)EParameterCodeToolkit.PACKAGE_PATH, selectedFiles);
                    //
                    //dispatcher.Dispatch(MainMenuEvents.PUBLISH_PACKAGE_TO_HEADSET, message);
                }
            }
            else
            {
                genericPopupDialog.SetActive(true);

                GenericPopupDialogTMPro dialogClass = genericPopupDialog.GetComponent<GenericPopupDialogTMPro>();

                dialogClass.InitializeDialog("There are no devices connected.", "Please check the device's connection and try again.", "OK", null);
            }
        }

        public void PublishToDevice()
        {
            TrasnferText.text = "Waiting...";
            TransferPopup.SetActive(true);
            TransferButton.interactable = false;

            Invoke("DelayedCopyToDevice", 0.25f);
        }

        public void DelayedCopyToDevice()
        {
            currentDevice.Connect();

            foreach (string modules in currentModulePaths)
            {
                bool skip = false;

                foreach (var files in currentDeviceModulePath.Files)
                {
                    if (files is PortableDeviceFile)
                    {
                        if (files.Name == Path.GetFileName(modules))
                        {
                            skip = true;
                            break;
                        }
                    }
                }

                if (skip)
                {
                    continue;
                }

                currentDevice.TransferFileToDevice(modules, currentDeviceModulePath.Id);
            }

            currentDevice.Disconnect();

            TrasnferText.text = "Done!";
            TransferButton.interactable = true;
        }

        public void CreateAndPublishToDevice()
        {
            TrasnferText.text = "Waiting...";
            TransferPopup.SetActive(true);
            TransferButton.interactable = false;

            Invoke("CreateAndPublishToDevice", 0.25f);
        }

        public void DelayedCreateAndPublishToDevice()
        {
            currentDevice.Connect();

            PortableDeviceFolder root = currentDevice.GetContents();

            if (null == root || null == root.Files || root.Files.Count == 0)
            {
                Invoke("DelayedErrorPopup", 0.5f);

                return;
            }

            PortableDeviceFolder internalStorage = root.Files[0] as PortableDeviceFolder;

            PortableDeviceFolder curiiousFolder = GetSubFolder(internalStorage, "CuriiousIQ");

            if (null == curiiousFolder)
            {
                currentDevice.CreateFolder("CuriiousIQ", internalStorage.Id);

                curiiousFolder = GetSubFolder(internalStorage, "CuriiousIQ");

                currentDevice.CreateFolder("Modules", curiiousFolder.Id);
            }
            else
            {
                currentDevice.CreateFolder("Modules", curiiousFolder.Id);
            }

            PortableDeviceFolder modulesFolder = GetSubFolder(curiiousFolder, "Modules");

            currentDeviceModulePath = modulesFolder;

            foreach (string modules in currentModulePaths)
            {
                currentDevice.TransferFileToDevice(modules, currentDeviceModulePath.Id);
            }

            currentDevice.Disconnect();

            TrasnferText.text = "Done!";
            TransferButton.interactable = true;
        }

        public void DelayedErrorPopup()
        {
            genericPopupDialog.SetActive(true);

            GenericPopupDialogTMPro dialogClass = genericPopupDialog.GetComponent<GenericPopupDialogTMPro>();

            dialogClass.InitializeDialog("Error!", "Cannot access the device. Please check the connection and try again.", "OK", null);
        }

        public void ImportPackage_ClickHandler()
        {
            ExtensionFilter[] extensions = new[]
            {
                new ExtensionFilter("Creators Toolkit content package", "ctkcontent")
            };

            //string[] selectedFiles = StandaloneFileBrowser.Open("Select CTK file", "", extensions, false);

            string[] selectedFiles = StandaloneFileBrowser.OpenFilePanel("Select CTK content package", "", extensions, false);

            if (selectedFiles.Length > 0)
            {
                MessageData message = new MessageData();
                message.Parameters.Add((byte)EParameterCodeToolkit.PACKAGE_PATH, selectedFiles[0]);

                dispatcher.Dispatch(MainMenuEvents.IMPORT_PACKAGE, message);
            }
        }

        public void PackagingFinished()
        {
            genericPopupDialog.SetActive(true);

            GenericPopupDialogTMPro dialogClass = genericPopupDialog.GetComponent<GenericPopupDialogTMPro>();

            dialogClass.InitializeDialog("Module Packaged", "To view in IQ, place the file in your headset's /CuriiousIQ/Modules folder", "OK", null);
        }

        public void ImportingFinished()
        {
            genericPopupDialog.SetActive(true);

            GenericPopupDialogTMPro dialogClass = genericPopupDialog.GetComponent<GenericPopupDialogTMPro>();

            dialogClass.InitializeDialog("CTK Package Imported", "Content pack imported successfully.", "OK", null);
        }

        public void QuitApp_ClickHandler()
        {
            dispatcher.Dispatch(MainMenuEvents.QUIT_APP);
        }

        public string PanelId
        {
            get
            {
                return gameObject.GetComponent<CPanel>()._panelId;
            }
        }

        public void EnableButtons()
        {
            foreach (Button button in buttonList)
            {
                button.enabled = true;
                button.GetComponent<CTextImageButton>().enabled = true;
            }

            newOpenWindow.SetActive(false);
        }

        public void HidePanel()
        {
            gameObject.SetActive(false);

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelId);

            dispatcher.Dispatch(MainMenuEvents.PANEL_HIDDEN, msgData);
        }

        public void ShowPanel()
        {
            gameObject.SetActive(true);

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, PanelId);

            dispatcher.Dispatch(MainMenuEvents.PANEL_SHOWN, msgData);
        }

        public void ShowPanelWithTween()
        {
            Debug.Log("ShowPanelWithTween");
        }

        public void HidePanelWithTween()
        {
            Debug.Log("HidePanelWithTween");
        }

        public void IncreaseHeight(float height)
        {
            Debug.Log("IncreaseHeight");
        }

        public void IncreaseHeightFromBottom(float height)
        {
            Debug.Log("IncreaseHeightFromBottom");
        }

        public void IncreaseHeightFromTop(float height)
        {
            Debug.Log("IncreaseHeightFromTop");
        }

        public void DecreaseHeight(float height)
        {
            Debug.Log("DecreaseHeight");
        }

        public void DecreaseHeightFromBottom(float height)
        {
            Debug.Log("DecreaseHeightFromBottom");
        }

        public void DecreaseHeightFromTop(float height)
        {
            Debug.Log("DecreaseHeightFromTop");
        }

        public void IncreaseWidth(float width)
        {
            Debug.Log("IncreaseWidth");
        }

        public void IncreaseWidthFromRight(float width)
        {
            Debug.Log("IncreaseWidthFromRight");
        }

        public void IncreaseWidthFromLeft(float width)
        {
            Debug.Log("IncreaseWidthFromLeft");
        }

        public void DecreaseWidth(float width)
        {
            Debug.Log("DecreaseWidth");
        }

        public void DecreaseWidthFromRight(float width)
        {
            Debug.Log("DecreaseWidthFromRight");
        }

        public void DecreaseWidthFromLeft(float width)
        {
            Debug.Log("DecreaseWidthFromLeft");
        }

        public void RepositionToTop(float height)
        {
            gameObject.GetComponent<CPanel>().RepositionToTop(height);
        }

        public void RepositionToBottom(float height)
        {
            gameObject.GetComponent<CPanel>().RepositionToBottom(height);
        }

        public void RepositionToLeft(float width)
        {
            gameObject.GetComponent<CPanel>().RepositionToLeft(width);
        }

        public void RepositionToRight(float width)
        {
            gameObject.GetComponent<CPanel>().RepositionToRight(width);
        }

        PortableDeviceFolder GetSubFolder(PortableDeviceFolder pdf, string folderName)
        {
            PortableDeviceFolder deviceFolder = null;

            foreach (var folder in pdf.Files)
            {

                if (folder is PortableDeviceFolder)
                {
                    if (folder.Name == folderName)
                    {
                        deviceFolder = folder as PortableDeviceFolder;
                    }

                }
            }

            return deviceFolder;
        }
    }
}
