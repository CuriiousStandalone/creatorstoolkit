﻿using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

using PulseIQ.Local.ClientCommon.Components.Common;
using CuriiousIQ.Local.CreatorsToolkit.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Main
{
    public class MediatorDownloadProgressDialog : EventMediator
    {
        [Inject]
        public ViewDownloadProgressDialog view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            dispatcher.UpdateListener(value, EMainEvent.SHOW_PROGRESS_WINDOW, OnShowView);
            dispatcher.UpdateListener(value, EMainEvent.HIDE_PROGRESS_WINDOW, OnHideView);
            dispatcher.UpdateListener(value, EMainEvent.UPDATE_DOWNLOAD_PROGRESS, OnUpdateProgress);
        }

        private void OnShowView()
        {
            view.ShowView();
        }

        private void OnHideView()
        {
            view.HideView();
        }

        private void OnUpdateProgress(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            double progressVal = (double)msgData[(byte)EParameterCodeToolkit.PROGRESS_BAR_VALUE];

            view.UpdateProgress(progressVal);
        }
    }
}
