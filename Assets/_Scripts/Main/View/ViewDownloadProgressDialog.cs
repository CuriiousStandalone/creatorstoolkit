﻿using UnityEngine;
using UnityEngine.UI;

using strange.extensions.mediation.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Main
{
    public class ViewDownloadProgressDialog : EventView
    {
        [SerializeField]
        private Image _progressBar;

        public void ShowView()
        {
            _progressBar.fillAmount = 0;
            gameObject.SetActive(true);
        }

        public void HideView()
        {
            gameObject.SetActive(false);
        }

        public void UpdateProgress(double value)
        {
            _progressBar.fillAmount = (float)value;
        }
    }
}
