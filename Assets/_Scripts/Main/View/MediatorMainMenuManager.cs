﻿using UnityEngine;

using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Main
{
    public class MediatorMainMenuManager : EventMediator
    {
        [Inject]
        public ViewMainMenuManager view { get; set; }

        [SerializeField]
        GameObject genericDialogPopup;

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.Init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewMainMenuManager.MainMenuEvents.PANEL_HIDDEN, OnPanelHidden);
            view.dispatcher.UpdateListener(value, ViewMainMenuManager.MainMenuEvents.PANEL_SHOWN, OnPanelShown);
            //view.dispatcher.UpdateListener(value, ViewMainMenuManager.MainMenuEvents.OPEN_MODULE, OnOpenModule);
            view.dispatcher.UpdateListener(value, ViewMainMenuManager.MainMenuEvents.MODULE_SELECTED, OnModuleSelected);
            view.dispatcher.UpdateListener(value, ViewMainMenuManager.MainMenuEvents.NEW_MODULE, OnNewModule);
            view.dispatcher.UpdateListener(value, ViewMainMenuManager.MainMenuEvents.SAVE_MODULE, OnSaveModule);
            view.dispatcher.UpdateListener(value, ViewMainMenuManager.MainMenuEvents.SAVE_MODULE_AS, OnSaveModuleAs);
            view.dispatcher.UpdateListener(value, ViewMainMenuManager.MainMenuEvents.PUBLISH_MODULE_CMS, OnPublishModuleToCMS);
            view.dispatcher.UpdateListener(value, ViewMainMenuManager.MainMenuEvents.UPLOAD_FILES, OnUploadFiles);
            view.dispatcher.UpdateListener(value, ViewMainMenuManager.MainMenuEvents.PACKAGE_MODULE, OnPackageModule);
            view.dispatcher.UpdateListener(value, ViewMainMenuManager.MainMenuEvents.IMPORT_PACKAGE, OnImportPackage);
            view.dispatcher.UpdateListener(value, ViewMainMenuManager.MainMenuEvents.PUBLISH_PACKAGE_TO_HEADSET, OnPublishPackageToHeadset);
            view.dispatcher.UpdateListener(value, ViewMainMenuManager.MainMenuEvents.QUIT_APP, OnQuitApp);

            dispatcher.UpdateListener(value, EMainEvent.HIDE_PANEL, OnHidePanel);
            dispatcher.UpdateListener(value, EMainEvent.SHOW_PANEL, OnShowPanel);
            dispatcher.UpdateListener(value, EEventToolkitCrossContext.OPEN_WORLD_REQUESTED, OnEnableButtons);
            dispatcher.UpdateListener(value, EEventToolkitCrossContext.NEW_WORLD_CREATED, OnEnableButtons);
            dispatcher.UpdateListener(value, EEventToolkitCrossContext.UPDATED_MODULE_NAME, OnUpdateModuleName);
            dispatcher.UpdateListener(value, EEventToolkitCrossContext.PACKAGING_FINISHED, OnPackagingFinished);
            dispatcher.UpdateListener(value, EEventToolkitCrossContext.IMPORTING_FINISHED, OnImportingFinished);
        }

        private void OnPackagingFinished()
        {
            view.PackagingFinished();
        }

        private void OnImportingFinished()
        {
            view.ImportingFinished();
        }

        private void OnUpdateModuleName(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;
            string name = (string)data[(byte)EParameterCodeToolkit.MODULE_NAME];

            view.UpdateModuleName(name);
        }

        private void OnPanelShown(IEvent evt)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.PANEL_SHOWN, evt.data);
        }

        private void OnPackageModule(IEvent evt)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.PACKAGE_MODULE, evt.data);

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "7");

            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, msgData);

            msgData.Parameters.Clear();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "5");

            dispatcher.Dispatch(EEventToolkitCrossContext.SHOW_PANEL, msgData);
        }

        private void OnPublishPackageToHeadset(IEvent evt)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.PUBLISH_PACKAGE_TO_HEADSET, evt.data);
        }

            private void OnImportPackage(IEvent evt)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.IMPORT_PACKAGE, evt.data);

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "7");

            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, msgData);
        }

        private void OnPanelHidden(IEvent evt)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.PANEL_HIDDEN, evt.data);
        }

        private void OnHidePanel(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            string panelId = (string)msgData[(byte)EParameterCodeToolkit.PANEL_ID];

            if (view.PanelId == panelId)
            {
                view.HidePanel();
            }
        }

        private void OnShowPanel(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            string panelId = (string)msgData[(byte)EParameterCodeToolkit.PANEL_ID];

            if (view.PanelId == panelId)
            {
                view.ShowPanel();
            }
        }

        public void OnOpenModule()
        {
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "5");

            dispatcher.Dispatch(EEventToolkitCrossContext.SHOW_PANEL, msgData);
        }

        public void OnModuleSelected(IEvent evt)
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.OPEN_WORLD_REQUESTED, evt.data);

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "7");

            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, msgData);

            msgData.Parameters.Clear();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "5");

            dispatcher.Dispatch(EEventToolkitCrossContext.SHOW_PANEL, msgData);
        }

        public void OnNewModule()
        {
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.APP_START_EVENT, false);

            dispatcher.Dispatch(EEventToolkitCrossContext.CREATE_NEW_WORLD_REQUESTED, msgData);
        }

        public void OnSaveModule()
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.SAVE_MODULE_REQUESTED);

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "7");

            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, msgData);

            msgData.Parameters.Clear();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "5");

            dispatcher.Dispatch(EEventToolkitCrossContext.SHOW_PANEL, msgData);
        }

        public void OnSaveModuleAs()
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.SAVE_MODULE_AS_REQUESTED);

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "7");

            dispatcher.Dispatch(EEventToolkitCrossContext.HIDE_PANEL, msgData);

            msgData.Parameters.Clear();
            msgData.Parameters.Add((byte)EParameterCodeToolkit.PANEL_ID, "5");

            dispatcher.Dispatch(EEventToolkitCrossContext.SHOW_PANEL, msgData);
        }

        public void OnPublishModuleToCMS()
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.PUBLISH_MODULE_REQUESTED);
        }

        public void OnUploadFiles()
        {
            dispatcher.Dispatch(EEventToolkitCrossContext.LAUNCH_BULK_RESOURCE_UPLOAD);
        }

        public void OnQuitApp()
        {
            //Check saving world etc here.
            Application.Quit();
        }

        private void OnEnableButtons()
        {
            view.EnableButtons();
        }
    }
}
