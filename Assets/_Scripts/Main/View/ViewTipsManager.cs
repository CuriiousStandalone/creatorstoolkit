﻿using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CuriiousIQ.Local.CreatorsToolkit.Main
{
    public class ViewTipsManager : EventView
    {
        [SerializeField]
        private List<GameObject> tipObjects = new List<GameObject>();

        [SerializeField]
        private GameObject textTip;
        private bool initialModuleSelected = false;

        private bool itemSelected = false;

        internal void init()
        {

        }

        public void EnableTextTip()
        {
            textTip.SetActive(true);
        }

        public void OnItemSelected()
        {
            itemSelected = true;
            DisableTips();
        }

        public void OnModuleSelected()
        {
            itemSelected = false;
            EnableTips();
        }

        public void OnPreviewToggledOn()
        {
            foreach (GameObject obj in tipObjects)
            {
                obj.SetActive(false);
            }
        }

        public void OnPreviewToggledOff()
        {
            if (!itemSelected)
            {
                EnableTips();
            }
        }

        public void DisableTips()
        {
            foreach(GameObject obj in tipObjects)
            {
                obj.SetActive(false);
            }
        }

        public void EnableTips()
        {
            if (initialModuleSelected)
            {
                foreach (GameObject obj in tipObjects)
                {
                    obj.SetActive(true);
                }
            }
            else
            {
                initialModuleSelected = true;
            }
        }
    }
}