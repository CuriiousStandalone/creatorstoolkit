﻿using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using strange.extensions.mediation.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Main
{
    public class MediatorTipsManager : EventMediator
    {
        [Inject]
        public ViewTipsManager view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            dispatcher.UpdateListener(value, EEventToolkitCrossContext.PREVIEW_MODE_DISABLED, OnPreviewToggledOff);
            dispatcher.UpdateListener(value, EEventToolkitCrossContext.PREVIEW_MODE_ENABLED, OnPreviewToggledOn);
            dispatcher.UpdateListener(value, EEventToolkitCrossContext.MODULE_SELECTED, OnModuleSelected);
            dispatcher.UpdateListener(value, EEventToolkitCrossContext.ITEM_SELECTED, OnItemSelected);

            dispatcher.UpdateListener(value, EEventToolkitCrossContext.OPEN_WORLD_REQUESTED, OnEnableTextTip);
            dispatcher.UpdateListener(value, EEventToolkitCrossContext.NEW_WORLD_CREATED, OnEnableTextTip);
        }

        private void OnEnableTextTip()
        {
            view.EnableTextTip();
        }

        private void OnPreviewToggledOn()
        {
            view.OnPreviewToggledOn();
        }

        private void OnPreviewToggledOff()
        {
            view.OnPreviewToggledOn();
        }

        private void OnModuleSelected()
        {
            view.OnModuleSelected();
        }

        private void OnItemSelected()
        {
            view.OnItemSelected();
        }
    }
}