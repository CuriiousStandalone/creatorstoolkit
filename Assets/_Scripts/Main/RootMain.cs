﻿using strange.extensions.context.impl;

namespace CuriiousIQ.Local.CreatorsToolkit.Main
{
    public class RootMain : ContextView
    {
        private void Awake()
        {
            context = new ContextMain(this);
        }
    }
}
