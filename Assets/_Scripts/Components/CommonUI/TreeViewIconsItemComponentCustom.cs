using PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents;

using TMPro;

using UIWidgets;

using UnityEngine;
using UnityEngine.UI;

namespace CuriiousIQ.Local.CreatorsToolkit.Components.CommonUI
{
    //copy class
    public class TreeViewIconsItemComponentCustom : ListViewItem, IViewData<ListViewIconsItemDescription>, IViewData<CTreeViewItem>
    {
        /// <summary>
        /// Gets the objects to resize.
        /// </summary>
        /// <value>The objects to resize.</value>
        public GameObject[] ObjectsToResize
        {
            get { return new GameObject[] {Icon.transform.parent.gameObject, Text.gameObject,}; }
        }

        /// <summary>
        /// The icon.
        /// </summary>
        [SerializeField]
        public Image Icon;

        /// <summary>
        /// The text.
        /// </summary>
        [SerializeField]
        public TextMeshProUGUI Text;

        /// <summary>
        /// Set icon native size.
        /// </summary>
        public bool SetNativeSize = true;

        /// <summary>
        /// Gets the current item.
        /// </summary>
        public ListViewIconsItemDescription Item { get; protected set; }

        /// <summary>
        /// Sets component data with specified item.
        /// </summary>
        /// <param name="item">Item.</param>
        public virtual void SetData(ListViewIconsItemDescription item)
        {
            Item = item;
            if (Item == null)
            {
                if (Icon != null)
                {
                    Icon.sprite = null;
                }

                if (Text != null)
                {
                    Text.text = string.Empty;
                }
            }
            else
            {
                if (Icon != null)
                {
                    Icon.sprite = Item.Icon;
                }

                if (Text != null)
                {
                    Text.text = Item.LocalizedName ?? Item.Name;
                }
            }

            if (Icon != null)
            {
                if (SetNativeSize)
                {
                    Icon.SetNativeSize();
                }

                // set transparent color if no icon
                Icon.color = (Icon.sprite == null) ? Color.clear : Color.white;
            }
        }

        /// <summary>
        /// Sets component data with specified item.
        /// </summary>
        /// <param name="item">Item.</param>
        public virtual void SetData(CTreeViewItem item)
        {
            SetData(new ListViewIconsItemDescription()
            {
                Name = item.Name,
                LocalizedName = item.LocalizedName,
                Icon = item.Icon,
                Value = item.Value,
            });
        }
    }
}