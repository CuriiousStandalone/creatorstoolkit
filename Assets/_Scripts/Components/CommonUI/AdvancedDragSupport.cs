﻿using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

using UIWidgets;

namespace CuriiousIQ.Local.CreatorsToolkit.Components.CommonUI
{
    public abstract class AdvancedDragSupport<T> : DragSupport<T>
    {
        public UnityEvent OnFailedDrop;

        /// <summary>
        /// When dragging is occurring this will be called every time the cursor is moved.
        /// </summary>
        /// <param name="eventData">Current event data.</param>
        public override void OnDrag(PointerEventData eventData)
        {
            if (!IsDragged)
            {
                return;
            }

            if (currentTarget != null)
            {
                oldTarget = currentTarget;
            }

            currentTarget = FindTarget(eventData);
            if ((oldTarget != null) && (currentTarget == null || (currentTarget != oldTarget)))
            {
                oldTarget.DropCanceled(Data, eventData);
            }

            Vector2 point;
            if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(CanvasTransform as RectTransform, Input.mousePosition, eventData.pressEventCamera, out point))
            {
                return;
            }

            DragPoint.localPosition = point;
        }

        public override void Dropped(bool success)
        {
            if (!success)
            {
                OnFailedDrop?.Invoke();
            }
            base.Dropped(success);
            oldTarget = null;
            currentTarget = null;
        }

        private readonly List<RaycastResult> _raycastResults = new List<RaycastResult>();

        /// <summary>
        /// Finds the target.
        /// </summary>
        /// <returns>The target.</returns>
        /// <param name="eventData">Event data.</param>
        protected override IDropSupport<T> FindTarget(PointerEventData eventData)
        {
            _raycastResults.Clear();

            EventSystem.current.RaycastAll(eventData, _raycastResults);

            foreach (var raycastResult in _raycastResults)
            {
                if (!raycastResult.isValid)
                {
                    continue;
                }

#if UNITY_5_0 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_3_OR_NEWER
                var target = raycastResult.gameObject.GetComponent<IDropSupport<T>>();
#else
				var target = raycastResult.gameObject.GetComponent(typeof(IDropSupport<T>)) as IDropSupport<T>;
#endif
                if (target != null)
                {
                    target.CanReceiveDrop(Data, eventData);
                    return target;
                }
            }

            return null;
        }
    }
}