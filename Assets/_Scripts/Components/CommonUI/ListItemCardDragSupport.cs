﻿using PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents;

using UnityEngine;
using UnityEngine.EventSystems;

namespace CuriiousIQ.Local.CreatorsToolkit.Components.CommonUI
{
    /// <summary>
    /// Enables Dragging for ListCardItems.
    /// </summary>
    [RequireComponent(typeof(ListItemCardComponent))]
    public class ListItemCardDragSupport : AdvancedListViewCustomDragSupport<CardItemList, ListItemCardComponent, ListItemCardData>
    {
        public float DragIconScale = 0.5f;
        private GameObject _dragIcon;

        protected override ListItemCardData GetData(ListItemCardComponent component)
        {
            return component.Item;
        }

        protected override void SetDragInfoData(ListItemCardData data)
        {
            DragInfo.SetData(data);
        }

        public override bool CanDrag(PointerEventData eventData)
        {
            return GetComponent<ListItemCardComponent>().Item.ResourceType == ListCardResourceTypes.PLATFORM || GetComponent<ListItemCardComponent>().Item.ResourceType == ListCardResourceTypes.HOTSPOT;
        }

        protected override void InitDrag(PointerEventData eventData)
        {
            _dragIcon = Instantiate(gameObject, transform);
            DragInfo = _dragIcon.GetComponent<ListItemCardComponent>();
            _dragIcon.transform.localScale = Vector3.one * DragIconScale;
            _dragIcon.name = $"Drag Icon for {gameObject.name}";
            _dragIcon.SetActive(false);
            base.InitDrag(eventData);
        }

        public override void Dropped(bool success)
        {
            Destroy(_dragIcon);
            Destroy(DragPoint.gameObject);
            base.Dropped(success);
        }
    }
}