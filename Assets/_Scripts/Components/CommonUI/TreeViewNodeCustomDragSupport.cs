using UnityEngine;
using UnityEngine.EventSystems;

using PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents;
using UnityEngine.UI;

namespace CuriiousIQ.Local.CreatorsToolkit.Components.CommonUI
{
    public class TreeViewNodeCustomDragSupport : TreeViewCustomDragDropSupport<TreeViewCustomComponent, TreeViewCustomComponent, CTreeViewItem>
    {
        public float DragIconScale = 1.5f;

        private GameObject _dragIcon;

        protected override void InitDrag(PointerEventData eventData)
        {
            _dragIcon = Instantiate(gameObject, transform);
            DragInfo = _dragIcon.GetComponent<TreeViewCustomComponent>();
            DragInfo.TextTMPro.transform.localScale *= DragIconScale;
            _dragIcon.name = $"Drag Icon for {gameObject.name}";
            _dragIcon.SetActive(true);
            base.InitDrag(eventData);
            _dragIcon.GetComponent<Image>().enabled = false;
            _dragIcon.transform.Find("Toggle").gameObject.SetActive(false);
            _dragIcon.transform.Find("Text").gameObject.GetComponent<TMPro.TMP_Text>().raycastTarget = false;
        }

        public override void Dropped(bool success)
        {
            Destroy(_dragIcon);
            Destroy(DragPoint.gameObject);
            base.Dropped(success);
        }
    }
}