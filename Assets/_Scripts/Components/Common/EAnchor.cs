﻿namespace CuriiousIQ.Local.CreatorsToolkit.Components.Common
{
    public enum EAnchor
    {
        TOP,
        RIGHT,
        BOTTOM,
        LEFT,
    }
}
