﻿using System.IO;

using PulseIQ.Local.Common;

namespace CuriiousIQ.Local.CreatorsToolkit.Components.Common
{
    public static class CreatorToolkitSettings
    {
        //private static readonly string _clientName = "/CreatorToolkit";

        //private static readonly string _localRawResourceDirectory = "UseableFiles/RawResources";
        //private static readonly string _localPlatformResourcesDirectory = "C:/ThePulseIQ" + _clientName + "/PackageData/Cache/ResourceData/Windows";
        //private static readonly string _localPlatformResourcesDirectoryAnd = "C:/ThePulseIQ" + _clientName + "/PackageData/Cache/ResourceData/Android";
        //private static readonly string _localMultimediaPlatformResourcesDirectory = "C:/ThePulseIQ" + _clientName + "/PackageData/Cache/ResourceData/MediaData";
        //private static readonly string _localTemplateDirectory = "C:/ThePulseIQ" + _clientName + "/PackageData/Cache/Templates";
        //private static readonly string _localTemplateThumbnailDirectory = "C:/ThePulseIQ" + _clientName + "/PackageData/Cache/Templates/Thumbnail";
        //private static readonly string _localThumbnailDirectory = "C:/ThePulseIQ" + _clientName + "/PackageData/Cache/ResourceData/Thumbnail";
        //private static readonly string _localWorldDirectory = "C:/ThePulseIQ" + _clientName + "/PackageData/Cache/Worlds";
        //private static readonly string _localWorldBackupDirectory = "C:/ThePulseIQ/CreatorToolkit/Cache/Worlds";
        //private static readonly string _localWorldThumbnailDirectory = "C:/ThePulseIQ" + _clientName + "/PackageData/Cache/Worlds/Thumbnail";

        //private static readonly string _localBaseItemDirectory = "1_Item";
        //private static readonly string _localInteractiveItemDirectory = "2_Interactive";
        //private static readonly string _localSlotItemDirectory = "3_Slot";
        //private static readonly string _localPanoramaImageDirectory = "4_Panorama";
        //private static readonly string _localPanoramaVideoDirectory = "5_PanoramaVideo";
        //private static readonly string _localPlanarVideoDirectory = "6_PlanarVideo";
        //private static readonly string _localTimerItemDirectory = "10_SyncTimer";
        //private static readonly string _localTutorialItemDirectory = "12_Tutorial";
        //private static readonly string _localPostprocessingDirectory = "13_Postprocessing";
        //private static readonly string _localSkyboxDirectory = "14_Skybox";
        //private static readonly string _localUIDirectory = "15_UI";
        //private static readonly string _localStereoPanoramaVideoDirectory = "17_StereoPanoramaVideo";
        //private static readonly string _localAudioDirectory = "20_Audio";
        //private static readonly string _localImageDirectory = "21_Image";

        //private static readonly float _moduleUploadLimit = 200; // limit in MB

        //private static readonly string _tempUploadPath = "TempUpload";

        //public static string LocalRawResourceDirectory
        //{
        //    get { return _localRawResourceDirectory; }
        //}

        //public static string LocalPlatformResourcesDirectory
        //{
        //    get { return _localPlatformResourcesDirectory; }
        //}

        //public static string LocalMultimediaPlatformResourcesDirectory
        //{
        //    get { return _localMultimediaPlatformResourcesDirectory; }
        //}

        //public static string TempUploadPath
        //{
        //    get { return _tempUploadPath; }
        //}

        //public static string LocalTemplateDirectory
        //{
        //    get { return _localTemplateDirectory; }
        //}

        //public static string LocalTemplateThumbnailDirectory
        //{
        //    get { return _localTemplateThumbnailDirectory; }
        //}

        //public static string LocalWorldBackupDirectory
        //{
        //    get { return _localWorldBackupDirectory; }
        //}

        //public static string GetPathByItemType(ItemTypeCode itemType, string platformType = "windows")
        //{
        //    switch (itemType)
        //    {
        //        default:
        //        case ItemTypeCode.ITEM:
        //            return (platformType == "windows") ? Path.Combine(_localPlatformResourcesDirectory, _localBaseItemDirectory) : Path.Combine(_localPlatformResourcesDirectoryAnd, _localBaseItemDirectory);

        //        case ItemTypeCode.INTERACTIVE_ITEM:
        //            return (platformType == "windows") ? Path.Combine(_localPlatformResourcesDirectory, _localInteractiveItemDirectory) : Path.Combine(_localPlatformResourcesDirectoryAnd, _localInteractiveItemDirectory);

        //        case ItemTypeCode.SLOT:
        //            return (platformType == "windows") ? Path.Combine(_localPlatformResourcesDirectory, _localSlotItemDirectory) : Path.Combine(_localPlatformResourcesDirectoryAnd, _localSlotItemDirectory);

        //        case ItemTypeCode.PANORAMA:
        //            return Path.Combine(_localMultimediaPlatformResourcesDirectory, _localPanoramaImageDirectory);

        //        case ItemTypeCode.PANORAMA_VIDEO:
        //            return Path.Combine(_localMultimediaPlatformResourcesDirectory, _localPanoramaVideoDirectory);

        //        case ItemTypeCode.PLANAR_VIDEO:
        //            return Path.Combine(_localMultimediaPlatformResourcesDirectory, _localPlanarVideoDirectory);

        //        case ItemTypeCode.SYNCTIMER:
        //            return (platformType == "windows") ? Path.Combine(_localPlatformResourcesDirectory, _localTimerItemDirectory) : Path.Combine(_localPlatformResourcesDirectoryAnd, _localTimerItemDirectory);

        //        case ItemTypeCode.TUTORIAL_ITEM:
        //            return (platformType == "windows") ? Path.Combine(_localPlatformResourcesDirectory, _localTutorialItemDirectory) : Path.Combine(_localPlatformResourcesDirectoryAnd, _localTutorialItemDirectory);

        //        case ItemTypeCode.POST_PROCESSING_PROFILE:
        //            return (platformType == "windows") ? Path.Combine(_localPlatformResourcesDirectory, _localPostprocessingDirectory) : Path.Combine(_localPlatformResourcesDirectoryAnd, _localPostprocessingDirectory);

        //        case ItemTypeCode.SKYBOX:
        //            return (platformType == "windows") ? Path.Combine(_localPlatformResourcesDirectory, _localSkyboxDirectory) : Path.Combine(_localPlatformResourcesDirectoryAnd, _localSkyboxDirectory);

        //        case ItemTypeCode.UI:
        //            return (platformType == "windows") ? Path.Combine(_localPlatformResourcesDirectory, _localUIDirectory) : Path.Combine(_localPlatformResourcesDirectoryAnd, _localUIDirectory);

        //        case ItemTypeCode.STEREO_PANORAMA_VIDEO:
        //            return Path.Combine(_localMultimediaPlatformResourcesDirectory, _localStereoPanoramaVideoDirectory);

        //        case ItemTypeCode.AUDIO:
        //            return Path.Combine(_localMultimediaPlatformResourcesDirectory, _localAudioDirectory);

        //        case ItemTypeCode.IMAGE:
        //            return Path.Combine(_localMultimediaPlatformResourcesDirectory, _localImageDirectory);
        //    }
        //}

        //public static string GetThumbnailPathByItemType(ItemTypeCode itemType)
        //{
        //    switch (itemType)
        //    {
        //        default:
        //        case ItemTypeCode.ITEM:
        //            return Path.Combine(_localThumbnailDirectory, _localBaseItemDirectory);
        //        case ItemTypeCode.INTERACTIVE_ITEM:
        //            return Path.Combine(_localThumbnailDirectory, _localInteractiveItemDirectory);
        //        case ItemTypeCode.SLOT:
        //            return Path.Combine(_localThumbnailDirectory, _localSlotItemDirectory);
        //        case ItemTypeCode.PANORAMA:
        //            return Path.Combine(_localThumbnailDirectory, _localPanoramaImageDirectory);
        //        case ItemTypeCode.PANORAMA_VIDEO:
        //            return Path.Combine(_localThumbnailDirectory, _localPanoramaVideoDirectory);
        //        case ItemTypeCode.PLANAR_VIDEO:
        //            return Path.Combine(_localThumbnailDirectory, _localPlanarVideoDirectory);
        //        case ItemTypeCode.SYNCTIMER:
        //            return Path.Combine(_localThumbnailDirectory, _localTimerItemDirectory);
        //        case ItemTypeCode.TUTORIAL_ITEM:
        //            return Path.Combine(_localThumbnailDirectory, _localTutorialItemDirectory);
        //        case ItemTypeCode.POST_PROCESSING_PROFILE:
        //            return Path.Combine(_localThumbnailDirectory, _localPostprocessingDirectory);
        //        case ItemTypeCode.SKYBOX:
        //            return Path.Combine(_localThumbnailDirectory, _localSkyboxDirectory);
        //        case ItemTypeCode.UI:
        //            return Path.Combine(_localThumbnailDirectory, _localUIDirectory);
        //        case ItemTypeCode.STEREO_PANORAMA_VIDEO:
        //            return Path.Combine(_localThumbnailDirectory, _localStereoPanoramaVideoDirectory);
        //        case ItemTypeCode.AUDIO:
        //            return Path.Combine(_localThumbnailDirectory, _localAudioDirectory);
        //        case ItemTypeCode.IMAGE:
        //            return Path.Combine(_localThumbnailDirectory, _localImageDirectory);
        //    }
        //}

        //public static string GetTemplateFile(int itemType = 0)
        //{
        //    if (itemType == 0)
        //    {
        //        return Path.Combine(_localTemplateDirectory, "Template_Interactive.ctk");
        //    }
        //    else
        //    {
        //        return Path.Combine(_localTemplateDirectory, "Template_" + itemType + ".ctk");
        //    }
        //}

        //public static string LocalWorldDirectory
        //{
        //    get { return _localWorldDirectory;  }
        //}

        //public static string LocalWorldThumbnailDirectory
        //{
        //    get { return _localWorldThumbnailDirectory; }
        //}

        //public static float ModuleUploadLimit
        //{
        //    get { return _moduleUploadLimit; }
        //}
    }
}