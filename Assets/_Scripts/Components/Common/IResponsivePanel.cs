﻿namespace CuriiousIQ.Local.CreatorsToolkit.Components.Common
{
    public interface IResponsivePanel
    {
        string PanelId { get; }

        void HidePanel();
        void ShowPanel();

        void ShowPanelWithTween();
        void HidePanelWithTween();

        void IncreaseHeight(float height);
        void IncreaseHeightFromBottom(float height);
        void IncreaseHeightFromTop(float height);

        void DecreaseHeight(float height);
        void DecreaseHeightFromBottom(float height);
        void DecreaseHeightFromTop(float height);

        void IncreaseWidth(float width);
        void IncreaseWidthFromRight(float width);
        void IncreaseWidthFromLeft(float width);

        void DecreaseWidth(float width);
        void DecreaseWidthFromRight(float width);
        void DecreaseWidthFromLeft(float width);

        void RepositionToTop(float height);
        void RepositionToBottom(float height);
        void RepositionToLeft(float width);
        void RepositionToRight(float width);
    }
}
