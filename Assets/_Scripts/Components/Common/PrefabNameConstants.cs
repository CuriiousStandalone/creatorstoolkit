﻿namespace CuriiousIQ.Local.CreatorsToolkit.Components.Common
{
    public static class PrefabNameConstants
    {
        #region Hotspot

        public static string HOTSPOT = "Prefabs/Hotspot/Hotspot";
        public static string HOTSPOT_BOX = "Prefabs/HotspotBox";
        public static string HOTSPOT_TEXT_ELEMENT = "Prefabs/Hotspot/HotspotTextElement";
        public static string HOTSPOT_IMAGE_ELEMENT = "Prefabs/Hotspot/HotspotImageElement";
        public static string HOTSPOT_AUDIO_ELEMENT = "Prefabs/Hotspot/HotspotAudioElement";
        public static string HOTSPOT_ICON = "Images/HotspotStatusIcons/StatusIcon";

        #endregion

        #region Font

        public static string GILROY_FONT_PREFIX = "Font/Gilroy/";

        #endregion
    }
}