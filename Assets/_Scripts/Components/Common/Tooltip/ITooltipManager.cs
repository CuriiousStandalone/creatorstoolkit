﻿namespace CuriiousIQ.Local.CreatorsToolkit.Components.Common.Tooltip
{
    public interface ITooltipManager
    {
        int ID { get; set; }

        string TooltipText { get; set; }
    }
}
