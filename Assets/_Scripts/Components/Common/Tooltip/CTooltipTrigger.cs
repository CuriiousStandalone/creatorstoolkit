﻿using UnityEngine;
using UnityEngine.EventSystems;

using Unitycoding.UIWidgets;

namespace CuriiousIQ.Local.CreatorsToolkit.Components.Common.Tooltip
{
    public class CTooltipTrigger : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        /// <summary>
        /// The name of the tooltip instance.
        /// </summary>
        [SerializeField]
        private string instanceName = "Tooltip";
        /// <summary>
        /// Color of the text.
        /// </summary>
        [SerializeField]
        private Color color = Color.white;
        /// <summary>
        /// The text to display
        /// </summary>
        [TextArea]
        private string tooltip;

        private Unitycoding.UIWidgets.Tooltip instance;


        /// <summary>
        /// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
        /// </summary>
        private void Start()
        {
            //Find tooltip instance with name "Tooltip"
            instance = UIUtility.Find<Unitycoding.UIWidgets.Tooltip>(instanceName);
            //Check if an instance of UITooltip is located in scene
            if (enabled && instance == null)
            {
                //No instance -> disable trigger
                enabled = false;
            }
        }

        /// <summary>
        /// Called when the mouse pointer starts hovering the ui element.
        /// </summary>
        /// <param name="eventData">Event data.</param>
        public void OnPointerEnter(PointerEventData eventData)
        {
            //Show tooltip
            tooltip = gameObject.GetComponent<ITooltipManager>().TooltipText;
            instance.Show(UIUtility.ColorString(tooltip, color), Input.mousePosition);
        }

        /// <summary>
        /// Called when the mouse pointer exits the element
        /// </summary>
        /// <param name="eventData">Event data.</param>
        public void OnPointerExit(PointerEventData eventData)
        {
            //Hide tooltip
            instance.Close();
        }

        /// <summary>
        /// Called when the mouse enters a 3d model with a collider in scene.
        /// </summary>
        private void OnMouseEnter()
        {
            //Show tooltip
            tooltip = gameObject.GetComponent<ITooltipManager>().TooltipText;
            instance.Show(UIUtility.ColorString(tooltip, color), Input.mousePosition);
        }

        /// <summary>
        /// Called when the mouse pointer exits a 3d model with a collider in scene
        /// </summary>
        private void OnMouseExit()
        {
            //Hide tooltip
            instance.Close();
        }
    }
}