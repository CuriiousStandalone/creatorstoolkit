﻿namespace CuriiousIQ.Local.CreatorsToolkit.Components.Common
{
    public enum EParameterCodeToolkit
    {
        GENERIC_MSG,

        PANEL_ID,

        ITEM_NAME,
        ITEM_INDEX,
        ITEM_ID,
        ITEM_DATA,
        ITEM_POSITION,
        ITEM_ROTATION,
        ITEM_SCALE,
        ITEM_ENABLED_BY_DEFAULT,
        ITEM_TYPE,

        WORLD_ID,
        WORLD_DATA,
        WORLD_PATH,

        HOTSPOT_NAME,
        HOTSPOT_ID,
        HOTSPOT_INDEX,
        HOTSPOT_DATA,//CTData
        HOTSPOT_IS_ACTIVE,
        HOTSPOT_DEACTIVATE_TIME,
        HOTSPOT_ACTIVATE_TIME,
        HOTSPOT_TRIGGER_STATE,
        HOTSPOT_TRIGGER_EVENT,
        HOTSPOT_XY_POSITION,
        HOTSPOT_ELEMENT_ID,
        HOTSPOT_ELEMENT_DATA,//TemplateData
        HOTSPOT_STATUS_TYPE,
        HOTSPOT_ICON_ID,
        HOTSPOT_OVERRIDE,

        STEREO_VIDEO_TYPE,
        PANORAMA_TELEPORT_TIME,
        PANORAMA_AUTO_TELEPORT,
        MEDIA_END_BEHAVIOUR,
        MEDIA_TELEPORT_ITEM,
        MEDIA_VIDEO_CURRENT_TIME,

        PANEL_HEIGHT,
        PANEL_WIDTH,
        PANEL_ANCHOR,
        MOUSE_COORDS,
        RESOURCE_ID,
        PROGRESS_BAR_VALUE,

        APP_START_EVENT,

        MODULE_NAME,
        MODULE_DESCRIPTION,
        MODULE_IS_MULTIPLAYER,

        TEMPLATE_ID,

        ELEMENT_TEXT,
        ELEMENT_MEDIA_ID,

        RESOURCE_LIST_FILTER,
        IMPORTED_ITEMS_PATH_LIST,

        TOOLTIP_ID,
        TOOLTIP_TEXT,
        PACKAGE_PATH,
        THUMBNAIL_ID,
        IS_HOTSPOT_PANEL,
        ELEMENT_TYPE,
    }
}
