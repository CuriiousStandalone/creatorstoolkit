﻿namespace CuriiousIQ.Local.CreatorsToolkit.Components.Common
{
    public enum EEffect
    {
        NONE,
        RESIZE,
        VISIBILITY,
        POSITION,
    }
}
