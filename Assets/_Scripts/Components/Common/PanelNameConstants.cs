﻿namespace CuriiousIQ.Local.CreatorsToolkit.Components.Common
{
    public static class PanelNameConstants
    {
        public const string TOP_BAR = "1";
        public const string RIGHT_BAR = "2";
        public const string LEFT_BAR = "3";
        public const string RESOURCE_MANAGER_PANEL = "4";
        public const string WORLD_HIERARCHY_PANEL = "5";
        public const string ITEM_INSPECTOR_PANEL = "6";
        public const string MAIN_MENU = "7";
        public const string MODULES_PANEL = "8";
    }
}
