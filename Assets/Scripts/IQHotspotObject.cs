﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using UnityEngine.XR.Interaction.Toolkit;
using TMPro;
using UnityEngine.Networking;

public class IQHotspotObject : MonoBehaviour
{

    public Canvas hotspotCanvas;

    public Sprite defaultSprite;
    public Sprite hoverSprite;
    public Sprite triggerSprite;
    public Image hotspotImage;
    public float activeTime;
    public float deactiveTime;
    public int targetItemIndex;

    public CanvasGroup contentCanvasGroup;

    //Gameobjects for the different content typesand of
    public GameObject textContentGameObject;
    public GameObject infoPanelContentGameObject;
    public GameObject imageContentGameObject;

    //Now the key objects for each panel type
    public TMP_Text textPanelText;
    public TMP_Text infoPanelTextHeader;
    public TMP_Text infoPanelTextBody;
    public GameObject infoPanelImageGameObject;
    public RawImage infoPanelRawImage;
    public RawImage imagePanelRawImage;

    public float maxImagePanelSize = 3000; //Biggest image panel can be as a square.

    //public GameObject textContainer;
    //public GameObject imageContainer;
    //public TMP_Text titleText;
    //public TMP_Text descriptionText;
    //public RawImage mediaImage;

    //IQController iqController;

    Button button;
    


    // Start is called before the first frame update
    void Start()
    {



        //iqController = GameObject.FindObjectOfType<IQController>();
        //hotspotCanvas.worldCamera = FindObjectOfType<XRRig>().cameraGameObject.GetComponent<Camera>();

        


        button = hotspotImage.GetComponent<Button>();


        //Set the sprite graphics
        hotspotImage.sprite = defaultSprite;
        SpriteState sprState = new SpriteState();
        sprState.highlightedSprite = hoverSprite;
        sprState.pressedSprite = triggerSprite;

        button.spriteState = sprState;

        //Set the button behaviour
        button.onClick.AddListener(delegate { Select(); });



    }


    public void Select()
    {
        Debug.Log("I am a hotspot (" + this.gameObject.name + ") and I have been selected");

        //We do behaviours according to if we're a media panel or not.
        //if (targetItemIndex > 0)
        //{
        //    //We're a teleport hotspot
        //    StartCoroutine(FindObjectOfType<IQController>().DisplayItemIndex(targetItemIndex));
        //}
        //else
        //{
        //    //We're a multimedia hotspot.
        //
        //    //Check if we're visible or not first
        //    if (contentCanvasGroup.alpha > 0.0f)
        //    {
        //        //We're visible, so fade down
        //        LeanTween.alphaCanvas(contentCanvasGroup, 0.0f, 0.3f).setEaseInCirc();
        //    }
        //    else
        //    {
        //        //We're off, so fade up.
        //        LeanTween.alphaCanvas(contentCanvasGroup, 1.0f, 0.5f).setEaseOutCirc();
        //
        //    }
        //}

    }

    public void SetupTeleporter()
    {
        //Set up the hotspot to be a teleporter.
        //Just turn off the content group and we're all sorted.
        contentCanvasGroup.gameObject.SetActive(false);

    }

    public void SetupTextContent(string titleText)
    {
        //Set up the text label.

        //Set the appropriate panels to be visible.
        contentCanvasGroup.gameObject.SetActive(true);
        textContentGameObject.SetActive(true);
        infoPanelContentGameObject.SetActive(false);
        imageContentGameObject.SetActive(false);

        //Populate the text.
        textPanelText.text = titleText;

        //And turn off the canvas group for the default state
        contentCanvasGroup.alpha = 0.0f;

    }

    public void SetupInfoPanelContent(string titleText, string descriptionText, Texture2D imageTexture2D = null)
    {
        //Set up the info panel. It can be with or without an image.

        //Set the appropriate panels to be visible.
        contentCanvasGroup.gameObject.SetActive(true);
        textContentGameObject.SetActive(false);
        infoPanelContentGameObject.SetActive(true);
        imageContentGameObject.SetActive(false);

        //Set the text fields
        infoPanelTextHeader.text = titleText;
        infoPanelTextBody.text = descriptionText;

        //If there's an image, set that up.
        if (imageTexture2D)
        {
            infoPanelImageGameObject.SetActive(true);
            infoPanelRawImage.texture = imageTexture2D;

            //Get the width of the imageGameobject so we can scale to that. (it should always be the same, but just in case)
            float imagePanelWidth = infoPanelImageGameObject.GetComponent<RectTransform>().rect.width;

            //Determine if the image is portrait or landscape
            if (imageTexture2D.width >= imageTexture2D.height)
            {
                //Landscape or square. This means the height must equal imagePanelWidth.
                //Find the multiplier to make that happen.
                float sizeMultiplier = imagePanelWidth / imageTexture2D.height;

                //Set the rect of the raw image appropriately.
                RectTransform rt = infoPanelRawImage.GetComponent<RectTransform>();
                rt.sizeDelta = new Vector2(imageTexture2D.width * sizeMultiplier, imagePanelWidth);

            }
            else
            {
                //Portrait. This means the width must equal imagePanelWidth.
                //Find the multiplier to make that happen.
                float sizeMultiplier = imagePanelWidth / imageTexture2D.width;

                //Set the rect of the raw image appropriately.
                RectTransform rt = infoPanelRawImage.GetComponent<RectTransform>();
                rt.sizeDelta = new Vector2(imagePanelWidth, imageTexture2D.height * sizeMultiplier);

            }

        }
        else
        {
            infoPanelImageGameObject.SetActive(false);
        }

        //And turn off the canvas group for the default state
        contentCanvasGroup.alpha = 0.0f;

    }

    public void SetupImageContent(Texture2D imageTexture2D)
    {
        //Set the image type hotspot

        //Set the appropriate panels to be visible.
        contentCanvasGroup.gameObject.SetActive(true);
        textContentGameObject.SetActive(false);
        infoPanelContentGameObject.SetActive(false);
        imageContentGameObject.SetActive(true);

        //Now set up the image.
        imagePanelRawImage.texture = imageTexture2D;

        //Set the dimensions so that its scale is the maximum allowed.
        //Determine if the image is portrait or landscape

        Vector2 rectTransformVector;

        if (imageTexture2D.width >= imageTexture2D.height)
        {
            //Landscape or square. 

            //Find the multiplier to make that happen.
            float sizeMultiplier = maxImagePanelSize / imageTexture2D.width;
            rectTransformVector = new Vector2( maxImagePanelSize, imageTexture2D.height * sizeMultiplier);

        }
        else
        {
            //Portrait.
            //Find the multiplier to make that happen.
            float sizeMultiplier = maxImagePanelSize / imageTexture2D.height;
            rectTransformVector = new Vector2( imageTexture2D.width * sizeMultiplier, maxImagePanelSize);
        }

        //Apply the sizing to both the container gameobject, and the rawimage
        imageContentGameObject.GetComponent<RectTransform>().sizeDelta = rectTransformVector;
        imagePanelRawImage.GetComponent<RectTransform>().sizeDelta = rectTransformVector;

        //And turn off the canvas group for the default state
        contentCanvasGroup.alpha = 0.0f;

    }

    IEnumerator RevealHotspot(int delayTime)
    {

        
        //print("Start time " + Time.time);
        yield return new WaitForSeconds(delayTime);
        print("Revealed Hotspot: " + gameObject.name + " after " + Time.time + " seconds");
        GetComponent<BoxCollider>().enabled = true;
        hotspotImage.enabled = true;
        

    }


}
