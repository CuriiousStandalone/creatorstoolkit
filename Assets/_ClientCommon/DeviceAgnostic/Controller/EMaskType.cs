﻿using UnityEngine;

namespace PulseIQ.Local.DeviceAgnostic
{
    public enum EMaskType : byte
    {
        DEFAULT,
        LOADING,
        MULTIMEDIA,
    }
}