﻿namespace PulseIQ.Local.DeviceAgnostic
{
    public interface IAgnosticController 
    {
        AgnosticController GetControllerType();

        void OnClicked();

        void OnRecalibrated();
    }
}