﻿using PulseIQ.Local.ClientCommon.BaseItem;
using UnityEngine;
using UnityEngine.XR;

namespace PulseIQ.Local.DeviceAgnostic
{
    public class AgnosticDisplayDevice : MonoBehaviour, IAgnosticDisplayDevice
    {

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public AgnosticDisplayDevice GetDisplayDeviceType()
        {
            return this;
        }

        public virtual void Raycast(ViewItemBase item)
        {
            // raycast logic
        }

        public virtual void Recalibrate()
        {

        }

        public virtual void SetDispayMode(EClientMode mode)
        {

        }

        protected virtual void DefaultMode()
        {

        }

        protected virtual void MultimediaMode()
        {

        }

        protected virtual void LoadingMode()
        {

        }

        protected void FadeInOut(float time, Color color)
        {

        }

        protected void SetMask(EMaskType type)
        {
            switch (type)
            {
                case EMaskType.DEFAULT:
                    {
                        break;
                    }

                case EMaskType.MULTIMEDIA:
                    {
                        break;
                    }

                case EMaskType.LOADING:
                    {
                        break;
                    }
            }
            
        }
    }
}