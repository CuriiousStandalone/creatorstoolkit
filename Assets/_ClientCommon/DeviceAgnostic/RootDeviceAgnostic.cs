﻿using strange.extensions.context.impl;

namespace PulseIQ.Local.DeviceAgnostic
{
    public class RootDeviceAgnostic : ContextView
    {
        private void Awake()
        {
            context = new ContextDeviceAgnostic(this);
        }
    }
}