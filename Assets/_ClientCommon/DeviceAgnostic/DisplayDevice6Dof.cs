﻿using UnityEngine;
using UnityEngine.XR;

namespace PulseIQ.Local.DeviceAgnostic
{
    public class DisplayDevice6Dof : AgnosticDisplayDevice
    {
        public void EnableGazeLaser(bool value)
        {

        }

        protected override void MultimediaMode()
        {

        }

        protected override void LoadingMode()
        {

        }

        public override void Recalibrate()
        {
            InputTracking.Recenter();
        }

        protected void LockTranslation(bool value)
        {
            InputTracking.disablePositionalTracking = value;
        }
    }
}