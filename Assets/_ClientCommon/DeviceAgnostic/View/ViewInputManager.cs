﻿using UnityEngine;

namespace PulseIQ.Local.DeviceAgnostic
{
    public class ViewInputManager : MonoBehaviour
    {
        private void Update()
        {
            if (Input.GetKey("CLICKED"))
            {
                DeviceAgnosticEventManager.CLICKED.Invoke();
            }

            if (Input.GetKey("RECALIBRATED"))
            {
                DeviceAgnosticEventManager.RECALIBRATED.Invoke();
            }


        }
    }
}