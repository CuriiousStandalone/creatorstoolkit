﻿using strange.extensions.mediation.impl;

namespace PulseIQ.Local.DeviceAgnostic
{
    public class MediatorControllerGearVRTouchPad : EventMediator
    {
        public ViewControllerGearVRTouchPad view { get; set; }

        public override void OnRegister()
        {
            view.init();
            UpdateListeners(true);
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {

        }
    }
}