﻿using strange.extensions.mediation.impl;

namespace PulseIQ.Local.DeviceAgnostic
{
    public class MediatorHmdGearVR : EventMediator
    {
        public ViewHmdGearVR view { get; set; }

        public override void OnRegister()
        {
            view.init();
            UpdateListeners(true);
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {

        }
    }
}