﻿
using UnityEngine.Events;

namespace PulseIQ.Local.DeviceAgnostic
{
    public static class DeviceAgnosticEventManager
    {
        public static UnityEvent CLICKED;
        public static UnityEvent RECALIBRATED;
    }
}