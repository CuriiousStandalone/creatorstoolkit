﻿using UnityEngine;

namespace PulseIQ.Local.DeviceAgnostic
{
    public class AgnosticController : MonoBehaviour, IAgnosticController
    {
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public AgnosticController GetControllerType()
        {
            return this;
        }

        public void OnClicked()
        {

        }

        public void OnRecalibrated()
        {

        }
    }
}