﻿using PulseIQ.Local.ClientCommon.BaseItem;
using UnityEngine;

namespace PulseIQ.Local.DeviceAgnostic
{
    public class AgnosticDevice : IAgnosticDevice
    {
        private IAgnosticController agnosticController;
        private IAgnosticDisplayDevice agnosticDisplay;

        public void OnSelect(ViewItemBase item)
        {

        }

        public void SetDeviceMode(EClientMode mode)
        {
            
        }

        protected void OnRecalibrated()
        {
            agnosticDisplay.Recalibrate();
        }

        protected void OnClicked()
        {

        }
    }
}