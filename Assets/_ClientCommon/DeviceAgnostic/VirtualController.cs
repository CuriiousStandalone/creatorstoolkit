﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirtualController : MonoBehaviour
{
    public void LockTranslation(bool value)
    {
        gameObject.SetActive(value);
    }
}
