﻿using PulseIQ.Local.ClientCommon.BaseItem;

namespace PulseIQ.Local.DeviceAgnostic
{
    public interface IAgnosticDevice
    {
        void OnSelect(ViewItemBase item);

        void SetDeviceMode(EClientMode mode);
    }
}