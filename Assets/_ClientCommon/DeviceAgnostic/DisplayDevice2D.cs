﻿using PulseIQ.Local.ClientCommon.BaseItem;

namespace PulseIQ.Local.DeviceAgnostic
{
    public class DisplayDevice2D : AgnosticDisplayDevice
    {
        protected VirtualController virtualControllerTranslation;
        protected VirtualController virtualControllerRotation;

        protected override void MultimediaMode()
        {

        }

        protected override void LoadingMode()
        {

        }

        public override void Recalibrate()
        {

        }

        public override void Raycast(ViewItemBase item)
        {

        }

        protected void LockTranslation(bool value)
        {
            virtualControllerTranslation.LockTranslation(value);
        }
    }
}