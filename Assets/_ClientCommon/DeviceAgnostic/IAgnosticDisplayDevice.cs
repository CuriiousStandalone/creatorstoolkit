﻿using PulseIQ.Local.ClientCommon.BaseItem;


namespace PulseIQ.Local.DeviceAgnostic
{
    public interface IAgnosticDisplayDevice
    {
        AgnosticDisplayDevice GetDisplayDeviceType();

        void SetDispayMode(EClientMode mode);

        void Raycast(ViewItemBase item);

        void Recalibrate();
    }
}