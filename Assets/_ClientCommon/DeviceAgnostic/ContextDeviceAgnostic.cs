﻿using strange.extensions.context.api;
using strange.extensions.context.impl;
using UnityEngine;

namespace PulseIQ.Local.DeviceAgnostic
{
    public class ContextDeviceAgnostic : MVCSContext
    {
        public ContextDeviceAgnostic(MonoBehaviour view) : base(view)
        {

        }


        public ContextDeviceAgnostic(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
        {

        }

        protected override void mapBindings()
        {

            mediationBinder.Bind<ViewInputManager>().To<MediatorInputManager>();
            mediationBinder.Bind<ViewAgnosticDevice>().To<MediatorAgnosticDevice>();

            #region Devices
            mediationBinder.Bind<ViewHmdGearVR>().To<MediatorHmdGearVR>();
            mediationBinder.Bind<ViewHmdOculusGo>().To<MediatorHmdGearVR>();
            mediationBinder.Bind<ViewHmdOculusQuest>().To<MediatorHmdOculusQuest>();
            mediationBinder.Bind<ViewTabletS4>().To<MediatorTabletS4>();
            #endregion

            #region Controllers
            mediationBinder.Bind<ViewControllerGearVR>().To<MediatorControllerGearVR>();
            mediationBinder.Bind<ViewControllerGearVRTouchPad>().To<MediatorControllerGearVRTouchPad>();
            mediationBinder.Bind<ViewControllerOculusGo>().To<MediatorControllerOculusGo>();
            mediationBinder.Bind<ViewControllerOculusQuest>().To<MediatorControllerOculusQuest>();
            mediationBinder.Bind<ViewControllerTabletS4>().To<MediatorControllerTabletS4>();
            #endregion


        }
    }
}