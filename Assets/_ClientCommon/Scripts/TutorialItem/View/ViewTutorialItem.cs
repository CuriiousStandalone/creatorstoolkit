﻿using System.Collections.Generic;
using UnityEngine;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.CustomType;
using PulseIQ.Local.ClientCommon.BaseItem;
using PulseIQ.Local.Client.Unity3DLib.Model;
using PulseIQ.Local.Common.Model.Item;

namespace PulseIQ.Local.ClientCommon.TutorialItem
{
    public class ViewTutorialItem : ViewItemBase
    {
        internal enum TutorialEvents
        {
            ACTIVATED,
            ACTIVATED_ON_RECONNECT,
            ACTIVATE_MEDIA_UI,
            DEACTIVATED,
            OBTAINED,
            ABANDONED,
            STARTED,
        }

        public Animator tutorialAnimator { get; private set; }

        [SerializeField]
        private Animator inspectorAnimator;

        public string currentAnimationName { get; private set; }

        private bool isTutorialFocusActive = false;
        private bool isNotating = false;

        private List<string> animationList = new List<string>();

        private MessageData messageData = new MessageData();

        Vector3 observerLocalPosition;
        Quaternion observerLocalRotation;

        Vector2 Touch1Data = new Vector2();
        Vector2 Touch2Data = new Vector2();
        Vector3 vecOldRotation = new Vector3();
        Vector2 oldScaleVector = new Vector2();

        Vector3 forwardVector = new Vector3();
        Vector3 moveVector = Vector3.zero;
        Vector3 startingPosition = new Vector3();
        float moveDistanceLimit = 10f;
        float zoomSpeed = 3.5f;

        float rotationFactor = 100f;

        bool isScaling = false;
        bool isTranslating = false;

        bool hasDecided = false;

        float moveUpdateRate = 0.25f;
        float moveTimer;

        GameObject tempObject;
        private Vector3 CameraRightVector = new Vector3();
        public List<Color> ogColors = new List<Color>();


        protected internal override void init()
        {
            itemType = Local.Common.ItemTypeCode.TUTORIAL_ITEM;
            inspectorAnimator = GetComponent<Animator>();
            tutorialAnimator = inspectorAnimator;

            forwardVector = transform.forward;

            Invoke("SetTempVariables", 0.2f);
        }

        void SetTempVariables()
        {
            tempObject = new GameObject();
            tempObject.transform.rotation = transform.rotation;
            tempObject.transform.position = transform.position;
            tempObject.transform.localScale = transform.localScale;
        }

        public void SetObserverVariables(CTVector3 localPos, CTQuaternion localRot)
        {
            observerLocalPosition = transform.position + (transform.forward * localPos.Z);
            observerLocalRotation = transform.rotation * (new Quaternion(localRot.X, localRot.Y, localRot.Z, localRot.W));
        }

        public void SetAnimationList(List<string> animList)
        {
            animationList = animList;

            if (animationList.Count > 0)
            {
                currentAnimationName = animationList[0];
            }
        }

        protected override void OnDestroy()
        {
            Destroy(tempObject);
        }

        private void Update()
        {
            if (isLerpMoving)
            {
                LerpMoveFunction();
            }

            if (isLerpRotating)
            {
                LerpRotateFunction();
            }

            if (isLerpScaling)
            {
                LerpScaleFunction();
            }

            if (isTutorialFocusActive && !isNotating)
            {
                if (Input.touchCount > 0 || Input.GetMouseButton(0))
                {
                    // Check if UI was pressed
                    if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() || UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(0))
                    {
                        return;
                    }

                    moveTimer += Time.deltaTime;

                    if (moveTimer > moveUpdateRate)
                    {
                        LerpMove(tempObject.transform.position);
                        LerpRotate(tempObject.transform.rotation);
                        LerpScale(tempObject.transform.localScale);
                        moveTimer = 0f;
                    }
                }

                if (1 == Input.touchCount)
                {
                    // Rotation
                    Touch touch = Input.touches[0];

                    Vector3 vecRotation = new Vector3();
                    vecRotation.x = touch.deltaPosition.y;
                    vecRotation.y = touch.deltaPosition.x;
                    vecRotation.z = 0;

                    Quaternion rotationQua = Quaternion.Euler(vecOldRotation - vecRotation);

                    tempObject.transform.RotateAround(CameraRightVector, touch.deltaPosition.y / rotationFactor);
                    tempObject.transform.RotateAround(Vector3.up, -touch.deltaPosition.x / rotationFactor);
                }
                else if (Input.touchCount == 2)
                {
                    // Scaling
                    Touch touch1 = Input.touches[0];
                    Touch touch2 = Input.touches[1];

                    if (touch1.phase == TouchPhase.Moved && touch2.phase == TouchPhase.Moved)
                    {
                        if (hasDecided)
                        {
                            Touch1Data = ((touch1.position + touch1.deltaPosition) - touch1.position).normalized;
                            Touch2Data = ((touch2.position + touch2.deltaPosition) - touch2.position).normalized;

                            if (isScaling)
                            {
                                Vector2 vecDeltaLength = new Vector2();
                                vecDeltaLength = touch1.position - touch2.position;

                                if (vecDeltaLength.sqrMagnitude - oldScaleVector.sqrMagnitude < -0.00001f)
                                {
                                    moveVector = forwardVector * (zoomSpeed * Time.deltaTime);
                                }
                                else if (vecDeltaLength.sqrMagnitude - oldScaleVector.sqrMagnitude > 0.00001f)
                                {
                                    moveVector = -(forwardVector * (zoomSpeed * Time.deltaTime));
                                }

                                oldScaleVector = vecDeltaLength;
                                if (Vector3.Distance(startingPosition, (tempObject.transform.position + moveVector)) < moveDistanceLimit)
                                {
                                    tempObject.transform.position += moveVector;
                                }
                            }

                            else if (isTranslating)
                            {
                                if (Touch1Data.x < -0.5 && Touch2Data.x < -0.5 && MovementYLimit())
                                {
                                    if (Vector3.Distance(startingPosition, (tempObject.transform.position + (CameraRightVector * 5f * Time.deltaTime))) < moveDistanceLimit)
                                    {
                                        tempObject.transform.position += -CameraRightVector * 5f * Time.deltaTime;
                                    }
                                }

                                else if (Touch1Data.y < -0.5 && Touch2Data.y < -0.5 && MovementXLimit())
                                {
                                    if (Vector3.Distance(startingPosition, (tempObject.transform.position + (-Vector3.up * 5f * Time.deltaTime))) < moveDistanceLimit)
                                    {
                                        tempObject.transform.position += -Vector3.up * 5f * Time.deltaTime;
                                    }
                                }

                                else if (Touch1Data.x > 0.5 && Touch2Data.x > 0.5 && MovementYLimit())
                                {
                                    if (Vector3.Distance(startingPosition, (tempObject.transform.position + -CameraRightVector * 5f * Time.deltaTime)) < moveDistanceLimit)
                                    {
                                        tempObject.transform.position += CameraRightVector * 5f * Time.deltaTime;
                                    }
                                }

                                else if (Touch1Data.y > 0.5 && Touch2Data.y > 0.5 && MovementXLimit())
                                {
                                    if (Vector3.Distance(startingPosition, (tempObject.transform.position + Vector3.up * 5f * Time.deltaTime)) < moveDistanceLimit)
                                    {
                                        tempObject.transform.position += Vector3.up * 5f * Time.deltaTime;
                                    }
                                }
                            }
                        }
                        else
                        {
                            Touch1Data = ((touch1.position + touch1.deltaPosition) - touch1.position).normalized;
                            Touch2Data = ((touch2.position + touch2.deltaPosition) - touch2.position).normalized;

                            if ((Touch1Data.x < -0.2 && Touch2Data.x > 0.2) || (Touch1Data.x > 0.2 && Touch2Data.x < -0.2) || (Touch1Data.y < -0.2 && Touch2Data.y > 0.2) || (Touch1Data.y > 0.2 && Touch2Data.y < -0.2) ||
                                (Touch2Data.x < -0.2 && Touch1Data.x > 0.2) || (Touch2Data.x > 0.2 && Touch1Data.x < -0.2) || (Touch2Data.y < -0.2 && Touch1Data.y > 0.2) || (Touch2Data.y > 0.2 && Touch1Data.y < -0.2))
                            {
                                isScaling = true;

                                Vector3 vecDeltaScale = new Vector3();
                                Vector2 vecDeltaLength = new Vector2();
                                vecDeltaLength = touch1.position - touch2.position;

                                float fScaleFactor = 0.0f;

                                if (vecDeltaLength.sqrMagnitude - oldScaleVector.sqrMagnitude < -0.00001f)
                                {
                                    fScaleFactor = -1.0f * 0.5f;
                                }
                                else if (vecDeltaLength.sqrMagnitude - oldScaleVector.sqrMagnitude > 0.00001f)
                                {
                                    fScaleFactor = 0.5f;
                                }

                                vecDeltaScale.x = fScaleFactor;
                                vecDeltaScale.y = fScaleFactor;
                                vecDeltaScale.z = fScaleFactor;

                                oldScaleVector = vecDeltaLength;
                            }

                            else if (Touch1Data.x < -0.5 && Touch2Data.x < -0.5) // && MovementYLimit())
                            {
                                isTranslating = true;

                                if (Vector3.Distance(startingPosition, tempObject.transform.position + (Vector3.right * Time.deltaTime)) < moveDistanceLimit)
                                {
                                    tempObject.transform.position += -Vector3.right * Time.deltaTime;
                                }
                            }

                            else if (Touch1Data.y < -0.5 && Touch2Data.y < -0.5) // && MovementXLimit())
                            {
                                isTranslating = true;

                                if (Vector3.Distance(startingPosition, tempObject.transform.position + (-Vector3.up * Time.deltaTime)) < moveDistanceLimit)
                                {
                                    tempObject.transform.position += -Vector3.up * Time.deltaTime;
                                }
                            }

                            else if (Touch1Data.x > 0.5 && Touch2Data.x > 0.5) // && MovementYLimit())
                            {
                                isTranslating = true;

                                if (Vector3.Distance(startingPosition, tempObject.transform.position + (-Vector3.right * Time.deltaTime)) < moveDistanceLimit)
                                {
                                    tempObject.transform.position += Vector3.right * Time.deltaTime;
                                }
                            }

                            else if (Touch1Data.y > 0.5 && Touch2Data.y > 0.5) // && MovementXLimit())
                            {
                                isTranslating = true;

                                if (Vector3.Distance(startingPosition, tempObject.transform.position + (Vector3.up * Time.deltaTime)) < moveDistanceLimit)
                                {
                                    tempObject.transform.position += Vector3.up * Time.deltaTime;
                                }
                            }

                            hasDecided = true;
                        }
                    }
                }
                else if (Input.touchCount == 0)
                {
                    hasDecided = false;
                    isTranslating = false;
                    isScaling = false;

                    if (null == tempObject)
                    {
                        return;
                    }

                    tempObject.transform.rotation = transform.rotation;
                    tempObject.transform.position = transform.position;
                    tempObject.transform.localScale = transform.localScale;
                }

                return;
            }
        }


        public override void ObtainedItem()
        {
            isObtained = true;

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);

            dispatcher.Dispatch(TutorialEvents.OBTAINED, data);
        }

        public override void ItemAbandoned()
        {
            isObtained = false;

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);

            dispatcher.Dispatch(TutorialEvents.ABANDONED, data);
        }

        public void ActivateTutorialItem(bool reconnect = false)
        {
            startingPosition = transform.position;
            CameraRightVector = observerLocalRotation * Vector3.right;
            isTutorialFocusActive = true;

            MessageData obData = new MessageData();

            CTVector3 obLocalPosition = new CTVector3(observerLocalPosition.x, observerLocalPosition.y, observerLocalPosition.z);
            CTQuaternion obLocalRotation = new CTQuaternion(observerLocalRotation.x, observerLocalRotation.y, observerLocalRotation.z, observerLocalRotation.w);
            obData.Parameters.Add((byte)CommonParameterCode.OBSERVER_LOCAL_POSITION, obLocalPosition);
            obData.Parameters.Add((byte)CommonParameterCode.OBSERVER_LOCAL_ROTATION, obLocalRotation);
            obData.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);
            obData.Parameters.Add((byte)CommonParameterCode.ANIMATION_LIST, animationList);

            //if (reconnect)
            //{
            //    dispatcher.Dispatch(TutorialEvents.ACTIVATED, obData);
            //}
            //else
            //{
            dispatcher.Dispatch(TutorialEvents.ACTIVATED_ON_RECONNECT, obData);
            //}

            MessageData tutorialData = new MessageData();
            tutorialData.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);
            tutorialData.Parameters.Add((byte)CommonParameterCode.MEDIA_NAME, itemID);
            tutorialData.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, ItemTypeCode.TUTORIAL_ITEM);

            dispatcher.Dispatch(TutorialEvents.ACTIVATE_MEDIA_UI, tutorialData);
            //visualise selected tut's for editor
            if (Application.isEditor)
            {
                ogColors.Clear();
                foreach (MeshRenderer MR in GetComponentsInChildren<MeshRenderer>())
                {
                    foreach (Material mat in MR.materials)
                    {
                        ogColors.Add(mat.color);
                        mat.color = Color.blue;
                    }
                }
            }
        }


        public void DeactivateTutorialItem()
        {
            isTutorialFocusActive = false;

            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);

            dispatcher.Dispatch(TutorialEvents.DEACTIVATED, data);
            //visualise selected tut's for editor
            if (Application.isEditor)
            {
                foreach (MeshRenderer MR in GetComponentsInChildren<MeshRenderer>())
                {
                    for (int index = 0; index < MR.materials.Length; index++)
                    {
                        Material mat = MR.materials[index];
                        mat.color = ogColors[index];
                    }
                }

                ogColors.Clear();
            }
        }

        public void PauseAnimation(float frame, string animationName)
        {
            tutorialAnimator.Play(animationName, 0, frame);
            tutorialAnimator.speed = 0;
        }

        public void StartAnimation(string animName)
        {
            tutorialAnimator.speed = 1f;
            tutorialAnimator.Play(animName);
            currentAnimationName = animName;

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);
            data.Parameters.Add((byte)CommonParameterCode.ANIMATION_LENGTH, tutorialAnimator.GetCurrentAnimatorClipInfo(0).Length);
            dispatcher.Dispatch(TutorialEvents.STARTED, data);
        }

        public void StopAnimation(string animName)
        {
        }

        bool MovementYLimit()
        {
            if (Touch1Data.y < 0.3f && Touch1Data.y > -0.3f && Touch2Data.y < 0.3f && Touch2Data.y > -0.3f)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        bool MovementXLimit()
        {
            if (Touch1Data.x < 0.3f && Touch1Data.x > -0.3f && Touch2Data.x < 0.3f && Touch2Data.x > -0.3f)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SetCurrentAnimation(string animName)
        {
            currentAnimationName = animName;
        }

        public void Notating()
        {
            isNotating = true;
        }

        public void StopNotating()
        {
            isNotating = false;
        }

        public override void SyncItemData(CTItemData data)
        {
            base.SyncItemData(data);

            //CTTutorialItemData tutorialData = (CTTutorialItemData)data;
            //
            //itemIndex = tutorialData.ItemIndex;
            //itemType = tutorialData.ItemType;
            //animationList = tutorialData.ListAnimationNames;
            //observerLocalPosition = new Vector3(tutorialData.ObserverLocalPosition.X, tutorialData.ObserverLocalPosition.Y, tutorialData.ObserverLocalPosition.Z);
            //observerLocalRotation = new Quaternion(tutorialData.ObserverLocalRotation.X, tutorialData.ObserverLocalRotation.Y, tutorialData.ObserverLocalRotation.Z, tutorialData.ObserverLocalRotation.W);
        }
    }
}