﻿using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Client.Unity3DLib.Model;
using PulseIQ.Local.ClientCommon.BaseItem;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.CustomType;

namespace PulseIQ.Local.ClientCommon.TutorialItem
{
    public class MediatorTutorialItem : EventMediator
    {
        [Inject]
        public ViewTutorialItem view { get; set; }

        public override void OnRegister()
        {
            view.init();
            UpdateListeners(true);
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewTutorialItem.TutorialEvents.ACTIVATED, OnItemActivated);
            view.dispatcher.UpdateListener(value, ViewTutorialItem.TutorialEvents.DEACTIVATED, OnItemDeactivated);
            view.dispatcher.UpdateListener(value, ViewTutorialItem.TutorialEvents.OBTAINED, OnItemObtained);
            view.dispatcher.UpdateListener(value, ViewTutorialItem.TutorialEvents.ABANDONED, OnItemAbandoned);
            view.dispatcher.UpdateListener(value, ViewTutorialItem.TutorialEvents.STARTED, OnAnimationStarted);
            view.dispatcher.UpdateListener(value, ViewTutorialItem.TutorialEvents.ACTIVATE_MEDIA_UI, OnActivateMediaUI);
            view.dispatcher.UpdateListener(value, ViewTutorialItem.TutorialEvents.ACTIVATED_ON_RECONNECT, OnActivateForReconnect);

            view.dispatcher.UpdateListener(value, ViewItemBase.EVENT_MOVE, OnMoveItem);
            view.dispatcher.UpdateListener(value, ViewItemBase.EVENT_ROTATE, OnRotateItem);
            view.dispatcher.UpdateListener(value, ViewItemBase.EVENT_SCALE, OnScaleItem);
            view.dispatcher.UpdateListener(value, ViewItemBase.LERP_MOVE, OnLerpMoveItem);
            view.dispatcher.UpdateListener(value, ViewItemBase.LERP_ROTATE, OnLerpRotateItem);
            view.dispatcher.UpdateListener(value, ViewItemBase.LERP_SCALE, OnLerpScaleItem);
        }


        private void OnMessageReceived(System.Object data)
        {
            MessageData tutorialData = (MessageData)data;
            ServerCmdCode code = (ServerCmdCode)tutorialData[(byte)CommonParameterCode.OPERATION_CODE];

            switch (code)
            {
                case ServerCmdCode.TUTORIAL_ACTIVATED:
                   {          
                       view.ActivateTutorialItem();
                       break;
                   }

                case ServerCmdCode.TUTORIAL_PAUSED:
                    { 
                       view.PauseAnimation((float)tutorialData[(byte)CommonParameterCode.ANIMATION_FRAME], (string)tutorialData[(byte)CommonParameterCode.ANIMATION_NAME]);
                       break;
                    }

                case ServerCmdCode.TUTORIAL_STARTED:
                   {
                       view.StartAnimation((string)tutorialData[(byte)CommonParameterCode.ANIMATION_NAME]);
                       break;
                   }

                case ServerCmdCode.TUTORIAL_STOPPED:
                   {
                       view.StopAnimation((string)tutorialData[(byte)CommonParameterCode.ANIMATION_NAME]);
                       break;
                   }

                case ServerCmdCode.TUTORIAL_DEACTIVATED:
                   {
                       view.DeactivateTutorialItem();

                        dispatcher.Dispatch(SubCode.TryAbandonItem, tutorialData);
                       break;
                   }

                case ServerCmdCode.ITEM_OBTAIN_SUCCESS:
                   {
                       view.ObtainedItem();

                        dispatcher.Dispatch(SubCode.ActivateTutorialItem, tutorialData);
                       break;
                   }

                case ServerCmdCode.ITEM_ABANDONED:
                   {
                        view.ItemAbandoned();

                        dispatcher.Dispatch(ECommonViewEvent.DROPDOWN_ITEM_HIDDEN, tutorialData);

                        break;
                   }

                case ServerCmdCode.ITEM_MOVED:
                   {
                       view.UpdatePosition((CTVector3)tutorialData[(byte)CommonParameterCode.ITEM_POSITION]);
                       break;
                   }

                case ServerCmdCode.ITEM_ROTATED:
                    {
                        view.UpdateRotation((CTQuaternion)tutorialData[(byte)CommonParameterCode.ITEM_ROTATION]);
                        break;
                    }

                case ServerCmdCode.ITEM_SCALED:
                    {
                        view.UpdateScale((CTVector3)tutorialData[(byte)CommonParameterCode.ITEM_SCALE]);
                        break;
                    }

                case ServerCmdCode.NOTATION_SHOWN:
                    {
                        view.Notating();
                        break;
                    }

                case ServerCmdCode.NOTATION_STOPPED:
                    {
                        view.StopNotating();
                        break;
                    }

                case ServerCmdCode.ITEM_STATUS_SYNCED:
                    {
                        CTItemData syncData = (CTItemData)(tutorialData[(byte)CommonParameterCode.DATA_CT_ITEM_DATA]);
                        view.SyncItemData(syncData);
                        break;
                    }

                case ServerCmdCode.ITEM_LERP_MOVED:
                    {
                        CTVector3 pos = (CTVector3)tutorialData[(byte)CommonParameterCode.MOVE_TO_POSITION];
                        view.LerpMovement(pos);
                        break;
                    }

                case ServerCmdCode.ITEM_LERP_ROTATED:
                    {
                        CTQuaternion rot = (CTQuaternion)tutorialData[(byte)CommonParameterCode.MOVE_TO_ROTATION];
                        view.LerpRotation(rot);
                        break;
                    }

                case ServerCmdCode.ITEM_LERP_SCALED:
                    {
                        CTVector3 scale = (CTVector3)tutorialData[(byte)CommonParameterCode.MOVE_TO_SCALE];
                        view.LerpScale(scale);
                        break;
                    }

                case ServerCmdCode.SYNC_TUTORIAL_ITEM_STATUS:
                    {
                        bool isAtive = (bool)tutorialData[(byte)CommonParameterCode.IS_ACTIVE];

                        if(isAtive)
                        {
                            view.ActivateTutorialItem(true);
                        }

                        ///TODO: Implement animation sync etc.
                        break;
                    }
            }
        }
        
        private void OnMoveItem(IEvent evt)
        {
		 	dispatcher.Dispatch(SubCode.MoveItem, evt.data);
        }

        private void OnRotateItem(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.RotateItem, evt.data);
        }

        private void OnScaleItem(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.ScaleItem, evt.data);
        }

        private void OnLerpMoveItem(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.LerpMoveItem, evt.data);
        }

        private void OnLerpRotateItem(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.LerpRotateItem, evt.data);
        }

        private void OnLerpScaleItem(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.LerpScaleItem, evt.data);
        }        
        
        private void OnActivateForReconnect(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.TUTORIAL_DATA, evt.data);
            dispatcher.Dispatch(ECommonViewEvent.TUTORIAL_ACTIVATED, evt.data);
        }
        
        private void OnItemActivated(IEvent evt)
       {
           dispatcher.Dispatch(ECommonViewEvent.TUTORIAL_DATA, evt.data);
       }
       
       private void OnItemDeactivated(IEvent evt)
       {
           dispatcher.Dispatch(ECommonViewEvent.TUTORIAL_DEACTIVATED, evt.data);
           dispatcher.Dispatch(ECommonViewEvent.MEDIA_HIDE, evt.data);
        }

        private void OnItemObtained(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.TUTORIAL_OBTAINED, evt.data);
            dispatcher.Dispatch(ECommonViewEvent.ITEM_OBTAINED, evt.data);
        }

        private void OnItemAbandoned(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.TUTORIAL_ABANDONED, evt.data);
            dispatcher.Dispatch(ECommonViewEvent.ITEM_ABANDONED, evt.data);
        }

        private void OnAnimationStarted(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.TUTORIAL_ANIM_STARTED, evt.data);
        }

        private void OnActivateMediaUI(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.MEDIA_SHOW, evt.data);
        }
    }
}