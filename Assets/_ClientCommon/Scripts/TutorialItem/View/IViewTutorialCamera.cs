﻿using PulseIQ.Local.Common.CustomType;

namespace PulseIQ.Local.ClientCommon
{
    public interface IViewMainCamera
    {
        void ActivateCamera(CTVector3 position, CTQuaternion rotation);

        void DeactivateCamera();

        void WorldNotationStarted();

        void WorldNotationEnded();

        void MaskCameraForVR();

        void UnmaskCameraFromVR();

        void ShowCamera();

        void HideCamera();
    }
}