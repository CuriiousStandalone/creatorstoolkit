﻿namespace PulseIQ.Local.ClientCommon.TutorialItem
{
    public enum IntCmdCodeTutorialItem : byte
    {
        CREATED,
        ACTIVATED,
        DEACTIVATED,
        STARTED,
        PAUSED,
        STOPPED,
        SYNCED,
        MESSAGE_CAMERA,
        CAMERA_MESSAGE,
    }
}