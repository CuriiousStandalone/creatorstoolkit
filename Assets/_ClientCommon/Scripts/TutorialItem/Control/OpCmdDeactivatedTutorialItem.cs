﻿using strange.extensions.command.impl;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.BaseItem;
using PulseIQ.Local.ClientCommon.AvatarItem;

namespace PulseIQ.Local.ClientCommon.TutorialItem
{
    public class OpCmdDeactivatedTutorialItem : EventCommand
    {
        [Inject] public IItemDataset model { get; set; }

        public override void Execute()
        {
            foreach (ViewItemBase item in model.itemList)
            {
                if (item.itemType == ItemTypeCode.AVATAR)
                {
                    item.GetComponent<ViewAvatar>().HideAvatarTutorial(false);
                }
            }

            MessageData data = (MessageData) evt.data;
            data.Parameters.Add((byte) CommonParameterCode.OPERATION_CODE, evt.type);

            dispatcher.Dispatch(IntCmdCodeTutorialItem.DEACTIVATED, data);
        }
    }
}