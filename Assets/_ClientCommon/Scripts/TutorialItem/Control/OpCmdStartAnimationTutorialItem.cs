﻿using UnityEngine;
using strange.extensions.command.impl;

using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.ItemManager;
using PulseIQ.Local.ClientCommon.BaseItem;

namespace PulseIQ.Local.ClientCommon.TutorialItem
{
    public class OpCmdStartAnimationTutorialItem : EventCommand
    {
        [Inject]
        public IItemDataset model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;
            string itemID = (string)data.Parameters[(byte)CommonParameterCode.ITEMID];
            string currentAnimationName = "";

            if (data.Parameters.ContainsKey((byte)CommonParameterCode.ANIMATION_NAME))
            {
                currentAnimationName = (string)data.Parameters[(byte)CommonParameterCode.ANIMATION_NAME];
            }

            else
            {
                foreach (ViewItemBase itemBase in model.itemList)
                {
                    if (itemID == itemBase.itemID)
                    {
                        currentAnimationName = itemBase.GetComponent<ViewTutorialItem>().currentAnimationName;
                    }
                }
            }

            if(currentAnimationName == "")
            {
                Debug.LogWarning("Current animation == NULL!");
            }
            else
            {
                OperationsTutorialItem.StartAnimation(ClientBridge.Instance, ClientBridge.Instance.CurWorldData.Id, itemID, currentAnimationName);
            }
        }
    }
}