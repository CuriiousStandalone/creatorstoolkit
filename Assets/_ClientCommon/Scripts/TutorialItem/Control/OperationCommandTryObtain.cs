﻿using strange.extensions.command.impl;

using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Client.Unity3DLib.Operations;

namespace PulseIQ.Local.ClientCommon.TutorialItem.OperationCommands
{
    public class OperationCommandTryObtain : EventCommand
    {
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            OperationsItem.TryObtain(ClientBridge.Instance, ClientBridge.Instance.CurUserID, ClientBridge.Instance.CurWorldData.Id, (string)data.Parameters[(byte)CommonParameterCode.ITEMID]);
        }
    }
}