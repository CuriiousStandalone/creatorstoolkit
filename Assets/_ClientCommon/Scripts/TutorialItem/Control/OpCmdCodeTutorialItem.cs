﻿namespace PulseIQ.Local.ClientCommon.TutorialItem
{
    public enum OpCmdCodeTutorialItem : byte
    {
        CREATE,
        ACTIVATE,
        DEACTIVATE,
        START,
        PAUSE,
        STOP,
    }
}