﻿using UnityEngine;
using strange.extensions.command.impl;

using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.ItemManager;
using PulseIQ.Local.ClientCommon.BaseItem;

namespace PulseIQ.Local.ClientCommon.TutorialItem
{
    public class OpCmdPauseAnimationTutorialItem : EventCommand
    {
        [Inject]
        public IItemDataset model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;
            string itemID = (string)data.Parameters[(byte)CommonParameterCode.ITEMID];
            float currentFrame = -1f;
            string currentAnimation = "";

            foreach (ViewItemBase itemBase in model.itemList)
            {
                if (itemID == itemBase.itemID)
                {
                    currentFrame = itemBase.GetComponent<ViewTutorialItem>().tutorialAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime;
                    currentAnimation = itemBase.GetComponent<ViewTutorialItem>().currentAnimationName;
                }
            }

            if (currentFrame == -1f || currentAnimation == "")
            {
                Debug.LogWarning("Null values in PauseAnimation!");
            }
            else
            {
                OperationsTutorialItem.PauseAnimation(ClientBridge.Instance, ClientBridge.Instance.CurWorldData.Id, itemID, currentAnimation, currentFrame);
           }
        }
    }
}