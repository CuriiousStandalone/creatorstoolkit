﻿using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.BaseItem;
using PulseIQ.Local.ClientCommon.AvatarItem;

using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.TutorialItem
{
    public class OpCmdSyncTutorialItemStatus : EventCommand
    {
        [Inject]
        public IItemDataset model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            bool isActive = (bool)data[(byte)CommonParameterCode.IS_ACTIVE];

            if(!isActive)
            {
                return;
            }

            foreach (ViewItemBase item in model.itemList)
            {
                if (item.itemType == ItemTypeCode.AVATAR)
                {
                    item.GetComponent<ViewAvatar>().HideAvatarTutorial(true);
                }
            }

            data.Parameters.Add((byte)CommonParameterCode.OPERATION_CODE, evt.type);

            dispatcher.Dispatch(IntCmdCodeTutorialItem.SYNCED, evt.data);
        }
    }
}
