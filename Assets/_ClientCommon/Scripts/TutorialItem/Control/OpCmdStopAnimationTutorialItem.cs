﻿using UnityEngine;
using strange.extensions.command.impl;

using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.ItemManager;
using PulseIQ.Local.ClientCommon.BaseItem;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace PulseIQ.Local.ClientCommon.TutorialItem
{
    public class OpCmdStopAnimationTutorialItem : EventCommand
    {
        [Inject]
        public IItemDataset model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;
            string itemID = (string)data.Parameters[(byte)CommonParameterCode.ITEMID];
            string currentAnimationName = "";

            foreach (ViewItemBase itemBase in model.itemList)
            {
                if (itemID == itemBase.itemID)
                {
                    currentAnimationName = itemBase.GetComponent<ViewTutorialItem>().currentAnimationName;
                }
            }

            if (currentAnimationName == "")
            {
                Debug.LogWarning("Current animation == NULL!");
            }
            else
            {
                OperationsTutorialItem.StopAnimation(ClientBridge.Instance, ClientBridge.Instance.CurWorldData.Id, itemID, currentAnimationName);
            }
        }
    }
}