﻿using System.Collections.Generic;
using System.IO;

using UnityEditor;
using UnityEditor.SceneManagement;

using UnityEngine;
using UnityEngine.SceneManagement;

namespace PulseIQ.Local.ClientCommon.Extensions
{
    public class SceneSwitcher : EditorWindow
    {
        //based on https://gist.github.com/KenneyNL/cc9a96e3db2fdf4c36db59999259a924
        [MenuItem("IQ Tools/Windows/Scenes")]
        public static void ShowWindow()
        {
            GetWindow(typeof(SceneSwitcher), false, "Scenes");
        }

        private int _selected;
        private int _changedSelected;
        readonly List<string> _scenes = new List<string>();
        private Scene[] _loadedScenesCache;

        private const float PLUS_MINUS_WIDTH = 30f;
        private const float SCENE_HORIZONTAL_HEIGHT = 20f;

        void OnGUI()
        {
            EditorGUILayout.Space();

            _scenes.Clear();

            string[] dropOptions = new string[EditorBuildSettings.scenes.Length];

            for (int i = 0; i < EditorBuildSettings.scenes.Length; i++)
            {
                EditorBuildSettingsScene scene = EditorBuildSettings.scenes[i];

                string sceneName = Path.GetFileNameWithoutExtension(scene.path);

                _scenes.Add(sceneName);

                dropOptions[i] = _scenes[i];

                if (scene.path == EditorSceneManager.GetActiveScene().path)
                {
                    _selected = _changedSelected = i;
                }
            }

            EditorGUILayout.Space();

            EditorGUILayout.LabelField("Scenes", EditorStyles.boldLabel);

            EditorGUIUtility.labelWidth = position.width / 3.5f;
            GUILayout.ExpandWidth(false);
            EditorSceneManager.playModeStartScene = (SceneAsset)EditorGUILayout.ObjectField(new GUIContent("Start Scene"), EditorSceneManager.playModeStartScene, typeof(SceneAsset), false);

            for (int i = 0; i < _scenes.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button(_scenes[i], GUILayout.Height(SCENE_HORIZONTAL_HEIGHT), GUILayout.Width(position.width - ((PLUS_MINUS_WIDTH + 10) * 3))))
                {
                    _selected = _changedSelected = i;

                    EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
                    EditorSceneManager.OpenScene(EditorBuildSettings.scenes[i].path);
                }

                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("+", GUILayout.Height(SCENE_HORIZONTAL_HEIGHT), GUILayout.Width(PLUS_MINUS_WIDTH)))
                {
                    EditorSceneManager.OpenScene(EditorBuildSettings.scenes[i].path, OpenSceneMode.Additive);
                }

                if (SceneManager.GetSceneByPath(EditorBuildSettings.scenes[i].path).isLoaded)
                {
                    if (GUILayout.Button("-", GUILayout.Height(SCENE_HORIZONTAL_HEIGHT), GUILayout.Width(PLUS_MINUS_WIDTH)))
                    {
                        _selected = _changedSelected = i;

                        EditorSceneManager.CloseScene(SceneManager.GetSceneByPath(EditorBuildSettings.scenes[i].path), true);
                    }
                }
                else
                {
                    GUILayout.Button("", GUILayout.Height(SCENE_HORIZONTAL_HEIGHT), GUILayout.Width(PLUS_MINUS_WIDTH));
                }

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.EndHorizontal();

                dropOptions[i] = _scenes[i];
            }
        }
    }
}