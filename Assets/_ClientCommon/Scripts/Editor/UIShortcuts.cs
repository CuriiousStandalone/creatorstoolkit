﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace PulseIQ.Local.ClientCommon.Extensions
{
    public class UIShortcuts : Editor
    {
        [MenuItem("IQ Tools/Make Tight #t")]
        public static void MakeTight()
        {
            Undo.RecordObject(Selection.activeTransform, "Tighten " + Selection.activeObject.name);
            ContentSizeFitter contentSizeFitter = Selection.activeGameObject.AddComponent<ContentSizeFitter>();
            contentSizeFitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
            contentSizeFitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
            contentSizeFitter.SetLayoutVertical();
            contentSizeFitter.SetLayoutHorizontal();
            DestroyImmediate(contentSizeFitter);
        }

        [MenuItem("IQ Tools/Group Selected %g", priority = 80)]
        static void Init()
        {
            Transform[] selected = Selection.GetTransforms(SelectionMode.ExcludePrefab | SelectionMode.TopLevel);

            GameObject emptyNode = new GameObject();
            if (Selection.activeGameObject.transform.parent != null)
            {
                GameObjectUtility.SetParentAndAlign(emptyNode, Selection.activeGameObject.transform.parent.gameObject);
            }

            Vector3 averagePosition = Vector3.zero;
            foreach (Transform node in selected)
            {
                averagePosition += node.position;
            }

            if (selected.Length > 0)
            {
                averagePosition /= selected.Length;
            }

            emptyNode.transform.position = averagePosition;
            emptyNode.name = "group";
            Undo.RecordObjects(selected, "Grouping");
            foreach (Transform node in selected)
            {
                node.parent = emptyNode.transform;
            }

            Selection.activeGameObject = emptyNode;
        }
    }
}