﻿using UnityEngine;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.api;
using System.Timers;

using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.Common.CustomType;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.InteractiveItem;

namespace PulseIQ.Local.ClientCommon.BaseItem
{
    public class OpCmdMoveToPositionBaseItem : EventCommand
    {
        Timer funcTimer = null;
        bool lockedPosition = false;
        string itemID = "";
        Vector3 itemMoveToLocation = new Vector3();
        //Quaternion itemMoveToRotation = new Quaternion();
        //Vector3 itemMoveToScale = new Vector3();
        float itemMoveToUpdateFrequency = 0f;

        Vector3 itemMoveToTempCurrentPosition = new Vector3();
        //Quaternion itemMoveToTempCurrentRotation = new Quaternion();

        Vector3 itemMoveToStartPostion = new Vector3(0,0,0);
        //Quaternion itemMoveToStartRotation = new Quaternion();

        float itemMoveToSpeed = 0f;
        float deltaTime = 0f;

        public override void Execute()
        {
            Retain();

            MessageData data = (MessageData)evt.data;

            if(itemMoveToSpeed == 0)
            {
                itemMoveToSpeed = (float)data.Parameters[(byte)CommonParameterCode.MOVE_TO_SPEED];
                itemMoveToUpdateFrequency = (float)data.Parameters[(byte)CommonParameterCode.MOVE_TO_FREQUENCY];
                itemMoveToTempCurrentPosition = (Vector3)data.Parameters[(byte)CommonParameterCode.MOVE_TO_START_POSITION];
                //itemMoveToTempCurrentRotation = (Quaternion)data.Parameters[(byte)CommonParameterCode.MOVE_TO_START_ROTATION];
                //itemMoveToScale = (Vector3)data.Parameters[(byte)CommonParameterCode.MOVE_TO_START_SCALE];
                itemID = (string)data.Parameters[(byte)CommonParameterCode.ITEMID];
            }

            itemMoveToLocation = (Vector3)data.Parameters[(byte)CommonParameterCode.MOVE_TO_POSITION];
            //itemMoveToRotation = (Quaternion)data.Parameters[(byte)CommonParameterCode.MOVE_TO_ROTATION];

            if (funcTimer == null && itemMoveToUpdateFrequency != 0)
            {
                funcTimer = new Timer();
                funcTimer.Elapsed += TimeElapsed;
                funcTimer.Interval = 250f;
                funcTimer.Start();
            }

			dispatcher.AddListener(SubCode.UpdateMoveTo, UpdateDataFunc);
			dispatcher.AddListener(SubCode.StopUpdate, StopUpdateFunc);
        }

        void UpdateDataFunc(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;

            itemMoveToLocation = (Vector3)data.Parameters[(byte)CommonParameterCode.MOVE_TO_POSITION];
            //itemMoveToRotation = (Quaternion)data.Parameters[(byte)CommonParameterCode.MOVE_TO_ROTATION];
            deltaTime = Time.deltaTime;
        }

        void TimeElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            itemMoveToTempCurrentPosition = itemMoveToTempCurrentPosition + (itemMoveToLocation - itemMoveToTempCurrentPosition).normalized * itemMoveToSpeed * deltaTime;

            //float distancePercentage = Vector3.Distance(itemMoveToStartPostion, itemMoveToTempCurrentPosition) / Vector3.Distance(itemMoveToStartPostion, itemMoveToLocation);
            //Vector3 euler = itemMoveToRotation.eulerAngles - itemMoveToStartRotation.eulerAngles;
            //euler = euler * distancePercentage;
            //itemMoveToTempCurrentRotation = Quaternion.Euler(euler);

            CTVector3 position = new CTVector3(itemMoveToTempCurrentPosition.x, itemMoveToTempCurrentPosition.y, itemMoveToTempCurrentPosition.z);
           //CTQuaternion rotation = new CTQuaternion(itemMoveToTempCurrentRotation.x, itemMoveToTempCurrentRotation.y, itemMoveToTempCurrentRotation.z, itemMoveToTempCurrentRotation.w);
           //CTVector3 scale = new CTVector3(itemMoveToScale.x, itemMoveToScale.y, itemMoveToScale.z);

            if (Vector3.Distance(itemMoveToTempCurrentPosition, itemMoveToLocation) < 1.5f || lockedPosition)
            {
                lockedPosition = true;
                position = new CTVector3(itemMoveToLocation.x, itemMoveToLocation.y, itemMoveToLocation.z);
                //rotation = new CTQuaternion(itemMoveToRotation.x, itemMoveToRotation.y, itemMoveToRotation.z, itemMoveToRotation.w);
                //scale = new CTVector3(itemMoveToScale.x, itemMoveToScale.y, itemMoveToScale.z);
            }

            OperationsItem.MoveItem(ClientBridge.Instance, itemID, position);
        }

        void StopUpdateFunc()
        {
            funcTimer.Stop();
            funcTimer.Dispose();
            dispatcher.RemoveListener(IntCmdCodeInteractiveItem.UPDATE_MOVE_TO, UpdateDataFunc);
            dispatcher.RemoveListener(IntCmdCodeInteractiveItem.STOP_UPDATE, StopUpdateFunc);
            Release();
        }
    }
}