﻿using strange.extensions.command.impl;

using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.CustomType;

namespace PulseIQ.Local.ClientCommon.BaseItem
{
    public class OpCmdScaleLerpBaseItem : EventCommand
    {
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            string itemID = (string)data.Parameters[(byte)CommonParameterCode.ITEMID];
            CTVector3 scale = (CTVector3)data.Parameters[(byte)CommonParameterCode.ITEM_SCALE];

            OperationsItem.LerpScaleItem(ClientBridge.Instance, itemID, scale);
        }
    }
}