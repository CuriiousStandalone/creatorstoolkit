﻿using strange.extensions.command.impl;

using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.Common.CustomType;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace PulseIQ.Local.ClientCommon.BaseItem
{
    public class OpCmdRotateBaseItem : EventCommand
    {
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            CTQuaternion itemRot = (CTQuaternion)data.Parameters[(byte)CommonParameterCode.ITEM_ROTATION];
            string id = (string)data.Parameters[(byte)CommonParameterCode.ITEMID];

            OperationsItem.RotateItem(ClientBridge.Instance, id, itemRot);
        }
    }
}