﻿namespace PulseIQ.Local.ClientCommon.BaseItem
{
    public enum OpCmdCodeBaseItem : byte
    {
        MOVE,
        MOVE_TO_POSITION,
        UPDATE_MOVE_TO,
        STOP_UPDATE,
        CREATE,
        DESTROY,
        ABANDON,
        OBTAIN,
        LOCAL_MOVE_TO,
        LOCAL_ROTATE_TO,
        LOCAL_SCALE_TO,
    }
}