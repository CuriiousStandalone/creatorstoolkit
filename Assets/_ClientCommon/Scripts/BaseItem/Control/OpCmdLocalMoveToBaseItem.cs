﻿using strange.extensions.command.impl;

using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.CustomType;

namespace PulseIQ.Local.ClientCommon.BaseItem
{
    public class OpCmdLocalMoveToBaseItem : EventCommand
    {
        [Inject]
        public IItemDataset model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            string itemID = (string)data[(byte)CommonParameterCode.ITEMID];
            CTVector3 position = (CTVector3)data[(byte)CommonParameterCode.MOVE_TO_POSITION];

            OperationsItem.OfflineMoveItem(ClientBridge.Instance, itemID, 1f, position);
        }
    }
	
}
