﻿using UnityEngine;
using strange.extensions.command.impl;

using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.Client.Unity3DLib.Model;
using PulseIQ.Local.Common.CustomType;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace PulseIQ.Local.ClientCommon.BaseItem
{
    public class OpCmdMoveBaseItem : EventCommand
    {
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            CTVector3 itemPos = (CTVector3)data.Parameters[(byte)CommonParameterCode.ITEM_POSITION];
            string id = (string)data.Parameters[(byte)CommonParameterCode.ITEMID];

            OperationsItem.MoveItem(ClientBridge.Instance, id, itemPos);
        }
    }
}