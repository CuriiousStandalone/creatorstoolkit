﻿using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.BaseItem
{
    public class OpCmdDestroyBaseItem : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(IntCmdCodeBaseItem.DESTROY, evt.data);
        }
    }
}