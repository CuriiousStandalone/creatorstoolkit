﻿using strange.extensions.command.impl;

using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.CustomType;

namespace PulseIQ.Local.ClientCommon.BaseItem
{
    public class OpCmdRotLerpBaseItem : EventCommand
    {
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            string itemID = (string)data.Parameters[(byte)CommonParameterCode.ITEMID];
            CTQuaternion rot = (CTQuaternion)data.Parameters[(byte)CommonParameterCode.ITEM_ROTATION];

            OperationsItem.LerpRotateItem(ClientBridge.Instance, itemID, rot);
        }
    }
}