﻿using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.BaseItem
{
    public class OpCmdCreatedBaseItem : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(IntCmdCodeBaseItem.CREATED, evt.data);
        }
    }
}