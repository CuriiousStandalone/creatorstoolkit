﻿using UnityEngine;

using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.SlotGroup;
using PulseIQ.Local.Common.CustomType;

namespace PulseIQ.Local.ClientCommon.BaseItem
{
    public class MediatorItemBase : EventMediator
    {
        [Inject]
        public ViewItemBase view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        virtual protected void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewItemBase.EVENT_MOVE, OnMove);
            view.dispatcher.UpdateListener(value, ViewItemBase.EVENT_ROTATE, OnRotate);
            view.dispatcher.UpdateListener(value, ViewItemBase.EVENT_SCALE, OnScale);
            view.dispatcher.UpdateListener(value, ViewItemBase.ABANDON_ITEM, OnAbandonItem);
            view.dispatcher.UpdateListener(value, ViewItemBase.ATTACH_TO_HOLDING_POS, OnAttachToHoldingPos);
            view.dispatcher.UpdateListener(value, ViewItemBase.ATTACH_TO_HOLDING_ROT, OnAttachToHoldingRot);
            view.dispatcher.UpdateListener(value, ViewItemBase.ATTACH_TO_HOLDING_SCALE, OnAttachToHoldingScale);
            view.dispatcher.UpdateListener(value, ViewItemBase.LERP_MOVE, OnLerpMove);
            view.dispatcher.UpdateListener(value, ViewItemBase.LERP_ROTATE, OnLerpRotate);
            view.dispatcher.UpdateListener(value, ViewItemBase.LERP_SCALE, OnLerpScale);
        }

        public void OnAttachToHoldingPos(IEvent evt)
        {
            dispatcher.Dispatch(IntCmdCodeBaseItem.ATTACH_TO_HOLDING_POS, evt.data);
        }

        public void OnAttachToHoldingRot(IEvent evt)
        {
            dispatcher.Dispatch(IntCmdCodeBaseItem.ATTACH_TO_HOLDING_ROT, evt.data);
        }

        public void OnAttachToHoldingScale(IEvent evt)
        {
            dispatcher.Dispatch(IntCmdCodeBaseItem.ATTACH_TO_HOLDING_SCALE, evt.data);
        }

        public void OnMove(IEvent evt)
        {
			dispatcher.Dispatch(SubCode.MoveItem, evt);
        }

        public void OnRotate(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.RotateItem, evt);
        }

        public void OnScale(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.ScaleItem, evt);
        }

        public void OnLerpMove(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.LerpMoveItem, evt);
        }

        public void OnLerpRotate(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.LerpRotateItem, evt);
        }

        public void OnLerpScale(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.LerpScaleItem, evt);
        }

        public void OnAbandonItem(IEvent evt)
        {
			dispatcher.Dispatch(SubCode.TryAbandonItem, evt.data);
        }

        private void OnMessageReceived(System.Object data)
        {
            MessageData itemData = (MessageData)data;

            ServerCmdCode Code = (ServerCmdCode)itemData[(byte)CommonParameterCode.OPERATION_CODE];

            switch (Code)
            {
                case ServerCmdCode.ITEM_MOVED:
                    {
                        CTVector3 pos = (CTVector3)itemData[(byte)CommonParameterCode.ITEM_POSITION];
                        view.UpdatePosition(pos);
                        break;
                    }

                case ServerCmdCode.ITEM_ROTATED:
                    {
                        CTQuaternion rot = (CTQuaternion)itemData[(byte)CommonParameterCode.ITEM_ROTATION];
                        view.UpdateRotation(rot);
                        break;
                    }

                case ServerCmdCode.ITEM_SCALED:
                    {
                        CTVector3 scale = (CTVector3)itemData[(byte)CommonParameterCode.ITEM_SCALE];
                        view.UpdateScale(scale);
                        break;
                    }

                case ServerCmdCode.ITEM_OBTAIN_FAILED:
                    {
                        view.ObtainedItemFailed();
                        break;
                    }

                case ServerCmdCode.ITEM_OBTAIN_SUCCESS:
                    {
                        view.ObtainedItem();
                        break;
                    }

                case ServerCmdCode.ITEM_ABANDONED:
                    {
                        view.ItemAbandoned();
                        break;
                    }

                case ServerCmdCode.ITEM_SHOWN:
                    {
                        view.ShowView();
                        break;
                    }

                case ServerCmdCode.ITEM_HIDDEN:
                    {
                        view.HideView();
                        break;
                    }

                case ServerCmdCode.SLOT_GROUP_FINISH_ANI_PLAYED:
                    {
                        break;
                    }


                case ServerCmdCode.ITEM_STATUS_SYNCED:
                    {
                        CTItemData syncData = (CTItemData)(itemData[(byte)CommonParameterCode.DATA_CT_ITEM_DATA]);
                        view.SyncItemData(syncData);
                        break;
                    }

                case ServerCmdCode.SLOT_GROUP_STATUS_SYNC:
                    {
                        CTSlotGroupData slotData = (CTSlotGroupData)(itemData[(byte)CommonParameterCode.DATA_CT_SLOT_GROUP_DATA]);
                        view.SlotGroupSynced(slotData.SlotGroupFinishAniTimeElapsed);
                        break;
                    }

                case ServerCmdCode.ITEM_LERP_MOVED:
                    {
                        CTVector3 pos = (CTVector3)itemData[(byte)CommonParameterCode.MOVE_TO_POSITION];
                        view.LerpMovement(pos);
                        break;
                    }

                case ServerCmdCode.ITEM_LERP_ROTATED:
                    {
                        CTQuaternion rot = (CTQuaternion)itemData[(byte)CommonParameterCode.MOVE_TO_ROTATION];
                        view.LerpRotation(rot);
                        break;
                    }

                case ServerCmdCode.ITEM_LERP_SCALED:
                    {
                        CTVector3 scale = (CTVector3)itemData[(byte)CommonParameterCode.MOVE_TO_SCALE];
                        view.LerpScale(scale);
                        break;
                    }

            }
        }

        private void OnAbandonSuccess()
        {

        }

        private void OnObtainFailed()
        {

        }

        private void OnObtainSuccess()
        {

        }

        private void OnShow()
        {

        }

        private void OnHide()
        {

        }
    }
}
