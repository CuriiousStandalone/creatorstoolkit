﻿using UnityEngine;
using strange.extensions.mediation.impl;

using PulseIQ.Local.Client.Unity3DLib.Model;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.InteractiveItem;
using PulseIQ.Local.Common.CustomType;
using PulseIQ.Local.Common.Model.Item;
using System.Collections.Generic;

namespace PulseIQ.Local.ClientCommon.BaseItem
{
    public class ViewItemBase : EventView
    {

        #region Base item events
        protected internal const string ABANDON_ITEM = "ABANDON_ITEM";
        protected internal const string EVENT_MOVE = "EVENT_MOVE";
        protected internal const string EVENT_ROTATE = "EVENT_ROTATE";
        protected internal const string EVENT_SCALE = "EVENT_SCALE";
        protected internal const string ATTACH_TO_HOLDING_POS = "ATTACH_TO_HOLDING_POS";
        protected internal const string ATTACH_TO_HOLDING_ROT = "ATTACH_TO_HOLDING_ROT";
        protected internal const string ATTACH_TO_HOLDING_SCALE = "ATTACH_TO_HOLDING_SCALE";
        protected internal const string OFFLINE_MOVE = "OFFLINE_MOVE";
        protected internal const string OFFLINE_ROTATE = "OFFLINE_ROTATE";
        protected internal const string OFFLINE_SCALE = "OFFLINE_SCALE";
        protected internal const string LERP_MOVE = "LERP_MOVE";
        protected internal const string LERP_ROTATE = "LERP_ROTATE";
        protected internal const string LERP_SCALE = "LERP_SCALE";
        #endregion

        protected EAbandonAction abandonAction;
        protected EObtainAction obtainAction;

        public string itemID = "";
		public int itemIndex = -1;
        public ItemTypeCode itemType;

        #region Update Position Variables
        protected bool itemMoveTo = false;
        protected float itemMoveToTime = 0;
        protected Transform itemMoveToTransform = null;
        protected Vector3 itemMoveToLocation = new Vector3();
        protected Vector3 itemMoveToTempCurrentPosition = new Vector3();
        protected Vector3 itemMoveToStartPostion = new Vector3();
        protected float itemMoveToUpdateRate = 0;
        protected float itemMoveToUpdateTimer = 0f;
        protected float itemLerpMoveTimer = 0f;
        protected bool isLerpMoving = false;
        protected float currentLerpMoveTime = 0;
        protected const float lerpMoveTime = 0.25f;
        protected virtual bool offlineMoving { get; set; }
        protected Vector3 itemReturnToLocation = new Vector3();
        protected float localMoveToSpeed = 0f;
        protected float localMoveToTime = 0f;
        protected bool interactiveLocalMoveTo = false;
        protected List<Vector3> lerpPositionList = new List<Vector3>();
        protected Vector3 lerpCurrentPos = new Vector3();
        protected Vector3 localPositionOffset = new Vector3();
        #endregion

        #region Update Rotation Variables
        protected bool itemRotateTo = false;
        protected bool offlineRotating = false;
        protected Quaternion itemMoveToRotation = new Quaternion();
        protected Quaternion itemMoveToTempCurrentRotation = new Quaternion();
        protected Quaternion itemMoveToStartRotation = new Quaternion();
        protected Quaternion localRotation = new Quaternion();
        protected Quaternion itemReturnToRotation = new Quaternion();
        protected Quaternion lerpCurrentRotation = new Quaternion();
        protected List<Quaternion> lerpRotationList = new List<Quaternion>();
        protected float itemLerpRotateTimer = 0f;
        protected float localRotateToTime = 0f;
        protected float itemRotateToUpdateTimer = 0f;
        protected float itemRotateToUpdateRate = 0f;
        protected float itemRotateToTime = 0f;
        protected float localRotateToSpeed = 0f;
        protected bool isLerpRotating = false;
        protected float currentLerpRotateTime = 0;
        protected float lerpRotateTime = 0.25f;
        #endregion

        #region Update Scale Variables
        protected bool itemScaleTo = false;
        protected bool offlineScaling = false;
        protected Vector3 itemMoveToScale = new Vector3();
        protected Vector3 itemMoveToTempCurrentScale = new Vector3();
        protected Vector3 itemMoveToStartScale = new Vector3();
        protected Vector3 localScale = new Vector3();
        protected Vector3 itemReturnToScale = new Vector3();
        protected Vector3 lerpCurrentScale = new Vector3();
        protected List<Vector3> lerpScaleList = new List<Vector3>();
        protected float itemLerpScaleTimer = 0f;
        protected float localScaleToTime = 0f;
        protected float itemScaleToUpdateTimer = 0f;
        protected float itemScaleToUpdateRate = 0f;
        protected float itemScaleToTime = 0f;
        protected float localScaleToSpeed = 0f;
        protected bool isLerpScaling = false;
        protected float currentLerpScaleTime = 0;
        protected const float lerpScaleTime = 0.25f;
        #endregion

        #region Item Base Flags 
        protected bool continueUpdatingItem = false;
        protected bool isUsingPhysics = false;
        protected bool isThrowing = false;
        protected bool isReturning = false;
        protected bool isTeleporting = false;
        protected bool stopThrowing = false;
        protected bool isObtained = false;
        protected bool mimicFlag = false;
        protected bool offlineReturn = false;
        #endregion


        virtual protected internal void init()
        {
            itemType = ItemTypeCode.ITEM;
        }

        protected override void OnDestroy()
        {
            CancelInvoke();
        }

        public void Update()
        { 
            if (itemMoveTo)
            {
                if (null == itemMoveToTransform)
                {
                    MoveToVector();
                }
                else
                {
                    MoveToTransform();
                }
            }

            if (isUsingPhysics)
            {
                if (GetComponent<Rigidbody>().velocity.magnitude <= 0.5f)
                {
                    //CancelInvoke("UpdatePos");
                    CancelInvoke("LerpUpdatePosition");
                    CancelInvoke("LerpUpdateRotation");
                    CancelInvoke("LerpUpdateScale");
                    isUsingPhysics = false;
                    GetComponent<Rigidbody>().isKinematic = true;
                    GetComponent<Rigidbody>().velocity = Vector3.zero;
                    isThrowing = false;

                    switch (abandonAction)
                    {
                        case EAbandonAction.THROW_RETURN:
                            {
                                stopThrowing = true;
                                break;
                            }

                        case EAbandonAction.THROW:
                            {
                                MessageData data = new MessageData();
                                data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);
                                dispatcher.Dispatch(ABANDON_ITEM, data);
                                break;
                            }
                    }
                }
            }

            // isReturnign not being set to true. Can maybe get rid of this function
            //if (isReturning)
            //{
            //    ReturnToVector();
            //}

            if(offlineMoving)
            {
                offlineMovement();
            }

            if (offlineRotating)
            {
                offlineRotation();
            }

            if (offlineScaling)
            {
                offlineScale();
            }

            if (isTeleporting)
            {
                TeleportItem();
            }

            if(isLerpMoving)
            {
                LerpMoveFunction();
            }

            if (isLerpRotating)
            {
                LerpRotateFunction();
            }

            if (isLerpScaling)
            {
                LerpScaleFunction();
            }
        }

        protected void LerpMoveFunction()
        {
            currentLerpMoveTime += Time.deltaTime;
            
            transform.position = Vector3.Lerp(lerpCurrentPos, lerpPositionList[0], currentLerpMoveTime / lerpMoveTime);

            if (currentLerpMoveTime >= lerpMoveTime)
            { 
                transform.position = lerpPositionList[0];
                lerpCurrentPos = transform.position;
                lerpPositionList.RemoveAt(0);
                currentLerpMoveTime = 0f;

                if (lerpPositionList.Count <= 0)
                {
                    isLerpMoving = false;
                }
            }
        }

        protected void LerpRotateFunction()
        {
            currentLerpRotateTime += Time.deltaTime;

            transform.rotation = Quaternion.Lerp(lerpCurrentRotation, lerpRotationList[0], currentLerpRotateTime / lerpRotateTime);

            if (currentLerpRotateTime >= lerpRotateTime)
            {
                transform.rotation = lerpRotationList[0];
                lerpCurrentRotation = transform.rotation;
                lerpRotationList.RemoveAt(0);
                currentLerpRotateTime = 0f;

                if (lerpRotationList.Count <= 0)
                {
                    isLerpRotating = false;
                }
            }
        }

        protected void LerpScaleFunction()
        {
            currentLerpScaleTime += Time.deltaTime;

            transform.localScale = Vector3.Lerp(lerpCurrentScale, lerpScaleList[0], currentLerpScaleTime / lerpScaleTime);

            if (currentLerpScaleTime >= lerpScaleTime)
            {
                transform.localScale = lerpScaleList[0];
                lerpCurrentScale = transform.localScale;
                lerpScaleList.RemoveAt(0);           
                currentLerpScaleTime = 0f;

                if (lerpScaleList.Count <= 0)
                {
                    isLerpScaling = false;
                }
            }
        }

        public void ReturnFunction()
        {
            if (!isReturning)
            {
                offlineMoving = false;
                stopThrowing = false;
                base.CancelInvoke("UpdateMoveToHolding");
                base.CancelInvoke("UpdateRotateToHolding");
                base.CancelInvoke("UpdateScaleToHolding");
                //CancelInvoke("UpdatePos");
                CancelInvoke("LerpUpdatePosition");
                isUsingPhysics = false;
                GetComponent<Rigidbody>().isKinematic = true;
                GetComponent<Rigidbody>().velocity = Vector3.zero;

                if(isThrowing)
                {
                    offlineReturn = true;
                }

                isThrowing = false;
                itemLerpMoveTimer = 0f;
                itemLerpRotateTimer = 0f;
                itemLerpScaleTimer = 0f;
                continueUpdatingItem = false;

                CTVector3 pos = new CTVector3(itemReturnToLocation.x, itemReturnToLocation.y, itemReturnToLocation.z);
                CTQuaternion rot = new CTQuaternion(itemReturnToRotation.x, itemReturnToRotation.y, itemReturnToRotation.z, itemReturnToRotation.w);
                CTVector3 scale = new CTVector3(itemReturnToScale.x, itemReturnToScale.y, itemReturnToScale.z);

                MessageData dataPos = new MessageData();
                MessageData dataRot = new MessageData();
                MessageData dataScale = new MessageData();

                dataPos.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);
                dataPos.Parameters.Add((byte)CommonParameterCode.MOVE_TO_POSITION, pos);

                dataRot.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);
                dataRot.Parameters.Add((byte)CommonParameterCode.MOVE_TO_ROTATION, rot);

                dataScale.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);
                dataScale.Parameters.Add((byte)CommonParameterCode.MOVE_TO_SCALE, scale);

                dispatcher.Dispatch(OFFLINE_MOVE, dataPos);
                dispatcher.Dispatch(OFFLINE_ROTATE, dataRot);
                dispatcher.Dispatch(OFFLINE_SCALE, dataScale);
            }
        }

        virtual protected void offlineMovement()
        {
            itemLerpMoveTimer += Time.deltaTime;

            transform.position = Vector3.Lerp(itemMoveToStartPostion, itemMoveToLocation, itemLerpMoveTimer / localMoveToTime);

            if (Vector3.Distance(transform.position, itemMoveToLocation) <= 0.02f)
            {
                offlineMoving = false;

                transform.position = itemMoveToLocation;

                if (interactiveLocalMoveTo && isObtained && !isThrowing)
                {
                    switch (obtainAction)
                    {
                        case EObtainAction.HOLD:
                            {                
                                InvokeRepeating("UpdateMoveToHolding", 0.2f, 0.2f);
                                break;
                            }

                        case EObtainAction.LIFT:
                            {
                                interactiveLocalMoveTo = false;
                                break;
                            }
                    }
                }
            }
        }

        virtual protected void offlineRotation()
        {
            itemLerpRotateTimer += Time.deltaTime;

            transform.rotation = Quaternion.Lerp(itemMoveToStartRotation, itemMoveToRotation, itemLerpRotateTimer / localRotateToTime);

            if (itemLerpRotateTimer >= localRotateToTime)
            {
                offlineMoving = false;

                transform.rotation = itemMoveToRotation;

                if (interactiveLocalMoveTo && isObtained && !isThrowing)
                {
                    switch (obtainAction)
                    {
                        case EObtainAction.HOLD:
                            {
                                InvokeRepeating("UpdateRotateToHolding", 0.2f, 0.2f);
                                break;
                            }

                        case EObtainAction.LIFT:
                            {
                                interactiveLocalMoveTo = false;
                                break;
                            }
                    }
                }
            }
        }

        virtual protected void offlineScale()
        {
            itemLerpScaleTimer += Time.deltaTime;

            transform.localScale = Vector3.Lerp(itemMoveToStartScale, itemMoveToScale, itemLerpScaleTimer / localScaleToTime);

            if (Vector3.Distance(transform.position, itemMoveToLocation) <= 0.02f)
            {
                offlineMoving = false;

                transform.localScale = itemMoveToScale;

                if (interactiveLocalMoveTo && isObtained && !isThrowing)
                {
                    switch (obtainAction)
                    {
                        case EObtainAction.HOLD:
                            {
                                InvokeRepeating("UpdateScaleToHolding", 0.2f, 0.2f);
                                break;
                            }

                        case EObtainAction.LIFT:
                            {
                                interactiveLocalMoveTo = false;
                                break;
                            }
                    }
                }
            }
        }

        protected void UpdateMoveToHolding()
        {
            MessageData itemData = new MessageData();
            itemData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);

            dispatcher.Dispatch(ATTACH_TO_HOLDING_POS, itemData);
        }

        protected void UpdateRotateToHolding()
        {
            MessageData itemData = new MessageData();
            itemData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);

            dispatcher.Dispatch(ATTACH_TO_HOLDING_ROT, itemData);
        }

        protected void UpdateScaleToHolding()
        {
            MessageData itemData = new MessageData();
            itemData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);

            dispatcher.Dispatch(ATTACH_TO_HOLDING_SCALE, itemData);
        }

        protected void MoveToTransform()
        {
            itemMoveToUpdateTimer += Time.deltaTime;
            itemLerpMoveTimer += Time.deltaTime;

            itemMoveToTempCurrentPosition = Vector3.Lerp(itemMoveToStartPostion, itemMoveToTransform.position + localPositionOffset, itemLerpMoveTimer / itemMoveToTime);

            if (Vector3.Distance(itemMoveToTempCurrentPosition, itemMoveToTransform.position + localPositionOffset) <= 0.02f)
            {
                Move(itemMoveToTransform.position + localPositionOffset);
                itemMoveToUpdateTimer = 0;
                itemMoveTo = false;

                if (continueUpdatingItem)
                {
                    InvokeRepeating("AttachItem", itemMoveToUpdateRate, itemMoveToUpdateRate);
                }
            }

            else if (itemMoveToUpdateTimer >= itemMoveToUpdateRate)
            {
                Move(itemMoveToTempCurrentPosition);
                itemMoveToUpdateTimer = 0;
            }
        }

        protected void RotateToTransform()
        {
            itemRotateToUpdateTimer += Time.deltaTime;
            itemLerpRotateTimer += Time.deltaTime;

            itemMoveToTempCurrentRotation = Quaternion.Lerp(itemMoveToStartRotation, localRotation, itemLerpRotateTimer / itemRotateToTime);

            if (itemLerpRotateTimer >= itemRotateToTime)
            {
                Rotate(localRotation);
                itemRotateToUpdateTimer = 0;
                itemRotateTo = false;

                if (continueUpdatingItem)
                {
                    InvokeRepeating("AttachItem", itemMoveToUpdateRate, itemMoveToUpdateRate);
                }
            }

            else if (itemMoveToUpdateTimer >= itemMoveToUpdateRate)
            {
                Rotate(itemMoveToTempCurrentRotation);
                itemRotateToUpdateTimer = 0;
            }
        }

        protected void ScaleToTransform()
        {
            itemScaleToUpdateTimer += Time.deltaTime;
            itemLerpScaleTimer += Time.deltaTime;

            itemMoveToTempCurrentScale = Vector3.Lerp(itemMoveToStartScale, localScale, itemLerpScaleTimer / itemScaleToTime);

            if (itemLerpScaleTimer >= itemScaleToTime)
            {
                Scale(localScale);
                itemScaleToUpdateTimer = 0;
                itemScaleTo = false;

                if (continueUpdatingItem)
                {
                    InvokeRepeating("AttachItem", itemMoveToUpdateRate, itemMoveToUpdateRate);
                }
            }

            else if (itemMoveToUpdateTimer >= itemMoveToUpdateRate)
            {
                Scale(itemMoveToTempCurrentScale);
                itemScaleToUpdateTimer = 0;
            }
        }

        protected void MoveToVector()
        {
            itemMoveToUpdateTimer += Time.deltaTime;
            itemLerpMoveTimer += Time.deltaTime;

            itemMoveToTempCurrentPosition = Vector3.Lerp(itemMoveToStartPostion, itemMoveToLocation, itemLerpMoveTimer / itemMoveToTime);

            if (Vector3.Distance(itemMoveToTempCurrentPosition, itemMoveToLocation) <= 0.02f)
            {
                Move(itemMoveToLocation);
                itemMoveToUpdateTimer = 0;
                itemMoveTo = false;

                mimicFlag = true;
            }

            else if (itemMoveToUpdateTimer >= itemMoveToUpdateRate)
            {
                Move(itemMoveToTempCurrentPosition);
                itemMoveToUpdateTimer = 0;
            }
        }

        protected void RotateToVector()
        {
            itemRotateToUpdateTimer += Time.deltaTime;
            itemLerpRotateTimer += Time.deltaTime;

            itemMoveToTempCurrentRotation = Quaternion.Lerp(itemMoveToStartRotation, itemMoveToRotation, itemLerpRotateTimer / itemRotateToTime);

            if (itemLerpRotateTimer >= itemRotateToTime)
            {
                Rotate(itemMoveToRotation);
                itemRotateToUpdateTimer = 0;
                itemRotateTo = false;

                mimicFlag = true;
            }

            else if (itemRotateToUpdateTimer >= itemRotateToUpdateRate)
            {
                Rotate(itemMoveToTempCurrentRotation);
                itemRotateToUpdateTimer = 0;
            }
        }

        protected void ScaleToVector()
        {
            itemScaleToUpdateTimer += Time.deltaTime;
            itemLerpScaleTimer += Time.deltaTime;

            itemMoveToTempCurrentScale = Vector3.Lerp(itemMoveToStartScale, itemMoveToScale, itemLerpScaleTimer / itemScaleToTime);

            if (itemLerpScaleTimer >= itemScaleToTime)
            {
                Scale(itemMoveToScale);
                itemLerpScaleTimer = 0;
                itemScaleTo = false;

                mimicFlag = true;
            }

            else if (itemScaleToUpdateTimer >= itemScaleToUpdateRate)
            {
                Scale(itemMoveToTempCurrentScale);
                itemScaleToUpdateTimer = 0;
            }
        }

        // Dont think it is being used anymore. Might get ride of this
        //protected void ReturnToVector()
        //{
        //    itemMoveToUpdateTimer += Time.deltaTime;
        //    itemLerpMoveTimer += Time.deltaTime;

        //    itemMoveToTempCurrentPosition = Vector3.Lerp(itemMoveToStartPostion, itemMoveToLocation, itemLerpMoveTimer / itemMoveToTime);
        //    itemMoveToTempCurrentRotation = itemMoveToRotation;//Quaternion.Lerp(itemMoveToStartRotation, itemMoveToRotation, itemLerpTimer / itemMoveToTime);
        //    itemMoveToTempCurrentScale = Vector3.Lerp(itemMoveToStartScale, itemMoveToScale, itemLerpMoveTimer / itemMoveToTime);

        //    if (Vector3.Distance(itemMoveToTempCurrentPosition, itemMoveToLocation) <= 0.02f)
        //    {
        //        Move(itemMoveToLocation);
        //        Rotate(itemMoveToRotation);
        //        Scale(itemMoveToScale);

        //        itemMoveToUpdateTimer = 0;

        //        MessageData data = new MessageData();
        //        data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);
        //        dispatcher.Dispatch(ABANDON_ITEM, data);
        //    }

        //    else if (itemMoveToUpdateTimer >= itemMoveToUpdateRate)
        //    {
        //        Move(itemMoveToTempCurrentPosition);
        //        Rotate(itemMoveToTempCurrentRotation);
        //        Scale(itemMoveToTempCurrentScale);

        //        itemMoveToUpdateTimer = 0;
        //    }
        //}

        protected void TeleportItem()
        {
            if (Vector3.Distance(transform.position, itemReturnToLocation) <= 0.5f)
            {
                MessageData data = new MessageData();
                data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);
                dispatcher.Dispatch(ABANDON_ITEM, data);

                isTeleporting = false;
            }
        }

        // Not beign called anywhere. Can maybe remove.
        //protected void AttachToTransform()
        //{
        //    Move(itemMoveToTransform.position, itemMoveToTransform.rotation, itemMoveToTransform.localScale);
        //}

        public void SetItemID(string id)
        {
            this.name = id;
            itemID = id;
        }

        /// <summary>
        /// Move to specified position
        /// </summary>
        public void Move(Vector3 destPostion)
        {
            CTVector3 pos = new CTVector3(destPostion.x, destPostion.y, destPostion.z);

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEM_POSITION, pos);
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);

            dispatcher.Dispatch(EVENT_MOVE, data);
        }

        public void Rotate(Quaternion destRotation)
        {
            CTQuaternion rot = new CTQuaternion(destRotation.x, destRotation.y, destRotation.z, destRotation.w);

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEM_ROTATION, rot);
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);

            dispatcher.Dispatch(EVENT_ROTATE, data);
        }

        public void Scale(Vector3 destScale)
        {
            CTVector3 scale = new  CTVector3(destScale.x, destScale.y, destScale.z);

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEM_SCALE, scale);
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);

            dispatcher.Dispatch(EVENT_SCALE, data);
        }

        /// </summary>
        /// Function to send out items position to the server to lerp.
        /// <summary>
        public void LerpMove(Vector3 destPostion)
        {
            CTVector3 pos = new CTVector3(destPostion.x, destPostion.y, destPostion.z);

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEM_POSITION, pos);
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);

            dispatcher.Dispatch(LERP_MOVE, data);
        }

        /// </summary>
        /// Function to send out items rotation to the server to lerp.
        /// <summary>
        public void LerpRotate(Quaternion destRotation)
        {
            CTQuaternion rot = new CTQuaternion(destRotation.x, destRotation.y, destRotation.z, destRotation.w);

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEM_ROTATION, rot);
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);

            dispatcher.Dispatch(LERP_ROTATE, data);
        }

        /// </summary>
        /// Function to send out items scale to the server to lerp.
        /// <summary>
        public void LerpScale(Vector3 destScale)
        {
            CTVector3 scale = new CTVector3(destScale.x, destScale.y, destScale.z);

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEM_SCALE, scale);
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);

            dispatcher.Dispatch(LERP_SCALE, data);
        }

        /// <summary>
        /// Moves to destination with a given speed. Server only provides the destination, there's no sync when moving.
        /// </summary>
        /// <param name="destPostion">Destination postion.</param>
        /// <param name="destRotation">Destination rotation.</param>
        /// <param name="speed">Speed.</param>
        /// <param name="updateRate">Amount of updates per second. Default = 5.</param>
        //public void UpdateMoveTo(Vector3 destPostion, Quaternion destRotation, Vector3 destScale, float speed = Settings.ItemMoveToSpeed, float updateRate = Settings.ItemMoveToUpdateRate)
        //{
        //    itemLerpMoveTimer = 0f;
        //    continueUpdatingItem = false;
        //    this.itemMoveTo = true;
        //    itemMoveToLocation = destPostion;
        //    itemMoveToRotation = destRotation;
        //    itemMoveToScale = destScale;
        //    itemMoveToUpdateRate = 1f / updateRate;
        //    itemMoveToTempCurrentPosition = transform.position;
        //    itemMoveToStartPostion = transform.position;
        //    itemMoveToStartRotation = transform.rotation;
        //    itemMoveToStartScale = transform.localScale;
        //    itemMoveToTime = Vector3.Distance(itemMoveToStartPostion, itemMoveToLocation) / (speed * 5);
        //}

        /// <summary>
        /// Moves to destination with a given speed. Server only provides the destination, there's no sync when moving.
        /// </summary>
        /// <param name="destTransform">Destination's transform.</param>
        /// <param name="speed">Speed.</param>
        /// <param name="updateRate">Amount of updates per second. Default = 5.</param>
        //public void UpdateMoveTo(Transform destTransform, bool continueUpdating, float speed = Settings.ItemMoveToSpeed, float updateRate = Settings.ItemMoveToUpdateRate)
        //{
        //    itemLerpTimer = 0f;
        //    this.itemMoveTo = true;
        //    itemMoveToTransform = destTransform;
        //    itemMoveToLocation = itemMoveToTransform.position;
        //    itemMoveToUpdateRate = 1f / updateRate;
        //    itemMoveToTempCurrentPosition = transform.position;
        //    itemMoveToStartPostion = transform.position;
        //    itemMoveToStartRotation = transform.rotation;
        //    itemMoveToStartScale = transform.localScale;
        //    itemMoveToTime = Vector3.Distance(itemMoveToStartPostion, itemMoveToLocation) / speed;
        //    continueUpdatingItem = continueUpdating;
        //
        //    localPositionOffset = Vector3.zero;
        //    localScale = itemMoveToTransform.localScale;
        //    localRotation = itemMoveToTransform.localRotation;
        //}

        /// <summary>
        /// Moves to destination with a given speed. Server only provides the destination, there's no sync when moving.
        /// </summary>
        /// <param name="destTransform">Destination's transform.</param>
        /// <param name="speed">Speed.</param>
        /// <param name="updateRate">Amount of updates per second. Default = 5.</param>
        //public void UpdateMoveTo(Transform destTransform, bool continueUpdating, Vector3 localPosition, Vector3 localScale, Quaternion localRotation, float speed = Settings.ItemMoveToSpeed, float updateRate = Settings.ItemMoveToUpdateRate)
        //{
        //    itemLerpTimer = 0f;
        //    this.itemMoveTo = true;
        //    itemMoveToTransform = destTransform;
        //    itemMoveToLocation = itemMoveToTransform.position;
        //    itemMoveToUpdateRate = 1f / updateRate;
        //    itemMoveToTempCurrentPosition = transform.position;
        //    itemMoveToStartPostion = transform.position;
        //    itemMoveToStartRotation = transform.rotation;
        //    itemMoveToTime = Vector3.Distance(itemMoveToStartPostion, itemMoveToLocation) / speed;
        //    continueUpdatingItem = continueUpdating;
        //
        //    this.localPositionOffset = localPosition;
        //    this.localRotation = localRotation;
        //    this.localScale = localScale;
        //}

        /// <summary>
        /// Updates items position.
        /// </summary>
        public void UpdatePosition(CTVector3 itemPose)
        {
            if(offlineMoving)
            {
                offlineMoving = false;
            }

            if (!isUsingPhysics)
            {
                Vector3 newPosition = new Vector3(itemPose.X, itemPose.Y, itemPose.Z);
                transform.position = newPosition;
            }
        }

        /// <summary>
        /// Updates items rotation.
        /// </summary>
        public void UpdateRotation(CTQuaternion itemRotation)
        {
            if (offlineMoving)
            {
                offlineMoving = false;
            }

            if (!isUsingPhysics)
            {
                Quaternion newRotation = new Quaternion(itemRotation.X, itemRotation.Y, itemRotation.Z, itemRotation.W);
                transform.rotation = newRotation;
            }
        }

        /// <summary>
        /// Updates items rotation.
        /// </summary>
        public void UpdateScale(CTVector3 itemScale)
        {
            if (offlineMoving)
            {
                offlineMoving = false;
            }

            if (!isUsingPhysics)
            {
                Vector3 newScale = new Vector3(itemScale.X, itemScale.Y, itemScale.Z);
                transform.localScale = newScale;
            }
        }

        /// <summary>
        /// Moves to destination with a given speed. Server only provides the destination, there's no sync when moving.
        /// </summary>
        /// <param name="destPostion">Destination postion.</param>
        /// <param name="destRotation">Destination rotation.</param>
        /// <param name="speed">Speed.</param>
        /// <param name="updateRate">Amount of updates per second. Default = 5.</param>
        public void LocalMoveTo(Vector3 destPostion, float speed = Settings.ItemMoveToSpeed)
        {
            offlineMoving = true;
            itemMoveToStartPostion = transform.position;
            itemMoveToStartRotation = transform.rotation;
            itemMoveToLocation = destPostion;
            localMoveToSpeed = speed;
            float distance = Vector3.Distance(transform.position, destPostion);
            itemLerpMoveTimer = 0f;
            //localMoveToTime = distance / speed;
            localMoveToTime = 1f;
        }

        /// <summary>
        /// Rotates to destination rotation with a given speed. Server only provides the rotation, there's no sync when moving.
        /// </summary>
        /// <param name="destPostion">Destination postion.</param>
        /// <param name="destRotation">Destination rotation.</param>
        /// <param name="speed">Speed.</param>
        /// <param name="updateRate">Amount of updates per second. Default = 5.</param>
        public void LocalRotateTo(Quaternion destRotation, float speed = Settings.ItemRotateToSpeed)
        {
            offlineRotating = true;
            itemMoveToStartRotation = transform.rotation;
            itemMoveToRotation = destRotation;
            localRotateToSpeed = speed;
            itemLerpRotateTimer = 0f;
            localRotateToTime = 1f;
        }

        /// <summary>
        /// Scales to destination scale with a given speed. Server only provides the scale, there's no sync when moving.
        /// </summary>
        /// <param name="destPostion">Destination postion.</param>
        /// <param name="destRotation">Destination rotation.</param>
        /// <param name="speed">Speed.</param>
        /// <param name="updateRate">Amount of updates per second. Default = 5.</param>
        public void LocalScaleTo(Vector3 destScale, float speed = Settings.ItemScaleToSpeed)
        {
            offlineScaling = true;
            itemMoveToStartScale = transform.localScale;
            itemMoveToScale = destScale;
            localScaleToSpeed = speed;
            itemLerpScaleTimer = 0f;
            localScaleToTime = 1f;
        }

        virtual public void HideView()
        {
            foreach (Animator anim in GetComponentsInChildren<Animator>())
            {
                anim.enabled = false;
            }

            foreach (Renderer renderer in GetComponentsInChildren<Renderer>())
            {
                renderer.enabled = false;
            }

            if(null != gameObject.GetComponent<Collider>())
            {
                gameObject.GetComponent<Collider>().enabled = false;
            }

            for(int i = 0; i < transform.childCount; i++)
            {
                if(null != transform.GetChild(i).gameObject.GetComponent<Collider>())
                {
                    transform.GetChild(i).gameObject.GetComponent<Collider>().enabled = false;
                }
            }
        }

        virtual public void ShowView()
        {
            foreach (Animator anim in GetComponentsInChildren<Animator>())
            {
                anim.enabled = true;
            }

            foreach (Renderer renderer in GetComponentsInChildren<Renderer>())
            {
                renderer.enabled = true;
            }

            if (null != gameObject.GetComponent<Collider>())
            {
                gameObject.GetComponent<Collider>().enabled = true;
            }

            for (int i = 0; i < transform.childCount; i++)
            {
                if (null != transform.GetChild(i).gameObject.GetComponent<Collider>())
                {
                    transform.GetChild(i).gameObject.GetComponent<Collider>().enabled = true;
                }
            }
        }

        public void InvokeUpdatePosition()
        {
            InvokeRepeating("LerpUpdatePosition", 0.25f, lerpMoveTime);
        }

        public void InvokeUpdateRotation()
        {
            InvokeRepeating("LerpUpdateRotation", 0.25f, lerpRotateTime);
        }

        public void InvokeUpdateScale()
        {
            InvokeRepeating("LerpUpdateScale", 0.25f, lerpScaleTime);
        }

        public void LerpUpdatePosition()
        {
            LerpMove(transform.position);
        }

        public void LerpUpdateRotation()
        {
            LerpRotate(transform.rotation);
        }

        public void LerpUpdateScale()
        {
            LerpScale(transform.localScale);
        }

        private void UpdatePos()
        {
            Move(transform.position);
        }

        private void UpdateRot()
        {
            Rotate(transform.rotation);
        }

        private void UpdateScale()
        {
            Scale(transform.localScale);
        }

        public void CancelUpdating()
        {
            isUsingPhysics = false;
            CancelInvoke("LerpUpdatePosition");
            CancelInvoke("LerpUpdateRotation");
            CancelInvoke("LerpUpdateScale");

            GetComponent<Rigidbody>().isKinematic = true;
            GetComponent<Rigidbody>().velocity = Vector3.zero;
        }

        virtual public void ObtainedItem()
        {
            isObtained = true;
        }

        virtual public void ItemAbandoned()
        {
            isObtained = false;
        }

        virtual public void ObtainedItemFailed()
        {
            // Do something
        }

        public void SetItemDefaultReturnPositions(Vector3 position, Quaternion rotation, Vector3 scale)
        {
            itemReturnToLocation = position;
            itemReturnToRotation = rotation;
            itemReturnToScale = scale;
        }

        public void SetAbandonAction(EAbandonAction action)
        {
            abandonAction = action;
        }

        public void SetObtainAction(EObtainAction action)
        {
            obtainAction = action;
        }

        public bool ReturnIsObtained()
        {
            return isObtained;
        }

        public EAbandonAction ReturnAbandonAction()
        {
            return abandonAction;
        }

        public EObtainAction ReturnObtainAction()
        {
            return obtainAction;
        }

        public void SetOfflineReturn(bool value)
        {
            offlineReturn = value;
        }

        public bool GetOfflineReturn()
        {
            return offlineReturn;
        }

        virtual public void SyncItemData(CTItemData data)
        {
            Vector3 pos = new Vector3(data.Position.X, data.Position.Y, data.Position.Z);
            Quaternion rot = new Quaternion(data.Rotation.X, data.Rotation.Y, data.Rotation.Z, data.Rotation.W);
            Vector3 scale = new Vector3(data.Scale.X, data.Scale.Y, data.Scale.Z);

            transform.position = pos;
            transform.rotation = rot;
            transform.localScale = scale;
        }

        /// <summary>
        /// Function to lerp the current transforms position. Callback from server gets to here
        /// </summary>
        virtual public void LerpMovement(CTVector3 Position)
        {
            if (!isUsingPhysics)
            {
                Vector3 pos = new Vector3(Position.X, Position.Y, Position.Z);

                if (lerpPositionList.Count <= 0)
                {
                    lerpCurrentPos = transform.position;
                }

                lerpPositionList.Add(pos);

                isLerpMoving = true;
            }
        }

        /// <summary>
        /// Function to lerp the current transforms rotation. Callback from server gets to here
        /// </summary>
        virtual public void LerpRotation(CTQuaternion Rotation)
        {
            if (!isUsingPhysics)
            {
                Quaternion rot = new Quaternion(Rotation.X, Rotation.Y, Rotation.Z, Rotation.W);

                if (lerpPositionList.Count <= 0)
                {
                    lerpCurrentRotation = transform.rotation;
                }

                lerpRotationList.Add(rot);

                isLerpRotating = true;
            }
        }

        /// <summary>
        /// Function to lerp the current transforms scale. Callback from server gets to here
        /// </summary>
        virtual public void LerpScale(CTVector3 Scale)
        {
            if (!isUsingPhysics)
            {
                Vector3 scale = new Vector3(Scale.X, Scale.Y, Scale.Z);

                if (lerpPositionList.Count <= 0)
                {
                    lerpCurrentScale = transform.localScale;
                }

                lerpScaleList.Add(scale);

                isLerpScaling = true;
            }
        }

        virtual public void SlotGroupSynced(float elapsedTime)
        {
            Debug.Log("Base Item!!!!!!!!!!!  " + elapsedTime);

            if (elapsedTime < 0)
            {
                return;
            }
            else
            {
                foreach (Animator anim in GetComponentsInChildren<Animator>())
                {
                    anim.enabled = true;

                    if (elapsedTime < anim.GetCurrentAnimatorStateInfo(0).length)
                    {
                        float time = elapsedTime / anim.GetCurrentAnimatorStateInfo(0).length;
                        anim.Play(0, 0, time);
                    }
                    else
                    {
                        if (anim.GetCurrentAnimatorStateInfo(0).loop)
                        {
                            float time = elapsedTime % anim.GetCurrentAnimatorStateInfo(0).length;
                            time = time / anim.GetCurrentAnimatorStateInfo(0).length;
                            anim.Play(0, 0, time);
                        }
                        else
                        {
                            anim.Play(0, 0, 1f);
                        }
                    }
                }

                foreach (Renderer renderer in GetComponentsInChildren<Renderer>())
                {
                    renderer.enabled = true;
                }

                if (null != gameObject.GetComponent<Collider>())
                {
                    gameObject.GetComponent<Collider>().enabled = true;
                }

                for (int i = 0; i < transform.childCount; i++)
                {
                    if (null != transform.GetChild(i).gameObject.GetComponent<Collider>())
                    {
                        transform.GetChild(i).gameObject.GetComponent<Collider>().enabled = true;
                    }
                }
            }
        }
    }
}
