﻿using UnityEngine;

using RenderHeads.Media.AVProVideo;

using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.BaseItem;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.Multimedia;

using System.Collections;

namespace PulseIQ.Local.ClientCommon.PlanarVideoItem
{
    public class ViewPlanarVideoPlayer : ViewItemBase, IMediaBasic, IVideoBasic
    {
        enum PlayerStatus
        {
            NONE,
            PLAY,
            PLAY_SEEK,
            PAUSE,
            RESTART,
            SEEK,
        }

        PlayerStatus playerStatus;

        GameObject mediaPlayerObject;
        public MediaPlayer mediaPlayer { get; private set; }

        [SerializeField]
        private MediaPlayer inspectorMediaPlayer;

        internal const string LOADED = "LOADED";
        internal const string SHOWN = "SHOWN";
        internal const string HIDDEN = "HIDDEN";
        internal const string UPDATE_TIMER = "UPDATE_TIMER";

        private string mediaName;

        private bool isLoaded = false;
        public bool isShowing = false;
        string shownItemID = "";
        float currentVideoFrame = 0f;
        float maxVideoTime = 0f;
        private bool EndBehaviourTriggered;

        private MessageData timerData = new MessageData();

        private EMultimediaEndBehaviorCode endBehaviorCode;

        protected override internal void init()
        {
            //inspectorMediaPlayer = transform.Find("MediaPlayer").GetComponent<MediaPlayer>();
            
            itemType = Local.Common.ItemTypeCode.PLANAR_VIDEO;
            timerData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);
        }

        public void InitializeMedia(GameObject player)
        {
            mediaPlayerObject = player;
            inspectorMediaPlayer = mediaPlayerObject.transform.Find("MediaPlayer").GetComponent<MediaPlayer>();
            mediaPlayer = inspectorMediaPlayer;
            EndBehaviourTriggered = false;
            mediaPlayer.Events.AddListener(OnVideoEvent);

            //LoadMedia(Settings.PlanarVideoPath + mediaName);
        }

        public string MediaName
        {
            get
            {
                return mediaName;
            }

            set
            {
                mediaName = value;
            }

        }

        private void Update()
        {
            base.Update();

            if (isShowing)
            {
                if (!EndBehaviourTriggered && maxVideoTime != 0 && mediaPlayer.Control.GetCurrentTimeMs() > (maxVideoTime - 1000))
                {
                    EndBehaviourTriggered = true;
                    PauseMedia(maxVideoTime - 1000);
                }
            }
        }

        protected override void OnDestroy()
        {
            mediaPlayer.Events.RemoveListener(OnVideoEvent);
            UnloadMedia();
        }

        public void HideMedia()
        {
            playerStatus = PlayerStatus.NONE;

            mediaPlayerObject.transform.GetChild(0).gameObject.SetActive(false);
            StopMedia();
            mediaPlayer.CloseVideo();

            isShowing = false;

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);

            dispatcher.Dispatch(HIDDEN, data);

            isShowing = false;
        }

        public void LoadMedia(string path)
        {
            isLoaded = false;
            EndBehaviourTriggered = false;

            if (mediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, path, false))
            {
                Debug.Log("Planar video loaded!");

                maxVideoTime = mediaPlayer.Info.GetDurationMs();
            }
            else
            {
                Debug.Log("Failed to load Planar Video File");
            }
        }

        public void PauseMedia(float frame)
        {
            if (!isLoaded)
            {
                playerStatus = PlayerStatus.PAUSE;
                currentVideoFrame = frame;
                return;
            }

            SeekMedia(frame);
            mediaPlayer.Pause();
        }

        public void PlayMedia(float frame)
        {
            if (!isLoaded)
            {
                playerStatus = PlayerStatus.PLAY;
                currentVideoFrame = frame;
                return;
            }

            SeekMedia(frame);
            mediaPlayer.Play();

            InvokeRepeating("SendCurrentTime", 0.5f, 0.5f);
        }

        public void RestartMedia()
        {
            if (!isLoaded)
            {
                playerStatus = PlayerStatus.RESTART;
                return;
            }

            mediaPlayer.Rewind(false);
        }

        public void SeekMedia(float frame)
        {
            if (!isLoaded)
            {
                if (playerStatus == PlayerStatus.PLAY_SEEK || playerStatus == PlayerStatus.SEEK)
                {
                    currentVideoFrame = frame;
                    return;
                }
                if (playerStatus == PlayerStatus.PLAY)
                {
                    playerStatus = PlayerStatus.PLAY_SEEK;
                }
                else
                {
                    playerStatus = PlayerStatus.SEEK;
                }

                currentVideoFrame = frame;
                return;
            }

            mediaPlayer.Control.SeekFast(frame);
        }

        // Change to
        public void ShowMedia(Vector3 cameraPos, Quaternion cameraRot)
        {
            shownItemID = itemID;
            LoadMedia(Settings.PlanarVideoPath + mediaName);
            mediaPlayerObject.transform.GetChild(0).gameObject.SetActive(true);

            mediaPlayerObject.transform.position = cameraPos;
            Vector3 camRotation = cameraRot.eulerAngles;
            Vector3 transRot = transform.eulerAngles;
            transRot.y = camRotation.y;
            transRot.x = camRotation.x;
            mediaPlayerObject.transform.eulerAngles = new Vector3(0, 180, 0);//Quaternion.Euler(transRot);

            isShowing = true;
        }

        public IEnumerator IShowMedia(Vector3 cameraPos, Quaternion cameraRot)
        {
            shownItemID = itemID;
            LoadMedia(Settings.PlanarVideoPath + mediaName);
            mediaPlayerObject.transform.GetChild(0).gameObject.SetActive(true);

            mediaPlayerObject.transform.position = cameraPos;
            Vector3 camRotation = cameraRot.eulerAngles;
            Vector3 transRot = transform.eulerAngles;
            transRot.y = camRotation.y;
            transRot.x = camRotation.x;
            mediaPlayerObject.transform.eulerAngles = new Vector3(0, 180, 0);//Quaternion.Euler(transRot);

            yield return 0;
        }

        public void StopMedia()
        {
            mediaPlayer.Stop();
            CancelInvoke("SendCurrentTime");
            SendDefaultStartTime();
        }

        public void UnloadMedia()
        {
            mediaPlayer.Stop();
            mediaPlayer.CloseVideo();
            //Destroy(mediaPlayer);
            //mediaPlayer = null;
        }

        public void ShowMedia(Vector3 cameraPos)
        {
            return;
        }

        public void SendCurrentTime()
        {
            timerData.Parameters.Clear();
            timerData.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);
            timerData.Parameters.Add((byte)CommonParameterCode.VIDEO_CURRENT_TIME, mediaPlayer.Control.GetCurrentTimeMs());

            dispatcher.Dispatch(UPDATE_TIMER, timerData);
        }

        public void SendDefaultStartTime()
        {
            timerData.Parameters.Clear();
            timerData.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);
            timerData.Parameters.Add((byte)CommonParameterCode.VIDEO_CURRENT_TIME, 0f);

            dispatcher.Dispatch(UPDATE_TIMER, timerData);
        }

        public override void SyncItemData(CTItemData data)
        {
            base.SyncItemData(data);
        } 

        public void DestroyPlanarMediaPlayer()
        {
            if (null != mediaPlayerObject)
            {
                Destroy(mediaPlayerObject.transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().material);
                Destroy(mediaPlayerObject.transform.GetChild(0).gameObject);
                Destroy(mediaPlayerObject);                
            }
        }

        public void OnVideoEvent(MediaPlayer mp, MediaPlayerEvent.EventType et, ErrorCode errorCode)
        {
            switch (et)
            {
                case MediaPlayerEvent.EventType.ReadyToPlay:
                    {
                        if (shownItemID == itemID)
                        {
                            maxVideoTime = mediaPlayer.Info.GetDurationMs();

                            if (!isLoaded)
                            {
                                MessageData mData = new MessageData();
                                mData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);

                                dispatcher.Dispatch(LOADED, mData);
                                isLoaded = true;
                            }

                            Invoke("DelayedLoadedFunction", 1f);

                            isShowing = true;
                            shownItemID = "";
                        }
                        break;
                    }
            }
            Debug.Log("Event: " + et.ToString());
        }

        void DelayedLoadedFunction()
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);
            data.Parameters.Add((byte)CommonParameterCode.MEDIA_NAME, mediaName);
            data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, itemType);
            data.Parameters.Add((byte)CommonParameterCode.VIDEO_TOTAL_TIME, mediaPlayer.Info.GetDurationMs());

            dispatcher.Dispatch(SHOWN, data);
        }

        public void TeleportMedia(string hotspotId, int targetItemIndex)
        {
            //foreach (var hotspot in hotspotItems)
            //{
            //    ViewHotspot viewHotspot = hotspot.GetComponent<ViewHotspot>();

            //    if (viewHotspot.ReturnHotspotID() == hotspotId)
            //    {
            //        viewHotspot.ShowTeleportMedia(ItemTypeCode.PLANAR_VIDEO);
            //    }
            //}
        }

        public EMultimediaEndBehaviorCode EndBehaviourCode
        {
            get
            {
                return endBehaviorCode;
            }

            set
            {
                endBehaviorCode = value;
            }
        }

        public bool IsMultiplayer { get; set; }
    }
}
