﻿using UnityEngine;

using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.Item;

namespace PulseIQ.Local.ClientCommon.PlanarVideoItem
{
    public class MediatorPlanarVideoPlayer : EventMediator
    {
        [Inject]
        public ViewPlanarVideoPlayer view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewPlanarVideoPlayer.LOADED, OnLoaded);
            view.dispatcher.UpdateListener(value, ViewPlanarVideoPlayer.SHOWN, OnShown);
            view.dispatcher.UpdateListener(value, ViewPlanarVideoPlayer.HIDDEN, OnHidden);
            view.dispatcher.UpdateListener(value, ViewPlanarVideoPlayer.UPDATE_TIMER, OnUpdateTimer);
        }

        private void OnMessageReceived(System.Object data)
        {
            MessageData planarData = (MessageData)data;
            ServerCmdCode code = (ServerCmdCode)planarData[(byte)CommonParameterCode.OPERATION_CODE];

            switch (code)
            {
                case ServerCmdCode.ALL_MULTIMEDIA_CLOSED:
                case ServerCmdCode.PLANAR_VIDEO_HIDDEN:
                    {
                        if (!view.isShowing)
                        {
                            return;
                        }

                        view.HideMedia();
                        dispatcher.Dispatch(IntCmdCodePlanarVideo.RETURN_CAMERA_ROT);
                        dispatcher.Dispatch(ECommonViewEvent.DROPDOWN_ITEM_HIDDEN, planarData);
                        break;
                    }

                case ServerCmdCode.PLANAR_VIDEO_SHOWN:
                    {
                        Vector3 camPos = (Vector3)planarData.Parameters[(byte)CommonParameterCode.MAIN_CAMERA_POSITION];
                        Quaternion camRot = (Quaternion)planarData.Parameters[(byte)CommonParameterCode.MAIN_CAMERA_ROTATION];

                        StartCoroutine(view.IShowMedia(camPos, camRot));
                        dispatcher.Dispatch(IntCmdCodePlanarVideo.ROTATE_CAMERA);
                        //view.ShowMedia(camPos, camRot);
                        break;
                    }

                case ServerCmdCode.PLANAR_VIDEO_PLAYED:
                    {
                        int frame = (int)planarData[(byte)CommonParameterCode.VIDEO_FRAME];
                        view.PlayMedia(frame);
                        break;
                    }

                case ServerCmdCode.PLANAR_VIDEO_PAUSED:
                    {
                        int frame = (int)planarData[(byte)CommonParameterCode.VIDEO_FRAME];
                        view.PauseMedia(frame);
                        break;
                    }

                case ServerCmdCode.PLANAR_VIDEO_REPLAYED:
                    {
                        view.RestartMedia();
                        break;
                    }

                case ServerCmdCode.PLANAR_VIDEO_STOPPED:
                    {
                        view.StopMedia();
                        break;
                    }
                case ServerCmdCode.PLANAR_VIDEO_SOUGHT:
                    {
                        int frame = (int)planarData[(byte)CommonParameterCode.VIDEO_FRAME];
                        view.SeekMedia(frame);
                        break;
                    }

                case ServerCmdCode.ITEM_STATUS_SYNCED:
                    {
                        CTItemData syncData = (CTItemData)(planarData[(byte)CommonParameterCode.DATA_CT_ITEM_DATA]);
                        view.SyncItemData(syncData);
                        break;
                    }
            }
        }

        private void OnLoaded(IEvent evt)
        {
            dispatcher.Dispatch(IntCmdCodePlanarVideo.LOADED, evt.data);
        }

        private void OnShown(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.MEDIA_SHOW, evt.data);
            dispatcher.Dispatch(ECommonViewEvent.SHOW_VR, evt.data);
        }

        private void OnHidden(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.MEDIA_HIDE, evt.data);
            dispatcher.Dispatch(ECommonViewEvent.HIDE_VR, evt.data);
        }

        private void OnUpdateTimer(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.MEDIA_CURRENT_TIME, evt.data);
        }

    }
}
