﻿using strange.extensions.command.impl;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace PulseIQ.Local.ClientCommon.PlanarVideoItem
{
    public class OpCmdShownPlanarVideo : EventCommand
    {
        [Inject]
        public IItemDataset model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;
            data.Parameters.Add((byte)CommonParameterCode.OPERATION_CODE, evt.type);
            data.Parameters.Add((byte)CommonParameterCode.MAIN_CAMERA_POSITION, model.mainCamera.transform.position);
            data.Parameters.Add((byte)CommonParameterCode.MAIN_CAMERA_ROTATION, model.mainCamera.transform.rotation);

            dispatcher.Dispatch(IntCmdCodePlanarVideo.SHOWN, data);
        }
    }
}