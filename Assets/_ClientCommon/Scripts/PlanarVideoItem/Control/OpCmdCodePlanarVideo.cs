﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.PlanarVideoItem
{
    public enum OpCmdCodePlanarVideo : byte
    {
        CREATE,
        START,
        PAUSE,
        RESET,
        STOP,
        SEEK,
        SHOW,
        HIDE,
        LOAD,
    }
}