﻿using UnityEngine;
using strange.extensions.command.impl;

using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.ItemManager;
using PulseIQ.Local.ClientCommon.BaseItem;

namespace PulseIQ.Local.ClientCommon.PlanarVideoItem
{
    public class OpCmdPausePlanarVideo : EventCommand
    {
        [Inject]
        public IItemDataset model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;
            string itemID = (string)data.Parameters[(byte)CommonParameterCode.ITEMID];
            float currentFrame = -1f;

            foreach (ViewItemBase itemBase in model.itemList)
            {
                if (itemID == itemBase.itemID)
                {
                    currentFrame = itemBase.GetComponent<ViewPlanarVideoPlayer>().mediaPlayer.Control.GetCurrentTimeMs();
                }
            }

            if (currentFrame == -1f)
            {
                Debug.LogWarning("Frame was not set in Panorama Video Player for pause!");
            }
            else
            {
                OperationsPlanarVideo.PausePlanarVideo(ClientBridge.Instance, ClientBridge.Instance.CurUserID, itemID, (int)currentFrame);
            }
        }
    }
}