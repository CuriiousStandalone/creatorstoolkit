﻿namespace PulseIQ.Local.ClientCommon.PlanarVideoItem
{
    public enum IntCmdCodePlanarVideo : byte
    {
        CREATED,
        STARTED,
        PAUSED,
        RESTARTED,
        STOPPED,
        SOUGHT,
        SHOWN,
        HIDDEN,
        LOADED,
        ROTATE_CAMERA,
        RETURN_CAMERA_ROT,
    }
}