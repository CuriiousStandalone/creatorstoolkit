﻿using strange.extensions.command.impl;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.BaseItem;
using PulseIQ.Local.ClientCommon.AvatarItem;

namespace PulseIQ.Local.ClientCommon.LineNotationItem
{
    public class OpCmdHiddenLineNotation : EventCommand
    {
        public override void Execute()
        { 
            MessageData data = (MessageData)evt.data;
            data.Parameters.Add((byte)CommonParameterCode.OPERATION_CODE, evt.type);

            dispatcher.Dispatch(IntCmdCodeLineNotation.HIDDEN, data);
        }
    }
}