﻿namespace PulseIQ.Local.ClientCommon.LineNotationItem
{
    public enum IntCmdCodeLineNotation : byte
    {
        CREATED,
        SHOWN,
        HIDDEN,
        STARTED,
        START,
        STOPPED,
        UNDO_LINE,
        CLEARED,
        COLOR_CHANGED,
        THICKNESS_CHANGED,
        POINTS_ADDED,
        SYNC_STATUS,
        ALL_UI_HIDDEN,
        ALL_UI_SHOWN,
    }
}