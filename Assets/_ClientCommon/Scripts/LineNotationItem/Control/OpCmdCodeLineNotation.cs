﻿namespace PulseIQ.Local.ClientCommon.LineNotationItem
{
    public enum OpCmdCodeLineNotation : byte
    {
        CREATE,
        SHOW,
        HIDE,
        START,
        STOP,
        UNDO_LINE,
        ADD_LINE_PACKAGE,
        CLEAR,
        CHANGE_COLOR,
        CHANGE_THICKNESS,    
    }
}