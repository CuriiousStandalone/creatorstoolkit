﻿using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.LineNotationItem
{
    public class IntCmdAllUIShown : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(IntCmdCodeLineNotation.ALL_UI_SHOWN);
        }
    }
}