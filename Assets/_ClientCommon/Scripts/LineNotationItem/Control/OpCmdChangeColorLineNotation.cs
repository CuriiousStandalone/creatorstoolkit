﻿using strange.extensions.command.impl;

using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.CustomType;

namespace PulseIQ.Local.ClientCommon.LineNotationItem
{
    public class OpCmdChangeColorLineNotation : EventCommand
    {
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            string itemID = (string)data[(byte)CommonParameterCode.ITEMID];
            CTVector4 color = (CTVector4)data[(byte)CommonParameterCode.NOTATION_COLOR];

            OperationsLineNotation.UpdateLineColor(ClientBridge.Instance, ClientBridge.Instance.CurUserID, ClientBridge.Instance.CurWorldData.Id, itemID, color);
        }
    }
}