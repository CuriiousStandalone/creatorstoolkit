﻿using strange.extensions.command.impl;

using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Client.Unity3DLib.Model;
using PulseIQ.Local.Common.CustomType;

namespace PulseIQ.Local.ClientCommon.LineNotationItem
{
    public class OpCmdStartLineNotation : EventCommand
    {
        [Inject]
        public IItemDataset model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            string itemID = (string)data[(byte)CommonParameterCode.ITEMID];
            DataLineNotationCamera camData = (DataLineNotationCamera)data[(byte)CommonParameterCode.DATA_LINE_NOTATION_CAMERA];
            camData.CamPosition = new CTVector3(model.mainCamera.transform.position.x, model.mainCamera.transform.position.y, model.mainCamera.transform.position.z);
            camData.CamRotation = new CTQuaternion(model.mainCamera.transform.rotation.x, model.mainCamera.transform.rotation.y, model.mainCamera.transform.rotation.z, model.mainCamera.transform.rotation.w);

            OperationsLineNotation.StartLineNotation(ClientBridge.Instance, ClientBridge.Instance.CurUserID, ClientBridge.Instance.CurWorldData.Id, itemID, camData.NotatedObjectItemId, camData.IsLocalCoordinate, camData.CamPosition, camData.CamRotation);
        }
    }
}