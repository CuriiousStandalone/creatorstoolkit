﻿using strange.extensions.command.impl;

using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.CustomType;

namespace PulseIQ.Local.ClientCommon.LineNotationItem
{
    public class OpCmdAddPointsLineNotation : EventCommand
    {
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            string itemID = (string)data[(byte)CommonParameterCode.ITEMID];
            string lineID = (string)data[(byte)CommonParameterCode.NOTATION_LINEID];
            int packageIndex = (int)data[(byte)CommonParameterCode.NOTATION_PACKAGE_INDEX];
            CTVector3[] points = (CTVector3[])data[(byte)CommonParameterCode.NOTATION_POINTLIST];

            OperationsLineNotation.AddTouchPoints(ClientBridge.Instance, ClientBridge.Instance.CurUserID, ClientBridge.Instance.CurWorldData.Id, itemID, packageIndex, lineID, points);
        }
    }
}