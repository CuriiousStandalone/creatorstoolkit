﻿using strange.extensions.command.impl;

using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace PulseIQ.Local.ClientCommon.LineNotationItem
{
    public class OpCmdUndoLineNotation : EventCommand
    {
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            string itemID = (string)data[(byte)CommonParameterCode.ITEMID];

            OperationsLineNotation.UndoLineNotation(ClientBridge.Instance, ClientBridge.Instance.CurUserID, ClientBridge.Instance.CurWorldData.Id, itemID);
        }
    }
}