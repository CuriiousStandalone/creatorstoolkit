﻿namespace PulseIQ.Local.ClientCommon.LineNotationItem
{
    public enum ENotationType
    {
        NONE,
        PLANAR_VIDEO,
        PANORAMA,
        PANORAMA_VIDEO,
        WORLD,
        TUTORIAL_ITEM,
        STEREO_PANORAMA_VIDEO,
    }
}