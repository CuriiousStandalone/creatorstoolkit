﻿using strange.extensions.command.impl;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.BaseItem;
using PulseIQ.Local.ClientCommon.AvatarItem;

namespace PulseIQ.Local.ClientCommon.LineNotationItem
{
    public class OpCmdStartedLineNotation : EventCommand
    {
        [Inject]
        public IItemDataset model { get; set; }

        public override void Execute()
        {
            foreach (ViewItemBase item in model.itemList)
            {
                if (item.itemType == ItemTypeCode.AVATAR)
                {
                    item.GetComponent<ViewAvatar>().HideAvatarNotation(true);
                }
            }

            MessageData data = (MessageData)evt.data;
            data.Parameters.Add((byte)CommonParameterCode.OPERATION_CODE, evt.type);
            data.Parameters.Add((byte)CommonParameterCode.MAIN_CAMERA_POSITION, model.mainCamera.transform.position);
            data.Parameters.Add((byte)CommonParameterCode.MAIN_CAMERA_ROTATION, model.mainCamera.transform.rotation);
            data.Parameters.Add((byte)CommonParameterCode.MAIN_CAMERA_FORWARD_DIR, model.mainCamera.transform.forward);

            dispatcher.Dispatch(IntCmdCodeLineNotation.STARTED, data);
        }
    }
}