﻿using UnityEngine;

using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Client.Unity3DLib.Model;
using PulseIQ.Local.Common.CustomType;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.LineNotation;

namespace PulseIQ.Local.ClientCommon.LineNotationItem
{
    public class MediatorItemLineNotation : EventMediator
    {
        [Inject]
        public ViewItemLineNotation view { get; set; }


        public override void OnRegister()
        {
            view.init();
            UpdateListeners(true);
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewItemLineNotation.NotationEvents.SEND_POINT_DATA, OnSendPointData);
            view.dispatcher.UpdateListener(value, ViewItemLineNotation.NotationEvents.CHANGE_LINE_THICKNESS, OnSendNewThickness);
            view.dispatcher.UpdateListener(value, ViewItemLineNotation.NotationEvents.HIDING_NOTATION, OnHideNotation);
            view.dispatcher.UpdateListener(value, ViewItemLineNotation.NotationEvents.START_NOTATION, OnStartNotation);
            view.dispatcher.UpdateListener(value, ViewItemLineNotation.NotationEvents.CHANGE_LINE_COLOR, OnSendNewLineColor);
            view.dispatcher.UpdateListener(value, ViewItemLineNotation.NotationEvents.DRAWDISTANCE_SET, OnDrawDistanceSet);
            view.dispatcher.UpdateListener(value, ViewItemLineNotation.NotationEvents.WORLD_NOTATION_START, OnWorldNotationStart);
            view.dispatcher.UpdateListener(value, ViewItemLineNotation.NotationEvents.WORLD_NOTATION_ENDED, OnWorldNotationEnded);
            view.dispatcher.UpdateListener(value, ViewItemLineNotation.NotationEvents.WORLD_NOTATION_STARTED, OnWorldNotationStarted);
            view.dispatcher.UpdateListener(value, ViewItemLineNotation.NotationEvents.WORLD_NOTATION_STARTED_AFTER_SYNC, OnWorldNotationStartedAfterSync);
            //view.dispatcher.UpdateListener(value, ViewItemLineNotation.NotationEvents.TUTORIAL_ITEM_NOTATION_START, OnTutorialItemNotationStart);
            //view.dispatcher.UpdateListener(value, ViewItemLineNotation.NotationEvents.TUTORIAL_ITEM_NOTATION_END, OnTutorialItemNotationEnd);
            //
            //// Player
            //view.dispatcher.UpdateListener(value, ViewItemLineNotation.SHOW_ITEM_CAMERA, OnShowItemCamera);
            //view.dispatcher.UpdateListener(value, ViewItemLineNotation.HIDE_ITEM_CAMERA, OnHideItemCamera);
            //view.dispatcher.UpdateListener(value, ViewItemLineNotation.SHOW_TUTORIAL_CAMERA, OnShowTutorialCamera);
            //view.dispatcher.UpdateListener(value, ViewItemLineNotation.HIDE_TUTORIAL_CAMERA, OnHideTutorialCamera);

            //dispatcher.UpdateListener(value, EEventNotation.NOTATION_OBJECT_FOUND, OnNotationItemFound);
            dispatcher.UpdateListener(value, IntCmdCodeLineNotation.ALL_UI_HIDDEN, OnAllUIHidden);
            dispatcher.UpdateListener(value, IntCmdCodeLineNotation.ALL_UI_SHOWN, OnAllUIShown);
        }

        private void OnSendPointData(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.AddTouchPointsToLineNotation, evt.data);
        }

        private void OnMessageReceived(System.Object data)
        {
            MessageData notationData = (MessageData)data;
            ServerCmdCode code = (ServerCmdCode)notationData[(byte)CommonParameterCode.OPERATION_CODE];
            
            switch (code)
            {
                case ServerCmdCode.NOTATION_STARTED:
                    {
                        DataLineNotationCamera camData = (DataLineNotationCamera)notationData.Parameters[(byte)CommonParameterCode.DATA_LINE_NOTATION_CAMERA];
                        Vector3 mainCameraPos = (Vector3)notationData.Parameters[(byte)CommonParameterCode.MAIN_CAMERA_POSITION];
                        Quaternion mainCameraRot = (Quaternion)notationData.Parameters[(byte)CommonParameterCode.MAIN_CAMERA_ROTATION];
                        Vector3 mainCameraForwardDir = (Vector3)notationData.Parameters[(byte)CommonParameterCode.MAIN_CAMERA_FORWARD_DIR];
                        view.StartLine(camData, mainCameraPos, mainCameraRot, mainCameraForwardDir);
                        break;
                    }

                case ServerCmdCode.NOTATION_SHOWN:
                    {
                        MessageData newMsgData = new MessageData();
                        newMsgData.Parameters.Add((byte)CommonParameterCode.ITEMID, view.mediaItemIDReference);
                        newMsgData.Parameters.Add((byte)CommonParameterCode.OPERATION_CODE, ServerCmdCode.NOTATION_SHOWN);

                        dispatcher.Dispatch(ECommonViewEvent.NOTATION_SHOWN, newMsgData);

                        view.ShowView();

                        break;
                    }

                case ServerCmdCode.NOTATION_HIDDEN:
                    {
                        //view.HideView();
                        break;
                    }

                case ServerCmdCode.NOTATION_THICKNESS_UPDATED:
                    {
                        //float thickness = (float)notationData.Parameters[(byte)CommonParameterCode.NOTATION_THICKNESS];
                        //view.ChangeLineThickness(thickness);
                        break;
                    }

                case ServerCmdCode.NOTATION_COLOR_UPDATED:
                   {
                        CTVector4 color = (CTVector4)notationData.Parameters[(byte)CommonParameterCode.NOTATION_COLOR];
                        view.SetDefaultColour(color);
                       break;
                   }

                case ServerCmdCode.NOTATION_CLEARED:
                    {
                        view.ClearLineRendererList();
                        break;
                    }

                case ServerCmdCode.NOTATION_LINE_REMOVED:
                    {
                        view.UndoLineRender();
                        break;
                    }
            
                case ServerCmdCode.NOTATION_ADD_TOUCH_POINTS:
                    {
                        DataTouchPointPackage touchData = (DataTouchPointPackage)notationData.Parameters[(byte)CommonParameterCode.DATA_TOUCHPOINT_PACKAGE];
                        view.TouchPointDataReceived(touchData);
                        break;
                    }

                case ServerCmdCode.NOTATION_STOPPED:
                    {
                        MessageData newMsgData = new MessageData();
                        newMsgData.Parameters.Add((byte)CommonParameterCode.ITEMID, view.mediaItemIDReference);
                        newMsgData.Parameters.Add((byte)CommonParameterCode.OPERATION_CODE, ServerCmdCode.NOTATION_STOPPED);

                        dispatcher.Dispatch(ECommonViewEvent.NOTATION_HIDDEN, newMsgData);

                        view.HideView();
                        break;
                    }

                case ServerCmdCode.ITEM_STATUS_SYNCED:
                    {
                        CTItemData syncData = (CTItemData)(notationData[(byte)CommonParameterCode.DATA_CT_ITEM_DATA]);
                        view.SyncItemData(syncData);
                        break;
                    }

                case ServerCmdCode.SYNC_NOTATION_STATUS:
                    {
                        bool isVisible = (bool)notationData[(byte)CommonParameterCode.IS_VISIBLE];

                        if(!isVisible)
                        {
                            return;
                        }

                        CTLineNotationData lineNotationData = (CTLineNotationData)notationData[(byte)CommonParameterCode.DATA_LINE_NOTATION];
                        Vector3 mainCameraPos = (Vector3)notationData.Parameters[(byte)CommonParameterCode.MAIN_CAMERA_POSITION];
                        Quaternion mainCameraRot = (Quaternion)notationData.Parameters[(byte)CommonParameterCode.MAIN_CAMERA_ROTATION];
                        Vector3 mainCameraForwardDir = (Vector3)notationData.Parameters[(byte)CommonParameterCode.MAIN_CAMERA_FORWARD_DIR];

                        //view.ShowView();
                        view.SyncNotationStatus(lineNotationData, mainCameraPos, mainCameraRot, mainCameraForwardDir);
                        break;
                    }
            }
        }

        private void OnSendNewThickness(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.UpdateLineNotationThickness, evt.data);
        }
        
        private void OnSendNewLineColor(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.UpdateLineNotationColor, evt.data);
        }
        
        private void OnHideNotation(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.MEDIA_SHOW, evt.data);
            dispatcher.Dispatch(ECommonViewEvent.NOTATION_HIDE, evt.data);
            dispatcher.Dispatch(ECommonViewEvent.NOTATION_OFF, evt.data);
        }
        
        private void OnStartNotation(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.NOTATION_SHOW, evt.data);
            dispatcher.Dispatch(ECommonViewEvent.NOTATION_ON, evt.data);
        }
        
        private void OnDrawDistanceSet(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.NOTATION_DRAWDISTANCE_SET, evt.data);
        }

        private void OnWorldNotationStart(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.WORLD_NOTATE_START, evt.data);
        }

        private void OnWorldNotationStarted(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.WORLD_NOTATION_STARTED, evt.data);
        }

        private void OnWorldNotationStartedAfterSync(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.WORLD_NOTATION_STARTED, evt.data);
        }

        private void OnWorldNotationEnded(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.WORLD_NOTATION_ENDED, evt.data);
            dispatcher.Dispatch(ECommonViewEvent.NOTATION_OFF, evt.data);
        }

        private void OnAllUIHidden()
        {
            view.AllUIHidden();
        }

        private void OnAllUIShown()
        {
            view.AllUIShown();
        }

        //
        //private void OnWorldNotationEnd(IEvent evt)
        //{
        //    dispatcher.Dispatch(EEventNotation.NOTATION_END_WORLD);
        //}
        //
        //private void OnTutorialItemNotationStart(IEvent evt)
        //{
        //    dispatcher.Dispatch(EEventNotation.NOTATION_START_TUTORIAL_ITEM);
        //}
        //
        //private void OnTutorialItemNotationEnd(IEvent evt)
        //{
        //    dispatcher.Dispatch(EEventNotation.NOTATION_END_TUTORIAL_ITEM);
        //}
        //
        //private void OnShowItemCamera(IEvent evt)
        //{
        //    dispatcher.Dispatch(EEventItem.WORLD_NOTATION_END);
        //}
        //
        //private void OnHideItemCamera(IEvent evt)
        //{
        //    dispatcher.Dispatch(EEventItem.WORLD_NOTATION_START);
        //}
        //
        //private void OnShowTutorialCamera()
        //{
        //    dispatcher.Dispatch(EEventItem.TUTORIAL_NOTATION_END);
        //}
        //
        //private void OnHideTutorialCamera()
        //{
        //    dispatcher.Dispatch(EEventItem.TUTORIAL_NOTATION_START);
        //}
        //
        //private void OnNotationItemFound(IEvent evt)
        //{
        //    SItemMessageData notationData = (SItemMessageData)evt.data;
        //    view.StartLine((DataLineNotationCamera)notationData.data);
        //}
    }
}