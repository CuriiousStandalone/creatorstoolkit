﻿using System.Collections.Generic;
using UnityEngine;

using strange.extensions.mediation.impl;

using PulseIQ.Local.Client.Unity3DLib.Model;
using PulseIQ.Local.Common.CustomType;

namespace PulseIQ.Local.ClientCommon.LineNotationItem
{
    public class ViewItemLineRenderer : EventView
    {
        public float drawDistance;
        private TrailRenderer lineRenderer = null;
        public Vector3 startPosition;
        Vector3 TempVec = new Vector3(0, 0, 0);

        public ENotationType notationType;
        public string lineID;

        int pointIndex = 0;
        int packageIndex = 1;

        public List<CTVector3> pointList = new List<CTVector3>();
        private List<DataTouchPointPackage> tempTouchData = new List<DataTouchPointPackage>();

        protected internal void init()
        {

        }

        private void TestFunction(object sedner, System.EventArgs args)
        {

        }

        protected override void Start()
        {
            lineRenderer = GetComponent<TrailRenderer>();
        }

        private void Update()
        {
            if (pointList.Count > 0 && pointList.Count > pointIndex)
            {
                Vector3 TempVec = new Vector3(pointList[pointIndex].X, pointList[pointIndex].Y, pointList[pointIndex].Z);

                transform.position = startPosition + TempVec;

                pointIndex++;
            }
        }


        public void SetLineRenderer()
        {
            lineRenderer = GetComponent<TrailRenderer>();
        }

        public void SetColorAndThickness(Color color, float thickness)
        {
            lineRenderer.material.color = color;
            lineRenderer.startColor = color;
            lineRenderer.endColor = color;
            lineRenderer.startWidth = thickness;
            lineRenderer.endWidth = thickness;
        }

        public void AddPointDataToList(DataTouchPointPackage touchData)
        {
            if (packageIndex == touchData.TouchPointPackageIndex)
            {
                foreach (CTVector3 position in touchData.TouchPoints)
                {
                    pointList.Add(position);
                }
                packageIndex++;
                CheckTempList();
            }
            else
            {
                tempTouchData.Add(touchData);
            }
        }

        public void Clear() => pointList.Clear();

        public void AddPointDataToList(List<CTVector3> points) => pointList.AddRange(points);

        private void CheckTempList()
        {
            if (tempTouchData.Count > 0)
            {
                for (int i = 0; i < tempTouchData.Count; ++i)
                {
                    if (tempTouchData[i].TouchPointPackageIndex == packageIndex)
                    {
                        foreach (CTVector3 position in tempTouchData[i].TouchPoints)
                        {
                            pointList.Add(position);
                        }

                        packageIndex++;
                        tempTouchData.RemoveAt(i);

                        if (tempTouchData.Count > 0)
                        {
                            CheckTempList();
                            return;
                        }
                        else
                        {
                            return;
                        }
                    }
                }
            }
        }
    }
}