﻿using System.Collections.Generic;
using UnityEngine;

using PulseIQ.Local.Client.Unity3DLib.Model;
using PulseIQ.Local.Common.CustomType;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.BaseItem;
using PulseIQ.Local.ClientCommon.TutorialItem;
using CTItemData = PulseIQ.Local.Common.Model.Item.CTItemData;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using PulseIQ.Local.Common.Model.LineNotation;

//NEED TO CHANGE
//using PulseIQ.Local.ControlClient.Item;

namespace PulseIQ.Local.ClientCommon.LineNotationItem
{
    public class ViewItemLineNotation : ViewItemBase
    {
        internal enum NotationEvents
        {
            SEND_POINT_DATA,
            CHANGE_LINE_THICKNESS,
            CHANGE_LINE_COLOR,
            HIDING_NOTATION,
            START_NOTATION,
            WORLD_NOTATION_START,
            WORLD_NOTATION_END,
            TUTORIAL_ITEM_NOTATION_START,
            TUTORIAL_ITEM_NOTATION_END,
            DRAWDISTANCE_SET,
            WORLD_NOTATION_STARTED,
            WORLD_NOTATION_STARTED_AFTER_SYNC,
            WORLD_NOTATION_ENDED,
        }

        public ENotationType notationType = ENotationType.NONE;

        private bool isLocalCoord = true;
        public string mediaItemIDReference = "";

        private bool isActive = false;
        private bool allUIHidden = false;
        private Camera mainCamera;

        [SerializeField]
        private GameObject lineRendererPrefab;

        private List<ViewItemLineRenderer> lineRendererList = new List<ViewItemLineRenderer>();
        private List<CTVector3> trailPostions = new List<CTVector3>();

        private float drawDistance;
        private Plane notationPlane;

        public string lineID;
        private int packageIndex = 1;
        float lineThickness = 0f;

        //Default Values
        private Color lineColour = new Color(0, 1, 1, 1);

        private bool isNotating = false;
        private float timer = 0f;
        float timerIntervals = 0f;
        float msgRate = 0.25f;

        Ray mRay = new Ray();

        Vector3 drawPosition = new Vector3();

        public GameObject notatedObject;

        private GameObject NotationCollider;

        internal const string SHOW_LINE_NOTATION = "HIDE_ITEM_CAMERA";
        internal const string HIDE_LINE_NOTATION = "SHOW_ITEM_CAMERA";

        private bool isPlayer = false;
        private bool released = false;

        bool hoveredButton = false;

        protected internal override void init()
        {
            itemType = Local.Common.ItemTypeCode.NOTATION_LINE;

            if(Settings.applicationType == EApplicationType.PLAYER_CLIENT)
            {
                isPlayer = true;
            }
            else
            {
                isPlayer = false;
            }
        }

        private void Update()
        {
            if (isActive && !isPlayer && !allUIHidden)
            {
                if ((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began) || Input.GetMouseButtonDown(0) && !isNotating)
                {
                    int id = 0;

                    // Check if UI was pressed
                    if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() || UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(0))
                    {
                        hoveredButton = true;
                        return;
                    }

                    Ray mRay = mainCamera.ScreenPointToRay(Input.mousePosition);
                    drawPosition = (mRay.direction * drawDistance);
                    CTVector3 position = new CTVector3(drawPosition.x, drawPosition.y, drawPosition.z);

                    trailPostions.Add(position);

                    released = false;
                    lineID = "LineID" + lineRendererList.Count;
                    isNotating = true;
                }

                else if (((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved) || Input.GetMouseButton(0)) && isNotating && !hoveredButton)
                {
                    Ray mRay = mainCamera.ScreenPointToRay(Input.mousePosition);

                    timer += Time.deltaTime;

                    if (timer >= timerIntervals)
                    {
                        drawPosition = (mRay.direction * drawDistance);
                        CTVector3 position = new CTVector3(drawPosition.x, drawPosition.y, drawPosition.z);

                        trailPostions.Add(position);
                        timerIntervals += 0.02f;
                    }

                    if (timer >= msgRate)
                    {

                        MessageData packageData = new MessageData();
                        packageData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);
                        packageData.Parameters.Add((byte)CommonParameterCode.NOTATION_POINTLIST, trailPostions.ToArray());
                        packageData.Parameters.Add((byte)CommonParameterCode.NOTATION_LINEID, lineID);
                        packageData.Parameters.Add((byte)CommonParameterCode.NOTATION_PACKAGE_INDEX, packageIndex);
                        
                        dispatcher.Dispatch(NotationEvents.SEND_POINT_DATA, packageData);

                        timer = 0f;
                        timerIntervals = 0f;
                        packageIndex++;
                        trailPostions.Clear();
                    }
                }

                if (((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) || Input.GetMouseButtonUp(0)))
                {
                    if (hoveredButton)
                    {
                        hoveredButton = false;
                        return;
                    }

                    if (trailPostions.Count > 0)
                    {
                        MessageData packageData = new MessageData();
                        packageData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);
                        packageData.Parameters.Add((byte)CommonParameterCode.NOTATION_POINTLIST, trailPostions.ToArray());
                        packageData.Parameters.Add((byte)CommonParameterCode.NOTATION_LINEID, lineID);
                        packageData.Parameters.Add((byte)CommonParameterCode.NOTATION_PACKAGE_INDEX, packageIndex);

                        dispatcher.Dispatch(NotationEvents.SEND_POINT_DATA, packageData);
                    }

                    timer = 0f;
                    timerIntervals = 0f;
                    trailPostions.Clear();
                    packageIndex = 1;
                    released = true; 
                    isNotating = false;
                }
            }
        }

        public override void ShowView()
        {
            isActive = true;

            switch (notationType)
            {
                case ENotationType.WORLD:
                    {
                        isLocalCoord = false;
                        dispatcher.Dispatch(NotationEvents.WORLD_NOTATION_START);
                        break;
                    }
                case ENotationType.TUTORIAL_ITEM:
                    {
                        isLocalCoord = false;
                        dispatcher.Dispatch(NotationEvents.TUTORIAL_ITEM_NOTATION_START);
                        break;
                    }
                default:
                    {
                        isLocalCoord = true;
                        break;
                    }
            }

            DataLineNotationCamera camData = new DataLineNotationCamera();
            camData.NotatedObjectItemId = mediaItemIDReference;
            camData.IsLocalCoordinate = isLocalCoord;

            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);
            msgData.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);
            msgData.Parameters.Add((byte)CommonParameterCode.MEDIA_REFERENCE_ID, mediaItemIDReference);
            msgData.Parameters.Add((byte)CommonParameterCode.DATA_LINE_NOTATION_CAMERA, camData);

            dispatcher.Dispatch(NotationEvents.START_NOTATION, msgData);
        }

        public override void HideView()
        {
            isActive = false;

            if(null != NotationCollider)
            {
                Destroy(NotationCollider);
            }

            if(notationType == ENotationType.WORLD)
            {
                MessageData data = new MessageData();
                dispatcher.Dispatch(NotationEvents.WORLD_NOTATION_ENDED, data);
            }
            else if(notationType == ENotationType.TUTORIAL_ITEM)
            {
                dispatcher.Dispatch(NotationEvents.TUTORIAL_ITEM_NOTATION_END);
            }
            else
            {
                MessageData data = new MessageData();
                data.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, mediaItemIDReference);
                data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);
                data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, ItemTypeCode.NOTATION_LINE);

                dispatcher.Dispatch(NotationEvents.HIDING_NOTATION, data);
            }

            notationType = ENotationType.NONE;

            ClearLineRendererList();
        }

        public void SetDrawDistance(ENotationType type, Vector3 cameraPos, Vector3 cameraForwardDir)
        {
            notationType = type;

            if(null == mainCamera)
            {
                return;
            }

            switch (notationType)
            {
                case ENotationType.PANORAMA:
                    {
                        int layerMask = LayerMask.GetMask("VROverlay");

                        mRay = mainCamera.ScreenPointToRay(Input.mousePosition);
                        mRay.origin = mRay.GetPoint(100f);
                        mRay.direction = -mRay.direction;
                        RaycastHit rayHit;

                        if (Physics.Raycast(mRay, out rayHit, layerMask))
                        {
                            Vector3 rightPos = transform.position + ((mainCamera.transform.forward + mainCamera.transform.right) / 2f).normalized * Vector3.Distance(rayHit.point, mainCamera.transform.position); ;
                            Vector3 leftPos = transform.position + ((mainCamera.transform.forward - mainCamera.transform.right) / 2f).normalized * Vector3.Distance(rayHit.point, mainCamera.transform.position); ;
                            drawDistance = Vector3.Distance(transform.position, (rightPos + leftPos) / 2f);

                            notationPlane = new Plane(transform.forward, transform.position + (transform.forward * drawDistance));
                        }

                        else
                        {
                            Debug.Log("----------------                        NO HIT");
                        }

                        notationPlane = new Plane(mainCamera.transform.forward, mainCamera.transform.position + (mainCamera.transform.forward * drawDistance));

                        break;
                    }

                case ENotationType.PANORAMA_VIDEO:
                    {
                        int layerMask = LayerMask.GetMask("VROverlay");

                        mRay = mainCamera.ScreenPointToRay(Input.mousePosition);
                        mRay.origin = mRay.GetPoint(100f);
                        mRay.direction = -mRay.direction;
                        RaycastHit rayHit;

                        if (Physics.Raycast(mRay, out rayHit))
                        {
                            Vector3 rightPos = transform.position + ((mainCamera.transform.forward + mainCamera.transform.right) / 2f).normalized * Vector3.Distance(rayHit.point, mainCamera.transform.position); ;
                            Vector3 leftPos = transform.position + ((mainCamera.transform.forward - mainCamera.transform.right) / 2f).normalized * Vector3.Distance(rayHit.point, mainCamera.transform.position); ;
                            drawDistance = Vector3.Distance(transform.position, (rightPos + leftPos) / 2f);

                            notationPlane = new Plane(transform.forward, transform.position + (transform.forward * drawDistance));
                        }

                        else 
                        {
                            Debug.Log("NO HIT");
                        }

                        notationPlane = new Plane(mainCamera.transform.forward, mainCamera.transform.position + (mainCamera.transform.forward * drawDistance));

                        break;
                    }

                case ENotationType.PLANAR_VIDEO:
                    {
                        int layerMask = LayerMask.GetMask("VROverlay");

                        Ray mRay = new Ray(mainCamera.transform.position, mainCamera.transform.forward);

                        RaycastHit rayHit;

                        if(Physics.Raycast(mRay, out rayHit, layerMask))
                        {
                            drawDistance = Vector3.Distance(rayHit.point, mainCamera.transform.position) * 0.85f;
                            notationPlane = new Plane(mainCamera.transform.forward, mainCamera.transform.position + (mainCamera.transform.forward * drawDistance));
                        }

                        else
                        {
                            Debug.Log("NO HIT");
                        }
                        

                        break;
                    }

                case ENotationType.WORLD:
                    {
                        NotationCollider = Resources.Load("Prefabs/NotationCollider") as GameObject;
                        NotationCollider = Instantiate(NotationCollider, mainCamera.transform.position, Quaternion.identity);
                        NotationCollider.name = "NOTATION COLLIDER";
                        NotationCollider.GetComponent<NotationCollider>().cam = mainCamera;
                        NotationCollider.GetComponent<NotationCollider>().forwardVector = mainCamera.transform.forward;
                        NotationCollider.GetComponent<NotationCollider>().notationItem = this;
                
                        break;
                    }
                
                case ENotationType.TUTORIAL_ITEM:
                    {
                        NotationCollider = Resources.Load("Prefabs/TutorialNotationCollider") as GameObject;
                        NotationCollider = Instantiate(NotationCollider, mainCamera.transform.position, Quaternion.identity);
                        NotationCollider.name = "NOTATION COLLIDER";
                        NotationCollider.GetComponent<NotationCollider>().eNotationType = ENotationType.TUTORIAL_ITEM;
                        NotationCollider.GetComponent<NotationCollider>().notatedObjItemId = notatedObject.GetComponent<ViewTutorialItem>().itemID;
                        NotationCollider.GetComponent<NotationCollider>().cam = mainCamera;
                        NotationCollider.GetComponent<NotationCollider>().forwardVector = mainCamera.transform.forward;
                        NotationCollider.GetComponent<NotationCollider>().notationItem = this;
                
                        break;
                    }
                
                case ENotationType.NONE:
                    {
                       // NotationCollider = Resources.Load("Prefabs/NotationCollider") as GameObject;
                       // NotationCollider = Instantiate(NotationCollider, mainCamera.transform.position, Quaternion.identity);
                       // NotationCollider.GetComponent<NotationCollider>().forwardVector = mainCamera.transform.forward;
                       // NotationCollider.GetComponent<NotationCollider>().notationItem = this;
                
                        break;
                    }

                default:
                    {
                        break;
                    }
            }
        }

        public void SetDrawDistance(Vector3 objectLocation)
        {
            Debug.Log(drawDistance = Mathf.Abs(Vector3.Distance(mainCamera.transform.position, objectLocation)));
            if (drawDistance == 0)
            {
                drawDistance = 4f;
            }
            notationPlane = new Plane(mainCamera.transform.forward, mainCamera.transform.position + (mainCamera.transform.forward * drawDistance));

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);
            data.Parameters.Add((byte)CommonParameterCode.NOTATION_DRAWDISTANCE, drawDistance);

            dispatcher.Dispatch(NotationEvents.DRAWDISTANCE_SET, data);

        }

        public void StartLine(DataLineNotationCamera startData, Vector3 mainCameraPos, Quaternion mainCameraRot, Vector3 mainCameraFordwardDir)
        {
            if (!startData.IsLocalCoordinate)
            {
                transform.position =  new Vector3(startData.CamPosition.X, startData.CamPosition.Y, startData.CamPosition.Z);
                transform.eulerAngles = Vector3.zero;
                notationType = ENotationType.WORLD;
            }
            else
            {
                transform.position = mainCameraPos;
                transform.eulerAngles = Vector3.zero;
            }

            if(notationType == ENotationType.PLANAR_VIDEO)
            {
                Quaternion temp = new Quaternion(startData.CamRotation.X, startData.CamRotation.Y, startData.CamRotation.Z, startData.CamRotation.W);
                transform.eulerAngles = new Vector3(transform.eulerAngles.x, temp.eulerAngles.y, transform.eulerAngles.z);
            }

            SetDrawDistance(notationType, mainCameraPos, mainCameraFordwardDir);

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);
            data.Parameters.Add((byte)CommonParameterCode.NOTATION_DRAWDISTANCE, drawDistance);

            dispatcher.Dispatch(NotationEvents.DRAWDISTANCE_SET, data);

            if (notationType == ENotationType.WORLD)
            {
                MessageData camData = new MessageData();
                camData.Parameters.Add((byte)CommonParameterCode.DATA_LINE_NOTATION_CAMERA, startData);

                dispatcher.Dispatch(NotationEvents.WORLD_NOTATION_STARTED, camData);
            }
        }

        public void TouchPointDataReceived(DataTouchPointPackage touchData)
        {
            if(!isActive)
            {
                return;
            }

            // Added a manual release check. This is because even though a new touch point was added, it had the same package ID as previous
            //if (!released)
            //{
                foreach (ViewItemLineRenderer renderer in lineRendererList)
                {
                    if (renderer.lineID == touchData.LineId)
                    {
                        renderer.AddPointDataToList(touchData);
                        return;
                    }
                }
            //}

            if (touchData.TouchPoints.Count > 0)
            {
                //GameObject objSpawned = Instantiate(lineRendererPrefab, transform.TransformPoint(new Vector3(touchData.TouchPoints[0].X, touchData.TouchPoints[0].Y, touchData.TouchPoints[0].Z)), transform.rotation);
                GameObject objSpawned = Instantiate(lineRendererPrefab, transform.position + new Vector3(touchData.TouchPoints[0].X, touchData.TouchPoints[0].Y, touchData.TouchPoints[0].Z), transform.rotation);
                SetDefaultThickness(touchData.Thickness);
                SetDefaultColour(touchData.LineColor);
                ViewItemLineRenderer newLR = objSpawned.GetComponent<ViewItemLineRenderer>();
                newLR.SetLineRenderer();
                newLR.startPosition = transform.position;
                newLR.drawDistance = drawDistance - (0.005f * lineRendererList.Count);
                newLR.SetColorAndThickness(lineColour, lineThickness);
                newLR.lineID = touchData.LineId;

                lineRendererList.Add(newLR);
                newLR.AddPointDataToList(touchData);
            }
        }

        public void UndoLineRender()
        {
            if (lineRendererList.Count > 0)
            {
                Destroy(lineRendererList[lineRendererList.Count - 1].gameObject);
                lineRendererList.RemoveAt(lineRendererList.Count - 1);
                lineID = "LineID" + lineRendererList.Count;
            }
        }

        public void ClearLineRendererList()
        {
            foreach (ViewItemLineRenderer obj in lineRendererList)
            {
                Destroy(obj.gameObject);
            }

            lineRendererList.Clear();
            lineID = "LineID" + lineRendererList.Count;
        }

        public void SetDefaultThickness(float value)
        {
            lineThickness = value;
        }

        public void SetDefaultColour(Color value)
        {
            lineColour = value;

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.NOTATION_COLOR, value);
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);

            dispatcher.Dispatch(NotationEvents.CHANGE_LINE_COLOR, data);
        }

        public void SetDefaultColour(CTVector4 value)
        {
            lineColour.r = value.X;
            lineColour.g = value.Y;
            lineColour.b = value.Z;
            lineColour.a = value.W;

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.NOTATION_COLOR, value);
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);

            dispatcher.Dispatch(NotationEvents.CHANGE_LINE_COLOR, data);
        }

        public void SetNotationType(ENotationType type)
        {
            notationType = type;
        }

        public void SetNotatedItem(GameObject obj)
        {
            notatedObject = obj;
            transform.LookAt(obj.transform.GetChild(0));
        }

        public void SetMainCamera(Camera camera)
        {
            mainCamera = camera;
        }

        public override void SyncItemData(CTItemData data)
        {
            base.SyncItemData(data);
        }

        public void SyncNotationStatus(CTLineNotationData lineNotationData, Vector3 mainCameraPos, Quaternion mainCameraRot, Vector3 mainCameraForwardDir)
        {
            isActive = true;

            if (!lineNotationData.IsLocalCoordinates)
            {
                transform.position = new Vector3(lineNotationData.CameraPosition.X, lineNotationData.CameraPosition.Y, lineNotationData.CameraPosition.Z);
                transform.eulerAngles = Vector3.zero;
                notationType = ENotationType.WORLD;
            }
            else
            {
                transform.position = mainCameraPos;
                transform.eulerAngles = Vector3.zero;
            }

            if (notationType == ENotationType.PLANAR_VIDEO)
            {
                Quaternion temp = new Quaternion(lineNotationData.CameraRotation.X, lineNotationData.CameraRotation.Y, lineNotationData.CameraRotation.Z, lineNotationData.CameraRotation.W);
                transform.eulerAngles = new Vector3(transform.eulerAngles.x, temp.eulerAngles.y, transform.eulerAngles.z);
            }

            SetDrawDistance(notationType, mainCameraPos, mainCameraForwardDir);

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);
            data.Parameters.Add((byte)CommonParameterCode.NOTATION_DRAWDISTANCE, drawDistance);

            dispatcher.Dispatch(NotationEvents.DRAWDISTANCE_SET, data);

            if (notationType == ENotationType.WORLD)
            {
                MessageData camData = new MessageData();

                DataLineNotationCamera startData = new DataLineNotationCamera
                {
                    CamPosition = lineNotationData.CameraPosition,
                    CamRotation = lineNotationData.CameraRotation,
                    IsLocalCoordinate = lineNotationData.IsLocalCoordinates,
                    NotatedObjectItemId = lineNotationData.NotatedObjectId,
                };

                camData.Parameters.Add((byte)CommonParameterCode.DATA_LINE_NOTATION_CAMERA, startData);

                dispatcher.Dispatch(NotationEvents.WORLD_NOTATION_STARTED_AFTER_SYNC, camData);
            }

            foreach(CTLine touchData in lineNotationData.Lines)
            {
                if (touchData.ListTouchPoints.Count > 0)
                {
                    GameObject objSpawned = Instantiate(lineRendererPrefab, transform.position + new Vector3(touchData.ListTouchPoints[0].X, touchData.ListTouchPoints[0].Y, touchData.ListTouchPoints[0].Z), transform.rotation);
                    SetDefaultThickness(touchData.Thickness);
                    SetDefaultColour(touchData.Color);

                    ViewItemLineRenderer newLR = objSpawned.GetComponent<ViewItemLineRenderer>();

                    newLR.SetLineRenderer();
                    newLR.startPosition = transform.position;
                    newLR.drawDistance = drawDistance - (0.005f * lineRendererList.Count);
                    newLR.SetColorAndThickness(lineColour, lineThickness);
                    newLR.lineID = touchData.LineId;

                    lineRendererList.Add(newLR);

                    newLR.Clear();

                    newLR.AddPointDataToList(touchData.ListTouchPoints);
                }
            }
        }

        public void AllUIHidden()
        {
            allUIHidden = true;
        }

        public void AllUIShown()
        {
            allUIHidden = false;
        }
    }
}