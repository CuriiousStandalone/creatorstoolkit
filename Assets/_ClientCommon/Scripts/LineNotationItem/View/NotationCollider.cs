﻿using UnityEngine;
using PulseIQ.Local.ClientCommon.TutorialItem;

namespace PulseIQ.Local.ClientCommon.LineNotationItem
{
    public class NotationCollider : MonoBehaviour
    {
        public ViewItemLineNotation notationItem;

        public ENotationType eNotationType;

        public Vector3 forwardVector;

        private float movementSpeed = 0.5f;

        private Vector3 initialScale;
        public float objectScale = 1.25f;
        public Camera cam;
        public string notatedObjItemId;

        float timerLimit = 1f;
        float limitTime = 0f;

        private void Start()
        {
            initialScale = transform.localScale;
        }

        private void Update()
        {
            if (null != cam)
            {
                //if(eNotationType == ENotationType.TUTORIAL_ITEM)
                {
                    Plane plane = new Plane(cam.transform.forward, cam.transform.position);
                    float dist = plane.GetDistanceToPoint(transform.position);
                    transform.localScale = initialScale * dist * objectScale;
                }

                transform.position += forwardVector * movementSpeed;

                limitTime += Time.deltaTime;

                Debug.Log("Notation Timer:   " + limitTime);

                if (limitTime >= timerLimit)
                {
                    notationItem.SetDrawDistance(transform.position + (forwardVector * 4f));
                    Destroy(this.gameObject);
                }
            }
            else
            {
                Destroy(this.gameObject);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (eNotationType == ENotationType.TUTORIAL_ITEM)
            {
                if(null != other.GetComponent<ViewTutorialItem>() && other.GetComponent<ViewTutorialItem>().itemID == notatedObjItemId)
                {
                    notationItem.SetDrawDistance(transform.position);
                    Destroy(this.gameObject);
                }
            }
            else
            {
                notationItem.SetDrawDistance(transform.position);
                Destroy(this.gameObject);
            }
        }
    }
}