﻿using strange.extensions.mediation.impl;

namespace PulseIQ.Local.ClientCommon.LineNotationItem
{
    public class MediatorItemLineRenderer : EventMediator
    {
        [Inject]
        public ViewItemLineRenderer view { get; set; }

        public override void OnRegister()
        {
            view.init();
            UpdateListeners(true);
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {

        }
    }
}