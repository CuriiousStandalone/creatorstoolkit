﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using RenderHeads.Media.AVProVideo;

using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.BaseItem;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.Multimedia;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.ClientCommon.Hotspot;

// Temp fix for restart video bug. Restart video is needed to play a video more than once on player client, but bugs out if video finishes. Coded the video to stop 1 second from the end of the video 
// to prevent the bug. 
namespace PulseIQ.Local.ClientCommon.StereoPanoramaVideoItem
{
    public class ViewStereoPanoramaVideoPlayer : ViewItemBase, IMediaBasic, IVideoBasic
    {
        enum PlayerStatus
        {
            NONE,
            PLAY,
            PLAY_SEEK,
            PAUSE,
            RESTART,
            SEEK,
        }

        PlayerStatus playerStatus;
        private GameObject mediaPlayerObject;
        public MediaPlayer mediaPlayer { get; private set; }
        StereoPacking stereoType;

        [SerializeField]
        private MediaPlayer inspectorMediaPlayer = null;

        private string mediaName;
        public bool isPlayer = false;
        private bool isLoaded = false;
        public bool isShowing = false;
        string shownItemID = "";
        private bool isMultiplayer = false;

        internal const string LOADED = "LOADED";
        internal const string SHOWN = "SHOWN";
        internal const string HIDDEN = "HIDDEN";
        internal const string UPDATE_TIMER = "UPDATE_TIMER";
        internal const string ROTATE_CAMERA = "ROTATE_CAMERA";
        internal const string CLOSE_MEDIA = "CLOSE_MEDIA";
        internal const string END_BEHAVIOUR_TELEPORT = "END_BEHAVIOUR_TELEPORT";
        internal const string PLAY = "PLAY";
        internal const string PLAYING_SYNC = "PLAYING_SYNC";

        private MessageData timerData = new MessageData();

        float maxVideoTime = 0f;
        float currentVideoFrame = 0f;

        private EMultimediaEndBehaviorCode endBehaviorCode;

        private bool isSync = false;
        private bool isSyncPlaying = false;
        private float syncTimeElapsed = 0;
        private float syncInitFrame = 0;
        private bool EndBehaviourTriggered = false;

        List<GameObject> hotspotItems = new List<GameObject>();

        protected override internal void init()
        {
            playerStatus = PlayerStatus.NONE;
            if (Settings.applicationType == EApplicationType.PLAYER_CLIENT)
            {
                isPlayer = true;
            }
            else
            {
                isPlayer = false;
            }

            itemType = Local.Common.ItemTypeCode.STEREO_PANORAMA_VIDEO;
            timerData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);
        }

        public void InitializeMedia(GameObject player, List<CTHotspotData> hotspotData)
        {
            mediaPlayerObject = player;
            inspectorMediaPlayer = mediaPlayerObject.transform.Find("MediaPlayer").GetComponent<MediaPlayer>();
            mediaPlayer = inspectorMediaPlayer;

            mediaPlayer.Events.AddListener(OnVideoEvent);

            Renderer itemRenderer = mediaPlayerObject.transform.GetChild(0).GetComponent<Renderer>();

            Vector3 rightPos = transform.position + ((Vector3.forward + Vector3.right) / 2f).normalized * itemRenderer.bounds.extents.x; ;
            Vector3 leftPos = transform.position + ((Vector3.forward - Vector3.right) / 2f).normalized * itemRenderer.bounds.extents.x; ;
            float drawDistance = Vector3.Distance(transform.position, (rightPos + leftPos) / 2f);
            EndBehaviourTriggered = false;
            foreach (CTHotspotData hotspot in hotspotData)
            {
                GameObject hotspotObj = Instantiate((Resources.Load(PrefabPathSettings.hotspot) as GameObject), this.transform);
                ViewHotspot hotspotScript = hotspotObj.GetComponent<ViewHotspot>();
                hotspotScript.SetHotspotData(transform.position, itemID, hotspot, drawDistance, itemType);
                hotspotItems.Add(hotspotObj);
                hotspotObj.SetActive(false);
            }
        }

        public string MediaName
        {
            get
            {
                return mediaName;
            }

            set
            {
                mediaName = value;
            }

        }

        protected override void OnDestroy()
        {
            UnloadMedia();
            mediaPlayer.Events.RemoveListener(OnVideoEvent);

            for (int i = 0; i < hotspotItems.Count; ++i)
            {
                Destroy(hotspotItems[i]);
            }
        }

        private void Update()
        {
            base.Update();

            if (isShowing)
            {
                if (!EndBehaviourTriggered && maxVideoTime != 0 && mediaPlayer.Control.GetCurrentTimeMs() > (maxVideoTime - 1000))
                {
                    EndBehaviourTriggered = true;
                    MessageData msgData;

                    switch (endBehaviorCode)
                    {
                        case EMultimediaEndBehaviorCode.CLOSE:
                            if (!isPlayer)
                            {
                                PauseMedia(maxVideoTime - 1000);
                            }

                            msgData = new MessageData();

                            msgData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);

                            dispatcher.Dispatch(END_BEHAVIOUR_TELEPORT, msgData);
                            break;
                        case EMultimediaEndBehaviorCode.LOOP:
                            ReplayMedia();
                            break;
                        case EMultimediaEndBehaviorCode.STOP:
                            PauseMedia(maxVideoTime - 1000);
                            break;
                        case EMultimediaEndBehaviorCode.TELEPORT:
                            msgData = new MessageData();

                            msgData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);

                            dispatcher.Dispatch(END_BEHAVIOUR_TELEPORT, msgData);
                            break;
                    }
                }
            }
        }

        public void HideMedia()
        {
            foreach (GameObject obj in hotspotItems)
            {
                obj.SetActive(false);
            }

            playerStatus = PlayerStatus.NONE;

            mediaPlayerObject.transform.GetChild(0).gameObject.SetActive(false);
            StopMedia(); 

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);

            dispatcher.Dispatch(HIDDEN, data);

            isShowing = false;
        }

        public IEnumerator IHideMedia()
        {
            yield return new WaitForSeconds(1f);

            foreach (GameObject obj in hotspotItems)
            {
                obj.SetActive(false);
            }

            playerStatus = PlayerStatus.NONE;

            mediaPlayerObject.transform.GetChild(0).gameObject.SetActive(false);
            StopMedia();

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);

            dispatcher.Dispatch(HIDDEN, data);

            isShowing = false;
        }

        public void HideMediaPlayerClient()
        {
            foreach (GameObject obj in hotspotItems)
            {
                obj.SetActive(false);
            }

            playerStatus = PlayerStatus.NONE;

            mediaPlayerObject.transform.GetChild(0).gameObject.SetActive(false);
            StopMedia();

            mediaPlayer.CloseVideo();

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);

            dispatcher.Dispatch(HIDDEN, data);

            isShowing = false;
        }

        public void LoadMedia(string path)
        {
            EndBehaviourTriggered = false;
            isLoaded = false; 

            if (mediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, path, false))
            {
                //Debug.Log("Loaded 360 Video File");

                maxVideoTime = mediaPlayer.Info.GetDurationMs();
            }
            else
            {
                //Debug.Log("Failed to load 360 Video File");
            }
        }

        public void PauseMedia(float frame)
        {
            if(!isLoaded)
            {
                playerStatus = PlayerStatus.PAUSE;
                currentVideoFrame = frame;
                return;
            }

            SeekMedia(frame);
            mediaPlayer.Pause();
        }

        public void PlayMedia(float frame)
        {
            if (!isLoaded)
            {
                playerStatus = PlayerStatus.PLAY;
                currentVideoFrame = frame;
                return;
            }

            SeekMedia(frame);
            mediaPlayer.Play();
            InvokeRepeating("SendCurrentTime", 0.5f, 0.5f);
        }

        public void RestartMedia()
        {
            if (!isLoaded)
            {
                playerStatus = PlayerStatus.RESTART;
                return;
            }
            mediaPlayer.Rewind(true);
        }

        public void ReplayMedia()
        {
            mediaPlayer.Rewind(false);
        }

        public void SeekMedia(float frame)
        {
            if (!isLoaded)
            {
                if(playerStatus == PlayerStatus.PLAY_SEEK || playerStatus == PlayerStatus.SEEK)
                {
                    currentVideoFrame = frame;
                    return;
                }
                if(playerStatus == PlayerStatus.PLAY)
                {
                    playerStatus = PlayerStatus.PLAY_SEEK;
                }
                else
                {
                    playerStatus = PlayerStatus.SEEK;
                }

                currentVideoFrame = frame;
                return;
            }

            mediaPlayer.Control.Seek(frame);
            UpdateHotspotTime(frame);
        }

        public void ShowMedia(Vector3 camPosition)
        {
            RestartMedia();
            SeekMedia(0);
            mediaPlayer.Stop();
            mediaPlayerObject.transform.position = camPosition;
            mediaPlayerObject.transform.GetChild(0).gameObject.SetActive(true);
            Invoke("PausePlayer", 0.5f);
            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);
            data.Parameters.Add((byte)CommonParameterCode.MEDIA_NAME, mediaName);
            data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, itemType);
            data.Parameters.Add((byte)CommonParameterCode.VIDEO_TOTAL_TIME, mediaPlayer.Info.GetDurationMs());
            data.Parameters.Add((byte)CommonParameterCode.IS_MULTIPLAYER, isMultiplayer);

            dispatcher.Dispatch(SHOWN, data);
            isShowing = true;

            foreach (GameObject obj in hotspotItems)
            {
                obj.GetComponent<ViewHotspot>().PositionHotspot(camPosition, mediaPlayerObject.transform.rotation);

                if (obj.GetComponent<ViewHotspot>().IsEnabled)
                {
                    obj.GetComponent<ViewHotspot>().EnableHotspot();
                }

                obj.SetActive(true);
            }
        }

        public void ShowMediaControl(Vector3 camPosition)
        {
            mediaPlayerObject.transform.position = camPosition;
            mediaPlayerObject.transform.GetChild(0).gameObject.SetActive(true);

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);
            data.Parameters.Add((byte)CommonParameterCode.MEDIA_NAME, mediaName);
            data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, itemType);
            data.Parameters.Add((byte)CommonParameterCode.VIDEO_TOTAL_TIME, mediaPlayer.Info.GetDurationMs());
            data.Parameters.Add((byte)CommonParameterCode.HOTSPOT_ID_LIST, GetHotspotIDs());
            data.Parameters.Add((byte)CommonParameterCode.HOTSPOT_NAME_LIST, GetHotspotNames());
            data.Parameters.Add((byte)CommonParameterCode.IS_MULTIPLAYER, isMultiplayer);
                
            dispatcher.Dispatch(SHOWN, data);
            isShowing = true;
        }

        public IEnumerator IShowMedia(Vector3 camPosition, Vector3 camRot)
        {
            yield return new WaitForSeconds(1f);
            shownItemID = itemID;
            mediaPlayer.m_StereoPacking = stereoType;
            mediaPlayerObject.transform.position = camPosition;
            mediaPlayerObject.transform.eulerAngles = new Vector3(mediaPlayerObject.transform.eulerAngles.x, camRot.y, mediaPlayerObject.transform.eulerAngles.z);
            mediaPlayerObject.transform.GetChild(0).gameObject.SetActive(true);
            LoadMedia(Settings.StereoPanoramaVideoPath + MediaName);

            foreach (GameObject obj in hotspotItems)
            {
                obj.GetComponent<ViewHotspot>().PositionHotspot(camPosition, mediaPlayerObject.transform.rotation);
            }
        }

        public IEnumerator IShowMedia(Vector3 camPosition)
        {
            yield return new WaitForSeconds(1f);
            shownItemID = itemID;
            mediaPlayer.m_StereoPacking = stereoType;
            mediaPlayerObject.transform.position = camPosition;
            mediaPlayerObject.transform.GetChild(0).gameObject.SetActive(true);
            LoadMedia(Settings.StereoPanoramaVideoPath + MediaName);

            foreach (GameObject obj in hotspotItems)
            {
                obj.GetComponent<ViewHotspot>().PositionHotspot(camPosition);
            }
        }

        public void ShowMediaAfterSync(Vector3 camPosition, bool isPlaying, float timeElapsed, int initFrame)
        {
            shownItemID = itemID;
            LoadMedia(Settings.StereoPanoramaVideoPath + MediaName);
            mediaPlayer.m_StereoPacking = stereoType;
            mediaPlayerObject.transform.position = camPosition;
            mediaPlayerObject.transform.GetChild(0).gameObject.SetActive(true);

            foreach (GameObject obj in hotspotItems)
            {
                obj.GetComponent<ViewHotspot>().PositionHotspot(camPosition);
            }

            isSync = true;
            isSyncPlaying = isPlaying;
            syncTimeElapsed = timeElapsed;
            syncInitFrame = initFrame;
        }

        public void StopMedia()
        {
            CancelInvoke("SendCurrentTime");
            SendDefaultStartTime();
            mediaPlayer.Stop();
        }

        public void UnloadMedia()
        {
            if (null != mediaPlayer)
            {
                mediaPlayer.Stop();
                mediaPlayer.CloseVideo();
            }
        }

        public void SetStereoType(EStereoPanoramaVideoTypeCode type)
        {
            switch (type)
            {
                case EStereoPanoramaVideoTypeCode.TOP_BOTTOM:
                    {
                        stereoType = StereoPacking.TopBottom;
                        mediaPlayer.m_StereoPacking = stereoType;

                        break;
                    }

                case EStereoPanoramaVideoTypeCode.BOTTOM_TOP:
                    {
                        break;
                    }

                case EStereoPanoramaVideoTypeCode.LEFT_RIGHT:
                    {
                        stereoType = StereoPacking.TopBottom;
                        mediaPlayer.m_StereoPacking = stereoType;
                        break;
                    }

                case EStereoPanoramaVideoTypeCode.RIGHT_LEFT:
                    {
                        break;
                    }
            }
        }

        public void ShowMedia(Vector3 cameraPos, Quaternion cameraRot)
        {
            foreach (GameObject obj in hotspotItems)
            {
                obj.GetComponent<ViewHotspot>().PositionHotspot(cameraPos, cameraRot);

                if (obj.GetComponent<ViewHotspot>().IsEnabled)
                {
                    obj.GetComponent<ViewHotspot>().EnableHotspot();
                }

                obj.SetActive(true);
            }

            return;
        }

        public void SendCurrentTime()
        {
            timerData.Parameters.Clear();
            timerData.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);
            timerData.Parameters.Add((byte)CommonParameterCode.VIDEO_CURRENT_TIME, mediaPlayer.Control.GetCurrentTimeMs());

            dispatcher.Dispatch(UPDATE_TIMER, timerData);

            UpdateHotspotTime(mediaPlayer.Control.GetCurrentTimeMs());
        }

        public void SendDefaultStartTime()
        {
            timerData.Parameters.Clear();
            timerData.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);
            timerData.Parameters.Add((byte)CommonParameterCode.VIDEO_CURRENT_TIME, 0.0f);

            dispatcher.Dispatch(UPDATE_TIMER, timerData);
        }

        public override void SyncItemData(CTItemData data)
        {
            base.SyncItemData(data);
        }

        public void OnVideoEvent(MediaPlayer mp, MediaPlayerEvent.EventType et, ErrorCode errorCode)
        {
            switch (et)
             {
                 case MediaPlayerEvent.EventType.ReadyToPlay:
                     {
                        if (shownItemID == itemID)
                         {
                            maxVideoTime = mediaPlayer.Info.GetDurationMs();

                            if (!isLoaded)
                            {
                                MessageData mData = new MessageData();
                                mData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);

                                dispatcher.Dispatch(LOADED, mData);
                                isLoaded = true;
                            }

                            if(isPlayer)
                            {
                                dispatcher.Dispatch(ROTATE_CAMERA);
                                DelayedLoadedFunction();
                                Invoke("DelayedPlayerStatusFunction", 0.5f);
                            }
                            else
                            {
                                Invoke("DelayedLoadedFunction", 2f);
                            }

                            isShowing = true;
                            shownItemID = "";
                        }
                         break;
                     }
             }
        }

        void DelayedLoadedFunction()
        {
            foreach (GameObject obj in hotspotItems)
            { 
                if (obj.GetComponent<ViewHotspot>().IsEnabled)
                {
                    obj.GetComponent<ViewHotspot>().EnableHotspot();
                }

                obj.SetActive(true);
            }

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);
            data.Parameters.Add((byte)CommonParameterCode.MEDIA_NAME, mediaName);
            data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, itemType);
            data.Parameters.Add((byte)CommonParameterCode.VIDEO_TOTAL_TIME, mediaPlayer.Info.GetDurationMs());
            data.Parameters.Add((byte)CommonParameterCode.HOTSPOT_ID_LIST, GetHotspotIDs());
            data.Parameters.Add((byte)CommonParameterCode.HOTSPOT_NAME_LIST, GetHotspotNames());
            data.Parameters.Add((byte)CommonParameterCode.IS_MULTIPLAYER, isMultiplayer);
            data.Parameters.Add((byte)CommonParameterCode.IS_PLAYING, isSync && isSyncPlaying);

            dispatcher.Dispatch(SHOWN, data);

            if (isSync)
            {
                if (isSyncPlaying)
                {
                    MessageData msgData = new MessageData();
                    msgData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);
                    msgData.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, itemType);
                    msgData.Parameters.Add((byte)CommonParameterCode.VIDEO_FRAME, syncInitFrame + syncTimeElapsed);

                    dispatcher.Dispatch(PLAY, msgData);
                }
                else
                {
                    SeekMedia(syncInitFrame + syncTimeElapsed);
                }


                MessageData mData = new MessageData();
                mData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);
                mData.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, itemType);
                dispatcher.Dispatch(PLAYING_SYNC, mData);

                isSync = false;
                isSyncPlaying = false;
                syncInitFrame = 0;
                syncTimeElapsed = 0;
            }
            else if (!isMultiplayer)
            {
                MessageData msgData = new MessageData();
                msgData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);
                msgData.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, itemType);

                dispatcher.Dispatch(PLAY, msgData);
            }
        }

        void DelayedPlayerStatusFunction()
        {
            switch (playerStatus)
            {
                case PlayerStatus.NONE:
                    {
                        break;
                    }

                case PlayerStatus.PAUSE:
                    {
                        PauseMedia(currentVideoFrame);
                        break;
                    }

                case PlayerStatus.PLAY:
                    {
                        PlayMedia(currentVideoFrame);
                        break;
                    }

                case PlayerStatus.PLAY_SEEK:
                    {
                        PlayMedia(currentVideoFrame);
                        SeekMedia(currentVideoFrame);
                        break;
                    }

                case PlayerStatus.RESTART:
                    {
                        RestartMedia();
                        break;
                    }

                case PlayerStatus.SEEK:
                    {
                        SeekMedia(currentVideoFrame);
                        break;
                    }
            }

            foreach (GameObject obj in hotspotItems)
            {
                if (obj.GetComponent<ViewHotspot>().IsEnabled)
                {
                    obj.GetComponent<ViewHotspot>().EnableHotspot();
                }

                obj.SetActive(true);
            }
        }

        public void DestroyPanoramaMediaPlayer()
        {
            if (null != mediaPlayerObject)
            {
                Destroy(mediaPlayerObject.transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().material);
                Destroy(mediaPlayerObject.transform.GetChild(0).gameObject);
                Destroy(mediaPlayerObject);
            }
        }

        public EMultimediaEndBehaviorCode EndBehaviourCode
        {
            get
            {
                return endBehaviorCode;
            }

            set
            {
                endBehaviorCode = value;
            }
        }

        public bool IsMultiplayer
        {
            get
            {
                return isMultiplayer;
            }
            set
            {
                isMultiplayer = value;
            }
        }

        private void UpdateHotspotTime(float currentTime)
        {
            foreach (GameObject obj in hotspotItems)
            {
                obj.GetComponent<ViewHotspot>().VideoTimeReceived(currentTime);
            }
        }

        private List<string> GetHotspotIDs()
        {
            List<string> ids = new List<string>();

            foreach(GameObject obj in hotspotItems)
            {
                ids.Add(obj.GetComponent<ViewHotspot>().ReturnHotspotID());
            }

            return ids;
        }
        
        
        private List<string> GetHotspotNames()
        {
            List<string> ids = new List<string>();

            foreach(GameObject obj in hotspotItems)
            {
                ids.Add(obj.GetComponent<ViewHotspot>().GetHotspotName);
            }

            return ids;
        }

        public void DisableHotspot(MessageData data)
        {
            string hotspotID = (string)data[(byte)CommonParameterCode.HOTSPOT_ID];

            foreach (GameObject obj in hotspotItems)
            {
                if (obj.GetComponent<ViewHotspot>().ReturnHotspotID() == hotspotID)
                {
                    obj.GetComponent<ViewHotspot>().DisableHotspot();
                }
            }
        }

        public void EnableHotspot(MessageData data)
        {
            string hotspotID = (string)data[(byte)CommonParameterCode.HOTSPOT_ID];

            foreach (GameObject obj in hotspotItems)
            {
                if (obj.GetComponent<ViewHotspot>().ReturnHotspotID() == hotspotID)
                {
                    obj.GetComponent<ViewHotspot>().EnableHotspot();
                    obj.SetActive(true);
                }
            }
        }

        //public void DisableHotspotStatus(MessageData data)
        //{
        //    string hotspotID = (string)data[(byte)CommonParameterCode.HOTSPOT_ID];
        //    string hotspotStatusID = (string)data[(byte)CommonParameterCode.HOTSPOT_STATUS_ID];
        //
        //    foreach (GameObject obj in hotspotItems)
        //    {
        //        if (obj.GetComponent<ViewHotspot>().ReturnHotspotID() == hotspotID)
        //        {
        //            obj.GetComponent<ViewHotspot>().DisableHotspotStatus(hotspotStatusID);
        //        }
        //    }
        //}

        public void EnableHotspotStatus(MessageData data)
        {
            string hotspotID = (string)data[(byte)CommonParameterCode.HOTSPOT_ID];
            string hotspotStatusID = (string)data[(byte)CommonParameterCode.HOTSPOT_STATUS_ID];

            foreach (GameObject obj in hotspotItems)
            {
                if (obj.GetComponent<ViewHotspot>().ReturnHotspotID() == hotspotID)
                {
                    obj.GetComponent<ViewHotspot>().EnableHotspotStatus(hotspotStatusID);
                }
            }
        }
		
        public void TeleportMedia(string hotspotId, int targetItemIndex)
        {
            foreach (var hotspot in hotspotItems)
            {
                ViewHotspot viewHotspot = hotspot.GetComponent<ViewHotspot>();

                if (viewHotspot.ReturnHotspotID() == hotspotId)
                {
                    viewHotspot.ShowTeleportMedia();
                }
            }
        }
    }
}
