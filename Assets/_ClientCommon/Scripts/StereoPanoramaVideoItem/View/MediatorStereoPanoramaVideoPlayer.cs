﻿using UnityEngine;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.ClientCommon.ItemManager;
using System.Collections;

namespace PulseIQ.Local.ClientCommon.StereoPanoramaVideoItem
{
    public class MediatorStereoPanoramaVideoPlayer : EventMediator
    {
        [Inject]
        public ViewStereoPanoramaVideoPlayer view { get; set; }

        private MessageData panoramaData;
        private MessageData msgData = new MessageData();

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewStereoPanoramaVideoPlayer.LOADED, OnLoaded);
            view.dispatcher.UpdateListener(value, ViewStereoPanoramaVideoPlayer.SHOWN, OnShown);
            view.dispatcher.UpdateListener(value, ViewStereoPanoramaVideoPlayer.HIDDEN, OnHidden);
            view.dispatcher.UpdateListener(value, ViewStereoPanoramaVideoPlayer.UPDATE_TIMER, OnUpdateTimer);
            view.dispatcher.UpdateListener(value, ViewStereoPanoramaVideoPlayer.ROTATE_CAMERA, OnRotateCamera);
            view.dispatcher.UpdateListener(value, ViewStereoPanoramaVideoPlayer.CLOSE_MEDIA, OnCloseMedia);
            view.dispatcher.UpdateListener(value, ViewStereoPanoramaVideoPlayer.END_BEHAVIOUR_TELEPORT, OnEndBehaviourTeleport);
            view.dispatcher.UpdateListener(value, ViewStereoPanoramaVideoPlayer.PLAY, OnPlayMedia);
            view.dispatcher.UpdateListener(value, ViewStereoPanoramaVideoPlayer.PLAYING_SYNC, OnPlayingAfterSync);
        }

        private void OnMessageReceived(System.Object data)
        {
            panoramaData = (MessageData)data;
            ServerCmdCode code = (ServerCmdCode)panoramaData[(byte)CommonParameterCode.OPERATION_CODE];

            switch (code)
            {
                case ServerCmdCode.ALL_MULTIMEDIA_CLOSED:
                case ServerCmdCode.STEREO_PANORAMA_VIDEO_HIDDEN:
                    {
                        Invoke("DelayedHideFunction", 1f);

                        MessageData newData = new MessageData();
                        newData.Parameters.Add((byte)CommonParameterCode.TOTAL_TIME, 2f);

                        dispatcher.Dispatch(ECommonViewEvent.FADE_CAMERA_IN_OUT, newData);

                        dispatcher.Dispatch(ECommonViewEvent.DROPDOWN_ITEM_HIDDEN, panoramaData);

                        break;
                    }

                case ServerCmdCode.END_BEHAVIOUR_SHOWN:
                    {
                        MessageData newData = new MessageData();
                        newData.Parameters.Add((byte)CommonParameterCode.ITEMID, view.itemID);
                        dispatcher.Dispatch(ECommonViewEvent.MEDIA_END_BEHAVIOUR, newData);
                        StereoPanoramaShown(panoramaData);
                        break;
                    }
                case ServerCmdCode.STEREO_PANORAMA_VIDEO_SHOWN:
                    {
                        StereoPanoramaShown(panoramaData);

                        break;
                    }

                case ServerCmdCode.STEREO_PANORAMA_VIDEO_PLAYED:
                    {
                        MessageData newData = new MessageData();
                        newData.Parameters.Add((byte)CommonParameterCode.ITEMID, view.itemID);

                        int frame = (int)panoramaData[(byte)CommonParameterCode.VIDEO_FRAME];
                        view.PlayMedia(frame);
                        break;
                    }

                case ServerCmdCode.STEREO_PANORAMA_VIDEO_PAUSED:
                    {
                        int frame = (int)panoramaData[(byte)CommonParameterCode.VIDEO_FRAME];
                        view.PauseMedia(frame);
                        break;
                    }

                case ServerCmdCode.STEREO_PANORAMA_VIDEO_REPLAYED:
                    {
                        view.RestartMedia();
                        break;
                    }

                case ServerCmdCode.STEREO_PANORAMA_VIDEO_STOPPED:
                    {
                        view.StopMedia();
                        break;
                    }

                case ServerCmdCode.STEREO_PANORAMA_VIDEO_SOUGHT:
                    {
                        int frame = (int)panoramaData[(byte)CommonParameterCode.VIDEO_FRAME];
                        view.SeekMedia(frame);
                        break;
                    }

                case ServerCmdCode.ITEM_STATUS_SYNCED:
                    {
                        CTItemData syncData = (CTItemData)(panoramaData[(byte)CommonParameterCode.DATA_CT_ITEM_DATA]);
                        view.SyncItemData(syncData);
                        break;
                    }

                case ServerCmdCode.HOTSPOT_DISABLED:
                    {
                        view.DisableHotspot(panoramaData);
                        break;
                    }

                case ServerCmdCode.HOTSPOT_ENABLED:
                    {
                        view.EnableHotspot(panoramaData);
                        break;
                    }

                case ServerCmdCode.HOTSPOT_STATUS_ENABLED:
                    {
                        view.EnableHotspotStatus(panoramaData);
                        break;
                    }

                case ServerCmdCode.HOTSPOT_TELEPORTED:
                    {
                        if (!view.isShowing)
                        {
                            return;
                        }

                        MessageData newData = new MessageData();
                        newData.Parameters.Add((byte)CommonParameterCode.TOTAL_TIME, 0.25f);

                        dispatcher.Dispatch(ECommonViewEvent.FADE_CAMERA_IN, newData);

                        StartCoroutine(IHotspotTeleport(panoramaData));

                        break;
                    }

                case ServerCmdCode.END_BEHAVIOUR_TELEPORTED:
                    {
                        if (!view.isShowing)
                        {
                            return;
                        }

                        MessageData newData = new MessageData();
                        newData.Parameters.Add((byte)CommonParameterCode.TOTAL_TIME, 0.25f);

                        dispatcher.Dispatch(ECommonViewEvent.FADE_CAMERA_IN, newData);

                        StartCoroutine(IEndBehaviourTeleport(panoramaData));

                        break;
                    }

                case ServerCmdCode.SYNC_STEREO_PANORAMA_VIDEO_STATUS:
                    {
                        bool isLoaded = (bool)panoramaData[(byte)CommonParameterCode.IS_LOADED];

                        if (isLoaded)
                        {
                            Vector3 camPos = (Vector3)panoramaData.Parameters[(byte)CommonParameterCode.MAIN_CAMERA_POSITION];

                            int initFrame = (int)panoramaData[(byte)CommonParameterCode.VIDEO_FRAME];
                            float timeElapsed = (float)panoramaData[(byte)CommonParameterCode.TIME_ELAPSED];
                            bool isPlaying = (bool)panoramaData[(byte)CommonParameterCode.IS_PLAYING];

                            view.ShowMediaAfterSync(camPos, isPlaying, timeElapsed, initFrame);
                        }

                        break;
                    }
            }
        }

        private void StereoPanoramaShown(MessageData data)
        {
            MessageData newData = new MessageData();
            newData.Parameters.Add((byte)CommonParameterCode.TOTAL_TIME, 0.25f);

            dispatcher.Dispatch(ECommonViewEvent.FADE_CAMERA_IN, newData);

            Vector3 camPos = (Vector3)panoramaData.Parameters[(byte)CommonParameterCode.MAIN_CAMERA_POSITION];


            if (!view.isPlayer)
            {
                StartCoroutine(view.IShowMedia(camPos));

                //view.ShowMediaControl(camPos);
            }
            else
            {
                StartCoroutine(view.IShowMedia(camPos));
                //view.ShowMedia(camPos);
            }
        }

        private IEnumerator IHotspotTeleport(MessageData data)
        {
            yield return new WaitForSeconds(0.5f);

            view.HideMedia();

            string hotspotId = (string)data[(byte)CommonParameterCode.HOTSPOT_ID];
            int targetItemIndex = (int)data[(byte)CommonParameterCode.ITEM_INDEX];

            view.TeleportMedia(hotspotId, targetItemIndex);
        }

        private IEnumerator IEndBehaviourTeleport(MessageData data)
        {
            yield return new WaitForSeconds(0.5f);

            view.HideMedia();

            int targetItemIndex = (int)data[(byte)CommonParameterCode.ITEM_INDEX];

            MessageData msgData = new MessageData();

            msgData.Parameters.Add((byte)CommonParameterCode.ITEM_INDEX, targetItemIndex);
            msgData.Parameters.Add((byte)CommonParameterCode.OPERATION_CODE, ServerCmdCode.END_BEHAVIOUR_SHOWN);

            dispatcher.Dispatch(IntCmdCodeMultimedia.SHOW_END_BEHAVIOUR_TELEPORT_MEDIA, msgData);
            
        
        }

        private void DelayedHideFunction()
        {
            if (!view.isShowing)
            {
                return;
            }

            if (!view.isPlayer)
            {
                view.HideMedia();
            }
            else
            {
                dispatcher.Dispatch(IntCmdCodeStereoPanoramaVideo.RETURN_CAMERA_ROT);
                view.HideMediaPlayerClient();
            }
        }

        private void OnCloseMedia()
        {
            MessageData msgData = new MessageData();

            msgData.Parameters.Add((byte)CommonParameterCode.MEDIA_NAME, view.itemID);

            dispatcher.Dispatch(ECommonViewEvent.CLOSE_MEDIA, msgData);
        }

        private void OnEndBehaviourTeleport(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.TriggerEndBehavior, evt.data);
        }

        private void OnPlayMedia(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.PlayStereoPanoramaVideo, evt.data);
        }

        private void OnPlayingAfterSync(IEvent evt)
        {
            dispatcher.Dispatch(IntCmdCodeMultimedia.PLAYING_MEDIA_AFTER_SYNC, evt.data);
        }

        private void OnLoaded(IEvent evt)
        {
            MessageData newData = new MessageData();
            newData.Parameters.Add((byte)CommonParameterCode.TOTAL_TIME, 0.25f);

            dispatcher.Dispatch(ECommonViewEvent.FADE_CAMERA_OUT, newData);
            dispatcher.Dispatch(ECommonViewEvent.SHOW_VR, evt.data);
            dispatcher.Dispatch(IntCmdCodeStereoPanoramaVideo.LOADED, evt.data);
        }

        private void OnShown(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.MEDIA_SHOW, evt.data);
        }

        private void OnHidden(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.MEDIA_HIDE, evt.data);
            dispatcher.Dispatch(ECommonViewEvent.HIDE_VR, evt.data);
        }

        private void OnMediaHidden(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.MEDIA_HIDDEN, evt.data);
        }

        private void OnUpdateTimer(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.MEDIA_CURRENT_TIME, evt.data);
        }

        private void OnRotateCamera()
        {
            dispatcher.Dispatch(IntCmdCodeStereoPanoramaVideo.ROTATE_CAMERA);
        }
    }
}