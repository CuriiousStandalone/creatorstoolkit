﻿namespace PulseIQ.Local.ClientCommon.StereoPanoramaVideoItem
{
    public enum IntCmdCodeStereoPanoramaVideo : byte
    {
        CREATED,
        STARTED,
        PAUSED,
        RESTARTED,
        STOPPED,
        SOUGHT,
        SHOWN,
        HIDDEN,
        LOADED,
        RETURN_CAMERA_ROT,
        ROTATE_CAMERA,
        STATUS_SYNCED,
    }
}