﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.StereoPanoramaVideoItem
{
    public enum OpCmdCodeStereoPanoramaVideo : byte
    {
        CREATE,
        START,
        PAUSE,
        RESET,
        STOP,
        SEEK,
        SHOW,
        HIDE,
    }
}