﻿using UnityEngine;
using strange.extensions.command.impl;

using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.BaseItem;

namespace PulseIQ.Local.ClientCommon.StereoPanoramaVideoItem
{
    public class OpCmdSeekStereoPanoramaVideo : EventCommand
    {
        [Inject]
        public IItemDataset model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;
            string itemID = (string)data.Parameters[(byte)CommonParameterCode.ITEMID];
            float sliderValue = (float)data.Parameters[(byte)CommonParameterCode.VIDEO_SLIDER_VALUE];
            float frame = -1f;

            foreach (ViewItemBase itemBase in model.itemList)
            {
                if (itemID == itemBase.itemID)
                {
                    frame = itemBase.GetComponent<ViewStereoPanoramaVideoPlayer>().mediaPlayer.Info.GetDurationMs() * sliderValue;
                }
            }

            if (frame == -1f)
            {
                Debug.LogWarning("Frame was not set in Panorama Video Player for seek!");
            }
            else
            {
                OperationsStereoPanoramaVideo.SeekStereoPanoramaVideo(ClientBridge.Instance, ClientBridge.Instance.CurUserID, itemID, (int)frame);
            }
        }
    }
}