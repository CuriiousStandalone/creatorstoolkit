﻿using UnityEngine;

using PulseIQ.Local.ClientCommon.Components.Common;

using strange.extensions.mediation.impl;

namespace PulseIQ.Local.ClientCommon.User
{
    public class ViewUserManager : EventView
    {
        internal static string RECONNECT_CLICKED = "RECONNECT_CLICKED";
        internal static string TRY_AGAIN_CLICKED = "TRY_AGAIN_CLICKED";

        internal void init()
        {

        }

        public void ShowUserDisconnectedDialog(GameObject dialog)
        {
            GenericPopupDialog dialogClass = dialog.GetComponent<GenericPopupDialog>();

            dialogClass.InitializeDialog("You are disconnected from server. Click on RECONNECT button to connect back to server.", "RECONNECT", ReconnectCallback);

            dialog.SetActive(true);
        }

        public void ShowRetryDialog(GameObject dialog)
        {
            GenericPopupDialog dialogClass = dialog.GetComponent<GenericPopupDialog>();

            dialogClass.InitializeDialog("Maximum user connection reached. Try again after some time.", "Try Again", TryAgainCallback);

            dialog.SetActive(true);
        }

        private void TryAgainCallback()
        {
            dispatcher.Dispatch(TRY_AGAIN_CLICKED);
        }

        private void ReconnectCallback()
        {
            dispatcher.Dispatch(RECONNECT_CLICKED);
        }
    }
}