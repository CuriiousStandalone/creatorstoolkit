﻿using PulseIQ.Local.Client.Unity3DLib.Model;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common.Model;
using PulseIQ.Local.Common;

using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

using UnityEngine;

namespace PulseIQ.Local.ClientCommon.User
{
    public class MediatorUserManager : EventMediator
    {
        [Inject]
        public ViewUserManager view { get; set; }

        [Inject]
        public ICommonModel commonModel { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            dispatcher.UpdateListener(value, IntCmdCodeUser.DISCONNECTED, OnDisconnected);
            dispatcher.UpdateListener(value, IntCmdCodeUser.LOGIN_FAILED, OnLoginFailed);

            view.dispatcher.UpdateListener(value, ViewUserManager.RECONNECT_CLICKED, OnReconnectClicked);
            view.dispatcher.UpdateListener(value, ViewUserManager.TRY_AGAIN_CLICKED, OnTryAgainClicked);
        }

        private void OnDisconnected()
        {
            GameObject go = commonModel.loadingScreen.GetComponent<IGenericDialogView>().GetGenericDialog();

            view.ShowUserDisconnectedDialog(go);
        }

        private void OnLoginFailed(IEvent evt)
        {
            MessageData msgData = (MessageData)evt.data;

            DataLogin loginData = (DataLogin)msgData[(byte)CommonParameterCode.DATA_LOGIN];

            if(loginData.ErrorCode == LoginCode.UserCountLimitReached)
            {
                commonModel.loadingScreen.GetComponent<ILoadingScreenView>().ShowView();

                GameObject genericDialog = commonModel.loadingScreen.GetComponent<IGenericDialogView>().GetGenericDialog();

                view.ShowRetryDialog(genericDialog);
            }
        }

        private void OnReconnectClicked()
        {
            dispatcher.Dispatch(SubCode.Reconnect);
        }

        private void OnTryAgainClicked()
        {
            commonModel.loadingScreen.GetComponent<ILoadingScreenView>().HideView();

            dispatcher.Dispatch(ECommonCrossContextEvents.RETRY_LOGIN);
        }
    }
}