﻿using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using strange.extensions.command.impl;
using System.Threading.Tasks;

namespace PulseIQ.Local.ClientCommon.User
{
    public class OpCmdRequestReconnect : EventCommand
    {
        [Inject]
        public IUserModel userModel { get; set; }

        public override void Execute()
        {
            userModel.toReconnect = true;

            ClientBridge.Instance.Initialize();
            ClientBridge.Instance.Connect();
        }
    }
}