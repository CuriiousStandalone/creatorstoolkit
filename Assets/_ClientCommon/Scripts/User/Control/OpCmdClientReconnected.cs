﻿using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;

using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.User
{
    public class OpCmdClientReconnected : EventCommand
    {
        public override void Execute()
        {
            MessageData msgData = (MessageData)evt.data;

            string worldId = (string)msgData[(byte)CommonParameterCode.WORLDID];

            //dispatcher.Dispatch(CrossContextEvents.JOIN_WAITING_WORLD, msgData);
            dispatcher.Dispatch(ECommonCrossContextEvents.RECONNECT_TO_WORLD, msgData);
        }
    }
}