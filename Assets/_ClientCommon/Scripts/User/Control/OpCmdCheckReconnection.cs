﻿using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;

using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.User
{
    public class OpCmdCheckReconnection : EventCommand
    {
        [Inject]
        public IUserModel userModel { get; set; }

        public override void Execute()
        {
            if (userModel.toReconnect)
            {
                userModel.toReconnect = false;

                OperationsUser.Login(ClientBridge.Instance, UserType.Player, userModel.UserName, userModel.Password);
            }
        }
    }
}