﻿using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.ItemManager;
using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.User
{
    public class OpCmdPlayerDisconnected : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(IntCmdCodeItemManager.DESTROY_EVERYTHING_ON_DISCONNECT);
            dispatcher.Dispatch(IntCmdCodeUser.DISCONNECTED);
            dispatcher.Dispatch(ECommonCrossContextEvents.DISCONNECTED);
        }
    }
}