using PulseIQ.Local.Client.Unity3DLib;
using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.User
{
    public class OpCmdGetWaitingWorld : EventCommand
    {
        public override void Execute()
        {
            OperationsWorld.GetWaitingWorld(ClientInstance.Instance, ClientBridge.Instance.CurUserID);
        }
    }
}