﻿using PulseIQ.Local.ClientCommon.ItemManager;
using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.User
{
    public class OpCmdLoginFailed : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(IntCmdCodeUser.LOGIN_FAILED, evt.data);
        }
    }
}
