﻿namespace PulseIQ.Local.ClientCommon.User
{
    public interface IUserModel
    {
        string UserId { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        bool toReconnect { get; set; }
    }
}