﻿namespace PulseIQ.Local.ClientCommon.User
{
    public class UserModel : IUserModel
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool toReconnect { get; set; }
    }
}