﻿using strange.extensions.command.impl;

using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace PulseIQ.Local.ClientCommon.SlotItem
{
    public class OpCmdCreatedSlotItem : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(IntCmdCodeSlotItem.CREATED, evt.data);
        }
    }
}