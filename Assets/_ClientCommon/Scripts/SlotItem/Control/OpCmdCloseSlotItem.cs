﻿using strange.extensions.command.impl;

using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace PulseIQ.Local.ClientCommon.SlotItem
{
    public class OpCmdCloseSlotItem : EventCommand
    {
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            string itemID = (string)data.Parameters[(byte)CommonParameterCode.ITEMID];

            OperationsSlot.CloseSlot(ClientBridge.Instance, itemID, ClientBridge.Instance.CurUserID, ClientBridge.Instance.CurWorldData.Id);
        }
    }
}