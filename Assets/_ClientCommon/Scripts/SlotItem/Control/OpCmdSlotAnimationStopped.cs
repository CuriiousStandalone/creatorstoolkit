﻿using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.SlotItem
{
    public class OpCmdSlotAnimationStopped : EventCommand
    {
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;
            data.Parameters.Add((byte)CommonParameterCode.OPERATION_CODE, evt.type);

            dispatcher.Dispatch(IntCmdCodeSlotItem.SLOT_STATE_ANI_STOPPED, data);
        }
    }
}
