﻿namespace PulseIQ.Local.ClientCommon.SlotItem
{
    public enum OpCmdCodeSlotItem : byte
    {
        CREATE,
        CLOSE,
        OPEN,
        SHOW_INDICATOR,
        HIDE_INDICATOR,
        SPECTATE_SLOT,
        STOP_SPECTATE,
    }
}