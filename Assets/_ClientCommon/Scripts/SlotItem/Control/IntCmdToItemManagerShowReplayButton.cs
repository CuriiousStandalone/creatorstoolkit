﻿

using PulseIQ.Local.ClientCommon.ItemManager;
using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.SlotItem
{
    public class IntCmdToItemManagerSlotAnimEnded : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(IntCmdCodeItemManager.SLOT_ANIMATION_ENDED);
        }
    }
}
