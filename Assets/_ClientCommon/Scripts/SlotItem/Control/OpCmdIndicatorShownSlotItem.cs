﻿using strange.extensions.command.impl;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace PulseIQ.Local.ClientCommon.SlotItem
{
    public class OpCmdIndicatorShownSlotItem : EventCommand
    {
        public override void Execute()
        {
            {
                MessageData data = (MessageData)evt.data;
                data.Parameters.Add((byte)CommonParameterCode.OPERATION_CODE, evt.type);

                dispatcher.Dispatch(IntCmdCodeSlotItem.INDICATOR_SHOWN, data);
            }
        }
    }
}