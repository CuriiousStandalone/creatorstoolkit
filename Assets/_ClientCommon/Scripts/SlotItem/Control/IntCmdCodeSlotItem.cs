﻿namespace PulseIQ.Local.ClientCommon.SlotItem
{
    public enum IntCmdCodeSlotItem : byte
    {
        CREATED,
        CLOSED,
        OPENED,
        INDICATOR_SHOWN,
        INDICATOR_HIDDEN,
        SLOT_STATE_ANI_PLAYED,
        SLOT_STATE_ANI_STOPPED,
        SLOT_ANI_ENDED,
    }
}