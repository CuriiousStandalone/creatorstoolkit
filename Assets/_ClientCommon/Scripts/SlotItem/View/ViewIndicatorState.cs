﻿using PulseIQ.Local.ClientCommon.Components.Common;
using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace PulseIQ.Local.ClientCommon.SlotItem
{
    public class ViewIndicatorState : MonoBehaviour
    {
        private bool isEnabled = false;

        private GameObject target = null;

        public Action onClick;

        public void UpdateSlotState(GameObject target)
        {
            if (null != target)
            {
                this.target = target;
                isEnabled = true;
            }
        }

        void Update()
        {
            if (isEnabled && null != target)
            {
                transform.LookAt(target.transform);

                Vector3 rot = transform.rotation.eulerAngles;
                rot.x = 0;
                rot.z = 0;

                transform.eulerAngles = rot;
            }
        }

        public void Highlight(float outlineWidth)
        {
            transform.GetChild(0).gameObject.GetComponent<Outline>().OutlineWidth = outlineWidth;
            transform.GetChild(1).gameObject.GetComponent<Outline>().OutlineWidth = outlineWidth;
        }

        public void OnClick()
        {
            if (null != onClick)
            {
                onClick();
            }
        }
    }
}