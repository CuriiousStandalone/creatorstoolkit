﻿using UnityEngine;

using PulseIQ.Local.ClientCommon.BaseItem;
using PulseIQ.Local.Common.CustomType;
using PulseIQ.Local.Common.Model.Slot;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;
using System.Collections.Generic;
using UnityEngine.UI;
using PulseIQ.Local.ClientCommon.Custom;
using System.Collections;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.Slot;
using PulseIQ.Local.Common.Model.SlotGroup;

namespace PulseIQ.Local.ClientCommon.SlotItem
{
    public class ViewSlotItem : ViewItemBase
    {
        internal const string TELEPORT_CAMERA = "TELEPORT_CAMERA";
        internal const string RETURN_CAMERA = "RETURN_CAMERA";
        internal const string SPECTATE_PRESSED = "SPECTATE_PRESSED";
        internal const string FADE_CAMERA = "FADE_CAMERA";
        internal const string STOP_SPECTATE = "STOP_SPECTATE";
        internal const string ANIMATION_ENDED = "ANIMATION_ENDED";

        public int slotID;

        protected Vector3 localSlotLocation = new Vector3(0, 0, 0);
        protected Quaternion localSlotRotation = new Quaternion(0, 0, 0, 0);
        protected Vector3 localSlotScale = new Vector3(1, 1, 1);

        [SerializeField]
        private GameObject slotIndicator = null;
        private GameObject animatedObject = null;

        [SerializeField]
        private MeshRenderer indicatorMesh;

        private Collider slotCollider;

        private GameObject mainCameraRef;
        private GameObject correctIncorrectObj = null;
        
        private string triggerStateName = "";
        private Transform SlotAnimCamera = null;

        Dictionary<ESlotState, string> triggerList = new Dictionary<ESlotState, string>();
        ESlotState currentSlotState = ESlotState.EMPTY;
        ESlotState currentObjState = ESlotState.EMPTY;

        private bool teleportToSlotSpec = false;
        private bool notIdling = false;
        private bool localTeleport = false;

        private List<Renderer> allRenderers = new List<Renderer>();
        private Dictionary<string, List<Renderer>> stateRendererLists = new Dictionary<string, List<Renderer>>();
        private Dictionary<string, List<Animator>> stateAnimatorLists = new Dictionary<string, List<Animator>>();

        protected internal override void init()
        {
            itemType = Local.Common.ItemTypeCode.SLOT;

            if (null != slotCollider)
            {
                slotCollider = slotIndicator.GetComponent<Collider>();
            }

            slotIndicator = transform.Find("Indicator").gameObject;
            SlotAnimCamera = FindChildTransform(this.transform, "SlotAniCamera");

            if (null == SlotAnimCamera)
            {
                Debug.Log("Slot Animation Camera is NULL!!!   " + itemID);
            }
        }

        private void OnDestroy()
        {
            if (null != correctIncorrectObj)
            {
                Destroy(correctIncorrectObj);
            }

            stateRendererLists.Clear();
            stateAnimatorLists.Clear();
            allRenderers.Clear();

            if (null != correctIncorrectObj)
            {
                correctIncorrectObj.GetComponent<ViewIndicatorState>().onClick -= TeleportToSpecatorPostion;
            }

            base.OnDestroy();
        }

        private void Update()
        {
            base.Update();

            if (teleportToSlotSpec)
            {
                notIdling = false;

                MessageData data = new MessageData();
                data.Parameters.Add((byte)CommonParameterCode.MAIN_CAMERA_POSITION, SlotAnimCamera.position);
                data.Parameters.Add((byte)CommonParameterCode.MAIN_CAMERA_ROTATION, SlotAnimCamera.rotation);

                dispatcher.Dispatch(TELEPORT_CAMERA, data);

                foreach (Animator anim in stateAnimatorLists[triggerStateName])
                {
                    bool temp = anim.GetBool(triggerStateName);

                    if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Idle") || temp)
                    {
                        notIdling = true; 
                    }
                }

                if (!notIdling)
                {
                    teleportToSlotSpec = false;

                    MessageData newData = new MessageData();
                    newData.Parameters.Add((byte)CommonParameterCode.TOTAL_TIME, 2f);
                    dispatcher.Dispatch(FADE_CAMERA, newData);

                    Invoke("DelayedTeleportBack", 1f);
                }
            }
        }

        private void DelayedTeleportBack()
        {
            ReturnToOriginalPosition();
        }

        private Transform FindChildTransform(Transform parent, string childName)
        {
            Transform result = parent.Find(childName);

            if (result != null)
            {
                return result;
            }

            foreach (Transform child in parent)
            {
                result = FindChildTransform(child, childName);

                if (result != null)
                {
                    return result;
                }
            }
            return null;
        }

        public void SetDefaultDetails(CTVector3 localPos, CTQuaternion localRot, CTVector3 localScale, string slotID, bool isOpen, bool isIndicating, ESlotState slotState, string emptyTriggerName, string correctTriggerName, string incorrectTriggerName)
        {
            if(null == slotIndicator)
            {
                slotIndicator = transform.Find("Indicator").gameObject;
                slotIndicator.SetActive(true);
            }
            else
            {
                slotIndicator.SetActive(true);
            }

            triggerList.Clear();
            stateRendererLists.Clear();
            stateAnimatorLists.Clear();
            allRenderers.Clear();

            localSlotLocation = new Vector3(localPos.X, localPos.Y, localPos.Z);
            localSlotRotation = new Quaternion(localRot.X, localRot.Y, localRot.Z, localRot.W);
            localSlotScale = new Vector3(localScale.X, localScale.Y, localScale.Z);
            this.slotID = int.Parse(slotID);
            currentSlotState = slotState;

            triggerList.Add(ESlotState.EMPTY, emptyTriggerName);
            triggerList.Add(ESlotState.CORRECT, correctTriggerName);
            triggerList.Add(ESlotState.INCORRECT, incorrectTriggerName);

            foreach (Renderer rend in GetComponentsInChildren<Renderer>())
            {
                allRenderers.Add(rend);
            }

            GetStateLists(emptyTriggerName);
            GetStateLists(correctTriggerName);
            GetStateLists(incorrectTriggerName);

            if (slotIndicator == null)
            {
                slotIndicator = transform.Find("Indicator").gameObject;
            }

            if (!isOpen)
            {
                SlotClosed();
            }
            else
            {
                SlotOpen();
            }

            if (!isIndicating)
            {
                SlotIndicatorHidden();
            }
            else
            {
                SlotIndicatorShown(slotState);
            }
        }

        private void GetStateLists(string stateName)
        {
            Animator[] animList = GetComponentsInChildren<Animator>();

            bool hasParam = false;

            List<Renderer> rendList = new List<Renderer>();

            foreach (Animator anim in animList)
            {
                hasParam = false;

                foreach (AnimatorControllerParameter param in anim.parameters)
                {
                    if (param.name == stateName)
                    {
                        hasParam = true;
                    }
                }

                if (hasParam)
                {
                    foreach (Renderer mesh in anim.GetComponentsInChildren<Renderer>())
                    {
                        rendList.Add(mesh);
                    }
                }
            }

            if (!stateRendererLists.ContainsKey(stateName))
            {
                stateRendererLists.Add(stateName, rendList);
            }

            List<Animator> animatorList = new List<Animator>();

            foreach (Animator anim in animList)
            {
                foreach (AnimatorControllerParameter param in anim.parameters)
                {
                    if (param.name == stateName)
                    {
                        animatorList.Add(anim);
                        break;
                    }
                }
            }

            if (!stateAnimatorLists.ContainsKey(stateName))
            {
                stateAnimatorLists.Add(stateName, animatorList);
            }
        }

        public void ReferenceMainCamera(GameObject mainCameraRef)
        {
            this.mainCameraRef = mainCameraRef;
        }

        public void SlotOpen()
        {
            if (null == slotCollider)
            {
                slotCollider = slotIndicator.GetComponent<Collider>();
            }

            slotCollider.enabled = true;
        }

        public void SlotClosed()
        {
            if (null == slotCollider)
            {
                slotCollider = slotIndicator.GetComponent<Collider>();
            }

            slotCollider.enabled = false;

            try
            {
                if (null != gameObject.GetComponent<Animation>())
                {
                    gameObject.GetComponent<Animation>().Play();
                }
            }
            catch
            {
                Debug.Log("No Animation Present!");
            }

        }

        public void SlotIndicatorShown(ESlotState slotState = ESlotState.EMPTY)
        {
            bool exists = false;
            bool isEmpty = true;
            currentSlotState = slotState;

            switch (slotState)
            {
                default:
                case ESlotState.EMPTY:
                    {
                        slotIndicator.SetActive(true);

                        if(null != slotIndicator.GetComponent<Renderer>())
                        {
                            slotIndicator.GetComponent<Renderer>().enabled = true;
                        }

                        if (null != slotIndicator.GetComponent<Collider>())
                        {
                            slotIndicator.GetComponent<Collider>().enabled = true;
                        }

                        isEmpty = true;
                        if (null != correctIncorrectObj)
                        {
                            Destroy(correctIncorrectObj);
                            correctIncorrectObj = null;
                        }
                        currentObjState = ESlotState.EMPTY;
                        break;
                    }

                case ESlotState.CORRECT:
                    {
                        if (currentObjState == ESlotState.CORRECT && null != correctIncorrectObj)
                        {
                            correctIncorrectObj.SetActive(true);
                            exists = true;

                            if (!stateAnimatorLists.ContainsKey("CORRECT"))
                            {
                                correctIncorrectObj.GetComponent<Collider>().enabled = false;
                            }
                            else
                            {
                                correctIncorrectObj.GetComponent<Collider>().enabled = true;
                            }

                            break;
                        }
                        else
                        {
                            slotIndicator.SetActive(false);
                            isEmpty = false;
                            if (null != correctIncorrectObj)
                            {
                                Destroy(correctIncorrectObj);
                                correctIncorrectObj = null;
                            }

                            correctIncorrectObj = Instantiate(Resources.Load("Prefabs/CorrectSlotIndicator3D") as GameObject, gameObject.transform.parent);

                            if (!stateAnimatorLists.ContainsKey("CORRECT"))
                            {
                                correctIncorrectObj.GetComponent<Collider>().enabled = false;
                            }
                            currentObjState = ESlotState.CORRECT;
                            break;
                        }
                    }

                case ESlotState.INCORRECT:
                    {
                        if (currentObjState == ESlotState.INCORRECT &&  null != correctIncorrectObj)
                        {
                            correctIncorrectObj.SetActive(true);
                            exists = true;

                            if (!stateAnimatorLists.ContainsKey("INCORRECT"))
                            {
                                correctIncorrectObj.GetComponent<Collider>().enabled = false;
                            }
                            else
                            {
                                correctIncorrectObj.GetComponent<Collider>().enabled = true;
                            }

                            break;
                        }
                        else
                        {

                            slotIndicator.SetActive(false);
                            isEmpty = false;
                            if (null != correctIncorrectObj)
                            {
                                Destroy(correctIncorrectObj);
                                correctIncorrectObj = null;
                            }

                            correctIncorrectObj = Instantiate(Resources.Load("Prefabs/IncorrectSlotIndicator3D") as GameObject, gameObject.transform.parent);

                            if (!stateAnimatorLists.ContainsKey("INCORRECT"))
                            {
                                correctIncorrectObj.GetComponent<Collider>().enabled = false;
                            }
                            currentObjState = ESlotState.INCORRECT;
                            break;
                        }
                    }
            }

            if (!isEmpty && null != correctIncorrectObj && !exists)
            {
                correctIncorrectObj.transform.position = gameObject.transform.position;
                correctIncorrectObj.transform.localPosition += Vector3.up * 10f;
                correctIncorrectObj.GetComponent<ViewIndicatorState>().UpdateSlotState(mainCameraRef);
                correctIncorrectObj.GetComponent<ViewIndicatorState>().onClick += TeleportToSpecatorPostion;
            }
        }

        private void TriggerSlotAnimations(ESlotState slotState)
        {
            if (!stateAnimatorLists.ContainsKey(triggerList[slotState]))
            { 
                return;
            }

            foreach (Animator anim in stateAnimatorLists[triggerList[slotState]])
            {
                if (!anim.GetBool(triggerList[slotState]) && anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
                {
                    anim.enabled = true;
                    anim.SetTrigger(triggerList[slotState]);
                }
            }
        }

        private void TriggerSlotAnimations(string triggerName)
        {
            if (!stateAnimatorLists.ContainsKey(triggerName))
            {
                return;
            }

            foreach (Animator anim in stateAnimatorLists[triggerName])
            {
                if (!anim.GetBool(triggerName) && anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
                {
                    anim.enabled = true;
                    anim.SetTrigger(triggerName);
                }
            }
        }

        private void EnableMeshRenders(ESlotState stateName)
        {
            foreach (Renderer rend in allRenderers)
            {
                rend.enabled = false;

                if (null != rend.gameObject.GetComponent<Collider>())
                {
                    rend.gameObject.GetComponent<Collider>().enabled = false;
                }
            }

            if (triggerList.ContainsKey(stateName))
            {
                foreach (Renderer rend in stateRendererLists[triggerList[stateName]])
                {
                    rend.enabled = true;
                    if (null != rend.gameObject.GetComponent<Collider>())
                    {
                        rend.gameObject.GetComponent<Collider>().enabled = true;
                    }
                }
            }
        }

        private void EnableMeshRenders(string triggerName)
        {
            if (!stateRendererLists.ContainsKey(triggerName))
            {
                return;
            }

            foreach (Renderer rend in allRenderers)
            {
                rend.enabled = false;
                if (null != rend.gameObject.GetComponent<Collider>())
                {
                    rend.gameObject.GetComponent<Collider>().enabled = false;
                }
            }

            if (stateRendererLists.ContainsKey(triggerName))
            {
                foreach (Renderer rend in stateRendererLists[triggerName])
                {
                    rend.enabled = true;
                    if (null != rend.gameObject.GetComponent<Collider>())
                    {
                        rend.gameObject.GetComponent<Collider>().enabled = true;
                    }
                }
            }
        }

        public void SlotIndicatorHidden()
        {
            slotIndicator.SetActive(false);

            if (null != correctIncorrectObj)
            {
                Destroy(correctIncorrectObj);
            }
        }

        public void FailedToPlaceSlot()
        {
            // Stuff and things
        }

        public bool GetIfClosed()
        {
            return !slotCollider.enabled;
        }

        public void StartAnimation(string triggerStateName)
        {
            EnableMeshRenders(triggerStateName);

            TriggerSlotAnimations(triggerStateName);

            if (localTeleport)
            {
                if (CheckIfIsSlotAnimator())
                {
                    correctIncorrectObj.SetActive(false);
                    slotIndicator.SetActive(false);

                    teleportToSlotSpec = true;

                    ShowSentenceUI();
                }
            }

            localTeleport = false;

            this.triggerStateName = triggerStateName;

            StartCoroutine(CheckIdleState(stateAnimatorLists[triggerStateName]));
        }

        public void SetStatementText(string statementText)
        {
            if(statementText == "")
            {
                return;
            }

            TextController textController = null;

            Transform textObject = FindChildTransform(transform, "Text");

            if (null != textObject)
            {
                textController = textObject.GetComponent<TextController>();
            }

            Debug.Log("statement text is: " + statementText);

            if (null != textController)
            {
                textController.SetText(statementText);
            }
        }

        public void StopAnimation()
        {
            Animator[] animators = GetComponentsInChildren<Animator>();

            foreach (Animator anim in animators)
            {
                anim.Play("Idle", 0, 0);
            }
        }

        public void TeleportToSpecatorPostion()
        {
            Invoke("DelayedTeleport", 1f);

            MessageData fadeData = new MessageData();
            fadeData.Parameters.Add((byte)CommonParameterCode.TOTAL_TIME, 1.5f);

            dispatcher.Dispatch(FADE_CAMERA, fadeData);
        }

        private void DelayEnableSpecCollider()
        {
            
        }

        private void DelayedTeleport()
        {
            localTeleport = true;

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);

            dispatcher.Dispatch(SPECTATE_PRESSED, data);
        }

        private bool CheckIfIsSlotAnimator()
        {
            foreach (Animator anim in stateAnimatorLists[triggerStateName])
            {
                foreach (AnimatorControllerParameter param in anim.parameters)
                {
                    if (param.name == triggerStateName)
                        return true;
                }
            }
            return false;
        }

        public void ReturnToOriginalPosition()
        {
            dispatcher.Dispatch(RETURN_CAMERA);

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);

            dispatcher.Dispatch(STOP_SPECTATE, data);

            if (currentSlotState == ESlotState.EMPTY)
            {
                slotIndicator.SetActive(true);
            }
            else
            {
                slotIndicator.SetActive(false);
            }

            HideSentenceUI();
        }

        public override void ShowView()
        {
            EnableMeshRenders(currentSlotState);
        }

        private void HideSentenceUI()
        {
            TextController textController = null;

            Transform textObject = FindChildTransform(transform, "Text");

            if (null != textObject)
            {
                textController = textObject.GetComponent<TextController>();
            }

            if (null != textController)
            {
                if (null != textController.gameObject.GetComponent<Renderer>())
                {
                    textController.gameObject.GetComponent<Renderer>().enabled = false;
                }

                textController.HideUI();
            }
        }

        private void ShowSentenceUI()
        {
            TextController textController = null;

            Transform textObject = FindChildTransform(transform, "Text");

            if (null != textObject)
            {
                textController = textObject.GetComponent<TextController>();
            }

            if (null != textController)
            {
                if (null != textController.gameObject.GetComponent<Renderer>())
                {
                    textController.gameObject.GetComponent<Renderer>().enabled = true;
                }

                textController.ShowUI();
            }
        }

        IEnumerator CheckIdleState(List<Animator> animList)
        {
            yield return new WaitForSeconds(1f);

            bool isIdle = true;

            foreach (Animator anim in animList)
            {
                if (anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
                {
                    isIdle = true;
                }
                else
                {
                    isIdle = false;
                    break;
                }
            }

            if(isIdle)
            {
                dispatcher.Dispatch(ANIMATION_ENDED);
            }
            else
            {
                StartCoroutine(CheckIdleState(animList));
            }
        }

        public override void SyncItemData(CTItemData data)
        {
            base.SyncItemData(data);

            //CTSlotData slotData = (CTSlotData)data;
            //
            //localSlotLocation = new Vector3(slotData.AttachedLocalPosition.X, slotData.AttachedLocalPosition.Y, slotData.AttachedLocalPosition.Z);
            //localSlotRotation = new Quaternion(slotData.AttachedLocalRotation.X, slotData.AttachedLocalRotation.Y, slotData.AttachedLocalRotation.Z, slotData.AttachedLocalRotation.W);
            //localSlotScale = new Vector3(slotData.AttachedLocalScale.X, slotData.AttachedLocalScale.Y, slotData.AttachedLocalScale.Z);
            //slotID = slotData.SlotId;
            //
            //if (!slotData.IsOpen)
            //{
            //    SlotClosed();
            //}
            //else
            //{
            //    SlotOpen();
            //}
            //
            //if (!slotData.IsIndicating)
            //{
            //    SlotIndicatorHidden();
            //}
            //
            //triggerList.Clear();
            //triggerList.Add(ESlotState.EMPTY, slotData.EmptyStateAniName);
            //triggerList.Add(ESlotState.CORRECT, slotData.CorrectStateAniName);
            //triggerList.Add(ESlotState.INCORRECT, slotData.IncorrectStateAniName);
            //
            //allRenderers.Clear();
            //foreach (Renderer rend in GetComponentsInChildren<Renderer>())
            //{
            //    allRenderers.Add(rend);
            //}
            //
            //GetStateLists(slotData.EmptyStateAniName);
            //GetStateLists(slotData.CorrectStateAniName);
            //GetStateLists(slotData.IncorrectStateAniName);

        }

        public override void SlotGroupSynced(float elapsedTime)
        {
            Debug.Log("Slot Item!!!!!!!!!!!  " + elapsedTime);
            if (elapsedTime == -1)
            {
                return;
            }
            else
            {
                foreach (Animator anim in GetComponentsInChildren<Animator>())
                {
                    anim.enabled = true;

                    if (elapsedTime < anim.GetCurrentAnimatorStateInfo(0).length)
                    {
                        float time = elapsedTime / anim.GetCurrentAnimatorStateInfo(0).length;
                        anim.Play(0, 0, time);
                    }
                    else
                    {
                        if (anim.GetCurrentAnimatorStateInfo(0).loop)
                        {
                            float time = elapsedTime % anim.GetCurrentAnimatorStateInfo(0).length;
                            time = time / anim.GetCurrentAnimatorStateInfo(0).length;
                            anim.Play(0, 0, time);
                        }
                        else
                        {
                            anim.Play(0, 0, 1f);
                        }
                    }
                }

                foreach (Renderer renderer in GetComponentsInChildren<Renderer>())
                {
                    renderer.enabled = true;
                }

                if (null != gameObject.GetComponent<Collider>())
                {
                    gameObject.GetComponent<Collider>().enabled = true;
                }

                for (int i = 0; i < transform.childCount; i++)
                {
                    if (null != transform.GetChild(i).gameObject.GetComponent<Collider>())
                    {
                        transform.GetChild(i).gameObject.GetComponent<Collider>().enabled = true;
                    }
                }
            }
        }
    }
}