﻿using strange.extensions.mediation.impl;

using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.Slot;
using strange.extensions.dispatcher.eventdispatcher.api;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.SlotGroup;
using PulseIQ.Local.Common.CustomType;

namespace PulseIQ.Local.ClientCommon.SlotItem
{
    public class MediatorSlotItem : EventMediator
    {
        [Inject]
        public ViewSlotItem view { get; set; }

        public override void OnRegister()
        {
            view.init();
            UpdateListeners(true);
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewSlotItem.TELEPORT_CAMERA, OnTeleportCamera);
            view.dispatcher.UpdateListener(value, ViewSlotItem.RETURN_CAMERA, OnReturnCamera);
            view.dispatcher.UpdateListener(value, ViewSlotItem.SPECTATE_PRESSED, OnTeleportPressed);
            view.dispatcher.UpdateListener(value, ViewSlotItem.FADE_CAMERA, OnFadeCamera);
            view.dispatcher.UpdateListener(value, ViewSlotItem.STOP_SPECTATE, OnStopSpectate);
            view.dispatcher.UpdateListener(value, ViewSlotItem.ANIMATION_ENDED, OnAnimationEnded);
        }

        private void OnMessageReceived(System.Object data)
        {
            MessageData itemData = (MessageData)data;

            ServerCmdCode Code = (ServerCmdCode)itemData[(byte)CommonParameterCode.OPERATION_CODE];

            switch (Code)
            {
                case ServerCmdCode.SLOT_CLOSED:
                    {
                        view.SlotClosed();
                        break;
                    }

                case ServerCmdCode.SLOT_OPENED:
                    {
                        view.SlotOpen();
                        break;
                    }

                case ServerCmdCode.SLOT_INDICATOR_SHOWN:
                    {
                        view.SlotIndicatorShown((ESlotState)itemData[(byte)CommonParameterCode.SLOT_STATE]);
                        break;
                    }

                case ServerCmdCode.SLOT_INDICATOR_HIDDEN:
                    {
                        view.SlotIndicatorHidden();
                        break;
                    }

                case ServerCmdCode.ITEM_MOVED:
                    {
                        CTVector3 pos = (CTVector3)itemData[(byte)CommonParameterCode.ITEM_POSITION];
                        view.UpdatePosition(pos);
                        break;
                    }

                case ServerCmdCode.ITEM_ROTATED:
                    {
                        CTQuaternion rot = (CTQuaternion)itemData[(byte)CommonParameterCode.ITEM_ROTATION];
                        view.UpdateRotation(rot);
                        break;
                    }

                case ServerCmdCode.ITEM_SCALED:
                    {
                        CTVector3 scale = (CTVector3)itemData[(byte)CommonParameterCode.ITEM_SCALE];
                        view.UpdateScale(scale);
                        break;
                    }

                case ServerCmdCode.ITEM_OBTAIN_FAILED:
                    {
                        view.ObtainedItemFailed();
                        break;
                    }

                case ServerCmdCode.ITEM_OBTAIN_SUCCESS:
                    {
                        view.ObtainedItem();
                        break;
                    }

                case ServerCmdCode.ITEM_ABANDONED:
                    {
                        view.ItemAbandoned();
                        break;
                    }
                case ServerCmdCode.SLOT_STATE_ANI_PLAYED:
                    {
                        view.SetStatementText((string)itemData[(byte)CommonParameterCode.GENERAL_TEXT]);
                        view.StartAnimation((string)itemData[(byte)CommonParameterCode.ANIMATION_NAME]);
                        break;
                    }
                case ServerCmdCode.SLOT_STATE_ANI_STOPPED:
                    {
                        view.StopAnimation();
                        break;
                    }

                case ServerCmdCode.ITEM_SHOWN:
                    {
                        view.ShowView();
                        break;
                    }

                case ServerCmdCode.ITEM_HIDDEN:
                    {
                        view.HideView();
                        break;
                    }

                case ServerCmdCode.SLOT_GROUP_FINISH_ANI_PLAYED:
                    {
                        break;
                    }

                case ServerCmdCode.ITEM_STATUS_SYNCED:
                    {
                        CTItemData syncData = (CTItemData)(itemData[(byte)CommonParameterCode.DATA_CT_ITEM_DATA]);
                        view.SyncItemData(syncData);
                        break;
                    }

                case ServerCmdCode.SLOT_GROUP_STATUS_SYNC:
                    {
                        CTSlotGroupData slotData = (CTSlotGroupData)(itemData[(byte)CommonParameterCode.DATA_CT_SLOT_GROUP_DATA]);
                        view.SlotGroupSynced(slotData.SlotGroupFinishAniTimeElapsed);
                        break;
                    }
            }
        }

        private void OnTeleportCamera(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.OBSERVE_SLOT_ITEM, evt.data);
        }

        private void OnReturnCamera(IEvent evt)
        {
            MessageData emptyData = new MessageData();
            dispatcher.Dispatch(ECommonViewEvent.STOP_OBSERVE_SLOT_ITEM, emptyData);
        }

        private void OnTeleportPressed(IEvent evt)
        {
            dispatcher.Dispatch(OpCmdCodeSlotItem.SPECTATE_SLOT, evt.data);
        }

        private void OnFadeCamera(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.FADE_CAMERA_IN_OUT, evt.data);
        }

        private void OnStopSpectate(IEvent evt)
        {
            dispatcher.Dispatch(OpCmdCodeSlotItem.STOP_SPECTATE, evt.data);
        }

        private void OnAnimationEnded()
        {
            dispatcher.Dispatch(IntCmdCodeSlotItem.SLOT_ANI_ENDED);
        }
    }
}