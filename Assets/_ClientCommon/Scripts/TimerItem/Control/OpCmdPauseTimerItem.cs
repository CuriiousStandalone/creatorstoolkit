﻿using strange.extensions.command.impl;

using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace PulseIQ.Local.ClientCommon.TimerItem
{
    public class OpCmdPauseTimerItem : EventCommand
    {
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            OperationsSyncTimer.PauseSyncTimer(ClientBridge.Instance, ClientBridge.Instance.CurUserID, ClientBridge.Instance.CurWorldData.Id, (string)data[(byte)CommonParameterCode.ITEMID]);
        }
    }
}