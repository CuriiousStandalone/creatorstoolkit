﻿namespace PulseIQ.Local.ClientCommon.TimerItem
{
    public enum OpCmdCodeTimerItem : byte
    {
        CREATE,
        START,
        PAUSE,
        RESET,
    }
}