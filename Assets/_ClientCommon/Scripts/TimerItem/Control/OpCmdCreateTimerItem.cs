﻿using strange.extensions.command.impl;

using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.Common.CustomType;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace PulseIQ.Local.ClientCommon.TimerItem
{
    public class OpCmdCreateTimerItem : EventCommand
    {
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            string resourceID = (string)data.Parameters[(byte)CommonParameterCode.RESOURCEID];
            string worldID = (string)data.Parameters[(byte)CommonParameterCode.WORLDID];
            CTVector3 localPos = (CTVector3)data.Parameters[(byte)CommonParameterCode.ITEM_LOCAL_POSITION];
            CTQuaternion localRot = (CTQuaternion)data.Parameters[(byte)CommonParameterCode.ITEM_LOCAL_ROTATION];
            CTVector3 localScale = (CTVector3)data.Parameters[(byte)CommonParameterCode.ITEM_LOCAL_SCALE];

            OperationsItem.CreateItem(ClientBridge.Instance, resourceID, worldID, localPos, localRot, localScale);
        }
    }
}