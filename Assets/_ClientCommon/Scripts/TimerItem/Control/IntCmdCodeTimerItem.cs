﻿namespace PulseIQ.Local.ClientCommon.TimerItem
{
    public enum IntCmdCodeTimerItem : byte
    {
        CREATED,
        ELAPSED,
        PAUSED,
        RESTARTED,
        STARTED,
        ENDED,
    }
}