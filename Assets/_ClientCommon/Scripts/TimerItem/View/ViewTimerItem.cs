﻿using UnityEngine;

using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

using PulseIQ.Local.ClientCommon.BaseItem;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.SyncTimer;

namespace PulseIQ.Local.ClientCommon.TimerItem
{
    public class ViewTimerItem : ViewItemBase
    {
        internal enum TimerEvents
        {
            OBTAINED,
            ABANDONED,
        }

        public bool isCountdownTimer = false;
        public float totalTime = 0f;
        public float interval = 0f;

        private float currentTimer = 0f;

        [SerializeField]
        private Animator timerAnimation = null;
        [SerializeField]
        private TextMesh timerText = null;

        private bool uiActive = false;

        protected internal override void init()
        {
            itemType = Local.Common.ItemTypeCode.SYNCTIMER;

            if(null == timerAnimation)
            {
                timerAnimation = GetComponent<Animator>();
            }

            if (null == timerText)
            {
                timerText = transform.Find("TimerText").GetComponent<TextMesh>();
            }

            if(null != timerAnimation)
            {
                timerAnimation.speed = 0f;
            }
        }

        public void SetDefaultValues(bool isCountdownTimer, float totalTime, float interval, float curTime, bool isVisible)
        {
            this.isCountdownTimer = isCountdownTimer;
            this.totalTime = totalTime / 1000f;
            this.interval = interval / 1000f;
            this.currentTimer = curTime / 1000f;


            if (null == timerAnimation)
            {
                timerAnimation = GetComponent<Animator>();
            }

            if (this.isCountdownTimer)
            {
                float timeleft = 1f - (currentTimer / totalTime);
                timerAnimation.Play ("Animation", -1, timeleft);
            }
            else
            {
                float timeleft = currentTimer / totalTime;
                timerAnimation.Play("Animation", -1, timeleft);
            }

            if (null == timerText)
            {
                timerText = transform.Find("TimerText").GetComponent<TextMesh>();
            }
                timerText.text = ((int)currentTimer).ToString();

            if(isVisible)
            {
                ShowView();
            }
            else
            {
                HideView();
            }
        }

        public void StartTimer()
        {
            if (null != timerAnimation)
            {
                if (isCountdownTimer)
                {
                    float timeleft = 1f - (currentTimer / totalTime);
                    timerAnimation.Play("Animation", -1, timeleft);
                }
                else
                {
                    float timeleft = currentTimer / totalTime;
                    timerAnimation.Play("Animation", -1, timeleft);
                }
            }

            if (null != timerText)
            {
                timerText.text = currentTimer.ToString();
            }
        }

        public void PauseTimer(float time)
        {
            currentTimer = time / 1000f;

            timerText.text = ((int)currentTimer).ToString();

        }

        public void ResetTimer()
        {
            if(isCountdownTimer)
            {
                currentTimer = totalTime;
            }
            
            else
            {
                currentTimer = 0;
            }
        }

        public void TimeElapsed(float time)
        {
             currentTimer = time / 1000f;

             if (null != timerAnimation)
             {
                 if (isCountdownTimer)
                 {
                     float timeleft = 1f - (currentTimer / totalTime);
                     timerAnimation.Play("Animation", -1, timeleft);
                 }
                 else
                 {
                     float timeleft = currentTimer / totalTime;
                     timerAnimation.Play("Animation", -1, timeleft);
                 }
             }

             if (null != timerText)
             {
                 timerText.text = currentTimer.ToString();
             }
        }


        public void TimerEnded()
        {
            if (null != timerAnimation)
            {
                timerAnimation.Play("Animation", -1, 1);
            }
        }

        public void ObtainedTimerItem()
        {
            isObtained = true;
            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);

            dispatcher.Dispatch(TimerEvents.OBTAINED, data);
        }

        public void AbandonedTimerItem()
        {
            isObtained = false;
            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);

            dispatcher.Dispatch(TimerEvents.ABANDONED, data);
        }

        public override void SyncItemData(CTItemData data)
        {
            base.SyncItemData(data);

            //CTSyncTimerData timerData = (CTSyncTimerData)data;
            //
            //this.isCountdownTimer = timerData.IsCountDown;
            //this.totalTime = timerData.TotalTime / 1000f;
            //this.interval = timerData.Interval / 1000f;
            //
            //if (null == timerAnimation)
            //{
            //    timerAnimation = GetComponent<Animator>();
            //}
            //
            //if (this.isCountdownTimer)
            //{
            //    float timeleft = 1f - (currentTimer / totalTime);
            //    timerAnimation.Play("Animation", -1, timeleft);
            //}
            //else
            //{
            //    float timeleft = currentTimer / totalTime;
            //    timerAnimation.Play("Animation", -1, timeleft);
            //}
            //
            //if (null == timerText)
            //{
            //    timerText = transform.Find("TimerText").GetComponent<TextMesh>();
            //}
            //
            //timerText.text = ((int)currentTimer).ToString();
        }
    }
}