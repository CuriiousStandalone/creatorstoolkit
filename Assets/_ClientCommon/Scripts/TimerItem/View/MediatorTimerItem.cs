﻿using strange.extensions.mediation.impl;
using strange.extensions.dispatcher.eventdispatcher.api;

using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.Item;

namespace PulseIQ.Local.ClientCommon.TimerItem
{
    public class MediatorTimerItem : EventMediator
    {
        [Inject]
        public ViewTimerItem view { get; set; }

        public override void OnRegister()
        {
            view.init();
            UpdateListeners(true);
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewTimerItem.TimerEvents.OBTAINED, OnObtainedItem);
            view.dispatcher.UpdateListener(value, ViewTimerItem.TimerEvents.ABANDONED, OnAbandonedItem);
        }

        private void OnMessageReceived(System.Object data)
        {
            MessageData timerData = (MessageData)data;
            ServerCmdCode code = (ServerCmdCode)timerData[(byte)CommonParameterCode.OPERATION_CODE];

            switch (code)
            {
                case ServerCmdCode.TIMER_ELAPSED:
                    {
                        float time = (float)timerData[(byte)CommonParameterCode.CURRENT_TIME];
                        view.TimeElapsed(time);
                        break;
                    }

                case ServerCmdCode.TIMER_PAUSED:
                    {
                        float time = (float)timerData[(byte)CommonParameterCode.CURRENT_TIME];
                        view.PauseTimer(time);
                        break;
                    }

                case ServerCmdCode.TIMER_RESET:
                    {
                        view.ResetTimer();
                        break;
                    }

                case ServerCmdCode.TIMER_STARTED:
                    {
                        view.StartTimer();
                        break;
                    }

                case ServerCmdCode.TIMER_ENDED:
                    {
                        view.TimerEnded();
                        break;
                    }

                case ServerCmdCode.ITEM_OBTAIN_SUCCESS:
                    {
                        view.ObtainedTimerItem();
                        break;
                    }

                case ServerCmdCode.ITEM_ABANDONED:
                    {
                        view.AbandonedTimerItem();
                        break;
                    }

                case ServerCmdCode.ITEM_SHOWN:
                    {
                        view.ShowView();
                        break;
                    }

                case ServerCmdCode.ITEM_HIDDEN:
                    {
                        view.HideView();
                        break;
                    }

                case ServerCmdCode.ITEM_STATUS_SYNCED:
                    {
                        CTItemData syncData = (CTItemData)(timerData[(byte)CommonParameterCode.DATA_CT_ITEM_DATA]);
                        view.SyncItemData(syncData);
                        break;
                    }
            }
        }

        private void OnAbandonedItem(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.TIMER_DEACTIVATED, evt.data);
            dispatcher.Dispatch(ECommonViewEvent.ITEM_ABANDONED, evt.data);
        }

        private void OnObtainedItem(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.TIMER_ACTIVATED, evt.data);
            dispatcher.Dispatch(ECommonViewEvent.ITEM_OBTAINED, evt.data);
        }
    }
}