﻿using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;

using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.SlotGroup
{
    public class OpCmdSlotGroupEvaluationVisibility : EventCommand
    {
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;
            data.Parameters.Add((byte)CommonParameterCode.OPERATION_CODE, evt.type);

            dispatcher.Dispatch(IntCmdCodeSlotGroup.SLOT_GROUP_EVALUATION_VISIBILITY, data);
        }
    }
}