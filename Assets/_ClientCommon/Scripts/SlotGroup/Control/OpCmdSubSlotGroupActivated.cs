﻿using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;

using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.SlotGroup
{
    public class OpCmdSubSlotGroupActivated : EventCommand
    {
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;
            data.Parameters.Add((byte)CommonParameterCode.OPERATION_CODE, evt.type);

            dispatcher.Dispatch(IntCmdCodeSlotGroup.SUB_GROUP_ACTIVATED, data);
        }
    }
}
