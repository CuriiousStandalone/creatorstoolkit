﻿using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;

using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.SlotGroup
{
    public class OpCmdActivateSubSlotGroup : EventCommand
    {
        public override void Execute()
        {
            MessageData messageData = (MessageData)evt.data;

            string slotGroupItemId = (string)messageData[(byte)CommonParameterCode.ITEMID];
            string subSlotGroupId = (string)messageData[(byte)CommonParameterCode.SLOT_GROUP_SUB_GROUP_ID];

            OperationsSlotGroup.ActivateSubSlotGroup(ClientBridge.Instance, slotGroupItemId, ClientBridge.Instance.CurWorldData.Id, subSlotGroupId);
        }
    }
}
