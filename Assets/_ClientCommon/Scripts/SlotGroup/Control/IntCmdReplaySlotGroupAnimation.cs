﻿

using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.SlotGroup
{
    public class IntCmdReplaySlotGroupAnimation : EventCommand
    {
        public override void Execute()
        {
            MessageData messageData = (MessageData)evt.data;
            string itemID = (string)messageData[(byte)CommonParameterCode.ITEMID];

            OperationsSlotGroup.ReplaySlotGroup(ClientBridge.Instance, itemID, ClientBridge.Instance.CurWorldData.Id);
        }
    }
}
