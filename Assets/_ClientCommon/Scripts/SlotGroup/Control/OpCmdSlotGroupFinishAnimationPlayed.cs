﻿using System.Collections;

using UnityEngine;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;

using strange.extensions.command.impl;
using strange.extensions.context.api;

namespace PulseIQ.Local.ClientCommon.SlotGroup
{
    class OpCmdSlotGroupFinishAnimationPlayed : EventCommand
    {
        [Inject(ContextKeys.CONTEXT_VIEW)]
        public GameObject contextView { get; set; }

        private MonoBehaviour root;

        private MessageData mData;

        private object eventType;

        public override void Execute()
        {
            Retain();

            mData = (MessageData)evt.data;
            eventType = evt.type;

            root = contextView.GetComponent<MonoBehaviour>();
            root.StartCoroutine(GiveShortDelay());
        }

        IEnumerator GiveShortDelay()
        {
            yield return new WaitForSeconds(1.0f);

            MessageData messageData = new MessageData();
            
            messageData.Parameters.Add((byte)CommonParameterCode.OPERATION_CODE, eventType);
            messageData.Parameters.Add((byte)CommonParameterCode.ITEM_INDEX, (int)mData[(byte)CommonParameterCode.SLOT_GROUP_FINISH_ANIMATION_ITEM_INDEX]);

            dispatcher.Dispatch(IntCmdCodeSlotGroup.SLOT_GROUP_FINISH_ANI_PLAYED, messageData);

            Release();
        }
    }
}
