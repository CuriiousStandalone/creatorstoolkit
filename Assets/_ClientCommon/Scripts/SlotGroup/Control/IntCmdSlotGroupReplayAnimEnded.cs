﻿using strange.extensions.command.impl;

using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;

namespace PulseIQ.Local.ClientCommon.SlotGroup
{
    public class IntCmdSlotGroupReplayAnimEnded : EventCommand
    {
        public override void Execute()
        {
            MessageData messageData = (MessageData)evt.data;
            string itemId = (string)messageData[(byte)CommonParameterCode.ITEMID];

            OperationsSlotGroup.StopReplaySlotGroup(ClientBridge.Instance, itemId, ClientBridge.Instance.CurWorldData.Id);
        }
    }
}
