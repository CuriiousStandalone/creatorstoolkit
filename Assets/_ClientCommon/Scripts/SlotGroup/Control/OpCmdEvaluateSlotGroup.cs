﻿using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using strange.extensions.command.impl;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.SlotGroup
{
    public class OpCmdEvaluateSlotGroup : EventCommand
    {
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            string itemID = (string)data[(byte)CommonParameterCode.ITEMID];

            OperationsSlotGroup.EvaluateSlotGroup(ClientBridge.Instance, itemID, ClientBridge.Instance.CurWorldData.Id);
        }
    }
}