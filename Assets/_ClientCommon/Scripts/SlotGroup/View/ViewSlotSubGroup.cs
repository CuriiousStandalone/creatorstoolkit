﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PulseIQ.Local.ClientCommon.SlotGroup
{
    public class ViewSlotSubGroup
    {
        private string slotSubGroupID;

        public string SlotSubGroupID
        {
            get
            {
                return slotSubGroupID;
            }

            set
            {
                slotSubGroupID = value;
            }
        }
    }
}
