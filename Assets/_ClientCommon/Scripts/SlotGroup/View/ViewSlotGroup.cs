﻿using System.Collections.Generic;

using UnityEngine;

using PulseIQ.Local.ClientCommon.BaseItem;
using PulseIQ.Local.Common.CustomType;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.SlotGroup;

namespace PulseIQ.Local.ClientCommon.SlotGroup
{
    public class ViewSlotGroup : ViewItemBase
    {
        protected internal enum ViewSlotGroupEvents
        {
            NEXT_PRESSED,
            PREVIOUS_PRESSED,
            GROUP_EVALUATED,
            UPDATE_SUB_GROUP_NAME,
            GROUP_EVALUATION_ENABLED,
            GROUP_EVALUATION_DISABLED,
            REPLAY_ANIMATION_ENDED,
        }

        private Vector3 slotGroupCamScale = new Vector3(1, 1, 1);
        private Vector3 slotGroupCamPosition = new Vector3(0, 0, 0);
        private Quaternion slotGroupCamRotation = new Quaternion(0, 0, 0, 0);

        private int slotGroupFinishAnimItemId = -1;

        private int currentSubSlotGroupIndex = -1;
        private int slotGroupControlUIItemIndex = -1;

        private string currentSubSlotGroupId = "";
        private string currentSubSlotGroupName = "";

        private List<string> slotSubGroupIdList = new List<string>();

        private List<ViewSlotSubGroup> slotSubGroupList = new List<ViewSlotSubGroup>();

        internal void init()
        {
            itemType = ItemTypeCode.SLOT_GROUP;
        }

        public void SetDefaultDetails(CTVector3 camPos, CTQuaternion camRot, CTVector3 camScl, int slotGroupFinishAnimItemId, int slotGroupControlUIItemIndex, List<string> subGroupList)
        {
            slotGroupCamPosition = new Vector3(camPos.X, camPos.Y, camPos.Z);
            slotGroupCamRotation = new Quaternion(camRot.X, camRot.Y, camRot.Z, camRot.W);
            slotGroupCamScale = new Vector3(camScl.X, camScl.Y, camScl.Z);

            this.slotGroupFinishAnimItemId = slotGroupFinishAnimItemId;
            this.slotGroupControlUIItemIndex = slotGroupControlUIItemIndex;

            slotSubGroupIdList = subGroupList;

            InstantiateSubGroups();
        }

        private void InstantiateSubGroups()
        {
            foreach (string subGroup in slotSubGroupIdList)
            {
                ViewSlotSubGroup slotSubGroup = new ViewSlotSubGroup
                {
                    SlotSubGroupID = subGroup
                };

                slotSubGroupList.Add(slotSubGroup);
            }
        }

        public void ActivateSlotSubGroup(MessageData messageData)
        {
            currentSubSlotGroupId = (string)messageData[(byte)CommonParameterCode.SLOT_GROUP_SUB_GROUP_ID];

            currentSubSlotGroupIndex = slotSubGroupIdList.IndexOf(currentSubSlotGroupId);

            currentSubSlotGroupName = (string)messageData[(byte)CommonParameterCode.SLOT_GROUP_SUB_GROUP_NAME];

            UpdateSubGroupName();
        }

        private void UpdateSubGroupName()
        {
            MessageData messageData = new MessageData();
            messageData.Parameters.Add((byte)CommonParameterCode.ITEM_INDEX, slotGroupControlUIItemIndex);
            messageData.Parameters.Add((byte)CommonParameterCode.GENERAL_TEXT, currentSubSlotGroupName);
            messageData.Parameters.Add((byte)CommonParameterCode.OPERATION_CODE, ServerCmdCode.SLOT_GROUP_SUBSLOTGROUP_ACTIVATED);

            dispatcher.Dispatch(ViewSlotGroupEvents.UPDATE_SUB_GROUP_NAME, messageData);
        }

        public void SlotGroupEvaluationEnabled()
        {
            MessageData messageData = new MessageData();
            messageData.Parameters.Add((byte)CommonParameterCode.ITEM_INDEX, slotGroupControlUIItemIndex);
            messageData.Parameters.Add((byte)CommonParameterCode.OPERATION_CODE, ServerCmdCode.SLOT_GROUP_EVALUATION_ENABLED);

            dispatcher.Dispatch(ViewSlotGroupEvents.GROUP_EVALUATION_ENABLED, messageData);
        }

        public void SlotGroupEvaluationDisabled()
        {
            MessageData messageData = new MessageData();
            messageData.Parameters.Add((byte)CommonParameterCode.ITEM_INDEX, slotGroupControlUIItemIndex);
            messageData.Parameters.Add((byte)CommonParameterCode.OPERATION_CODE, ServerCmdCode.SLOT_GROUP_EVALUATION_DISABLED);

            dispatcher.Dispatch(ViewSlotGroupEvents.GROUP_EVALUATION_DISABLED, messageData);
        }

        public void NavigateNext()
        {
            if(slotSubGroupIdList.Count < 1)
            {
                return;
            }

            int nextIndex = currentSubSlotGroupIndex + 1;

            if(nextIndex >= slotSubGroupIdList.Count)
            {
                nextIndex = 0;
            }

            MessageData messageData = new MessageData();
            messageData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);
            messageData.Parameters.Add((byte)CommonParameterCode.SLOT_GROUP_SUB_GROUP_ID, slotSubGroupIdList[nextIndex]);

            dispatcher.Dispatch(ViewSlotGroupEvents.NEXT_PRESSED, messageData);
        }

        public void NavigatePrevious()
        {
            if (slotSubGroupIdList.Count < 1)
            {
                return;
            }

            int previousIndex = currentSubSlotGroupIndex - 1;

            if (previousIndex < 0)
            {
                previousIndex = slotSubGroupIdList.Count - 1;
            }

            MessageData messageData = new MessageData();
            messageData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);
            messageData.Parameters.Add((byte)CommonParameterCode.SLOT_GROUP_SUB_GROUP_ID, slotSubGroupIdList[previousIndex]);

            dispatcher.Dispatch(ViewSlotGroupEvents.PREVIOUS_PRESSED, messageData);
        }

        public void EvaluatedSlotGroup()
        {
            MessageData camData = new MessageData();
            camData.Parameters.Add((byte)CommonParameterCode.SLOT_GROUP_CAM_POS, slotGroupCamPosition);
            camData.Parameters.Add((byte)CommonParameterCode.SLOT_GROUP_CAM_ROT, slotGroupCamRotation);
            camData.Parameters.Add((byte)CommonParameterCode.SLOT_GROUP_CAM_SCL, slotGroupCamScale);

            dispatcher.Dispatch(ViewSlotGroupEvents.GROUP_EVALUATED, camData);
        }

        public void ReplayAnimEnded()
        {
            MessageData messageData = new MessageData();
            messageData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);

            dispatcher.Dispatch(ViewSlotGroupEvents.REPLAY_ANIMATION_ENDED, messageData);
        }

        public override void SyncItemData(CTItemData data)
        {
            base.SyncItemData(data);

            //CTSlotGroupData slotGroupData = (CTSlotGroupData)data;
            //
            //slotGroupCamPosition = new Vector3(slotGroupData.SlotGroupCamPosition.X, slotGroupData.SlotGroupCamPosition.Y, slotGroupData.SlotGroupCamPosition.Z);
            //slotGroupCamRotation = new Quaternion(slotGroupData.SlotGroupCamRotation.X, slotGroupData.SlotGroupCamRotation.Y, slotGroupData.SlotGroupCamRotation.Z, slotGroupData.SlotGroupCamRotation.W);
            //slotGroupCamScale = new Vector3(slotGroupData.SlotGroupCamScale.X, slotGroupData.SlotGroupCamScale.Y, slotGroupData.SlotGroupCamScale.Z);
            //
            //this.slotGroupFinishAnimItemId = slotGroupData.SlotGroupFinishAniItemIndex;
            //this.slotGroupControlUIItemIndex = slotGroupData.ControlUIItemIndex;
            //
            //slotSubGroupIdList.Clear();
            //
            //foreach (SubSlotGroupData name in slotGroupData.ListSubSlotGroup)
            //{
            //    slotSubGroupIdList.Add(name.SubSlotGroupID);
            //}
            //
            //InstantiateSubGroups();
        }

        public void SyncSlotGroup(CTSlotGroupData slotGroupData)
        {
            slotGroupCamPosition = new Vector3(slotGroupData.SlotGroupCamPosition.X, slotGroupData.SlotGroupCamPosition.Y, slotGroupData.SlotGroupCamPosition.Z);
            slotGroupCamRotation = new Quaternion(slotGroupData.SlotGroupCamRotation.X, slotGroupData.SlotGroupCamRotation.Y, slotGroupData.SlotGroupCamRotation.Z, slotGroupData.SlotGroupCamRotation.W);
            slotGroupCamScale = new Vector3(slotGroupData.SlotGroupCamScale.X, slotGroupData.SlotGroupCamScale.Y, slotGroupData.SlotGroupCamScale.Z);
            
            this.slotGroupFinishAnimItemId = slotGroupData.SlotGroupFinishAniItemIndex;
            this.slotGroupControlUIItemIndex = slotGroupData.ControlUIItemIndex;
            
            slotSubGroupIdList.Clear();
            
            foreach (SubSlotGroupData name in slotGroupData.ListSubSlotGroup)   
            {
                slotSubGroupIdList.Add(name.SubSlotGroupID);
            }
            
            InstantiateSubGroups();
        }
    }
}
 