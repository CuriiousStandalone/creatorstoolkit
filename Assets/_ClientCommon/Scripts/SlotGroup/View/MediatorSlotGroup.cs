﻿using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.UIItem;
using PulseIQ.Local.Common;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.SlotGroup;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

namespace PulseIQ.Local.ClientCommon.SlotGroup
{
    public class MediatorSlotGroup : EventMediator
    {
        [Inject]
        public ViewSlotGroup view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewSlotGroup.ViewSlotGroupEvents.NEXT_PRESSED, OnActivateSubgroup);
            view.dispatcher.UpdateListener(value, ViewSlotGroup.ViewSlotGroupEvents.PREVIOUS_PRESSED, OnActivateSubgroup);
            view.dispatcher.UpdateListener(value, ViewSlotGroup.ViewSlotGroupEvents.UPDATE_SUB_GROUP_NAME, OnUpdateSubSlotGroupName);
            view.dispatcher.UpdateListener(value, ViewSlotGroup.ViewSlotGroupEvents.GROUP_EVALUATED, OnGroupEvaluated);
            view.dispatcher.UpdateListener(value, ViewSlotGroup.ViewSlotGroupEvents.GROUP_EVALUATION_ENABLED, OnGroupEvaluationVisibility);
            view.dispatcher.UpdateListener(value, ViewSlotGroup.ViewSlotGroupEvents.GROUP_EVALUATION_DISABLED, OnGroupEvaluationVisibility);
            view.dispatcher.UpdateListener(value, ViewSlotGroup.ViewSlotGroupEvents.REPLAY_ANIMATION_ENDED, OnReplayAnimationEnded);
        }

        private void OnMessageReceived(System.Object data)
        {
            MessageData itemData = (MessageData)data;

            ServerCmdCode Code = (ServerCmdCode)itemData[(byte)CommonParameterCode.OPERATION_CODE];

            switch (Code)
            {
                case ServerCmdCode.SLOT_GROUP_SUBSLOTGROUP_ACTIVATED:
                    {
                        view.ActivateSlotSubGroup(itemData);
                        break;
                    }

                case ServerCmdCode.SLOT_GROUP_EVALUATED:
                    {
                        view.EvaluatedSlotGroup();

                        MessageData newData = new MessageData();
                        newData.Parameters.Add((byte)CommonParameterCode.TOTAL_TIME, 2f);

                        dispatcher.Dispatch(ECommonViewEvent.FADE_CAMERA_IN_OUT, newData);

                        break;
                    }

                case ServerCmdCode.SLOT_GROUP_EVALUATION_ENABLED:
                    {
                        view.SlotGroupEvaluationEnabled();
                        break;
                    }

                case ServerCmdCode.SLOT_GROUP_EVALUATION_DISABLED:
                    {
                        view.SlotGroupEvaluationDisabled();
                        break;
                    }

                case ServerCmdCode.ITEM_STATUS_SYNCED:
                    {
                        CTItemData syncData = (CTItemData)(itemData[(byte)CommonParameterCode.DATA_CT_ITEM_DATA]);
                        view.SyncItemData(syncData);
                        break;
                    }

                case ServerCmdCode.SLOT_GROUP_STATUS_SYNC:
                    {
                        CTSlotGroupData slotData = (CTSlotGroupData)(itemData[(byte)CommonParameterCode.DATA_CT_SLOT_GROUP_DATA]);
                        view.SyncSlotGroup(slotData);
                        break;
                    }

            }
        }

        private void OnUIMessageReceived(MessageData data)
        {
            UIActionCode code = (UIActionCode)data[(byte)CommonParameterCode.UI_ACTION];

            switch(code)
            {
                case UIActionCode.NAVIGATE_NEXT:
                    {
                        view.NavigateNext();
                        break;
                    }

                case UIActionCode.NAVIGATE_PREV:
                    {
                        view.NavigatePrevious();
                        break;
                    }

                case UIActionCode.EVALUATE:
                    {
                        Invoke("DelayedEvaluate", 3f);

                        MessageData fadeData = new MessageData();
                        fadeData.Parameters.Add((byte)CommonParameterCode.TOTAL_TIME, 9f);

                        dispatcher.Dispatch(ECommonViewEvent.SLOT_GROUP_EVALUATE, fadeData);

                        break;
                    }

                case UIActionCode.REPLAY_ANIM:
                    {
                        MessageData messageData = new MessageData();
                        messageData.Parameters.Add((byte)CommonParameterCode.ITEMID, view.itemID);

                        dispatcher.Dispatch(IntCmdCodeSlotGroup.REPLAY_SLOT_GROUP_ANIMATIONS, messageData);
                        break;
                    }
            }
        }

        public void OnActivateSubgroup(IEvent evt)
        {
            dispatcher.Dispatch(OpCmdCodeSlotGroup.ACTIVATE_SUB_SLOT_GROUP, evt.data);
        }

        public void OnUpdateSubSlotGroupName(IEvent evt)
        {
            dispatcher.Dispatch(IntCmdCodeSlotGroup.UPDATE_SUB_GROUP_NAME, evt.data);
        }

        private void OnGroupEvaluated(IEvent evt)
        {
            dispatcher.Dispatch(IntCmdCodeSlotGroup.TELEPORT_AVATAR, evt.data);
        }

        private void DelayedEvaluate()
        {
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte)CommonParameterCode.ITEMID, view.itemID);

            dispatcher.Dispatch(SubCode.EvaluateSlotGroup, msgData);
        }

        private void OnGroupEvaluationVisibility(IEvent evt)
        {
            dispatcher.Dispatch(IntCmdCodeSlotGroup.SLOT_GROUP_EVALUATION_UI_VISIBILITY, evt.data);
        }

        private void OnReplayAnimationEnded(IEvent evt)
        {
            dispatcher.Dispatch(IntCmdCodeSlotGroup.SLOT_GROUP_REPLAY_ANIMATIONS_ENDED, evt.data);
        }
    }
}
