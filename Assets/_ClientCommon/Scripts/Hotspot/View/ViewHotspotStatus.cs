﻿using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;
using PulseIQ.Local.Common.Model.Hotspot;
using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.Hotspot
{

    public class ViewHotspotStatus : EventView
    {
        [SerializeField]
        SpriteRenderer statusRenderer;
        [SerializeField]
        Collider statusCollider;
        Color originalColor;

        private bool isEnabled = false;
        Vector3 startPosition = new Vector3();
        Vector3 lookAtPos = new Vector3();
        Quaternion originalRot = new Quaternion();

        string statusID = "";
        string statusIconID = "";
        EHotspotStatusTypeCode statusType = new EHotspotStatusTypeCode();

        internal void init()
        {

        }

        public void SetHotspotStatusData(CTHotspotStatusData data, Vector3 lookAt, Vector3 startPos, Quaternion startRot)
        {
            lookAtPos = lookAt;
            transform.position = startPos;
            startPosition = startPos;
            transform.LookAt(lookAtPos);
            originalRot = startRot;

            statusID = data.StatusId;
            statusIconID = data.StatusIconId;
            statusType = data.StatusTypeCode;

            if(statusIconID == "Placeholder")
            {
                statusIconID = "99";
            }

            statusRenderer.sprite = Resources.Load<Sprite>("Images/HotspotStatusIcons/StatusIcon" + statusIconID);
            transform.Rotate(new Vector3(0, 180, 0));        

            originalColor = statusRenderer.material.color;
        }

        public string GetStatusID()
        {
            return statusID;
        }

        public void UpdateStatusPositionAndRotation(Quaternion rot)
        {
            transform.localPosition = Vector3.zero;
            transform.rotation = rot;
            transform.Rotate(new Vector3(0, 180, 0));
            //transform.LookAt(lookAtPos);
        }

        public void UpdateStatusPosition(Vector3 pos)
        {
            transform.localPosition = Vector3.zero;
        }


        public void ShowStatus()
        {
            isEnabled = true;

            if (Settings.applicationType == EApplicationType.CONTROL_CLIENT)
            {
                ControlClientShow();
            }
            else
            {
                PlayerClientShow();
            }
        }

        public void HideStatus()
        {
            if (Settings.applicationType == EApplicationType.CONTROL_CLIENT)
            {
                ControlClientHide();
            }
            else
            {
                PlayerClientHide();
            }
        }

        public void EnableStatus()
        {
            isEnabled = true;

            if (Settings.applicationType == EApplicationType.CONTROL_CLIENT)
            {
                ControlClientEnabled();
            }
            else
            {
                PlayerClientEnabled();
            }
        }

        public void DisableStatus()
        {
            if (Settings.applicationType == EApplicationType.CONTROL_CLIENT)
            {
                ControlClientDisabled();
            }
            else
            {
                PlayerClientDisabled();
            }
        }

        public void HotspotInactive()
        {
            if (Settings.applicationType == EApplicationType.CONTROL_CLIENT)
            {
                gameObject.SetActive(true);
                statusRenderer.material.color = new Color(originalColor.r, originalColor.g, originalColor.b, 0.2f);
                statusCollider.enabled = false;
            }
            else
            {
                PlayerClientHide();
            }
        }

        private void ControlClientHide()
        {
            gameObject.SetActive(false);
            statusCollider.enabled = false;
        }

        private void PlayerClientHide()
        {
            gameObject.SetActive(false);
            statusCollider.enabled = false;
        }

        private void ControlClientShow()
        {
            gameObject.SetActive(true);
            statusRenderer.material.color = originalColor;
            statusCollider.enabled = true;
        }

        private void PlayerClientShow()
        {
            statusCollider.enabled = true;
            statusRenderer.enabled = true;
            gameObject.SetActive(true);
        }

        private void ControlClientDisabled()
        {
            isEnabled = false;
            statusRenderer.material.color = new Color(1f, 0, 0, 0.2f);
            statusCollider.enabled = false;
        }

        private void PlayerClientDisabled()
        {
            isEnabled = false;
            statusCollider.enabled = false;
            gameObject.SetActive(false);
        }

        private void ControlClientEnabled()
        {
            isEnabled = true;
            statusRenderer.material.color = originalColor;
            statusCollider.enabled = true;
            gameObject.SetActive(true);
        }

        private void PlayerClientEnabled()
        {
            isEnabled = true;
            statusCollider.enabled = true;
            gameObject.SetActive(true);
        }

        public EHotspotStatusTypeCode GetStatusType()
        {
            return statusType;
        }
        
        public EHotspotStatusTypeCode StatusType
        {
            get
            {
                return statusType;
            }
        }
    }
}