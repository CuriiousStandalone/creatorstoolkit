﻿using System.IO;
using System.Collections;

using UnityEngine;

using TMPro;

using PulseIQ.Local.Common.Model.Hotspot.HotspotElement;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace PulseIQ.Local.ClientCommon.Hotspot
{
    public class ViewHotspotElement : MonoBehaviour
    {
        private EHotspotElementTypeCode elementType;

        public void SetHotspotElementData(CTHotspotElementBaseData hotspotElementData, float parentWidth, float parentHeight)
        {
            elementType = hotspotElementData.TypeCode;

            switch (elementType)
            {
                case EHotspotElementTypeCode.TEXT:
                    {
                        TextMeshPro textMesh = gameObject.GetComponent<TextMeshPro>();

                        TMP_FontAsset fontResource = Resources.Load("Font/Gilroy/" + ((CTHotspotElementTextData)hotspotElementData).FontName, typeof(TMP_FontAsset)) as TMP_FontAsset;

                        textMesh.font = fontResource;
                        textMesh.fontSize = 1;
                        textMesh.color = Color.black;
                        textMesh.text = ((CTHotspotElementTextData)hotspotElementData).Text;
                        textMesh.GetComponent<Renderer>().material = fontResource.material;

                        textMesh.transform.parent = transform.parent;

                        gameObject.GetComponent<RectTransform>().localScale = Vector3.one;
                        gameObject.GetComponent<RectTransform>().localPosition = new Vector3(-(parentWidth/2) + hotspotElementData.X/100, (parentHeight/2) - hotspotElementData.Y/100, 0);
                        gameObject.GetComponent<RectTransform>().localRotation = new Quaternion(0, 0, 0, 1);

                        gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(hotspotElementData.Width/100, hotspotElementData.Height/100);

                        break;
                    }

                case EHotspotElementTypeCode.IMAGE:
                    {
                        SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();

                        string imagePath = Path.Combine(Settings.ImagePath, ((CTHotspotElementImageData)hotspotElementData).MultimediaId);

                        spriteRenderer.sprite = LoadImage(imagePath, ((CTHotspotElementImageData)hotspotElementData).Width / 100, ((CTHotspotElementImageData)hotspotElementData).Height / 100);

                        spriteRenderer.size = new Vector2(hotspotElementData.Width / 100, hotspotElementData.Height / 100);

                        transform.localScale = Vector3.one;
                        transform.localPosition = new Vector3((-parentWidth / 2) + (hotspotElementData.X/100), (-((hotspotElementData.Height / 100) - (parentHeight / 2))) - (hotspotElementData.Y/100), 0);
                        transform.localRotation = new Quaternion(0, 0, 0, 1);

                        break;
                    }

                case EHotspotElementTypeCode.AUDIO:
                    {
                        string filePath = Path.Combine(Settings.AudioFilePath, ((CTHotspotElementAudioData)hotspotElementData).MultimediaId);

                        StartCoroutine(LoadAudio(filePath));

                        break;
                    }
            }
        }

        IEnumerator LoadAudio(string url)
        {
            AudioSource audioSource = gameObject.GetComponent<AudioSource>();

            WWW www = new WWW(url);

            Debug.Log("Doing something 1");

            yield return new WaitForSeconds(0.1f);

            while (!www.isDone)
            {
                Debug.Log("Doing something 2");
                yield return null;
            }

            Debug.Log("Doing something 3");

            yield return new WaitForSeconds(0.1f);

            audioSource.clip = www.GetAudioClip(false, false, AudioType.WAV);
        }

        private Sprite LoadImage(string imagePath, float canvasWidth, float canvasHeight)
        {
            if (imagePath != "" && File.Exists(imagePath))
            {
                Texture2D tex = null;
                byte[] fileData;

                fileData = File.ReadAllBytes(imagePath);
                tex = new Texture2D(2, 2);
                tex.LoadImage(fileData);

                int originalWidth = tex.width;
                int originalHeight = tex.height;

                float ratioX = (float)canvasWidth / (float)originalWidth;
                float ratioY = (float)canvasHeight / (float)originalHeight;

                float ratio = ratioX < ratioY ? ratioX : ratioY;

                int newHeight = System.Convert.ToInt32(originalHeight * ratio);
                int newWidth = System.Convert.ToInt32(originalWidth * ratio);

                return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2());
            }

            return null;
        }
    }
}
