﻿using UnityEngine;

using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.Common.Model.Hotspot.HotspotElement;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace PulseIQ.Local.ClientCommon.Hotspot
{
    public class ViewHotspotBox : MonoBehaviour
    {
        [SerializeField]
        SpriteRenderer backgroundBoxRenderer;

        private Vector3 lookAtPos = new Vector3();

        public void SetHotspotBoxData(CTHotspotBoxData data, Vector3 lookAt, Vector3 startPos)
        {
            backgroundBoxRenderer.size = new Vector2(data.Width/100, data.Height/100);

            Debug.Log("22222222222222222222222         " + data.Elements.Count);

            foreach (var item in data.Elements)
            {
                switch (item.TypeCode)
                {
                    case EHotspotElementTypeCode.TEXT:
                        {
                            GameObject textElement = Instantiate((Resources.Load(PrefabPathSettings.hotspotTextElement) as GameObject), transform);
        
                            ViewHotspotElement element = textElement.GetComponent<ViewHotspotElement>();
                            element.SetHotspotElementData(item, data.Width/100, data.Height/100);
                            break;
                        }
        
                    case EHotspotElementTypeCode.IMAGE:
                        {
                            GameObject imageElement = Instantiate((Resources.Load(PrefabPathSettings.hotspotImageElement) as GameObject), transform);
        
                            ViewHotspotElement element = imageElement.GetComponent<ViewHotspotElement>();
                            element.SetHotspotElementData(item, data.Width / 100, data.Height / 100);
                            break;
                        }
        
                    case EHotspotElementTypeCode.AUDIO:
                        {
                            GameObject audioElement = Instantiate(Resources.Load(PrefabPathSettings.hotspotAudioElement) as GameObject, transform);
        
                            ViewHotspotElement element = audioElement.GetComponent<ViewHotspotElement>();
                            element.SetHotspotElementData(item, data.Width / 100, data.Height / 100);
                            break;
                        }
        
                    default:
                        {
                            GameObject go = new GameObject("hotspot element");
                            break;
                        }
                }
            }
            lookAtPos = lookAt;
        
            transform.position = startPos + new Vector3(data.X, data.Y);
        
            transform.LookAt(lookAtPos);
        
            transform.Rotate(new Vector3(0, 180, 0));
        }

        //public void SetHotspotBoxData(CTHotspotBoxData data, Vector3 lookAt, Vector3 startPos)
        //{
        //    foreach (var item in data.Elements)
        //    {
        //        switch (item.TypeCode)
        //        {
        //            case EHotspotElementTypeCode.TEXT:
        //                {
        //                    GameObject textElement = Instantiate((Resources.Load(PrefabPathSettings.hotspotTextElement) as GameObject), transform);
        //
        //                    ViewHotspotElement element = textElement.GetComponent<ViewHotspotElement>();
        //                    element.SetHotspotElementData(item, data.Width / 100, data.Height / 100);
        //                    break;
        //                }
        //
        //            case EHotspotElementTypeCode.IMAGE:
        //                {
        //                    GameObject imageElement = Instantiate((Resources.Load(PrefabPathSettings.hotspotImageElement) as GameObject), transform);
        //
        //                    ViewHotspotElement element = imageElement.GetComponent<ViewHotspotElement>();
        //                    element.SetHotspotElementData(item, data.Width / 100, data.Height / 100);
        //                    break;
        //                }
        //
        //            case EHotspotElementTypeCode.AUDIO:
        //                {
        //                    GameObject audioElement = Instantiate(Resources.Load(PrefabPathSettings.hotspotAudioElement) as GameObject, transform);
        //
        //                    ViewHotspotElement element = audioElement.GetComponent<ViewHotspotElement>();
        //                    element.SetHotspotElementData(item, data.Width / 100, data.Height / 100);
        //                    break;
        //                }
        //
        //            default:
        //                {
        //                    GameObject go = new GameObject("hotspot element");
        //                    break;
        //                }
        //        }
        //    }
        //    lookAtPos = lookAt;
        //
        //    transform.position = startPos + new Vector3(data.X, data.Y);
        //
        //    transform.LookAt(lookAtPos);
        //
        //    transform.Rotate(new Vector3(0, 180, 0));
        //}
    }
}
