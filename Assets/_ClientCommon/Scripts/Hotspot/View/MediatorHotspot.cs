﻿using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

namespace PulseIQ.Local.ClientCommon.Hotspot
{
    public class MediatorHotspot : EventMediator
    {
        [Inject]
        public ViewHotspot view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewHotspot.TRIGGERED, OnHotspotTriggered);
            view.dispatcher.UpdateListener(value, ViewHotspot.ENABLE_STATUS, OnHotspotHoverStatus);
            view.dispatcher.UpdateListener(value, ViewHotspot.SHOW_TELEPORT_MEDIA, OnShowTeleportMedia);
            dispatcher.UpdateListener(value, IntCmdCodeHotspot.GET_HOTSPOT_STATE, GetHotspotActiveState);
        }

        private void GetHotspotActiveState(IEvent payload)
        {
            MessageData msgData = (MessageData)payload.data;

            string HotspotID = (string)msgData.Parameters[(byte)CommonParameterCode.HOTSPOT_ID];
            if (HotspotID.Equals(view.ReturnHotspotID()))
            {
                MessageData outData = new MessageData();
                outData.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, msgData[(byte)CommonParameterCode.ITEMID]);
                outData.Parameters.Add((byte)CommonParameterCode.HOTSPOT_ID, HotspotID);
                outData.Parameters.Add((byte)CommonParameterCode.IS_ACTIVE, view.IsEnabled);
                dispatcher.Dispatch(ECommonViewEvent.HOTSPOT_STATUS, outData);
            }
        }

        private void OnHotspotTriggered(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.TriggerHotspot, evt.data);
        }

        private void OnHotspotHoverStatus(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.EnableHotspotStatus, evt.data);
        }

        private void OnShowTeleportMedia(IEvent evt)
        {
            dispatcher.Dispatch(IntCmdCodeHotspot.SHOW_TELEPORT_MEDIA, evt.data);
        }
    }
}