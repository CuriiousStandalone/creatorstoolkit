﻿using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;
using PulseIQ.Local.Common.Model.Hotspot;
using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.Hotspot
{
    public class ViewHotspot : EventView
    {
        internal const string TRIGGERED = "TRIGGERED";
        internal const string ENABLE_STATUS = "HIGHLIGHT";

        internal const string SHOW_TELEPORT_MEDIA = "SHOW_TELEPORT_MEDIA";

        ItemTypeCode mediaItemType;
        private string id = "";
        private bool isEnabled = false;
        private string mediaID = "";
        private float activateTime = -1;
        private float deactivateTime = -1;
        private bool isActivated = false;
        private int teleportID = -1;

        private string name;
        float votePercentage = 100;
        [SerializeField]
        EHotspotTriggerCode triggerCode = EHotspotTriggerCode.SELECT;
        [SerializeField]
        EHotspotTriggerEventCode triggerEventCode = EHotspotTriggerEventCode.ENABLE_TRIGGER_STATE;
        float currentVideoTime = 0f;

        private Quaternion originalRotation = new Quaternion();
        private Vector3 lookAtPosition = new Vector3();
        private Vector3 localPostition = new Vector3();

        List<GameObject> hotspotStatusList = new List<GameObject>();
        List<GameObject> hotspotBoxList = new List<GameObject>();

        private bool _isHotspotBoxVisible = false;

        internal void init()
        {          

        }

        public void SetHotspotData(Vector3 lookAtPos, string mediaItemId, CTHotspotData itemData, float drawDistance, ItemTypeCode type)
        {
            mediaItemType = type;
            name = itemData.HotspotName;
            id = itemData.HotspotId;
            activateTime = itemData.ActiveTimeInMillisec;
            deactivateTime = itemData.DeactiveTimeInMillisec;
            mediaID = mediaItemId;
            teleportID = itemData.TeleportMultimediaItemIndex;
            votePercentage = itemData.VotePercentage;
            triggerCode = itemData.HotspotTriggerCode;
            triggerEventCode = itemData.HotspotTriggerEventCode;
            lookAtPosition = lookAtPos;

            originalRotation = Quaternion.Euler(itemData.HotspotLocalPostionX, itemData.HotspotLocalPostionY, transform.eulerAngles.z);
            localPostition = originalRotation * (Vector3.forward * drawDistance);
            transform.position = lookAtPos + localPostition;

            transform.LookAt(lookAtPos);
            isEnabled = true;

            foreach (CTHotspotStatusData statusData in itemData.ListHotspotStatus)
            {
                GameObject statusObj = Instantiate((Resources.Load(PrefabPathSettings.hotspotStatus) as GameObject), this.transform);
                ViewHotspotStatus hotspotScript = statusObj.GetComponent<ViewHotspotStatus>();

                hotspotScript.SetHotspotStatusData(statusData, lookAtPos, transform.position, originalRotation);
                hotspotStatusList.Add(statusObj);
            }

            foreach (CTHotspotBoxData boxData in itemData.HotspotBoxes)
            {   
                GameObject boxObject = Instantiate(Resources.Load(PrefabPathSettings.hotspotBox) as GameObject, this.transform);
                ViewHotspotBox hotspotBoxScript = boxObject.GetComponent<ViewHotspotBox>();

                hotspotBoxScript.SetHotspotBoxData(boxData, lookAtPos, transform.position);
                boxObject.SetActive(false);
                hotspotBoxList.Add(boxObject);
            }

            if (!itemData.IsEnabledByDefault)
            {
                DisableDefaultHotspot();

                isEnabled = false;
            }

            if ((activateTime <= 0 || deactivateTime <= 0) && isEnabled)
            {
                EnableDefaultHotspot();
                isActivated = true;
            }
        }

        public void VideoTimeReceived(float videoTime)
        {
            if (isEnabled)
            {
                currentVideoTime = videoTime;

                if(activateTime <= 0 && deactivateTime <= 0)
                {
                    return;
                }

                if (videoTime >= activateTime && videoTime <= deactivateTime && !isActivated)
                {
                    isActivated = true;

                    EnableDefaultHotspot();
                }
                else
                {
                    if (isActivated && (videoTime < activateTime || videoTime > deactivateTime))
                    {
                        isActivated = false;

                        HotspotInactive();
                    }
                }
            }
        }


        public string GetHotspotName
        {
            get
            {
                return name;
            }
        }

        public void OnHotspotTriggered()
        {
            if (isEnabled && isActivated)
            {
                MessageData data = new MessageData();

                data.Parameters.Add((byte)CommonParameterCode.HOTSPOT_TRIGGER_EVENT_CODE, TriggerEventCode);
                data.Parameters.Add((byte)CommonParameterCode.ITEMID, mediaID);
                data.Parameters.Add((byte)CommonParameterCode.HOTSPOT_ID, id);

                dispatcher.Dispatch(TRIGGERED, data);
            }
        }

        public void OnEnableHotspotStatus(EHotspotStatusTypeCode statusType)
        {
            if(null == hotspotStatusList || hotspotStatusList.Count < 1)
            {
                return;
            }

            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, mediaID);
            data.Parameters.Add((byte)CommonParameterCode.HOTSPOT_ID, id);

            ViewHotspotStatus status;

            string statusId = "";

            foreach (var item in hotspotStatusList)
            {
                status = item.GetComponent<ViewHotspotStatus>();

                if (status.StatusType == statusType)
                {
                    statusId = status.GetStatusID();
                    break;
                }
            }

            data.Parameters.Add((byte)CommonParameterCode.HOTSPOT_STATUS_ID, statusId);

            dispatcher.Dispatch(ENABLE_STATUS, data);
        }
       
        private void HideStatus()
        {
            foreach(GameObject obj in hotspotStatusList)
            {
                obj.GetComponent<ViewHotspotStatus>().HideStatus();
            }
        }

        public void PositionHotspot(Vector3 pos, Quaternion mediaRotation)
        {
            transform.position = pos + localPostition;
            transform.rotation = originalRotation * mediaRotation;
            transform.LookAt(lookAtPosition);

            foreach (GameObject statusData in hotspotStatusList)
            {
                statusData.GetComponent<ViewHotspotStatus>().UpdateStatusPositionAndRotation(mediaRotation);
            }
        }

        public void PositionHotspot(Vector3 pos)
        {
            transform.position = pos + localPostition;

            foreach (GameObject statusData in hotspotStatusList)
            {
                statusData.GetComponent<ViewHotspotStatus>().UpdateStatusPosition(pos);
            }
        }

        public string ReturnHotspotID()
        {
            return id;
        }

        public EHotspotTriggerCode TriggerCode
        {
            get
            {
                return triggerCode;
            }
        }

        public EHotspotTriggerEventCode TriggerEventCode
        {
            get
            {
                return triggerEventCode;
            }
        }

        public bool IsEnabled
        {
            get
            {
                return isEnabled;
            }
        }

        public bool IsActivated
        {
            get
            {
                return isActivated;
            }
        }

        public void EnableHotspot()
        {
            isEnabled = true;

            if(mediaItemType == ItemTypeCode.PANORAMA)
            {
                EnableDefaultHotspot();
                isActivated = true;
            }
            else
            {
                if ((activateTime <= 0 || deactivateTime <= 0) || (currentVideoTime >= activateTime && currentVideoTime < deactivateTime))
                {
                    isActivated = true;
                    EnableDefaultHotspot();
                }
                else
                {
                    HotspotInactive();
                }
            }
        }

        public void DisableHotspot()
        {
            isEnabled = false;
            isActivated = false;
            DisableDefaultHotspot();
            EnableDisableHotspotBoxes(false);
        }

        public void EnableHotspotStatus(string id)
        {
            if (isEnabled && IsActivated)
            {
                foreach (GameObject statusData in hotspotStatusList)
                {
                    if (id == statusData.GetComponent<ViewHotspotStatus>().GetStatusID())
                    {
                        statusData.GetComponent<ViewHotspotStatus>().UpdateStatusPositionAndRotation(transform.rotation);
                        statusData.GetComponent<ViewHotspotStatus>().ShowStatus();

                        if(statusData.GetComponent<ViewHotspotStatus>().StatusType == EHotspotStatusTypeCode.TRIGGER)
                        {
                            EnableDisableHotspotBoxes(true);
                        }

                        if (statusData.GetComponent<ViewHotspotStatus>().StatusType == EHotspotStatusTypeCode.DEFAULT)
                        {
                            EnableDisableHotspotBoxes(false);
                        }
                    }
                    else
                    {
                        statusData.GetComponent<ViewHotspotStatus>().HideStatus();
                    }
                }
            }
        }

        public void EnableDisableHotspotBoxes(bool toShow)
        {
            if (isEnabled && IsActivated)
            {
                foreach (GameObject hotspotBox in hotspotBoxList)
                {
                    _isHotspotBoxVisible = toShow;
                    hotspotBox.SetActive(toShow);
                }
            }
        }

        public void ShowTeleportMedia()
        {
            MessageData msgData = new MessageData();
            
            msgData.Parameters.Add((byte)CommonParameterCode.ITEM_INDEX, teleportID);

            dispatcher.Dispatch(SHOW_TELEPORT_MEDIA, msgData);
        }

        public void EnableDefaultHotspot()
        {
            foreach (GameObject obj in hotspotStatusList)
            {
                if (obj.GetComponent<ViewHotspotStatus>().GetStatusType() == EHotspotStatusTypeCode.DEFAULT)
                {
                    obj.GetComponent<ViewHotspotStatus>().EnableStatus();
                }
                else
                {
                    obj.GetComponent<ViewHotspotStatus>().HideStatus();
                }
            }
        }

        public void DisableDefaultHotspot()
        {
            foreach (GameObject obj in hotspotStatusList)
            {
                if (obj.GetComponent<ViewHotspotStatus>().GetStatusType() == EHotspotStatusTypeCode.DEFAULT)
                {
                    obj.GetComponent<ViewHotspotStatus>().DisableStatus();
                }
                else
                {
                    obj.GetComponent<ViewHotspotStatus>().HideStatus();
                }
            }
        }

        public void HotspotInactive()
        {
            foreach (GameObject obj in hotspotStatusList)
            {
                if (obj.GetComponent<ViewHotspotStatus>().GetStatusType() == EHotspotStatusTypeCode.DEFAULT)
                {
                    obj.GetComponent<ViewHotspotStatus>().HotspotInactive();
                }
                else
                {
                    obj.GetComponent<ViewHotspotStatus>().HideStatus();
                }
            }
        }

        public bool IsHotspotBoxVisible
        {
            get
            {
                return _isHotspotBoxVisible;
            }
        }
    }
}