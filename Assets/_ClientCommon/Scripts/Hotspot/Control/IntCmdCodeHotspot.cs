﻿namespace PulseIQ.Local.ClientCommon.Hotspot
{
    public enum IntCmdCodeHotspot : byte
    {
        HOTSPOT_DISABLED,
        HOTSPOT_ENABLED,
        HOTSPOT_STATUS_ENABLED,
        HOTSPOT_TELEPORTED,
        SHOW_TELEPORT_MEDIA,
        GET_HOTSPOT_STATE,
    }
}