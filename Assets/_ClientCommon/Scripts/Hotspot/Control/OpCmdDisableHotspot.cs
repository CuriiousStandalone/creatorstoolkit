﻿using strange.extensions.command.impl;

using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace PulseIQ.Local.ClientCommon.Hotspot
{
    public class OpCmdDisableHotspot : EventCommand
    {
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            string itemID = (string)data.Parameters[(byte)CommonParameterCode.ITEMID];
            string hotspotID = (string)data.Parameters[(byte)CommonParameterCode.HOTSPOT_ID];

            OperationsMultimedia.DisableHotspot(ClientBridge.Instance, ClientBridge.Instance.CurUserID, itemID, hotspotID);
        }
    }
}