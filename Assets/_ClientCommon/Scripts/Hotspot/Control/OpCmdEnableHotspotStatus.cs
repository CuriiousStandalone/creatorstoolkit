﻿using strange.extensions.command.impl;

using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;
using PulseIQ.Local.Common.Model.Hotspot;

namespace PulseIQ.Local.ClientCommon.Hotspot
{
    public class OpCmdEnableHotspotStatus : EventCommand
    {
        public override void Execute()
        {
            MessageData msgData = (MessageData)evt.data;

            string itemId = (string)msgData[(byte)CommonParameterCode.ITEMID];

            string hotspotId = (string)msgData[(byte)CommonParameterCode.HOTSPOT_ID];

            string statusId = (string)msgData[(byte)CommonParameterCode.HOTSPOT_STATUS_ID];

            OperationsMultimedia.EnableHotspotStatus(ClientBridge.Instance, ClientBridge.Instance.CurUserID, itemId, hotspotId, statusId);
        }
    }
}
