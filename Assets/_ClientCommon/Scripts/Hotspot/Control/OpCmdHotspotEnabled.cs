﻿using strange.extensions.command.impl;

using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace PulseIQ.Local.ClientCommon.Hotspot
{
    public class OpCmdHotspotEnabled : EventCommand
    {
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;
            data.Parameters.Add((byte)CommonParameterCode.OPERATION_CODE, evt.type);

            dispatcher.Dispatch(IntCmdCodeHotspot.HOTSPOT_ENABLED, data);
        }
    }
}