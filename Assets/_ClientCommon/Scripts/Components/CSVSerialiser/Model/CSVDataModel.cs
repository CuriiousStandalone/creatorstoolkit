﻿using System;
using System.Collections.Generic;

namespace PulseIQ.Local.ClientCommon.Components.CSVSerialiser
{
    public class CSVDataModel
    {
        private string reportTitle = "";

        private int dataCount = 0;

        private DateTime date;

        private List<string> columnList = new List<string>();

        private List<List<string>> dataList = new List<List<string>>();

        public void AddReportTitle(string reportTitle)
        {
            this.reportTitle = reportTitle;
        }

        public void AddReportDate(DateTime date)
        {
            this.date = date;
        }

        public void AddColumnName(string columnName)
        {
            columnList.Add(columnName);
        }

        public void AddDataRow(List<string> dataRow)
        {
            if (dataRow.Count > 0)
            {
                dataList.Add(dataRow);
                dataCount++;
            }
        }

        public List<string> GetColumnData(string column)
        {
            int index = columnList.IndexOf(column);

            List<string> resultList = new List<string>();

            foreach(var item in dataList)
            {
                resultList.Add(item[index]);
            }

            return resultList;
        }

        public List<string> GetColumnList()
        {
            return columnList;
        }

        public List<List<string>> GetDataList()
        {
            return dataList;
        }

        public string ReportTitle
        {
            get
            {
                return reportTitle;
            }
        }

        public int DataCount
        {
            get
            {
                return dataCount;
            }
        }

        public DateTime Date
        {
            get
            {
                return date;
            }
        }
    }
}
