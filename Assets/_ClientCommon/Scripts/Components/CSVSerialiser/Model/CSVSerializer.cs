﻿using System;
using System.IO;
using System.Text;
using System.Globalization;
using System.Collections.Generic;

using Mono.Csv;

using PulseIQ.Local.ClientCommon.Components.Common;

namespace PulseIQ.Local.ClientCommon.Components.CSVSerialiser
{
    public class CSVSerializer
    {
        public static void WriteCSVFile(CSVDataModel data)
        {
            List<List<string>> dataGrid = new List<List<string>>();

            dataGrid.Add(data.GetColumnList());

            List<List<string>> templist = data.GetDataList();

            foreach(var item in templist)
            {
                dataGrid.Add(item);
            }

            DateTime newDate = DateTime.Now;

            CsvFileWriter.WriteAll(dataGrid, Path.Combine(Settings.RootPath, data.ReportTitle + "_" + newDate.ToString("yyyyMMdd") + "_" + data.DataCount + ".csv"), Encoding.GetEncoding("utf-8"));
        }

        public static void ReadCSVFile(string path, out CSVDataModel reportData)
        {
            List<string> row = new List<string>();

            CSVDataModel data = new CSVDataModel();

            bool isFirstRow = true;

            using (var reader = new CsvFileReader(path))
            {
                while (reader.ReadRow(row))
                {
                    List<string> rowData = new List<string>();

                    foreach (string cell in row)
                    {
                        if(isFirstRow)
                        {
                            data.AddColumnName(cell);
                        }
                        else
                        {
                            rowData.Add(cell);
                        }
                    }

                    data.AddDataRow(rowData);

                    isFirstRow = false;
                }
            }

            string fileName = Path.GetFileNameWithoutExtension(path);

            string[] delimatedFileName = fileName.Split('_');

            data.AddReportTitle(delimatedFileName[0]);

            DateTime newDate;
            DateTime.TryParseExact(delimatedFileName[1], "yyyyMMdd", null, DateTimeStyles.None, out newDate);
            data.AddReportDate(newDate);

            reportData = data;
        }
    }
}
