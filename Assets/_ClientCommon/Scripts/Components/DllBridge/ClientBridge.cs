﻿using System.Collections.Generic;
using UnityEngine;
using strange.extensions.mediation.impl;

using ExitGames.Client.Photon;

using PulseIQ.Local.Common;
using PulseIQ.Local.Common.CustomType;
using PulseIQ.Local.Client.Unity3DLib;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Client.Unity3DLib.Model;
using PulseIQ.Local.Common.Model.InteractiveItem;
using PulseIQ.Local.Common.Model.Slot;
using PulseIQ.Local.Common.Model.UI;
using PulseIQ.Local.Common.Model.SlotGroup;
using PulseIQ.Local.Common.Model.Report;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.Model.Panorama;
using PulseIQ.Local.Common.Model.PanoramaVideo;
using PulseIQ.Local.Common.Model.LineNotation;
using PulseIQ.Local.Common.Model.SyncTimer;
using PulseIQ.Local.Common.Model.TutorialItem;
using PulseIQ.Local.Common.Model.Avatar;
using PulseIQ.Local.Common.Model.PlanarVideo;
using PulseIQ.Local.Common.Model.Audio;
using PulseIQ.Local.Common.Model.Image;

namespace PulseIQ.Local.ClientCommon.Components.DllBridge
{	
	public class ClientBridge : EventMediator, IClientListener 
	{
		[Inject]
		public ClientBridgeInit view{ get; set;}

		public static ClientInstance Instance { get; private set; }

        public override void OnRegister()
		{

        }

        void OnApplicationQuit()
        {
            Instance.Disconnect();
        }

        // Use this for initialization
        void Start () 
		{
			//GlobalSettings.ReadData (Settings.ConfigFilePath);

			ClientSettings settings = new ClientSettings ();
			settings.ApplicationName = "PulseIQ";
			settings.SendReliable = false;
            settings.ServerAddress = GlobalSettings.ServerIP + ":5055";

            Instance = new ClientInstance (this, settings);
			Instance.Initialize ();
			Instance.Connect ();

            Application.runInBackground = true;
        }

		// Update is called once per frame
		void Update () 
		{
			Instance.Update ();
		}

		public bool IsDebugLogEnabled { get { return true; } }

		public void LogDebug(object message)
		{
            //Debug.Log("!!!" + message);
        }

        public void LogError(object message)
		{

		}

		public void LogInfo(object message)
		{
		}

		public void OnConnect(EventCode evtCode)
		{
			Debug.Log ("Control Client Bridge : OnConnect!!!");
			dispatcher.Dispatch (evtCode, "Connecting");
		}

		public void OnDisconnect(EventCode evtCode, StatusCode returnCode)
		{
			Debug.Log ("Control Client Bridge : OnDisconnect!!!");
			dispatcher.Dispatch (evtCode);

		}

        public void OnClientReconnected(EventCode evtCode, string worldId)
        {
            MessageData messageData = new MessageData();

            messageData.Parameters.Add((byte)CommonParameterCode.WORLDID, worldId);

            dispatcher.Dispatch(evtCode, messageData);
        }

        public void OnLoginFailed (EventCode evtCode, DataLogin loginData)
		{
            Debug.Log("---- Debugging ---- : Login Failed!");
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.DATA_LOGIN, loginData);

            dispatcher.Dispatch(evtCode, data);
        }

		public void OnLoginSucceeded (EventCode evtCode, DataLogin loginData)
		{
            Debug.Log("---- Debugging ---- : Login Success!");
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.DATA_LOGIN, loginData);

            dispatcher.Dispatch(evtCode, data);
        }

		public void OnWorldCreated(EventCode evtCode, DataWorldData worldData)
		{
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.WORLDID, worldData.Id);

            Debug.Log("---- Debugging ---- : World Created!");

            dispatcher.Dispatch(evtCode, data);
        }

		public void OnWorldEntered(EventCode evtCode, DataWorldData worldData)
		{
            Debug.Log("---- Debugging ---- : World Entered!");

            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.DATA_WORLD_DATA, worldData);

            dispatcher.Dispatch(evtCode, data);
        }

		public void OnAvatarCreated (EventCode evtCode, DataAvatar newAvatar)
		{
            if (ClientInstance.Instance.CurWorldData.Id != "")
            {
                MessageData data = new MessageData();
                data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, newAvatar.ItemType);
                data.Parameters.Add((byte)CommonParameterCode.DATA_AVATAR, newAvatar);

                dispatcher.Dispatch(evtCode, data);
            }
        }

        public void OnAvatarSelected(EventCode evtCode, string userId, string avatarName, string avatarId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.USERID, userId);
            data.Parameters.Add((byte)CommonParameterCode.AVATARID, avatarId);
            data.Parameters.Add((byte)CommonParameterCode.AVATAR_NAME, avatarName);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnAvatarHeadRotated(EventCode evtCode, ItemRotation newHeadRotation)
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, newHeadRotation.ItemId);
            data.Parameters.Add((byte)CommonParameterCode.DATA_AVATAR_HEAD_ROTATION, newHeadRotation.Rotation);
            dispatcher.Dispatch(evtCode, data);
        }

        //      public void OnAvatarHeadMoved (EventCode evtCode, DataAvatarRotation newAvatarHeadPose)
        //{
        //          MessageData data = new MessageData();

        //          data.Parameters.Add((byte)CommonParameterCode.ITEMID, newAvatarHeadPose.ItemId);
        //          data.Parameters.Add((byte)CommonParameterCode.DATA_AVATAR_PART_POSE, newAvatarHeadPose);

        //          dispatcher.Dispatch(evtCode, data);
        //      }

        public void OnAvatarAnimationPlayed(EventCode evtCode, string avatarItemId, string avatarAniName)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, avatarItemId);
            data.Parameters.Add((byte)CommonParameterCode.ANIMATION_NAME, avatarAniName);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnAvatarLeftHandMoved(EventCode evtCode, ItemPosition position)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEM_POSITION, position);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnAvatarLeftHandRotated(EventCode evtCode, ItemRotation rotation)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEM_ROTATION, rotation);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnAvatarRightHandMoved(EventCode evtCode, ItemPosition position)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEM_POSITION, position);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnAvatarRightHandRotated(EventCode evtCode, ItemRotation rotation)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEM_ROTATION, rotation);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnPackageDataReturn(EventCode evtCode, DataPackageData newPackageData)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.DATA_PACKAGE_DATA, newPackageData);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnGetControlClientUsers(EventCode evtCode, string[] userNames, string[] passwords)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.USERNAME, userNames);
            data.Parameters.Add((byte)CommonParameterCode.PASSWORD, passwords);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnGetOnlineUserCount(EventCode evtCode, int userCount)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.USERCOUNT, userCount);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnPackageDataListReturn(EventCode evtCode, List<DataPackageDataListItem> packageDataList)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.DATA_PACKAGE_LIST_DATA, packageDataList);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnWorldExit(EventCode evtCode, string worldId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.WORLDID, worldId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnClassInstanceListReturn(EventCode evtCode, List<DataClassInstance> classInstances)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.DATA_CLASS_INSTANCE_LIST, classInstances);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnClassMemberActivated(EventCode evtCode, string userId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.USERID, userId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnClassMemberDeactivated(EventCode evtCode, string userId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.USERID, userId);
            dispatcher.Dispatch(evtCode, data);
        }

        public void OnSetActiveClassInstance(EventCode evtCode, string classInstanceId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.CLASS_INSTANCE_ID, classInstanceId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnTeleported(EventCode evtCode, string userId, string classInstanceId, string teamId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.USERID, userId);
            data.Parameters.Add((byte)CommonParameterCode.CLASS_INSTANCE_ID, classInstanceId);
            data.Parameters.Add((byte)CommonParameterCode.TEAMID, teamId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnClassInstanceUpdated(EventCode evtCode, string userId, string classInstanceId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.USERID, userId);
            data.Parameters.Add((byte)CommonParameterCode.CLASS_INSTANCE_ID, classInstanceId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnClassInstanceCreated(EventCode evtCode, string userId, string classInstanceId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.USERID, userId);
            data.Parameters.Add((byte)CommonParameterCode.CLASS_INSTANCE_ID, classInstanceId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnAllMembersReturned(EventCode eventCode, List<DataClientData> memberList)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.MEMBER_LIST, memberList);

            dispatcher.Dispatch(eventCode, data);
        }

        public void OnTeleportClassToWorld(EventCode evtCode, string worldId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.WORLDID, worldId);

            dispatcher.Dispatch(evtCode, data);
        }


        public void OnClassLoadingFinished(EventCode evtCode)
        {
            MessageData data = new MessageData();

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnClassLoadingStatusUpdated(EventCode evtCode, string classLoadingStatus)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.CLASS_LOADING_STATUS, classLoadingStatus);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnGetCurWorld(EventCode evtCode, string worldId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.WORLDID, worldId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnGetWaitingWorld(EventCode evtCode, string worldId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.WORLDID, worldId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnReportDataReturned(EventCode evtCode, ReportData reportData)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.DATA_REPORT_DATA, reportData); 

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnItemStatusSynced(EventCode evtCode, string itemId, CTItemData itemData)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.DATA_CT_ITEM_DATA, itemData);

            dispatcher.Dispatch(evtCode, data);
        }

		public void OnSlotGroupStatusSynced(EventCode evtCode, string itemId, CTSlotGroupData slotGroupData, string finishAniItemID)
		{
			MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.DATA_CT_SLOT_GROUP_DATA, slotGroupData);

            dispatcher.Dispatch(evtCode, data);

            // Temporary fix to get data sent to the correspdonding finish anim item. Will need to be looked at again 
            MessageData data2 = new MessageData();

            data2.Parameters.Add((byte)CommonParameterCode.ITEMID, finishAniItemID);
            data2.Parameters.Add((byte)CommonParameterCode.DATA_CT_SLOT_GROUP_DATA, slotGroupData);

            dispatcher.Dispatch(evtCode, data2);
        }

        public void OnCloseAllMultimedia(EventCode evtCode)
        {
            dispatcher.Dispatch(evtCode);
        }

        #region UI

        public void OnUICreated(EventCode evtCode, DataItemData itemData, EUIType uiType, List<int> listResponseItemIndex)
        {
            MessageData messageData = new MessageData();

            messageData.Parameters.Add((byte)CommonParameterCode.DATA_ITEM_DATA, itemData);
            messageData.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, itemData.ItemType);
            messageData.Parameters.Add((byte)CommonParameterCode.UI_TYPE, uiType);
            messageData.Parameters.Add((byte)CommonParameterCode.ITEM_INDICES, listResponseItemIndex);

            dispatcher.Dispatch(evtCode, messageData);
        }

        #endregion

        #region Item Base
        public void OnItemCreated(EventCode evtCode, DataItemData newItemData)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, newItemData.ItemType);
            data.Parameters.Add((byte)CommonParameterCode.DATA_ITEM_DATA, newItemData);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnItemDestroyed(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        //public void OnItemMoved(EventCode evtCode, DataItemPose newPose)
        //{
        //    //Debug.Log ("Control Client Bridge : OnItemMoved!!! " + newPose.Position.X + " " + newPose.Position.Y + " " + newPose.Position.Z);
        //    MessageData data = new MessageData();
        //
        //    data.Parameters.Add((byte)CommonParameterCode.ITEMID, newPose.ItemId);
        //    data.Parameters.Add((byte)CommonParameterCode.DATA_ITEM_POSE, newPose);
        //    
        //    dispatcher.Dispatch(evtCode, data);
        //}

        public void OnItemMoved(EventCode evtCode, string itemId, CTVector3 position)
        {
            var messageData = new MessageData();
            messageData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            messageData.Parameters.Add((byte)CommonParameterCode.ITEM_POSITION, position);
            dispatcher.Dispatch(evtCode, messageData);
        }

        public void OnItemRotated(EventCode evtCode, string itemId, CTQuaternion rotation)
        {
            var messageData = new MessageData();
            messageData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            messageData.Parameters.Add((byte)CommonParameterCode.ITEM_ROTATION, rotation);
            dispatcher.Dispatch(evtCode, messageData);
        }

        public void OnItemScaled(EventCode evtCode, string itemId, CTVector3 scale)
        {
            var messageData = new MessageData();
            messageData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            messageData.Parameters.Add((byte)CommonParameterCode.ITEM_SCALE, scale);
            dispatcher.Dispatch(evtCode, messageData);
        }

        public void OnItemOfflineMoved(EventCode evtCode, string itemId, float duration, CTVector3 destPoistion)
        {
            var messageData = new MessageData();
            messageData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            messageData.Parameters.Add((byte)CommonParameterCode.MOVE_TO_SPEED, duration);
            messageData.Parameters.Add((byte)CommonParameterCode.MOVE_TO_POSITION, destPoistion);
            dispatcher.Dispatch(evtCode, messageData);
        }

        public void OnItemOfflineRotated(EventCode evtCode, string itemId, float duration, CTQuaternion destRotation)
        {
            var messageData = new MessageData();
            messageData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            messageData.Parameters.Add((byte)CommonParameterCode.MOVE_TO_SPEED, duration);
            messageData.Parameters.Add((byte)CommonParameterCode.MOVE_TO_ROTATION, destRotation);
            dispatcher.Dispatch(evtCode, messageData);
        }

        public void OnItemOfflineScaled(EventCode evtCode, string itemId, float duration, CTVector3 destScale)
        {
            var messageData = new MessageData();
            messageData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            messageData.Parameters.Add((byte)CommonParameterCode.MOVE_TO_SPEED, duration);
            messageData.Parameters.Add((byte)CommonParameterCode.MOVE_TO_SCALE, destScale);
            dispatcher.Dispatch(evtCode, messageData);
        }

        public void OnItemLerpMoved(EventCode evtCode, string itemId, CTVector3 destPoistion)
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.MOVE_TO_POSITION, destPoistion);
            dispatcher.Dispatch(evtCode, data);
        }

        public void OnItemLerpRotated(EventCode evtCode, string itemId, CTQuaternion destRotation)
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.MOVE_TO_ROTATION, destRotation);
            dispatcher.Dispatch(evtCode, data);
        }

        public void OnItemLerpScaled(EventCode evtCode, string itemId, CTVector3 destScale)
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.MOVE_TO_SCALE, destScale);
            dispatcher.Dispatch(evtCode, data);
        }

        public void OnItemAbandoned(EventCode evtCode, string itemId, string userId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.USERID, userId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnItemObtainDenied(EventCode evtCode, string itemId, string userId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.USERID, userId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnItemObtained(EventCode evtCode, string itemId, string userId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.USERID, userId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnItemShown(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnItemHidden(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        #endregion


        #region PannoramaImage
        public void OnPanoramaCreated(EventCode evtCode, string itemId, string worldId, bool isOwner, CTPanoramaData newPanorama)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, newPanorama.ItemType);
            data.Parameters.Add((byte)CommonParameterCode.IS_OWNER, isOwner);
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.WORLDID, worldId);
            data.Parameters.Add((byte)CommonParameterCode.DATA_PANORAMA, newPanorama);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnPanoramaHidden(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnPanoramaShown(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnEndBehaviorTeleport(EventCode evtCode, string itemId, int targetMultimediaItemIndex)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.ITEM_INDEX, targetMultimediaItemIndex);

            dispatcher.Dispatch(evtCode, data);
        }

        #endregion

        #region PanoramaVideo
        public void OnPanoramaVideoCreated(EventCode evtCode, string itemId, string worldId, bool isOwner, CTPanoramaVideoData newPanoramaVideoData)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, newPanoramaVideoData.ItemType);
            data.Parameters.Add((byte)CommonParameterCode.IS_OWNER, isOwner);
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.WORLDID, worldId);
            data.Parameters.Add((byte)CommonParameterCode.DATA_PANORAMA_VIDEO, newPanoramaVideoData);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnPanoramaVideoHidden(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnPanoramaVideoShown(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnPanoramaVideoPlayed(EventCode evtCode, string itemId, int frameIndex)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.VIDEO_FRAME, frameIndex);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnPanoramaVideoPaused(EventCode evtCode, string itemId, int frameIndex)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.VIDEO_FRAME, frameIndex);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnPanoramaVideoReplayed(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnPanoramaVideoStopped(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnPanoramaVideoSought(EventCode evtCode, string itemId, int frameIndex)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.VIDEO_FRAME, frameIndex);

            dispatcher.Dispatch(evtCode, data);
        }

        #endregion

        #region StereoPanoramaVideo

        public void OnStereoPanoramaVideoCreated(EventCode evtCode, string itemId, string worldId, bool isOwner, CTStereoPanoramaVideoData newStereoPanoramaVideoData)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.WORLDID, worldId);
            data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, newStereoPanoramaVideoData.ItemType);
            data.Parameters.Add((byte)CommonParameterCode.STEREO_PANORAMA_VIDEO_DATA, newStereoPanoramaVideoData);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnStereoPanoramaVideoHidden(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnStereoPanoramaVideoShown(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnStereoPanoramaVideoPlayed(EventCode evtCode, string itemId, int frameIndex)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.VIDEO_FRAME, frameIndex);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnStereoPanoramaVideoPaused(EventCode evtCode, string itemId, int frameIndex)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.VIDEO_FRAME, frameIndex);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnStereoPanoramaVideoReplayed(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnStereoPanoramaVideoStopped(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnStereoPanoramaVideoSought(EventCode evtCode, string itemId, int frameIndex)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.VIDEO_FRAME, frameIndex);

            dispatcher.Dispatch(evtCode, data);
        }

        #endregion

        #region PlanarVideo
        public void OnPlanarVideoCreated(EventCode evtCode, DataPlanarVideo newPlanarVideo)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, newPlanarVideo.ItemType);
            data.Parameters.Add((byte)CommonParameterCode.DATA_PLANAR_VIDEO, newPlanarVideo);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnPlanarVideoHidden(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnPlanarVideoShown(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnPlanarVideoPlayed(EventCode evtCode, string itemId, int frameIndex)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.VIDEO_FRAME, frameIndex);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnPlanarVideoPaused(EventCode evtCode, string itemId, int frameIndex)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.VIDEO_FRAME, frameIndex);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnPlanarVideoReplayed(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnPlanarVideoStopped(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnPlanarVideoSought(EventCode evtCode, string itemId, int frameIndex)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.VIDEO_FRAME, frameIndex);

            dispatcher.Dispatch(evtCode, data);
        }

        #endregion

        #region Notation
        public void OnLineNotationCreated(EventCode evtCode, DataLineNotation newLineNotation)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, newLineNotation.ItemType);
            data.Parameters.Add((byte)CommonParameterCode.DATA_LINE_NOTATION, newLineNotation);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnShowLineNotation(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnHideLineNotation(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnLineNotationStarted(EventCode evtCode, string itemId, DataLineNotationCamera dataLineNotationCamera)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.DATA_LINE_NOTATION_CAMERA, dataLineNotationCamera);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnLineNotationStopped(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnLineNotationCleaned(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnLineRemoved(EventCode evtCode, string itemId, string LineId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.NOTATION_LINEID, LineId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnTouchPointsAdded(EventCode evtCode, DataTouchPointPackage touchPointPackage)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, touchPointPackage.ItemId);
            data.Parameters.Add((byte)CommonParameterCode.DATA_TOUCHPOINT_PACKAGE, touchPointPackage);

            dispatcher.Dispatch(evtCode, data);
        }

        #endregion

        #region Interactive Item

        public void OnInteractiveItemCreated(EventCode evtCode, DataItemData itemData, int[] InteractiveItemSlotIds, EAbandonAction abandonAction, EObtainAction obtainAction, int placedSlotItemIndex, bool isRepickable)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, itemData.ItemType);
            data.Parameters.Add((byte)CommonParameterCode.DATA_ITEM_DATA, itemData);
            data.Parameters.Add((byte)CommonParameterCode.INTERACTIVE_ITEM_SLOTS, InteractiveItemSlotIds);
            data.Parameters.Add((byte)CommonParameterCode.INTERACTIVE_ITEM_ABANDON_ACTION, abandonAction);
            data.Parameters.Add((byte)CommonParameterCode.INTERACTIVE_ITEM_OBTAIN_ACTION, obtainAction);
            data.Parameters.Add((byte)CommonParameterCode.ITEM_INDEX, placedSlotItemIndex);
            data.Parameters.Add((byte)CommonParameterCode.INTERACTIVE_ISREPICKABLE, isRepickable);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnInteractiveItemPlacedToSlot(EventCode evtCode, string itemId, string userId, string slotItemId, CTVector3 interactiveItemLocalPosition, CTQuaternion interactiveItemLocalRotation, CTVector3 InteractiveItemLocalScale)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.USERID, userId);
            data.Parameters.Add((byte)CommonParameterCode.SLOTID, slotItemId);
            data.Parameters.Add((byte)CommonParameterCode.INTERACTIVE_LOCAL_POSITION, interactiveItemLocalPosition);
            data.Parameters.Add((byte)CommonParameterCode.INTERACTIVE_LOCAL_ROTATION, interactiveItemLocalRotation);
            data.Parameters.Add((byte)CommonParameterCode.INTERACTIVE_LOCAL_SCALE, InteractiveItemLocalScale);
            
            dispatcher.Dispatch(evtCode, data);
        }

        public void OnInteractiveItemPlaceToSlotDenied(EventCode evtCode, string itemId, string userId, string SlotItemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.USERID, userId);
            data.Parameters.Add((byte)CommonParameterCode.SLOTID, SlotItemId);

            dispatcher.Dispatch(evtCode, data);
        }
        #endregion

        #region Slot Item

        public void OnSlotItemCreated(EventCode evtCode, DataItemData itemData, int slotId, bool isOpen, bool isIndicating, ESlotState curState, CTVector3 attachedLocalPosition, CTQuaternion attachedLocalRotation, CTVector3 attachedLocalScale, string correctStateAniName, string incorrectStateAniName, string emptyStateAniName)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, itemData.ItemType);
            data.Parameters.Add((byte)CommonParameterCode.DATA_ITEM_DATA, itemData);
            data.Parameters.Add((byte)CommonParameterCode.SLOTID, slotId);
            data.Parameters.Add((byte)CommonParameterCode.SLOT_LOCAL_POSITION, attachedLocalPosition);
            data.Parameters.Add((byte)CommonParameterCode.SLOT_LOCAL_ROTATION, attachedLocalRotation);
            data.Parameters.Add((byte)CommonParameterCode.SLOT_LOCAL_SCALE, attachedLocalScale);
            data.Parameters.Add((byte)CommonParameterCode.SLOT_IS_OPEN, isOpen);
            data.Parameters.Add((byte)CommonParameterCode.SLOT_IS_INDICATING, isIndicating);
            data.Parameters.Add((byte)CommonParameterCode.SLOT_EMPTY_ANIM_NAME, emptyStateAniName);
            data.Parameters.Add((byte)CommonParameterCode.SLOT_CORRECT_ANIM_NAME, correctStateAniName);
            data.Parameters.Add((byte)CommonParameterCode.SLOT_INCORRECT_ANIM_NAME, incorrectStateAniName);
            data.Parameters.Add((byte)CommonParameterCode.SLOT_STATE, curState);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnSlotOpened(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnSlotClosed(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnSlotIndicatorHidden(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnSlotIndicatorShown(EventCode evtCode, string itemId, ESlotState slotState)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.SLOT_STATE, slotState);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnSlotStateAniPlayed(EventCode evtCode, string itemId, string slotStateAniName, string slotStateExplainerSentence)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.ANIMATION_NAME, slotStateAniName);
            data.Parameters.Add((byte)CommonParameterCode.GENERAL_TEXT, slotStateExplainerSentence);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnSlotStateAniStopped(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnSlotGroupEvaluationEnabled(EventCode evtCode, string itemId)
        {
            MessageData messageData = new MessageData();

            messageData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, messageData);
        }

        public void OnSlotGroupEvaluationDisabled(EventCode evtCode, string itemId)
        {
            MessageData messageData = new MessageData();

            messageData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, messageData);
        }

        #endregion

        #region Slot Group

        public void OnSlotGroupCreated(EventCode evtCode, DataItemData itemData, string slotGroupName, CTVector3 slotGroupCamPosition, CTQuaternion slotGroupCamRotation, CTVector3 slotGroupCamScale, int slotGroupFinishAniItemIndex, int slotGroupControlUIItemIndex, List<string> subSlotGroups)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, itemData.ItemType);
            data.Parameters.Add((byte)CommonParameterCode.DATA_ITEM_DATA, itemData);
            data.Parameters.Add((byte)CommonParameterCode.SLOT_GROUP_NAME, slotGroupName);
            data.Parameters.Add((byte)CommonParameterCode.SLOT_GROUP_CAM_POS, slotGroupCamPosition);
            data.Parameters.Add((byte)CommonParameterCode.SLOT_GROUP_CAM_ROT, slotGroupCamRotation);
            data.Parameters.Add((byte)CommonParameterCode.SLOT_GROUP_CAM_SCL, slotGroupCamScale);
            data.Parameters.Add((byte)CommonParameterCode.SLOT_GROUP_CONTROL_UI_ITEM_INDEX, slotGroupControlUIItemIndex);
            data.Parameters.Add((byte)CommonParameterCode.SLOT_GROUP_FINISH_ANIMATION_ITEM_INDEX, slotGroupFinishAniItemIndex);
            data.Parameters.Add((byte)CommonParameterCode.SLOT_GROUP_SUB_GROUPS_LIST, subSlotGroups);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnSubSlotGroupActivated(EventCode evtCode, string itemId, string subSlotGroupId, string subSlotGroupName)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.SLOT_GROUP_SUB_GROUP_ID, subSlotGroupId);
            data.Parameters.Add((byte)CommonParameterCode.SLOT_GROUP_SUB_GROUP_NAME, subSlotGroupName);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnSlotGroupEvaluated(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnSlotGroupFinishAniPlayed(EventCode evtCode, string itemId, int finishAniItemIndex)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.SLOT_GROUP_FINISH_ANIMATION_ITEM_INDEX, finishAniItemIndex);
            dispatcher.Dispatch(evtCode, data);
        }

        #endregion

        #region Timer Item
        public void OnSyncTimerCreated(EventCode evtCode, DataItemData itemData, bool isCountDownTimer, float totalTime, float interval, float curTime)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, itemData.ItemType);
            data.Parameters.Add((byte)CommonParameterCode.DATA_ITEM_DATA, itemData);
            data.Parameters.Add((byte)CommonParameterCode.COUNTDOWN_TIMER, isCountDownTimer);
            data.Parameters.Add((byte)CommonParameterCode.TOTAL_TIME, totalTime);
            data.Parameters.Add((byte)CommonParameterCode.INTERVAL, interval);
            data.Parameters.Add((byte)CommonParameterCode.CURRENT_TIME, curTime);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnSyncTimerStarted(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnSyncTimerPaused(EventCode evtCode, string itemId, float curTime)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.CURRENT_TIME, curTime);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnSyncTimerReset(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnSyncTimerElapsed(EventCode evtCode, string itemId, float curTime)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.CURRENT_TIME, curTime);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnSyncTimerEnded(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        #endregion

        #region Tutorial Item

        public void OnTutorialItemCreated(EventCode evtCode, DataItemData itemData, List<string> animationNameList, CTVector3 observerLocalPosition, CTQuaternion observerLocalRotation)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, itemData.ItemType);
            data.Parameters.Add((byte)CommonParameterCode.DATA_ITEM_DATA, itemData);
            data.Parameters.Add((byte)CommonParameterCode.ANIMATION_LIST, animationNameList);
            data.Parameters.Add((byte)CommonParameterCode.OBSERVER_LOCAL_POSITION, observerLocalPosition);
            data.Parameters.Add((byte)CommonParameterCode.OBSERVER_LOCAL_ROTATION, observerLocalRotation);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnTutorialItemActivated(EventCode evtCode, string itemId, CTVector3 observerLocalPosition, CTQuaternion observerLocalRotation)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.OBSERVER_LOCAL_POSITION, observerLocalPosition);
            data.Parameters.Add((byte)CommonParameterCode.OBSERVER_LOCAL_ROTATION, observerLocalRotation);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnTutorialItemDeactivated(EventCode evtCode, string itemId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnTutorialItemAnimationStarted(EventCode evtCode, string itemId, string animationName)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.ANIMATION_NAME, animationName);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnTutorialItemAnimationPaused(EventCode evtCode, string itemId, string animationName, float animationFrame)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.ANIMATION_NAME, animationName);
            data.Parameters.Add((byte)CommonParameterCode.ANIMATION_FRAME, animationFrame);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnTutorialItemAnimationStopped(EventCode evtCode, string itemId, string animationName)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.ANIMATION_NAME, animationName);

            dispatcher.Dispatch(evtCode, data);
        }
        #endregion

        #region Post Processing
        public void OnPostProcessingActivated(EventCode evtCode, string postProcessingProfileId)
        {
            MessageData data = new MessageData();
            
            data.Parameters.Add((byte)CommonParameterCode.POST_PROCESSING_PROFILE_ID, postProcessingProfileId);
            
            dispatcher.Dispatch(evtCode, data);
        }

        public void OnGetWorldPostProcessingList(EventCode evtCode, string worldId, DataPostProcessing[] postProcessingList)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.WORLDID, worldId);
            data.Parameters.Add((byte)CommonParameterCode.POST_PROCESSING_LIST, postProcessingList);

            dispatcher.Dispatch(evtCode, data);
        }

        #endregion

        #region Skybox

        public void OnSkyboxActivated(EventCode evtCode, string skyboxId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.SKYBOXID, skyboxId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnGetWorldSkyboxList(EventCode evtCode, string worldId, DataSkybox[] skyboxList)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.WORLDID, worldId);
            data.Parameters.Add((byte)CommonParameterCode.SKYBOX_LIST, skyboxList);

            dispatcher.Dispatch(evtCode, data);
        }

        #endregion

        #region Hotspot

        public void OnAllItemsDestroyed(EventCode evtCode)
        {
            dispatcher.Dispatch(evtCode);
        }

        public void OnEnableHotspot(EventCode evtCode, string itemId, string HotspotId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.HOTSPOT_ID, HotspotId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnDisableHotspot(EventCode evtCode, string itemId, string HotspotId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.HOTSPOT_ID, HotspotId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnHotspotTeleport(EventCode evtCode, string itemId, string hotspotId, int targetItemIndex)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.HOTSPOT_ID, hotspotId);
            data.Parameters.Add((byte)CommonParameterCode.ITEM_INDEX, targetItemIndex);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnEnableHotspotStatus(EventCode evtCode, string itemId, string hotspotId, string hotspotStatusId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.HOTSPOT_ID, hotspotId);
            data.Parameters.Add((byte)CommonParameterCode.HOTSPOT_STATUS_ID, hotspotStatusId);

            dispatcher.Dispatch(evtCode, data);
            return;
        }

        #endregion

        #region Data Synchronization

        public void OnPanoramaSynced(EventCode evtCode, string itemId, CTPanoramaData panoramaData, float elapsedTimeMilliseconds, bool isLoaded)
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.DATA_PANORAMA, panoramaData);
            data.Parameters.Add((byte)CommonParameterCode.TIME_ELAPSED, elapsedTimeMilliseconds);
            data.Parameters.Add((byte)CommonParameterCode.IS_LOADED, isLoaded);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnPanoramaVideoSynced(EventCode evtCode, string itemId, CTPanoramaVideoData panoramaVideoData, int frameIndex, float elapsedTimeMilliseconds, bool isPlaying, bool isLoaded)
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.DATA_PANORAMA_VIDEO, panoramaVideoData);
            data.Parameters.Add((byte)CommonParameterCode.VIDEO_FRAME, frameIndex);
            data.Parameters.Add((byte)CommonParameterCode.TIME_ELAPSED, elapsedTimeMilliseconds);
            data.Parameters.Add((byte)CommonParameterCode.IS_PLAYING, isPlaying);
            data.Parameters.Add((byte)CommonParameterCode.IS_LOADED, isLoaded);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnStereoPanoramaVideoSynced(EventCode evtCode, string itemId, CTStereoPanoramaVideoData stereoPanoramaVideoData, int frameIndex, float elapsedTimeMilliseconds, bool isPlaying, bool isLoaded)
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.STEREO_PANORAMA_VIDEO_DATA, stereoPanoramaVideoData);
            data.Parameters.Add((byte)CommonParameterCode.VIDEO_FRAME, frameIndex);
            data.Parameters.Add((byte)CommonParameterCode.TIME_ELAPSED, elapsedTimeMilliseconds);
            data.Parameters.Add((byte)CommonParameterCode.IS_PLAYING, isPlaying);
            data.Parameters.Add((byte)CommonParameterCode.IS_LOADED, isLoaded);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnLineNotationSynced(EventCode evtCode, string itemId, bool isVisible, CTLineNotationData lineNotationData)
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.DATA_LINE_NOTATION, lineNotationData);
            data.Parameters.Add((byte)CommonParameterCode.IS_VISIBLE, isVisible);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnInteractiveItemSynced(EventCode evtCode, string itemId, CTInteractiveItemData interactiveItemData, string attachedSlotId)
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.INTERACTIVE_ITEM_DATA, interactiveItemData);
            data.Parameters.Add((byte)CommonParameterCode.INTERACTIVE_ATTACHED_SLOT_ID, attachedSlotId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnSlotStatusSynced(EventCode evtCode, string itemId, CTSlotData slotData, string attachedItemId, ESlotState slotState, string animationName, float elapsedTimeMilliseconds)
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.SLOT_ITEM_DATA, slotData);
            data.Parameters.Add((byte)CommonParameterCode.SLOT_ATTACHED_ITEM_ID, attachedItemId);
            data.Parameters.Add((byte)CommonParameterCode.SLOT_STATE, slotState);
            data.Parameters.Add((byte)CommonParameterCode.ANIMATION_NAME, animationName);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnSyncTimerSynced(EventCode evtCode, string itemId, CTSyncTimerData syncTimerData, float curTimeMilliseconds, bool isActive)
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.SYNC_TIMER_DATA, syncTimerData);
            data.Parameters.Add((byte)CommonParameterCode.CURRENT_TIME, curTimeMilliseconds);
            data.Parameters.Add((byte)CommonParameterCode.IS_ACTIVE, isActive);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnTutorialItemSynced(EventCode evtCode, string itemId, CTTutorialItemData tutorialItemData, bool isActivated, string animationName, float elapsedTimeMilliseconds, bool isPlaying)
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.TUTORIAL_ITEM_DATA, tutorialItemData);
            data.Parameters.Add((byte)CommonParameterCode.IS_ACTIVE, isActivated);
            data.Parameters.Add((byte)CommonParameterCode.ANIMATION_NAME, animationName);
            data.Parameters.Add((byte)CommonParameterCode.TIME_ELAPSED, elapsedTimeMilliseconds);
            data.Parameters.Add((byte)CommonParameterCode.IS_PLAYING, isPlaying);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnUIStatusSynced(EventCode evtCode, string itemId, CTUIData uiData, bool isVisible)
        {
            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.UI_ITEM_DATA, uiData);
            data.Parameters.Add((byte)CommonParameterCode.IS_VISIBLE, isVisible);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnAvatarStatusSynced(EventCode evtCode, string itemId, CTAvatarData avatarData)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.DATA_AVATAR, avatarData);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnPlanarVideoSynced(EventCode evtCode, string itemId, CTPlanarVideoData planarVideoData, int frameIndex, float elapsedTimeMilliseconds, bool isPlaying, bool isLoaded)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.DATA_PLANAR_VIDEO, planarVideoData);
            data.Parameters.Add((byte)CommonParameterCode.VIDEO_FRAME, frameIndex);
            data.Parameters.Add((byte)CommonParameterCode.TIME_ELAPSED, elapsedTimeMilliseconds);
            data.Parameters.Add((byte)CommonParameterCode.IS_PLAYING, isPlaying);
            data.Parameters.Add((byte)CommonParameterCode.IS_LOADED, isLoaded);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnSlotGroupStatusSynced(EventCode evtCode, string itemId, CTSlotGroupData slotGroupData, string finishAniItemId, string activatedSubSlotGroupId)
        {
            MessageData data = new MessageData();

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.DATA_CT_SLOT_GROUP_DATA, slotGroupData);
            data.Parameters.Add((byte)CommonParameterCode.ACTIVE_SUB_SLOT_GROUP_ID, activatedSubSlotGroupId);

            dispatcher.Dispatch(evtCode, data);
        }

        public void OnVolumeSet(EventCode evtCode, float volume)
        {
            // TODO: Implement method
        }

        #endregion

        #region Hotspot Elements

        public void OnAudioCreated(EventCode evtCode, string itemId, string worldId, bool isOwner, CTAudioData audioData)
        {
            Debug.Log("Error! Not yet implemented in ClientBridge!");
        }

        public void OnAudioHidden(EventCode evtCode, string itemId)
        {
            Debug.Log("Error! Not yet implemented in ClientBridge!");
        }

        public void OnAudioShown(EventCode evtCode, string itemId)
        {
            Debug.Log("Error! Not yet implemented in ClientBridge!");
        }

        public void OnAudioPlayed(EventCode evtCode, string itemId, int frameIndex)
        {
            Debug.Log("Error! Not yet implemented in ClientBridge!");
        }

        public void OnAudioPaused(EventCode evtCode, string itemId, int frameIndex)
        {
            Debug.Log("Error! Not yet implemented in ClientBridge!");
        }

        public void OnAudioReplayed(EventCode evtCode, string itemId)
        {
            Debug.Log("Error! Not yet implemented in ClientBridge!");
        }

        public void OnAudioStopped(EventCode evtCode, string itemId)
        {
            Debug.Log("Error! Not yet implemented in ClientBridge!");
        }

        public void OnAudioSought(EventCode evtCode, string itemId, int frameIndex)
        {
            Debug.Log("Error! Not yet implemented in ClientBridge!");
        }

        public void OnAudioSynced(EventCode evtCode, string itemId, CTAudioData audioData, int frameIndex, float elapsedTimeMilliseconds, bool isPlaying, bool isLoaded)
        {
            Debug.Log("Error! Not yet implemented in ClientBridge!");
        }

        public void OnImageCreated(EventCode evtCode, string itemId, string worldId, bool isOwner, CTImageData imageData)
        {
            Debug.Log("Error! Not yet implemented in ClientBridge!");
        }

        public void OnImageSynced(EventCode evtCode, string itemId, CTImageData imageData)
        {
            Debug.Log("Error! Not yet implemented in ClientBridge!");
        }

        #endregion
    }
}