﻿using System;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace PulseIQ.Local.ClientCommon.Components.DllBridge
{
    public struct OldSItemMessageData
    {
        public Object data;
        public EventCode eventType;
        public string itemID;
    }
}