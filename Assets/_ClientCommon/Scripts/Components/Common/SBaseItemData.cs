﻿using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.Components.Common
{
    public struct SBaseItemData
    {
        public string itemID;
        public ItemTypeCode itemType;
        public Vector3 itemPostion;
        public Quaternion itemRotation;
    }
}