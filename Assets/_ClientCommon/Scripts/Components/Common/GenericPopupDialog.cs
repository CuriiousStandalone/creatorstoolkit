﻿using System;

using UnityEngine;
using UnityEngine.UI;

namespace PulseIQ.Local.ClientCommon.Components.Common
{
    public class GenericPopupDialog : MonoBehaviour
    {
        //[SerializeField]
        //private UILabel headingText;

        [SerializeField]
        private Text bodyText;

        [SerializeField]
        private Text leftButtonText, rightButtonText, centerButtonText;

        [SerializeField]
        private Button leftButton, rightButton, centerButton;

        private Action leftButtonAction, rightButtonAction, centerButtonAction;

        public void InitializeDialog(/*string headingTxt, */string bodyTxt, string buttonText, Action buttonCallBack)
        {
            //headingText.text = headingTxt;
            bodyText.text = bodyTxt;
            centerButtonText.text = buttonText;

            leftButton.gameObject.SetActive(false);
            rightButton.gameObject.SetActive(false);
            centerButton.gameObject.SetActive(true);

            centerButtonAction = buttonCallBack;
        }

        public void InitializeDialog(/*string headingTxt, */string bodyTxt, string buttonLeftTxt, string buttonRightTxt, Action buttonLeftCallBack, Action buttonRightCallBack)
        {
            //headingText.text = headingTxt;
            bodyText.text = bodyTxt;
            leftButtonText.text = buttonLeftTxt;
            rightButtonText.text = buttonRightTxt;

            leftButton.gameObject.SetActive(true);
            rightButton.gameObject.SetActive(true);
            centerButton.gameObject.SetActive(false);

            leftButtonAction = buttonLeftCallBack;
            rightButtonAction = buttonRightCallBack;
        }

        public void LeftButton_ClickHandler()
        {
            leftButtonAction();
            gameObject.SetActive(false);
        }

        public void RightButton_ClickHandler()
        {
            rightButtonAction();
            gameObject.SetActive(false);
        }

        public void CenterButton_ClickHandler()
        {
            centerButtonAction();
            gameObject.SetActive(false);
        }
    }
}