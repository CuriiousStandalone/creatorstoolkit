﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.Components.Common
{
	public class MemoryUtilities
	{
		public static void CleanUpMemory ()
		{
			Resources.UnloadUnusedAssets ();
		}
	}
}
