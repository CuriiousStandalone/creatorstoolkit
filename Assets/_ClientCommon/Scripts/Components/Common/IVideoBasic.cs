﻿using PulseIQ.Local.Common.Model.Multimedia;

namespace PulseIQ.Local.ClientCommon.Components.Common
{
    public interface IVideoBasic
    {
        void PlayMedia(float frame);

        void PauseMedia(float frame);

        void StopMedia();

        void RestartMedia();

        void SeekMedia(float frame);

        EMultimediaEndBehaviorCode EndBehaviourCode { get; set; }
    }
}