﻿using System;
using System.IO;
using System.Diagnostics;
using System.Threading.Tasks;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.Components.Common
{
    public static class FFMpegUtility
    {
        private static Process process;

        public static Task<string> GenerateThumbnailOfVideo(string inputFile, string outputFile)
        {
            if (File.Exists(outputFile))
            {
                File.Delete(outputFile);
            }

            string ffmpegFileLocation = Path.GetFullPath(Settings.FFMpegDirectory);

            inputFile = Path.GetFullPath(inputFile);
            outputFile = Path.GetFullPath(outputFile);

            ProcessStartInfo processInfo = new ProcessStartInfo();

            if (Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor)
            {
                string arguments = "-itsoffset -1 -i \"" + inputFile + "\" -vframes 1 -filter:v scale=\"160:87\" \"" + outputFile + "\"";

                UnityEngine.Debug.Log("Arguments are \n" + arguments);

                processInfo.FileName = ffmpegFileLocation;
                processInfo.Arguments = arguments;
                processInfo.CreateNoWindow = true;
                processInfo.UseShellExecute = false;
                processInfo.RedirectStandardError = true;
                processInfo.RedirectStandardOutput = true;
            }
            else
            {
                string arguments = "ffmpeg -itsoffset -1 -i \"" + inputFile + "\" -vframes 1 -filter:v scale=\"160:87\" \"" + outputFile + "\"";

                UnityEngine.Debug.Log("Arguments are \n" + arguments);

                processInfo = new ProcessStartInfo("cmd.exe", "/c" + arguments);

                processInfo.WorkingDirectory = ffmpegFileLocation;
                processInfo.CreateNoWindow = true;
                processInfo.UseShellExecute = false;
                processInfo.RedirectStandardInput = true;
                processInfo.RedirectStandardError = true;
                processInfo.RedirectStandardOutput = true;
            }

            try
            {
                process = new Process();

                process.StartInfo = processInfo;
                process.OutputDataReceived += new DataReceivedEventHandler(OnOutputDataReceived);
                process.ErrorDataReceived += new DataReceivedEventHandler(OnErrorDataReceived);
                process.EnableRaisingEvents = true;

                process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();

                process.WaitForExit();

                process.Close();
            }
            catch (Exception ex)
            {
                UnityEngine.Debug.Log("exception: " + ex.Message);
                if (null != process)
                {
                    process.Close();
                }
            }

            return Task.FromResult(outputFile);
        }

        public static Task<string> GenerateThumbnailOfImage(string inputFile, string outputFile)
        {
            if (File.Exists(outputFile))
            {
                File.Delete(outputFile);
            }

            string ffmpegFileLocation = Path.GetFullPath(Settings.FFMpegDirectory);

            inputFile = Path.GetFullPath(inputFile);
            outputFile = Path.GetFullPath(outputFile);

            ProcessStartInfo processInfo;

            if (Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor)
            {
                string arguments = "-i \"" + inputFile + "\" -vf scale=\"160:87\" \"" + outputFile + "\"";

                UnityEngine.Debug.Log("Arguments are \n" + arguments);

                processInfo = new ProcessStartInfo();

                processInfo.FileName = ffmpegFileLocation;
                processInfo.Arguments = arguments;
                processInfo.CreateNoWindow = true;
                processInfo.UseShellExecute = false;
                processInfo.RedirectStandardError = true;
                processInfo.RedirectStandardOutput = true;
            }
            else
            {
                string arguments = "ffmpeg -i \"" + inputFile + "\" -vf scale=\"160:87\" \"" + outputFile + "\"";

                UnityEngine.Debug.Log("Arguments are \n" + arguments);

                processInfo = new ProcessStartInfo("cmd.exe", "/c" + arguments);

                processInfo.WorkingDirectory = ffmpegFileLocation;
                processInfo.CreateNoWindow = true;
                processInfo.UseShellExecute = false;
                processInfo.RedirectStandardInput = true;
                processInfo.RedirectStandardError = true;
                processInfo.RedirectStandardOutput = true;
            }

            try
            {
                process = new Process();

                process.StartInfo = processInfo;
                process.OutputDataReceived += new DataReceivedEventHandler(OnOutputDataReceived);
                process.ErrorDataReceived += new DataReceivedEventHandler(OnErrorDataReceived);
                process.EnableRaisingEvents = true;

                process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();

                process.WaitForExit();

                process.Close();
            }
            catch (Exception ex)
            {
                UnityEngine.Debug.Log("exception: " + ex.Message);
                if (null != process)
                {
                    process.Close();
                }
            }

            return Task.FromResult(outputFile);
        }

        public static Task<string> ConvertImagetoPNG(string inputFile, string outputFile)
        {
            if (File.Exists(outputFile))
            {
                File.Delete(outputFile);
            }

            string ffmpegFileLocation = Path.GetFullPath(Settings.FFMpegDirectory);

            inputFile = Path.GetFullPath(inputFile);
            outputFile = Path.GetFullPath(outputFile);

            ProcessStartInfo processInfo = new ProcessStartInfo();

            if (Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor)
            {
                string arguments = "-i \"" + inputFile + "\" \"" + outputFile + "\"";

                UnityEngine.Debug.Log("Arguments are \n" + arguments);

                processInfo.FileName = ffmpegFileLocation;
                processInfo.Arguments = arguments;
                processInfo.CreateNoWindow = true;
                processInfo.UseShellExecute = false;
                processInfo.RedirectStandardError = true;
                processInfo.RedirectStandardOutput = true;
            }
            else
            {
                    string arguments = "ffmpeg -i \"" + inputFile + "\" \"" + outputFile + "\"";

                    UnityEngine.Debug.Log("Arguments are \n" + arguments);

                    processInfo = new ProcessStartInfo("cmd.exe", "/c" + arguments);

                    processInfo.WorkingDirectory = ffmpegFileLocation;
                    processInfo.CreateNoWindow = true;
                    processInfo.UseShellExecute = false;
                    processInfo.RedirectStandardInput = true;
                    processInfo.RedirectStandardError = true;
                    processInfo.RedirectStandardOutput = true;
            }
            try
            {
                process = new Process();

                process.StartInfo = processInfo;
                process.OutputDataReceived += new DataReceivedEventHandler(OnOutputDataReceived);
                process.ErrorDataReceived += new DataReceivedEventHandler(OnErrorDataReceived);
                process.EnableRaisingEvents = true;

                process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();

                process.WaitForExit();

                process.Close();
            }
            catch (Exception ex)
            {
                UnityEngine.Debug.Log("exception: " + ex.Message);
                if (null != process)
                {
                    process.Close();
                }
            }

            return Task.FromResult(outputFile);
        }

        public static Task<string> ConvertAudioToMP4(string inputFile, string outputFile)
        {
            if (File.Exists(outputFile))
            {
                File.Delete(outputFile);
            }

            string ffmpegFileLocation = Path.GetFullPath(Settings.FFMpegDirectory);

            inputFile = Path.GetFullPath(inputFile);
            outputFile = Path.GetFullPath(outputFile);

            ProcessStartInfo processInfo = new ProcessStartInfo();

            if (Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor)
            {
                string arguments = "-i \"" + inputFile + "\" \"" + outputFile + "\"";

                UnityEngine.Debug.Log("Arguments are \n" + arguments);

                processInfo.FileName = ffmpegFileLocation;
                processInfo.Arguments = arguments;
                processInfo.CreateNoWindow = true;
                processInfo.UseShellExecute = false;
                processInfo.RedirectStandardError = true;
                processInfo.RedirectStandardOutput = true;
            }
            else
            {
                string arguments = "ffmpeg -i \"" + inputFile + "\" \"" + outputFile + "\"";

                UnityEngine.Debug.Log("Arguments are \n" + arguments);

                processInfo = new ProcessStartInfo("cmd.exe", "/c" + arguments);

                processInfo.WorkingDirectory = ffmpegFileLocation;
                processInfo.CreateNoWindow = true;
                processInfo.UseShellExecute = false;
                processInfo.RedirectStandardInput = true;
                processInfo.RedirectStandardError = true;
                processInfo.RedirectStandardOutput = true;
            }

            try
            {
                process = new Process();

                process.StartInfo = processInfo;
                process.OutputDataReceived += new DataReceivedEventHandler(OnOutputDataReceived);
                process.ErrorDataReceived += new DataReceivedEventHandler(OnErrorDataReceived);
                process.EnableRaisingEvents = true;

                process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();

                process.WaitForExit();

                process.Close();
            }
            catch (Exception ex)
            {
                UnityEngine.Debug.Log("exception: " + ex.Message);
                if (null != process)
                {
                    process.Close();
                }
            }

            return Task.FromResult(outputFile);
        }

        private static void OnOutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            UnityEngine.Debug.Log("output >> " + e.Data);
        }

        private static void OnErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            UnityEngine.Debug.Log("error >> " + e.Data);
        }
    }
}
