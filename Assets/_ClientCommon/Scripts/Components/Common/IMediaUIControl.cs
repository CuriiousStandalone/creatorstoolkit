﻿using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using strange.extensions.dispatcher.eventdispatcher.api;

namespace PulseIQ.Local.ClientCommon.Components.Common
{
    public interface IMediaUIControl 
    {
       void init();

       string ReturnItemID();

       void OnShowMedia(ItemTypeCode type, string mediaID, string mediaName, float maxTime);

       void SetCurrentTime(float time);

       void OnHideMedia();

       IEventDispatcher GetEventDispatcher(); 
    }
}