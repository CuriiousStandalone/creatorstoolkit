using CuriiousIQ.Local.CreatorsToolkit.Components.Common;
using PulseIQ.Local.Common;
using System.IO;

using UnityEngine;

namespace PulseIQ.Local.ClientCommon.Components.Common
{
    public class Settings
    {
        public static EApplicationType applicationType;

        public static string TestItemPath = "MockupPackages/PackageTest/WorldTest1/";
        public static string ResourcePath = "";

        public static string editorRootPath = "";
        public static string defaultEditorRootPath = "";

        private static string tempDownloadPath = "/PackageData/TempDownloads/";
        private static string tempAndroidDownloadPath = "/PulseIQ/PackageData/TempDownloads/";

        private static string baseItemPath = "/PackageData/ResourceData/Windows/1_Item/";
        private static string itemPath = "/PackageData/ResourceData/Windows/2_Interactive/";
        private static string slotItemPath = "/PackageData/ResourceData/Windows/3_Slot/";
        private static string timerItemPath = "/PackageData/ResourceData/Windows/10_SyncTimer/";
        private static string tutorialItemPath = "/PackageData/ResourceData/Windows/12_Tutorial/";
        private static string uiItemPath = "/PackageData/ResourceData/Windows/15_UI/";

        private static string androidbaseItemPath = "/PulseIQ/PackageData/ResourceData/Android/1_Item/";
        private static string androiditemPath = "/PulseIQ/PackageData/ResourceData/Android/2_Interactive/";
        private static string androidslotItemPath = "/PulseIQ/PackageData/ResourceData/Android/3_Slot/";
        private static string androidTimerItemPath = "/PulseIQ/PackageData/ResourceData/Android/10_SyncTimer/";
        private static string androidTutorialItemPath = "/PulseIQ/PackageData/ResourceData/Android/12_Tutorial/";
        private static string androidUIItemPath = "/PulseIQ/PackageData/ResourceData/Android/15_UI/";

        public static string baseItemThumbPath = "/PackageData/ResourceData/Thumbnail/1_Item/";
        public static string itemThumbPath = "/PackageData/ResourceData/Thumbnail/2_Interactive/";
        public static string slotItemThumbPath = "/PackageData/ResourceData/Thumbnail/3_Slot/";
        public static string panoramaThumbPath = "/PackageData/ResourceData/Thumbnail/4_Panorama/";
        public static string panoramaVideoThumbPath = "/PackageData/ResourceData/Thumbnail/5_PanoramaVideo/";
        public static string planarVideoThumbPath = "/PackageData/ResourceData/Thumbnail/6_PlanarVideo/";
        public static string timerItemThumbPath = "/PackageData/ResourceData/Thumbnail/10_SyncTimer/";
        public static string tutorialItemThumbPath = "/PackageData/ResourceData/Thumbnail/12_Tutorial/";
        public static string pospProcessingThumbPath = "/PackageData/ResourceData/Thumbnail/13_PostProcessing/";
        public static string skyboxThumbPath = "/PackageData/ResourceData/Thumbnail/14_Skybox/";
        public static string uiItemThumbPath = "/PackageData/ResourceData/Thumbnail/15_UI/";
        public static string stereoPanoramaVideoThumbPath = "/PackageData/ResourceData/Thumbnail/17_StereoPanoramaVideo/";
        public static string audioThumbPath = "/PackageData/ResourceData/Thumbnail/20_Audio/";
        public static string imageThumbPath = "/PackageData/ResourceData/Thumbnail/21_Image/";

        public static string internalBaseItemThumbPath = "PackageData/ResourceData/Thumbnail/1_Item/";
        public static string internalItemThumbPath = "PackageData/ResourceData/Thumbnail/2_Interactive/";
        public static string internalSlotItemThumbPath = "PackageData/ResourceData/Thumbnail/3_Slot/";
        public static string internalPanoramaThumbPath = "PackageData/ResourceData/Thumbnail/4_Panorama/";
        public static string internalPanoramaVideoThumbPath = "PackageData/ResourceData/Thumbnail/5_PanoramaVideo/";
        public static string internalPlanarVideoThumbPath = "PackageData/ResourceData/Thumbnail/6_PlanarVideo/";
        public static string internalTimerItemThumbPath = "PackageData/ResourceData/Thumbnail/10_SyncTimer/";
        public static string internalTutorialItemThumbPath = "PackageData/ResourceData/Thumbnail/12_Tutorial/";
        public static string internalPospProcessingThumbPath = "PackageData/ResourceData/Thumbnail/13_PostProcessing/";
        public static string internalSkyboxThumbPath = "PackageData/ResourceData/Thumbnail/14_Skybox/";
        public static string internalUiItemThumbPath = "PackageData/ResourceData/Thumbnail/15_UI/";
        public static string internalStereoPanoramaVideoThumbPath = "PackageData/ResourceData/Thumbnail/17_StereoPanoramaVideo/";
        public static string internalAudioThumbPath = "PackageData/ResourceData/Thumbnail/20_Audio/";
        public static string internalImageThumbPath = "PackageData/ResourceData/Thumbnail/21_Image/";

        private static string ffmpegDirectory = Pathy.Combine(Application.streamingAssetsPath, "ffmpeg");
        private static string ffmpegDirectoryMacOSX = Pathy.Combine(Application.streamingAssetsPath, "ffmpeg/MacOSX/ffmpeg");

        public static string AvatarResPath = "Prefabs/Avatars/";
        public static string MediaPlayerPath = "Prefabs/MediaPlayers/";
        public static string PrefabPath = "Prefabs/";

        private static string editorPanoramaPath = "/PackageData/ResourceData/MediaData/4_Panorama/";
        private static string editorPanoramaVideoPath = "/PackageData/ResourceData/MediaData/5_PanoramaVideo/";
        private static string editorPlanarVideoPath = "/PackageData/ResourceData/MediaData/6_PlanarVideo/";
        private static string editorStereoPanoramaVideoPath = "/PackageData/ResourceData/MediaData/17_StereoPanoramaVideo/";
        private static string editorAudioPath = "/PackageData/ResourceData/MediaData/20_Audio/";
        private static string editorImagePath = "/PackageData/ResourceData/MediaData/21_Image/";

        public static string internalPanoramaPath = "PackageData/ResourceData/MediaData/4_Panorama/";
        public static string internalPanoramaVideoPath = "PackageData/ResourceData/MediaData/5_PanoramaVideo/";
        public static string internalPlanarVideoPath = "PackageData/ResourceData/MediaData/6_PlanarVideo/";
        public static string internalStereoPanoramaVideoPath = "PackageData/ResourceData/MediaData/17_StereoPanoramaVideo/";
        public static string internalAudioPath = "PackageData/ResourceData/MediaData/20_Audio/";
        public static string internalImagePath = "PackageData/ResourceData/MediaData/21_Image/";

        private static string androidPanoramaPath = "/PulseIQ/PackageData/ResourceData/MediaData/4_Panorama/";
        private static string androidPanoramaVideoPath = "/PulseIQ/PackageData/ResourceData/MediaData/5_PanoramaVideo/";
        private static string androidPlanarVideoPath = "/PulseIQ/PackageData/ResourceData/MediaData/6_PlanarVideo/";
        private static string androidStereoPanoramaVideoPath = "/PulseIQ/PackageData/ResourceData/MediaData/17_StereoPanoramaVideo/";
        private static string androidAudioPath = "/PulseIQ/PackageData/ResourceData/MediaData/20_Audio/";
        private static string androidImagePath = "/PulseIQ/PackageData/ResourceData/MediaData/21_Image/";

        private static string postProcessingProfilePath = "/PackageData/ResourceData/Windows/13_PostProcessing/";
        private static string androidPostProcessingProfilePath = "/PulseIQ/PackageData/ResourceData/Android/13_PostProcessing/";

        private static string skyboxPath = "/PackageData/ResourceData/Windows/14_Skybox/";
        private static string androidSkyboxPath = "/PulseIQ/PackageData/ResourceData/Android/14_Skybox/";

        private static string streamingAssetPathPC = System.IO.Path.Combine(Application.streamingAssetsPath, "PulseIQConfig.xml");
        private static string streamingAssetPathAndroid = "/PulseIQ/PulseIQConfig.xml";

        private static string androidPackagePath = "/PulseIQ/PackageData/Packages/";
        private static string packagePath = "/PackageData/Packages/";

        private static string androidWorldPath = "/PulseIQ/PackageData/Worlds/";
        private static string worldPath = "/PackageData/";

        private static string androidItemVersionPath = "/PulseIQ/PackageData/ResourceData/VersionControl.xml";
        private static string itemVersionPath = "/PackageData/ResourceData/VersionControl.xml";

        private static string androidCachePath = "/PulseIQ/PackageData/";
        private static string cachePath = "/PackageData/";

        private static string _configFilePath = "/PackageData/PulseIQConfig.xml";
        private static string _configFilePath_Android = "/PulseIQ/PackageData/PulseIQConfig.xml";

        public const float ItemMoveToUpdateRate = 2.5f;
        public const float ItemMoveToSpeed = 4f;
        public const float ItemRotateToSpeed = 4f;
        public const float ItemScaleToSpeed = 4f;

        public static string StreamingAssetsPath
        {
            get
            {
                if (Application.platform == RuntimePlatform.WindowsPlayer)
                {
                    return Application.streamingAssetsPath;
                }
                else 
                {
                    return Application.streamingAssetsPath;
                }                        
            }
        }

        public static string PanoramaPath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform ==  RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, editorPanoramaPath);
                }
                else
                {
                    return Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidPanoramaPath;
                }
            }
        }

        public static string PanoramaVideoPath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                   return Pathy.Combine(editorRootPath, editorPanoramaVideoPath);
                }
                else
                {
                    return Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidPanoramaVideoPath;
                }
            }
        }

        public static string PlanarVideoPath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, editorPlanarVideoPath);
                }
                else
                {
                    return Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidPlanarVideoPath;

                }
            }
        }

        public static string StereoPanoramaVideoPath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, editorStereoPanoramaVideoPath);
                }
                else
                {
                    return Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidStereoPanoramaVideoPath;
                }
            }
        }

        public static string AudioPath
        {
            get
            {
                if(Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, editorAudioPath);
                }
                else
                {
                    return Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidAudioPath;
                }
            }
        }

        public static string ImagePath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, editorImagePath);
                }
                else
                {
                    return Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidImagePath;
                }
            }
        }

        public static string ItemPath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, itemPath);
                }
                else
                {
                    return Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androiditemPath;
                }
            }
        }

        public static string BaseItemPath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, baseItemPath);
                }
                else
                {
                    return Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidbaseItemPath;
                }
            }
        }

        public static string SlotItemPath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, slotItemPath);
                }
                else
                {
                    return Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidslotItemPath;
                }
            }
        }

        public static string TimerItemPath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, timerItemPath);
                }
                else
                {
                    return Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidTimerItemPath;
                }
            }
        }

        public static string TutorialItemPath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath,tutorialItemPath);
                }
                else
                {
                    return Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidTutorialItemPath;
                }
            }
        }

        public static string PostProcessingProfilePath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath,postProcessingProfilePath);
                }
                else
                {
                    return Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidPostProcessingProfilePath;
                }
            }
        }

        public static string SkyboxPath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, skyboxPath);
                }
                else
                {
                    return Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidSkyboxPath;
                }
            }
        }

        public static string UIItemPath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, uiItemPath);
                }
                else
                {
                    return Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidUIItemPath;
                }
            }
        }

        public static string PackagePath
        {
            get
            {
                if(Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, packagePath);
                }
                else
                {
                    return Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidPackagePath;
                }
            }
        }

        public static string WorldPath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, worldPath);
                }
                else
                {
                    return Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidWorldPath;
                }
            }
        }

        public static string PackageThumbnailPath
        {
            get
            {
                return PackagePath + "Thumbnail/";
            }
        }

        public static string WorldThumbnailPath
        {
            get
            {
                return ImagePath;
            }
        }

        public static string ItemVersionPath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, itemVersionPath);
                }
                else
                {
                    return Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidItemVersionPath;
                }
            }
        }

        public static string CachePath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, cachePath);
                }
                else
                {
                    return Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidCachePath;
                }
            }
        }

        /*
		public static string ConfigFilePath
		{
			get
			{
				if (Application.isEditor)
				{
					return streamingAssetPathPC;
				}
				else
				{
					return "file://" + Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + streamingAssetPathAndroid;
				}
			}
		}*/

        public static string TempDownloadPath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, tempDownloadPath);
                }
                else
                {
                    return Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + tempAndroidDownloadPath;
                }
            }
        }

        public static string PanoramaFilePath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, editorPanoramaPath);
                }
                else
                {
                    return "file://" + Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidPanoramaPath;
                }
            }
        }

        public static string PanoramaVideoFilePath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, editorPanoramaVideoPath);
                }
                else
                {
                    return "file://" + Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidPanoramaVideoPath;
                }
            }
        }

        public static string PlanarVideoFilePath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, editorPlanarVideoPath);
                }
                else
                {
                    return "file://" + Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidPlanarVideoPath;
                }
            }
        }

        public static string StereoPanoramaVideoFilePath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, editorStereoPanoramaVideoPath);
                }
                else
                {
                    return "file://" + Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidStereoPanoramaVideoPath;
                }
            }
        }

        public static string AudioFilePath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, editorAudioPath);
                }
                else
                {
                    return "file://" + Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidAudioPath;
                }
            }
        }

        public static string ImageFilePath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, editorImagePath);
                }
                else
                {
                    return "file://" + Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidImagePath;
                }
            }
        }

        public static string ItemFilePath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, itemPath);
                }
                else
                {
                    return "file://" + Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androiditemPath;
                }
            }
        }

        public static string BaseItemFilePath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, baseItemPath);
                }
                else
                {
                    return "file://" + Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidbaseItemPath;
                }
            }
        }

        public static string SlotItemFilePath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, slotItemPath);
                }
                else
                {
                    return "file://" + Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidslotItemPath;
                }
            }
        }

        public static string TimerItemFilePath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return editorRootPath + timerItemPath;
                }
                else
                {
                    return "file://" + Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidTimerItemPath;
                }
            }
        }

        public static string TutorialItemFilePath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer)
                {
                    return editorRootPath + tutorialItemPath;
                }
                else
                {
                    return "file://" + Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidTutorialItemPath;
                }
            }
        }

        public static string UIItemFilePath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer)
                {
                    return editorRootPath + uiItemPath;
                }
                else
                {
                    return "file://" + Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidUIItemPath;
                }
            }
        }

        public static string PostProcessingProfileFilePath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer)
                {
                    return editorRootPath + postProcessingProfilePath;
                }
                else
                {
                    return "file://" + Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidPostProcessingProfilePath;
                }
            }
        }

        public static string SkyboxFilePath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer)
                {
                    return editorRootPath + skyboxPath;
                }
                else
                {
                    return "file://" + Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidSkyboxPath;
                }
            }
        }

        public static string WorldFilePath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer)
                {
                    return editorRootPath + worldPath;
                }
                else
                {
                    return "file://" + Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidWorldPath;
                }
            }
        }

        public static string PackageThumbnailFilePath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Path.Combine(PackagePath + "Thumbnail/");
                }
                else
                {
                    return "file://" + PackagePath + "Thumbnail/";
                }
            }
        }

        public static string WorldThumbnailFilePath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return WorldPath + "Thumbnail/";
                }
                else
                {
                    return "file://" + WorldPath + "Thumbnail/";
                }
            }
        }

        public static string CacheFilePath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Pathy.Combine(editorRootPath, cachePath);
                }
                else
                {
                    return "file://" + Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + androidCachePath;
                }
            }
        }

        public static string TempDownloadFilePath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return editorRootPath + tempDownloadPath;
                }
                else
                {
                    return "file://" + Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + tempAndroidDownloadPath;
                }
            }
        }

        public static string ConfigFilePath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
                {
                    return Path.Combine(editorRootPath + _configFilePath);
                }
                else
                {
                    return Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + _configFilePath_Android;
                }
            }
        }

        public static string RootPath
        {
            get
            {
                if(Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer)
                {
                    return editorRootPath;
                }
                else
                {
                    return Directory.GetParent(Application.persistentDataPath).Parent.Parent.FullName;
                }
            }
        }

        public static string FFMpegDirectory
        {
            get
            {
                if(Application.platform == RuntimePlatform.WindowsEditor|| Application.platform == RuntimePlatform.WindowsPlayer)
                {
                    return ffmpegDirectory;
                }
                else if(Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor)
                {
                    return ffmpegDirectoryMacOSX;
                }
                else
                {
                    return "";
                }
            }
        }

        public static string GetPathByItemType(ItemTypeCode itemType, string platformType = "windows")
        {
            switch (itemType)
            {
                default:
                case ItemTypeCode.ITEM:
                    return BaseItemPath;

                case ItemTypeCode.INTERACTIVE_ITEM:
                    return ItemPath;

                case ItemTypeCode.SLOT:
                    return SlotItemPath;

                case ItemTypeCode.PANORAMA:
                    return PanoramaPath;

                case ItemTypeCode.PANORAMA_VIDEO:
                    return PanoramaVideoPath;

                case ItemTypeCode.PLANAR_VIDEO:
                    return PlanarVideoPath;

                case ItemTypeCode.SYNCTIMER:
                    return TimerItemPath;

                case ItemTypeCode.TUTORIAL_ITEM:
                    return TutorialItemPath;

                case ItemTypeCode.POST_PROCESSING_PROFILE:
                    return PostProcessingProfilePath;

                case ItemTypeCode.SKYBOX:
                    return SkyboxPath;

                case ItemTypeCode.UI:
                    return UIItemPath;

                case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                    return StereoPanoramaVideoPath;

                case ItemTypeCode.AUDIO:
                    return AudioPath;

                case ItemTypeCode.IMAGE:
                    return ImagePath;
            }
        }

        public static string GetThumbnailPathByItemType(ItemTypeCode itemType)
        {
            switch (itemType)
            {
                default:
                case ItemTypeCode.ITEM:
                    return Pathy.Combine(editorRootPath, baseItemThumbPath);
                case ItemTypeCode.INTERACTIVE_ITEM:
                    return Pathy.Combine(editorRootPath, itemThumbPath);
                case ItemTypeCode.SLOT:
                    return Pathy.Combine(editorRootPath, slotItemThumbPath);
                case ItemTypeCode.PANORAMA:
                    return Pathy.Combine(editorRootPath, panoramaThumbPath);
                case ItemTypeCode.PANORAMA_VIDEO:
                    return Pathy.Combine(editorRootPath, panoramaVideoThumbPath);
                case ItemTypeCode.PLANAR_VIDEO:
                    return Pathy.Combine(editorRootPath, planarVideoThumbPath);
                case ItemTypeCode.SYNCTIMER:
                    return Pathy.Combine(editorRootPath, timerItemThumbPath);
                case ItemTypeCode.TUTORIAL_ITEM:
                    return Pathy.Combine(editorRootPath, tutorialItemThumbPath);
                case ItemTypeCode.POST_PROCESSING_PROFILE:
                    return Pathy.Combine(editorRootPath, pospProcessingThumbPath);
                case ItemTypeCode.SKYBOX:
                    return Pathy.Combine(editorRootPath, skyboxThumbPath);
                case ItemTypeCode.UI:
                    return Pathy.Combine(editorRootPath, uiItemThumbPath);
                case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                    return Pathy.Combine(editorRootPath, stereoPanoramaVideoThumbPath);
                case ItemTypeCode.AUDIO:
                    return Pathy.Combine(editorRootPath, audioThumbPath);
                case ItemTypeCode.IMAGE:
                    return Pathy.Combine(editorRootPath, imageThumbPath);
            }
        }
    }
}