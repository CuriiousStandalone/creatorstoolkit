﻿using strange.extensions.command.impl;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public class IntCmdCreateItemItemManager : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(IntCmdCodeItemManager.CREATE_ITEM, evt.data);
        }
    }
}