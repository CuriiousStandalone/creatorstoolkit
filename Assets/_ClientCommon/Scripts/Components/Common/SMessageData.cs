﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.Components.Common
{
    public class MessageData
    {
        public Dictionary<byte, object> Parameters;

        public MessageData()
        {
            Parameters = new Dictionary<byte, object>();
        }

        public object this[byte key]
        {
            get
            {
                if (Parameters.ContainsKey(key))
                {
                    return Parameters[key];
                }
                else
                {
                    return null;
                }
            }

            set
            {

            }
        }
    }
}