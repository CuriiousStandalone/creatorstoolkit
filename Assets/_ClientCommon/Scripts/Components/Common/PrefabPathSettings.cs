﻿namespace PulseIQ.Local.ClientCommon.Components.Common
{
    public class PrefabPathSettings 
    {
        #region UI Panels

        public static string panelManager = "Prefabs/UI/Panels/PanelManager";
        public static string panelTabBackground = "Prefabs/UI/Panels/CollapseButtonBackground";
        public static string panelTab = "Prefabs/UI/Panels/PanelTab";
        public static string collapseTab = "Prefabs/UI/Panels/CollapseTab";
        public static string worldItemPanel = "Prefabs/UI/Panels/WorldHierachyPanel";
        public static string skyboxPanel = "Prefabs/UI/Panels/SkyboxPanel";
        public static string postProcessingPanel = "Prefabs/UI/Panels/PostProcessingPanel";
        public static string inspectorPanel = "Prefabs/UI/Panels/InspectorPanel";
        public static string worldInfoPanel = "Prefabs/UI/Panels/WorldInfoPanel";
        public static string resourcePanel = "Prefabs/UI/Panels/ResourcePanel";
        public static string mediaResourcePanel = "Prefabs/UI/Panels/MediaResourcePanel";
        public static string hotspotPanel = "Prefabs/UI/Panels/HotspotHierachyPanel";

        #endregion

        #region ItemPrefabs

        public static string hotspot = "Prefabs/Hotspot";
        public static string hotspotStatus = "Prefabs/HotspotStatus";
        public static string hotspotBox = "Prefabs/HotspotBox";
        public static string hotspotTextElement = "Prefabs/HotspotTextElement";
        public static string hotspotImageElement = "Prefabs/HotspotImageElement";
        public static string hotspotAudioElement = "Prefabs/HotspotAudioElement";

        #endregion
    }
}