﻿using UnityEditor;

using PulseIQ.Local.ClientCommon.Components.Common.UIElements;

[CustomEditor(typeof(CTextImageButton))]
public class CTextImageButtonEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        CTextImageButton myScript = target as CTextImageButton;

        if (myScript.TransitionalText)
        {
            myScript.NormalTextColor = EditorGUILayout.ColorField("Normal Color:", myScript.NormalTextColor);
            myScript.PressedTextColor = EditorGUILayout.ColorField("Pressed Color:", myScript.PressedTextColor);
            myScript.HoverTextColor = EditorGUILayout.ColorField("Hover Color:", myScript.HoverTextColor);
            myScript.DisabledTextColor = EditorGUILayout.ColorField("Disabled Color:", myScript.DisabledTextColor);
        }
    }
}