﻿using UnityEditor;
using UnityEngine;

using PulseIQ.Local.ClientCommon.Components.Common.UIElements;

public class UIElementsDebugger : EditorWindow
{
    [MenuItem("IQ Tools/Windows/UIElements Debugger")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(UIElementsDebugger), false, "UIElements Debugger");
    }

    Vector2 scrollPosition = Vector2.zero;

    void OnGUI()
    {
        scrollPosition = GUILayout.BeginScrollView(scrollPosition);
        foreach (Transform selection in Selection.transforms)
        {
            BaseUIElement uiElement = selection.GetComponent<BaseUIElement>();
            if (uiElement != null)
            {
                GUILayout.Space(20);
                GUILayout.BeginVertical();
                GUI.color = Color.white;
                GUILayout.Label(selection.name);
                GUILayout.Space(5);
                GUI.color = Color.gray;
                GUILayout.Label("ElementId: " + uiElement.ElementId);
                GUILayout.Label("DefaultWidth: " + uiElement.DefaultWidth);
                GUILayout.Label("DefaultHeight: " + uiElement.DefaultHeight);
                GUILayout.Label("CurrentWidth : " + uiElement.CurrentWidth);
                GUILayout.Label("CurrentHeight: " + uiElement.CurrentHeight);
                GUILayout.Label("MinWidthLimit: " + uiElement.MinWidthLimit);
                GUILayout.Label("MinHeightLimit: " + uiElement.MinHeightLimit);
                GUILayout.Label("DefaultPosition: " + uiElement.DefaultPosition);
                GUILayout.Label("CurrentPosition: " + uiElement.CurrentPosition);
                GUILayout.Label("IsSelfResizeable: " + uiElement.IsSelfResizeable);
                GUILayout.EndVertical();
            }
        }

        GUILayout.EndScrollView();
    }
}