﻿using UnityEditor;

using PulseIQ.Local.ClientCommon.Components.Common.UIElements;

[CustomEditor(typeof(CTextButton))]
public class CTextButtonEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        CTextButton myScript = target as CTextButton;

        if (myScript.TransitionalText)
        {
            myScript.NormalTextColor = EditorGUILayout.ColorField("Normal Color:", myScript.NormalTextColor);
            myScript.PressedTextColor = EditorGUILayout.ColorField("Pressed Color:", myScript.PressedTextColor);
            myScript.HoverTextColor = EditorGUILayout.ColorField("Hover Color:", myScript.HoverTextColor);
            myScript.DisabledTextColor = EditorGUILayout.ColorField("Disabled Color:", myScript.DisabledTextColor);
        }
    }
}