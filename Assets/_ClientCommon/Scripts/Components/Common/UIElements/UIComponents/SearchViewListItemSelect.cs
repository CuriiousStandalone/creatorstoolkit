﻿using UnityEngine.Events;
namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents
{
    /// <summary>
    /// Copy class of ViewListItemSelect.
    /// </summary>
    public class SearchViewListItemSelect : UnityEvent<SearchListViewItem>
    {
    }
}