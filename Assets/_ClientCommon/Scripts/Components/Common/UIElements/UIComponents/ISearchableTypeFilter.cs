﻿namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents
{
    /// <summary>
    /// Implements filter search by term and category/item type.
    ///
    /// Extend this Interface on the collection and then implement filtering based on the filter string and
    /// the typeFilter passed.
    /// </summary>
    public interface ISearchableTypeFilter : ISearchable
    {
        void ApplyFilter(string filter, string[] typeFilter);
        void CancelTypeFilter(string currentTerm);
    }
}