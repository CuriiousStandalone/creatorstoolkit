﻿using System;
using System.Linq;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

using TMPro;

using UIWidgets;
using System.Collections.Generic;
using PulseIQ.Local.Common;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents
{
    [RequireComponent(typeof(Image))]
    public class CDropBox : BaseUIElement, IDropSupport<ListItemCardData>, IDropSupport<TreeNode<CTreeViewItem>>, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
    {
        #region Serailized

        [Space]
        public UnityEvent OnClick;

        public UnityEvent OnFilledClick;

        [SerializeField]
        public CustomToggleEvent OnFilled;

        public UnityEvent OnCleared;

        [Space]
        public string[] SupportedCardDragTypes;

        public string[] SupportedItemDropTypes;

        [Space]
        [SerializeField]
        private GameObject _helpTipBox;

        [SerializeField]
        private TextMeshProUGUI _helpTipText;

        [SerializeField]
        private TextMeshProUGUI _nameText;

        [SerializeField]
        private TextMeshProUGUI _resourceTypeText;

        [SerializeField]
        private TextMeshProUGUI _metaText;

        [Space]
        [SerializeField]
        private Image _iconImage;

        [SerializeField]
        private Sprite _onHoverSprite;

        [SerializeField]
        private Sprite _onHoverFilledImage;

        [SerializeField]
        private Sprite _onHoverDragDeclineSprite;

        [SerializeField]
        private GameObject _buttons;

        [Space]
        public ImagesSprites[] Images;

        #endregion

        private Sprite _defaultSprite;
        private Sprite _currentDataSprite;
        private Sprite _spriteBeforeFailed;
        private string _currentDataID;
        private string _currentDataName;
        private string _currentResourceType;
        private string _currentMeta;
        private string _defaultName;
        private string _droppedItemType;

        private States _currentState;

        internal enum DROPBOX_EVENTS
        {
            ITEM_DROPPED,
        }

        private States CurrentState
        {
            get { return _currentState; }
            set
            {
                _currentState = value;
                SetImageSprite((SpriteSwapEvent)value);
            }
        }

        private States _stateBeforeFailedDrop;
        private bool _justDropped;
        private bool _initiated = false;

        #region Internal Classes

        private enum States
        {
            DEFAULT,
            FILLED,
            //attempted to drop on a box that doesn't match
            FAILED_DROP,
            //hovered over box that doesn't match.
            DECLINED_DROP
        }

        public enum SpriteSwapEvent
        {
            DEFAULT,
            FILLED,
            //attempted to drop on a box that doesn't match
            FAILED_DROP,
            //hovered over box that doesn't match.
            DECLINED_DROP,
            ON_HOVER_FILLED,
            ON_HOVER_EMPTY,
            ON_EXIT_HOVER,
        }

        [Serializable]
        public struct ImagesSprites
        {
            public Image Image;

            public Sprite OnHoverSprite;

            public Sprite OnHoverFilledImage;

            public Sprite OnHoverDragDeclineSprite;

            public Sprite DefaultImageSprite;
        }

        #endregion

        private void Start()
        {
            if (_iconImage != null)
            {
                if (_defaultSprite == null)
                {
                    _defaultSprite = _iconImage.sprite;
                    _initiated = true;
                }
                _iconImage.DisableSpriteOptimizations();
            }
            if (_nameText != null)
            {
                _defaultName = "Drag file here";
                _nameText.text = string.IsNullOrEmpty(_currentDataName) ? _defaultName : _currentDataName;
            }
            _helpTipBox.SetActive(false);
        }

        protected override void Init()
        {
        }

        #region Public

        public string CurrentDataId => _currentDataID;

        public void FillData(string id, Sprite icon)
        {
            _currentDataID = id;
            _currentDataSprite = icon;
            _currentDataName = name;
            _spriteBeforeFailed = null;
            CurrentState = States.FILLED;
            _stateBeforeFailedDrop = States.FILLED;
            SetToCurrentValues();
            OnFilled?.Invoke(id);
        }

        public void FillData(string id, string resourceName, Sprite currentSprite = null, string resourceType = "", string meta = "")
        {
            _currentDataID = id;
            _currentDataSprite = currentSprite;
            _currentDataName = resourceName;
            _currentResourceType = resourceType;
            _currentMeta = meta;
            _spriteBeforeFailed = null;
            CurrentState = States.FILLED;
            _stateBeforeFailedDrop = States.FILLED;
            SetToCurrentValues();
            OnFilled?.Invoke(id);
        }

        public void ClearCurrentData()
        {
            _currentDataID = "";
            _currentDataSprite = null;
            _droppedItemType = "";
            CurrentState = States.DEFAULT;
            if (_iconImage != null)
            {
                if(!_initiated)
                {
                    _defaultSprite = _iconImage.sprite;
                    _initiated = true;
                }

                _iconImage.sprite = _defaultSprite;
                _spriteBeforeFailed = null;
            }
            if (_nameText != null)
            {
                _nameText.text = string.IsNullOrEmpty(_defaultName) ? "Drag file here" : _defaultName;
            }
            if (_resourceTypeText != null)
            {
                _resourceTypeText.text = "Resource file type";
            }
            if (_metaText != null)
            {
                _metaText.text = "Meta information";
            }
            OnCleared?.Invoke();
        }

        #endregion

        #region Events

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (_justDropped)
            {
                return;
            }

            if (CurrentState == States.DEFAULT)
            {
                SetImageSprite(SpriteSwapEvent.ON_HOVER_EMPTY);
                if (_iconImage != null)
                {
                    _iconImage.sprite = _onHoverSprite;
                }
            }
            else if (CurrentState == States.FILLED)
            {
                SetImageSprite(SpriteSwapEvent.ON_HOVER_FILLED);

                if (_iconImage != null)
                {
                    _iconImage.sprite = _onHoverFilledImage;
                }
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (_justDropped)
            {
                _justDropped = false;
                return;
            }
            if (CurrentState == States.DEFAULT)
            {
                SetImageSprite(SpriteSwapEvent.DEFAULT);
                if (_iconImage != null)
                {
                    _iconImage.sprite = _defaultSprite;
                }
            }
            else if (CurrentState == States.FILLED)
            {
                SetImageSprite(SpriteSwapEvent.FILLED);
                if (_iconImage != null)
                {
                    _iconImage.sprite = _currentDataSprite != null ? _currentDataSprite : _defaultSprite;
                }
            }
            else if (CurrentState == States.DECLINED_DROP)
            {
                if (_stateBeforeFailedDrop == States.DEFAULT)
                {
                    CurrentState = States.DEFAULT;
                    SetImageSprite(SpriteSwapEvent.DEFAULT);
                }
                else if(_stateBeforeFailedDrop == States.FILLED)
                {
                    CurrentState = States.FILLED;
                    SetImageSprite(SpriteSwapEvent.FILLED);
                }
                if (_iconImage != null)
                {
                    _iconImage.sprite = _spriteBeforeFailed == null ? _defaultSprite : _spriteBeforeFailed;
                }
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Debug.Log("OnClick");
            if (CurrentState == States.DEFAULT)
            {
                OnClick?.Invoke();
            }
            else if (CurrentState == States.FILLED)
            {
                OnFilledClick?.Invoke();
                ClearCurrentData();
            }
        }

        #endregion

        #region Drag & Drop

        public bool CanReceiveDrop(ListItemCardData data, PointerEventData eventData)
        {
            bool matching = data.Type.Any(type => SupportedCardDragTypes.Contains(type)) && data.ResourceType == ListCardResourceTypes.PLATFORM || data.ResourceType == ListCardResourceTypes.HOTSPOT;

            if (matching)
            {
                return true;
            }

            if (_iconImage != null)
            {
                _spriteBeforeFailed = _spriteBeforeFailed == null ? _currentDataSprite : _spriteBeforeFailed;
                _iconImage.sprite = _onHoverDragDeclineSprite;
            }

            SetImageSprite(SpriteSwapEvent.DECLINED_DROP);
            if (CurrentState == States.DEFAULT || CurrentState == States.FILLED)
            {
                _stateBeforeFailedDrop = CurrentState;
            }
            CurrentState = States.DECLINED_DROP;
            return false;
        }

        private async void ReturnToDefault()
        {
            await Task.Delay(3000);
            CurrentState = _stateBeforeFailedDrop;
            //for case canrecievedrop and drop call the same function before canrecieve has finished.
            if (_iconImage != null)
            {
                if (_currentDataSprite != null)
                {
                    _iconImage.sprite = _currentDataSprite != null ? _currentDataSprite : _defaultSprite;
                }
                else
                {
                    _iconImage.sprite = _defaultSprite;
                }
            }
            _helpTipBox.gameObject.SetActive(false);
        }

        public virtual void Drop(ListItemCardData data, PointerEventData eventData)
        {
            if (CanReceiveDrop(data, eventData))
            {
                CurrentState = States.FILLED;
                _stateBeforeFailedDrop = States.FILLED;
                _droppedItemType = data.Type[0];
                _currentDataName = data.Name;
                _currentDataID = data.ResourceID;
                _currentDataSprite = data.Icon;
                _currentMeta = data.Meta;
                _currentResourceType = Enum.GetName(data.ResourceType.GetType(), data.ResourceType);
                _justDropped = true;
                SetToCurrentValues();
                OnFilled?.Invoke(_currentDataName);
            }
            else
            {
                _helpTipBox.SetActive(true);
                _helpTipText.text = $"Cannot drop card of type {data.Type[0]} into a {SupportedCardDragTypes[0]} Dropbox.";
                if (_iconImage != null)
                {
                    _iconImage.sprite = _onHoverDragDeclineSprite;
                }
                ReturnToDefault();
            }
        }

        public void DropCanceled(ListItemCardData data, PointerEventData eventData)
        {
            CommonDropCanceled();
        }

        private void CommonDropCanceled()
        {
            if (_stateBeforeFailedDrop != States.FILLED)
            {
                SetImageSprite(SpriteSwapEvent.FILLED);
                if (_iconImage != null)
                {
                    _iconImage.sprite = _defaultSprite;
                }
            }
            else
            {
                SetImageSprite(SpriteSwapEvent.DEFAULT);
                if (_iconImage != null)
                {
                    _iconImage.sprite = _currentDataSprite != null ? _currentDataSprite : _defaultSprite;
                }
            }
        }

        public bool CanReceiveDrop(TreeNode<CTreeViewItem> data, PointerEventData eventData)
        {
            bool matching = data.Item.Types.Any(type => SupportedItemDropTypes.Contains(type));
            if (matching)
            {
                return true;
            }

            if (_iconImage != null)
            {
                _spriteBeforeFailed = _spriteBeforeFailed == null ? _currentDataSprite : _spriteBeforeFailed;
                _iconImage.sprite = _onHoverDragDeclineSprite;
            }
            SetImageSprite(SpriteSwapEvent.DECLINED_DROP);
            if (CurrentState == States.DEFAULT || CurrentState == States.FILLED)
            {
                _stateBeforeFailedDrop = CurrentState;
            }
            CurrentState = States.DECLINED_DROP;
            return false;
        }

        public void Drop(TreeNode<CTreeViewItem> data, PointerEventData eventData)
        {
            if (CanReceiveDrop(data, eventData))
            {
                CurrentState = States.FILLED;
                _stateBeforeFailedDrop = States.FILLED;

                _currentDataName = data.Item.Name;
                _currentDataID = data.Item.ItemID;
                _currentDataSprite = data.Item.Icon;
                _currentMeta = data.Item.Name;
                _droppedItemType = data.Item.Types[0];
                //check this ^
                _currentResourceType = data.Item.Types[0];
                _justDropped = true;
                SetToCurrentValues();
                OnFilled?.Invoke(_currentDataID);
            }
            else
            {
                _helpTipBox.SetActive(true);
                _helpTipText.text = $"Cannot drop item of type {data.Item.Types[0]} into a {SupportedItemDropTypes[0]} Dropbox.";
                if (_iconImage != null)
                {

                    _iconImage.sprite = _onHoverDragDeclineSprite;
                }
                ReturnToDefault();
            }
        }

        public void DropCanceled(TreeNode<CTreeViewItem> data, PointerEventData eventData)
        {
            CommonDropCanceled();
        }

        #endregion

        private void SetToCurrentValues()
        {
            if (_nameText != null)
            {
                _nameText.text = _currentDataName;
            }
            if (_iconImage != null)
            {
                if (_defaultSprite == null)
                {
                    _defaultSprite = _iconImage.sprite;
                }

                _iconImage.sprite = _currentDataSprite != null ? _currentDataSprite : _defaultSprite;
            }
            if (_resourceTypeText != null)
            {
                _resourceTypeText.text = _currentResourceType;
            }
            if (_metaText != null)
            {
                _metaText.text = _currentMeta;
            }
        }

        private void SetImageSprite(SpriteSwapEvent ssevent)
        {
            if (_buttons != null)
            {
                _buttons.SetActive(true);
            }
            foreach (var imageCollection in Images)
            {
                switch (ssevent)
                {
                    case SpriteSwapEvent.DEFAULT:
                        imageCollection.Image.sprite = imageCollection.DefaultImageSprite;
                        break;
                    case SpriteSwapEvent.FILLED:
                        imageCollection.Image.sprite = imageCollection.DefaultImageSprite;
                        if (_iconImage != null)
                        {
                            _iconImage.sprite = _defaultSprite;
                        }
                        break;
                    case SpriteSwapEvent.FAILED_DROP:
                        imageCollection.Image.sprite = imageCollection.OnHoverDragDeclineSprite;
                        break;
                    case SpriteSwapEvent.DECLINED_DROP:
                        if (_buttons != null)
                        {
                            _buttons.SetActive(false);
                        }
                        imageCollection.Image.sprite = imageCollection.OnHoverDragDeclineSprite;
                        break;
                    case SpriteSwapEvent.ON_HOVER_FILLED:
                        imageCollection.Image.sprite = imageCollection.OnHoverFilledImage;
                        break;
                    case SpriteSwapEvent.ON_HOVER_EMPTY:
                        imageCollection.Image.sprite = imageCollection.OnHoverSprite;
                        break;
                    case SpriteSwapEvent.ON_EXIT_HOVER:
                        if (_iconImage != null)
                        {
                            _iconImage.sprite = _defaultSprite;
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(ssevent), ssevent, null);
                }
            }
        }

        public void SetCurrentDateSprite(Sprite sprite)
        {
            _currentDataSprite = sprite;
            _iconImage.sprite = _currentDataSprite;
        }

        public ItemTypeCode GetDroppedItemType()
        { 
            return (ItemTypeCode)Enum.Parse(typeof(ItemTypeCode), _droppedItemType);
        }

        public bool IsNewDroppedItem()
        {
            if(_currentDataID == _currentDataName)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}