﻿using System;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

using TMPro;

using UIWidgets;
using UIWidgets.Styles;
using UIWidgets.TMProSupport;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents
{
    /// <summary>
    /// NEW UI WIDGETS Combobox class copy but using difference list view type.
    /// </summary>
    public class SearchComboBox : MonoBehaviour, ISubmitHandler, IStylable
    {
        [SerializeField]
        SearchList listView;

        /// <summary>
        /// Gets or sets the ListView.
        /// </summary>
        /// <value>ListView component.</value>
        public SearchList ListView
        {
            get { return listView; }

            set { SetListView(value); }
        }

        [SerializeField]
        Button toggleButton;

        /// <summary>
        /// Gets or sets the toggle button.
        /// </summary>
        /// <value>The toggle button.</value>
        public Button ToggleButton
        {
            get { return toggleButton; }

            set { SetToggleButton(value); }
        }

        /// <summary>
        /// Is Combobox.InputField editable?
        /// </summary>
        [SerializeField]
        protected bool editable = true;

        /// <summary>
        /// Gets or sets a value indicating whether this is editable and user can add new items.
        /// </summary>
        /// <value><c>true</c> if editable; otherwise, <c>false</c>.</value>
        public bool Editable
        {
            get { return editable; }

            set
            {
                editable = value;
                if (InputProxy != null)
                {
                    InputProxy.interactable = editable;
                }
            }
        }

        /// <summary>
        /// Proxy for InputField.
        /// Required to improve compatibility between different InputFields (like Unity.UI and TextMesh Pro versions).
        /// </summary>
        protected IInputFieldProxy InputProxy;

        /// <summary>
        /// Input field value.
        /// </summary>
        /// <value>The input value.</value>
        public string InputValue
        {
            get { return InputProxy.text; }

            set
            {
                if (InputValue != null)
                {
                    InputProxy.text = value;
                }
            }
        }

        /// <summary>
        /// Hide ListView on item select or deselect.
        /// </summary>
        [SerializeField]
        [Tooltip("Hide ListView on item select or deselect.")]
        public bool HideAfterItemToggle = true;

        /// <summary>
        /// Allow to add new items to list.
        /// </summary>
        [SerializeField]
        public bool AllowNewItems = true;

        /// <summary>
        /// Reset input if item not found and new items not allowed.
        /// </summary>
        [SerializeField]
        public bool ResetInput = true;

        /// <summary>
        /// OnSelect event.
        /// </summary>
        public ListViewEvent OnSelect = new ListViewEvent();

        Transform listViewCanvas;

        /// <summary>
        /// Canvas where ListView placed.
        /// </summary>
        protected Transform ListViewCanvas
        {
            get
            {
                if (listViewCanvas == null)
                {
                    listViewCanvas = Utilites.FindTopmostCanvas(listView.transform.parent);
                }

                return listViewCanvas;
            }
        }

        /// <summary>
        /// ListView parent.
        /// </summary>
        protected Transform ListViewParent;

        protected Vector2 _listViewLocalPositionParent;

        /// <summary>
        /// Raised when ListView opened.
        /// </summary>
        [SerializeField]
        public UnityEvent OnShowListView = new UnityEvent();

        /// <summary>
        /// Raised when ListView closed.
        /// </summary>
        [SerializeField]
        public UnityEvent OnHideListView = new UnityEvent();

        [NonSerialized]
        bool isComboboxInited;

        private RectTransform _rectTrans;

        private bool _isOpen;

        /// <summary>
        /// Start this instance.
        /// </summary>
        public virtual void Start()
        {
            Init();
        }

        /// <summary>
        /// Init this instance.
        /// </summary>
        public virtual void Init()
        {
            if (isComboboxInited)
            {
                return;
            }
            _rectTrans = listView.transform as RectTransform;
            _listViewLocalPositionParent = _rectTrans.anchoredPosition;

            isComboboxInited = true;

            InitInputFieldProxy();

            SetToggleButton(toggleButton);

            SetListView(listView);

            if (listView != null)
            {
                listView.gameObject.SetActive(true);
                listView.Init();
                listView.OnSelect.AddListener(ItemSelected);
                if (listView.SelectedIndex != -1)
                {
                    InputValue = listView.DataSource[listView.SelectedIndex].Name;
                }

                listView.gameObject.SetActive(false);
            }
        }

        /// <summary>
        /// Init InputFieldProxy.
        /// </summary>
        protected virtual void InitInputFieldProxy()
        {
            InputProxy = new InputFieldTMProProxy(GetComponent<TMP_InputField>());
            InputProxy.interactable = editable;
            InputProxy.onEndEdit.AddListener(InputItem);
        }

        /// <summary>
        /// Sets the list view.
        /// </summary>
        /// <param name="value">Value.</param>
        protected virtual void SetListView(SearchList value)
        {
            if (listView != null)
            {
                ListViewParent = null;

                listView.OnFocusOut.RemoveListener(OnFocusHideList);

                listView.onCancel.RemoveListener(OnListViewCancel);
                listView.onItemCancel.RemoveListener(OnListViewCancel);

                RemoveDeselectCallbacks();
            }

            listView = value;

            if (listView != null)
            {
                ListViewParent = listView.transform.parent;

                listView.OnFocusOut.AddListener(OnFocusHideList);

                listView.onCancel.AddListener(OnListViewCancel);
                listView.onItemCancel.AddListener(OnListViewCancel);

                AddDeselectCallbacks();
            }
        }

        /// <summary>
        /// Sets the toggle button.
        /// </summary>
        /// <param name="value">Value.</param>
        protected virtual void SetToggleButton(Button value)
        {
            if (toggleButton != null)
            {
                toggleButton.onClick.RemoveListener(ToggleList);
            }

            toggleButton = value;

            if (toggleButton != null)
            {
                toggleButton.onClick.AddListener(ToggleList);
            }
        }

        /// <summary>
        /// Clear ListView and selected item.
        /// </summary>
        public virtual void Clear()
        {
            listView.DataSource.Clear();
            InputValue = string.Empty;
        }

        /// <summary>
        /// Toggles the list visibility.
        /// </summary>
        public virtual void ToggleList()
        {
            if (listView == null)
            {
                return;
            }

            if (listView.gameObject.activeSelf)
            {
                HideList();
            }
            else
            {
                ShowList();
            }
        }

        /// <summary>
        /// The modal key.
        /// </summary>
        protected int? ModalKey;

        /// <summary>
        /// Shows the list.
        /// </summary>
        public virtual void ShowList()
        {
            if (_isOpen)
            {
                return;
            }
            _isOpen = true;
            if (listView == null)
            {
                return;
            }

            listView.gameObject.SetActive(true);

            ModalKey = ModalHelper.Open(this, null, new Color(0, 0, 0, 0f), HideList);

            if (ListViewCanvas != null)
            {
                ListViewParent = listView.transform.parent;

                listView.transform.SetParent(ListViewCanvas);
            }

            if (listView.Layout != null)
            {
                listView.Layout.UpdateLayout();
            }

            OnShowListView.Invoke();
        }

        /// <summary>
        /// Hides the list.
        /// </summary>
        public virtual void HideList()
        {
            if (!_isOpen)
            {
                return;
            }
            _isOpen = false;
            {
                if (ModalKey != null)
                {
                    ModalHelper.Close((int)ModalKey);
                    ModalKey = null;
                }
            }

            if (listView == null)
            {
                return;
            }

            if (ListViewCanvas != null)
            {
                listView.transform.SetParent(ListViewParent);
                _rectTrans.anchoredPosition = _listViewLocalPositionParent;
            }

            listView.gameObject.SetActive(false);
            OnHideListView.Invoke();
        }

        /// <summary>
        /// The children deselect.
        /// </summary>
        protected List<SelectListener> childrenDeselect = new List<SelectListener>();

        /// <summary>
        /// Hide list when focus lost.
        /// </summary>
        /// <param name="eventData">Event data.</param>
        protected void OnFocusHideList(BaseEventData eventData)
        {
            if (eventData.selectedObject == gameObject)
            {
                return;
            }

            var ev_item = eventData as ListViewItemEventData;
            if (ev_item != null)
            {
                if (ev_item.NewSelectedObject != null)
                {
                    SetChildDeselectListener(ev_item.NewSelectedObject);
                }

                return;
            }

            var ev = eventData as PointerEventData;
            if (ev == null)
            {
                HideList();
                return;
            }

            var go = ev.pointerPressRaycast.gameObject;
            if (go == null)
            {
                return;
            }

            if (go.Equals(toggleButton.gameObject))
            {
                return;
            }

            if (go.transform.IsChildOf(listView.transform))
            {
                SetChildDeselectListener(go);
                return;
            }

            HideList();
        }

        /// <summary>
        /// Sets the child deselect listener.
        /// </summary>
        /// <param name="child">Child.</param>
        protected void SetChildDeselectListener(GameObject child)
        {
            var deselectListener = GetDeselectListener(child);
            if (!childrenDeselect.Contains(deselectListener))
            {
                deselectListener.onDeselect.AddListener(OnFocusHideList);
                childrenDeselect.Add(deselectListener);
            }
        }

        /// <summary>
        /// Gets the deselect listener.
        /// </summary>
        /// <returns>The deselect listener.</returns>
        /// <param name="go">Go.</param>
        protected static SelectListener GetDeselectListener(GameObject go)
        {
            return Utilites.GetOrAddComponent<SelectListener>(go);
        }

        /// <summary>
        /// Adds the deselect callbacks.
        /// </summary>
        protected void AddDeselectCallbacks()
        {
            if (listView.ScrollRect == null)
            {
                return;
            }

            if (listView.ScrollRect.verticalScrollbar == null)
            {
                return;
            }

            var scrollbar = listView.ScrollRect.verticalScrollbar.gameObject;
            var deselectListener = GetDeselectListener(scrollbar);

            deselectListener.onDeselect.AddListener(OnFocusHideList);
            childrenDeselect.Add(deselectListener);
        }

        /// <summary>
        /// Removes the deselect callbacks.
        /// </summary>
        protected void RemoveDeselectCallbacks()
        {
            childrenDeselect.ForEach(RemoveDeselectCallback);
            childrenDeselect.Clear();
        }

        /// <summary>
        /// Removes the deselect callback.
        /// </summary>
        /// <param name="listener">Listener.</param>
        protected void RemoveDeselectCallback(SelectListener listener)
        {
            if (listener != null)
            {
                listener.onDeselect.RemoveListener(OnFocusHideList);
            }
        }

        /// <summary>
        /// Set the specified item.
        /// </summary>
        /// <param name="item">Item.</param>
        /// <param name="allowDuplicate">If set to <c>true</c> allow duplicate.</param>
        /// <returns>Index of item.</returns>
        public virtual int Set(string item, bool allowDuplicate = true)
        {
            return listView.Set(listView.DataSource.Find(a => a.Name == item), allowDuplicate);
        }

        private void ItemSelected(int index, SearchListViewItem item)
        {
            SelectItem(index, listView.GetItemComponent(index).Text.text);
        }

        /// <summary>
        /// Selects the item.
        /// </summary>
        /// <param name="index">Index.</param>
        /// <param name="text">Text.</param>
        protected virtual void SelectItem(int index, string text)
        {
            InputValue = text;

            if (HideAfterItemToggle)
            {
                HideList();
            }

            OnSelect.Invoke(index, text);
        }

        /// <summary>
        /// Work with input.
        /// </summary>
        /// <param name="item">Item.</param>
        protected virtual void InputItem(string item)
        {
            if (!editable)
            {
                return;
            }

            if (string.IsNullOrEmpty(item))
            {
                return;
            }

            if (listView.DataSource.All(a => a.Name != item))
            {
                if (AllowNewItems)
                {
                    var index = listView.Add(new SearchListStringItem(item, true));
                    listView.Select(index);
                }
                else if (ResetInput)
                {
                    InputProxy.text = string.Empty;
                }
            }
        }

        /// <summary>
        /// This function is called when the MonoBehaviour will be destroyed.
        /// </summary>
        protected virtual void OnDestroy()
        {
            ListView = null;
            ToggleButton = null;
            if (InputProxy != null)
            {
                InputProxy.onEndEdit.RemoveListener(InputItem);
            }
        }

        /// <summary>
        /// Hide list view.
        /// </summary>
        protected void OnListViewCancel()
        {
            HideList();
        }

        /// <summary>
        /// Called when OnSubmit event occurs.
        /// </summary>
        /// <param name="eventData">Event data.</param>
        public virtual void OnSubmit(BaseEventData eventData)
        {
            ShowList();
        }

        #region IStylable implementation

        /// <summary>
        /// Set the specified style.
        /// </summary>
        /// <returns><c>true</c>, if style was set for children gameobjects, <c>false</c> otherwise.</returns>
        /// <param name="style">Style data.</param>
        public virtual bool SetStyle(Style style)
        {
            style.Combobox.SingleInputBackground.ApplyTo(GetComponent<Image>());

            if (toggleButton != null)
            {
                style.Combobox.ToggleButton.ApplyTo(toggleButton.GetComponent<Image>());
            }

            var input = GetComponent<InputField>();
            if (input != null)
            {
                style.Combobox.Input.ApplyTo(input.textComponent, true);
                if (input.placeholder != null)
                {
                    style.Combobox.Placeholder.ApplyTo(input.placeholder.gameObject, true);
                }
            }

            if (listView != null)
            {
                listView.SetStyle(style);
            }

            return true;
        }

        #endregion
    }
}