namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents
{
    public interface ISearchable
    {
        void ApplyFilterTerm(string filter);

        void ClearFilterTerm();
    }
}