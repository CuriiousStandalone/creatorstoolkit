﻿using UnityEngine;

using TMPro;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class CLabel : BaseUIElement
    {
        private TextMeshProUGUI _textMeshReference;

        private void Awake()
        {
            _textMeshReference = GetComponent<TextMeshProUGUI>();
        }

        //Map to public properties of TextMeshProUGUI here.
        public string Text => _textMeshReference.text;
        public Color Color => _textMeshReference.color;
        public float FontSize => _textMeshReference.fontSize;
        public TextAlignmentOptions Alignment => _textMeshReference.alignment;
        public FontStyles FontStyle => _textMeshReference.fontStyle;
        
        protected override void Init()
        {
            
        }
    }
}