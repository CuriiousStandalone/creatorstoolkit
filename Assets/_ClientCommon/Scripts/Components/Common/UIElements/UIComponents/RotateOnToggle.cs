using System;
using System.Collections;

using UnityEngine;

using UIWidgets;

using DG.Tweening;

/// <summary>
/// Used with accordions to Rotate the arrows.
/// </summary>
public class RotateOnToggle : MonoBehaviour
{
    public Accordion Accordion;

    public float Angle = 90f;

    public void Start()
    {
        if (Accordion == null)
        {
            Debug.Log("Couldn't find Accordion reference");
            enabled = false;
        }
        else
        {
            Accordion.AnimationOpen = AnimationOpen;
            Accordion.AnimationClose = AnimationClose;
        }
    }

    private IEnumerator AnimationClose(RectTransform p1, float p2, bool p3, bool p4, Action p5)
    {
        StartCoroutine(Animations.Collapse(p1, p2, p3, p4, p5));
        transform.DORotate(Vector3.forward * Angle, p2);
        yield return new WaitForSeconds(p2);
    }

    private IEnumerator AnimationOpen(RectTransform p1, float p2, bool p3, bool p4, Action p5)
    {
        StartCoroutine(Animations.Open(p1, p2, p3, p4, p5));
        transform.DORotate(Vector3.forward * 0, p2);
        yield return new WaitForSeconds(p2);
    }
}