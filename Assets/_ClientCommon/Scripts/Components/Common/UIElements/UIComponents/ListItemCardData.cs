﻿using System;
using System.IO;
using System.ComponentModel;

using UnityEngine;

using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

/// <summary>
/// Copy of IconItemDescription but adds a few required values etc
/// </summary>
[Serializable]
public class ListItemCardData : INotifyPropertyChanged
{
    [SerializeField]
    private Sprite _icon;

    /// <summary>
    /// The icon.
    /// </summary>
    public Sprite Icon
    {
        get { return _icon; }

        set
        {
            _icon = value;
            Changed("Icon");
        }
    }

    [SerializeField]
    private string _name;

    /// <summary>
    /// The name.
    /// </summary>
    public string Name
    {
        get { return _name; }

        set
        {
            _name = value;
            Changed("Name");
        }
    }

    [SerializeField]
    private string _meta;

    public string Meta
    {
        get { return _meta; }
        set { _meta = value; }
    }

    [SerializeField]
    private string[] _type;

    public string[] Type
    {
        get { return _type; }
        set { _type = value; }
    }

    [NonSerialized]
    string _localizedName;

    /// <summary>
    /// The localized name.
    /// </summary>
    public string LocalizedName
    {
        get { return _localizedName; }

        set
        {
            _localizedName = value;
            Changed("LocalizedName");
        }
    }

    [SerializeField]
    private int _value;

    /// <summary>
    /// The value.
    /// </summary>
    public int Value
    {
        get { return _value; }

        set
        {
            _value = value;
            Changed("Value");
        }
    }

    public ListCardResourceTypes ResourceType
    {
        get
        {
            return _resourceType;
        }
        set
        {
            _resourceType = value;
            Changed("ResourceType");
        }
    }

    [HideInInspector]
    private ListCardResourceTypes _resourceType;

    [HideInInspector]
    public string ResourceID;

    public async void LoadIcon(string inputPath, string outputPath)
    {
        if (!Directory.Exists(Settings.TempDownloadPath))
        {
            Directory.CreateDirectory(Settings.TempDownloadPath);
        }

        ItemTypeCode itemType = (ItemTypeCode)Enum.Parse(typeof(ItemTypeCode), DisplayToEnumString(Type[0]));

        string thumbnailPath = "";

        if (itemType == ItemTypeCode.PANORAMA_VIDEO || itemType == ItemTypeCode.STEREO_PANORAMA_VIDEO)
        {
            thumbnailPath = await FFMpegUtility.GenerateThumbnailOfVideo(inputPath, outputPath);
        }
        else if(itemType == ItemTypeCode.PANORAMA || itemType == ItemTypeCode.IMAGE)
        {
            thumbnailPath = await FFMpegUtility.GenerateThumbnailOfImage(inputPath, outputPath);
        }

        if(!string.IsNullOrEmpty(thumbnailPath))
        {
            byte[] fileData = File.ReadAllBytes(thumbnailPath);
            Texture2D tex = new Texture2D(2, 2);
            tex.LoadImage(fileData);
            Icon = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2());
        }
    }

    private static string DisplayToEnumString(string input)
    {
        return input.ToUpperInvariant().Replace(" ", "_");
    }

    /// <summary>
    /// Occurs when a property value changes.
    /// </summary>
    public event PropertyChangedEventHandler PropertyChanged = (x, y) => { };

    /// <summary>
    /// Raise PropertyChanged event.
    /// </summary>
    /// <param name="propertyName">Property name.</param>
    protected void Changed(string propertyName)
    {
        PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
    }
}