﻿using UnityEngine.Events;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents
{
    /// <summary>
    /// Copy class of ListViewBaseEvent.
    /// </summary>
    public class SearchListViewBaseEvent : UnityEvent<int, SearchListViewItem>
    {
    }
}