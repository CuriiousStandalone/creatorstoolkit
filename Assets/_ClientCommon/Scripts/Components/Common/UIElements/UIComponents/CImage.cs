﻿using System;

using UnityEngine;
using UnityEngine.UI;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents
{
    /// <summary>
    /// PlaceHolder for now
    /// </summary>
    public class CImage : BaseUIElement
    {
        private Image _image;

        [SerializeField]
        private Sprite _sprite;
        public Color Color
        {
            get => _image.color;
            set => _image.color = value;
        }

        public bool RaycastTarget
        {
            get => _image.raycastTarget;
            set => _image.raycastTarget = value;
        }

        public Sprite Sprite
        {
            get => _image.sprite;
            set => _image.sprite = value;
        }

        public Sprite OverrideSprite
        {
            get => _image.overrideSprite;
            set => _image.overrideSprite = value;
        }

        public Image.Type Type
        {
            get => _image.type;
            set => _image.type = value;
        }

        public bool PreserveAspect
        {
            get => _image.preserveAspect;
            set => _image.preserveAspect = value;
        }

        public bool FillCenter
        {
            get => _image.fillCenter;
            set => _image.fillCenter = value;
        }

        public Image.FillMethod fFllMethod
        {
            get => _image.fillMethod;
            set => _image.fillMethod = value;
        }

        public float FillAmount
        {
            get => _image.fillAmount;
            set => _image.fillAmount = value;
        }

        public bool FillClockwise
        {
            get => _image.fillClockwise;
            set => _image.fillClockwise = value;
        }

        public int FillOrigin
        {
            get => _image.fillOrigin;
            set => _image.fillOrigin = value;
        }

        public Texture MainTexture => _image.mainTexture;

        private void Start()
        {
            _image = GetComponent<Image>();
            Sprite = _sprite;
        }

        protected override void Init()
        {
        }
        
    }
}