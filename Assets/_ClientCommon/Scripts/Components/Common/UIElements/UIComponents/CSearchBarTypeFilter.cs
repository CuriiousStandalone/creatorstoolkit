﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

using UnityEngine;
using UnityEngine.Serialization;

using TMPro;

using Debug = UnityEngine.Debug;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents
{
    /// <summary>
    /// For search bar behaviours and calling on ISearchableTypeFilter.
    /// To create a new instance of seach bar for a new collection extend this class and make
    /// T the category/non search term based filter.
    ///
    /// See ISearchable for how to accept Searches within a collection.  
    /// </summary>
    public class CSearchBarTypeFilter : BaseUIElement
    {
        [SerializeField]
        protected ISearchableTypeFilter[] _filterableCollection;

        [SerializeField]
        protected GameObject[] _filterableCollectionGameObject;

        [SerializeField]
        private SearchComboBox _combobox;

        [SerializeField]
        private GameObject _comboboxTextArea;

        [SerializeField]
        private GameObject _comboboxTextComponent;

        [SerializeField]
        private GameObject _comboboxTextOverlay;

        [SerializeField]
        private TMP_InputField _inputField;

        public bool FirstItemClearsFilter = true;

        [FormerlySerializedAs("StartingCollection")]
        [Space]
        [SerializeField]
        private SearchListStringItem[] _startingCollection;

        [SerializeField]
        [Space]
        private DisplayNameConversion[] _displayNameConversionCollection;

        private int _currentComboBoxValue;

        [Serializable]
        public struct DisplayNameConversion
        {
            public string InternalName;
            public string DisplayName;
        }

        private string[] _currentFilter;

        //also denotes if default item is in use.
        private bool _filterEnabled;
        private string _currentTerm = "";
        private const string DEFAULT_TYPE = "All Items";
        private bool _inited;

        private const bool DEBUG = false;

        private bool _currentTermIsDefault
        {
            get { return string.IsNullOrEmpty(_currentTerm) || _currentTerm.Equals(DEFAULT_TYPE); }
        }

        //To be removed.
        private IEnumerator Start()
        {
            yield return new WaitForEndOfFrame();
            Init();
        }

        public void InitNow()
        {
            Init();
        }

        protected override void Init()
        {
            if (_inited)
            {
                return;
            }
            _inited = true;
            UpdateListeners(true);
            _combobox.HideAfterItemToggle = true;
            _filterableCollection = _filterableCollectionGameObject.Select(gameObj => gameObj.GetComponent<ISearchableTypeFilter>()).ToArray();
            foreach (SearchListStringItem item in _startingCollection)
            {
                item.Name = ConvertToDisplay(item.Name);
            }
            _combobox.ListView.DataSource.AddRange(_startingCollection);
            if (_combobox.ListView.DataSource.Any(a => a.DefaultInteractable))
            {
                int selectIndex = _combobox.ListView.DataSource.IndexOf(_combobox.ListView.DataSource.First(a => a.DefaultInteractable));
                _currentComboBoxValue = selectIndex;
                _combobox.ListView.Select(selectIndex);
            }
        }

        private string ConvertToDisplay(string internalName)
        {
            if (_displayNameConversionCollection.Any(a => a.InternalName.Equals(internalName, StringComparison.OrdinalIgnoreCase)))
            {
                return _displayNameConversionCollection.First(a => a.InternalName.Equals(internalName, StringComparison.OrdinalIgnoreCase)).DisplayName;
            }
            return internalName;
        }

        private string GetInternalName(string displayName)
        {
            if (_displayNameConversionCollection.Any(a => a.DisplayName.Equals(displayName, StringComparison.OrdinalIgnoreCase)))
            {
                return _displayNameConversionCollection.First(a => a.DisplayName.Equals(displayName, StringComparison.OrdinalIgnoreCase)).InternalName;
            }
            return displayName;
        }

        #region Public

        /// <summary>
        /// Replace the collection to avoid duplicates
        /// </summary>
        /// <param name="types">type filters.</param>
        public void ReplaceTypeCollection(string[] types)
        {
            _currentComboBoxValue = _combobox.ListView.SelectedIndex;
            //add starters
            List<SearchListStringItem> newList = new List<SearchListStringItem>(_startingCollection);
            //add elements that aren't included in types.
            newList.AddRange(_combobox.ListView.DataSource.Where(a => !types.Contains(GetInternalName(a.Name))
                                                                      && !newList.Contains(a)
                                                                      && !_startingCollection.Contains(a)));
            foreach (SearchListStringItem stringItem in newList)
            {
                stringItem.Name = ConvertToDisplay(stringItem.Name);
            }
            _combobox.ListView.DataSource.Clear();
            _combobox.ListView.DataSource.AddRange(newList);
            //add new items
            _combobox.ListView.DataSource.AddRange(Array.ConvertAll(types, input => new SearchListStringItem(ConvertToDisplay(input), true)));
            _combobox.ListView.Select(_currentComboBoxValue);
        }

        /// <summary>
        /// Add types to the filter list.
        /// </summary>
        /// <param name="types">Array of strings to be used as type.</param>
        public void AddTypeRange(string[] types)
        {
            _combobox.ListView.DataSource.AddRange(Array.ConvertAll(types, input => new SearchListStringItem(ConvertToDisplay(input), true)));
        }

        /// <summary>
        /// Add a type filter to the combobox list
        /// </summary>
        /// <param name="type">type filter</param>
        public void AddType(string type)
        {
            if (!_combobox.ListView.DataSource.Any(a => a.Name.Equals(ConvertToDisplay(type), StringComparison.OrdinalIgnoreCase)))
            {
                _combobox.ListView.DataSource.Add(new SearchListStringItem(ConvertToDisplay(type), true));
            }
        }

        /// <summary>
        /// change the currently Searched term
        /// "" for none
        /// </summary>
        public void SetSearchTerm(string inputValue)
        {
            if (_filterableCollection != null)
            {
                _currentTerm = inputValue;
                if (_inputField.text != inputValue)
                {
                    _inputField.text = inputValue;
                }

                ApplyFilter();
            }
        }

        /// <summary>
        /// Change the currently selected filter.
        /// </summary>
        public void SetFilter(string[] filter)
        {
            if (_filterableCollection != null)
            {
                _currentFilter = Array.ConvertAll(filter, GetInternalName);
                foreach (string filterName in _currentFilter)
                {
                    SearchStringListComponent searchStringListComponent = _combobox.ListView.GetVisibleComponents().Find(component => component.Text.text == filterName);
                    if (searchStringListComponent == null)
                    {
                        continue;
                    }
                    int index = searchStringListComponent.Index;
                    if (index == -1)
                    {
                        index = searchStringListComponent.Index;
                    }

                    if (index != -1)
                    {
                        _combobox.ListView.SelectedIndex = index;
                        break;
                    }
                }

                if (_currentTermIsDefault)
                {
                    _inputField.placeholder.gameObject.SetActive(false);
                    _comboboxTextArea.gameObject.SetActive(true);
                }

                _filterEnabled = true;

                ApplyFilter();
            }
        }

        public void ResetFilter()
        {
            CancelTypeFilter();
            _combobox.ListView.Select(0);
        }

        #endregion

        #region Debug

        private static void Log(object value)
        {
            if (DEBUG)
            {
                Debug.Log(value);
            }
        }

        private static void LogMethodName([CallerMemberName] string memberName = "")
        {
            if (DEBUG)
            {
                Debug.Log(memberName);
            }
        }

        #endregion

        #region Events

        private void OnComboBoxDeselect()
        {
            LogMethodName();
        }

        private void InputFieldDeselect(string arg0)
        {
            LogMethodName();
            if (_currentTermIsDefault)
            {
                _comboboxTextArea.SetActive(true);
            }
        }

        private void InputFieldSelected(string arg0)
        {
            LogMethodName();
            _inputField.textComponent.gameObject.SetActive(true);
            _comboboxTextArea.SetActive(false);
        }

        /// <summary>
        /// Combo box value changed
        /// </summary>
        /// <param name="index"></param>
        /// <param name="value"></param>
        /// <exception cref="Exception">Exception: Enum Filter Parse failures.</exception>
        private void OnFilterTypeChanged(int index, string value)
        {
            LogMethodName();
            Debug.Log($"{index} + {value}");

            if (!string.IsNullOrEmpty(value))
            {
                if (index == 0 && FirstItemClearsFilter)
                {
                    CancelTypeFilter();
                    return;
                }

                SetFilter(new[] {GetInternalName(value)});
            }
        }

        /// <summary>
        /// InputField value changed.
        /// </summary>
        /// <param name="inputValue"></param>
        private void OnFilterChanged(string inputValue)
        {
            LogMethodName();
            SetSearchTerm(inputValue);
        }

        private void OnOpen()
        {
            _combobox.ListView.ComponentsColoring(true);
            _combobox.ListView.SelectColoring(_combobox.ListView.GetItemComponent(_combobox.ListView.SelectedIndex));
        }

        //triggered by event trigger not unity internal events.
        public void OnPointerEnter()
        {
            if (_currentTermIsDefault)
            {
                _comboboxTextOverlay.SetActive(true);
                _comboboxTextComponent.SetActive(false);
            }
        }

        public void OnPointerExit()
        {
            _comboboxTextComponent.SetActive(true);
            _comboboxTextOverlay.SetActive(false);
        }

        private void OnDestroy()
        {
            UpdateListeners(false);
        }

        #endregion

        #region Search

        /// <summary>
        /// Apply the current Filter settings.
        /// </summary>
        private void ApplyFilter()
        {
            if (_filterableCollection != null)
            {
                foreach (ISearchableTypeFilter searchable in _filterableCollection)
                {
                    if (_filterEnabled)
                    {
                        searchable?.ApplyFilter(_currentTerm, _currentFilter);
                    }
                    else
                    {
                        CancelTypeFilter();
                        searchable?.ApplyFilterTerm(_currentTerm);
                    }
                }
            }
        }

        /// <summary>
        /// Cancel the type filter
        /// if a search term is in use then leave the term filter intact.
        /// </summary>
        private void CancelTypeFilter()
        {
            if (_filterableCollection != null)
            {
                _filterEnabled = false;
                foreach (ISearchableTypeFilter searchable in _filterableCollection)
                {
                    searchable?.CancelTypeFilter(_currentTerm);
                }
            }
        }

        /// <summary>
        /// Clear the current Search term.
        /// Should not effect the current type filter.
        /// </summary>
        private void ClearSearchTerm()
        {
            if (_filterableCollection != null)
            {
                foreach (ISearchableTypeFilter searchable in _filterableCollection)
                {
                    searchable?.ClearFilterTerm();
                }
            }
        }

        #endregion

        private void UpdateListeners(bool value)
        {
            if (value)
            {
                if (_combobox != null)
                {
                    _combobox.OnSelect.AddListener(OnFilterTypeChanged);
                    _combobox.OnHideListView.AddListener(OnComboBoxDeselect);
                    _combobox.OnShowListView.AddListener(OnOpen);
                }

                if (_inputField != null)
                {
                    _inputField.onValueChanged.AddListener(OnFilterChanged);
                    _inputField.onSelect.AddListener(InputFieldSelected);
                    _inputField.onDeselect.AddListener(InputFieldDeselect);
                }
            }
            else
            {
                if (_combobox != null)
                {
                    _combobox.OnSelect.RemoveListener(OnFilterTypeChanged);
                    _combobox.OnHideListView.RemoveListener(OnComboBoxDeselect);
                    _combobox.OnShowListView.RemoveListener(OnOpen);
                }

                if (_inputField != null)
                {
                    _inputField.onValueChanged.RemoveListener(OnFilterChanged);
                    _inputField.onSelect.RemoveListener(InputFieldSelected);
                    _inputField.onDeselect.RemoveListener(InputFieldDeselect);
                }
            }
        }
    }
}