﻿using System;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents
{
    /// <summary>
    /// Data item type for the Searchbar Combobox List
    /// </summary>
    [Serializable]
    public class SearchListStringItem
    {
        public string Name;
        public bool DefaultInteractable;

        public SearchListStringItem(string name, bool interactable)
        {
            Name = name;
            DefaultInteractable = interactable;
        }
    }
}