﻿using UIWidgets;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents
{
    using System;
    using System.ComponentModel;

    using UnityEngine;
    using UnityEngine.Serialization;

    /// <summary>
    /// Tree view item.
    /// </summary>
    [Serializable]
    public class CTreeViewItem : TreeViewItem
    {
        /// <summary>
        /// The name.
        /// </summary>
        [SerializeField]
        string itemID;

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string ItemID
        {
            get { return itemID; }

            set
            {
                itemID = value;
                Changed("ItemID");
            }
        }

        [SerializeField]
        string[] types;

        /// <summary>
        /// Gets or sets the types.
        /// </summary>
        /// <value>The types</value>
        public string[] Types
        {
            get { return types; }

            set
            {
                types = value;
                Changed("Types");
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UIWidgets.TreeViewItem"/> class.
        /// </summary>
        /// <param name="itemName">Item name.</param>
        /// <param name="itemIcon">Item icon.</param>
        public CTreeViewItem(string itemName, string id, string[] types, Sprite itemIcon = null) : base(itemName, itemIcon)
        {
            itemID = id;
            this.types = types;
        }
    }
}