﻿namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents
{
    /// <summary>
    /// Concrete type for SearchBar combobox list.
    /// </summary>
    public class SearchList : SearchListViewCustom<SearchStringListComponent, SearchListStringItem>
    {
    }
}