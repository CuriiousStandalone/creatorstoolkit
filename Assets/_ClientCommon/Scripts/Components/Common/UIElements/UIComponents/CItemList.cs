﻿using System;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

using UIWidgets;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents
{
    public class CItemList : BaseUIElement, ISearchableTypeFilter
    {
        [SerializeField]
        private CardItemList _cardItemListItem;

        [FormerlySerializedAs("DataSource")]
        [SerializeField]
        private ListItemCardData[] _dataSource;

        private List<ListItemCardData> _dataSourceList;

        private bool _inited;

        [SerializeField]
        private bool _isSortFilter = false;

        public Action<string, string[]> SortFilterFunction;

        public enum SortTypes
        {
            NONE,
            DATA,
            META,
        }

        // To be removed
        public void Start()
        {
            Init();
        }

        public void InitNow()
        {
            Init();
        }

        public void ClearList()
        {
            _cardItemListItem.Clear();
            Array.Clear(_dataSource, 0, _dataSource.Length);
            _dataSourceList.Clear();
        }

        protected override void Init()
        {
            if (_inited)
            {
                return;
            }
            _inited = true;
            _dataSourceList = new List<ListItemCardData>(_dataSource);
            _cardItemListItem = GetComponent<CardItemList>();
            _cardItemListItem.DataSource = new ObservableList<ListItemCardData>(_dataSourceList);
        }

        #region Search

        public void ApplyFilterTerm(string filter)
        {
            if (filter == "")
            {
                ClearFilterTerm();
                gameObject.SetActive(true);
                return;
            }

            bool anyActive = false;
            _cardItemListItem.DataSource = new ObservableList<ListItemCardData>(_dataSourceList);
            ObservableList<ListItemCardData> list = _cardItemListItem.DataSource;
            ObservableList<ListItemCardData> removeList = new ObservableList<ListItemCardData>();

            for (int index = 0; index < list.Count; index++)
            {
                var cardData = list[index];
                bool active = cardData.Name.ToLower().Contains(filter.ToLower());
                anyActive |= active;

                if (!active)
                {
                    removeList.Add(cardData);
                }
            }

            list.RemoveAll(data => removeList.Contains(data));
            gameObject.SetActive(anyActive);
        }

        public void ClearFilterTerm()
        {
            _cardItemListItem.DataSource = new ObservableList<ListItemCardData>(_dataSourceList);
        }

        public void ApplyFilter(string filter, string[] typeFilter)
        {
            if(_isSortFilter)
            {
                SortFilterFunction.Invoke(filter, typeFilter);
                return;
            }

            _cardItemListItem.DataSource = new ObservableList<ListItemCardData>(_dataSourceList);
            ObservableList<ListItemCardData> list = _cardItemListItem.DataSource;
            ObservableList<ListItemCardData> removeList = new ObservableList<ListItemCardData>();
            bool anyActive = false;

            for (int index = 0; index < list.Count; index++)
            {
                var cardData = list[index];

                bool termMatches = cardData.Name.ToLower().Contains(filter.ToLower()) || string.IsNullOrEmpty(filter);
                bool typeMatches = false;

                foreach (string type in  cardData.Type)
                {
                    typeMatches |= typeFilter.Any(a => a.Equals(type, StringComparison.OrdinalIgnoreCase));
                }

                anyActive |= (termMatches && typeMatches);

                if (!termMatches || !typeMatches)
                {
                    removeList.Add(cardData);
                }
            }
            list.RemoveAll(data => removeList.Contains(data));
            gameObject.SetActive(anyActive);
        }

        public void ApplyCustomFilter(string filter, int value, SortTypes sortType)
        {
            _cardItemListItem.DataSource = new ObservableList<ListItemCardData>(_dataSourceList);
            ObservableList<ListItemCardData> list = _cardItemListItem.DataSource;
            ObservableList<ListItemCardData> removeList = new ObservableList<ListItemCardData>();
            bool anyActive = false;

            for (int index = 0; index < list.Count; index++)
            {
                var cardData = list[index];

                bool termMatches = cardData.Name.ToLower().Contains(filter.ToLower()) || string.IsNullOrEmpty(filter);
                bool typeMatches = false;

                if(value == cardData.Value)
                {
                    removeList.Add(cardData);
                }
            }

            list.RemoveAll(data => removeList.Contains(data));

            if (sortType == SortTypes.DATA)
            {
                list.Sort(_comparisonData);
            }
            else if(sortType == SortTypes.META)
            {
                list.Sort(_comparisonMeta);
            }

            gameObject.SetActive(true);
        }

        private Comparison<ListItemCardData> _comparisonMeta = (x, y) =>
        {
            return DateTime.Parse(y.Meta).CompareTo(DateTime.Parse(x.Meta));
        };

        private Comparison<ListItemCardData> _comparisonData = (x, y) =>
        {
            return x.Name.CompareTo(y.Name);
        };

        public void CancelTypeFilter(string currentTerm)
        {
            ApplyFilterTerm(currentTerm);
        }

        #endregion

        #region Getters

        /// <summary>
        /// Replaces the current collection with a new list.
        /// </summary>
        /// <param name="newCollection">collection to become.</param>
        public void ReplaceCollection(IEnumerable<ListItemCardData> newCollection)
        {
            if (_dataSourceList == null)
            {
                _dataSourceList = new List<ListItemCardData>();
            }

            IEnumerable<ListItemCardData> newCardData = newCollection.ToList();
            IEnumerable<ListItemCardData> listWithoutDuplicates = _dataSourceList.Where(item => !newCardData.Any(newItem => newItem.Name.Equals(item.Name))).ToList();

            _dataSourceList = new List<ListItemCardData>(listWithoutDuplicates);
            _dataSourceList.AddRange(newCardData);
            _cardItemListItem.DataSource = new ObservableList<ListItemCardData>(_dataSourceList);
        }

        /// <summary>
        /// Adds a range to the collection
        /// </summary>
        /// <param name="range">items to add.</param>
        public void AddRange(IEnumerable<ListItemCardData> range)
        {
            IEnumerable<ListItemCardData> newRange = range.ToList();
            _dataSourceList.AddRange(newRange);
            _cardItemListItem.DataSource.AddRange(newRange);
        }

        /// <summary>
        /// Adds a single item to the list.
        /// </summary>
        /// <param name="item">item to add.</param>
        public void AddItem(ListItemCardData item)
        {
            _dataSourceList.Add(item);
            _cardItemListItem.DataSource.Add(item);
        }

        /// <summary>
        /// Removes and item from the list
        /// </summary>
        /// <param name="itemIdentifier">name or id of item to remove.</param>
        public void RemoveItem(string itemIdentifier)
        {
            var itemToBeRemoved = _cardItemListItem.GetVisibleComponents().FirstOrDefault(comp => comp.Item.ResourceID == itemIdentifier);

            if (itemToBeRemoved != null)
            {
                _cardItemListItem.Remove(itemToBeRemoved.Index);
            }

            _dataSourceList.Remove(GetData(itemIdentifier));
        }

        /// <summary>
        /// Find items with the same name.
        /// </summary>
        /// <param name="itemIdentifier">name to search for.</param>
        /// <returns></returns>
        public ListItemCardData GetData(string itemIdentifier)
        {
            return _dataSourceList.Find(data => data.Name == itemIdentifier || data.ResourceID == itemIdentifier);
        }

        /// <summary>
        /// Gets component that an item is currently on. Only works if the item is currently visible due to pooling.
        /// </summary>
        /// <param name="itemName">Name to look for.</param>
        /// <returns></returns>
        public ListItemCardComponent GetListComponent(string itemName)
        {
            return _cardItemListItem.GetItemComponent(_dataSourceList.FindIndex(data => data.Name == itemName));
        }

        public UnityEvent OnSubmit
        {
            get { return _cardItemListItem.onSubmit; }
        }

        public bool ShouldSort
        {
            get { return _cardItemListItem.Sort; }
            set { _cardItemListItem.Sort = value; }
        }

        public ListItemCardEvent OnRemove
        {
            get { return _cardItemListItem.OnRemoveClicked; }
        }

        public ListItemCardEvent OnDownload
        {
            get { return _cardItemListItem.OnDownloadClicked; }
        }

        public ListItemCardEvent OnAddThumbnail
        {
            get { return _cardItemListItem.OnAddThumbnailClicked; }
        }

        public ListItemCardEvent OnDragDropFailed
        {
            get { return _cardItemListItem.OnDragDropFailed; }
        }

        public int Count
        {
            get { return _cardItemListItem.GetItemsCount(); }
        }

        /// <summary>
        /// Change the sort function of the list
        /// </summary>
        /// <param name="sortFunction"></param>
        public void ChangeSortFunction(Func<IEnumerable<ListItemCardData>, IEnumerable<ListItemCardData>> sortFunction)
        {
            _cardItemListItem.SortFunc = sortFunction;
        }

        #endregion

        public void Clear()
        {
            _cardItemListItem.Clear();
        }
    }
}