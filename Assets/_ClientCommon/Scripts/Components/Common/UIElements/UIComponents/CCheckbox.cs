﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements
{
    public class CCheckbox : BaseUIElement
    {
        [SerializeField]
        private Toggle _checkBox;

        protected override void Init()
        {
            
        }

        protected virtual void Start()
        {
            if(null == _checkBox)
            {
                _checkBox = GetComponent<Toggle>();
            }
        }

        public Toggle GetCheckbox()
        {
            return _checkBox;
        }
    }
}