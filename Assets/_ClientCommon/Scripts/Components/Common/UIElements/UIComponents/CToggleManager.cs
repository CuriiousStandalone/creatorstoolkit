﻿using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.Events;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements
{
    /// <summary>
    /// holds and manages a group of IToggleables.
    /// </summary>
    public class CToggleManager : MonoBehaviour
    {
        [SerializeField]
        private GameObject[] _toggleables;

        [SerializeField]
        private GameObject _defaultToggled;

        [SerializeField]
        private bool HasToHaveValue;

        [Space]
        [SerializeField]
        private CustomToggleEvent OnValueChange;

        private List<IToggleable> _toggles = new List<IToggleable>();

        public IToggleable CurrentlySelectedToggle;

        private void Start()
        {
            foreach (GameObject toggleGameObj in _toggleables)
            {
                IToggleable toggleable = toggleGameObj.GetComponent<IToggleable>();
                if (toggleable != null && !_toggles.Contains(toggleable))
                {
                    _toggles.Add(toggleable);
                }
            }

            foreach (IToggleable toggleable in _toggles)
            {
                toggleable.Manager = this;
                toggleable.Reset();
            }
            SetDefaultValueOn();
        }

        private void SetDefaultValueOn()
        {
            if (HasToHaveValue)
            {
                if (_defaultToggled != null)
                {
                    IToggleable defaultToggleable = _defaultToggled.GetComponent<IToggleable>();
                    if (defaultToggleable != null)
                    {
                        defaultToggleable.IsSelected = true;
                    }
                    else
                    {
                        _toggles[0].IsSelected = true;
                    }
                }
                else
                {
                    _toggles[0].IsSelected = true;
                }
            }
        }

        /// <summary>
        /// resets all toggles back to starting state.
        /// </summary>
        public void UnselectAll()
        {
            CurrentlySelectedToggle = null;
            foreach (IToggleable toggleable in _toggles)
            {
                toggleable.Reset();
            }
            SetDefaultValueOn();
        }

        /// <summary>
        /// sets the new selected toggle and tells all others.
        /// </summary>
        /// <param name="selectedToggle">new selected toggle.</param>
        public void Selected(IToggleable selectedToggle)
        {
            foreach (IToggleable toggleable in _toggles.Where(a => a != selectedToggle))
            {
                toggleable.OnOtherSelected();
            }

            OnValueChange.Invoke(selectedToggle.Name);

            CurrentlySelectedToggle = selectedToggle;
        }

        public void SelectWithoutNotify(IToggleable toggle)
        {
            foreach (IToggleable toggleable in _toggles.Where(a => a != toggle))
            {
                toggleable.OnOtherSelected();
            }

            CurrentlySelectedToggle = toggle;
        }
    }
}