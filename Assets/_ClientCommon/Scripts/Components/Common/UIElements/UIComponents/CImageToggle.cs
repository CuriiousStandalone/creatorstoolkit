﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

using TMPro;

using UnityEngine.Events;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements
{
    /// <summary>
    /// Directly wraps an image. does not need a button.
    /// </summary>
    [RequireComponent(typeof(Image))]
    public class CImageToggle : MonoBehaviour, IToggleable, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
    {
        public CToggleManager Manager { get; set; }

        private bool _isSelected;

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                if (value)
                {
                    SetSelectedState();
                }
                else
                {
                    SetResetState();
                }
            }
        }

        public string Name { get; set; }

        private Color _defaultColor;

        [SerializeField]
        private Image _colorTargetImage;

        [SerializeField]
        private Image _spriteTargetImage;

        [Space]
        [SerializeField]
        private bool _spriteSwap;

        [Space]
        private Sprite _defaultSprite;

        [SerializeField]
        private Sprite _selectedSprite;

        [SerializeField]
        private Sprite _nonSelectedSprite;

        [SerializeField]
        private Sprite _hoverSprite;

        [Space]
        [SerializeField]
        private bool _colorSwap;

        [SerializeField]
        private Color _otherSelectedColor = new Color(0.55f, 0.55f, 0.55f, 0.75f);

        [SerializeField]
        private Color _thisSelectedColor = new Color(0.55f, 0.55f, 0.55f, 0.75f);

        [Space]
        [SerializeField]
        private bool _hasText;

        [SerializeField]
        private TMP_Text _txtField;

        [SerializeField]
        private Color _textSelectedColor;

        [SerializeField]
        private Color _textNonSelectedColor;

        [SerializeField]
        private Color _textHoverColor;

        private void Awake()
        {
            Name = gameObject.name;

            if (_colorTargetImage != null)
            {
                _defaultColor = _colorTargetImage.color;
            }
            if (_spriteTargetImage != null)
            {
                _defaultSprite = _spriteTargetImage.sprite;
            }
        }

        public void SetImages(Sprite defaultImg, Sprite selectedImage, Sprite hoverImage)
        {
            GetComponent<Image>().sprite = defaultImg;
            _selectedSprite = selectedImage;
            _defaultSprite = _nonSelectedSprite = defaultImg;
            _hoverSprite = hoverImage;
        }

        public void SetText(string value)
        {
            if (_hasText)
            {
                _txtField.text = value;
            }
        }

        public void OnOtherSelected()
        {
            IsSelected = false;

            SetOtherSelectedState();
        }

        private void SetOtherSelectedState()
        {
            if (_colorTargetImage != null && _colorSwap)
            {
                _colorTargetImage.color = _otherSelectedColor;
            }
            if (_spriteTargetImage != null && _spriteSwap)
            {
                _spriteTargetImage.sprite = _nonSelectedSprite;
            }
            if (_txtField != null && _hasText)
            {
                _txtField.color = _textNonSelectedColor;
            }
        }

        public void Reset()
        {

        }

        private void SetResetState()
        {
            if (_colorTargetImage != null && _colorSwap)
            {
                _colorTargetImage.color = _defaultColor;
            }
            if (_spriteTargetImage != null && _spriteSwap)
            {
                _spriteTargetImage.sprite = _defaultSprite;
            }
            if (_txtField != null && _hasText)
            {
                _txtField.color = _textNonSelectedColor;
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            SetSelectedState();

            IsSelected = true;
            Manager.Selected(this);
        }

        public void SetSelectedState()
        {
            if (_colorTargetImage != null && _colorSwap)
            {
                _colorTargetImage.color = _thisSelectedColor;
            }
            if (_spriteTargetImage != null && _spriteSwap)
            {
                _spriteTargetImage.sprite = _selectedSprite;
            }
            if (_txtField != null && _hasText)
            {
                _txtField.color = _textSelectedColor;
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (!IsSelected)
            {
                if (_spriteTargetImage != null && _spriteSwap)
                {
                    _spriteTargetImage.sprite = _hoverSprite;
                }
                if (_txtField != null && _hasText)
                {
                    _txtField.color = _textHoverColor;
                }
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (!IsSelected)
            {
                if (_spriteTargetImage != null && _spriteSwap)
                {
                    _spriteTargetImage.sprite = _nonSelectedSprite;
                }
                if (_txtField != null && _hasText)
                {
                    _txtField.color = _textNonSelectedColor;
                }
            }
        }
    }
}