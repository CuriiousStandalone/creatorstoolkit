using System;

using UIWidgets;

using UnityEngine.Events;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents
{
    /// <summary>
    /// TreeViewNode event.
    /// </summary>
    [Serializable]
    public class CustomTreeViewNodeEvent : UnityEvent<TreeNode<CTreeViewItem>>
    {
    }
}