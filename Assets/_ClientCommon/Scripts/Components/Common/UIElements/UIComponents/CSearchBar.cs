using UnityEngine;

using TMPro;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents
{
    /// <summary>
    /// Base class for a search bar. can also be used for simple term based search bars.
    /// </summary>
    public class CSearchBar : BaseUIElement
    {
        [SerializeField]
        private ISearchable[] _filterableCollection;

        [SerializeField]
        private TMP_InputField _inputField;
        protected override void Init()
        {
            _inputField.onValueChanged.AddListener(OnFilterChanged);
        }

        private void OnFilterChanged(string inputValue)
        {
            if (_filterableCollection != null)
            {
                foreach (ISearchable searchable in _filterableCollection)
                {
                    searchable?.ApplyFilterTerm(inputValue);
                }
            }
        }

        private void OnDestroy()
        {
            if (_filterableCollection != null)
            {
                foreach (ISearchable searchable in _filterableCollection)
                {
                    searchable?.ClearFilterTerm();
                }
            }

            if (_inputField != null)
            {
                _inputField.onValueChanged.RemoveListener(OnFilterChanged);
            }
        }
    }
}