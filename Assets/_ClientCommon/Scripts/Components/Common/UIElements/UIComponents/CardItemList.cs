﻿using System;

using UIWidgets;

using UnityEngine;
using UnityEngine.Events;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents
{
    /// <summary>
    /// Sets the actual types used by the list.
    /// </summary>
    public class CardItemList : ListViewCustom<ListItemCardComponent, ListItemCardData>
    {
        public ListItemCardEvent OnRemoveClicked = new ListItemCardEvent();
        public ListItemCardEvent OnDownloadClicked = new ListItemCardEvent();
        public ListItemCardEvent OnAddThumbnailClicked = new ListItemCardEvent();
        public ListItemCardEvent OnDragDropFailed = new ListItemCardEvent();
    }
}