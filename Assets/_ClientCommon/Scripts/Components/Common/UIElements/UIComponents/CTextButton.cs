﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements
{
    public class CTextButton : CButton
    {
        [SerializeField]
        [HideInInspector]
        private Color _normalTextColor = Color.white;

        [SerializeField]
        [HideInInspector]
        private Color _pressedTextColor = Color.white;

        [SerializeField]
        [HideInInspector]
        private Color _hoverTextColor = Color.white;

        [SerializeField]
        [HideInInspector]
        private Color _disabledTextColor = Color.white;

        [SerializeField]
        private TextMeshProUGUI _buttonText = null;

        [SerializeField]
        private bool _transitionalText = false;

        private bool isHovering = false;
        private bool isMouseDown = false;

        public bool TransitionalText
        {
            get
            {
                return _transitionalText;
            }

            set
            {
                _transitionalText = value;
            }
        }

        public Color NormalTextColor
        {
            get
            {
                return _normalTextColor;
            }

            set
            {
                _normalTextColor = value;
            }
        }

        public Color PressedTextColor
        {
            get
            {
                return _pressedTextColor;
            }

            set
            {
                _pressedTextColor = value;
            }
        }

        public Color HoverTextColor
        {
            get
            {
                return _hoverTextColor;
            }

            set
            {
                _hoverTextColor = value;
            }
        }

        public Color DisabledTextColor
        {
            get
            {
                return _disabledTextColor;
            }

            set
            {
                _disabledTextColor = value;
            }
        }

        protected override void Init()
        {

        }

        protected override void Start()
        {
            base.Start();

            if (null == _buttonText)
            {
                _buttonText = transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            }

            if (_transitionalText && null != _buttonText)
            {
                EventTrigger trigger = gameObject.AddComponent<EventTrigger>();

                EventTrigger.Entry mouseUp = new EventTrigger.Entry();
                mouseUp.eventID = EventTriggerType.PointerUp;
                mouseUp.callback.AddListener((data) => { OnMouseUp((PointerEventData)data); });
                trigger.triggers.Add(mouseUp);

                EventTrigger.Entry mouseDown = new EventTrigger.Entry();
                mouseDown.eventID = EventTriggerType.PointerDown;
                mouseDown.callback.AddListener((data) => { OnMouseDown((PointerEventData)data); });
                trigger.triggers.Add(mouseDown);

                EventTrigger.Entry hoverEnter = new EventTrigger.Entry();
                hoverEnter.eventID = EventTriggerType.PointerEnter;
                hoverEnter.callback.AddListener((data) => { OnHoverStart((PointerEventData)data); });
                trigger.triggers.Add(hoverEnter);

                EventTrigger.Entry hoverExit = new EventTrigger.Entry();
                hoverExit.eventID = EventTriggerType.PointerExit;
                hoverExit.callback.AddListener((data) => { OnHoverExit((PointerEventData)data); });
                trigger.triggers.Add(hoverExit);

                _buttonText.color = _normalTextColor;
            }
        }

        private void OnDisable()
        {
            if (_transitionalText)
            {
                _buttonText.color = _disabledTextColor;
            }
        }

        private void OnEnable()
        {
            if (_transitionalText)
            {
                _buttonText.color = _normalTextColor;
            }
        }

        public void OnHoverStart(PointerEventData data)
        {
            isHovering = true;
            _buttonText.color = isMouseDown ? _pressedTextColor : _hoverTextColor;
        }

        public void OnHoverExit(PointerEventData data)
        {
            isHovering = false;
            _buttonText.color = isMouseDown ? _pressedTextColor : _normalTextColor;
        }

        public void OnMouseDown(PointerEventData data)
        {
            isMouseDown = true;
            _buttonText.color = _pressedTextColor;
        }

        public void OnMouseUp(PointerEventData data)
        {
            isMouseDown = false;
            _buttonText.color = isHovering ? _hoverTextColor : _normalTextColor;
        }
    }
}