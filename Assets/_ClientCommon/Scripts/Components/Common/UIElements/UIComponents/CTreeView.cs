﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

using UIWidgets;

using PulseIQ.Local.ClientCommon.Extensions;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents
{
    public class CTreeView : BaseUIElement, ISearchableTypeFilter, IDropSupport<ListItemCardData>
    {
        /// <summary>
        /// Rough copy of TreeNode but it doesn't use Observerable list so I can manually manage references.
        /// </summary>
        /// <typeparam name="T">Item type</typeparam>
        private class TreeNodeWithoutObserver<T> : IEnumerable<TreeNodeWithoutObserver<T>>
        {
            public TreeNodeWithoutObserver<T> Parent;
            public List<TreeNodeWithoutObserver<T>> Nodes = new List<TreeNodeWithoutObserver<T>>();
            public T Item;

            public IEnumerator<TreeNodeWithoutObserver<T>> GetEnumerator()
            {
                return Nodes.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }

        public string[] AllowedDropTypes;

        private TreeViewCustom _treeView;

        private bool _isCurrentlyFlat;
        private readonly List<TreeNodeWithoutObserver<CTreeViewItem>> _masterHierarchy = new List<TreeNodeWithoutObserver<CTreeViewItem>>();
        private readonly Dictionary<string, bool> _nodeIsExpanded = new Dictionary<string, bool>();

        public string[] SupportedDropTypes;

        #region Debug

        private const int FIRST_COUNT = 3;
        private const int SECOND_COUNT = 1;
        private const int THIRD_DEPTH = 1;

        private const string VIDEO = "Video";
        private const string IMAGE = "Image";
        private const string MODEL = "Model";

        private const bool DEBUG_MODE = false;

        #endregion

        public void InitNow()
        {
            Init();
        }

        protected override void Init()
        {
            _treeView = GetComponent<TreeViewCustom>();
            _treeView.Nodes = new ObservableList<TreeNode<CTreeViewItem>>();

            if (DEBUG_MODE)
            {
                for (int i = 0; i < FIRST_COUNT; i++)
                {
                    string parent = $"AddedNode {(i + 1)}";
                    string parentName = $"{parent} ({VIDEO})";
                    AddItem(parentName, parentName, new[] {VIDEO});
                    for (int j = 0; j < SECOND_COUNT; j++)
                    {
                        string child = $"{parent}-{(j + 1)}";
                        string childName = $"{child} ({IMAGE})";
                        AddItem(childName, childName, new[] {IMAGE}, parentName);
                        for (int k = 0; k < THIRD_DEPTH; k++)
                        {
                            string grandChild = $"{child}-{(k + 1)}";
                            string grandChildName = $"{grandChild} ({MODEL})";
                            AddItem(grandChildName, grandChildName, new[] {MODEL}, childName);
                        }
                    }
                }
            }
        }

        #region Events

        public UnityEvent<ListItemCardData> ResourceDropped = new ListItemCardEvent();

        public ListNode<CTreeViewItem> SelectedItem => _treeView.SelectedItem;

        public ListViewBaseEvent OnItemSelected => _treeView.OnSelect;

        #endregion

        public void ExpandItem(string itemId)
        {
            if (null != FindNode(itemId))
            {
                if (!FindNode(itemId).IsExpanded)
                {
                    _treeView.OnToggleNode(FindNode(itemId).Index);
                }
            }
        }

        public void SelectItem(int index)
        {
            _treeView.Select(index);
        }

        public void SelectItem(string itemId)
        {
            _treeView.Select(FindNode(itemId).Index, true);
        }

        public void SelectItemSilent(string itemId)
        {
            _treeView.Select(FindNode(itemId).Index, false);
        }

        #region Add Remove Copy Items

        /// <summary>
        /// Makes a new Object with the same item.
        /// </summary>
        /// <param name="input">Item to Copy</param>
        /// <returns></returns>
        private static TreeNode<CTreeViewItem> MakeCopy(TreeNodeWithoutObserver<CTreeViewItem> input)
        {
            TreeNode<CTreeViewItem> result = new TreeNode<CTreeViewItem>(input.Item, new ObservableList<TreeNode<CTreeViewItem>>());
            result.Parent = new TreeNode<CTreeViewItem>(new CTreeViewItem("Dead", "Dead", new[] {""}));
            return result;
        }

        /// <summary>
        /// Removes items from the tree. This will also remove and children.
        /// </summary>
        /// <param name="itemName">name of item.</param>
        public void RemoveItem(string itemName)
        {
            //Remove from master.
            var nodeInMaster = FindNode(itemName, _masterHierarchy);
            if (nodeInMaster.Parent != null)
            {
                var parentNodeInMaster = FindNode(nodeInMaster.Parent.Item.Name, _masterHierarchy);
                parentNodeInMaster.Nodes.Remove(nodeInMaster);
            }
            else
            {
                _masterHierarchy.Remove(nodeInMaster);
            }
            //remove from view
            _treeView.Remove(node => node.Item.Name == itemName);
        }

        public void RemoveItemWithID(string itemID)
        {
            //Remove from master.
            var nodeInMaster = FindNodeWithID(itemID, _masterHierarchy);
            if (nodeInMaster.Parent != null)
            {
                var parentNodeInMaster = FindNode(nodeInMaster.Parent.Item.Name, _masterHierarchy);
                parentNodeInMaster.Nodes.Remove(nodeInMaster);
            }
            else
            {
                _masterHierarchy.Remove(nodeInMaster);
            }
            //remove from view
            _treeView.Remove(node => node.Item.ItemID == itemID);
        }

        /// <summary>
        /// Adds items to the tree view. if not parent is given it will sit on top level.
        /// </summary>
        /// <param name="itemName">Name of new item</param>
        /// <param name="itemIcon">Icon for the item</param>
        /// <param name="nodeParent">Parent Item to fall under</param>
        public void AddItem(string itemName, string id, string[] types = null, Sprite itemIcon = null, TreeNode<CTreeViewItem> nodeParent = null)
        {
            CTreeViewItem masterItem = new CTreeViewItem(itemName, id, types);
            masterItem.Icon = itemIcon;
            TreeNodeWithoutObserver<CTreeViewItem> masterNode = new TreeNodeWithoutObserver<CTreeViewItem>();
            masterNode.Item = masterItem;
            var copyNode = MakeCopy(masterNode);
            masterNode.Nodes = new List<TreeNodeWithoutObserver<CTreeViewItem>>();
            copyNode.Nodes = new ObservableList<TreeNode<CTreeViewItem>>();
            if (nodeParent != null)
            {
                TreeNodeWithoutObserver<CTreeViewItem> masterParent = FindNode(nodeParent.Item.Name, _masterHierarchy);
                if (_isCurrentlyFlat)
                {
                    //add to both the current list in a flat format and the cached master node tree.
                    _treeView.Nodes.Add(copyNode);
                    masterNode.Parent = FindNode(nodeParent.Item.Name, _masterHierarchy);
                    masterParent.Nodes.Add(masterNode);
                }
                else
                {
                    if (nodeParent.Nodes == null)
                    {
                        nodeParent.Nodes = new ObservableList<TreeNode<CTreeViewItem>>();
                    }

                    nodeParent.Nodes.Add(copyNode);
                    masterParent.Nodes.Add(masterNode);
                    masterNode.Parent = masterParent;
                }
            }
            else
            {
                _masterHierarchy.Add(masterNode);
                _treeView.Nodes.Add(copyNode);
            }
        }

        /// <summary>
        /// Adds items to the tree view. if not parent is given it will sit on top level.
        /// </summary>
        /// <param name="itemName"></param>
        /// <param name="parentName"></param>
        /// <param name="type"></param>
        public void AddItem(string itemName, string id, string[] type, string parentName)
        {
            AddItem(itemName, id, type, null, FindNode(parentName, _treeView.Nodes));
        }

        public void Reset()
        {
            _treeView.Clear();
            _masterHierarchy.Clear();
        }

        #endregion

        #region FindNodes

        /// <summary>
        /// Traverses all elements of the tree view looking for matching name.
        /// </summary>
        /// <param name="nodeName">Name of node to find</param>
        /// <param name="treeNodes">Collection to Search against</param>
        /// <returns>Matching node or null</returns>
        private TreeNodeWithoutObserver<CTreeViewItem> FindNode(string nodeName, IEnumerable<TreeNodeWithoutObserver<CTreeViewItem>> treeNodes)
        {
            return treeNodes.Traverse(a => a.Nodes).First(node => node.Item.Name.Equals(nodeName));
        }

        private TreeNodeWithoutObserver<CTreeViewItem> FindNodeWithID(string nodeID, IEnumerable<TreeNodeWithoutObserver<CTreeViewItem>> treeNodes)
        {
            return treeNodes.Traverse(a => a.Nodes).First(node => node.Item.ItemID.Equals(nodeID));
        }

        /// <summary>
        /// Traverses all elements of the tree view looking for matching name.
        /// </summary>
        /// <param name="nodeIdentifier">Name or ID of node to find</param>
        /// <param name="treeNodes">Collection to Search against</param>
        /// <returns>Matching node or null</returns>
        public TreeNode<CTreeViewItem> FindNode(string nodeIdentifier)
        {
            return _treeView.Nodes.Traverse(a => a.Nodes).FirstOrDefault(node => node.Item.Name.Equals(nodeIdentifier) || node.Item.ItemID.Equals(nodeIdentifier));
        }

        /// <summary>
        /// Traverses all elements of the tree view looking for matching name.
        /// </summary>
        /// <param name="nodeIdentifier">Name or ID of node to find</param>
        /// <param name="treeNodes">Collection to Search against</param>
        /// <returns>Matching node or null</returns>
        private TreeNode<CTreeViewItem> FindNode(string nodeIdentifier, IEnumerable<TreeNode<CTreeViewItem>> treeNodes)
        {
            return treeNodes.Traverse(a => a.Nodes).FirstOrDefault(node => node.Item.Name.Equals(nodeIdentifier) || node.Item.ItemID.Equals(nodeIdentifier));
        }

        #endregion

        #region Search

        /// <summary>
        /// Temporarily changes tree view into a flatten list showing all nodes as root level.
        /// Mostly for use with searching.
        /// </summary>
        private void Flatten()
        {
            if (_isCurrentlyFlat)
            {
                Unflatten();
            }

            _isCurrentlyFlat = true;
            TreeNode<CTreeViewItem>[] flatNodes = _treeView.Nodes.Traverse(node => node.Nodes).ToArray();
            //clear previous
            _treeView.Nodes.Clear();
            for (int index = 0; index < flatNodes.Length; index++)
            {
                TreeNode<CTreeViewItem> treeNode = flatNodes[index];
                _nodeIsExpanded.Add(treeNode.Item.ItemID, treeNode.IsExpanded);
                //manipulate to flat nodes.
                treeNode.Parent = null;
                treeNode.Nodes = new ObservableList<TreeNode<CTreeViewItem>>();
                _treeView.Nodes.Add(treeNode);
            }
        }

        /// <summary>
        /// Reverses a flattening by making a new copy of the hierarchy based on the master copy.
        /// </summary>
        private void Unflatten()
        {
            if (!_isCurrentlyFlat)
            {
                Flatten();
            }

            _isCurrentlyFlat = false;

            _treeView.Nodes.Clear();
            var hierarchyCopy = _masterHierarchy;
            hierarchyCopy.Reverse();
            var stack = new Stack<TreeNodeWithoutObserver<CTreeViewItem>>(hierarchyCopy);
            while (stack.Count > 0)
            {
                var masterNode = stack.Pop();
                //create copy from master.
                var masterNodeCopy = MakeCopy(masterNode);
                if (_nodeIsExpanded.ContainsKey(masterNodeCopy.Item.Name))
                {
                    masterNodeCopy.IsExpanded = _nodeIsExpanded[masterNodeCopy.Item.Name];
                }

                //is top level using master?
                if (masterNode.Parent == null || masterNode.Parent.Item?.Name == "Dead")
                {
                    // add copy to top level view
                    _treeView.Nodes.Add(masterNodeCopy);
                }
                else
                {
                    //add copy to parent in view (which is also a copy.
                    var parentInView = FindNode(masterNode.Parent.Item.Name, _treeView.Nodes);
                    if (parentInView != null)
                    {
                        parentInView.Nodes.Add(masterNodeCopy);
                    }
                }

                //add children to traversal. using master node
                foreach (var child in masterNode.Nodes)
                {
                    stack.Push(child);
                }
            }

            _nodeIsExpanded.Clear();
        }

        public void ApplyFilterTerm(string filter)
        {
            filter = filter?.Trim();
            if (string.IsNullOrEmpty(filter))
            {
                ClearFilterTerm();
                FixMasks();
                return;
            }

            Flatten();
            _treeView.ScrollTo(0);
            List<CTreeViewItem> removeItems = new List<CTreeViewItem>();
            //no need to traverse here due to flatten.
            foreach (var nodeItem in _treeView.Nodes.Select(a => a.Item))
            {
                bool containsTerm = nodeItem.Name.ToLower().Contains(filter.ToLower());
                if (!containsTerm)
                {
                    removeItems.Add(nodeItem);
                }
            }
            _treeView.Nodes.RemoveAll(node => removeItems.Contains(node.Item));
            FixMasks();
        }

        public void ApplyFilter(string filter, string[] type)
        {
            Flatten();
            _treeView.ScrollTo(0);

            filter = filter?.Trim();
            //no need to traverse here due to flatten.
            List<CTreeViewItem> removeItems = new List<CTreeViewItem>();
            foreach (var nodeItem in _treeView.Nodes.Select(a => a.Item))
            {
                bool containsTerm = string.IsNullOrEmpty(filter) || nodeItem.Name.ToLower().Contains(filter.ToLower());
                bool matchesType = false;
                if (nodeItem.Types != null)
                {
                    string[] nodeTypes = (string[])nodeItem.Types;
                    foreach (string nodeType in nodeTypes)
                    {
                        matchesType |= type.Contains(nodeType);
                    }
                    if (!matchesType || !containsTerm)
                    {
                        removeItems.Add(nodeItem);
                    }
                }
            }
            _treeView.Nodes.RemoveAll(node => removeItems.Contains(node.Item));

            FixMasks();
        }

        public void CancelTypeFilter(string currentTerm)
        {
            ApplyFilterTerm(currentTerm);
            _treeView.ScrollTo(0);
        }

        private void FixMasks()
        {
            foreach (var treeNode in _treeView.Nodes.Traverse(a => a.Nodes))
            {
                _treeView.GetItemComponent(treeNode.Index)?.OnPointerExit(null);
            }
        }

        public void ClearFilterTerm()
        {
            Unflatten();
            _treeView.ScrollTo(0);
            foreach (var node in _treeView.Nodes.Traverse(node => node.Nodes))
            {
                _treeView.GetItemComponent(node.Index)?.gameObject.SetActive(true);
            }
        }

        #endregion

        #region Debug

        [ContextMenu("Debug - Log Master")]
        public void LogStuff()
        {
            if (!DEBUG_MODE)
            {
                return;
            }

            int nodesCount = 0;
            int grandNodesCount = 0;
            if (_masterHierarchy.Count > 0)
            {
                nodesCount = _masterHierarchy[0].Nodes.Count;
                if (_masterHierarchy[0].Nodes.Count > 0)
                {
                    grandNodesCount = _masterHierarchy[0].Nodes[0].Nodes.Count;
                }
            }

            Debug.Log($"Master: {_masterHierarchy.Count}-{nodesCount}-{grandNodesCount}");
        }

        [ContextMenu("Debug - Add Stack")]
        public void DebugAddStack()
        {
            if (!DEBUG_MODE)
            {
                return;
            }

            for (int i = 0; i < FIRST_COUNT; i++)
            {
                string parent = $"AddedNode {(i + 1)}";
                string parentName = $"{parent} ({VIDEO})";
                AddItem(parentName, parentName, new[] {VIDEO});
                for (int j = 0; j < SECOND_COUNT; j++)
                {
                    string child = $"{parent}-{(j + 1)}";
                    string childName = $"{child} ({IMAGE})";
                    AddItem(childName, childName, new[] {IMAGE}, parentName);
                    for (int k = 0; k < THIRD_DEPTH; k++)
                    {
                        string grandChild = $"{child}-{(k + 1)}";
                        string grandChildName = $"{grandChild} ({MODEL})";
                        AddItem(grandChildName, grandChildName, new[] {MODEL}, childName);
                    }
                }
            }
        }

        [ContextMenu("Debug - ResetToData")]
        public void DebugResetToData()
        {
            if (!DEBUG_MODE)
            {
                return;
            }

            _treeView.Clear();
            Init();
        }

        #endregion

        #region DropSupport

        public bool CanReceiveDrop(ListItemCardData data, PointerEventData eventData)
        {
            return data.Type.Any(type => SupportedDropTypes.Contains(type)) && data.ResourceType == ListCardResourceTypes.PLATFORM;
        }

        public void Drop(ListItemCardData data, PointerEventData eventData)
        {
            if (CanReceiveDrop(data, eventData))
            {
                ResourceDropped?.Invoke(data);
            }
        }

        public void DropCanceled(ListItemCardData data, PointerEventData eventData)
        {
        }

        #endregion
    }
}