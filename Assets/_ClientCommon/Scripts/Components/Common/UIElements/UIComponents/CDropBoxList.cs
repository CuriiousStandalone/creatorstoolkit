﻿using UIWidgets;

using UnityEngine;
using UnityEngine.EventSystems;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents
{
    public class CDropBoxList : CDropBox
    {
        [SerializeField]
        private CardItemList _listView;

        public override void Drop(ListItemCardData data, PointerEventData eventData)
        {
            _listView.Add(data);
        }
        
    }
}