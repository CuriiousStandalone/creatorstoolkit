using UnityEngine.Events;
namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents
{
    public class ListItemCardEvent : UnityEvent<ListItemCardData>
    {
        
    }
}