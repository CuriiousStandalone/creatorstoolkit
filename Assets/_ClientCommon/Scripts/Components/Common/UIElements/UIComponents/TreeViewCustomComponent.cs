﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

using TMPro;

using UIWidgets;

using UnityEngine;
using UnityEngine.UI;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents
{
    public class TreeViewCustomComponent : TreeViewComponentBase<CTreeViewItem>
    {
        /// <summary>
        /// Text.
        /// </summary>
        public TextMeshProUGUI TextTMPro;

        CTreeViewItem item;

        /// <summary>
        /// Gets foreground graphics for coloring.
        /// </summary>
        public override Graphic[] GraphicsForeground
        {
            get { return new Graphic[] {TextTMPro,}; }
        }
        
        /// <summary>
        /// Gets or sets the item.
        /// </summary>
        /// <value>The item.</value>
        public CTreeViewItem Item
        {
            get { return item; }

            set
            {
                if (item != null)
                {
                    item.PropertyChanged -= UpdateView;
                }

                item = value;

                if (item != null)
                {
                    item.PropertyChanged += UpdateView;
                }

                UpdateView();
            }
        }

        /// <summary>
        /// Sets the data.
        /// </summary>
        /// <param name="node">Node.</param>
        /// <param name="depth">Depth.</param>
        public override void SetData(TreeNode<CTreeViewItem> node, int depth)
        {
            Node = node;
            base.SetData(Node, depth);
            TextTMPro.text = node.Item.Name;

            Item = (Node == null) ? null : Node.Item;
        }

        /// <summary>
        /// Updates the view.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="ev">Event.</param>
        protected virtual void UpdateView(object sender = null, PropertyChangedEventArgs ev = null)
        {
            if ((Icon == null) || (TextTMPro == null))
            {
                return;
            }

            if (Item == null)
            {
                Icon.sprite = null;
                TextTMPro.text = string.Empty;
            }
            else
            {
                Icon.sprite = Item.Icon;
                TextTMPro.text = Item.LocalizedName ?? Item.Name;
            }

            if (SetNativeSize)
            {
                Icon.SetNativeSize();
            }

            // set transparent color if no icon
            Icon.color = (Icon.sprite == null) ? Color.clear : Color.white;
        }

        /// <summary>
        /// Called when item moved to cache, you can use it free used resources.
        /// </summary>
        public override void MovedToCache()
        {
            if (Icon != null)
            {
                Icon.sprite = null;
            }
        }

        /// <summary>
        /// This function is called when the MonoBehaviour will be destroyed.
        /// </summary>
        protected override void OnDestroy()
        {
            if (item != null)
            {
                item.PropertyChanged -= UpdateView;
            }

            base.OnDestroy();
        }
    }
}