﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements
{
    public class CDropdown : BaseUIElement
    {
        [SerializeField]
        private TMP_Dropdown _dropdown = null;

        protected override void Init()
        {

        }

        private void Start()
        {
            if(null == _dropdown)
            {
                _dropdown = GetComponent<TMP_Dropdown>();
            }
        }        

        public int GetIndexByName(string name)
        {
            return _dropdown.options.FindIndex((i) => { return i.text.Equals(name); });
        }

        public TMP_Dropdown GetDropdown()
        {
            return _dropdown;
        }

        public string GetDropdownStringValue()
        {
            return _dropdown.options[_dropdown.value].text;
        }
    }
}