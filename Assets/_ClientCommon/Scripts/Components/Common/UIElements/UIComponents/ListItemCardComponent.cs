﻿using System;
using System.ComponentModel;
using System.Linq;

using UnityEngine;
using UnityEngine.UI;

using TMPro;

using UIWidgets;
using System.IO;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents
{
    /// <summary>
    /// Copy of ListIconsItemComponent with added type field. Will likely add more in the future.
    /// </summary>
    public class ListItemCardComponent : ListViewItem, IViewData<ListItemCardData>
    {
        /// <summary>
        /// Gets foreground graphics for coloring.
        /// </summary>
        public override Graphic[] GraphicsForeground
        {
            get { return new Graphic[] {NameText,}; }
        }

        /// <summary>
        /// The icon.
        /// </summary>
        [SerializeField]
        public Image Icon;

        /// <summary>
        /// The text.
        /// </summary>
        [SerializeField]
        public TextMeshProUGUI NameText;

        /// <summary>
        /// The text for meta.
        /// </summary>
        [SerializeField]
        public TextMeshProUGUI MetaText;

        /// <summary>
        /// The text for type.
        /// </summary>
        [SerializeField]
        public TextMeshProUGUI TypeText;

        /// <summary>
        /// Icon to symbolize remote resource
        /// </summary>
        [SerializeField]
        public GameObject CloudIcon;

        /// <summary>
        /// Button that can delete local resources.
        /// </summary>
        [SerializeField]
        public GameObject TrashButton;

        /// <summary>
        /// Set icon native size.
        /// </summary>
        public bool SetNativeSize = true;

        public GameObject ToggleOnImageHover;

        public Sprite DefaultImage;

        [Serializable]
        public struct StringSpriteDictionary
        {
            public string Type;
            public Sprite Sprite;
        }

        public StringSpriteDictionary[] DefaultImages;

        /// <summary>
        /// Gets the current item.
        /// </summary>
        public ListItemCardData Item { get; protected set; }

        public Vector3 DragIconScale = Vector3.one * 0.5f;
        public Sprite AlphaSprite;

        /// <summary>
        /// Sets component data with specified item.
        /// </summary>
        /// <param name="item">Item.</param>
        public virtual void SetData(ListItemCardData item)
        {
            if (item == null)
            {
                return;
            }

            Item = item;
            gameObject.name = item == null ? "DefaultItem (Clone)" : item.Name;
            item.PropertyChanged += ItemOnPropertyChanged;

            if (Icon != null)
            {
                if (null != Item.Icon || DefaultImage == null)
                {
                    Icon.sprite = Item.Icon;
                    if (item.Icon == null)
                    {
                        Icon.sprite = DefaultImages?.FirstOrDefault(sprite => sprite.Type.Equals(item.Type[0], StringComparison.OrdinalIgnoreCase)).Sprite;
                        if (Icon.sprite != null)
                        {
                            item.Icon = Icon.sprite;
                        }
                    }
                }
                else
                    {
                    Icon.sprite = DefaultImage;
                }
            }

            if (NameText != null)
            {
                NameText.text = Item.LocalizedName ?? Path.GetFileNameWithoutExtension(Item.Name);
            }
            if (MetaText != null)
            {
                if (null != item.Meta)
                {
                    string text = string.Copy(item.Meta);
                    MetaText.text = text;
                }
                else
                {
                    MetaText.text = "";
                }
            }
            if (TypeText != null)
            {
                TypeText.text = GetName(item.Type);
            }
            if (CloudIcon != null && TrashButton != null)
            {
                switch (item.ResourceType)
                {
                    case ListCardResourceTypes.REMOTE:
                        CloudIcon.SetActive(true);
                        TrashButton.SetActive(false);
                        break;
                    case ListCardResourceTypes.PLATFORM:
                        CloudIcon.SetActive(false);
                        TrashButton.SetActive(true);
                        break;
                    case ListCardResourceTypes.HOTSPOT:
                        CloudIcon.SetActive(false);
                        TrashButton.SetActive(false);
                        break;
                    default:
                        Debug.Log("Shouldn't See this");
                        break;
                }
            }

            if (Icon != null)
            {
                if (SetNativeSize)
                {
                    Icon.SetNativeSize();
                }

                // set transparent color if no icon
                Icon.color = (Icon.sprite == null) ? Color.clear : Color.white;
            }
        }

        private void ItemOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            ListItemCardData itemData = (ListItemCardData)sender;
            itemData.PropertyChanged -= ItemOnPropertyChanged;
            if (!itemData.ResourceID.Equals(Item.ResourceID))
            {
                return;
            }
            SetData(itemData);
        }

        /// <summary>
        /// Called when item moved to cache, you can use it free used resources.
        /// </summary>
        public override void MovedToCache()
        {
            if (Icon != null)
            {
                Icon.sprite = null;
            }
            Item.PropertyChanged -= ItemOnPropertyChanged;
        }

        public void OnDownloadClicked()
        {
            Owner.gameObject.GetComponent<CardItemList>()?.OnDownloadClicked.Invoke(Item);
        }

        public void OnRemoveClicked()
        {
            Owner.gameObject.GetComponent<CardItemList>()?.OnRemoveClicked.Invoke(Item);
        }

        public void OnPointerEnteredThumbnail()
        {
            if (Item.Icon == null || DefaultImages.Any(a=>a.Sprite.Equals(Item.Icon)))
            {
                //ToggleOnImageHover.SetActive(true);
            }
        }

        public void OnPointerExitedThumbnail()
        {
            ToggleOnImageHover.SetActive(false);
        }

        public void OnAddThumbnailClicked()
        {
            OnPointerExitedThumbnail();
            Owner.gameObject.GetComponent<CardItemList>()?.OnAddThumbnailClicked.Invoke(Item);
        }

        public void OnDragDropFailed()
        {
            Owner.gameObject.GetComponent<CardItemList>()?.OnDragDropFailed.Invoke(Item);
        }

        private string GetName(string[] name)
        {
            if (null != name && name.Length > 0)
            {
                switch (name[0])
                {
                    case "IMAGE":
                        {
                            return "Image";
                        }

                    case "PANORAMA":
                        {
                            return "Panorama";
                        }

                    case "AUDIO":
                        {
                            return "Audio";
                        }

                    case "STEREO_PANORAMA_VIDEO":
                        {
                            return "Stereo Panorama Video";
                        }

                    case "PANORAMA_VIDEO":
                        {
                            return "Panorama Video";
                        }
                    default:
                        {
                            return name[0];
                        }
                }
            }
            else
            {
                return "";
            }
        }
    }
}