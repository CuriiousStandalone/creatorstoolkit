﻿using System;

using UIWidgets;

using UnityEngine;
using UnityEngine.EventSystems;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents
{
    public class TreeViewCustom : TreeViewCustom<TreeViewCustomComponent, CTreeViewItem>
    {
        /// <summary>
        /// NodeToggleProxy event.
        /// </summary>
        public CustomTreeViewNodeEvent NodeToggleProxy = new CustomTreeViewNodeEvent();

        bool isInited = false;

        /// <summary>
        /// Init this instance.
        /// </summary>
        public override void Init()
        {
            if (isInited)
            {
                return;
            }

            isInited = true;

            base.Init();
            Sort = false;

            NodeToggle.AddListener(NodeToggleProxy.Invoke);
        }

        /// <summary>
        /// Scrolls to item with specified index.
        /// </summary>
        /// <param name="index">Index.</param>
        public override void ScrollTo(int index)
        {
            if (!IsVirtualizationSupported())
            {
                return;
            }
            
            var current_position = scrollRect.content.anchoredPosition;
            ListRenderer.GetItemSize(true);
            float itemHeight = ItemSize.y;
            int topViewportIndex = Mathf.CeilToInt(current_position.y / itemHeight);
            int botViewportIndex = Mathf.CeilToInt(current_position.y / itemHeight + scrollRect.viewport.rect.height / itemHeight);
            Vector2 newPosition = new Vector2();
            
            if (index + 3 >= botViewportIndex && index + 3 < GetItemsCount())
            {
                newPosition = new Vector2(current_position.x, current_position.y + itemHeight);
            }
            else if (index - 3 <= topViewportIndex && index - 3 > 0)
            {
                newPosition = new Vector2(current_position.x, current_position.y - itemHeight);
            }
            
            if (newPosition != default)
            {
                const float delta = 0.01f;
                var diff = (IsHorizontal() && Mathf.Abs(current_position.x - newPosition.x) >= delta)
                           || (!IsHorizontal() && Mathf.Abs(current_position.y - newPosition.y) >= delta);

                scrollRect.StopMovement();

                if (diff)
                {
                    scrollRect.content.anchoredPosition = newPosition;
                    UpdateView();
                }
            }
        }

        protected override void OnItemMove(AxisEventData eventData, ListViewItem item)
        {
            if (!Navigation)
            {
                return;
            }

            var treeItem = (TreeViewCustomComponent)item;
            switch (eventData.moveDir)
            {
                case MoveDirection.Left:
                    treeItem.Node.IsExpanded = false;
                    break;
                case MoveDirection.Right:
                    treeItem.Node.IsExpanded = true;
                    break;
                case MoveDirection.Up:
                    if (item.Index > 0)
                    {
                        SelectComponentByIndex(item.Index - 1);
                    }
                    break;
                case MoveDirection.Down:
                    if (IsValid(item.Index + 1))
                    {
                        SelectComponentByIndex(item.Index + 1);
                    }
                    break;
            }
        }
    }
}