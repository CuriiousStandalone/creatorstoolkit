﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UIWidgets;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements
{
    public class CAccordion : BaseUIElement
    {
        [SerializeField]
        private Accordion _accordion = null;

        protected override void Init()
        {

        }

        private void Start()
        {
            if (null != _accordion)
            {
                _accordion = GetComponent<Accordion>();
            }
        }

        /// <summary>
        /// Function to add options to the accordion. This takes in two string paths that should direct the function to spawn a prefab from the resources folder.
        /// </summary>
        /// <param name="headerPath"></param>
        /// <param name="contentPath"></param>
        public void AddAccordionOption(string headerPath, string contentPath)
        {
            GameObject header = Instantiate(Resources.Load(headerPath) as GameObject, _accordion.transform);

            GameObject content = Instantiate(Resources.Load(contentPath) as GameObject, _accordion.transform);

            AccordionItem item = new AccordionItem()
            {
                ToggleObject = header,
                ContentObject = content,
                Open = false
            };

            _accordion.DataSource.Add(item);
        }
    }
}