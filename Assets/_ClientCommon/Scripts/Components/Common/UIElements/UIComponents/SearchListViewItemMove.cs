﻿using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents
{
    /// <summary>
    /// Copy class of ListViewItemMove.
    /// </summary>
    public class SearchListViewItemMove : UnityEvent<AxisEventData, SearchListViewItem>
    {
    }
}