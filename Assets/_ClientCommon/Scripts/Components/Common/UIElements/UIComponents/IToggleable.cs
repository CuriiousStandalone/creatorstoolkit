﻿using System;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements
{
    public interface IToggleable
    {
        CToggleManager Manager { get; set; }
        string Name { get; set; }
        bool IsSelected { get; set; }
        void OnOtherSelected();
        void Reset();
    }
}