﻿using PulseIQ.Local.ClientCommon.Components.Common.UIElements;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements
{
    /// <summary>
    /// Wraps UGUI Toggle for CUI. Interfaces with CToggleManager.
    /// </summary>
    [RequireComponent(typeof(Toggle))]
    public class CToggle : MonoBehaviour, IToggleable
    {
        private Toggle _toggle;

        public CToggleManager Manager { get; set; }

        [SerializeField]
        private TMP_Text _toggleLabel;

        private void Awake()
        {
            _toggle = GetComponent<Toggle>();
            _toggle.isOn = false;
            _toggle.onValueChanged.AddListener(OnToggled);

            if (null != _toggleLabel)
            {
                Name = _toggleLabel.text;
            }
        }

        private void OnToggled(bool value)
        {
            if (value)
            {
                Manager.Selected(this);
            }
        }

        public void OnOtherSelected()
        {
            _toggle.SetIsOnWithoutNotify(false);
        }

        public void Reset()
        {
            _toggle.SetIsOnWithoutNotify(false);
        }

        public bool IsSelected
        {
            get { return _toggle.isOn; }
            set { _toggle.SetIsOnWithoutNotify(value); }
        }

        public string Name { get; set; }
    }
}