﻿using UnityEngine.UI;
using UnityEngine.EventSystems;

using TMPro;

using UIWidgets;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIComponents
{
    /// <summary>
    /// controls card behaviour for search string items within the combobox list.
    /// Overrides Interactable behaviour to allow splitters.
    /// </summary>
    public class SearchStringListComponent : SearchListViewItem, IViewData<SearchListStringItem>
    {
        public TextMeshProUGUI Text;
        public Image Icon;

        public void SetData(SearchListStringItem item)
        {
            Text.text = item.Name;
            Interactable = item.DefaultInteractable;
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            base.OnPointerEnter(eventData);
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            base.OnPointerExit(eventData);
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            base.OnPointerClick(eventData);
        }
    }
}