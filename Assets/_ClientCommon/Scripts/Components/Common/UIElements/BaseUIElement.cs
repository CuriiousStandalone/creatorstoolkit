﻿using UnityEngine;

using System;
using PulseIQ.Local.ClientCommon.Extensions;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements
{
    /// <summary>
    /// A mostly complete implementation of IUIElement to reduce code repeating.
    /// </summary>
    public abstract class BaseUIElement : MonoBehaviour
    {
        [Header("IUIElement")]
        [ReadOnly]
        protected string _elementId;

        [SerializeField]
        protected float _defaultWidth;

        [SerializeField]
        protected float _defaultHeight;

        [SerializeField]
        [ReadOnly]
        protected float _currentWidth;


        [SerializeField]
        [ReadOnly]
        protected float _currentHeight;

        [SerializeField]
        protected float _minWidthLimit;

        [SerializeField]
        protected float _minHeightLimit;

        [SerializeField]
        protected Vector3 _defaultPosition;

        [SerializeField]
        [ReadOnly]
        protected Vector3 _currentPosition;

        [SerializeField]
        protected bool _isSelfResizeable;

        protected RectTransform _rectTransform;

        protected RectTransform RectTransform
        {
            get
            {
                if (_rectTransform == null)
                {
                    _rectTransform = GetComponent<RectTransform>();
                }

                return _rectTransform;
            }
        }

        public string ElementId
        {
            get
            {
                if(string.IsNullOrEmpty(_elementId))
                {
                    _elementId = Guid.NewGuid().ToString();
                }

                return _elementId;
            }
        }

        public float DefaultWidth
        {
            get
            {
                return _defaultWidth;
            }
        }

        public float DefaultHeight
        {
            get
            {
                return _defaultHeight;
            }
        }

        public float CurrentWidth
        {
            get
            {
                return RectTransform.sizeDelta.x;
            }

            set
            {
                RectTransform.SetWidth(value);
            }
        }

        public float CurrentHeight
        {
            get
            {
                return RectTransform.sizeDelta.y;
            }

            set
            {
                RectTransform.SetHeight(value);
            }
        }

        public float MinWidthLimit
        {
            get
            {
                return _minWidthLimit;
            }
        }

        public float MinHeightLimit
        {
            get
            {
                return _minHeightLimit;
            }
        }

        public Vector3 DefaultPosition
        {
            get
            {
                return _defaultPosition;
            }
        }

        public Vector3 CurrentPosition
        {
            get
            {
                return RectTransform.position;
            }

            set
            {
                RectTransform.position = value;
            }
        }

        public bool IsSelfResizeable
        {
            get
            {
                return _isSelfResizeable;
            }
        }

        protected abstract void Init();
    }
}