﻿namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements
{
    public enum EPanelLayout
    {
        NONE,
        LEFT,
        TOP,
        RIGHT,
        BOTTOM
    }
}
