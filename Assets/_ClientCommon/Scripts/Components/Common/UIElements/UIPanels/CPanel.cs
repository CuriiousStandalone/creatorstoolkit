﻿using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace PulseIQ.Local.ClientCommon.Components.Common.UIElements.UIPanels
{
    public class CPanel : BaseUIElement
    {
        private List<GameObject> _childrenUIElementsList;

        public EPanelLayout _panelLayout;

        public string _panelId;

        public Text _testingTextField;

        public UnityEvent OnResize;

        private void Awake()
        {
            if (_testingTextField != null)
            {
                _testingTextField.gameObject.SetActive(false);
            }

            _currentWidth = DefaultWidth;
            _currentHeight = DefaultHeight;
            _currentPosition = DefaultPosition;
        }

        protected override void Init()
        {
            
        }

        public void IncreaseHeightFromBottom(float height)
        {
            OnResize?.Invoke();
            RectTransform trans = GetComponent<RectTransform>();

            height += trans.rect.height;

            int screenHeight = Screen.height;

            if (height > screenHeight)
            {
                return;
            }

            Vector2 newSize = new Vector2(trans.rect.size.x, height);

            Vector2 oldSize = trans.rect.size;
            Vector2 deltaSize = newSize - oldSize;
            trans.offsetMin = trans.offsetMin - new Vector2(deltaSize.x * trans.pivot.x, deltaSize.y * trans.pivot.y) - new Vector2(deltaSize.x * (1f - trans.pivot.x), deltaSize.y * (1f - trans.pivot.y));
        }

        public void IncreaseHeightFromTop(float height)
        {
            OnResize?.Invoke();

            RectTransform trans = GetComponent<RectTransform>();

            height += trans.rect.height;

            Vector2 newSize = new Vector2(trans.rect.size.x, height);

            Vector2 oldSize = trans.rect.size;
            Vector2 deltaSize = newSize - oldSize;
            trans.offsetMax = trans.offsetMax + new Vector2(deltaSize.x * (1f - trans.pivot.x), deltaSize.y * (1f - trans.pivot.y)) + new Vector2(deltaSize.x * trans.pivot.x, deltaSize.y * trans.pivot.y);
        }

        public void IncreaseHeightEqually(float height)
        {
            OnResize?.Invoke();

            RectTransform trans = GetComponent<RectTransform>();

            height += trans.rect.height;

            Vector2 newSize = new Vector2(trans.rect.size.x, height);

            Vector2 oldSize = trans.rect.size;
            Vector2 deltaSize = newSize - oldSize;
            trans.offsetMin = trans.offsetMin - new Vector2(deltaSize.x * trans.pivot.x, deltaSize.y * trans.pivot.y);
            trans.offsetMax = trans.offsetMax + new Vector2(deltaSize.x * (1f - trans.pivot.x), deltaSize.y * (1f - trans.pivot.y));
        }

        public void DecreaseHeightFromBottom(float height)
        {
            OnResize?.Invoke();

            RectTransform trans = GetComponent<RectTransform>();

            height = trans.rect.height - height;

            if(height < _defaultHeight)
            {
                return;
            }

            Vector2 newSize = new Vector2(trans.rect.size.x, height);

            Vector2 oldSize = trans.rect.size;
            Vector2 deltaSize = newSize - oldSize;
            trans.offsetMin = trans.offsetMin - new Vector2(deltaSize.x * trans.pivot.x, deltaSize.y * trans.pivot.y) - new Vector2(deltaSize.x * (1f - trans.pivot.x), deltaSize.y * (1f - trans.pivot.y));
        }

        public void DecreaseHeightFromTop(float height)
        {
            OnResize?.Invoke();

            RectTransform trans = GetComponent<RectTransform>();

            height = trans.rect.height - height;

            if (height < _defaultHeight)
            {
                return;
            }

            Vector2 newSize = new Vector2(trans.rect.size.x, height);

            Vector2 oldSize = trans.rect.size;
            Vector2 deltaSize = newSize - oldSize;
            trans.offsetMax = trans.offsetMax + new Vector2(deltaSize.x * (1f - trans.pivot.x), deltaSize.y * (1f - trans.pivot.y)) + new Vector2(deltaSize.x * trans.pivot.x, deltaSize.y * trans.pivot.y);
        }

        public void DecreaseHeightEqually(float height)
        {
            OnResize?.Invoke();

            RectTransform trans = GetComponent<RectTransform>();

            height = trans.rect.height - height;

            if (height < _defaultHeight)
            {
                return;
            }

            Vector2 newSize = new Vector2(trans.rect.size.x, height);

            Vector2 oldSize = trans.rect.size;
            Vector2 deltaSize = newSize - oldSize;
            trans.offsetMin = trans.offsetMin - new Vector2(deltaSize.x * trans.pivot.x, deltaSize.y * trans.pivot.y);
            trans.offsetMax = trans.offsetMax + new Vector2(deltaSize.x * (1f - trans.pivot.x), deltaSize.y * (1f - trans.pivot.y));
        }

        public void RepositionToTop(float height)
        {
            RectTransform trans = GetComponent<RectTransform>();

            trans.localPosition = new Vector3(trans.localPosition.x, trans.localPosition.y - height, trans.localPosition.z);
        }

        public void RepositionToBottom(float height)
        {
            RectTransform trans = GetComponent<RectTransform>();

            trans.localPosition = new Vector3(trans.localPosition.x, trans.localPosition.y + height, trans.localPosition.z);
        }

        public void RepositionToLeft(float width)
        {
            RectTransform trans = GetComponent<RectTransform>();

            trans.localPosition = new Vector3(trans.localPosition.x - width, trans.localPosition.y, trans.localPosition.z);
        }

        public void RepositionToRight(float width)
        {
            RectTransform trans = GetComponent<RectTransform>();

            trans.localPosition = new Vector3(trans.localPosition.x + width, trans.localPosition.y, trans.localPosition.z);
        }

        private void OnRectTransformDimensionsChange()
        {
            OnResize?.Invoke();
        }

    }
}
