﻿using UnityEngine;

namespace PulseIQ.Local.ClientCommon.Components.Common
{
    public class CustomCameraRotation : MonoBehaviour
    {

        public float horizontalSpeed = 150f;
        public float verticalSpeed = 150f;

        private float yaw = 0.0f;
        private float pitch = 0.0f;

        // Use this for initialization
        void Start()
        {
            yaw = transform.eulerAngles.y;
            pitch = transform.eulerAngles.x;
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetMouseButton(1))
            {
                yaw += horizontalSpeed * Input.GetAxis("Mouse X") * Time.deltaTime;
                pitch -= verticalSpeed * Input.GetAxis("Mouse Y") * Time.deltaTime;

                transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
            }

            if (Input.GetMouseButtonDown(1))
            {
                Cursor.lockState = CursorLockMode.Locked;
            }

            if (Input.GetMouseButtonUp(1))
            {
                Cursor.lockState = CursorLockMode.None;
            }
        }

        public void ResetTransform()
        {
            yaw = transform.eulerAngles.y;
            pitch = transform.eulerAngles.x;
        }
    }
}