﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using PulseIQ.Local.Common;

namespace PulseIQ.Local.ClientCommon.Components.Common
{
    [System.Serializable]
    public class BaseJsonData
    {
        public int versionNumber;
        public string xmlurl;
    }

    [System.Serializable]
    public class ResourceJsonData : BaseJsonData
    {
        public int resourceId;
        public string givenRId;
        public string resourceName;
        public ItemTypeCode itemType;
        public bool isDirectory;
        public string multimediaId;
    }

    [System.Serializable]
    public class PostProcessingJsonData : BaseJsonData
    {
        public string postProcessingId;
        public string postProcessingName;
        public bool isDirectory;
    }

    [System.Serializable]
    public class SkyboxJsonData : BaseJsonData
    {
        public string skyboxId;
        public string skyboxName;
        public bool isDirectory;
    }

    [System.Serializable]
    public class VirtualDirectoryJsonData
    {
        public string virtualDirectoryID;
        public string directoryName;
        public string description;
        public string xmlurl;
    }

    [System.Serializable]
    public class ResourceData
    {
        public string givenRID;
        public string multimediaID;
        public int itemType;
        public int versionNumber = -1;
    }

    [System.Serializable]
    public class PostProcessingSerializableData
    {
        public string postProcessingID;
        public int versionNumber = -1;
    }

    [System.Serializable]
    public class SkyboxSerializableData
    {
        public string skyboxID;
        public int versionNumber = -1;
    }

    [System.Serializable]
    public class GeneralResponse
    {
        public string status;
        public string message;
    }

    [System.Serializable]
    public class ThumbnailData
    {
        public string path;
        public int itemType;
    }

    [System.Serializable]
    public class PackageDataJson
    {
        public string packageId;
        public List<string> worldList;
    }

    public class JsonHelper
    {
        [SerializeField]
        private static ResourceData[] dataList;

        [System.Serializable]
        private class Wrapper<T>
        {
            public T[] data;
        }

        [System.Serializable]
        private class WrapperObj<T>
        {
            public T data;
        }

        public static T[] GetJsonArray<T>(string json)
        {
            string newJson = "{ \"data\": " + json + "}";
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(newJson);
            return wrapper.data;
        }

        public static T GetJsonObject<T>(string json)
        {
            string newJson = "{ \"data\": " + json + "}";
            WrapperObj<T> wrapper = JsonUtility.FromJson<WrapperObj<T>>(newJson);
            return wrapper.data;
        }

        public static string GetJsonString(ResourceData[] rdList)
        {
            Wrapper<ResourceData> wrd = new Wrapper<ResourceData>();
            wrd.data = rdList;
            string str = JsonUtility.ToJson(wrd);
            return str;
        }

        public static string GetJsonString(PostProcessingSerializableData[] rdList)
        {
            Wrapper<PostProcessingSerializableData> wrd = new Wrapper<PostProcessingSerializableData>();
            wrd.data = rdList;
            string str = JsonUtility.ToJson(wrd);
            return str;
        }

        public static string GetJsonString(SkyboxSerializableData[] rdList)
        {
            Wrapper<SkyboxSerializableData> wrd = new Wrapper<SkyboxSerializableData>();
            wrd.data = rdList;
            string str = JsonUtility.ToJson(wrd);
            return str;
        }

        public static T[] GetDirectoryJsonArray<T>(string json)
        {
            string newJson = "{ \"data\": " + json + "}";
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(newJson);
            return wrapper.data;
        }
    }
}
