﻿using UnityEngine;

namespace PulseIQ.Local.ClientCommon.Components.Common.Model
{
    public interface ICommonModel
    {
        GameObject loadingScreen { get; set; }

        GameObject progressBar { get; set; }

        GameObject applicationTypeGO { get; set; }
    }
}
