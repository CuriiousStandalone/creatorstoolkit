﻿using UnityEngine;

namespace PulseIQ.Local.ClientCommon.Components.Common.Model
{
    public class CommonModel : ICommonModel
    {
        public GameObject loadingScreen { get; set; }

        public GameObject progressBar { get; set; }

        public GameObject applicationTypeGO { get; set; }
    }
}
