﻿namespace PulseIQ.Local.ClientCommon.Components.Common
{
    public enum EApplicationType
    {
        CONTROL_CLIENT,
        PLAYER_CLIENT,
        CREATOR_TOOL_KIT,
    }
}
