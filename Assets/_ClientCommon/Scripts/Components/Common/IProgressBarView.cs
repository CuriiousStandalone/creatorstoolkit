﻿using UnityEngine;

namespace PulseIQ.Local.ClientCommon.Components.Common
{
    public interface IProgressBarView
    {
        void ShowProgressBar();

        void HideProgressBar();

        void UpdateProgress(float val);

        void UpdateProgressText(string val);
    }
}
