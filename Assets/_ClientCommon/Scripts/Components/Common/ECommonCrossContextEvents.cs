﻿namespace PulseIQ.Local.ClientCommon.Components.Common
{
    public enum ECommonCrossContextEvents : byte
    {
        ALL_ITEMS_CREATED,
        
        //Config
        SCAN_IP_CONNECTIONS,
        SCAN_IPS_FINSIHED,
        CHECK_GLOBAL_SETTINGS_SERVER_IP,
        BAD_GLOABL_IP_SETTINGS,
        CHECK_GLOBAL_SETTINGS_SERVER_IP_FINISHED,
        SHOW_IP_SELECTION,
        SET_NEW_IP,
        SET_NEW_USERID,
        RECONNECT_TO_WORLD,
        SHOWING_MEDIA_AFTER_SYNC,
        SHOW_ALL_UI,
        HIDE_ALL_UI,
        RETRY_LOGIN,
        DISCONNECTED,
        CONNECT_SHOW_ALL_UI,
    }
}