﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.Components.Common 
{
    public interface IHideShowView
    {
        void HideView();

        void ShowView();
    }
}