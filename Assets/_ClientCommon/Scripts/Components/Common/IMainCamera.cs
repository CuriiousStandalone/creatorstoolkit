﻿using UnityEngine;

namespace PulseIQ.Local.ClientCommon.Components.Common
{
    public interface IMainCamera
    {
        Vector3 ReturnCameraHoldingPosition();

        Quaternion ReturnCameraHoldingRotation();

        Vector3 ReturnCameraHoldingScale();

        Camera ReturnMainCamera();

        void RotateCameraWorldZero(bool value);
    }
}