using System;

using UnityEngine;

using TMPro;

namespace PulseIQ.Local.ClientCommon.Components.Common
{
    public class GenericRenameDialog : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI _dialog;

        [SerializeField]
        private TextMeshProUGUI _currentName;

        [SerializeField]
        private TMP_InputField _inputField;

        [SerializeField]
        private TextMeshProUGUI _submitBtnText;

        [SerializeField]
        private TextMeshProUGUI _cancelBtnText;

        private Action<string> _onSubmit;
        private Action _onCancel;

        public void Initialize(string dialog, string currentName, Action<string> onSubmit, Action onCancel = null, string submitText = "Submit", string cancelText = "Cancel")
        {
            _dialog.text = dialog;
            _currentName.text = currentName;
            _inputField.text = currentName;
            _onSubmit = onSubmit;
            _onCancel = onCancel;
            _submitBtnText.text = submitText;
            _cancelBtnText.text = cancelText;
            gameObject.SetActive(true);
        }

        public void OnSubmit()
        {
            gameObject.SetActive(false);
            _onSubmit?.Invoke(_inputField.text);
        }

        public void OnCancel()
        {
            gameObject.SetActive(false);
            _onCancel?.Invoke();
        }
    }
}