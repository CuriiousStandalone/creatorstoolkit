﻿using strange.extensions.command.impl;
using PulseIQ.Local.Common;

namespace PulseIQ.Local.ClientCommon.Components.Common
{
    public class LogicCommand : EventCommand
    {
        public override void Execute()
        {
            switch ((EventCode)evt.type)
            {
			    case EventCode.ItemCreated:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.ITEM_CREATED, evt.data);
			    		break;
			    	}

			    case EventCode.ItemMoved:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.ITEM_MOVED, evt.data);
			    		break;
			    	}

                case EventCode.ItemScaled:
                    {
                        dispatcher.Dispatch(ServerCmdCode.ITEM_ROTATED, evt.data);
                        break;
                    }

                case EventCode.ItemRotated:
                    {
                        dispatcher.Dispatch(ServerCmdCode.ITEM_SCALED, evt.data);
                        break;
                    }

                case EventCode.ItemShown:
                    {
                        dispatcher.Dispatch(ServerCmdCode.ITEM_SHOWN, evt.data);
                        break;
                    }

                case EventCode.ItemHidden:
                    {
                        dispatcher.Dispatch(ServerCmdCode.ITEM_HIDDEN, evt.data);
                        break;
                    }

                case EventCode.AvatarCreated:
			        {
			    	    dispatcher.Dispatch(ServerCmdCode.AVATAR_CREATED, evt.data);
			    	    break;
			        }

			    case EventCode.TeleportClassToWorld:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.TeleportClassToWorld, evt.data);
			    		break;
			    	}

			    case EventCode.AvatarHeadRotated:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.AVATAR_HEAD_ROTATED, evt.data);
			    		break;
			    	}

                case EventCode.ItemDestroyed:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.ITEM_DESTROYED, evt.data);
			    		break;
			    	}

                case EventCode.ItemAllDestroyed:
                    {
                        dispatcher.Dispatch(ServerCmdCode.ALL_ITEM_DESTROYED);
                        break;
                    }

                case EventCode.CloseAllMultimedia:
                    {
                        dispatcher.Dispatch(ServerCmdCode.ALL_MULTIMEDIA_CLOSED, evt.data);
                        break;
                    }

                case EventCode.PanoramaCreated:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.PANORAMA_CREATED, evt.data);
			    		break;
			    	}

			    case EventCode.PanoramaHidden:
			    	{
			    		//dispatcher.Dispatch(ServerCmdCode.PANORAMA_HIDDEN, evt.data);
			    		break;
			    	}

			    case EventCode.PanoramaShown:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.PANORAMA_SHOWN, evt.data);
			    		break;
			    	}

			    case EventCode.PanoramaVideoCreated:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.PANORAMA_VIDEO_CREATED, evt.data);
			    		break;
			    	}

			    case EventCode.PanoramaVideoHidden:
			    	{
			    		//dispatcher.Dispatch(ServerCmdCode.PANORAMA_VIDEO_HIDDEN, evt.data);
			    		break;
			    	}

			    case EventCode.PanoramaVideoPaused:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.PANORAMA_VIDEO_PAUSED, evt.data);
			    		break;
			    	}

			    case EventCode.PanoramaVideoPlayed:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.PANORAMA_VIDEO_PLAYED, evt.data);
			    		break;
			    	}

			    case EventCode.PanoramaVideoReplayed:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.PANORAMA_VIDEO_REPLAYED, evt.data);
			    		break;
			    	}

			    case EventCode.PanoramaVideoShown:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.PANORAMA_VIDEO_SHOWN, evt.data);
			    		break;
			    	}

			    case EventCode.PanoramaVideoSought:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.PANORAMA_VIDEO_SOUGHT, evt.data);
			    		break;
			    	}

			    case EventCode.PanoramaVideoStopped:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.PANORAMA_VIDEO_STOPPED, evt.data);
			    		break;
			    	}

                case EventCode.StereoPanoramaVideoCreated:
                    {
                        dispatcher.Dispatch(ServerCmdCode.STEREO_PANORAMA_VIDEO_CREATED, evt.data);
                        break;
                    }

                case EventCode.StereoPanoramaVideoHidden:
                    {
                        //dispatcher.Dispatch(ServerCmdCode.STEREO_PANORAMA_VIDEO_HIDDEN, evt.data);
                        break;
                    }

                case EventCode.StereoPanoramaVideoPaused:
                    {
                        dispatcher.Dispatch(ServerCmdCode.STEREO_PANORAMA_VIDEO_PAUSED, evt.data);
                        break;
                    }

                case EventCode.StereoPanoramaVideoPlayed:
                    {
                        dispatcher.Dispatch(ServerCmdCode.STEREO_PANORAMA_VIDEO_PLAYED, evt.data);
                        break;
                    }

                case EventCode.StereoPanoramaVideoReplayed:
                    {
                        dispatcher.Dispatch(ServerCmdCode.STEREO_PANORAMA_VIDEO_REPLAYED, evt.data);
                        break;
                    }

                case EventCode.StereoPanoramaVideoShown:
                    {
                        dispatcher.Dispatch(ServerCmdCode.STEREO_PANORAMA_VIDEO_SHOWN, evt.data);
                        break;
                    }

                case EventCode.StereoPanoramaVideoSought:
                    {
                        dispatcher.Dispatch(ServerCmdCode.STEREO_PANORAMA_VIDEO_SOUGHT, evt.data);
                        break;
                    }

                case EventCode.StereoPanoramaVideoStopped:
                    {
                        dispatcher.Dispatch(ServerCmdCode.STEREO_PANORAMA_VIDEO_STOPPED, evt.data);
                        break;
                    }

                case EventCode.PlanarVideoCreated:
			    {
			    	dispatcher.Dispatch(ServerCmdCode.PLANAR_VIDEO_CREATED, evt.data);
			    	break;
			    }

			    case EventCode.PlanarVideoHidden:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.PLANAR_VIDEO_HIDDEN, evt.data);
			    		break;
			    	}

			    case EventCode.PlanarVideoPaused:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.PLANAR_VIDEO_PAUSED, evt.data);
			    		break;
			    	}

			    case EventCode.PlanarVideoPlayed:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.PLANAR_VIDEO_PLAYED, evt.data);
			    		break;
			    	}

			    case EventCode.PlanarVideoReplayed:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.PLANAR_VIDEO_REPLAYED, evt.data);
			    		break;
			    	}

			    case EventCode.PlanarVideoShown:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.PLANAR_VIDEO_SHOWN, evt.data);
			    		break;
			    	}

			    case EventCode.PlanarVideoSought:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.PLANAR_VIDEO_SOUGHT, evt.data);
			    		break;
			    	}

			    case EventCode.PlanarVideoStopped:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.PLANAR_VIDEO_STOPPED, evt.data);
			    		break;
			    	}

			    case EventCode.LineNotationClear:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.NOTATION_CLEARED, evt.data);
			    		break;
			    	}

			    case EventCode.LineNotationColorUpdated:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.NOTATION_COLOR_UPDATED, evt.data);
			    		break;
			    	}

			    case EventCode.LineNotationCreated:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.NOTATION_CREATED, evt.data);
			    		break;
			    	}

			    case EventCode.LineNotationHide:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.NOTATION_HIDDEN, evt.data);
			    		break;
			    	}

			    case EventCode.LineNotationLineRemoved:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.NOTATION_LINE_REMOVED, evt.data);
			    		break;
			    	}

			    case EventCode.LineNotationShow:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.NOTATION_SHOWN, evt.data);
			    		break;
			    	}

			    case EventCode.LineNotationStarted:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.NOTATION_STARTED, evt.data);
			    		break;
			    	}

			    case EventCode.LineNotationStopped:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.NOTATION_STOPPED, evt.data);
			    		break;
			    	}

			    case EventCode.LineNotationThicknessUpdated:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.NOTATION_THICKNESS_UPDATED, evt.data);
			    		break;
			    	}

			    case EventCode.LineNotationTouchPointsAdded:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.NOTATION_ADD_TOUCH_POINTS, evt.data);
			    		break;
			    	}

			    case EventCode.InteractiveItemCreated:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.INTERACTIVE_CREATED, evt.data);
			    		break;
			    	}

			    case EventCode.InteractiveItemPlacedToSlot:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.INTERACTIVE_SLOT_PLACED, evt.data);
			    		break;
			    	}

			    case EventCode.InteractiveItemPlaceToSlotDenied:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.INTERACTIVE_SLOT_DENIED, evt.data);
			    		break;
			    	}

			    case EventCode.SlotClosed:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.SLOT_CLOSED, evt.data);
			    		break;
			    	}

			    case EventCode.SlotCreated:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.SLOT_CREATED, evt.data);
			    		break;
			    	}

			    case EventCode.SlotIndicatorHidden:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.SLOT_INDICATOR_HIDDEN, evt.data);
			    		break;
			    	}

			    case EventCode.SlotIndicatorShown:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.SLOT_INDICATOR_SHOWN, evt.data);
			    		break;
			    	}

			    case EventCode.SlotOpened:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.SLOT_OPENED, evt.data);
			    		break;
			    	}

                case EventCode.SlotStateAniPlayed:
                    {
                        dispatcher.Dispatch(ServerCmdCode.SLOT_STATE_ANI_PLAYED, evt.data);
                        break;
                    }

                case EventCode.SlotStateAniStopped:
                    {
                        dispatcher.Dispatch(ServerCmdCode.SLOT_STATE_ANI_STOPPED, evt.data);
                        break;
                    }

			    case EventCode.SyncTimerCreated:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.TIMER_CREATED, evt.data);
			    		break;
			    	}

			    case EventCode.SyncTimerElapsed:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.TIMER_ELAPSED, evt.data);
			    		break;
			    	}

			    case EventCode.SyncTimerPaused:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.TIMER_PAUSED, evt.data);
			    		break;
			    	}

			    case EventCode.SyncTimerEnded:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.TIMER_ENDED, evt.data);
			    		break;
			    	}

			    case EventCode.SyncTimerReset:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.TIMER_RESET, evt.data);
			    		break;
			    	}

			    case EventCode.SyncTimerStarted:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.TIMER_STARTED, evt.data);
			    		break;
			    	}

			    case EventCode.TutorialItemActivated:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.TUTORIAL_ACTIVATED, evt.data);
			    		break;
			    	}

			    case EventCode.TutorialItemAnimationPaused:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.TUTORIAL_PAUSED, evt.data);
			    		break;
			    	}

			    case EventCode.TutorialItemAnimationStarted:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.TUTORIAL_STARTED, evt.data);
			    		break;
			    	}

			    case EventCode.TutorialItemAnimationStopped:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.TUTORIAL_STOPPED, evt.data);
			    		break;
			    	}

			    case EventCode.TutorialItemCreated:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.TUTORIAL_CREATED, evt.data);
			    		break;
			    	}

			    case EventCode.TutorialItemDeactivated:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.TUTORIAL_DEACTIVATED, evt.data);
			    		break;
			    	}

			    case EventCode.ItemAbandoned:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.ITEM_ABANDONED, evt.data);
			    		break;
			    	}

			    case EventCode.ItemObtainDenied:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.ITEM_OBTAIN_FAILED, evt.data);
			    		break;
			    	}

			    case EventCode.ItemObtained:
			    	{
			    		dispatcher.Dispatch(ServerCmdCode.ITEM_OBTAIN_SUCCESS, evt.data);
			    		break;
			    	}

                case EventCode.AvatarAnimationPlayed:
                    {
                        dispatcher.Dispatch(ServerCmdCode.AVATAR_ANI_PLAYED, evt.data);
                        break;
                    }

                case EventCode.ItemOfflineMoved:
                    {
                        dispatcher.Dispatch(ServerCmdCode.ITEM_OFFLINE_MOVED, evt.data);
                        break;
                    }

                case EventCode.ItemOfflineRotated:
                    {
                        dispatcher.Dispatch(ServerCmdCode.ITEM_OFFLINE_ROTATED, evt.data);
                        break;
                    }

                case EventCode.ItemOfflineScaled:
                    {
                        dispatcher.Dispatch(ServerCmdCode.ITEM_OFFLINE_SCALED, evt.data);
                        break;
                    }

                case EventCode.ItemLerpMoved:
                    {
                        dispatcher.Dispatch(ServerCmdCode.ITEM_LERP_MOVED, evt.data);
                        break;
                    }

                case EventCode.ItemLerpRotated:
                    {
                        dispatcher.Dispatch(ServerCmdCode.ITEM_LERP_ROTATED, evt.data);
                        break;
                    }

                case EventCode.ItemLerpScaled:
                    {
                        dispatcher.Dispatch(ServerCmdCode.ITEM_LERP_SCALED, evt.data);
                        break;
                    }

                case EventCode.ItemStatusSynced:
                    {
                        dispatcher.Dispatch(ServerCmdCode.ITEM_STATUS_SYNCED, evt.data);
                        break;
                    }

                    case EventCode.UICreated:
                    {
                        dispatcher.Dispatch(ServerCmdCode.UI_CREATED, evt.data);
                        break;
                    }

                case EventCode.SlotGroupCreated:
                    {
                        dispatcher.Dispatch(ServerCmdCode.SLOT_GROUP_CREATED, evt.data);
                        break;
                    }

                case EventCode.SlotGroupEvaluated:
                    {
                        dispatcher.Dispatch(ServerCmdCode.SLOT_GROUP_EVALUATED, evt.data);
                        break;
                    }

                case EventCode.SlotGroupFinishAniPlayed:
                    {
                        dispatcher.Dispatch(ServerCmdCode.SLOT_GROUP_FINISH_ANI_PLAYED, evt.data);
                        break;
                    }

                case EventCode.SubSlotGroupActivated:
                    {
                        dispatcher.Dispatch(ServerCmdCode.SLOT_GROUP_SUBSLOTGROUP_ACTIVATED, evt.data);
                        break;
                        }

                case EventCode.SlotGroupEvaluationEnabled:
                    {
                        dispatcher.Dispatch(ServerCmdCode.SLOT_GROUP_EVALUATION_ENABLED, evt.data);
                        break;
                    }

                case EventCode.SlotGroupEvaluationDisabled:
                    {
                        dispatcher.Dispatch(ServerCmdCode.SLOT_GROUP_EVALUATION_DISABLED, evt.data);
                        break;
                    }

                case EventCode.SlotGroupStatusSynced:
                    {
                        dispatcher.Dispatch(ServerCmdCode.SLOT_GROUP_STATUS_SYNC, evt.data);
                        break;
                    }

                case EventCode.EnableHotspot:
                    {
                        dispatcher.Dispatch(ServerCmdCode.HOTSPOT_ENABLED, evt.data);
                        break;
                    }

                case EventCode.DisableHotspot:
                    {
                        dispatcher.Dispatch(ServerCmdCode.HOTSPOT_DISABLED, evt.data);
                        break;
                    }

                case EventCode.HotspotTeleport:
                    {
                        dispatcher.Dispatch(ServerCmdCode.HOTSPOT_TELEPORTED, evt.data);
                        break;
                    }

                case EventCode.EnableHotspotStatus:
                    {
                        dispatcher.Dispatch(ServerCmdCode.HOTSPOT_STATUS_ENABLED, evt.data);
                        break;
                    }

                case EventCode.Teleported:
                    {
                        dispatcher.Dispatch(ServerCmdCode.ALL_MULTIMEDIA_CLOSED);
                        break;
                    }

                case EventCode.EndBehaviorTeleport:
                    {
                        dispatcher.Dispatch(ServerCmdCode.END_BEHAVIOUR_TELEPORTED, evt.data);
                        break;
                    }

                case EventCode.Disconnected:
                    {
                        dispatcher.Dispatch(ServerCmdCode.DISCONNECTED);
                        break;
                    }

                case EventCode.ClientReconnected:
                    {
                        dispatcher.Dispatch(ServerCmdCode.CLIENT_RECONNECTED, evt.data);
                        break;
                    }

                case EventCode.LoginFailed:
                    {
                        dispatcher.Dispatch(ServerCmdCode.LOGIN_FAILED, evt.data);
                        break;
                    }

                case EventCode.PanoramaStatusSynced:
                    {
                        dispatcher.Dispatch(ServerCmdCode.SYNC_PANORAMA_STATUS, evt.data);
                        break;
                    }

                case EventCode.PanoramaVideoStatusSynced:
                    {
                        dispatcher.Dispatch(ServerCmdCode.SYNC_PANORAMA_VIDEO_STATUS, evt.data);
                        break;
                    }

                case EventCode.StereoPanoramaVideoStatusSynced:
                    {
                        dispatcher.Dispatch(ServerCmdCode.SYNC_STEREO_PANORAMA_VIDEO_STATUS, evt.data);
                        break;
                    }

                case EventCode.LineNotationStatusSynced:
                    {
                        dispatcher.Dispatch(ServerCmdCode.SYNC_NOTATION_STATUS, evt.data);
                        break;
                    }

                case EventCode.TutorialItemStatusSynced:
                    {
                        dispatcher.Dispatch(ServerCmdCode.SYNC_TUTORIAL_ITEM_STATUS, evt.data);
                        break;
                    }

                default:
				break;
            }
        }
    }
}