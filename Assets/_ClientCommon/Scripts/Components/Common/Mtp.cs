﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;
using PortableDevices;

public class Mtp : MonoBehaviour
{


    // Start is called before the first frame update
    void Start()
    {
        //Creating instances
        //PortableDevice tablet;

        //foreach(PortableDevice device in devices)
        //{
        //    device.Connect();
        //
        //    Debug.Log("Information  1:  " + device.FriendlyName);
        //    Debug.Log("Information  2:  " + device.ToString());
        //    Debug.Log("Information  3:  " + device.DeviceId);
        //    Debug.Log("Information  4:  " + device.GetType());
        //
        //    device.Disconnect();
        //}

        ////Connect to MTP devices and pick up the first one
        //tablet = devices[0];
        //tablet.Connect();
        //
        //Debug.Log(tablet.FriendlyName);
        //Debug.Log(tablet.DeviceId);
        //
        ////Getting root directory
        //PortableDeviceFolder root = tablet.GetContents();
        //PortableDeviceFolder internalStorage = root.Files[0] as PortableDeviceFolde
        //
        //
        //PortableDeviceFolder curiiousFolder = GetSubFolder(internalStorage, "CuriiousIQ");
        //PortableDeviceFolder modulesFolder = GetSubFolder(curiiousFolder, "Modules");
        //
        ////Now that we have the modules folder, transfer something to it.
        //
        //tablet.TransferFileToDevice(@"C:\Users\phil.s.THEPULSE\Documents\CuriiousIQ\old\smallModule.ciqmodule", modulesFolder.Id);
        //
        //
        ////Close the connection
        //tablet.Disconnect();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    PortableDeviceFolder GetSubFolder(PortableDeviceFolder pdf, string folderName)
    {
        PortableDeviceFolder ret = null;

        foreach (var f in pdf.Files)
        {

            if (f is PortableDeviceFolder)
            {
                if (f.Name == folderName)
                {
                    ret = f as PortableDeviceFolder;
                }

            }
        }

        return ret;
    }

}
