﻿using UnityEngine;

namespace PulseIQ.Local.ClientCommon.Components.Common
{
    public interface IGenericDialogView
    {
        GameObject GetGenericDialog();
    }
}