﻿using UnityEngine;

namespace PulseIQ.Local.ClientCommon.Components.Common
{
    public interface IMediaBasic
    {
        string MediaName
        {
            get;
            set;
        }

        void LoadMedia(string path);

        void UnloadMedia();

        void ShowMedia(Vector3 cameraPos);

        void ShowMedia(Vector3 cameraPos, Quaternion cameraRot);

        void HideMedia();

        void TeleportMedia(string hotspotId, int targetItemIndex);

        bool IsMultiplayer { get; set; }
    }
}