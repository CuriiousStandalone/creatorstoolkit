﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.IO;
using PulseIQ.Local.Common;

namespace PulseIQ.Local.ClientCommon.Components.Common
{
    public class CMSSettings
    {
        private static string _tempDownloadPath = "TempDownload";

        private static string _packageApiUrl = "api/packages/";
        private static string _directoryApiUrl = "api/directories/";
        private static string _worldApiUrl = "api/worlds/";
        private static string _resourceApiUrl = "api/resource/";

        public static string CMSUrl
        {
            get
            {
                return GlobalSettings.CMSURL;
            }
        }

        public static string PackageApiUrl
        {
            get
            {
                return GlobalSettings.CMSURL + _packageApiUrl;
            }
        }

        public static string DirectoryApiUrl
        {
            get
            {
                return GlobalSettings.CMSURL + _directoryApiUrl;
            }
        }

        public static string WorldApiUrl
        {
            get
            {
                return GlobalSettings.CMSURL + _worldApiUrl;
            }
        }

        public static string ResourceApiUrl
        {
            get
            {
                return GlobalSettings.CMSURL + _resourceApiUrl;
            }
        }

        public static string TempDownloadPath
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer)
                {
                    return _tempDownloadPath;
                }
                else
                {
                    return Directory.GetParent(Application.persistentDataPath).Parent.Parent.Parent.FullName + "/PulseIQ/PackageData/Cache/";
                }
                
            }
        }

        public static string PlatformType
        {
            get
            {
                if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer)
                {
                    return "windows";
                }
                else
                {
                    return "android";             
                }
            }
        }
    }
}
