using System;

using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

using TMPro;

namespace PulseIQ.Local.ClientCommon.Components.Common
{
    public class GenericPopupDialogTMPro : MonoBehaviour
    {
        [FormerlySerializedAs("headingText")]
        [SerializeField]
        private TextMeshProUGUI _headingText;

        [FormerlySerializedAs("bodyText")]
        [SerializeField]
        private TextMeshProUGUI _bodyText;

        [FormerlySerializedAs("leftButtonText")]
        [SerializeField]
        private TextMeshProUGUI _leftButtonText;

        [FormerlySerializedAs("rightButtonText")]
        [SerializeField]
        private TextMeshProUGUI _rightButtonText;

        [FormerlySerializedAs("centerButtonText")]
        [SerializeField]
        private TextMeshProUGUI _centerButtonText;

        [FormerlySerializedAs("leftButton")]
        [Space]
        [SerializeField]
        private Button _leftButton;

        [FormerlySerializedAs("rightButton")]
        [Space]
        [SerializeField]
        private Button _rightButton;

        [FormerlySerializedAs("centerButton")]
        [Space]
        [SerializeField]
        private Button _centerButton;

        private Action _leftButtonAction;
        private Action _rightButtonAction;
        private Action _centerButtonAction;

        public GenericPopupDialogTMPro(Action centerButtonAction)
        {
            _centerButtonAction = centerButtonAction;
        }

        public void InitializeDialog(string headingTxt, string bodyTxt, string buttonText, Action buttonCallBack)
        {
            _headingText.text = headingTxt;
            _bodyText.text = bodyTxt;
            _rightButtonText.text = buttonText;
            if (_centerButtonText != null)
            {
                _centerButton.gameObject.SetActive(false);
            }
            _leftButton.gameObject.SetActive(false);
            _rightButton.gameObject.SetActive(true);

            _rightButtonAction = buttonCallBack;
            gameObject.SetActive(true);
        }

        public void InitializeDialog(string headingTxt, string bodyTxt, string buttonLeftTxt, string buttonRightTxt, Action buttonLeftCallBack, Action buttonRightCallBack)
        {
            _headingText.text = headingTxt;
            _bodyText.text = bodyTxt;
            _leftButtonText.text = buttonLeftTxt;
            _rightButtonText.text = buttonRightTxt;

            _leftButton.gameObject.SetActive(true);
            _rightButton.gameObject.SetActive(true);
            if (_centerButtonText != null)
            {
                _centerButton.gameObject.SetActive(false);
            }

            _leftButtonAction = buttonLeftCallBack;
            _rightButtonAction = buttonRightCallBack;
            gameObject.SetActive(true);
        }

        public void LeftButton_ClickHandler()
        {
            _leftButtonAction?.Invoke();
            gameObject.SetActive(false);
        }

        public void RightButton_ClickHandler()
        {

            _rightButtonAction?.Invoke();
            gameObject.SetActive(false);
        }

        public void CenterButton_ClickHandler()
        {
            _centerButtonAction?.Invoke();
            gameObject.SetActive(false);
        }
    }
}