﻿using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public class IntCmdMessageRelayItemManager : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(IntCmdCodeItemManager.RELAY_MESSAGE, evt.data);
        }
    }
}