﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.Components.Common
{
    public enum ECommonViewEvent : byte
    {
        MEDIA_CURRENT_TIME,
        MEDIA_SHOW,
        MEDIA_HIDE,
        MEDIA_HIDDEN,
        CLOSE_MEDIA,

        DROPDOWN_ADD_TO_LIST,
        DROPDOWN_CLEAR,

        NOTATION_SHOW,
        NOTATION_HIDE,
        NOTATION_SHOWN,
        NOTATION_HIDDEN,
        NOTATION_UI_CLOSED,
        NOTATION_PRESSED,
        NOTATION_DRAWDISTANCE_SET,
        NOTATION_ON,
        NOTATION_OFF,

        CAMERA_DATA,

        ITEM_ABANDONED,
        ITEM_OBTAINED,

        SHOW_VR,
        HIDE_VR,
        SET_POST_PROCESSING,

        TUTORIAL_OBTAINED,
        TUTORIAL_ABANDONED,
        TUTORIAL_ACTIVATED,
        TUTORIAL_DEACTIVATED,
        TUTORIAL_DATA,
        TUTORIAL_ANIM_STARTED,
        WORLD_NOTATION_STARTED,
        WORLD_NOTATION_ENDED,

        TIMER_ACTIVATED,
        TIMER_DEACTIVATED,

        WORLD_NOTATE_END,
        WORLD_NOTATE_START,

        WORLD_XML_DATA_RECEIVED,
        SLOT_CLOSED,
        THROWING_INTERACTIVE_ITEM,

        PLAY_AVATAR_WAVE_ANIMATION,
        DROPDOWN_ITEM_HIDDEN,

        OBSERVE_SLOT_ITEM,
        STOP_OBSERVE_SLOT_ITEM,
        SLOT_GROUP_EVALUATE,
        FADE_CAMERA_IN,
        FADE_CAMERA_IN_OUT,
        FADE_CAMERA_OUT,

        HOTSPOT_TELEPORT,
        HOTSPOT_SELECTED,
        HOTSPOT_DESELECTED,
        HOTSPOT_STATUS,
        
        MEDIA_END_BEHAVIOUR
    }
}