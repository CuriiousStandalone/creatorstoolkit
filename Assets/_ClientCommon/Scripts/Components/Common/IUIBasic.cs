﻿using System;

namespace PulseIQ.Local.ClientCommon.Components.Common
{
    public interface IUIBasic
    {
        void OnMessageReceived(Object data);

        string ReturnItemID();

        string ReturnMediaID();
    }
}