﻿using System.Collections.Generic;
using UnityEngine;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.CustomType;
using PulseIQ.Local.ClientCommon.BaseItem;
using PulseIQ.Local.Common.Model.InteractiveItem;
using PulseIQ.Local.Common.Model.Item;

namespace PulseIQ.Local.ClientCommon.InteractiveItem
{
    public class ViewItemInteractive : ViewItemBase
    {
        protected internal enum InteractiveItemEvents
        {
            TRY_OBTAIN,
            TRY_ABANDON,
            TRY_PLACE_TO_SLOT,

            ABANDONED,
            OBTAINED,
            STOP_UPDATE,
            THROWING,
            RETURN_ITEM,
        }

        protected Transform itemHoldingPositionTransform = null;
        protected Transform slotItemTransform = null;
        protected List<int> InteractiveItemSlotIdsList = new List<int>();
        protected int attachedSlotID = -1;
        protected bool isRepickable = false;
        private bool isPickable = true;

        private List<Collider> objColliders = new List<Collider>();
        private int currentSlotID = -1;
        private Rigidbody rigidBody;
        MessageData msgData;

        private GameObject slotRef;
        protected bool isPlacingToSlot = false;

        private Dictionary<Renderer, Material[]> previousMaterialDic = new Dictionary<Renderer, Material[]>();
        private List<Material> currentMaterials = new List<Material>();

        private Dictionary<Renderer, Material[]> originalMaterialDic = new Dictionary<Renderer, Material[]>();
        private List<Material> currentHighlightMaterials = new List<Material>();

        private bool isSemiTransparent = false;


        protected bool actualOfflineMoving;

        protected float offlineMovingStartTime = 0f;
        protected const float offlineMovingLockInTime = 0.2f;

        protected Outline outline;

        protected override bool offlineMoving
        {
            get { return actualOfflineMoving; }
            set
            {
                if (value)
                {
                    offlineMovingStartTime = Time.time;
                    actualOfflineMoving = true;
                }
                else
                {
                    if (!actualOfflineMoving || Time.time - offlineMovingStartTime > offlineMovingLockInTime)
                    {
                        actualOfflineMoving = false;
                    }
                }

                Debug.Log("OfflineMovingSet to " + value);
            }
        }


        protected internal override void init()
        {
            itemType = Local.Common.ItemTypeCode.INTERACTIVE_ITEM;

            objColliders = new List<Collider>(GetComponentsInChildren<Collider>());

            if (GetComponent<Rigidbody>() != null)
            {
                rigidBody = GetComponent<Rigidbody>();
            }
            else
            {
                rigidBody = gameObject.AddComponent<Rigidbody>();
            }

            outline = gameObject.AddComponent<Outline>();
            outline.OutlineColor = new Color(0.616f, 0.8772459f, 1f, 1f);
            outline.OutlineWidth = 6;
            outline.OutlineMode = Outline.Mode.OutlineVisible;
            outline.enabled = false;
            rigidBody.isKinematic = true;
            rigidBody.drag = 0.25f;
            msgData = new MessageData();

        }

        public void SetInteractiveItemSlotList(int[] itemSlotArray)
        {
            InteractiveItemSlotIdsList.Clear();
            InteractiveItemSlotIdsList.AddRange(itemSlotArray);
        }

        public void SetIsRepickable(bool value)
        {
            isRepickable = value;
        }


        public void PlaceToSlot(CTVector3 localPosition, CTQuaternion localRotation, CTVector3 localScale)
        {
            //CancelInvoke("UpdateMoveToPosition"); ForOnline
            isPlacingToSlot = true;
            interactiveLocalMoveTo = false;
            CancelInvoke("UpdateMoveToHolding");
            CancelInvoke("UpdateRotateToHolding");
            CancelInvoke("UpdateScaleToHolding");
            dispatcher.Dispatch(InteractiveItemEvents.STOP_UPDATE);


            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);
            dispatcher.Dispatch(InteractiveItemEvents.ABANDONED, data);

            ApplyOriginalMaterials();

            if (!isRepickable)
            {
                isPickable = false;
            }
        }

        public void MimicSlotTransform(GameObject slotRef)
        {
            this.slotRef = slotRef;
        }

        void Update()
        {
            base.Update();
        }

        override protected void offlineMovement()
        {
            itemLerpMoveTimer += Time.deltaTime;

            transform.position = Vector3.Lerp(itemMoveToStartPostion, itemMoveToLocation, itemLerpMoveTimer / localMoveToTime);

            if (Vector3.Distance(transform.position, itemMoveToLocation) <= 0.02f)
            {
                offlineMoving = false;

                transform.position = itemMoveToLocation;

                if (interactiveLocalMoveTo && isObtained && !isThrowing)
                {
                    switch (obtainAction)
                    {
                        case EObtainAction.HOLD:
                            {
                                InvokeRepeating("UpdateMoveToHolding", 0.2f, 0.2f);
                                break;
                            }

                        case EObtainAction.LIFT:
                            {
                                interactiveLocalMoveTo = false;
                                break;
                            }
                    }
                }

                if (isPlacingToSlot)
                {
                    MessageData data = new MessageData();
                    data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);
                    dispatcher.Dispatch(InteractiveItemEvents.TRY_ABANDON, data);

                    isPlacingToSlot = false;
                }
            }
        }

        override protected void offlineRotation()
        {
            itemLerpRotateTimer += Time.deltaTime;

            transform.rotation = Quaternion.Lerp(itemMoveToStartRotation, itemMoveToRotation, itemLerpRotateTimer / localRotateToTime);

            if (itemLerpRotateTimer >= localRotateToTime)
            {
                offlineRotating = false;

                transform.rotation = itemMoveToRotation;

                if (interactiveLocalMoveTo && isObtained && !isThrowing)
                {
                    switch (obtainAction)
                    {
                        case EObtainAction.HOLD:
                            {
                                InvokeRepeating("UpdateRotateToHolding", 0.2f, 0.2f);
                                break;
                            }

                        case EObtainAction.LIFT:
                            {
                                interactiveLocalMoveTo = false;
                                break;
                            }
                    }
                }
            }
        }

        override protected void offlineScale()
        {
            itemLerpScaleTimer += Time.deltaTime;

            transform.localScale = Vector3.Lerp(itemMoveToStartScale, itemMoveToScale, itemLerpScaleTimer / localScaleToTime);

            if (itemLerpScaleTimer >= localScaleToTime)
            {
                offlineScaling = false;

                transform.localScale = itemMoveToScale;

                if (interactiveLocalMoveTo && isObtained && !isThrowing)
                {
                    switch (obtainAction)
                    {
                        case EObtainAction.HOLD:
                            {
                                InvokeRepeating("UpdateScaleToHolding", 0.2f, 0.2f);
                                break;
                            }

                        case EObtainAction.LIFT:
                            {
                                interactiveLocalMoveTo = false;
                                break;
                            }
                    }
                }
            }
        }

        public void SaveCurrentMaterials()
        {
            foreach (Renderer renderer in gameObject.GetComponentsInChildren<Renderer>(true))
            {
                previousMaterialDic.Add(renderer, renderer.gameObject.GetComponent<Renderer>().materials);
            }
        }

        public void SaveCurrentOriginalMaterials()
        {
            foreach (Renderer renderer in gameObject.GetComponentsInChildren<Renderer>(true))
            {
                originalMaterialDic.Add(renderer, renderer.gameObject.GetComponent<Renderer>().materials);
            }
        }

        private void ApplySemiTransparentMaterials()
        {
            Material semiTransparentMaterial = Resources.Load("Materials/selected") as Material;

            foreach (Renderer renderer in gameObject.GetComponentsInChildren<Renderer>(true))
            {
                currentMaterials.Clear();

                for (int i = 0; i < renderer.materials.Length; i++)
                {
                    currentMaterials.Add(semiTransparentMaterial);
                }

                renderer.materials = currentMaterials.ToArray();
            }

            isSemiTransparent = true;
        }

        public void ApplyHighlightMaterials()
        {
            outline.enabled = true;
        }

        private void ApplyOriginalMaterials()
        {

            if (isSemiTransparent)
            {
                foreach (Renderer renderer in GetComponentsInChildren<Renderer>(true))
                {
                    renderer.materials = previousMaterialDic[renderer];
                }

                isSemiTransparent = false;
            }
        }

        public void ApplyOriginalHighlightMaterials()
        {
            outline.enabled = false;
        }

        public void FailedToPlaceToSlot()
        {
            ReturnFunction();
            ApplyOriginalMaterials();
        }

        public override void ObtainedItem()
        {
            if (!isThrowing)
            {
                if (isUsingPhysics)
                {
                    CancelUpdating();
                }

                if (objColliders.Count < 1)
                {
                    objColliders = new List<Collider>(GetComponentsInChildren<Collider>());

                    foreach (Collider col in objColliders)
                    {
                        col.enabled = false;
                    }
                }
                else
                {
                    foreach (Collider col in objColliders)
                    {
                        col.enabled = false;
                    }
                }

                isObtained = true;

                interactiveLocalMoveTo = true;

                MessageData data = new MessageData();
                data.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);
                dispatcher.Dispatch(InteractiveItemEvents.OBTAINED, data);

                switch (ReturnObtainAction())
                {
                    case EObtainAction.LIFT:
                        {
                            ApplySemiTransparentMaterials();
                            break;
                        }
                }
            }
        }

        public override void ItemAbandoned()
        {
            if (objColliders.Count < 1)
            {
                objColliders = new List<Collider>(GetComponentsInChildren<Collider>());

                foreach (Collider col in objColliders)
                {
                    col.enabled = true;
                }
            }
            else
            {
                foreach (Collider col in objColliders)
                {
                    col.enabled = true;
                }
            }

            isObtained = false;
            slotItemTransform = null;
            isThrowing = false;
            isReturning = false;

            ApplyOriginalMaterials();

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);

            dispatcher.Dispatch(InteractiveItemEvents.ABANDONED, data);

            dispatcher.Dispatch(InteractiveItemEvents.STOP_UPDATE);
        }

        public override void ObtainedItemFailed()
        {
        }

        public void OnThrowItem(Vector3 forwardDir)
        {
            if (!isThrowing && !stopThrowing && !isReturning)
            {
                isThrowing = true;

                ApplyOriginalMaterials();
                CancelInvoke("UpdateMoveToHolding");
                CancelInvoke("UpdateRotateToHolding");
                CancelInvoke("UpdateScaleToHolding");

                if (objColliders.Count < 1)
                {
                    objColliders = new List<Collider>(GetComponentsInChildren<Collider>());

                    foreach (Collider col in objColliders)
                    {
                        col.enabled = true;
                    }
                }
                else
                {
                    foreach (Collider col in objColliders)
                    {
                        col.enabled = true;
                    }
                }

                switch (abandonAction)
                {
                    case EAbandonAction.THROW:
                        {
                            isUsingPhysics = true;
                            break;
                        }

                    case EAbandonAction.THROW_RETURN:
                        {
                            Invoke("ReturnFunction", 2.5f);
                            break;
                        }

                    case EAbandonAction.TELEPORT:
                        {
                            dispatcher.Dispatch(InteractiveItemEvents.STOP_UPDATE);

                            CTVector3 pos = new CTVector3(itemReturnToLocation.x, itemReturnToLocation.y, itemReturnToLocation.z);
                            Quaternion rot = new Quaternion(itemReturnToRotation.x, itemReturnToRotation.y, itemReturnToRotation.z, itemReturnToRotation.w);
                            CTVector3 scale = new CTVector3(itemReturnToScale.x, itemReturnToScale.y, itemReturnToScale.z);

                            MessageData data = new MessageData();
                            data.Parameters.Add((byte)CommonParameterCode.ITEM_POSITION, pos);
                            data.Parameters.Add((byte)CommonParameterCode.ITEM_ROTATION, rot);
                            data.Parameters.Add((byte)CommonParameterCode.ITEM_SCALE, scale);
                            data.Parameters.Add((byte)CommonParameterCode.DATA_ITEM_POSE, itemID);

                            dispatcher.Dispatch(InteractiveItemEvents.RETURN_ITEM, data);

                            offlineMoving = false;
                            isTeleporting = true;

                            return;
                        }

                    case EAbandonAction.RETURN:
                        {
                            offlineMoving = false;

                            dispatcher.Dispatch(InteractiveItemEvents.STOP_UPDATE);
                            ReturnFunction();

                            return;
                        }
                }

                rigidBody.isKinematic = false;
                rigidBody.velocity = forwardDir * 5f;
                rigidBody.AddForce(forwardDir * 400);

                dispatcher.Dispatch(InteractiveItemEvents.STOP_UPDATE);

                MessageData tempData = new MessageData();

                dispatcher.Dispatch(InteractiveItemEvents.THROWING, tempData);

                InvokeUpdatePosition();
                InvokeUpdateRotation();
                InvokeUpdateScale();
            }
        }

        public void SetSlotItemTransform(Transform slotTransform)
        {
            slotItemTransform = slotTransform;
        }

        public void OnOfflineMovement(float speed, CTVector3 position)
        {
            if (!isThrowing)
            {
                Vector3 pos = new Vector3(position.X, position.Y, position.Z);

                LocalMoveTo(pos, speed);
            }
        }

        public void OnOfflineRotation(float speed, CTQuaternion rotation)
        {
            if (!isThrowing)
            {
                Quaternion rot = new Quaternion(rotation.X, rotation.Y, rotation.Z, rotation.W);

                LocalRotateTo(rot, speed);
            }
        }

        public void OnOfflineScaling(float speed, CTVector3 scale)
        {
            if (!isThrowing)
            {
                Vector3 scal = new Vector3(scale.X, scale.Y, scale.Z);

                LocalScaleTo(scal, speed);
            }
        }

        public void SetAttachedItemIndex(int index)
        {
            attachedSlotID = index;
        }

        public MessageData GetObtainHoverData()
        {
            MessageData data = new MessageData();

            Vector3 movePosition = transform.position + (Vector3.up * 0.5f);
            CTVector3 position = new CTVector3(movePosition.x, movePosition.y, movePosition.z);
            CTQuaternion rotation = new CTQuaternion(transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w);
            CTVector3 scale = new CTVector3(transform.localScale.x, transform.localScale.y, transform.localScale.z);

            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);
            data.Parameters.Add((byte)CommonParameterCode.MOVE_TO_POSITION, position);
            data.Parameters.Add((byte)CommonParameterCode.MOVE_TO_ROTATION, rotation);
            data.Parameters.Add((byte)CommonParameterCode.MOVE_TO_SCALE, scale);

            return data;
        }

        public bool GetIsRepickable()
        {
            return isRepickable;
        }

        public bool GetIsPickable()
        {
            return isPickable;
        }

        public override void SyncItemData(CTItemData data)
        {
            base.SyncItemData(data);

            //CTInteractiveItemData interactiveData = (CTInteractiveItemData)data;
            //
            //abandonAction = interactiveData.AbandonAction;
            //obtainAction = interactiveData.ObtainAction;
            //isRepickable = interactiveData.IsRepickable;
            //InteractiveItemSlotIdsList = interactiveData.ListSlotId;
        }

        public void ItemObtainedExternally()
        {
            isObtained = true;
        }

        public void ItemAbandonedExternally()
        {
            isObtained = false;
        }

        public override void SlotGroupSynced(float elapsedTime)
        {
            Debug.Log("Interactive Item!!!!!!!!!!!  " + elapsedTime);
            if (elapsedTime == -1)
            {
                return;
            }
            else
            {
                foreach (Animator anim in GetComponentsInChildren<Animator>())
                {
                    anim.enabled = true;

                    if (elapsedTime < anim.GetCurrentAnimatorStateInfo(0).length)
                    {
                        float time = elapsedTime / anim.GetCurrentAnimatorStateInfo(0).length;
                        anim.Play(0, 0, time);
                    }
                    else
                    {
                        if (anim.GetCurrentAnimatorStateInfo(0).loop)
                        {
                            float time = elapsedTime % anim.GetCurrentAnimatorStateInfo(0).length;
                            time = time / anim.GetCurrentAnimatorStateInfo(0).length;
                            anim.Play(0, 0, time);
                        }
                        else
                        {
                            anim.Play(0, 0, 1f);
                        }
                    }
                }

                foreach (Renderer renderer in GetComponentsInChildren<Renderer>())
                {
                    renderer.enabled = true;
                }

                if (null != gameObject.GetComponent<Collider>())
                {
                    gameObject.GetComponent<Collider>().enabled = true;
                }

                for (int i = 0; i < transform.childCount; i++)
                {
                    if (null != transform.GetChild(i).gameObject.GetComponent<Collider>())
                    {
                        transform.GetChild(i).gameObject.GetComponent<Collider>().enabled = true;
                    }
                }
            }
        }
    }
}