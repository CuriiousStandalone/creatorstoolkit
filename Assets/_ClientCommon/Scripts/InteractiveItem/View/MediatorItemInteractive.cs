﻿using strange.extensions.mediation.impl;
using strange.extensions.dispatcher.eventdispatcher.api;

using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.CustomType;
using PulseIQ.Local.ClientCommon.BaseItem;

using UnityEngine;
using PulseIQ.Local.Common.Model.InteractiveItem;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Client.Unity3DLib;
using PulseIQ.Local.Common.Model.SlotGroup;

namespace PulseIQ.Local.ClientCommon.InteractiveItem
{
    public class MediatorItemInteractive : EventMediator
    {
        [Inject]
        public ViewItemInteractive view { get; set; }

        [Inject]
        public IItemDataset model { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        protected void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewItemInteractive.InteractiveItemEvents.TRY_ABANDON, OnAbandonItem);
            view.dispatcher.UpdateListener(value, ViewItemInteractive.InteractiveItemEvents.ABANDONED, OnAbandoned);
            view.dispatcher.UpdateListener(value, ViewItemBase.ABANDON_ITEM, OnAbandonItem);
            view.dispatcher.UpdateListener(value, ViewItemBase.EVENT_MOVE, OnMoveItem);
            view.dispatcher.UpdateListener(value, ViewItemBase.EVENT_ROTATE, OnRotateItem);
            view.dispatcher.UpdateListener(value, ViewItemBase.EVENT_SCALE, OnScaleItem);
            view.dispatcher.UpdateListener(value, ViewItemBase.ATTACH_TO_HOLDING_POS, OnAttachToHoldingPos);
            view.dispatcher.UpdateListener(value, ViewItemBase.ATTACH_TO_HOLDING_ROT, OnAttachToHoldingRot);
            view.dispatcher.UpdateListener(value, ViewItemBase.ATTACH_TO_HOLDING_SCALE, OnAttachToHoldingScale);
            view.dispatcher.UpdateListener(value, ViewItemBase.OFFLINE_MOVE, OnOfflineMove);
            view.dispatcher.UpdateListener(value, ViewItemBase.OFFLINE_ROTATE, OnOfflineRotate);
            view.dispatcher.UpdateListener(value, ViewItemBase.OFFLINE_SCALE, OnOfflineScale);
            view.dispatcher.UpdateListener(value, ViewItemBase.LERP_MOVE, OnLerpMoveItem);
            view.dispatcher.UpdateListener(value, ViewItemBase.LERP_ROTATE, OnLerpRotateItem);
            view.dispatcher.UpdateListener(value, ViewItemBase.LERP_SCALE, OnLerpScaleItem);
            view.dispatcher.UpdateListener(value, ViewItemInteractive.InteractiveItemEvents.OBTAINED, OnObtained);
            //view.dispatcher.UpdateListener(value, ViewItemInteractive.InteractiveItemEvents.MOVE_TO, OnMoveTo);
           // view.dispatcher.UpdateListener(value, ViewItemInteractive.InteractiveItemEvents.UPDATE_MOVE_TO, OnUpdateMoveTo);
            view.dispatcher.UpdateListener(value, ViewItemInteractive.InteractiveItemEvents.STOP_UPDATE, OnStopUpdate);
            view.dispatcher.UpdateListener(value, ViewItemInteractive.InteractiveItemEvents.THROWING, OnThrowingItem);
            view.dispatcher.UpdateListener(value, ViewItemInteractive.InteractiveItemEvents.RETURN_ITEM, OnReturnItem);
        }

        private void OnMessageReceived(System.Object data)
        {
            MessageData itemData = (MessageData)data;

            ServerCmdCode Code = (ServerCmdCode)itemData[(byte)CommonParameterCode.OPERATION_CODE];

            switch (Code)
            {
                case ServerCmdCode.ITEM_MOVED:
                    {
                        CTVector3 pos = (CTVector3)itemData[(byte)CommonParameterCode.ITEM_POSITION];
                        view.UpdatePosition(pos);
                        break;
                    }

                case ServerCmdCode.ITEM_ROTATED:
                    {
                        CTQuaternion rot = (CTQuaternion)itemData[(byte)CommonParameterCode.ITEM_ROTATION];
                        view.UpdateRotation(rot);
                        break;
                    }

                case ServerCmdCode.ITEM_SCALED:
                    {
                        CTVector3 scale = (CTVector3)itemData[(byte)CommonParameterCode.ITEM_SCALE];
                        view.UpdateScale(scale);
                        break;
                    }


                case ServerCmdCode.ITEM_OBTAIN_FAILED:
                    {
                        view.ObtainedItemFailed();
                        break;
                    }

                case ServerCmdCode.ITEM_OBTAIN_SUCCESS:
                    {
                        string userID = (string)itemData[(byte)CommonParameterCode.USERID];

                        if (userID == ClientInstance.Instance.CurUserID)
                        {
                            view.ObtainedItem();

                            float speed = 10f;

                            switch (view.ReturnObtainAction())
                            {
                                case EObtainAction.HOLD:
                                    {
                                        MessageData localMoveData = new MessageData();
                                        localMoveData.Parameters.Add((byte)CommonParameterCode.MOVE_TO_SPEED, speed);
                                        localMoveData.Parameters.Add((byte)CommonParameterCode.ITEMID, view.itemID);

                                        MessageData localRotateData = new MessageData();
                                        localRotateData.Parameters.Add((byte)CommonParameterCode.MOVE_TO_SPEED, speed);
                                        localRotateData.Parameters.Add((byte)CommonParameterCode.ITEMID, view.itemID);

                                        MessageData localScaleData = new MessageData();
                                        localScaleData.Parameters.Add((byte)CommonParameterCode.MOVE_TO_SPEED, speed);
                                        localScaleData.Parameters.Add((byte)CommonParameterCode.ITEMID, view.itemID);

                                        dispatcher.Dispatch(IntCmdCodeInteractiveItem.LOCAL_MOVE_TO, localMoveData);
                                        dispatcher.Dispatch(IntCmdCodeInteractiveItem.LOCAL_ROTATE_TO, localRotateData);
                                        dispatcher.Dispatch(IntCmdCodeInteractiveItem.LOCAL_SCALE_TO, localScaleData);
                                        break;
                                    }

                                case EObtainAction.LIFT:
                                    {
                                        MessageData localMoveData = view.GetObtainHoverData();
                                        MessageData localRotateData = view.GetObtainHoverData();
                                        MessageData localScaleData = view.GetObtainHoverData();

                                        localMoveData.Parameters.Add((byte)CommonParameterCode.MOVE_TO_SPEED, speed);
                                        localRotateData.Parameters.Add((byte)CommonParameterCode.MOVE_TO_SPEED, speed);
                                        localScaleData.Parameters.Add((byte)CommonParameterCode.MOVE_TO_SPEED, speed);

                                        dispatcher.Dispatch(OpCmdCodeBaseItem.LOCAL_MOVE_TO, localMoveData);
                                        dispatcher.Dispatch(OpCmdCodeBaseItem.LOCAL_ROTATE_TO, localRotateData);
                                        dispatcher.Dispatch(OpCmdCodeBaseItem.LOCAL_SCALE_TO, localScaleData);

                                        break;
                                    }
                            }

                            break;
                        }
                        else
                        {
                            view.ItemObtainedExternally();
                            break;
                        }
                    }

                case ServerCmdCode.ITEM_ABANDONED:
                    {
                        string userID = (string)itemData[(byte)CommonParameterCode.USERID];

                        if (userID == ClientInstance.Instance.CurUserID)
                        {
                            view.ItemAbandoned();
                            break;
                        }
                        else
                        {
                            view.ItemAbandonedExternally();
                            break;
                        }
                    }

                case ServerCmdCode.INTERACTIVE_SLOT_DENIED:
                    {
                        if (!view.GetOfflineReturn())
                        {
                            view.SetOfflineReturn(true);
                            view.FailedToPlaceToSlot();
                        }
                        break;
                    }

                case ServerCmdCode.INTERACTIVE_SLOT_PLACED:
                    {
                        CTVector3 pos = (CTVector3)itemData[(byte)CommonParameterCode.INTERACTIVE_MOVE_POSITION];
                        CTQuaternion rot = (CTQuaternion)itemData[(byte)CommonParameterCode.INTERACTIVE_MOVE_ROTATION];
                        CTVector3 scale = (CTVector3)itemData[(byte)CommonParameterCode.INTERACTIVE_MOVE_SCALE];

                        float speed = 15f;

                        MessageData localMoveData = new MessageData();
                        MessageData localRotateData = new MessageData();
                        MessageData localScaleData = new MessageData();

                        localMoveData.Parameters.Add((byte)CommonParameterCode.MOVE_TO_POSITION, pos);
                        localMoveData.Parameters.Add((byte)CommonParameterCode.MOVE_TO_SPEED, speed);
                        localMoveData.Parameters.Add((byte)CommonParameterCode.ITEMID, view.itemID);

                        localRotateData.Parameters.Add((byte)CommonParameterCode.MOVE_TO_ROTATION, rot);
                        localRotateData.Parameters.Add((byte)CommonParameterCode.MOVE_TO_SPEED, speed);
                        localRotateData.Parameters.Add((byte)CommonParameterCode.ITEMID, view.itemID);

                        localScaleData.Parameters.Add((byte)CommonParameterCode.MOVE_TO_SCALE, scale);
                        localScaleData.Parameters.Add((byte)CommonParameterCode.MOVE_TO_SPEED, speed);
                        localScaleData.Parameters.Add((byte)CommonParameterCode.ITEMID, view.itemID);

                        dispatcher.Dispatch(OpCmdCodeBaseItem.LOCAL_MOVE_TO, localMoveData);
                        dispatcher.Dispatch(OpCmdCodeBaseItem.LOCAL_ROTATE_TO, localRotateData);
                        dispatcher.Dispatch(OpCmdCodeBaseItem.LOCAL_SCALE_TO, localScaleData);

                        view.PlaceToSlot(pos, rot, scale);
                        break;
                    }

                case ServerCmdCode.ITEM_THROWN:
                    {
                        Vector3 forwardPost = (Vector3)itemData[(byte)CommonParameterCode.MAIN_CAMERA_FORWARD_DIR];
                        view.OnThrowItem(forwardPost);
                        break;
                    }

                case ServerCmdCode.ITEM_OFFLINE_MOVED:
                    {
                        float speed = (float)itemData[(byte)CommonParameterCode.MOVE_TO_SPEED];
                        CTVector3 position = (CTVector3)itemData[(byte)CommonParameterCode.MOVE_TO_POSITION];

                        view.OnOfflineMovement(speed, position);

                        if(view.GetOfflineReturn())
                        {
                            MessageData abandonData = new MessageData();
                            abandonData.Parameters.Add((byte)CommonParameterCode.ITEMID, view.itemID);

                            dispatcher.Dispatch(SubCode.TryAbandonItem, abandonData);
                            view.SetOfflineReturn(false);
                        }

                        break;
                    }

                case ServerCmdCode.ITEM_OFFLINE_ROTATED:
                    {
                        float speed = (float)itemData[(byte)CommonParameterCode.MOVE_TO_SPEED];
                        CTQuaternion rotation = (CTQuaternion)itemData[(byte)CommonParameterCode.MOVE_TO_ROTATION];

                        view.OnOfflineRotation(speed, rotation);

                        break;
                    }

                case ServerCmdCode.ITEM_OFFLINE_SCALED:
                    {
                        float speed = (float)itemData[(byte)CommonParameterCode.MOVE_TO_SPEED];
                        CTVector3 scale = (CTVector3)itemData[(byte)CommonParameterCode.MOVE_TO_SCALE];

                        view.OnOfflineScaling(speed, scale);

                        break;
                    }

                case ServerCmdCode.ITEM_SHOWN:
                    {
                        view.ShowView();
                        break;
                    }

                case ServerCmdCode.ITEM_HIDDEN:
                    {
                        view.HideView();
                        break;
                    }

                case ServerCmdCode.SLOT_GROUP_FINISH_ANI_PLAYED:
                    {
                        break;
                    }

                case ServerCmdCode.ITEM_STATUS_SYNCED:
                    {
                        CTItemData syncData = (CTItemData)(itemData[(byte)CommonParameterCode.DATA_CT_ITEM_DATA]);
                        view.SyncItemData(syncData);
                        break;
                    }

                case ServerCmdCode.SLOT_GROUP_STATUS_SYNC:
                    {
                        CTSlotGroupData slotData = (CTSlotGroupData)(itemData[(byte)CommonParameterCode.DATA_CT_SLOT_GROUP_DATA]);
                        view.SlotGroupSynced(slotData.SlotGroupFinishAniTimeElapsed);
                        break;
                    }

                case ServerCmdCode.ITEM_LERP_MOVED:
                    {
                        CTVector3 pos = (CTVector3)itemData[(byte)CommonParameterCode.MOVE_TO_POSITION];
                        view.LerpMovement(pos);
                        break;
                    }

                case ServerCmdCode.ITEM_LERP_ROTATED:
                    {
                        CTQuaternion rot = (CTQuaternion)itemData[(byte)CommonParameterCode.MOVE_TO_ROTATION];
                        view.LerpRotation(rot);
                        break;
                    }

                case ServerCmdCode.ITEM_LERP_SCALED:
                    {
                        CTVector3 scale = (CTVector3)itemData[(byte)CommonParameterCode.MOVE_TO_SCALE];
                        view.LerpScale(scale);
                        break;
                    }
            }
        }

        public void OnAttachToHoldingPos(IEvent evt)
        {
            dispatcher.Dispatch(IntCmdCodeBaseItem.ATTACH_TO_HOLDING_POS, evt.data);
        }

        public void OnAttachToHoldingRot(IEvent evt)
        {
            dispatcher.Dispatch(IntCmdCodeBaseItem.ATTACH_TO_HOLDING_ROT, evt.data);
        }

        public void OnAttachToHoldingScale(IEvent evt)
        {
            dispatcher.Dispatch(IntCmdCodeBaseItem.ATTACH_TO_HOLDING_SCALE, evt.data);
        }

        private void OnThrowingItem(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.THROWING_INTERACTIVE_ITEM, evt.data);
        }

        private void OnAbandonItem(IEvent evt)
        {
			dispatcher.Dispatch(SubCode.TryAbandonItem, evt.data);
        }

        private void OnMoveItem(IEvent evt)
        {
			dispatcher.Dispatch(SubCode.MoveItem, evt.data);
        }

        private void OnRotateItem(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.RotateItem, evt.data);
        }

        private void OnScaleItem(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.ScaleItem, evt.data);
        }

        private void OnLerpMoveItem(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.LerpMoveItem, evt.data);
        }

        private void OnLerpRotateItem(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.LerpRotateItem, evt.data);
        }

        private void OnLerpScaleItem(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.LerpScaleItem, evt.data);
        }

        private void OnAbandoned(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.ITEM_ABANDONED, evt.data);
        }

        private void OnObtained(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.ITEM_OBTAINED, evt.data);
        }

        //private void OnMoveTo(IEvent evt)
        //{
        //    dispatcher.Dispatch(IntCmdCodeInteractiveItem.MOVE_TO_POSITION, evt.data);
        //}

        //private void OnUpdateMoveTo(IEvent evt)
        //{
        //    dispatcher.Dispatch(IntCmdCodeInteractiveItem.UPDATE_MOVE_TO, evt.data);
        //}

        private void OnStopUpdate(IEvent evt)
        {
			dispatcher.Dispatch(SubCode.StopUpdate, evt.data);
        }

        private void OnThrowItem(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;
            Vector3 forwardDir = (Vector3)data[(byte)CommonParameterCode.MAIN_CAMERA_FORWARD_DIR];
            view.OnThrowItem(forwardDir); 
        }

        private void OnReturnItem(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.MoveItem, evt.data);
        }

        private void OnOfflineMove(IEvent evt)
        {
            dispatcher.Dispatch(OpCmdCodeBaseItem.LOCAL_MOVE_TO, evt.data);
        }

        private void OnOfflineRotate(IEvent evt)
        {
            dispatcher.Dispatch(OpCmdCodeBaseItem.LOCAL_ROTATE_TO, evt.data);
        }

        private void OnOfflineScale(IEvent evt)
        {
            dispatcher.Dispatch(OpCmdCodeBaseItem.LOCAL_SCALE_TO, evt.data);
        }
    }
}