﻿using strange.extensions.command.impl;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.BaseItem;
using UnityEngine;
using PulseIQ.Local.Common.CustomType;

namespace PulseIQ.Local.ClientCommon.InteractiveItem
{
    public class IntCmdLocalScaleToInteractiveItem : EventCommand
    {
        [Inject]
        public IItemDataset model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            Vector3 camScale = model.mainCamera.GetComponent<IMainCamera>().ReturnCameraHoldingScale();
            CTVector3 scale = new CTVector3(camScale.x, camScale.y, camScale.z);

            data.Parameters.Add((byte)CommonParameterCode.MOVE_TO_SCALE, scale);

            dispatcher.Dispatch(OpCmdCodeBaseItem.LOCAL_SCALE_TO, data);
        }
    }
}