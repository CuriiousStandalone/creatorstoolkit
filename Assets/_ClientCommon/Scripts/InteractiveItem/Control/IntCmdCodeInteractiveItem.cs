﻿namespace PulseIQ.Local.ClientCommon.InteractiveItem
{
    public enum IntCmdCodeInteractiveItem : byte
    {
        CREATED,
        PLACED_TO_SLOT,
        SLOT_DENIED,
        LOCAL_MOVE_TO,
        LOCAL_ROTATE_TO,
        LOCAL_SCALE_TO,
        MOVE_TO_POSITION,
        UPDATE_MOVE_TO,
        STOP_UPDATE,
    }
}