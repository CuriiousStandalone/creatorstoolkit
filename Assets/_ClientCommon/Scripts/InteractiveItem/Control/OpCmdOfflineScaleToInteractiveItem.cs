﻿using strange.extensions.command.impl;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.CustomType;
using UnityEngine;
using PulseIQ.Local.Common;

namespace PulseIQ.Local.ClientCommon.InteractiveItem
{
    public class OpCmdOfflineScaleToInteractiveItem : EventCommand
    {
        [Inject]
        public IItemDataset model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            string itemID = (string)data[(byte)CommonParameterCode.ITEMID];

            Vector3 camScale = model.mainCamera.GetComponent<IMainCamera>().ReturnCameraHoldingScale();
            CTVector3 scale = new CTVector3(camScale.x, camScale.y, camScale.z);

            data.Parameters.Add((byte)CommonParameterCode.ITEM_SCALE, scale);

            dispatcher.Dispatch(SubCode.ScaleItem, data); 
        }
    }
}