﻿using strange.extensions.command.impl;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.CustomType;
using UnityEngine;
using PulseIQ.Local.Common;

namespace PulseIQ.Local.ClientCommon.InteractiveItem
{
    public class OpCmdOfflineRotateToInteractiveItem : EventCommand
    {
        [Inject]
        public IItemDataset model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            string itemID = (string)data[(byte)CommonParameterCode.ITEMID];

            Quaternion rotation = model.mainCamera.GetComponent<IMainCamera>().ReturnCameraHoldingRotation();
            CTQuaternion rot = new CTQuaternion(rotation.x, rotation.y, rotation.z, rotation.w);

            data.Parameters.Add((byte)CommonParameterCode.ITEM_ROTATION, rot);

            dispatcher.Dispatch(SubCode.RotateItem, data); 
        }
    }
}