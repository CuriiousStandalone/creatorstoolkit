﻿using strange.extensions.command.impl;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.CustomType;
using UnityEngine;
using PulseIQ.Local.Common;

namespace PulseIQ.Local.ClientCommon.InteractiveItem
{
    public class OpCmdOfflineMoveToInteractiveItem : EventCommand
    {
        [Inject]
        public IItemDataset model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            string itemID = (string)data[(byte)CommonParameterCode.ITEMID];

            Vector3 position = model.mainCamera.GetComponent<IMainCamera>().ReturnCameraHoldingPosition();
            CTVector3 pos = new CTVector3(position.x, position.y, position.z);

            data.Parameters.Add((byte)CommonParameterCode.ITEM_POSITION, pos);

            dispatcher.Dispatch(SubCode.MoveItem, data); 
        }
    }
}