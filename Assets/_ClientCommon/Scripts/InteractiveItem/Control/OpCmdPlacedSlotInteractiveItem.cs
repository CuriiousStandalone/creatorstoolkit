﻿using strange.extensions.command.impl;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.CustomType;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.BaseItem;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.InteractiveItem
{
    public class OpCmdPlacedSlotInteractiveItem : EventCommand
    {
        [Inject]
        public IItemDataset model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;
            string slotID = (string)data[(byte)CommonParameterCode.SLOTID];
            CTVector3 localPos = (CTVector3)data[(byte)CommonParameterCode.INTERACTIVE_LOCAL_POSITION];
            CTQuaternion localRot = (CTQuaternion)data[(byte)CommonParameterCode.INTERACTIVE_LOCAL_ROTATION];
            CTVector3 localScale = (CTVector3)data[(byte)CommonParameterCode.INTERACTIVE_LOCAL_SCALE];

            foreach (ViewItemBase item in model.itemList)
            {
                if(slotID == item.itemID)
                {
                    CTVector3 movePos = new CTVector3(item.transform.position.x, item.transform.position.y, item.transform.position.z) + localPos;
                    Quaternion temp = new Quaternion(localRot.X, localRot.Y, localRot.Z, localRot.W) * item.transform.rotation;
                    CTQuaternion moveRot = new CTQuaternion(temp.x, temp.y, temp.z, temp.w);
                    CTVector3 moveScale = localScale;

                    data.Parameters.Add((byte)CommonParameterCode.INTERACTIVE_MOVE_POSITION, movePos);
                    data.Parameters.Add((byte)CommonParameterCode.INTERACTIVE_MOVE_ROTATION, moveRot);
                    data.Parameters.Add((byte)CommonParameterCode.INTERACTIVE_MOVE_SCALE, moveScale);

                    break;
                }
            }


            data.Parameters.Add((byte)CommonParameterCode.OPERATION_CODE, evt.type);

            dispatcher.Dispatch(IntCmdCodeInteractiveItem.PLACED_TO_SLOT, data);
        }
    }
}