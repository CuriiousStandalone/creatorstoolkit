﻿using strange.extensions.command.impl;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.BaseItem;

namespace PulseIQ.Local.ClientCommon.InteractiveItem
{
    public class IntCmdMoveToInteractiveItem : EventCommand
    {
        [Inject]
        public IItemDataset model { get; set; }
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            data.Parameters.Add((byte)CommonParameterCode.MOVE_TO_POSITION, model.mainCamera.GetComponent<IMainCamera>().ReturnCameraHoldingPosition());
            data.Parameters.Add((byte)CommonParameterCode.MOVE_TO_ROTATION, model.mainCamera.GetComponent<IMainCamera>().ReturnCameraHoldingRotation());
            data.Parameters.Add((byte)CommonParameterCode.MOVE_TO_SCALE, model.mainCamera.GetComponent<IMainCamera>().ReturnCameraHoldingScale());

            dispatcher.Dispatch(SubCode.MoveToPostion, data);
        }
    }
}