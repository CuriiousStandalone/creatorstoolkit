﻿using strange.extensions.command.impl;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.BaseItem;
using UnityEngine;
using PulseIQ.Local.Common.CustomType;

namespace PulseIQ.Local.ClientCommon.InteractiveItem
{
    public class IntCmdLocalRotateToInteractiveItem : EventCommand
    {
        [Inject]
        public IItemDataset model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            Quaternion rotation = model.mainCamera.GetComponent<IMainCamera>().ReturnCameraHoldingRotation();
            CTQuaternion rot = new CTQuaternion(rotation.x, rotation.y, rotation.z, rotation.w);

            data.Parameters.Add((byte)CommonParameterCode.MOVE_TO_ROTATION, rot);

            dispatcher.Dispatch(OpCmdCodeBaseItem.LOCAL_ROTATE_TO, data);
        }
    }
}