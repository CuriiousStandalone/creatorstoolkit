﻿namespace PulseIQ.Local.ClientCommon.AvatarItem
{
    public enum IntCmdCodeAvatarItem
    { 
        CREATED,
        HEAD_MOVED,
        THROW_ITEM,
        ANIMATION_PLAYED,
        SLOT_GROUP_EVALUATED,
        MOVED,
    }
}