﻿using PulseIQ.Local.ClientCommon.BaseItem;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;

using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.AvatarItem
{
    public class IntCmdSlotGroupEvaluatedAvatarItem : EventCommand
    {
        [Inject]
        public IItemDataset model { get; set; }

        public override void Execute()
        {
            foreach (ViewItemBase item in model.itemList)
            {
                if (item.itemType == ItemTypeCode.AVATAR)
                {
                    item.GetComponent<ViewAvatar>().HideAvatarNotation(true);
                }
            }

            MessageData data = (MessageData)evt.data;

            dispatcher.Dispatch(IntCmdCodeAvatarItem.SLOT_GROUP_EVALUATED, data);
        }
    }
}