﻿using strange.extensions.command.impl;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace PulseIQ.Local.ClientCommon.AvatarItem
{
    public class IntCmdReturnCameraRotAvatarItem : EventCommand
    {
        [Inject]
        public IItemDataset model { get; set; }

        public override void Execute()
        {
            model.mainCamera.GetComponent<IMainCamera>().RotateCameraWorldZero(false);
        }
    }
}