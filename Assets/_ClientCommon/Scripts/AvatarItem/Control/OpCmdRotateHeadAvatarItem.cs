﻿using strange.extensions.command.impl;

using PulseIQ.Local.Client.Unity3DLib.Model;
using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.CustomType;
using PulseIQ.Local.Common.Model.Item;

namespace PulseIQ.Local.ClientCommon.AvatarItem
{
	public class OpCmdRotateHeadAvatarItem : EventCommand 
	{
		public override void Execute()
		{
            MessageData data = (MessageData)evt.data;
            ItemRotation rot = (ItemRotation)data[(byte)CommonParameterCode.DATA_AVATAR_HEAD_ROTATION];

            OperationsAvatar.RotateHead(ClientBridge.Instance, rot.ItemId, rot.Rotation);
		}
	}
}
