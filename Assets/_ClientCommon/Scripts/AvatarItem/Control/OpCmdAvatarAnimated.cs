﻿using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.SlotItem;
using PulseIQ.Local.Common;
using strange.extensions.command.impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PulseIQ.Local.ClientCommon.AvatarItem
{
    public class OpCmdAvatarAnimated : EventCommand
    {
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;
            data.Parameters.Add((byte)CommonParameterCode.OPERATION_CODE, evt.type);

            dispatcher.Dispatch(IntCmdCodeAvatarItem.ANIMATION_PLAYED, data);
        }
    }
}
