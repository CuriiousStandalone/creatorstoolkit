﻿namespace PulseIQ.Local.ClientCommon.AvatarItem
{
    public enum OpCmdCodeAvatarItem
    {
        CREATE,
        MOVE_HEAD,
        AVATAR_ANIM_PLAYED,
    }
}