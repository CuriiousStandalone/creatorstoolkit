﻿using strange.extensions.command.impl;

using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.DllBridge;

using UnityEngine;

namespace PulseIQ.Local.ClientCommon.AvatarItem
{
    public class OpCmdAvatarCreated : EventCommand
    {
        public override void Execute()
        {
            Debug.Log("OpCmdAvatarCreated");

            MessageData msgData = (MessageData)evt.data;

            string itemId = (string)msgData[(byte)CommonParameterCode.AVATARID];

            OperationsAvatar.AvatarCreated(ClientBridge.Instance, itemId);
        }
    }
}
