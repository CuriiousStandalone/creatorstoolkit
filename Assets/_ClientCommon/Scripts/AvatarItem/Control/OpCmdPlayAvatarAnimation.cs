﻿using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;

using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.AvatarItem
{
    public class OpCmdPlayAvatarAnimation : EventCommand
    {
        [Inject]
        public IItemDataset itemDataset { get; set; }

        public override void Execute()
        {
            MessageData messageData = (MessageData)evt.data;

            int avatarCount = 0;
            ViewAvatar vAvatar = null;

            foreach (var item in itemDataset.itemList)
            {
                if(null != item.gameObject.GetComponent<ViewAvatar>())
                {
                    avatarCount++;
                    if (item.gameObject.GetComponent<ViewAvatar>().isLocal)
                    {
                        vAvatar = item.gameObject.GetComponent<ViewAvatar>();
                    }
                }
            }

            if(avatarCount <= 1 || null == vAvatar)
            {
                return;
            }

            string avatarItemId = vAvatar.itemID;
            string animName = (string)messageData[(byte)CommonParameterCode.ANIMATION_NAME];

            OperationsAvatar.PlayAvatarAnimation(ClientBridge.Instance, avatarItemId, animName);
        }
    }
}
