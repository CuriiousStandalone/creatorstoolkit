﻿using UnityEngine;
using PulseIQ.Local.ClientCommon.BaseItem;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using CTItemData = PulseIQ.Local.Common.Model.Item.CTItemData;
using ItemRotation = PulseIQ.Local.Common.Model.Item.ItemRotation;
using PulseIQ.Local.Common.CustomType;

namespace PulseIQ.Local.ClientCommon.AvatarItem
{
    public class ViewAvatar : ViewItemBase
    {
        #region Avatar Events

        internal const string EVENT_MOVE_HEAD = "EVENT_MOVE_HEAD";
        internal const string ANIMATE_LOCAL_AVATAR = "ANIMATE_LOCAL_AVATAR";
        internal const string MOVE_AVATAR = "MOVE_AVATAR";
        internal const string ROTATE_AVATAR = "ROTATE_AVATAR";

        #endregion

        protected string AvatarId = "";
        private string wavingAnimName = "waving";

        #region Avatar object references

        private Animator avatarAnimator;
        public Transform headPose;

        [SerializeField]
        private GameObject bodyGameobject;

        private GameObject namePrefab;
        LineRenderer lineRend = null;
        RaycastHit lineRendHit = new RaycastHit();

        [SerializeField]
        private Transform lineRendererPosition = null;

        private GameObject lineRendPointObj = null;

        #endregion

        #region avatar body rotation variables

        private float rotateTimer = 0f;
        private bool rotatingAvatar = false;
        private Quaternion startBodyYRot = new Quaternion();
        private Quaternion targetBodyRot = new Quaternion();
        private Quaternion currentBodyRot = new Quaternion();

        #endregion

        #region Head rotation variables 

        private bool rotatingHead = false;
        private float headRotationTimer = 0f;
        private const float rotationTime = 0.4f;
        private Quaternion startHeadRot = new Quaternion();
        private Quaternion targetHeadRot = new Quaternion();
        private Quaternion newRot = new Quaternion();

        #endregion

        #region Avatar movement variables

        private bool movingAvatar = false;
        private float movementTimer = 0f;
        private const float movementTime = 1f;
        private Vector3 startPos = new Vector3();
        private Vector3 targetPos = new Vector3();
        private Vector3 newPos = new Vector3();

        #endregion

        #region Avatar flags

        private bool HiddenDueToTut;
        private bool HiddenDueToNotation;
        private bool HiddenNormally;
        public bool isLocal = false;

        #endregion

        protected override internal void init()
        {
            // TODO: more init
            base.init();
            itemType = Local.Common.ItemTypeCode.AVATAR;
            newPos = transform.position;
            newRot = headPose.localRotation;
            avatarAnimator = GetComponentInChildren<Animator>(false);

            if (Application.isEditor && isLocal)
            {
                gameObject.AddComponent<CustomCameraRotation>();
            }
        }

        private void LateUpdate()
        {
            if (rotatingHead)
            {
                headRotationTimer += Time.deltaTime;

                newRot = Quaternion.Slerp(startHeadRot, targetHeadRot, headRotationTimer / rotationTime);

                if (headRotationTimer >= rotationTime)
                {
                    rotatingHead = false;
                    headRotationTimer = 0f;
                    newRot = targetHeadRot;
                }

                headPose.rotation = newRot;
                lineRendererPosition.localRotation = newRot;
            }


            // Rotating avatar body in relation to avatars head rotation
            if (!isLocal)
            {
                currentBodyRot = headPose.rotation * Quaternion.Inverse(transform.rotation);

                if ((currentBodyRot.eulerAngles.y > 45 && currentBodyRot.eulerAngles.y < 315) && !rotatingAvatar)
                {
                    rotateTimer += Time.deltaTime;

                    if (rotateTimer > 2f || (currentBodyRot.eulerAngles.y > 90 && currentBodyRot.eulerAngles.y < 270))
                    {
                        lerpRotateTime = 0;
                        rotateTimer = 0;
                        rotatingAvatar = true;
                        startBodyYRot = transform.rotation;
                        targetBodyRot = Quaternion.Euler(startBodyYRot.eulerAngles.x, headPose.eulerAngles.y, startBodyYRot.eulerAngles.z);
                    }
                }
                else
                {
                    rotateTimer = 0f;
                }

                if (rotatingAvatar)
                {
                    lerpRotateTime += Time.deltaTime;

                    transform.rotation = Quaternion.Lerp(startBodyYRot, targetBodyRot, lerpRotateTime / 1.25f);
                    if (lerpRotateTime > 1.25)
                    {
                        rotatingAvatar = false;
                        rotateTimer = 0;
                    }
                }
            }

            if (null != lineRend && null != lineRendererPosition)
            {
                if (Physics.Raycast(lineRendererPosition.position, lineRendererPosition.forward, out lineRendHit))
                {
                    lineRend.SetPosition(0, lineRendererPosition.position + (lineRendererPosition.forward * 0.2f));
                    lineRend.SetPosition(1, lineRendHit.point);
                    lineRendPointObj.transform.position = lineRendHit.point;
                }
                else
                {
                    lineRend.SetPosition(0, lineRendererPosition.position + (lineRendererPosition.forward * 0.2f));
                    lineRend.SetPosition(1, lineRendererPosition.position + (lineRendererPosition.forward * 100f));
                    lineRendPointObj.transform.position = lineRendererPosition.position + (lineRendererPosition.forward * 100f);
                }
            }

            if (movingAvatar)
            {
                movementTimer += Time.deltaTime;

                transform.position = Vector3.Lerp(startPos, targetPos, movementTimer / movementTime);

                if (movementTimer >= movementTime)
                {
                    movingAvatar = false;
                    movementTimer = 0f;
                    transform.position = targetPos;
                }

                Vector3 direction = Vector3.Normalize(targetPos - transform.position);
                float speed = Vector3.Distance(targetPos, transform.position);

                float angle = Vector2.Angle(new Vector2(transform.forward.x, transform.forward.z), new Vector2(direction.x, direction.z));
                Vector3 cross = Vector3.Cross(transform.forward, Vector3.Normalize(targetPos - transform.position));
                float dir = Vector3.Dot(cross, Vector3.up);

                if (angle < 45)
                {
                    avatarAnimator.SetFloat("SideSpeed", 0);
                    avatarAnimator.SetFloat("ForwardSpeed", speed);
                }
                else if (angle > 45 && angle < 135)
                {
                    if (dir > 0)
                    {
                        avatarAnimator.SetFloat("ForwardSpeed", 0);
                        avatarAnimator.SetFloat("SideSpeed", speed);
                    }
                    else
                    {
                        avatarAnimator.SetFloat("ForwardSpeed", 0);
                        avatarAnimator.SetFloat("SideSpeed", -speed);
                    }
                }
                else
                {
                    avatarAnimator.SetFloat("SideSpeed", 0);
                    avatarAnimator.SetFloat("ForwardSpeed", -speed);
                }
            }

            //transform.position = newPos;
        }

        protected override void OnDestroy()
        {
            if (null != lineRendPointObj)
            {
                Destroy(lineRendPointObj);
            }

            base.OnDestroy();
        }

        public void SetAvatarId(string avatarId)
        {
            AvatarId = avatarId;
        }

        public void SetIsAvatarLocal(bool flag)
        {
            isLocal = flag;
        }

        public override void HideView()
        {
            HiddenNormally = true;
            bodyGameobject.SetActive(false);
            namePrefab.SetActive(false);
        }

        public override void ShowView()
        {
            HiddenNormally = false;
            if (!isLocal)
            {
                bodyGameobject.SetActive(true);
            }

            namePrefab.SetActive(true);
        }

        public void RotateHead(Quaternion destLocalRotation)
        {
            MessageData messageData = new MessageData();

            ItemRotation newLocalRot = new ItemRotation(itemID, new Common.CustomType.CTQuaternion(destLocalRotation.x, destLocalRotation.y, destLocalRotation.z, destLocalRotation.w));

            messageData.Parameters.Add((byte)CommonParameterCode.DATA_AVATAR_HEAD_ROTATION, newLocalRot);

            dispatcher.Dispatch(EVENT_MOVE_HEAD, messageData);
        }

        public void UpdateHeadRotation(CTQuaternion rotation)
        {
            targetHeadRot = new Quaternion(rotation.X, rotation.Y, rotation.Z, rotation.W);

            rotatingHead = true;

            startHeadRot = headPose.rotation;
        }

        public void UpdateAvatarPosition(CTVector3 position)
        {
            if (!isLocal)
            {
                startPos = transform.position;
                targetPos = new Vector3(position.X, transform.position.y, position.Z);
                movingAvatar = true;
                movementTimer = 0f;
            }
        }

        public void PlayAnimation(string itemID, string animName)
        {
            if (isLocal && animName == AvatarAnimations.WAVING)
            {
                MessageData messageData = new MessageData();
                messageData.Parameters.Add((byte)CommonParameterCode.ANIMATION_NAME, animName);

                dispatcher.Dispatch(ANIMATE_LOCAL_AVATAR, messageData);
                return;
            }

            if (animName == wavingAnimName)
            {
                avatarAnimator.Play(animName, 1);
            }
            else
            {
                avatarAnimator.Play(animName);
            }
        }

        /// Leaving following function here, just in case we want to move the avatar movement on slot group evaluation instead of camera
        ////public void SlotGroupEvaluated(MessageData data)
        ////{
        ////    Vector3 pos = (Vector3)data[(byte)CommonParameterCode.SLOT_GROUP_CAM_POS];
        ////    Quaternion rot = (Quaternion)data[(byte)CommonParameterCode.SLOT_GROUP_CAM_ROT];
        ////    Vector3 scale = (Vector3)data[(byte)CommonParameterCode.SLOT_GROUP_CAM_SCL];

        //    CTVector3 itemPos;
        //    CTQuaternion itemRot;
        //    itemPos = new Common.CustomType.CTVector3(pos.x, pos.y, pos.z);
        //    itemRot = new Common.CustomType.CTQuaternion(rot.x, rot.y, rot.z, rot.w);

        //    MessageData posData = new MessageData();
        //    MessageData rotData = new MessageData();
        //    posData.Parameters.Add((byte)CommonParameterCode.ITEM_POSITION, itemPos);
        //    rotData.Parameters.Add((byte)CommonParameterCode.ITEM_ROTATION, itemRot);
        //    posData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);
        //    rotData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);

        //    dispatcher.Dispatch(MOVE_AVATAR, posData);
        //    dispatcher.Dispatch(ROTATE_AVATAR, posData);
        ////}
        public void SetNamePrefab(GameObject nameObj)
        {
            namePrefab = nameObj;
        }

        public void InitialiseLineRenderer()
        {
            lineRend = gameObject.AddComponent<LineRenderer>();
            lineRend.startColor = Color.red;
            lineRend.endColor = Color.red;
            lineRend.startWidth = 0.01f;
            lineRend.endWidth = 0.01f;

            lineRendPointObj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            lineRendPointObj.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
            lineRendPointObj.GetComponent<Collider>().enabled = false;

            Material renderMat = Resources.Load("Materials/rayTraceMat") as Material;
            lineRend.material = renderMat;
        }

        public override void SyncItemData(CTItemData data)
        {
            base.SyncItemData(data);
        }

        public void HideAvatarTutorial(bool value)
        {
            if (value)
            {
                HiddenDueToTut = true;
                bodyGameobject.SetActive(false);
                namePrefab.SetActive(false);
            }
            else
            {
                HiddenDueToTut = false;
                if (!HiddenDueToNotation)
                {
                    if (!HiddenNormally)
                    {
                        if (!isLocal)
                        {
                            bodyGameobject.SetActive(true);
                        }

                        namePrefab.SetActive(true);
                    }
                }
            }
        }

        public void HideAvatarNotation(bool value)
        {
            if (value)
            {
                HiddenDueToNotation = true;
                bodyGameobject.SetActive(false);
                namePrefab.SetActive(false);
            }
            else
            {
                HiddenDueToNotation = false;
                if (!HiddenDueToTut)
                {
                    if (!HiddenNormally)
                    {
                        if (!isLocal)
                        {
                            bodyGameobject.SetActive(true);
                        }

                        namePrefab.SetActive(true);
                    }
                }
            }
        }
    }
}