﻿using UnityEngine;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

using PulseIQ.Local.Client.Unity3DLib.Model;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.BaseItem;
using PulseIQ.Local.Common.Model.Item;
using PulseIQ.Local.Common.CustomType;

namespace PulseIQ.Local.ClientCommon.AvatarItem
{
	public class MediatorAvatar : EventMediator 
	{
		[Inject]
		public ViewAvatar view{ get; set;}

		public override void OnRegister()
		{
			Debug.Log ("MediatorAvatar OnRegister");
			UpdateListeners(true);
			view.init ();
		}

		public override void OnRemove()
		{
			UpdateListeners(false);
		}

		protected void UpdateListeners(bool value)
		{
			Debug.Log ("MediatorAvatar UpdateListeners");
			// out
			view.dispatcher.UpdateListener(value, ViewAvatar.EVENT_MOVE_HEAD, OnMoveHead);
			view.dispatcher.UpdateListener(value, ViewItemBase.EVENT_MOVE, OnMove);
            view.dispatcher.UpdateListener(value, ViewAvatar.MOVE_AVATAR, OnMove);
            view.dispatcher.UpdateListener(value, ViewAvatar.ROTATE_AVATAR, OnRotate);
            view.dispatcher.UpdateListener(value, ViewAvatar.ANIMATE_LOCAL_AVATAR, WaveOnCamera);

            //dispatcher.UpdateListener(value, IntCmdCodeAvatarItem.SLOT_GROUP_EVALUATED, OnSlotGroupEvaluated);

        }

		protected void OnMoveHead(IEvent evt)
		{
			//dispatcher.Dispatch (EEventAvatar.MOVE_AVATAR_HEAD, evt);
		}

        public void WaveOnCamera(IEvent evt)
        {
            dispatcher.Dispatch(ECommonViewEvent.PLAY_AVATAR_WAVE_ANIMATION, evt.data);
        }

        private void OnMessageReceived(System.Object data)
        {
            MessageData avatarData = (MessageData)data;
            ServerCmdCode code = (ServerCmdCode)avatarData[(byte)CommonParameterCode.OPERATION_CODE];

            switch (code)
            {
                case ServerCmdCode.ITEM_MOVED:
                    {
                        view.UpdateAvatarPosition((CTVector3)avatarData[(byte)CommonParameterCode.ITEM_POSITION]);
                        break;
                    }

                case ServerCmdCode.AVATAR_HEAD_ROTATED:
                    {
                        view.UpdateHeadRotation((CTQuaternion)avatarData[(byte)CommonParameterCode.DATA_AVATAR_HEAD_ROTATION]);
                        break;
                    }
                case ServerCmdCode.AVATAR_ANI_PLAYED:
                    {
                        string itemId = (string)avatarData[(byte)CommonParameterCode.ITEMID];
                        string animName = (string)avatarData[(byte)CommonParameterCode.ANIMATION_NAME];
                        view.PlayAnimation(itemId, animName);
                        break;
                    }
                case ServerCmdCode.ITEM_HIDDEN:
                    {
                        view.HideView();
                        break;
                    }

                case ServerCmdCode.ITEM_SHOWN:
                    {
                        view.ShowView();
                        break;
                    }

                case ServerCmdCode.ITEM_STATUS_SYNCED:
                    {
                        CTItemData syncData = (CTItemData)(avatarData[(byte)CommonParameterCode.DATA_CT_ITEM_DATA]);
                        view.SyncItemData(syncData);
                        break;
                    }
            }
        }

        protected void OnMove(IEvent evt)
        {
            dispatcher.Dispatch (SubCode.MoveItem, evt.data);
        }

        protected void OnRotate(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.RotateItem, evt.data);
        }

        //protected void OnSlotGroupEvaluated(IEvent evt)
        //{
        //    view.SlotGroupEvaluated((MessageData)evt.data);
        //}

    }
}
