﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.AvatarItem
{
    public class ViewNameLabel : MonoBehaviour
    {

        [SerializeField]
        TextMesh BottomText;
        [SerializeField]
        TextMesh TopText;

        [SerializeField]
        GameObject TopName;
        [SerializeField]
        GameObject BottomName;

        private bool startRotate = false;
        private GameObject target;

        public void SetNames(string name)
        {
            BottomText.text = name;
            TopText.text = name;
        }

        public void HideNameTag()
        {
            BottomName.SetActive(false);
            TopName.SetActive(false); 
        }

        public void SetNameGameobject(bool isLocalAvatar)
        {
            if (isLocalAvatar)
            {
                BottomName.SetActive(true);
                TopName.SetActive(false);
            }
            else
            {
                BottomName.SetActive(false);
                TopName.SetActive(true);
            }
        }

        public void PositionBillboard(bool isLocalAvatar, GameObject target)
        {
            if (!isLocalAvatar && target != null)
            {
                this.target = target;
                startRotate = true;
            }
        }

        private void Update()
        {
            if (startRotate)
            {
                if(null == target)
                {
                    return;
                }

                TopName.transform.LookAt(target.transform);

                Vector3 rot = TopName.transform.rotation.eulerAngles;
                rot.x = 0;
                rot.z = 0;

                TopName.transform.eulerAngles = rot;
            }
        }
    }
}