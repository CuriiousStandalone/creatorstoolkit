﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.EventSystem;
using PulseIQ.Local.ClientCommon.ItemManager;
using PulseIQ.Local.ClientCommon.BaseItem;
using PulseIQ.Local.ClientCommon.SlotItem;
using PulseIQ.Local.ClientCommon.InteractiveItem;
using PulseIQ.Local.ClientCommon.TimerItem;
using PulseIQ.Local.ClientCommon.TutorialItem;
using PulseIQ.Local.ClientCommon.Multimedia;
using PulseIQ.Local.ClientCommon.PlanarVideoItem;
using PulseIQ.Local.ClientCommon.PanoramaImageItem;
using PulseIQ.Local.ClientCommon.PanoramaVideoItem;
using PulseIQ.Local.ClientCommon.LineNotationItem;
using PulseIQ.Local.ClientCommon.AvatarItem;
using PulseIQ.Local.ClientCommon.UIItem;
using PulseIQ.Local.ClientCommon.SlotGroup;
using PulseIQ.Local.ClientCommon.Components.Common.Model;
using PulseIQ.Local.ClientCommon.Config;
using PulseIQ.Local.ClientCommon.StereoPanoramaVideoItem;
using PulseIQ.Local.ClientCommon.World;
using PulseIQ.Local.ClientCommon.Hotspot;
using PulseIQ.Local.ClientCommon.User;

namespace PulseIQ.Local.ClientCommon
{
    public class ContextCommon : MVCSContext
    {
        public ContextCommon(MonoBehaviour view) : base(view)
        {
        }

        public ContextCommon(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
        {
        }

        protected override void mapBindings()
        {
            //Event System
            //commandBinder.Bind(OpCmdCodeEventSystem.LOAD_ASSOCIATIONS).To<OpCmdLoadAssociations>();
            crossContextBridge.Bind(ECommonCrossContextEvents.ALL_ITEMS_CREATED);
            crossContextBridge.Bind(ECommonCrossContextEvents.RECONNECT_TO_WORLD);
            crossContextBridge.Bind(ECommonCrossContextEvents.SHOWING_MEDIA_AFTER_SYNC);
            crossContextBridge.Bind(ECommonCrossContextEvents.RETRY_LOGIN);

            #region Commom

            injectionBinder.Bind<IItemDataset>().To<ModeItemDataset>().ToSingleton().CrossContext();
            injectionBinder.Bind<iWorldXmlDataset>().To<ModelXmlDataset>().ToSingleton().CrossContext();
            injectionBinder.Bind<IAssociationDataset>().To<ModelAssociationDataset>().ToSingleton().CrossContext();
            injectionBinder.Bind<ICommonModel>().To<CommonModel>().ToSingleton().CrossContext();
            injectionBinder.Bind<IWorldDataSet>().To<ModelWorldDataSet>().ToSingleton().CrossContext();

            #endregion

            #region Item Manager

            mediationBinder.Bind<ViewItemManager>().To<MediatorItemManager>();

            commandBinder.Bind(EventCode.TeleportClassToWorld).To<LogicCommand>();

            commandBinder.Bind(ECommonViewEvent.WORLD_XML_DATA_RECEIVED).To<IntCmdWorldXmlDataReceived>();
            commandBinder.Bind(ECommonViewEvent.SLOT_CLOSED).To<IntCmdSetSlotPlacedItemManager>();

            commandBinder.Bind(ECommonViewEvent.MEDIA_CURRENT_TIME).To<IntCmdMediaDataItemManager>();
            commandBinder.Bind(ECommonViewEvent.MEDIA_HIDE).To<IntCmdMediaDataItemManager>();
            commandBinder.Bind(ECommonViewEvent.MEDIA_SHOW).To<IntCmdMediaDataItemManager>();
            commandBinder.Bind(ECommonViewEvent.NOTATION_UI_CLOSED).To<IntCmdMediaDataItemManager>();
            commandBinder.Bind(ECommonViewEvent.HOTSPOT_SELECTED).To<IntCmdMediaDataItemManager>();
            commandBinder.Bind(ECommonViewEvent.HOTSPOT_DESELECTED).To<IntCmdMediaDataItemManager>();
            commandBinder.Bind(ECommonViewEvent.HOTSPOT_STATUS).To<IntCmdMediaDataItemManager>();


            commandBinder.Bind(ECommonViewEvent.MEDIA_HIDDEN).To<IntCmdNotationMediaDataItemManager>();

            commandBinder.Bind(ECommonViewEvent.NOTATION_HIDE).To<IntCmdNotationDataItemManager>();
            commandBinder.Bind(ECommonViewEvent.NOTATION_SHOW).To<IntCmdNotationDataItemManager>();
            commandBinder.Bind(ECommonViewEvent.NOTATION_DRAWDISTANCE_SET).To<IntCmdNotationDataItemManager>();

            commandBinder.Bind(ECommonViewEvent.TIMER_ACTIVATED).To<IntCmdTimerDataItemManager>();
            commandBinder.Bind(ECommonViewEvent.TIMER_DEACTIVATED).To<IntCmdTimerDataItemManager>();

            commandBinder.Bind(ECommonViewEvent.DROPDOWN_ITEM_HIDDEN).To<IntCmdDropdownDataItemManager>();
            commandBinder.Bind(ECommonViewEvent.DROPDOWN_CLEAR).To<IntCmdDropdownDataItemManager>();

            commandBinder.Bind(ECommonViewEvent.TUTORIAL_ABANDONED).To<IntCmdTutorialDataItemManager>();
            commandBinder.Bind(ECommonViewEvent.TUTORIAL_DATA).To<IntCmdTutorialDataItemManager>();
            commandBinder.Bind(ECommonViewEvent.TUTORIAL_ANIM_STARTED).To<IntCmdTutorialDataItemManager>();

            commandBinder.Bind(IntCmdCodeItemManager.WORLD_NOTATE_END).To<IntCmdWorldNotateEndItemManager>();

            commandBinder.Bind(IntCmdCodeItemManager.NOTATE_PRESSED).To<IntCmdFindNotationItemManager>();
            commandBinder.Bind(IntCmdCodeItemManager.ALL_ITEMS_REMOVED).To<IntCmdAllItemsRemovedItemManager>();
            commandBinder.Bind(IntCmdCodeItemManager.WORLD_DATA_STORED).To<IntCmdWorldItemStoredItemManager>();
            commandBinder.Bind(IntCmdCodeItemManager.WORLD_ITEMS_CREATED).To<IntCmdWorldItemsCreatedItemManager>()
                .To<IntCmdCheckSlotGroupItemManager>();
            commandBinder.Bind(IntCmdCodeItemManager.ALL_ITEMS_DESTROYED).To<IntCmdWorldItemsDestroyedItemManager>();
            commandBinder.Bind(IntCmdCodeItemManager.INTERACTIVE_ITEM_CREATED)
                .To<IntCmdCheckInteractivePlacedItemManager>();
            commandBinder.Bind(ServerCmdCode.TeleportClassToWorld).To<OpCmdDestroyUIItemManager>();

            commandBinder.Bind(ECommonViewEvent.ITEM_OBTAINED).To<IntCmdCameraMessageItemManager>();
            commandBinder.Bind(ECommonViewEvent.ITEM_ABANDONED).To<IntCmdCameraMessageItemManager>();
            commandBinder.Bind(ECommonViewEvent.SHOW_VR).To<IntCmdCameraMessageItemManager>();
            commandBinder.Bind(ECommonViewEvent.HIDE_VR).To<IntCmdCameraMessageItemManager>();
            commandBinder.Bind(ECommonViewEvent.SET_POST_PROCESSING).To<IntCmdCameraMessageItemManager>();
            commandBinder.Bind(ECommonViewEvent.TUTORIAL_DEACTIVATED).To<IntCmdCameraMessageItemManager>();
            commandBinder.Bind(ECommonViewEvent.WORLD_NOTATION_STARTED).To<IntCmdCameraMessageItemManager>();
            commandBinder.Bind(ECommonViewEvent.WORLD_NOTATION_ENDED).To<IntCmdCameraMessageItemManager>();
            commandBinder.Bind(ECommonViewEvent.THROWING_INTERACTIVE_ITEM).To<IntCmdCameraMessageItemManager>();
            commandBinder.Bind(ECommonViewEvent.OBSERVE_SLOT_ITEM).To<IntCmdCameraMessageItemManager>();
            commandBinder.Bind(ECommonViewEvent.STOP_OBSERVE_SLOT_ITEM).To<IntCmdCameraMessageItemManager>();
            commandBinder.Bind(ECommonViewEvent.SLOT_GROUP_EVALUATE).To<IntCmdCameraMessageItemManager>();
            commandBinder.Bind(ECommonViewEvent.FADE_CAMERA_IN).To<IntCmdCameraMessageItemManager>();
            commandBinder.Bind(ECommonViewEvent.FADE_CAMERA_IN_OUT).To<IntCmdCameraMessageItemManager>();
            commandBinder.Bind(ECommonViewEvent.FADE_CAMERA_OUT).To<IntCmdCameraMessageItemManager>();

            commandBinder.Bind(ECommonViewEvent.NOTATION_SHOWN).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(ECommonViewEvent.NOTATION_HIDDEN).To<IntCmdMessageRelayItemManager>();

            #endregion

            #region Base Item

            mediationBinder.Bind<ViewItemBase>().To<MediatorItemBase>();

            commandBinder.Bind(EventCode.ItemAbandoned).To<LogicCommand>().To<OpCmdEvtReceived>();
            commandBinder.Bind(EventCode.ItemMoved).To<LogicCommand>();
            commandBinder.Bind(EventCode.ItemRotated).To<LogicCommand>();
            commandBinder.Bind(EventCode.ItemScaled).To<LogicCommand>();
            commandBinder.Bind(EventCode.ItemObtained).To<LogicCommand>();
            commandBinder.Bind(EventCode.ItemObtainDenied).To<LogicCommand>();
            commandBinder.Bind(EventCode.ItemDestroyed).To<LogicCommand>();
            commandBinder.Bind(EventCode.ItemCreated).To<LogicCommand>();
            commandBinder.Bind(EventCode.ItemOfflineMoved).To<LogicCommand>();
            commandBinder.Bind(EventCode.ItemOfflineRotated).To<LogicCommand>();
            commandBinder.Bind(EventCode.ItemOfflineScaled).To<LogicCommand>();
            commandBinder.Bind(EventCode.ItemShown).To<LogicCommand>();
            commandBinder.Bind(EventCode.ItemHidden).To<LogicCommand>();
            commandBinder.Bind(EventCode.ItemStatusSynced).To<LogicCommand>();
            commandBinder.Bind(EventCode.SlotGroupStatusSynced).To<LogicCommand>();
            commandBinder.Bind(EventCode.ItemLerpMoved).To<LogicCommand>();
            commandBinder.Bind(EventCode.ItemLerpRotated).To<LogicCommand>();
            commandBinder.Bind(EventCode.ItemLerpScaled).To<LogicCommand>();

            commandBinder.Bind(SubCode.CreateItem).To<OpCmdCreateBaseItem>();
            commandBinder.Bind(SubCode.DestroyItem).To<OpCmdDestroyBaseItem>();
            commandBinder.Bind(SubCode.TryAbandonItem).To<OpCmdAbandonBaseItem>();
            commandBinder.Bind(SubCode.TryObtainItem).To<OpCmdObtainBaseItem>();
            commandBinder.Bind(SubCode.MoveItem).To<OpCmdMoveBaseItem>();
            commandBinder.Bind(SubCode.RotateItem).To<OpCmdRotateBaseItem>();
            commandBinder.Bind(SubCode.ScaleItem).To<OpCmdScaleBaseItem>();
            //commandBinder.Bind(SubCode.MoveToPostion).To<OpCmdMoveToPositionBaseItem>();
            commandBinder.Bind(SubCode.LerpMoveItem).To<OpCmdPosLerpBaseItem>();
            commandBinder.Bind(SubCode.LerpRotateItem).To<OpCmdRotLerpBaseItem>();
            commandBinder.Bind(SubCode.LerpScaleItem).To<OpCmdScaleLerpBaseItem>();

            //TEMPO
            commandBinder.Bind(ServerCmdCode.ITEM_THROWN).To<OpCmdMovedBaseItem>();

            commandBinder.Bind(ServerCmdCode.ALL_ITEM_DESTROYED).To<OpCmdDestroyAllItems>();

            commandBinder.Bind(ServerCmdCode.ITEM_MOVED).To<OpCmdMovedBaseItem>();
            commandBinder.Bind(ServerCmdCode.ITEM_ROTATED).To<OpCmdRotatedBaseItem>();
            commandBinder.Bind(ServerCmdCode.ITEM_SCALED).To<OpCmdScaledBaseItem>();
            commandBinder.Bind(ServerCmdCode.ITEM_ABANDONED).To<OpCmdAbandonedBaseItem>();
            commandBinder.Bind(ServerCmdCode.ITEM_OBTAIN_FAILED).To<OpCmdObtainFailedBaseItem>();
            commandBinder.Bind(ServerCmdCode.ITEM_OBTAIN_SUCCESS).To<OpCmdObtainSuccessBaseItem>();
            commandBinder.Bind(ServerCmdCode.ITEM_CREATED).To<OpCmdCreatedBaseItem>();
            commandBinder.Bind(ServerCmdCode.ITEM_DESTROYED).To<OpCmdDestroyBaseItem>();
            commandBinder.Bind(ServerCmdCode.ITEM_OFFLINE_MOVED).To<OpCmdMovedOfflineBaseItem>();
            commandBinder.Bind(ServerCmdCode.ITEM_OFFLINE_ROTATED).To<OpCmdRotatedOfflineBaseItem>();
            commandBinder.Bind(ServerCmdCode.ITEM_OFFLINE_SCALED).To<OpCmdScaledOfflineBaseItem>();
            commandBinder.Bind(ServerCmdCode.ITEM_SHOWN).To<OpCmdItemShown>();
            commandBinder.Bind(ServerCmdCode.ITEM_HIDDEN).To<OpCmdItemHidden>();
            commandBinder.Bind(ServerCmdCode.ITEM_STATUS_SYNCED).To<OpCmdItemStatusSync>();
            commandBinder.Bind(ServerCmdCode.ITEM_LERP_MOVED).To<OpCmdPosLerpedBaseItem>();
            commandBinder.Bind(ServerCmdCode.ITEM_LERP_ROTATED).To<OpCmdRotLerpedBaseItem>();
            commandBinder.Bind(ServerCmdCode.ITEM_LERP_SCALED).To<OpCmdScaleLerpedBaseItem>();

            commandBinder.Bind(IntCmdCodeBaseItem.CREATED).To<IntCmdCreateItemItemManager>();
            commandBinder.Bind(IntCmdCodeBaseItem.DESTROY).To<IntCmdDestroyItemItemManager>();

            commandBinder.Bind(IntCmdCodeBaseItem.ABANDONED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeBaseItem.MOVED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeBaseItem.OBTAIN_FAILED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeBaseItem.OBTAIN_SUCCESS).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeBaseItem.OFFLINE_MOVED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeBaseItem.OFFLINE_ROTATED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeBaseItem.OFFLINE_SCALED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeBaseItem.ITEM_SHOWN).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeBaseItem.ITEM_HIDDEN).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeBaseItem.STATUS_SYNC).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeBaseItem.LERP_MOVED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeBaseItem.LERP_ROTATED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeBaseItem.LERP_SCALED).To<IntCmdMessageRelayItemManager>();

            commandBinder.Bind(IntCmdCodeBaseItem.ATTACH_TO_HOLDING_POS).To<OpCmdOfflineMoveToInteractiveItem>();
            commandBinder.Bind(IntCmdCodeBaseItem.ATTACH_TO_HOLDING_ROT).To<OpCmdOfflineRotateToInteractiveItem>();
            commandBinder.Bind(IntCmdCodeBaseItem.ATTACH_TO_HOLDING_SCALE).To<OpCmdOfflineScaleToInteractiveItem>();

            commandBinder.Bind(OpCmdCodeBaseItem.LOCAL_MOVE_TO).To<OpCmdLocalMoveToBaseItem>();
            commandBinder.Bind(OpCmdCodeBaseItem.LOCAL_ROTATE_TO).To<OpCmdLocalRotateToBaseItem>();
            commandBinder.Bind(OpCmdCodeBaseItem.LOCAL_SCALE_TO).To<OpCmdLocalScaleToBaseItem>();

            #endregion

            #region Avatar

            mediationBinder.Bind<ViewAvatar>().To<MediatorAvatar>();

            commandBinder.Bind(EventCode.AvatarCreated).To<LogicCommand>();
            commandBinder.Bind(EventCode.AvatarHeadRotated).To<LogicCommand>();
            commandBinder.Bind(EventCode.AvatarAnimationPlayed).To<LogicCommand>();

            commandBinder.Bind(SubCode.CreateAvatar).To<OpCmdCreateAvatarItem>();
            commandBinder.Bind(SubCode.AvatarRotateHead).To<OpCmdRotateHeadAvatarItem>();
            commandBinder.Bind(SubCode.AvatarCreated).To<OpCmdAvatarCreated>();

            commandBinder.Bind(ServerCmdCode.AVATAR_CREATED).To<OpCmdCreatedAvatarItem>();
            commandBinder.Bind(ServerCmdCode.AVATAR_HEAD_ROTATED).To<OpCmdHeadMovedAvatarItem>();
            commandBinder.Bind(ServerCmdCode.AVATAR_ANI_PLAYED).To<OpCmdAvatarAnimated>();

            commandBinder.Bind(IntCmdCodeAvatarItem.CREATED).To<IntCmdCreateItemItemManager>();
            commandBinder.Bind(IntCmdCodeAvatarItem.HEAD_MOVED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeAvatarItem.MOVED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeAvatarItem.ANIMATION_PLAYED).To<IntCmdMessageRelayItemManager>();

            commandBinder.Bind(IntCmdCodeSlotGroup.TELEPORT_AVATAR).To<IntCmdSlotGroupEvaluatedAvatarItem>();

            commandBinder.Bind(ECommonViewEvent.PLAY_AVATAR_WAVE_ANIMATION).To<IntCmdCameraMessageItemManager>();

            commandBinder.Bind(OpCmdCodeAvatarItem.AVATAR_ANIM_PLAYED).To<OpCmdPlayAvatarAnimation>();

            #endregion

            #region Slot Item

            mediationBinder.Bind<ViewSlotItem>().To<MediatorSlotItem>();

            commandBinder.Bind(EventCode.SlotCreated).To<LogicCommand>();
            commandBinder.Bind(EventCode.SlotIndicatorShown).To<LogicCommand>();
            commandBinder.Bind(EventCode.SlotIndicatorHidden).To<LogicCommand>();
            commandBinder.Bind(EventCode.SlotStateAniPlayed).To<LogicCommand>();
            commandBinder.Bind(EventCode.SlotStateAniStopped).To<LogicCommand>();
            commandBinder.Bind(EventCode.SlotClosed).To<LogicCommand>();
            commandBinder.Bind(EventCode.SlotOpened).To<LogicCommand>();

            commandBinder.Bind(SubCode.CloseSlot).To<OpCmdCloseSlotItem>();
            commandBinder.Bind(SubCode.OpenSlot).To<OpCmdOpenSlotItem>();
            commandBinder.Bind(SubCode.ShowSlotIndicator).To<OpCmdShowIndicatorSlotItem>();
            commandBinder.Bind(SubCode.HideSlotIndicator).To<OpCmdHideIndicatorSlotItem>();

            commandBinder.Bind(OpCmdCodeSlotItem.SPECTATE_SLOT).To<OpCmdSpectateSlotItem>();
            commandBinder.Bind(OpCmdCodeSlotItem.STOP_SPECTATE).To<OpCmdStopSpectateSlotItem>();

            commandBinder.Bind(ServerCmdCode.SLOT_CLOSED).To<OpCmdClosedSlotItem>();
            commandBinder.Bind(ServerCmdCode.SLOT_OPENED).To<OpCmdOpenedSlotItem>();
            commandBinder.Bind(ServerCmdCode.SLOT_INDICATOR_HIDDEN).To<OpCmdIndicatorHiddenSlotItem>();
            commandBinder.Bind(ServerCmdCode.SLOT_INDICATOR_SHOWN).To<OpCmdIndicatorShownSlotItem>();
            commandBinder.Bind(ServerCmdCode.SLOT_CREATED).To<OpCmdCreatedSlotItem>();
            commandBinder.Bind(ServerCmdCode.SLOT_STATE_ANI_PLAYED).To<OpCmdSlotAnimationPlayed>();
            commandBinder.Bind(ServerCmdCode.SLOT_STATE_ANI_STOPPED).To<OpCmdSlotAnimationStopped>();

            commandBinder.Bind(IntCmdCodeSlotItem.CREATED).To<IntCmdCreateItemItemManager>();

            commandBinder.Bind(IntCmdCodeSlotItem.CLOSED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeSlotItem.INDICATOR_HIDDEN).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeSlotItem.INDICATOR_SHOWN).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeSlotItem.OPENED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeSlotItem.SLOT_STATE_ANI_PLAYED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeSlotItem.SLOT_STATE_ANI_STOPPED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeSlotItem.SLOT_ANI_ENDED).To<IntCmdToItemManagerSlotAnimEnded>();

            #endregion

            #region Interactive Item

            mediationBinder.Bind<ViewItemInteractive>().To<MediatorItemInteractive>();

            commandBinder.Bind(EventCode.InteractiveItemCreated).To<LogicCommand>();
            commandBinder.Bind(EventCode.InteractiveItemPlacedToSlot).To<LogicCommand>();
            commandBinder.Bind(EventCode.InteractiveItemPlaceToSlotDenied).To<LogicCommand>();

            commandBinder.Bind(SubCode.TryPlaceInteractiveItem).To<OpCmdPlaceSlotInteractiveItem>();

            commandBinder.Bind(ServerCmdCode.INTERACTIVE_SLOT_PLACED).To<OpCmdPlacedSlotInteractiveItem>();
            commandBinder.Bind(ServerCmdCode.INTERACTIVE_SLOT_DENIED).To<OpCmdSlotDeniedInteractiveItem>();
            commandBinder.Bind(ServerCmdCode.INTERACTIVE_CREATED).To<OpCmdCreatedInteractiveItem>();

            commandBinder.Bind(IntCmdCodeInteractiveItem.CREATED).To<IntCmdCreateItemItemManager>();

            commandBinder.Bind(IntCmdCodeInteractiveItem.PLACED_TO_SLOT).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeInteractiveItem.SLOT_DENIED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeInteractiveItem.MOVE_TO_POSITION).To<IntCmdMoveToInteractiveItem>();
            commandBinder.Bind(IntCmdCodeInteractiveItem.UPDATE_MOVE_TO).To<IntCmdUpdateMoveToInteractiveItem>();
            commandBinder.Bind(IntCmdCodeInteractiveItem.LOCAL_MOVE_TO).To<IntCmdLocalMoveToInteractiveItem>();
            commandBinder.Bind(IntCmdCodeInteractiveItem.LOCAL_ROTATE_TO).To<IntCmdLocalRotateToInteractiveItem>();
            commandBinder.Bind(IntCmdCodeInteractiveItem.LOCAL_SCALE_TO).To<IntCmdLocalScaleToInteractiveItem>();

            #endregion

            #region Config

            commandBinder.Bind(ECommonCrossContextEvents.CHECK_GLOBAL_SETTINGS_SERVER_IP)
                .To<OpCmdCheckGlobalSettingsServerIP>();
            commandBinder.Bind(ECommonCrossContextEvents.SCAN_IP_CONNECTIONS).To<OpCmdScanIPs>();
            commandBinder.Bind(ECommonCrossContextEvents.SET_NEW_IP).To<OpCmdSetNewIP>();
            commandBinder.Bind(ECommonCrossContextEvents.SET_NEW_USERID).To<OpCmdSetNewUserID>();

            commandBinder.Bind(IntCmdCodeConfig.ATTEMPT_CONNECTION_ON_IP).To<IntCmdAttemptIPConnection>();

            #endregion

            #region Slot Group

            mediationBinder.Bind<ViewSlotGroup>().To<MediatorSlotGroup>();

            commandBinder.Bind(SubCode.EvaluateSlotGroup).To<OpCmdEvaluateSlotGroup>();

            commandBinder.Bind(EventCode.SlotGroupCreated).To<LogicCommand>();
            commandBinder.Bind(EventCode.SlotGroupEvaluated).To<LogicCommand>();
            commandBinder.Bind(EventCode.SubSlotGroupActivated).To<LogicCommand>();
            commandBinder.Bind(EventCode.SlotGroupFinishAniPlayed).To<LogicCommand>();
            commandBinder.Bind(EventCode.SlotGroupEvaluationEnabled).To<LogicCommand>();
            commandBinder.Bind(EventCode.SlotGroupEvaluationDisabled).To<LogicCommand>();

            commandBinder.Bind(OpCmdCodeSlotGroup.ACTIVATE_SUB_SLOT_GROUP).To<OpCmdActivateSubSlotGroup>();

            commandBinder.Bind(ServerCmdCode.SLOT_GROUP_CREATED).To<OpCmdCreatedSlotGroup>();
            commandBinder.Bind(ServerCmdCode.SLOT_GROUP_SUBSLOTGROUP_ACTIVATED).To<OpCmdSubSlotGroupActivated>();
            commandBinder.Bind(ServerCmdCode.SLOT_GROUP_FINISH_ANI_PLAYED).To<OpCmdSlotGroupFinishAnimationPlayed>();
            commandBinder.Bind(ServerCmdCode.SLOT_GROUP_EVALUATED).To<OpCmdSubSlotGroupEvaluated>();
            commandBinder.Bind(ServerCmdCode.SLOT_GROUP_EVALUATION_ENABLED).To<OpCmdSlotGroupEvaluationVisibility>();
            commandBinder.Bind(ServerCmdCode.SLOT_GROUP_EVALUATION_DISABLED).To<OpCmdSlotGroupEvaluationVisibility>();
            commandBinder.Bind(ServerCmdCode.SLOT_GROUP_STATUS_SYNC).To<OpCmdSlotGroupStatusSynced>();

            commandBinder.Bind(IntCmdCodeSlotGroup.CREATED).To<IntCmdCreateItemItemManager>();
            commandBinder.Bind(IntCmdCodeSlotGroup.SUB_GROUP_ACTIVATED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeSlotGroup.UPDATE_SUB_GROUP_NAME)
                .To<IntCmdMessageRelayItemManagerForItemIndex>();
            commandBinder.Bind(IntCmdCodeSlotGroup.SLOT_GROUP_FINISH_ANI_PLAYED)
                .To<IntCmdMessageRelayItemManagerForItemIndex>();
            commandBinder.Bind(IntCmdCodeSlotGroup.SUB_GROUP_EVALUATED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeSlotGroup.SLOT_GROUP_EVALUATION_VISIBILITY)
                .To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeSlotGroup.SLOT_GROUP_EVALUATION_UI_VISIBILITY)
                .To<IntCmdMessageRelayItemManagerForItemIndex>();
            commandBinder.Bind(IntCmdCodeSlotGroup.REPLAY_SLOT_GROUP_ANIMATIONS).To<IntCmdReplaySlotGroupAnimation>();
            commandBinder.Bind(IntCmdCodeSlotGroup.SLOT_GROUP_REPLAY_ANIMATIONS_ENDED)
                .To<IntCmdSlotGroupReplayAnimEnded>();
            commandBinder.Bind(IntCmdCodeSlotGroup.SLOT_GROUP_STATUS_SYNCED).To<IntCmdMessageRelayItemManager>();

            #endregion

            #region Timer Item

            mediationBinder.Bind<ViewTimerItem>().To<MediatorTimerItem>();

            commandBinder.Bind(EventCode.SyncTimerCreated).To<LogicCommand>();
            commandBinder.Bind(EventCode.SyncTimerPaused).To<LogicCommand>();
            commandBinder.Bind(EventCode.SyncTimerReset).To<LogicCommand>();
            commandBinder.Bind(EventCode.SyncTimerStarted).To<LogicCommand>();
            commandBinder.Bind(EventCode.SyncTimerElapsed).To<LogicCommand>();
            commandBinder.Bind(EventCode.SyncTimerEnded).To<LogicCommand>();

            commandBinder.Bind(SubCode.PauseSyncTimer).To<OpCmdPauseTimerItem>();
            commandBinder.Bind(SubCode.ResetSyncTimer).To<OpCmdResetTimerItem>();
            commandBinder.Bind(SubCode.StartSyncTimer).To<OpCmdStartTimerItem>();

            commandBinder.Bind(ServerCmdCode.TIMER_ELAPSED).To<OpCmdElapsedTimerItem>();
            commandBinder.Bind(ServerCmdCode.TIMER_ENDED).To<OpCmdEndedTimerItem>();
            commandBinder.Bind(ServerCmdCode.TIMER_PAUSED).To<OpCmdPausedTimerItem>();
            commandBinder.Bind(ServerCmdCode.TIMER_RESET).To<OpCmdRestartedTimerItem>();
            commandBinder.Bind(ServerCmdCode.TIMER_STARTED).To<OpCmdStartedTimerItem>();
            commandBinder.Bind(ServerCmdCode.TIMER_CREATED).To<OpCmdCreatedTimerItem>();

            commandBinder.Bind(IntCmdCodeTimerItem.CREATED).To<IntCmdCreateItemItemManager>();

            commandBinder.Bind(IntCmdCodeTimerItem.ELAPSED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeTimerItem.ENDED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeTimerItem.PAUSED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeTimerItem.RESTARTED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeTimerItem.STARTED).To<IntCmdMessageRelayItemManager>();

            #endregion

            #region Tutorial item

            mediationBinder.Bind<ViewTutorialItem>().To<MediatorTutorialItem>();

            commandBinder.Bind(EventCode.TutorialItemCreated).To<LogicCommand>();
            commandBinder.Bind(EventCode.TutorialItemActivated).To<LogicCommand>();
            commandBinder.Bind(EventCode.TutorialItemAnimationPaused).To<LogicCommand>();
            commandBinder.Bind(EventCode.TutorialItemAnimationStarted).To<LogicCommand>();
            commandBinder.Bind(EventCode.TutorialItemAnimationStopped).To<LogicCommand>();
            commandBinder.Bind(EventCode.TutorialItemDeactivated).To<LogicCommand>();
            commandBinder.Bind(EventCode.TutorialItemStatusSynced).To<LogicCommand>();

            commandBinder.Bind(SubCode.ActivateTutorialItem).To<OpCmdActivateTutorialItem>();
            commandBinder.Bind(SubCode.DeactivateTutorialItem).To<OpCmdDeactivateTutorialItem>();
            commandBinder.Bind(SubCode.PauseTutorialItemAniamtion).To<OpCmdPauseAnimationTutorialItem>();
            commandBinder.Bind(SubCode.StartTutorialItemAnimation).To<OpCmdStartAnimationTutorialItem>();
            commandBinder.Bind(SubCode.StopTutorialItemAnimation).To<OpCmdStopAnimationTutorialItem>();

            commandBinder.Bind(ServerCmdCode.TUTORIAL_CREATED).To<OpCmdCreatedTutorialItem>();
            commandBinder.Bind(ServerCmdCode.TUTORIAL_ACTIVATED).To<OpCmdActivatedTutorialItem>();
            commandBinder.Bind(ServerCmdCode.TUTORIAL_DEACTIVATED).To<OpCmdDeactivatedTutorialItem>();
            commandBinder.Bind(ServerCmdCode.TUTORIAL_PAUSED).To<OpCmdPausedAnimationTutorialItem>();
            commandBinder.Bind(ServerCmdCode.TUTORIAL_STARTED).To<OpCmdStartedAnimationTutorialItem>();
            commandBinder.Bind(ServerCmdCode.TUTORIAL_STOPPED).To<OpCmdStoppedAnimationTutorialItem>();
            commandBinder.Bind(ServerCmdCode.SYNC_TUTORIAL_ITEM_STATUS).To<OpCmdSyncTutorialItemStatus>();

            commandBinder.Bind(IntCmdCodeTutorialItem.CREATED).To<IntCmdCreateItemItemManager>();

            commandBinder.Bind(IntCmdCodeTutorialItem.ACTIVATED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeTutorialItem.DEACTIVATED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeTutorialItem.PAUSED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeTutorialItem.STARTED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeTutorialItem.STOPPED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeTutorialItem.SYNCED).To<IntCmdMessageRelayItemManager>();

            #endregion

            #region Multimedia

            commandBinder.Bind(EventCode.EndBehaviorTeleport).To<LogicCommand>();

            commandBinder.Bind(SubCode.TriggerEndBehavior).To<OpCmdTriggerEndBehaviour>();

            commandBinder.Bind(ServerCmdCode.END_BEHAVIOUR_TELEPORTED).To<OpCmdEndBehaviourTriggered>();

            commandBinder.Bind(IntCmdCodeMultimedia.END_BEHAVIOUR_TELEPORTED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeMultimedia.SHOW_END_BEHAVIOUR_TELEPORT_MEDIA)
                .To<IntCmdShowEndBehaviourTeleportMedia>();
            commandBinder.Bind(IntCmdCodeMultimedia.PLAYING_MEDIA_AFTER_SYNC).To<IntCmdPlayingMediaAfterSync>();

            #endregion

            #region Planar Item

            mediationBinder.Bind<ViewPlanarVideoPlayer>().To<MediatorPlanarVideoPlayer>();

            commandBinder.Bind(EventCode.PlanarVideoCreated).To<LogicCommand>();
            commandBinder.Bind(EventCode.PlanarVideoHidden).To<LogicCommand>();
            commandBinder.Bind(EventCode.PlanarVideoShown).To<LogicCommand>();
            commandBinder.Bind(EventCode.PlanarVideoPaused).To<LogicCommand>();
            commandBinder.Bind(EventCode.PlanarVideoPlayed).To<LogicCommand>();
            commandBinder.Bind(EventCode.PlanarVideoReplayed).To<LogicCommand>();
            commandBinder.Bind(EventCode.PlanarVideoSought).To<LogicCommand>();
            commandBinder.Bind(EventCode.PlanarVideoStopped).To<LogicCommand>();

            commandBinder.Bind(SubCode.HidePlanarVideo).To<OpCmdHidePlanarVideo>();
            commandBinder.Bind(SubCode.PausePlanarVideo).To<OpCmdPausePlanarVideo>();
            commandBinder.Bind(SubCode.ReplayPlanarVideo).To<OpCmdResetPlanarVideo>();
            commandBinder.Bind(SubCode.SeekPlanarVideo).To<OpCmdSeekPlanarVideo>();
            commandBinder.Bind(SubCode.ShowPlanarVideo).To<OpCmdShowPlanarVideo>();
            commandBinder.Bind(SubCode.PlayPlanarVideo).To<OpCmdStartPlanarVideo>();
            commandBinder.Bind(SubCode.StopPlanarVideo).To<OpCmdStopPlanarVideo>();

            commandBinder.Bind(ServerCmdCode.PLANAR_VIDEO_CREATED).To<OpCmdCreatedPlanarVideo>();
            commandBinder.Bind(ServerCmdCode.PLANAR_VIDEO_HIDDEN).To<OpCmdHiddenPlanarVideo>();
            commandBinder.Bind(ServerCmdCode.PLANAR_VIDEO_PAUSED).To<OpCmdPausedPlanarVideo>();
            commandBinder.Bind(ServerCmdCode.PLANAR_VIDEO_PLAYED).To<OpCmdStartedPlanarVideo>();
            commandBinder.Bind(ServerCmdCode.PLANAR_VIDEO_REPLAYED).To<OpCmdRestartedPlanarVideo>();
            commandBinder.Bind(ServerCmdCode.PLANAR_VIDEO_SHOWN).To<OpCmdShownPlanarVideo>();
            commandBinder.Bind(ServerCmdCode.PLANAR_VIDEO_SOUGHT).To<OpCmdSoughtPlanarVideo>();
            commandBinder.Bind(ServerCmdCode.PLANAR_VIDEO_STOPPED).To<OpCmdStoppedPlanarVideo>();

            commandBinder.Bind(IntCmdCodePlanarVideo.CREATED).To<IntCmdCreateItemItemManager>();

            commandBinder.Bind(IntCmdCodePlanarVideo.HIDDEN).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodePlanarVideo.PAUSED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodePlanarVideo.RESTARTED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodePlanarVideo.SHOWN).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodePlanarVideo.SOUGHT).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodePlanarVideo.STARTED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodePlanarVideo.STOPPED).To<IntCmdMessageRelayItemManager>();

            commandBinder.Bind(IntCmdCodePlanarVideo.RETURN_CAMERA_ROT).To<IntCmdReturnCameraRotAvatarItem>();
            commandBinder.Bind(IntCmdCodePlanarVideo.ROTATE_CAMERA).To<IntCmdRotateCameraAvatarItem>();

            commandBinder.Bind(IntCmdCodePlanarVideo.LOADED).To<IntCmdLoadCompletePlanarVideo>();

            #endregion

            #region Panorama Video Item

            mediationBinder.Bind<ViewPanoramaVideoPlayer>().To<MediatorPanoramaVideoPlayer>();

            commandBinder.Bind(EventCode.CloseAllMultimedia).To<LogicCommand>();

            commandBinder.Bind(EventCode.PanoramaVideoCreated).To<LogicCommand>();
            commandBinder.Bind(EventCode.PanoramaVideoHidden).To<LogicCommand>();
            commandBinder.Bind(EventCode.PanoramaVideoShown).To<LogicCommand>();
            commandBinder.Bind(EventCode.PanoramaVideoPaused).To<LogicCommand>();
            commandBinder.Bind(EventCode.PanoramaVideoPlayed).To<LogicCommand>();
            commandBinder.Bind(EventCode.PanoramaVideoReplayed).To<LogicCommand>();
            commandBinder.Bind(EventCode.PanoramaVideoSought).To<LogicCommand>();
            commandBinder.Bind(EventCode.PanoramaVideoStopped).To<LogicCommand>();
            commandBinder.Bind(EventCode.PanoramaVideoStatusSynced).To<LogicCommand>();

            commandBinder.Bind(SubCode.HidePanoramaVideo).To<OpCmdHidePanoramaVideo>().To<OpCmdCloseAllMultimedias>();
            commandBinder.Bind(SubCode.PausePanoramaVideo).To<OpCmdPausePanoramaVideo>();
            commandBinder.Bind(SubCode.ReplayPanoramaVideo).To<OpCmdResetPanoramaVideo>();
            commandBinder.Bind(SubCode.SeekPanoramaVideo).To<OpCmdSeekPanoramaVideo>();
            commandBinder.Bind(SubCode.ShowPanoramaVideo).To<OpCmdShowPanoramaVideo>();
            commandBinder.Bind(SubCode.PlayPanoramaVideo).To<OpCmdStartPanoramaVideo>();
            commandBinder.Bind(SubCode.StopPanoramaVideo).To<OpCmdStopPanoramaVideo>();

            commandBinder.Bind(ServerCmdCode.PANORAMA_VIDEO_CREATED).To<OpCmdCreatedPanoramaVideo>();
            commandBinder.Bind(ServerCmdCode.PANORAMA_VIDEO_HIDDEN).To<OpCmdHiddenPanoramaVideo>();
            commandBinder.Bind(ServerCmdCode.PANORAMA_VIDEO_PAUSED).To<OpCmdPausedPanoramaVideo>();
            commandBinder.Bind(ServerCmdCode.PANORAMA_VIDEO_PLAYED).To<OpCmdStartedPanoramaVideo>();
            commandBinder.Bind(ServerCmdCode.PANORAMA_VIDEO_REPLAYED).To<OpCmdRestartedPanoramaVideo>();
            commandBinder.Bind(ServerCmdCode.PANORAMA_VIDEO_SHOWN).To<OpCmdShownPanoramaVideo>();
            commandBinder.Bind(ServerCmdCode.PANORAMA_VIDEO_SOUGHT).To<OpCmdSoughtPanoramaVideo>();
            commandBinder.Bind(ServerCmdCode.PANORAMA_VIDEO_STOPPED).To<OpCmdStoppedPanoramaVideo>();
            commandBinder.Bind(ServerCmdCode.SYNC_PANORAMA_VIDEO_STATUS).To<OpCmdPanoramaVideoSynced>();

            commandBinder.Bind(IntCmdCodePanoramaVideo.CREATED).To<IntCmdCreateItemItemManager>();

            commandBinder.Bind(IntCmdCodePanoramaVideo.HIDDEN).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodePanoramaVideo.PAUSED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodePanoramaVideo.RESTARTED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodePanoramaVideo.SHOWN).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodePanoramaVideo.SOUGHT).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodePanoramaVideo.STARTED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodePanoramaVideo.STOPPED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodePanoramaVideo.STATUS_SYNCED).To<IntCmdMessageRelayItemManager>();

            commandBinder.Bind(IntCmdCodePanoramaVideo.RETURN_CAMERA_ROT).To<IntCmdReturnCameraRotAvatarItem>();
            commandBinder.Bind(IntCmdCodePanoramaVideo.ROTATE_CAMERA).To<IntCmdRotateCameraAvatarItem>();
            commandBinder.Bind(IntCmdCodePanoramaVideo.LOADED).To<IntCmdLoadCompletePanoramaVideo>();

            #endregion

            #region Stereo Panorama Video Item

            mediationBinder.Bind<ViewStereoPanoramaVideoPlayer>().To<MediatorStereoPanoramaVideoPlayer>();

            commandBinder.Bind(EventCode.StereoPanoramaVideoCreated).To<LogicCommand>();
            commandBinder.Bind(EventCode.StereoPanoramaVideoHidden).To<LogicCommand>();
            commandBinder.Bind(EventCode.StereoPanoramaVideoShown).To<LogicCommand>();
            commandBinder.Bind(EventCode.StereoPanoramaVideoPaused).To<LogicCommand>();
            commandBinder.Bind(EventCode.StereoPanoramaVideoPlayed).To<LogicCommand>();
            commandBinder.Bind(EventCode.StereoPanoramaVideoReplayed).To<LogicCommand>();
            commandBinder.Bind(EventCode.StereoPanoramaVideoSought).To<LogicCommand>();
            commandBinder.Bind(EventCode.StereoPanoramaVideoStopped).To<LogicCommand>();
            commandBinder.Bind(EventCode.StereoPanoramaVideoStatusSynced).To<LogicCommand>();

            commandBinder.Bind(SubCode.HideStereoPanoramaVideo).To<OpCmdHideStereoPanoramaVideo>()
                .To<OpCmdCloseAllMultimedias>();
            commandBinder.Bind(SubCode.PauseStereoPanoramaVideo).To<OpCmdPauseStereoPanoramaVideo>();
            commandBinder.Bind(SubCode.ReplayStereoPanoramaVideo).To<OpCmdResetStereoPanoramaVideo>();
            commandBinder.Bind(SubCode.SeekStereoPanoramaVideo).To<OpCmdSeekStereoPanoramaVideo>();
            commandBinder.Bind(SubCode.ShowStereoPanoramaVideo).To<OpCmdShowStereoPanoramaVideo>();
            commandBinder.Bind(SubCode.PlayStereoPanoramaVideo).To<OpCmdStartStereoPanoramaVideo>();
            commandBinder.Bind(SubCode.StopStereoPanoramaVideo).To<OpCmdStopStereoPanoramaVideo>();

            commandBinder.Bind(ServerCmdCode.STEREO_PANORAMA_VIDEO_CREATED).To<OpCmdCreatedStereoPanoramaVideo>();
            commandBinder.Bind(ServerCmdCode.STEREO_PANORAMA_VIDEO_HIDDEN).To<OpCmdHiddenStereoPanoramaVideo>();
            commandBinder.Bind(ServerCmdCode.STEREO_PANORAMA_VIDEO_PAUSED).To<OpCmdPausedStereoPanoramaVideo>();
            commandBinder.Bind(ServerCmdCode.STEREO_PANORAMA_VIDEO_PLAYED).To<OpCmdStartedStereoPanoramaVideo>();
            commandBinder.Bind(ServerCmdCode.STEREO_PANORAMA_VIDEO_REPLAYED).To<OpCmdRestartedStereoPanoramaVideo>();
            commandBinder.Bind(ServerCmdCode.STEREO_PANORAMA_VIDEO_SHOWN).To<OpCmdShownStereoPanoramaVideo>();
            commandBinder.Bind(ServerCmdCode.STEREO_PANORAMA_VIDEO_SOUGHT).To<OpCmdSoughtStereoPanoramaVideo>();
            commandBinder.Bind(ServerCmdCode.STEREO_PANORAMA_VIDEO_STOPPED).To<OpCmdStoppedStereoPanoramaVideo>();
            commandBinder.Bind(ServerCmdCode.SYNC_STEREO_PANORAMA_VIDEO_STATUS).To<OpCmdStereoPanoramaVideoSynced>();

            commandBinder.Bind(IntCmdCodeStereoPanoramaVideo.CREATED).To<IntCmdCreateItemItemManager>();

            commandBinder.Bind(IntCmdCodeStereoPanoramaVideo.HIDDEN).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeStereoPanoramaVideo.PAUSED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeStereoPanoramaVideo.RESTARTED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeStereoPanoramaVideo.SHOWN).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeStereoPanoramaVideo.SOUGHT).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeStereoPanoramaVideo.STARTED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeStereoPanoramaVideo.STOPPED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeStereoPanoramaVideo.STATUS_SYNCED).To<IntCmdMessageRelayItemManager>();

            commandBinder.Bind(IntCmdCodeStereoPanoramaVideo.LOADED).To<IntCmdLoadCompleteStereoPanoramaVideo>();

            commandBinder.Bind(IntCmdCodeStereoPanoramaVideo.RETURN_CAMERA_ROT).To<IntCmdReturnCameraRotAvatarItem>();
            commandBinder.Bind(IntCmdCodeStereoPanoramaVideo.ROTATE_CAMERA).To<IntCmdRotateCameraAvatarItem>();

            #endregion

            #region Panorama Image Item

            mediationBinder.Bind<ViewPanoramaImagePlayer>().To<MediatorPanoramaImagePlayer>();

            commandBinder.Bind(EventCode.PanoramaCreated).To<LogicCommand>();
            commandBinder.Bind(EventCode.PanoramaHidden).To<LogicCommand>();
            commandBinder.Bind(EventCode.PanoramaShown).To<LogicCommand>();
            commandBinder.Bind(EventCode.PanoramaStatusSynced).To<LogicCommand>();

            commandBinder.Bind(SubCode.HidePanorama).To<OpCmdHidePanoramaImage>().To<OpCmdCloseAllMultimedias>();
            commandBinder.Bind(SubCode.ShowPanorama).To<OpCmdShowPanoramaImage>();

            commandBinder.Bind(ServerCmdCode.PANORAMA_CREATED).To<OpCmdCreatedPanoramaImage>();
            commandBinder.Bind(ServerCmdCode.PANORAMA_HIDDEN).To<OpCmdHiddenPanoramaImage>();
            commandBinder.Bind(ServerCmdCode.PANORAMA_SHOWN).To<OpCmdShownPanoramaImage>();
            commandBinder.Bind(ServerCmdCode.SYNC_PANORAMA_STATUS).To<OpCmdPanoramaImageSynced>();

            commandBinder.Bind(IntCmdCodePanoramaImage.CREATED).To<IntCmdCreateItemItemManager>();
            commandBinder.Bind(IntCmdCodePanoramaImage.STATUS_SYNCED).To<IntCmdMessageRelayItemManager>();

            commandBinder.Bind(IntCmdCodePanoramaImage.HIDDEN).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodePanoramaImage.SHOWN).To<IntCmdMessageRelayItemManager>();

            commandBinder.Bind(IntCmdCodePanoramaImage.RETURN_CAMERA_ROT).To<IntCmdReturnCameraRotAvatarItem>();
            commandBinder.Bind(IntCmdCodePanoramaImage.ROTATE_CAMERA).To<IntCmdRotateCameraAvatarItem>();
            commandBinder.Bind(IntCmdCodePanoramaImage.LOADED).To<IntCmdLoadCompletePanoramaImage>();

            #endregion

            //commandBinder.Bind(SubCode.CloseAllMultimedias).To<OpCmdCloseAllMultimedias>();
            commandBinder.Bind(ECommonViewEvent.CLOSE_MEDIA).To<IntCmdUIControlItemManager>();
            commandBinder.Bind(ServerCmdCode.ALL_MULTIMEDIA_CLOSED).To<OpCmdAllMultimediaClosed>();
            commandBinder.Bind(IntCmdCodeMultimedia.CLOSE_ALL_MULTIMEDIA).To<IntCmdMessageRelayMultimedia>();

            #region Line Notation Item

            mediationBinder.Bind<ViewItemLineNotation>().To<MediatorItemLineNotation>();

            commandBinder.Bind(EventCode.LineNotationCreated).To<LogicCommand>();
            commandBinder.Bind(EventCode.LineNotationTouchPointsAdded).To<LogicCommand>();
            commandBinder.Bind(EventCode.LineNotationClear).To<LogicCommand>();
            commandBinder.Bind(EventCode.LineNotationColorUpdated).To<LogicCommand>();
            commandBinder.Bind(EventCode.LineNotationHide).To<LogicCommand>();
            commandBinder.Bind(EventCode.LineNotationShow).To<LogicCommand>();
            commandBinder.Bind(EventCode.LineNotationStarted).To<LogicCommand>();
            commandBinder.Bind(EventCode.LineNotationStopped).To<LogicCommand>();
            commandBinder.Bind(EventCode.LineNotationThicknessUpdated).To<LogicCommand>();
            commandBinder.Bind(EventCode.LineNotationLineRemoved).To<LogicCommand>();
            commandBinder.Bind(EventCode.LineNotationStatusSynced).To<LogicCommand>();

            commandBinder.Bind(SubCode.UpdateLineNotationColor).To<OpCmdChangeColorLineNotation>();
            commandBinder.Bind(SubCode.UpdateLineNotationThickness).To<OpCmdChangeThicknessLineNotation>();
            commandBinder.Bind(SubCode.CleanLineNotation).To<OpCmdClearLineNotation>();
            commandBinder.Bind(SubCode.HideLineNotation).To<OpCmdHideLineNotation>();
            commandBinder.Bind(SubCode.ShowLineNotation).To<OpCmdShowLineNotation>();
            commandBinder.Bind(SubCode.StartLineNotation).To<OpCmdStartLineNotation>();
            commandBinder.Bind(SubCode.StopLineNotation).To<OpCmdStopLineNotation>();
            commandBinder.Bind(SubCode.UndoLineNotation).To<OpCmdUndoLineNotation>();
            commandBinder.Bind(SubCode.AddTouchPointsToLineNotation).To<OpCmdAddPointsLineNotation>();

            commandBinder.Bind(ServerCmdCode.NOTATION_ADD_TOUCH_POINTS).To<OpCmdPointsAddedLineNotation>();
            commandBinder.Bind(ServerCmdCode.NOTATION_CLEARED).To<OpCmdClearedLineNotation>();
            commandBinder.Bind(ServerCmdCode.NOTATION_COLOR_UPDATED).To<OpCmdColorChangedLineNotation>();
            commandBinder.Bind(ServerCmdCode.NOTATION_CREATED).To<OpCmdCreatedLineNotation>();
            commandBinder.Bind(ServerCmdCode.NOTATION_HIDDEN).To<OpCmdHiddenLineNotation>();
            commandBinder.Bind(ServerCmdCode.NOTATION_LINE_REMOVED).To<OpCmdUndunLineNotation>();
            commandBinder.Bind(ServerCmdCode.NOTATION_SHOWN).To<OpCmdShownLineNotation>();
            commandBinder.Bind(ServerCmdCode.NOTATION_STARTED).To<OpCmdStartedLineNotation>();
            commandBinder.Bind(ServerCmdCode.NOTATION_STOPPED).To<OpCmdStoppedLineNotation>();
            commandBinder.Bind(ServerCmdCode.NOTATION_THICKNESS_UPDATED).To<OpCmdThicknessChangedLineNotation>();
            commandBinder.Bind(ServerCmdCode.SYNC_NOTATION_STATUS).To<OpCmdSyncLineNotationStatus>();

            commandBinder.Bind(IntCmdCodeLineNotation.CREATED).To<IntCmdCreateItemItemManager>();

            commandBinder.Bind(IntCmdCodeLineNotation.CLEARED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeLineNotation.COLOR_CHANGED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeLineNotation.HIDDEN).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeLineNotation.POINTS_ADDED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeLineNotation.SHOWN).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeLineNotation.STARTED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeLineNotation.STOPPED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeLineNotation.THICKNESS_CHANGED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeLineNotation.UNDO_LINE).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeLineNotation.SYNC_STATUS).To<IntCmdMessageRelayItemManager>();

            #endregion

            #region UI Item

            mediationBinder.Bind<ViewUIItem>().To<MediatorUIItem>();

            commandBinder.Bind(EventCode.UICreated).To<LogicCommand>();

            commandBinder.Bind(ServerCmdCode.UI_CREATED).To<OpCmdCreatedUIItem>();

            commandBinder.Bind(IntCmdCodeUIItem.CREATED).To<IntCmdCreateItemItemManager>();
            commandBinder.Bind(IntCmdCodeUIItem.COMMUNICATE_ITEM_MANAGER).To<IntCmdMessageUIRelayItemManager>();

            #endregion

            #region World

            commandBinder.Bind(OpCmdCodeWorld.ENTER_WORLD).To<OpCmdEnterWorld>();

            commandBinder.Bind(EventCode.GetWaitingWorld).To<IntCmdSetWaitingWorld>();
            commandBinder.Bind(EventCode.ItemAllDestroyed).To<LogicCommand>();

            #endregion

            #region Hotspot

            mediationBinder.Bind<ViewHotspot>().To<MediatorHotspot>();

            commandBinder.Bind(SubCode.DisableHotspot).To<OpCmdDisableHotspot>();
            commandBinder.Bind(SubCode.EnableHotspot).To<OpCmdEnableHotspot>();
            commandBinder.Bind(SubCode.TriggerHotspot).To<OpCmdHotspotTriggered>();
            commandBinder.Bind(SubCode.EnableHotspotStatus).To<OpCmdEnableHotspotStatus>();

            commandBinder.Bind(EventCode.EnableHotspot).To<LogicCommand>();
            commandBinder.Bind(EventCode.DisableHotspot).To<LogicCommand>();
            commandBinder.Bind(EventCode.HotspotTeleport).To<LogicCommand>();
            commandBinder.Bind(EventCode.EnableHotspotStatus).To<LogicCommand>();

            commandBinder.Bind(ServerCmdCode.HOTSPOT_DISABLED).To<OpCmdHotspotDisabled>();
            commandBinder.Bind(ServerCmdCode.HOTSPOT_ENABLED).To<OpCmdHotspotEnabled>();
            commandBinder.Bind(ServerCmdCode.HOTSPOT_TELEPORTED).To<OpCmdHotspotTeleported>();
            commandBinder.Bind(ServerCmdCode.HOTSPOT_STATUS_ENABLED).To<OpCmdHotspotStatusEnabled>();

            commandBinder.Bind(IntCmdCodeHotspot.HOTSPOT_DISABLED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeHotspot.HOTSPOT_ENABLED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeHotspot.HOTSPOT_STATUS_ENABLED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeHotspot.HOTSPOT_TELEPORTED).To<IntCmdMessageRelayItemManager>();
            commandBinder.Bind(IntCmdCodeHotspot.SHOW_TELEPORT_MEDIA).To<IntCmdHotspotTeleportItemManager>();

            #endregion

            #region Media Dropdown

            //commandBinder.Bind(EventCode.Teleported).To<LogicCommand>();

            #endregion

            #region User

            injectionBinder.Bind<IUserModel>().To<UserModel>().ToSingleton().CrossContext();

            mediationBinder.Bind<ViewUserManager>().To<MediatorUserManager>();

            commandBinder.Bind(EventCode.Disconnected).To<LogicCommand>();
            commandBinder.Bind(EventCode.LoginFailed).To<LogicCommand>();
            commandBinder.Bind(EventCode.ClientReconnected).To<LogicCommand>();

            commandBinder.Bind(ServerCmdCode.DISCONNECTED).To<OpCmdPlayerDisconnected>();
            commandBinder.Bind(ServerCmdCode.LOGIN_FAILED).To<OpCmdLoginFailed>();
            commandBinder.Bind(ServerCmdCode.CLIENT_RECONNECTED).To<OpCmdClientReconnected>();
            commandBinder.Bind(OpCmdCodeUser.GET_WAITING_WORLD).To<OpCmdGetWaitingWorld>();


            commandBinder.Bind(SubCode.Reconnect).To<OpCmdRequestReconnect>();

            #endregion
        }
    }
}