using System;
using System.Collections.Generic;
using System.Linq;

namespace PulseIQ.Local.ClientCommon.Extensions
{
    public static class CollectionExtensions
    {
        /// <summary>
        /// gives flattened hierarchy to search against,
        /// https://stackoverflow.com/questions/20974248/recursive-hierarchy-recursive-query-using-linq
        /// </summary>
        /// <param name="items"></param>
        /// <param name="childSelector"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<T> Traverse<T>(this IEnumerable<T> items,
            Func<T, IEnumerable<T>> childSelector)
        {
            var stack = new Stack<T>(items);
            while (stack.Any())
            {
                var next = stack.Pop();
                yield return next;
                foreach (var child in childSelector(next))
                    stack.Push(child);
            }
        }
    }
}