﻿using System;

using PulseIQ.Local.ClientCommon.Components.Common;

using strange.extensions.dispatcher.eventdispatcher.api;

using UnityEngine;
using UnityEngine.Events;

namespace PulseIQ.Local.ClientCommon.Extensions
{
    public static class IQExtensions
    {
        #region RectTransform

        public static void SetWidth(this RectTransform rectTrans, float width)
        {
            Vector2 delta = rectTrans.sizeDelta;
            delta.x = width;
            rectTrans.sizeDelta = delta;
        }

        public static void SetHeight(this RectTransform rectTrans, float height)
        {
            Vector2 delta = rectTrans.sizeDelta;
            delta.y = height;
            rectTrans.sizeDelta = delta;
        }

        public static void SetWidthHeight(this RectTransform rectTrans, float width, float height)
        {
            Vector2 delta = new Vector2(width, height);
            rectTrans.sizeDelta = delta;
        }

        #endregion

        #region Vectors

        public static Vector3 WithX(this Vector3 vector, float x)
        {
            Vector3 newVector = vector;
            newVector.x = x;
            return newVector;
        }

        public static Vector3 WithY(this Vector3 vector, float y)
        {
            Vector3 newVector = vector;
            newVector.y = y;
            return newVector;
        }

        public static Vector3 WithZ(this Vector3 vector, float z)
        {
            Vector3 newVector = vector;
            newVector.z = z;
            return newVector;
        }

        public static Vector2 WithX(this Vector2 vector, float x)
        {
            Vector2 newVector = vector;
            newVector.x = x;
            return newVector;
        }

        public static Vector2 WithY(this Vector2 vector, float y)
        {
            Vector2 newVector = vector;
            newVector.y = y;
            return newVector;
        }

        #endregion

        #region Strange

        public static MessageData MessageData(this IEvent payload)
        {
            return (MessageData)payload.data;
        }

        #endregion

        #region Events

        public static void UpdateListeners(this UnityEvent unityEvent, bool value, UnityAction listener)
        {
            if (value)
            {
                unityEvent.AddListener(listener);
            }
            else
            {
                unityEvent.RemoveListener(listener);
            }
        }
        
        public static void UpdateListeners<T>(this UnityEvent<T> unityEvent, bool value, UnityAction<T> listener)
        {
            if (value)
            {
                unityEvent.AddListener(listener);
            }
            else
            {
                unityEvent.RemoveListener(listener);
            }
        }

        #endregion
    }
}