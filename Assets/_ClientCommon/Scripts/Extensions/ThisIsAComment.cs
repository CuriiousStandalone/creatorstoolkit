﻿using UnityEngine;
/// <summary>
/// Attach this script to any gameObject for which you want to put a note.
/// from https://www.codeproject.com/Tips/1208852/How-to-Add-Comments-Notes-to-a-GameObject-in-Unity
/// </summary>
public class ThisIsAComment : MonoBehaviour
{
    [TextArea]
    public string Notes = "Comment Here."; // Do not place your note/comment here. 
    // Enter your note in the Unity Editor.
    /// <summary>
    /// Might Use in the future to mark items that need more work or new assets etc.
    /// currently is useless dead data but can be searched for using and editor tool later.
    /// </summary>
    public bool TODO = false;
}