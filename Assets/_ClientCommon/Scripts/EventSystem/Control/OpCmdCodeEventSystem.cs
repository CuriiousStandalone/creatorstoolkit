﻿namespace PulseIQ.Local.ClientCommon.EventSystem
{
	public enum OpCmdCodeEventSystem : byte
	{
		EVT_RECEIVED,
		LOAD_ASSOCIATIONS,
		ADD_ASSOCIATION,
		UPDATE_ASSOCIAION,
		REMOVE_ASSOCIATION,
	}
}