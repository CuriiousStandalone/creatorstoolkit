﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using strange.extensions.command.impl;

using PulseIQ.Local.Common;
using PulseIQ.Local.Common.Model.EventSystem;
using PulseIQ.Local.Client.Unity3DLib.Model;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.World;

namespace PulseIQ.Local.ClientCommon.EventSystem
{
	public class OpCmdLoadAssociations : EventCommand 
	{
		[Inject]
		public IAssociationDataset associationDataset{ get; set; }

		public override void Execute()
		{
			string worldID = (string)evt.data;
			WorldDataXML enteredWorldXML;
			WorldDataSerializer.ReadDataByWorldId (worldID, out enteredWorldXML);
			WorldData enteredWorldData = new WorldData (enteredWorldXML);

			foreach (AssociationData assData in enteredWorldData.ListAssociation) 
			{
				associationDataset.AddAssociation (assData);
			}
		}
	}
}
