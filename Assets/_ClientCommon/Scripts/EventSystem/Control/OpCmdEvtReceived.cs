﻿using UnityEngine;
using System.Collections.Generic;

using strange.extensions.command.impl;

using PulseIQ.Local.Common;
using PulseIQ.Local.Common.CustomType;
using PulseIQ.Local.Client.Unity3DLib.Model;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.EventSystem;
using PulseIQ.Local.Common.CustomType;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.ClientCommon.TutorialItem;
using PulseIQ.Local.ClientCommon.TimerItem;
using PulseIQ.Local.ClientCommon.SlotItem;
using PulseIQ.Local.ClientCommon.BaseItem;

namespace PulseIQ.Local.ClientCommon.EventSystem
{
	public class OpCmdEvtReceived : EventCommand
	{
		[Inject]
		public IAssociationDataset associationDataset{ get; set; }

		[Inject]
		public IItemDataset itemDataset { get; set; }

		public override void Execute()
		{
			//List<AssociationData> triggeredAssocationList = associationDataset.GetAssociationByEvtCode ((EventCode)evt.type);
			//foreach (AssociationData assData in triggeredAssocationList) 
			//{
			//	MessageData data = (MessageData)evt.data;
			//	string itemId;
			//	try
			//	{
			//		itemId = (string)data.Parameters [(byte)CommonParameterCode.ITEMID];
			//		ViewItemBase eventTargetItem = itemDataset.FindItemByItemId(itemId);
			//		if (null == eventTargetItem
			//			|| assData.TriggerEventTargetItemIndex != eventTargetItem.itemIndex)
			//		{
			//			continue;
			//		}
			//	}
			//	catch
			//	{
			//		continue;
			//	}

			//	MessageData actionData = new MessageData();
			//	switch (assData.ActionSubCode) 
			//	{
			//	// TODO: find item by item index, get data from found item, then dispatch event with those items.
			//	case SubCode.CreateItem:
					
			//		break;
			//	case SubCode.TryObtainItem:
			//		{
			//			// Target ItemID
			//			string targetItemId;
			//			ViewItemBase itemIdDataProvider = itemDataset.FindItemByItemIndex (assData.ActionTargetItemIndex);
			//			if (null != itemIdDataProvider) 
			//			{
			//				targetItemId = itemIdDataProvider.itemID;
			//			} 
			//			else 
			//			{
			//				Debug.LogError ("Must provide the itemId to excute the action!");
			//				targetItemId = " ";
			//			}

			//			actionData.Parameters.Add ((byte)CommonParameterCode.ITEMID, targetItemId);
			//			break;
			//		}
			//	case SubCode.TryPlaceInteractiveItem:
			//		{
			//			// Target ItemID
			//			string targetItemId;
			//			ViewItemBase itemIdDataProvider = itemDataset.FindItemByItemIndex (assData.ActionTargetItemIndex);
			//			if (null != itemIdDataProvider) 
			//			{
			//				targetItemId = itemIdDataProvider.itemID;
			//			} 
			//			else 
			//			{
			//				Debug.LogError ("Must provide the itemId to excute the action!");
			//				targetItemId = " ";
			//			}

			//			int slotId;
			//			AssociationParameterData slotIdPara;
			//			assData.DictActionCmdParameter.TryGetValue (ParameterCode.SlotId, out slotIdPara);
			//			ViewSlotItem slotIdDataProvider = (ViewSlotItem)itemDataset.FindItemByItemIndex (assData.ActionTargetItemIndex);
			//			if (null != slotIdDataProvider) 
			//			{
			//				slotId = slotIdDataProvider.slotID;
			//			} 
			//			else 
			//			{
			//				Debug.LogError ("Must provide the slot id to excute the action!");
			//				slotId = -1;
			//			}

			//			actionData.Parameters.Add ((byte)CommonParameterCode.ITEMID, targetItemId);
			//			actionData.Parameters.Add ((byte)CommonParameterCode.SLOTID, slotId);
			//			break;
			//		}
			//	case SubCode.TryAbandonItem:
			//		{
			//			// Target ItemID
			//			string targetItemId;
			//			ViewItemBase itemIdDataProvider = itemDataset.FindItemByItemIndex (assData.ActionTargetItemIndex);
			//			if (null != itemIdDataProvider) 
			//			{
			//				targetItemId = itemIdDataProvider.itemID;
			//			} 
			//			else 
			//			{
			//				Debug.LogError ("Must provide the itemId to excute the action!");
			//				targetItemId = " ";
			//			}

			//			actionData.Parameters.Add ((byte)CommonParameterCode.ITEMID, targetItemId);
			//			break;
			//		}
			//	case SubCode.ShowPanorama:
			//		{
			//			string targetItemId;
			//			ViewItemBase itemIdDataProvider = itemDataset.FindItemByItemIndex (assData.ActionTargetItemIndex);
			//			if (null != itemIdDataProvider) 
			//			{
			//				targetItemId = itemIdDataProvider.itemID;
			//			} 
			//			else 
			//			{
			//				Debug.LogError ("Must provide the itemId to excute the action!");
			//				targetItemId = " ";
			//			}

			//			actionData.Parameters.Add ((byte)CommonParameterCode.ITEMID, targetItemId);
			//			break;
			//		}
			//	case SubCode.MoveItem:
			//		{
			//			DataItemPose dataItemPose = new DataItemPose ();

			//			// PositionX
			//			AssociationParameterData itemPositionXPara;
			//			assData.DictActionCmdParameter.TryGetValue (ParameterCode.ItemPositionX, out itemPositionXPara);
			//			ViewItemBase positionXDataProvider = itemDataset.FindItemByItemIndex (itemPositionXPara.ItemIndex);
			//			if (null != positionXDataProvider) {
			//				dataItemPose.Position.X = positionXDataProvider.transform.position.x;
			//			} else {
			//				dataItemPose.Position.X = (float)itemPositionXPara.Value;
			//			}

			//			// PositionY
			//			AssociationParameterData itemPositionYPara;
			//			assData.DictActionCmdParameter.TryGetValue (ParameterCode.ItemPositionY, out itemPositionYPara);
			//			ViewItemBase positionYDataProvider = itemDataset.FindItemByItemIndex (itemPositionYPara.ItemIndex);
			//			if (null != positionYDataProvider) {
			//				dataItemPose.Position.Y = positionYDataProvider.transform.position.y;
			//			} else {
			//				dataItemPose.Position.Y = (float)itemPositionYPara.Value;
			//			}

			//			// PositionZ
			//			AssociationParameterData itemPositionZPara;
			//			assData.DictActionCmdParameter.TryGetValue (ParameterCode.ItemPositionZ, out itemPositionZPara);
			//			ViewItemBase positionZDataProvider = itemDataset.FindItemByItemIndex (itemPositionZPara.ItemIndex);
			//			if (null != positionZDataProvider) {
			//				dataItemPose.Position.Z = positionZDataProvider.transform.position.z;
			//			} else {
			//				dataItemPose.Position.Z = (float)itemPositionZPara.Value;
			//			}

			//			// RotationX
			//			AssociationParameterData itemRotationXPara;
			//			assData.DictActionCmdParameter.TryGetValue (ParameterCode.ItemRotationX, out itemRotationXPara);
			//			ViewItemBase rotationXDataProvider = itemDataset.FindItemByItemIndex (itemRotationXPara.ItemIndex);
			//			if (null != rotationXDataProvider) {
			//				dataItemPose.Rotation.X = rotationXDataProvider.transform.rotation.x;
			//			} else {
			//				dataItemPose.Rotation.X = (float)itemRotationXPara.Value;
			//			}

			//			// RotationY
			//			AssociationParameterData itemRotationYPara;
			//			assData.DictActionCmdParameter.TryGetValue (ParameterCode.ItemRotationY, out itemRotationYPara);
			//			ViewItemBase rotationYDataProvider = itemDataset.FindItemByItemIndex (itemRotationYPara.ItemIndex);
			//			if (null != rotationYDataProvider) {
			//				dataItemPose.Rotation.Y = rotationYDataProvider.transform.rotation.y;
			//			} else {
			//				dataItemPose.Rotation.Y = (float)itemRotationYPara.Value;
			//			}
				
			//			// RotationZ
			//			AssociationParameterData itemRotationZPara;
			//			assData.DictActionCmdParameter.TryGetValue (ParameterCode.ItemRotationZ, out itemRotationZPara);
			//			ViewItemBase rotationZDataProvider = itemDataset.FindItemByItemIndex (itemRotationZPara.ItemIndex);
			//			if (null != rotationZDataProvider) {
			//				dataItemPose.Rotation.Z = rotationYDataProvider.transform.rotation.z;
			//			} else {
			//				dataItemPose.Rotation.Z = (float)itemRotationZPara.Value;
			//			}

			//			// RotationW
			//			AssociationParameterData itemRotationWPara;
			//			assData.DictActionCmdParameter.TryGetValue (ParameterCode.ItemRotationW, out itemRotationWPara);
			//			ViewItemBase rotationWDataProvider = itemDataset.FindItemByItemIndex (itemRotationWPara.ItemIndex);
			//			if (null != rotationWDataProvider) {
			//				dataItemPose.Rotation.W = rotationWDataProvider.transform.rotation.w;
			//			} else {
			//				dataItemPose.Rotation.W = (float)itemRotationWPara.Value;
			//			}

			//			// Target ItemID
			//			ViewItemBase itemIdDataProvider = itemDataset.FindItemByItemIndex (assData.ActionTargetItemIndex);
			//			if (null != itemIdDataProvider) {
			//				dataItemPose.ItemId = itemIdDataProvider.itemID;
			//			} else {
			//				Debug.LogError ("Must provide the itemId to excute the action!");
			//			}
						
			//			actionData.Parameters.Add ((byte)CommonParameterCode.DATA_ITEM_POSE, dataItemPose);
			//			break;
			//		}
			//	case SubCode.MoveToPostion:
			//		break;
			//	case SubCode.UpdateMoveTo:
			//		break;
			//	case SubCode.StopUpdate:
			//		break;
			//	case SubCode.DestroyItem:
			//		break;
			//	default:
			//		break;
			//	}

			//	dispatcher.Dispatch(assData.ActionSubCode, actionData);
			//}
		}
	}
}
