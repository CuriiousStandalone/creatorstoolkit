﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using PulseIQ.Local.Common.Model.EventSystem;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace PulseIQ.Local.ClientCommon.EventSystem
{
	public interface IAssociationDataset
	{
		List<AssociationData> GetAssociationByEvtCode(EventCode evtCode);

		void AddAssociation(AssociationData newAssociation);

		bool RemoveAssociation (string associationId);

		bool UpdateAssociation (AssociationData updateAssociation);
	}
}
