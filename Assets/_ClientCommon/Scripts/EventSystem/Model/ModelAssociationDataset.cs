﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.EventSystem;

namespace PulseIQ.Local.ClientCommon.EventSystem
{
	public class ModelAssociationDataset : IAssociationDataset 
	{
		private Dictionary<string, AssociationData> _dictAssociation;

		public Dictionary<string, AssociationData> DictAssociation
		{
			get
			{ 
				return _dictAssociation;
			}
			set
			{
				_dictAssociation = value;
			}
		}

		public List<AssociationData> GetAssociationByEvtCode(EventCode evtCode)
		{
			List<AssociationData> assDataList = new List<AssociationData> ();

			foreach (AssociationData assData in _dictAssociation.Values) 
			{
				if (evtCode == assData.TriggerEventCode) 
				{
					assDataList.Add (assData);
				}
			}

			return assDataList;
		}

		public ModelAssociationDataset()
		{
			_dictAssociation = new Dictionary<string, AssociationData> ();
		}

		public void AddAssociation(AssociationData newAssociation)
		{
			_dictAssociation.Add (newAssociation.AssociationID, newAssociation);
		}

		public bool RemoveAssociation (string associationId)
		{
			return _dictAssociation.Remove (associationId);
		}

		public bool UpdateAssociation (AssociationData updateAssociation)
		{
			AssociationData foundData;
			if (_dictAssociation.TryGetValue (updateAssociation.AssociationID, out foundData)) 
			{
				foundData.TriggerEventCode = updateAssociation.TriggerEventCode;
				foundData.AssociationID = updateAssociation.AssociationID;
				foundData.ActionSubCode = updateAssociation.ActionSubCode;

				foundData.DictActionCmdParameter.Clear ();
				foreach (ParameterCode paraCode in updateAssociation.DictActionCmdParameter.Keys) 
				{
					AssociationParameterData assParaData;
					if (!updateAssociation.DictActionCmdParameter.TryGetValue (paraCode, out assParaData)) 
					{
						Debug.LogError ("Null Parameter " + paraCode.ToString());
					}

					foundData.DictActionCmdParameter.Add (paraCode, assParaData);
				}

				return true;
			} 
			else 
			{
				return false;
			}
		}
	}
}
