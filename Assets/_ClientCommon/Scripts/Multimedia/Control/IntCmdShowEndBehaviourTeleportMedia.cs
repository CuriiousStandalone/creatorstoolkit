﻿using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.ItemManager;

using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.Multimedia
{
    public class IntCmdShowEndBehaviourTeleportMedia : EventCommand
    {
        [Inject]
        public IItemDataset model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            data.Parameters.Add((byte)CommonParameterCode.MAIN_CAMERA_POSITION, model.mainCamera.transform.position);
            data.Parameters.Add((byte)CommonParameterCode.MAIN_CAMERA_ROTATION, model.mainCamera.transform.eulerAngles);

            dispatcher.Dispatch(IntCmdCodeItemManager.RELAY_MESSAGE_ITEM_INDEX, data);
        }
    }
}
