﻿using PulseIQ.Local.Common;
using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.DllBridge;

using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.Multimedia
{
    public class OpCmdTriggerEndBehaviour : EventCommand
    {
        public override void Execute()
        {
            MessageData msgData = (MessageData)evt.data;

            string itemId = (string)msgData[(byte)CommonParameterCode.ITEMID];

            OperationsMultimedia.TriggerEndBehavior(ClientBridge.Instance, ClientBridge.Instance.CurUserID, itemId);
        }
    }
}
