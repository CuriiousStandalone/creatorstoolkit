﻿using strange.extensions.command.impl;

using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.ItemManager;

namespace PulseIQ.Local.ClientCommon.Multimedia
{
    public class OpCmdEndBehaviourTriggered : EventCommand
    {
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;
            data.Parameters.Add((byte)CommonParameterCode.OPERATION_CODE, evt.type);

            dispatcher.Dispatch(IntCmdCodeMultimedia.END_BEHAVIOUR_TELEPORTED, data);
        }
    }
}
