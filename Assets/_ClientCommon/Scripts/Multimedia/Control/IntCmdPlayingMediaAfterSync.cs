﻿using PulseIQ.Local.ClientCommon.Components.Common;

using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.Multimedia
{
    public class IntCmdPlayingMediaAfterSync : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(ECommonCrossContextEvents.SHOWING_MEDIA_AFTER_SYNC, evt.data);
        }
    }
}
