﻿using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Client.Unity3DLib.Model;
using PulseIQ.Local.Common.CustomType;

namespace PulseIQ.Local.ClientCommon
{
    public class MediatorTutorialCamera : EventMediator
    {
        [Inject]
        public IViewMainCamera view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {

        }

        private void OnMessageReceived(System.Object data)
        {
            MessageData tutorialData = (MessageData)data;
            ServerCmdCode code = (ServerCmdCode)tutorialData[(byte)CommonParameterCode.OPERATION_CODE];

            switch (code)
            {
                case ServerCmdCode.TUTORIAL_ACTIVATED:
                    {
                        CTVector3 position = (CTVector3)tutorialData.Parameters[(byte)CommonParameterCode.OBSERVER_LOCAL_POSITION];
                        CTQuaternion rotation = (CTQuaternion)tutorialData.Parameters[(byte)CommonParameterCode.OBSERVER_LOCAL_ROTATION];

                        view.ActivateCamera(position, rotation);
                        break;
                    }

                case ServerCmdCode.TUTORIAL_DEACTIVATED:
                    {
                        view.DeactivateCamera();
                        break;
                    }
            }
        }
    }
}