﻿using strange.extensions.command.impl;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

using UnityEngine;

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public class IntCmdRaycasterDataItemManager : EventCommand
    {
        public override void Execute()
        { 
            dispatcher.Dispatch(IntCmdCodeItemManager.RAYCAST_DATA, evt.data);
        }
    }
}