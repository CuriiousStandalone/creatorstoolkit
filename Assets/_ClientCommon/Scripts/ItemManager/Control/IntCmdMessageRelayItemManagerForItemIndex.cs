﻿using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public class IntCmdMessageRelayItemManagerForItemIndex : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(IntCmdCodeItemManager.RELAY_MESSAGE_ITEM_INDEX, evt.data);
        }
    }
}
