﻿using strange.extensions.command.impl;
using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.DllBridge;

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public class IntCmdWorldItemStoredItemManager : EventCommand
    {
        public override void Execute()
        {
            OperationsWorld.CreateWorldItems(ClientBridge.Instance, ClientBridge.Instance.CurWorldData.Id);
        }
    }
}