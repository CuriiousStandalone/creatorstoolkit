﻿using strange.extensions.command.impl;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public class OpCmdAllMultimediaClosed : EventCommand
    {
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            if(null == data)
            {
                data = new MessageData();
            }

            data.Parameters.Add((byte)CommonParameterCode.OPERATION_CODE, evt.type);

            dispatcher.Dispatch(IntCmdCodeMultimedia.CLOSE_ALL_MULTIMEDIA, data);
        }
    }
}
