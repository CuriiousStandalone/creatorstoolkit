﻿using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;
using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public class IntCmdHotspotTeleportItemManager : EventCommand
    {
        [Inject]
        public IItemDataset model { get; set; }

        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            int itemIndex = (int)data[(byte)CommonParameterCode.ITEM_INDEX];

            foreach (var item in model.itemList)
            {
                if(item.itemIndex == itemIndex)
                {
                    switch (item.itemType)
                    {
                        case ItemTypeCode.PANORAMA:
                            {
                                data.Parameters.Add((byte)CommonParameterCode.OPERATION_CODE, ServerCmdCode.PANORAMA_SHOWN);

                                break;
                            }
                        case ItemTypeCode.PANORAMA_VIDEO:
                            {
                                data.Parameters.Add((byte)CommonParameterCode.OPERATION_CODE, ServerCmdCode.PANORAMA_VIDEO_SHOWN);
                                break;
                            }
                        case ItemTypeCode.PLANAR_VIDEO:
                            {
                                data.Parameters.Add((byte)CommonParameterCode.OPERATION_CODE, ServerCmdCode.PLANAR_VIDEO_SHOWN);
                                break;
                            }
                        case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                            {
                                data.Parameters.Add((byte)CommonParameterCode.OPERATION_CODE, ServerCmdCode.STEREO_PANORAMA_VIDEO_SHOWN);
                                break;
                            }
                    }

                    MessageData uiData = new MessageData();
                    uiData.Parameters.Add((byte)CommonParameterCode.UI_EVENT_TYPE, ECommonViewEvent.HOTSPOT_TELEPORT);
                    uiData.Parameters.Add((byte)CommonParameterCode.ITEMID, item.itemID);
                    dispatcher.Dispatch(IntCmdCodeItemManager.RELAY_MESSAGE_TO_UI, uiData);

                    break;
                }
            }

            data.Parameters.Add((byte)CommonParameterCode.MAIN_CAMERA_POSITION, model.mainCamera.transform.position);
            data.Parameters.Add((byte)CommonParameterCode.MAIN_CAMERA_ROTATION, model.mainCamera.transform.eulerAngles);

            dispatcher.Dispatch(IntCmdCodeItemManager.RELAY_MESSAGE_ITEM_INDEX, data);
        }
    }
}
