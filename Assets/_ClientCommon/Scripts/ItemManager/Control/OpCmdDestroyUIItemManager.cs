﻿using strange.extensions.command.impl;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public class OpCmdDestroyUIItemManager : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(IntCmdCodeItemManager.DESTROY_UI, evt.data);
        }

    }
}