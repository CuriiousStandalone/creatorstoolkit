﻿using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using strange.extensions.command.impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public class IntCmdWorldItemsDestroyedItemManager : EventCommand
    {
        public override void Execute()
        {
            OperationsWorld.WorldItemDestroyCompleted(ClientBridge.Instance, ClientBridge.Instance.CurWorldData.Id);
        }
    }
}
