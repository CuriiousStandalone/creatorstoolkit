﻿using strange.extensions.command.impl;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public class IntCmdUIControlItemManager : EventCommand
    {
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            if (data.Parameters.ContainsKey((byte)CommonParameterCode.UI_EVENT_TYPE))
            {
                data.Parameters.Remove((byte)CommonParameterCode.UI_EVENT_TYPE);
            }

            data.Parameters.Add((byte)CommonParameterCode.UI_EVENT_TYPE, evt.type);

            dispatcher.Dispatch(IntCmdCodeItemManager.MEDIA_UI_CONTROL_DATA, data);
        }
    }
}
