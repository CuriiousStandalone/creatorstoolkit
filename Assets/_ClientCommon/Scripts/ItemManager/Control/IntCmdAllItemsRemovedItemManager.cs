﻿using strange.extensions.command.impl;

using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public class IntCmdAllItemsRemovedItemManager : EventCommand
    {
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            string worldID = (string)data[(byte)CommonParameterCode.TEAMID];

            OperationsWorld.EnterWorld(ClientBridge.Instance, ClientBridge.Instance.CurWorldData.Id);
        }
    }
}