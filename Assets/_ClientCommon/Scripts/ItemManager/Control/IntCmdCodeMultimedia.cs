﻿namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public enum IntCmdCodeMultimedia
    {
        CLOSE_ALL_MULTIMEDIA,
        RELAY_MESSAGE_MULTIMEDIA,
        END_BEHAVIOUR_TELEPORTED,
        SHOW_END_BEHAVIOUR_TELEPORT_MEDIA,
        PLAYING_MEDIA_AFTER_SYNC,
    }
}
