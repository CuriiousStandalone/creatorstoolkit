﻿using strange.extensions.command.impl;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.World;
using PulseIQ.Local.ClientCommon.SlotItem;
using PulseIQ.Local.ClientCommon.BaseItem;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public class IntCmdCheckSlotGroupItemManager : EventCommand
    {
        [Inject]
        public iWorldXmlDataset model { get; set; }

        [Inject]
        public IItemDataset itemModel { get; set; }

        public override void Execute()
        {
        //    if(model.worldXmlData.ListSlotGroupItemIndices.Count <= 0)
        //    {
        //        return;
        //    }

        //    foreach (int index in model.worldXmlData.ListSlotGroupItemIndices)
        //    {
        //        ViewItemBase item = itemModel.FindItemByItemIndex(index);

        //        if (item != null && item.itemType == ItemTypeCode.SLOT)
        //        {
        //            if (null != item && ((ViewSlotItem)item).GetIfClosed())
        //            {
        //                continue;
        //            }
        //            else
        //            {

        //                ViewItemBase itemBase =  itemModel.FindItemByItemIndex(model.worldXmlData.SlotGroupTimerItemIndex);
        //                itemBase.HideView();

        //                MessageData timerData = new MessageData();
        //                timerData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemBase.itemID);
        //                dispatcher.Dispatch(SubCode.StartSyncTimer, timerData);
        //                return;
        //            }
        //        }
        //        else
        //        {
        //            // return if item does not exist
        //            Debug.Log("NULL SLOT ITEM!");
        //            return;
        //        }
        //    }

        //    Debug.Log("ALL SLOT ITEMS SPAWNED");

        //    foreach (int index in model.worldXmlData.ListSlotGroupItemIndices)
        //    {
        //        itemModel.FindItemByItemIndex(index).HideView();
        //    }

        //    itemModel.FindItemByItemIndex(model.worldXmlData.SlotGroupFinishActionItemIndex).ShowView();

        //    foreach (string id in model.interactiveItemList)
        //    {
        //        foreach (ViewItemBase baseItem in itemModel.itemList)
        //        {
        //            if (baseItem.itemID == id)
        //            {
        //                baseItem.HideView();
        //                continue;
        //            }
        //        }
        //    }

        //    itemModel.FindItemByItemIndex(model.worldXmlData.SlotGroupTimerItemIndex).ShowView();
        }
    }
}