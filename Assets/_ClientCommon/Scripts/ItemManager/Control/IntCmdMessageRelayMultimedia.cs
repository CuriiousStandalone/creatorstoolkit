﻿using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public class IntCmdMessageRelayMultimedia : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(IntCmdCodeMultimedia.RELAY_MESSAGE_MULTIMEDIA, evt.data);
        }
    }
}
