﻿using strange.extensions.command.impl;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public class IntCmdTutorialDataItemManager : EventCommand
    {
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            if (data.Parameters.ContainsKey((byte)CommonParameterCode.UI_EVENT_TYPE))
            {
                data.Parameters.Remove((byte)CommonParameterCode.UI_EVENT_TYPE);
            }

            data.Parameters.Add((byte)CommonParameterCode.UI_EVENT_TYPE, evt.type);

            dispatcher.Dispatch(IntCmdCodeItemManager.TUTORIAL_DATA, data);
        }
    }
}