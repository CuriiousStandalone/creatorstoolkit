﻿//using PulseIQ.Local.ClientCommon.AvatarItem;
//using PulseIQ.Local.ClientCommon.BaseItem;
//using PulseIQ.Local.ClientCommon.Hotspot;
//using PulseIQ.Local.ClientCommon.InteractiveItem;
//using PulseIQ.Local.ClientCommon.SlotItem;
//using PulseIQ.Local.Common;
//using PulseIQ.Local.Common.Model.Hotspot;
//using strange.extensions.command.impl;
//using UnityEngine;

//namespace PulseIQ.Local.ClientCommon.Components.Common
//{
//    public class IntCmdOnSelectedItemManager : EventCommand
//    {
//        [Inject]
//        public IItemDataset model { get; set; }

//        Transform hotspotHitObj;

//        public override void Execute()
//        {
//            MessageData data = (MessageData)evt.data;
//            MessageData newDdata = new MessageData();

//            if(null != data[(byte)CommonParameterCode.ITEMID])
//            {
//                if(null != model.selectedItem)
//                {
//                    newDdata.Parameters.Add((byte)CommonParameterCode.ITEMID, model.selectedItem.itemID);
//                    newDdata.Parameters.Add((byte)CommonParameterCode.MAIN_CAMERA_FORWARD_DIR, (Vector3)data[(byte)CommonParameterCode.MAIN_CAMERA_FORWARD_DIR]);

//                    dispatcher.Dispatch(ServerCmdCode.ITEM_THROWN, newDdata);
//                }
//                else
//                {
//                    newDdata.Parameters.Add((byte)CommonParameterCode.ANIMATION_NAME, AvatarAnimations.WAVING);
//                    dispatcher.Dispatch(OpCmdCodeAvatarItem.AVATAR_ANIM_PLAYED, newDdata);
//                }
//            }

//            ItemTypeCode code = (ItemTypeCode)data[(byte)CommonParameterCode.ITEM_TYPE];
//            string itemID = (string)data[(byte)CommonParameterCode.ITEMID];

//            switch (code)
//            {
//                case ItemTypeCode.HOTSPOT:
//                    {
//                        foreach(ViewItemBase item in model.itemList)
//                        {
//                            if(item.itemID ==itemID)
//                            {
//                                string hotspotID = (string)data[(byte)CommonParameterCode.HOTSPOT_ID];

//                                foreach(GameObject obj in item.GetComponent<IMediaBasic>().HotspotItems)
//                                {
//                                    if(hotspotID == obj.GetComponent<ViewHotspot>().ReturnHotspotID())
//                                    {
//                                        if(null != hotspotHitObj && hotspotHitObj.GetComponent<ViewHotspot>().ReturnHotspotID() != hotspotID)
//                                        {
//                                            hotspotHitObj.GetComponent<ViewHotspot>().OnEnableHotspotStatus(EHotspotStatusTypeCode.DEFAULT);

//                                            hotspotHitObj = null;
//                                        }

//                                        ViewHotspot view = hotspotHitObj.transform.GetComponent<ViewHotspot>();

//                                        if (view.TriggerCode == EHotspotTriggerCode.SELECT)
//                                        {
//                                            view.OnHotspotTriggered();

//                                            if (view.TriggerEventCode == EHotspotTriggerEventCode.ENABLE_TRIGGER_STATE)
//                                            {
//                                                hotspotHitObj = obj.transform;
//                                            }
//                                            break;
//                                        }
//                                        else
//                                        {
//                                            if (null != hotspotHitObj)
//                                            {
//                                                hotspotHitObj.GetComponent<ViewHotspot>().OnEnableHotspotStatus(EHotspotStatusTypeCode.DEFAULT);

//                                                hotspotHitObj = null;
//                                                break;
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        }

//                        if (null != hotspotHitObj)
//                        {
//                            hotspotHitObj.GetComponent<ViewHotspot>().OnEnableHotspotStatus(EHotspotStatusTypeCode.DEFAULT);

//                            hotspotHitObj = null;
//                        }

//                        break;
//                    }

//                case ItemTypeCode.SLOT:
//                    {
//                        foreach (ViewItemBase item in model.itemList)
//                        {
//                            if (item.itemID == itemID)
//                            {
//                                // Do need check for interactive item?
//                                if (null != model.selectedItem && null != model.selectedItem.GetComponent<ViewItemInteractive>() /*!isThrowing*/)
//                                {
//                                    newDdata.Parameters.Add((byte)CommonParameterCode.ITEMID, model.selectedItem.GetComponent<ViewItemBase>().itemID);
//                                    newDdata.Parameters.Add((byte)CommonParameterCode.SLOTID, itemID);

//                                    dispatcher.Dispatch(ServerCmdCode.ITEM_THROWN, data);
//                                    break;
//                                }
//                                else if(null != item.GetComponent<ViewSlotItem>().correctIncorrectObj)
//                                {
//                                    item.GetComponent<ViewSlotItem>().correctIncorrectObj.transform.GetComponent<ViewIndicatorState>().OnClick();
//                                }
//                            }
//                        }
//                        break;
//                    }

//                case ItemTypeCode.ITEM:
//                    {

//                        if (null != model.selectedItem /*&& !isThrowing*/)
//                        {
//                            newDdata.Parameters.Add((byte)CommonParameterCode.ITEMID, model.selectedItem.itemID);
//                            newDdata.Parameters.Add((byte)CommonParameterCode.MAIN_CAMERA_FORWARD_DIR, (Vector3)data[(byte)CommonParameterCode.MAIN_CAMERA_FORWARD_DIR]);
//                            dispatcher.Dispatch(ServerCmdCode.ITEM_THROWN, data);
//                        }
//                            break;
//                    }

//                case ItemTypeCode.INTERACTIVE_ITEM:
//                    {
//                        if (null != model.selectedItem)
//                        {
//                            newDdata.Parameters.Add((byte)CommonParameterCode.ITEMID, model.selectedItem.itemID);
//                            newDdata.Parameters.Add((byte)CommonParameterCode.MAIN_CAMERA_FORWARD_DIR, (Vector3)data[(byte)CommonParameterCode.MAIN_CAMERA_FORWARD_DIR]);
//                            dispatcher.Dispatch(ServerCmdCode.ITEM_THROWN, data);
//                        }

//                        else if (null == model.selectedItem)
//                        {
//                            newDdata.Parameters.Clear();
//                            newDdata.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);

//                            dispatcher.Dispatch(SubCode.TryObtainItem, evt.data);
//                        }

//                        break;
//                    }

//                case ItemTypeCode.UI:
//                    {
//                        // Implement once UI items are only single buttons. Right now there can be multiple so will wait to implment logic once design has been confirmed.
//                        break;
//                    }
//            }

//        }
//    }
//}