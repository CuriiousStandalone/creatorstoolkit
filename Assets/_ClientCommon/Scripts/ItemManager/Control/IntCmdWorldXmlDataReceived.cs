﻿using strange.extensions.command.impl;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.World;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public class IntCmdWorldXmlDataReceived : EventCommand
    {
        [Inject]
        public iWorldXmlDataset model { get; set; }

        public override void Execute()
        {
            Debug.Log("Saving World XML data to model!!!!!!!!!!!");

            MessageData data = (MessageData)evt.data;

            WorldData worldData = (WorldData)data[(byte)CommonParameterCode.DATA_WORLD_DATA];

            model.worldXmlData = worldData;
            model.interactiveItemList = new System.Collections.Generic.List<string>();
        }
    }
}