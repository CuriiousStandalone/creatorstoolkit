﻿using strange.extensions.command.impl;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public class IntCmdFindNotationItemManager : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(IntCmdCodeItemManager.FIND_NOTATION_ITEM, evt.data);
        }
    }
}