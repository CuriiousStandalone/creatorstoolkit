﻿using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public class IntCmdWorldEnteredItemManager : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(IntCmdCodeItemManager.WORLD_ENTERED, evt.data);
        }
    }
}