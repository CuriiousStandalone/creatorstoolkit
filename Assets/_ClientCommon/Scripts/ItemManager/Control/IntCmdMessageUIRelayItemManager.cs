﻿using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public class IntCmdMessageUIRelayItemManager : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(IntCmdCodeItemManager.UI_RELAY_MESSAGE, evt.data);
        }
    }
}
