﻿using strange.extensions.command.impl;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public class IntCmdWorldNotateEndItemManager : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(ECommonViewEvent.WORLD_NOTATE_END, data);
        }
    }
}