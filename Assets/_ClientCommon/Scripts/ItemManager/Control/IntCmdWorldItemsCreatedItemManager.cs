﻿using strange.extensions.command.impl;
using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common.Model;

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public class IntCmdWorldItemsCreatedItemManager : EventCommand
    {
        [Inject]
        public ICommonModel commonModel { get; set; }

        public override void Execute()
        {
            if (!ClientBridge.Instance.CurWorldData.Id.Equals(ClientBridge.Instance.TargetWorldId))
            {
                //end of loading out of date world. in case of changing loading world during load.
                dispatcher.Dispatch(IntCmdCodeItemManager.DESTROY_EVERYTHING);
                return;
            }

            OperationsWorld.WorldItemCreationCompleted(ClientBridge.Instance, ClientBridge.Instance.CurWorldData.Id);

            MemoryUtilities.CleanUpMemory();

            if (Settings.applicationType == EApplicationType.PLAYER_CLIENT)
            {
                OperationsAvatar.CreateAvatar(ClientBridge.Instance, ClientBridge.Instance.CurWorldData.Id, new PulseIQ.Local.Common.CustomType.CTVector3(-0, 0, 0), new PulseIQ.Local.Common.CustomType.CTQuaternion(0, 0, 0, 1), new PulseIQ.Local.Common.CustomType.CTVector3(1, 1, 1));
            }

            if (null != commonModel.loadingScreen)
            {
                commonModel.loadingScreen.GetComponent<ILoadingScreenView>().HideView();
            }

            dispatcher.Dispatch(ECommonCrossContextEvents.ALL_ITEMS_CREATED);
        }
    }
}