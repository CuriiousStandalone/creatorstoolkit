﻿using strange.extensions.command.impl;

using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.DllBridge;

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public class OpCmdCloseAllMultimedias : EventCommand
    {
        public override void Execute()
        {
            OperationsMultimedia.CloseMultimedia(ClientBridge.Instance);
        }
    }
}
