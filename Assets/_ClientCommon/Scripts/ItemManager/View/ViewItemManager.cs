﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using strange.extensions.mediation.impl;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.Common.Model;
using PulseIQ.Local.Client.Unity3DLib.Model;
using PulseIQ.Local.Common.CustomType;
using PulseIQ.Local.ClientCommon.AvatarItem;
using PulseIQ.Local.ClientCommon.BaseItem;
using PulseIQ.Local.ClientCommon.InteractiveItem;
using PulseIQ.Local.ClientCommon.LineNotationItem;
using PulseIQ.Local.ClientCommon.PanoramaImageItem;
using PulseIQ.Local.ClientCommon.PanoramaVideoItem;
using PulseIQ.Local.ClientCommon.PlanarVideoItem;
using PulseIQ.Local.ClientCommon.SlotItem;
using PulseIQ.Local.ClientCommon.TimerItem;
using PulseIQ.Local.ClientCommon.TutorialItem;
using PulseIQ.Local.ClientCommon.SlotGroup;
using PulseIQ.Local.ClientCommon.UIItem;
using PulseIQ.Local.Common.Model.InteractiveItem;
using PulseIQ.Local.Common.Model.UI;
using PulseIQ.Local.Common.Model.Slot;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;
using PulseIQ.Local.ClientCommon.StereoPanoramaVideoItem;
using PulseIQ.Local.ClientCommon.World;
using PulseIQ.Local.Client.Unity3DLib;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common.Model.Panorama;
using PulseIQ.Local.Common.Model.PanoramaVideo;
using PulseIQ.Local.ClientCommon.Custom;

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public class ViewItemManager : EventView
    {
        [Inject]
        public IItemDataset itemDataset { get; set; }

        [Inject]
        public IItemCreator itemCreator { get; set; }

        [Inject]
        public iWorldXmlDataset worldDataset { get; set; }

        [Inject]
        public ICommonModel commonModel { get; set; }

        [Inject]
        public IWorldDataSet worldDataSet { get; set; }

        private bool isDestroyedAll = false;

        private Dictionary<string, bool> worldItemDictionary = new Dictionary<string, bool>();

        internal enum ItemManagerEvents
        {
            NOTATION_FOUND,
            ALL_ITEMS_REMOVED,
            LOAD_ITEM,
            WORLD_NOTATE_END,
            START_TIMER,
            ITEM_LIST_STORED,
            ALL_ITEMS_CREATED,
            INTERACTIVE_ITEM_CREATED,
            ALL_ITEMS_DESTROYED,
            OWN_AVATAR_CREATED,
        }

        /// <summary>
        /// FOR testing only
        /// </summary>
        /// 
        private int progressBarCounter = 0;

        private int slotEndedAnimatingCounter = 0;

        private bool isFirstTimeAnimPlayed = false;
        private bool defaultSet = false;
        private bool isLocalCreated = false;
        private bool noItemsInWorld = true;

        private List<GameObject> nameLabelList = new List<GameObject>();

        private List<System.Object> listItemData = new List<System.Object>();

        private IEnumerator loadingItemCoroutine;

        // NEW VARIABLES

        private Transform uiRootTransform = null;
        private Transform uiURootTransform = null;

        private GameObject mediaDropdownUI = null;
        List<GameObject> mediaUIControls = new List<GameObject>();
        List<GameObject> notationUIControls = new List<GameObject>();
        List<GameObject> timerUIControls = new List<GameObject>();
        List<GameObject> tutorialUIControls = new List<GameObject>();

        public const string OnMessageReceived = "OnMessageReceived";


        internal void init()
        {
            commonModel.applicationTypeGO = GameObject.Find("ApplicationType");
            Settings.applicationType = commonModel.applicationTypeGO.GetComponent<IApplicationType>().ApplicaitonType;

            if (Settings.applicationType == EApplicationType.CONTROL_CLIENT)
            {
                //force children only. world also has "UnityCanvas"
                uiURootTransform = transform.Find("UnityCanvas").transform;
            }

            commonModel.loadingScreen = GameObject.Find("LoadingScreenBackground");
            commonModel.loadingScreen.GetComponent<ILoadingScreenView>().HideView();

            itemDataset.mainCamera = itemCreator.GetMainCamera();

            if (null == worldDataSet.worldItemToDestroyList)
            {
                worldDataSet.worldItemToDestroyList = new HashSet<string>();
            }

            uiRootTransform = GameObject.Find("Item UI Root").transform;
            Debug.Log("-----------------------------------                                 project = " +
                      Application.productName);
        }

        protected override void OnDestroy()
        {
            listItemData.Clear();
            mediaUIControls.Clear();
        }

        private void Update()
        {
        }

        private bool doesNotMatchTargetWorldID(string worldID)
        {
            return !ClientBridge.Instance.TargetWorldId.Equals(worldID);
        }

        public void SpawnItem(MessageData itemData)
        {
            DataItemData data = (DataItemData)itemData[(byte)CommonParameterCode.DATA_ITEM_DATA];
            if (doesNotMatchTargetWorldID(data.WorldId))
            {
                return;
            }

            for (int i = 0; i < itemDataset.itemList.Count; ++i)
            {
                if (itemDataset.itemList[i].itemIndex == data.ItemIndex)
                {
                    UpdateItem(data, itemDataset.itemList[i].gameObject, i);

                    CheckAllItemsCreated();
                    return;
                }
            }


            listItemData.Add(itemData);

            StartLoadingCoroutine();
        }

        // Dont match Target id to world id since this can happen anytime after initial loading.
        public IEnumerator SpawnAvatar(DataAvatar avatarData)
        {
            worldDataSet.worldItemToDestroyList.Add(avatarData.ItemId);

            foreach (var item in itemDataset.itemList)
            {
                if (item.itemID == avatarData.ItemId)
                {
                    yield break;
                }
            }

            Vector3 position = new Vector3(avatarData.Position.X, avatarData.Position.Y, avatarData.Position.Z);
            Quaternion rotation = new Quaternion(avatarData.Rotation.X, avatarData.Rotation.Y, avatarData.Rotation.Z,
                avatarData.Rotation.W);

            GameObject objSpawnedItem =
                Resources.Load(Settings.AvatarResPath + avatarData.ItemResourceId, typeof(GameObject)) as GameObject;
            objSpawnedItem = Instantiate(objSpawnedItem, position, rotation) as GameObject;
            objSpawnedItem.GetComponent<ViewAvatar>().SetItemID(avatarData.ItemId);
            objSpawnedItem.GetComponent<ViewAvatar>().SetAvatarId(avatarData.AvatarId);
            objSpawnedItem.transform.SetParent(this.transform);
            objSpawnedItem.name = avatarData.ItemId;

            itemDataset.itemList.Add(objSpawnedItem.GetComponent<ViewItemBase>());

            GameObject nameLabel =
                Instantiate(Resources.Load("Prefabs/NamePrefab") as GameObject, objSpawnedItem.transform);
            nameLabel.transform.localPosition = new Vector3(0, (1.8f / objSpawnedItem.transform.localScale.y), 0);
            if (null != nameLabel && null != nameLabel.GetComponent<ViewNameLabel>())
            {
                nameLabel.GetComponent<ViewNameLabel>().SetNames(avatarData.AvatarName);
            }

            if (ClientInstance.Instance.CurWorldData.IsNameTagVisible)
            {
                if (null != nameLabel && null != nameLabel.GetComponent<ViewNameLabel>())
                {
                    nameLabel.GetComponent<ViewNameLabel>().SetNameGameobject(avatarData.IsOwner);
                }
            }
            else
            {
                if (null != nameLabel && null != nameLabel.GetComponent<ViewNameLabel>())
                {
                    nameLabel.GetComponent<ViewNameLabel>().HideNameTag();
                }
            }

            objSpawnedItem.GetComponent<ViewAvatar>().SetNamePrefab(nameLabel);

            if (avatarData.IsOwner)
            {
                objSpawnedItem.GetComponent<ViewAvatar>().SetIsAvatarLocal(true);

                GameObject Camera = Instantiate(itemCreator.GetMainCamera(avatarData));
                Camera.transform.parent = objSpawnedItem.transform.Find("CameraPosition").transform;
                Camera.transform.localRotation = Quaternion.Euler(0, 0, 0);

                MessageData data = new MessageData();
                data.Parameters.Add((byte)CommonParameterCode.UI_EVENT_TYPE, ECommonViewEvent.CAMERA_DATA);
                data.Parameters.Add((byte)CommonParameterCode.AVATARID, avatarData.ItemId);

                itemDataset.mainCamera = Camera;

                yield return new WaitForSeconds(0.2f);

                Camera.SendMessage(OnMessageReceived, data);
                Camera.transform.localPosition = new Vector3(0, 0, 0);


                isLocalCreated = true;

                objSpawnedItem.transform.GetChild(0).gameObject.SetActive(false); // Disable local Model

                foreach (GameObject avatarLabel in nameLabelList)
                {
                    if (null != avatarLabel && null != avatarLabel.GetComponent<ViewNameLabel>())
                    {
                        avatarLabel.GetComponent<ViewNameLabel>().PositionBillboard(false, itemDataset.mainCamera);
                    }
                }

                foreach (ViewItemBase itm in itemDataset.itemList)
                {
                    if (itm.itemType == ItemTypeCode.SLOT)
                    {
                        itm.gameObject.GetComponent<ViewSlotItem>().ReferenceMainCamera(itemDataset.mainCamera);
                    }

                    if (null != itm.GetComponent<Billboard>())
                    {
                        itm.GetComponent<Billboard>().SetLookatCamera(itemDataset.mainCamera.transform);
                    }
                }

                foreach (ViewItemBase nItem in itemDataset.itemList)
                {
                    if (nItem.itemType == ItemTypeCode.NOTATION_LINE)
                    {
                        Camera cam = itemDataset.mainCamera.GetComponent<IMainCamera>().ReturnMainCamera();
                        nItem.GetComponent<ViewItemLineNotation>().SetMainCamera(cam);
                    }
                }

                nameLabel.transform.parent = objSpawnedItem.transform.Find("CameraPosition").transform;
                nameLabelList.Clear();

                MessageData msgdata = new MessageData();
                msgdata.Parameters.Add((byte)CommonParameterCode.AVATARID, avatarData.ItemId);

                dispatcher.Dispatch(ItemManagerEvents.OWN_AVATAR_CREATED, msgdata);
            }
            else
            {
                objSpawnedItem.GetComponent<ViewAvatar>().SetIsAvatarLocal(false);

                if (itemDataset.mainCamera == null && Settings.applicationType != EApplicationType.PLAYER_CLIENT)
                {
                    itemDataset.mainCamera = itemCreator.GetMainCamera();
                }

                if (isLocalCreated || Settings.applicationType != EApplicationType.PLAYER_CLIENT)
                {
                    if (null != nameLabel && null != nameLabel.GetComponent<ViewNameLabel>())
                    {
                        nameLabel.GetComponent<ViewNameLabel>()
                            .PositionBillboard(avatarData.IsOwner, itemDataset.mainCamera);
                    }
                }
                else
                {
                    nameLabelList.Add(nameLabel);
                }

                if (ClientInstance.Instance.CurWorldData.IsGazeVisible)
                {
                    objSpawnedItem.GetComponent<ViewAvatar>().InitialiseLineRenderer();
                }
            }

            if (!avatarData.IsVisible)
            {
                objSpawnedItem.GetComponent<ViewAvatar>().HideView();
            }
        }


        public IEnumerator SpawnPanoramaImagePlayer(string itemId, string worldId, ItemTypeCode itemType,
            CTPanoramaData panoramaData)
        {
            if (doesNotMatchTargetWorldID(worldId))
            {
                yield break;
            }

            worldDataSet.worldItemToDestroyList.Add(itemId);

            for (int i = 0; i < itemDataset.itemList.Count; ++i)
            {
                if (itemDataset.itemList[i].itemIndex == panoramaData.ItemIndex)
                {
                    UpdatePanoramaImagePlayer(itemId, panoramaData, itemDataset.itemList[i].gameObject, itemId, i);

                    CheckAllItemsCreated();
                    yield break;
                }
            }

            if (null == itemDataset.panoramaImagePlayer)
            {
                itemDataset.panoramaImagePlayer =
                    Resources.Load(Settings.MediaPlayerPath +
                                   panoramaData.ItemResourceId) as GameObject; // temp for testings
                itemDataset.panoramaImagePlayer = Instantiate(itemDataset.panoramaImagePlayer, this.transform);
            }

            GameObject objSpawnedItem =
                Resources.Load(Settings.MediaPlayerPath + "PanoramaImageItem") as GameObject; // temp for testings
            objSpawnedItem = Instantiate(objSpawnedItem, this.transform);
            objSpawnedItem.name = itemId;
            ViewPanoramaImagePlayer viewPanoramaImagePlayer = objSpawnedItem.GetComponent<ViewPanoramaImagePlayer>();
            viewPanoramaImagePlayer.SetItemID(itemId);

            viewPanoramaImagePlayer.itemIndex = panoramaData.ItemIndex;

            viewPanoramaImagePlayer.EndBehaviourCode = panoramaData.EndBehavior;

            viewPanoramaImagePlayer.PanoramaTeleportTime = panoramaData.PanoramaTeleportTime;

            viewPanoramaImagePlayer.SetMediaID();

            viewPanoramaImagePlayer.IsMultiplayer = panoramaData.IsMultiplayer;

            viewPanoramaImagePlayer.MediaName = panoramaData.PanoramaId;

            viewPanoramaImagePlayer.InitializeMedia(itemDataset.panoramaImagePlayer, panoramaData.ListHotspot);

            itemDataset.itemList.Add(viewPanoramaImagePlayer);

            SpawnAndActivateMediaUI();

            if (Settings.applicationType != EApplicationType.PLAYER_CLIENT)
            {
                mediaUIControls.Add(Instantiate(itemCreator.GetMediaUI(), uiRootTransform));
            }

            yield return new WaitForSeconds(0.2f);

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.PANORAMA_ID, panoramaData.PanoramaId);
            data.Parameters.Add((byte)CommonParameterCode.MEDIA_NAME, panoramaData.ItemName);
            data.Parameters.Add((byte)CommonParameterCode.HOTSPOT_ID_LIST, panoramaData.ListHotspot);
            data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, ItemTypeCode.PANORAMA);
            data.Parameters.Add((byte)CommonParameterCode.UI_EVENT_TYPE, ECommonViewEvent.DROPDOWN_ADD_TO_LIST);

            if (null != mediaDropdownUI)
            {
                mediaDropdownUI.SendMessage(OnMessageReceived, data);
            }

            worldItemDictionary[itemId] = true;

            CheckAllItemsCreated();
        }

        private void SpawnAndActivateMediaUI()
        {
            if (Settings.applicationType != EApplicationType.PLAYER_CLIENT)
            {
                if (mediaDropdownUI == null)
                {
                    mediaDropdownUI = Instantiate(itemCreator.GetMediaDropdownUI(), uiURootTransform);
                    mediaDropdownUI.SetActive(true);
                }
            }
        }

        public IEnumerator SpawnPanoramaVideoPlayer(string itemId, string worldId, ItemTypeCode itemType,
            CTPanoramaVideoData panoramaVideoData)
        {
            if (doesNotMatchTargetWorldID(worldId))
            {
                yield break;
            }

            worldDataSet.worldItemToDestroyList.Add(itemId);


            for (int i = 0; i < itemDataset.itemList.Count; ++i)
            {
                if (itemDataset.itemList[i].itemIndex == panoramaVideoData.ItemIndex)
                {
                    UpdatePanoramaPlayer(itemId, panoramaVideoData, itemDataset.itemList[i].gameObject, itemId, i);

                    CheckAllItemsCreated();
                    yield break;
                }
            }

            if (null == itemDataset.panoramaPlayer)
            {
                itemDataset.panoramaPlayer =
                    Resources.Load(Settings.MediaPlayerPath +
                                   panoramaVideoData.ItemResourceId) as GameObject; // temp for testings
                itemDataset.panoramaPlayer = Instantiate(itemDataset.panoramaPlayer, this.transform);
            }

            GameObject objSpawnedItem =
                Resources.Load(Settings.MediaPlayerPath + "PanoramaVideoItem") as GameObject; // temp for testings
            objSpawnedItem = Instantiate(objSpawnedItem, this.transform);
            ViewPanoramaVideoPlayer viewPanoramaVideoPlayer = objSpawnedItem.GetComponent<ViewPanoramaVideoPlayer>();
            viewPanoramaVideoPlayer.SetItemID(itemId);
            viewPanoramaVideoPlayer.itemIndex = panoramaVideoData.ItemIndex;
            objSpawnedItem.name = itemId;
            viewPanoramaVideoPlayer.MediaName = panoramaVideoData.PanoramaVideoId;

            viewPanoramaVideoPlayer.EndBehaviourCode = panoramaVideoData.EndBehavior;
            viewPanoramaVideoPlayer.IsMultiplayer = panoramaVideoData.IsMultiplayer;

            viewPanoramaVideoPlayer.InitializeMedia(itemDataset.panoramaPlayer, panoramaVideoData.ListHotspot);

            itemDataset.itemList.Add(viewPanoramaVideoPlayer);

            SpawnAndActivateMediaUI();

            if (Settings.applicationType != EApplicationType.PLAYER_CLIENT)
            {
                mediaUIControls.Add(Instantiate(itemCreator.GetMediaUI(), uiRootTransform));
            }

            yield return new WaitForSeconds(0.2f);

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.PANORAMA_VIDEO_ID, panoramaVideoData.PanoramaVideoId);
            data.Parameters.Add((byte)CommonParameterCode.MEDIA_NAME, panoramaVideoData.ItemName);
            data.Parameters.Add((byte)CommonParameterCode.HOTSPOT_ID_LIST, panoramaVideoData.ListHotspot);
            data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, ItemTypeCode.PANORAMA_VIDEO);
            data.Parameters.Add((byte)CommonParameterCode.UI_EVENT_TYPE, ECommonViewEvent.DROPDOWN_ADD_TO_LIST);

            if (null != mediaDropdownUI)
            {
                mediaDropdownUI.SendMessage(OnMessageReceived, data);
            }

            worldItemDictionary[itemId] = true;

            CheckAllItemsCreated();
        }

        public IEnumerator SpawnPlanarVideoPlayer(DataPlanarVideo planarVideoData)
        {
            if (doesNotMatchTargetWorldID(planarVideoData.WorldId))
            {
                yield break;
            }

            worldDataSet.worldItemToDestroyList.Add(planarVideoData.ItemId);


            for (int i = 0; i < itemDataset.itemList.Count; ++i)
            {
                if (itemDataset.itemList[i].itemIndex == planarVideoData.ItemIndex)
                {
                    UpdatePlanarPlayer(planarVideoData, itemDataset.itemList[i].gameObject, i);

                    CheckAllItemsCreated();
                    yield break;
                }
            }

            if (null == itemDataset.planarPlayer)
            {
                itemDataset.planarPlayer =
                    Resources.Load(Settings.MediaPlayerPath +
                                   planarVideoData.ItemResourceId) as GameObject; // temp for testings
                itemDataset.planarPlayer = Instantiate(itemDataset.planarPlayer, this.transform);
            }

            GameObject objSpawnedItem =
                Resources.Load(Settings.MediaPlayerPath + "PlanarVideoItem") as GameObject; // temp for testings
            objSpawnedItem = Instantiate(objSpawnedItem, this.transform);
            ViewPlanarVideoPlayer viewPlanarVideoPlayer = objSpawnedItem.GetComponent<ViewPlanarVideoPlayer>();
            viewPlanarVideoPlayer.SetItemID(planarVideoData.ItemId);
            viewPlanarVideoPlayer.itemIndex = planarVideoData.ItemIndex;
            objSpawnedItem.name = planarVideoData.ItemId;

            viewPlanarVideoPlayer.MediaName = planarVideoData.PlanarVideoId;
            viewPlanarVideoPlayer.InitializeMedia(itemDataset.planarPlayer);

            itemDataset.itemList.Add(viewPlanarVideoPlayer);

            SpawnAndActivateMediaUI();

            if (Settings.applicationType != EApplicationType.PLAYER_CLIENT)
            {
                mediaUIControls.Add(Instantiate(itemCreator.GetMediaUI(), uiRootTransform));
            }

            yield return new WaitForSeconds(0.2f);

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, planarVideoData.ItemId);
            data.Parameters.Add((byte)CommonParameterCode.PLANAR_VIDEO_ID, planarVideoData.PlanarVideoId);
            data.Parameters.Add((byte)CommonParameterCode.MEDIA_NAME, planarVideoData.ItemName);
            data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, ItemTypeCode.PLANAR_VIDEO);
            data.Parameters.Add((byte)CommonParameterCode.UI_EVENT_TYPE, ECommonViewEvent.DROPDOWN_ADD_TO_LIST);

            if (null != mediaDropdownUI)
            {
                mediaDropdownUI.SendMessage(OnMessageReceived, data);
            }

            worldItemDictionary[planarVideoData.ItemId] = true;

            CheckAllItemsCreated();
        }

        public IEnumerator SpawnStereoPanoramaVideoPlayer(string itemId, string worldId, ItemTypeCode itemType,
            CTStereoPanoramaVideoData stereoPanoramaVideoData)
        {
            if (doesNotMatchTargetWorldID(worldId))
            {
                yield break;
            }

            worldDataSet.worldItemToDestroyList.Add(itemId);


            for (int i = 0; i < itemDataset.itemList.Count; ++i)
            {
                if (itemDataset.itemList[i].itemIndex == stereoPanoramaVideoData.ItemIndex)
                {
                    UpdateStereoPanoramaPlayer(itemId, stereoPanoramaVideoData, itemDataset.itemList[i].gameObject,
                        itemId, i);

                    CheckAllItemsCreated();
                    yield break;
                }
            }

            if (null == itemDataset.stereoPanoramaPlayer)
            {
                itemDataset.stereoPanoramaPlayer =
                    Resources.Load(Settings.MediaPlayerPath +
                                   stereoPanoramaVideoData.ItemResourceId) as GameObject; // temp for testings
                itemDataset.stereoPanoramaPlayer = Instantiate(itemDataset.stereoPanoramaPlayer, this.transform);
            }

            GameObject objSpawnedItem =
                Resources.Load(Settings.MediaPlayerPath + "StereoPanoramaVideoItem") as GameObject; // temp for testings
            objSpawnedItem = Instantiate(objSpawnedItem, this.transform);
            ViewStereoPanoramaVideoPlayer viewStereoPanoramaVideoPlayer =
                objSpawnedItem.GetComponent<ViewStereoPanoramaVideoPlayer>();
            viewStereoPanoramaVideoPlayer.SetItemID(itemId);
            viewStereoPanoramaVideoPlayer.itemIndex = stereoPanoramaVideoData.ItemIndex;
            objSpawnedItem.name = itemId;
            viewStereoPanoramaVideoPlayer.MediaName = stereoPanoramaVideoData.StereoPanoramaVideoId;

            viewStereoPanoramaVideoPlayer.InitializeMedia(itemDataset.stereoPanoramaPlayer,
                stereoPanoramaVideoData.ListHotspot);

            viewStereoPanoramaVideoPlayer.EndBehaviourCode = stereoPanoramaVideoData.EndBehavior;
            viewStereoPanoramaVideoPlayer.IsMultiplayer = stereoPanoramaVideoData.IsMultiplayer;

            viewStereoPanoramaVideoPlayer.SetStereoType(stereoPanoramaVideoData.StereoPanoramaVideoType);

            itemDataset.itemList.Add(viewStereoPanoramaVideoPlayer);

            SpawnAndActivateMediaUI();
            if (Settings.applicationType != EApplicationType.PLAYER_CLIENT)
            {
                mediaUIControls.Add(Instantiate(itemCreator.GetMediaUI(), uiRootTransform));
            }

            yield return new WaitForSeconds(0.2f);

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemId);
            data.Parameters.Add((byte)CommonParameterCode.STEREO_PANORAMA_VIDEO_ID,
                stereoPanoramaVideoData.StereoPanoramaVideoId);
            data.Parameters.Add((byte)CommonParameterCode.MEDIA_NAME, stereoPanoramaVideoData.ItemName);
            data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, ItemTypeCode.STEREO_PANORAMA_VIDEO);
            data.Parameters.Add((byte)CommonParameterCode.HOTSPOT_ID_LIST, stereoPanoramaVideoData.ListHotspot);

            data.Parameters.Add((byte)CommonParameterCode.UI_EVENT_TYPE, ECommonViewEvent.DROPDOWN_ADD_TO_LIST);

            if (null != mediaDropdownUI)
            {
                mediaDropdownUI.SendMessage(OnMessageReceived, data);
            }

            worldItemDictionary[itemId] = true;

            CheckAllItemsCreated();
        }

        public IEnumerator SpawnNotation(DataLineNotation notationData)
        {
            if (doesNotMatchTargetWorldID(notationData.WorldId))
            {
                yield break;
            }

            worldDataSet.worldItemToDestroyList.Add(notationData.ItemId);

            for (int i = 0; i < itemDataset.itemList.Count; ++i)
            {
                if (itemDataset.itemList[i].itemIndex == notationData.ItemIndex)
                {
                    UpdateLineNotation(notationData, itemDataset.itemList[i].gameObject, i);

                    CheckAllItemsCreated();
                    yield break;
                }
            }

            GameObject objSpawnedItem = Resources.Load(Settings.PrefabPath + notationData.ResourceId) as GameObject;
            objSpawnedItem = Instantiate(objSpawnedItem, this.transform);
            ViewItemLineNotation viewItemLineNotation = objSpawnedItem.GetComponent<ViewItemLineNotation>();
            viewItemLineNotation.SetItemID(notationData.ItemId);
            viewItemLineNotation.itemIndex = notationData.ItemIndex;
            objSpawnedItem.name = notationData.ItemId;

            if (Settings.applicationType == EApplicationType.CONTROL_CLIENT)
            {
                if (null == itemDataset.mainCamera)
                {
                    itemDataset.mainCamera = itemCreator.GetMainCamera();
                }
            }

            yield return 0;

            if (null == itemDataset.mainCamera)
            {
                itemDataset.mainCamera = itemCreator.GetMainCamera();
            }

            if (null != itemDataset.mainCamera && Settings.applicationType != EApplicationType.PLAYER_CLIENT)
            {
                viewItemLineNotation.SetMainCamera(
                    itemDataset.mainCamera.GetComponent<IMainCamera>().ReturnMainCamera());
            }

            itemDataset.itemList.Add(viewItemLineNotation);

            if (Settings.applicationType != EApplicationType.PLAYER_CLIENT)
            {
                if (null != uiURootTransform)
                {
                    notationUIControls.Add(Instantiate(itemCreator.GetNotationItemUI(), uiURootTransform));
                }
                else
                {
                    Debug.Log("No Unity Canvas!!!!!!!!!!!!!!");
                }
            }

            worldItemDictionary[notationData.ItemId] = true;

            if (noItemsInWorld)
            {
                CheckAllItemsCreated();
                noItemsInWorld = false;
            }
        }

        public void SpawnInteractiveItem(MessageData data)
        {
            DataItemData itemData = (DataItemData)data[(byte)CommonParameterCode.DATA_ITEM_DATA];
            if (doesNotMatchTargetWorldID(itemData.WorldId))
            {
                return;
            }

            worldDataSet.worldItemToDestroyList.Add(itemData.Id);

            for (int i = 0; i < itemDataset.itemList.Count; ++i)
            {
                if (itemDataset.itemList[i].itemIndex == itemData.ItemIndex)
                {
                    UpdateInteractiveItem(data, itemDataset.itemList[i].gameObject, i);

                    CheckAllItemsCreated();
                    return;
                }
            }

            listItemData.Add(data);

            StartLoadingCoroutine();
        }

        public void SpawnSlotItem(MessageData data)
        {
            DataItemData itemData = (DataItemData)data[(byte)CommonParameterCode.DATA_ITEM_DATA];
            if (doesNotMatchTargetWorldID(itemData.WorldId))
            {
                return;
            }

            for (int i = 0; i < itemDataset.itemList.Count; ++i)
            {
                if (itemDataset.itemList[i].itemIndex == itemData.ItemIndex)
                {
                    UpdateSlotItem(data, itemDataset.itemList[i].gameObject, i);

                    CheckAllItemsCreated();
                    return;
                }
            }

            listItemData.Add(data);

            StartLoadingCoroutine();
        }


        public void SpawnUIItem(MessageData data)
        {
            DataItemData itemData = (DataItemData)data[(byte)CommonParameterCode.DATA_ITEM_DATA];
            if (doesNotMatchTargetWorldID(itemData.WorldId))
            {
                return;
            }

            worldDataSet.worldItemToDestroyList.Add(itemData.Id);


            for (int i = 0; i < itemDataset.itemList.Count; ++i)
            {
                if (itemDataset.itemList[i].itemIndex == itemData.ItemIndex)
                {
                    UpdateUIItem(data, itemDataset.itemList[i].gameObject, i);

                    CheckAllItemsCreated();
                    return;
                }
            }

            listItemData.Add(data);

            StartLoadingCoroutine();
        }

        public void SpawnSlotGroup(MessageData data)
        {
            DataItemData itemsData = (DataItemData)data[(byte)CommonParameterCode.DATA_ITEM_DATA];
            if (doesNotMatchTargetWorldID(itemsData.WorldId))
            {
                return;
            }

            worldDataSet.worldItemToDestroyList.Add(itemsData.Id);

            for (int i = 0; i < itemDataset.itemList.Count; ++i)
            {
                if (itemDataset.itemList[i].itemIndex == itemsData.ItemIndex)
                {
                    UpdateSlotGroupItem(data, itemDataset.itemList[i].gameObject, i);

                    CheckAllItemsCreated();
                    return;
                }
            }

            DataItemData itemData = (DataItemData)data.Parameters[(byte)CommonParameterCode.DATA_ITEM_DATA];

            GameObject objSpawnedItem = SpawnObject(null, itemData);

            CTVector3 position = (CTVector3)data.Parameters[(byte)CommonParameterCode.SLOT_GROUP_CAM_POS];
            CTQuaternion rotation = (CTQuaternion)data.Parameters[(byte)CommonParameterCode.SLOT_GROUP_CAM_ROT];
            CTVector3 scale = (CTVector3)data.Parameters[(byte)CommonParameterCode.SLOT_GROUP_CAM_SCL];

            int slotGroupFinishAnimItemIndex =
                (int)data.Parameters[(byte)CommonParameterCode.SLOT_GROUP_FINISH_ANIMATION_ITEM_INDEX];

            int slotGroupControlUIItemIndex =
                (int)data.Parameters[(byte)CommonParameterCode.SLOT_GROUP_CONTROL_UI_ITEM_INDEX];

            List<string> subGroups =
                (List<string>)data.Parameters[(byte)CommonParameterCode.SLOT_GROUP_SUB_GROUPS_LIST];

            objSpawnedItem.GetComponent<ViewSlotGroup>().SetDefaultDetails(position, rotation, scale,
                slotGroupFinishAnimItemIndex, slotGroupControlUIItemIndex, subGroups);

            itemDataset.itemList.Add(objSpawnedItem.GetComponent<ViewItemBase>());
        }

        public void SpawnTimerItem(MessageData data)
        {
            DataItemData itemData = (DataItemData)data[(byte)CommonParameterCode.DATA_ITEM_DATA];
            if (doesNotMatchTargetWorldID(itemData.WorldId))
            {
                return;
            }

            worldDataSet.worldItemToDestroyList.Add(itemData.Id);

            for (int i = 0; i < itemDataset.itemList.Count; ++i)
            {
                if (itemDataset.itemList[i].itemIndex == itemData.ItemIndex)
                {
                    UpdateTimerItem(data, itemDataset.itemList[i].gameObject, i);

                    CheckAllItemsCreated();
                    return;
                }
            }

            listItemData.Add(data);

            StartLoadingCoroutine();
        }

        public void SpawnTutorialItem(MessageData data)
        {
            DataItemData itemData = (DataItemData)data[(byte)CommonParameterCode.DATA_ITEM_DATA];
            if (doesNotMatchTargetWorldID(itemData.WorldId))
            {
                return;
            }

            worldDataSet.worldItemToDestroyList.Add(itemData.Id);

            for (int i = 0; i < itemDataset.itemList.Count; ++i)
            {
                if (itemDataset.itemList[i].itemIndex == itemData.ItemIndex)
                {
                    UpdateTutorialItem(data, itemDataset.itemList[i].gameObject, i);

                    CheckAllItemsCreated();
                    return;
                }
            }

            listItemData.Add(data);

            if (Settings.applicationType != EApplicationType.PLAYER_CLIENT)
            {
                mediaUIControls.Add(Instantiate(itemCreator.GetMediaUI(), uiRootTransform));
            }

            StartLoadingCoroutine();
        }


        private void StartLoadingCoroutine()
        {
            if (null == loadingItemCoroutine)
            {
                loadingItemCoroutine = LoadAssetBundleItem();
                StartCoroutine(loadingItemCoroutine);
            }
        }

        public void DestroyItem(string itemID, bool toCheckAllDestroyed = true)
        {
            for (int i = 0; i < itemDataset.itemList.Count; ++i)
            {
                if (itemDataset.itemList[i].itemID == itemID)
                {
                    switch (itemDataset.itemList[i].itemType)
                    {
                        case ItemTypeCode.PLANAR_VIDEO:
                            {
                                bool hasPlanar = false;

                                foreach (ViewItemBase item in itemDataset.itemList)
                                {
                                    if (item.itemType == ItemTypeCode.PLANAR_VIDEO && item.itemID != itemID)
                                    {
                                        hasPlanar = true;
                                        break;
                                    }
                                }

                                if (!hasPlanar)
                                {
                                    itemDataset.itemList[i].GetComponent<ViewPlanarVideoPlayer>().DestroyPlanarMediaPlayer();
                                    itemDataset.planarPlayer = null;
                                }

                                break;
                            }

                        case ItemTypeCode.PANORAMA_VIDEO:
                            {
                                bool hasPanorama = false;

                                foreach (ViewItemBase item in itemDataset.itemList)
                                {
                                    if (item.itemType == ItemTypeCode.PANORAMA_VIDEO && item.itemID != itemID)
                                    {
                                        hasPanorama = true;
                                        break;
                                    }
                                }

                                if (!hasPanorama)
                                {
                                    itemDataset.itemList[i].GetComponent<ViewPanoramaVideoPlayer>()
                                        .DestroyPanoramaMediaPlayer();
                                    itemDataset.panoramaPlayer = null;
                                }

                                break;
                            }

                        case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                            {
                                bool hasPanorama = false;

                                foreach (ViewItemBase item in itemDataset.itemList)
                                {
                                    if (item.itemType == ItemTypeCode.STEREO_PANORAMA_VIDEO && item.itemID != itemID)
                                    {
                                        hasPanorama = true;
                                        break;
                                    }
                                }

                                if (!hasPanorama)
                                {
                                    itemDataset.itemList[i].GetComponent<ViewStereoPanoramaVideoPlayer>()
                                        .DestroyPanoramaMediaPlayer();
                                    itemDataset.stereoPanoramaPlayer = null;
                                }

                                break;
                            }

                        case ItemTypeCode.PANORAMA:
                            {
                                bool hasPanorama = false;

                                foreach (ViewItemBase item in itemDataset.itemList)
                                {
                                    if (item.itemType == ItemTypeCode.PANORAMA && item.itemID != itemID)
                                    {
                                        hasPanorama = true;
                                        break;
                                    }
                                }

                                if (!hasPanorama)
                                {
                                    itemDataset.itemList[i].GetComponent<ViewPanoramaImagePlayer>()
                                        .DestroyPanoramaMediaPlayer();
                                    itemDataset.panoramaImagePlayer = null;
                                }

                                break;
                            }

                        case ItemTypeCode.NOTATION_LINE:
                            {
                                itemDataset.itemList[i].GetComponent<ViewItemLineNotation>().HideView();
                                break;
                            }
                    }


                    Destroy(itemDataset.itemList[i].gameObject);
                    itemDataset.itemList.Remove(itemDataset.itemList[i]);

                    if (worldDataSet.worldItemToDestroyList.Contains(itemID))
                    {
                        worldDataSet.worldItemToDestroyList.Remove(itemID);
                    }

                    break;
                }
            }

            if (toCheckAllDestroyed)
            {
                CheckAllItemsDestroyed();
            }
        }

        /// <summary>
        /// Destroys all the items in the world. If it's from disconnection then don't check
        /// if all items are destroyed to avoid sending the server the related events.
        /// </summary>
        /// <param name="OnDisconnectedCase">Is this called from Disconnect Event</param>
        public void DestroyAllItems(bool OnDisconnectedCase)
        {
            commonModel.loadingScreen.GetComponent<ILoadingScreenView>().ShowView();

            int size = itemDataset.itemList.Count;
            Debug.Log("Destroy All items Count = " + size);

            for (int i = 0; i < size; ++i)
            {
                DestroyItem(itemDataset.itemList[0].itemID, !OnDisconnectedCase);
            }

            ClearUI();

            if (!OnDisconnectedCase)
            {
                CheckAllItemsDestroyed();
            }
        }

        public void SendMainCameraMessage(System.Object data)
        {
            if (itemDataset.mainCamera != null)
            {
                itemDataset.mainCamera.SendMessage(OnMessageReceived, data);
            }
            else
            {
                Debug.LogWarning("Main camera == NULL!");
            }
        }

        public void SendDataToItem(MessageData data)
        {
            string itemID = (string)data[(byte)CommonParameterCode.ITEMID];

            foreach (ViewItemBase item in itemDataset.itemList)
            {
                if (item.itemID == itemID)
                {
                    item.SendMessage(OnMessageReceived, data);
                }
            }
        }

        public void SendDataToItemByIndex(MessageData data)
        {
            int itemIndex = (int)data[(byte)CommonParameterCode.ITEM_INDEX];

            foreach (ViewItemBase item in itemDataset.itemList)
            {
                if (item.itemIndex == itemIndex)
                {
                    item.SendMessage(OnMessageReceived, data);
                }
            }
        }

        public void SendDataToMultimediaItems(MessageData data)
        {
            foreach (ViewItemBase item in itemDataset.itemList)
            {
                if (item.itemType == ItemTypeCode.PANORAMA || item.itemType == ItemTypeCode.PANORAMA_VIDEO ||
                    item.itemType == ItemTypeCode.PLANAR_VIDEO || item.itemType == ItemTypeCode.STEREO_PANORAMA_VIDEO)
                {
                    item.SendMessage(OnMessageReceived, data);
                }
            }
        }

        public void SendUIDataToItemByItemIndex(MessageData messageData)
        {
            int itemIndex = (int)messageData[(byte)CommonParameterCode.ITEM_INDEX];

            foreach (ViewItemBase item in itemDataset.itemList)
            {
                if (item.itemIndex == itemIndex)
                {
                    item.SendMessage("OnUIMessageReceived", messageData);
                }
            }
        }

        public void FindNotationItem(MessageData data)
        {
            string itemID = (string)data[(byte)CommonParameterCode.MEDIA_REFERENCE_ID];

            ENotationType notationType = (ENotationType)data[(byte)CommonParameterCode.NOTATION_TYPE];

            foreach (ViewItemBase item in itemDataset.itemList)
            {
                if (item.itemType == ItemTypeCode.NOTATION_LINE)
                {
                    ViewItemLineNotation viewItemLineNotation = item.gameObject.GetComponent<ViewItemLineNotation>();
                    if (viewItemLineNotation.notationType == ENotationType.NONE)
                    {
                        viewItemLineNotation.notationType = notationType;

                        if ((notationType) != ENotationType.WORLD)
                        {
                            viewItemLineNotation.mediaItemIDReference = itemID;

                            if ((notationType) == ENotationType.TUTORIAL_ITEM)
                            {
                                foreach (ViewItemBase player in itemDataset.itemList)
                                {
                                    if (player.itemID == itemID)
                                    {
                                        viewItemLineNotation.notatedObject = player.gameObject;
                                    }
                                }
                            }
                        }
                        else
                        {
                            viewItemLineNotation.mediaItemIDReference = itemID;
                        }

                        MessageData itemData = new MessageData();
                        itemData.Parameters.Add((byte)CommonParameterCode.ITEMID, item.itemID);

                        dispatcher.Dispatch(ItemManagerEvents.NOTATION_FOUND, itemData);
                    }
                }
            }
        }

        public void ActivateMediaUI(MessageData itemData)
        {
            string itemID = (string)itemData[(byte)CommonParameterCode.ITEMID];
            ENotationType notationType = (ENotationType)itemData[(byte)CommonParameterCode.NOTATION_TYPE];

            foreach (ViewItemBase item in itemDataset.itemList)
            {
                if (item.itemID == itemID)
                {
                    switch (notationType)
                    {
                        case ENotationType.PANORAMA:
                            {
                                item.GetComponent<ViewPanoramaImagePlayer>().ActivateUI();
                                break;
                            }

                        case ENotationType.PANORAMA_VIDEO:
                            {
                                //item.GetComponent<ViewPanoramaVideoPlayer>().controlUI.SetActive(true);
                                break;
                            }

                        case ENotationType.PLANAR_VIDEO:
                            {
                                //item.GetComponent<ViewPlanarVideoPlayer>().controlUI.SetActive(true);
                                break;
                            }

                        case ENotationType.TUTORIAL_ITEM:
                        case ENotationType.WORLD:
                            {
                                break;
                            }

                        default:
                            {
                                break;
                            }
                    }
                }
            }
        }

        public void MediaDataReceived(MessageData data)
        {
            GameObject mediaControlUI = null;

            string itemID = (string)data[(byte)CommonParameterCode.ITEM_REFERENCE_ID];

            if (itemID == "WORLDNOTATION")
            {
                dispatcher.Dispatch(ItemManagerEvents.WORLD_NOTATE_END);
                return;
            }

            foreach (GameObject mediaUI in mediaUIControls)
            {
                if (null != mediaUI.GetComponent<IUIBasic>() &&
                    mediaUI.GetComponent<IUIBasic>().ReturnItemID() == itemID)
                {
                    mediaControlUI = mediaUI;
                }
            }

            if (null == mediaControlUI)
            {
                foreach (GameObject mediaUI in mediaUIControls)
                {
                    if (null != mediaUI.GetComponent<IUIBasic>() &&
                        mediaUI.GetComponent<IUIBasic>().ReturnItemID() == "")
                    {
                        mediaControlUI = mediaUI;
                        break;
                    }
                }
            }

            if (null != mediaControlUI)
            {
                mediaControlUI.SendMessage(OnMessageReceived, data);
            }

            if (null != mediaDropdownUI)
            {
                mediaDropdownUI.SendMessage(OnMessageReceived, data);
            }
        }

        public void NotationDataReceived(MessageData data)
        {
            GameObject notationUI = null;

            string itemID = (string)data[(byte)CommonParameterCode.ITEM_REFERENCE_ID];

            foreach (GameObject notationObjs in notationUIControls)
            {
                string objID = notationObjs.GetComponent<IUIBasic>().ReturnItemID();

                if (null != notationObjs.GetComponent<IUIBasic>() &&
                    notationObjs.GetComponent<IUIBasic>().ReturnItemID() == itemID)
                {
                    notationUI = notationObjs;
                }
            }

            if (null == notationUI)
            {
                foreach (GameObject notationObjs in notationUIControls)
                {
                    if (null != notationObjs.GetComponent<IUIBasic>() &&
                        notationObjs.GetComponent<IUIBasic>().ReturnItemID() == "")
                    {
                        notationUI = notationObjs;
                        break;
                    }
                }
            }

            if (notationUI != null)
            {
                notationUI.SendMessage(OnMessageReceived, data);
            }
        }

        public void NotationMediaDataReceived(MessageData data)
        {
            GameObject notationUI = null;

            string itemID = (string)data[(byte)CommonParameterCode.MEDIA_REFERENCE_ID];

            foreach (GameObject notationObjs in notationUIControls)
            {
                string objID = notationObjs.GetComponent<IUIBasic>().ReturnMediaID();

                if (null != notationObjs.GetComponent<IUIBasic>() &&
                    notationObjs.GetComponent<IUIBasic>().ReturnMediaID() == itemID)
                {
                    notationUI = notationObjs;
                }
            }

            if (notationUI != null)
            {
                notationUI.SendMessage(OnMessageReceived, data);
            }
        }

        public void TutorialDataReceived(MessageData data)
        {
            if (mediaDropdownUI != null)
            {
                mediaDropdownUI.SendMessage(OnMessageReceived, data);
            }
        }

        public void TimerDataReceived(MessageData data)
        {
            GameObject timerUI = null;

            string itemID = (string)data[(byte)CommonParameterCode.ITEM_REFERENCE_ID];

            foreach (GameObject timerObjs in timerUIControls)
            {
                string objID = timerObjs.GetComponent<IUIBasic>().ReturnItemID();

                if (null != timerObjs.GetComponent<IUIBasic>() &&
                    timerObjs.GetComponent<IUIBasic>().ReturnItemID() == itemID)
                {
                    timerUI = timerObjs;
                }
            }

            if (null == timerUI)
            {
                foreach (GameObject timerObjs in timerUIControls)
                {
                    if (null != timerObjs.GetComponent<IUIBasic>() &&
                        timerObjs.GetComponent<IUIBasic>().ReturnItemID() == "")
                    {
                        timerUI = timerObjs;
                        break;
                    }
                }
            }

            if (timerUI != null)
            {
                timerUI.SendMessage(OnMessageReceived, data);
            }
        }

        public void DropdownDataReceived(MessageData data)
        {
            if (mediaDropdownUI != null)
            {
                mediaDropdownUI.SendMessage(OnMessageReceived, data);
            }
        }

        public void UIControlDataReceived(MessageData msgData)
        {
            if (null != mediaUIControls)
            {
                foreach (var item in mediaUIControls)
                {
                    item.SendMessage(OnMessageReceived, msgData);
                }
            }
        }

        public void RaycasterDataReceived(MessageData data)
        {
            itemDataset.mainCamera.SendMessage(OnMessageReceived, data);
        }

        IEnumerator LoadAssetBundleItem()
        {
            AssetBundle assetBundle = null;

            DataItemData itemData;

            string path;

            var data = listItemData[0];
            listItemData.RemoveAt(0);

            itemData = (DataItemData)((MessageData)data).Parameters[(byte)CommonParameterCode.DATA_ITEM_DATA];
            ItemTypeCode type = (ItemTypeCode)((MessageData)data).Parameters[(byte)CommonParameterCode.ITEM_TYPE];

            if (type == ItemTypeCode.INTERACTIVE_ITEM)
            {
                path = Settings.ItemFilePath;
            }
            else if (type == ItemTypeCode.SLOT)
            {
                path = Settings.SlotItemFilePath;
            }
            else if (type == ItemTypeCode.SYNCTIMER)
            {
                path = Settings.TimerItemFilePath;
            }
            else if (type == ItemTypeCode.TUTORIAL_ITEM)
            {
                path = Settings.TutorialItemFilePath;
            }
            else if (type == ItemTypeCode.UI)
            {
                path = Settings.UIItemFilePath;
            }
            else
            {
                path = Settings.BaseItemFilePath;
            }

            // Debug.Log("Resource Path: " + path + itemData.ItemResourceId);

            Caching.ClearCache();

            using (WWW www = new WWW(path + itemData.ItemResourceId))
            {
                yield return www;

                if (null != www.error)
                {
                    Debug.Log("ViewItemManager LoadAssetBundleItem Error: " + www.error);
                }
                else
                {
                    assetBundle = www.assetBundle;

                    AssetBundleRequest request =
                        assetBundle.LoadAssetAsync(itemData.ItemResourceId, typeof(GameObject));

                    yield return request;

                    GameObject obj = request.asset as GameObject;

                    GameObject objSpawnedItem = SpawnObject(obj, itemData);

                    if (Settings.applicationType != EApplicationType.PLAYER_CLIENT)
                    {
                        if (null != objSpawnedItem.GetComponent<Billboard>())
                        {
                            objSpawnedItem.GetComponent<Billboard>()
                                .SetLookatCamera(itemCreator.GetMainCamera().transform);
                        }
                    }

                    if (itemData.ItemType == ItemTypeCode.ITEM)
                    {
                        if (itemData.IsVisible)
                        {
                            objSpawnedItem.GetComponent<ViewItemBase>().ShowView();
                        }
                        else
                        {
                            objSpawnedItem.GetComponent<ViewItemBase>().HideView();
                        }
                    }
                    else if (itemData.ItemType == ItemTypeCode.SLOT)
                    {
                        CTVector3 position =
                            (CTVector3)((MessageData)data).Parameters[(byte)CommonParameterCode.SLOT_LOCAL_POSITION];
                        CTQuaternion rotation =
                            (CTQuaternion)((MessageData)data).Parameters[
                                (byte)CommonParameterCode.SLOT_LOCAL_ROTATION];
                        CTVector3 scale =
                            (CTVector3)((MessageData)data).Parameters[(byte)CommonParameterCode.SLOT_LOCAL_SCALE];
                        string slotID = ((int)((MessageData)data).Parameters[(byte)CommonParameterCode.SLOTID])
                            .ToString();
                        bool isIndicating =
                            (bool)((MessageData)data).Parameters[(byte)CommonParameterCode.SLOT_IS_INDICATING];
                        bool isOpen = (bool)((MessageData)data).Parameters[(byte)CommonParameterCode.SLOT_IS_OPEN];
                        string emptyAnimName =
                            (string)((MessageData)data).Parameters[(byte)CommonParameterCode.SLOT_EMPTY_ANIM_NAME];
                        string correctAnimName =
                            (string)((MessageData)data).Parameters[(byte)CommonParameterCode.SLOT_CORRECT_ANIM_NAME];
                        string incorrectAnimName =
                            (string)((MessageData)data).Parameters[
                                (byte)CommonParameterCode.SLOT_INCORRECT_ANIM_NAME];
                        ESlotState slotState =
                            (ESlotState)((MessageData)data).Parameters[(byte)CommonParameterCode.SLOT_STATE];

                        if (Settings.applicationType != EApplicationType.PLAYER_CLIENT)
                        {
                            objSpawnedItem.GetComponent<ViewSlotItem>()
                                .ReferenceMainCamera(itemCreator.GetMainCamera());
                        }

                        objSpawnedItem.GetComponent<ViewSlotItem>().SetDefaultDetails(position, rotation, scale, slotID,
                            isOpen, isIndicating, slotState, emptyAnimName, correctAnimName, incorrectAnimName);

                        if (itemData.IsVisible)
                        {
                            objSpawnedItem.GetComponent<ViewItemBase>().ShowView();
                        }
                        else
                        {
                            objSpawnedItem.GetComponent<ViewItemBase>().HideView();
                        }

                        if (isIndicating)
                        {
                            objSpawnedItem.GetComponent<ViewSlotItem>().SlotIndicatorShown(slotState);
                        }
                        else
                        {
                            objSpawnedItem.GetComponent<ViewSlotItem>().SlotIndicatorHidden();
                        }
                    }
                    else if (itemData.ItemType == ItemTypeCode.INTERACTIVE_ITEM)
                    {
                        int[] slotIDs =
                            (int[])((MessageData)data).Parameters[(byte)CommonParameterCode.INTERACTIVE_ITEM_SLOTS];
                        EAbandonAction abandonAction =
                            (EAbandonAction)((MessageData)data).Parameters[
                                (byte)CommonParameterCode.INTERACTIVE_ITEM_ABANDON_ACTION];
                        EObtainAction obtainAction =
                            (EObtainAction)((MessageData)data).Parameters[
                                (byte)CommonParameterCode.INTERACTIVE_ITEM_OBTAIN_ACTION];
                        int attachedIndex =
                            (int)((MessageData)data).Parameters[(byte)CommonParameterCode.ITEM_INDEX];
                        bool isRepickable =
                            (bool)((MessageData)data).Parameters[(byte)CommonParameterCode.INTERACTIVE_ISREPICKABLE];

                        ViewItemInteractive viewItemInteractive = objSpawnedItem.GetComponent<ViewItemInteractive>();
                        viewItemInteractive.SetInteractiveItemSlotList(slotIDs);
                        viewItemInteractive.SetAbandonAction(abandonAction);
                        viewItemInteractive.SetObtainAction(obtainAction);
                        viewItemInteractive.SetAttachedItemIndex(attachedIndex);
                        viewItemInteractive.SetIsRepickable(isRepickable);
                        viewItemInteractive.SaveCurrentMaterials();

                        //if (Settings.applicationType == EApplicationType.PLAYER_CLIENT)
                        //{
                        //    objSpawnedItem.AddComponent<Outline>();
                        //    objSpawnedItem.GetComponent<Outline>().OutlineWidth = 0f;
                        //    objSpawnedItem.GetComponent<Outline>().OutlineMode = Outline.Mode.OutlineAll;
                        //}

                        dispatcher.Dispatch(ItemManagerEvents.INTERACTIVE_ITEM_CREATED, data);

                        if (itemData.IsVisible)
                        {
                            objSpawnedItem.GetComponent<ViewItemBase>().ShowView();
                        }
                        else
                        {
                            objSpawnedItem.GetComponent<ViewItemBase>().HideView();
                        }
                    }
                    else if (itemData.ItemType == ItemTypeCode.SYNCTIMER)
                    {
                        bool isCountdownTimer =
                            (bool)((MessageData)data).Parameters[(byte)CommonParameterCode.COUNTDOWN_TIMER];
                        float totalTime =
                            (float)((MessageData)data).Parameters[(byte)CommonParameterCode.TOTAL_TIME];
                        float interval = (float)((MessageData)data).Parameters[(byte)CommonParameterCode.INTERVAL];
                        float curTime =
                            (float)((MessageData)data).Parameters[(byte)CommonParameterCode.CURRENT_TIME];

                        objSpawnedItem.GetComponent<ViewTimerItem>().SetDefaultValues(isCountdownTimer, totalTime,
                            interval, curTime, itemData.IsVisible);

                        if (Settings.applicationType != EApplicationType.PLAYER_CLIENT)
                        {
                            timerUIControls.Add(Instantiate(itemCreator.GetTimerItemUI(), uiRootTransform));
                        }

                        foreach (GameObject ui in timerUIControls)
                        {
                            ui.SetActive(false);
                        }

                        if (itemData.IsVisible)
                        {
                            objSpawnedItem.GetComponent<ViewItemBase>().ShowView();
                        }
                        else
                        {
                            objSpawnedItem.GetComponent<ViewItemBase>().HideView();
                        }
                    }
                    else if (itemData.ItemType == ItemTypeCode.TUTORIAL_ITEM)
                    {
                        CTVector3 position =
                            (CTVector3)((MessageData)data).Parameters[
                                (byte)CommonParameterCode.OBSERVER_LOCAL_POSITION];
                        CTQuaternion rotation =
                            (CTQuaternion)((MessageData)data).Parameters[
                                (byte)CommonParameterCode.OBSERVER_LOCAL_ROTATION];
                        List<string> animList =
                            (List<string>)((MessageData)data).Parameters[(byte)CommonParameterCode.ANIMATION_LIST];
                        objSpawnedItem.GetComponent<ViewTutorialItem>().SetObserverVariables(position, rotation);
                        objSpawnedItem.GetComponent<ViewTutorialItem>().SetAnimationList(animList);

                        if (Settings.applicationType != EApplicationType.PLAYER_CLIENT)
                        {
                            SpawnAndActivateMediaUI();

                            mediaUIControls.Add(Instantiate(itemCreator.GetMediaUI(), uiRootTransform));

                            yield return new WaitForSeconds(0.2f);

                            MessageData tutData = new MessageData();
                            tutData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemData.Id);
                            tutData.Parameters.Add((byte)CommonParameterCode.MEDIA_NAME, itemData.Name);
                            tutData.Parameters.Add((byte)CommonParameterCode.OBSERVER_LOCAL_POSITION, position);
                            tutData.Parameters.Add((byte)CommonParameterCode.OBSERVER_LOCAL_ROTATION, rotation);
                            tutData.Parameters.Add((byte)CommonParameterCode.ANIMATION_LIST, animList);
                            tutData.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, ItemTypeCode.TUTORIAL_ITEM);
                            tutData.Parameters.Add((byte)CommonParameterCode.UI_EVENT_TYPE,
                                ECommonViewEvent.DROPDOWN_ADD_TO_LIST);

                            if (null != mediaDropdownUI)
                            {
                                mediaDropdownUI.SendMessage(OnMessageReceived, tutData);
                            }
                        }
                    }
                    else if (itemData.ItemType == ItemTypeCode.UI)
                    {
                        EUIType uiType = (EUIType)((MessageData)data).Parameters[(byte)CommonParameterCode.UI_TYPE];
                        List<int> itemIndices =
                            (List<int>)((MessageData)data).Parameters[(byte)CommonParameterCode.ITEM_INDICES];

                        ViewUIItem viewUiItem = objSpawnedItem.GetComponent<ViewUIItem>();
                        viewUiItem.SaveUIType(uiType);
                        viewUiItem.SetCorrespondingItemIndexes(itemIndices);
                        viewUiItem.itemType = ItemTypeCode.UI;

                        if (itemData.IsVisible)
                        {
                            viewUiItem.ShowView();
                        }
                        else
                        {
                            viewUiItem.HideView();
                        }
                    }

                    itemDataset.itemList.Add(objSpawnedItem.GetComponent<ViewItemBase>());

                    assetBundle.Unload(false);

                    worldItemDictionary[itemData.Id] = true;

                    UpdateProgressBar();

                    CheckAllItemsCreated();

                    www.Dispose();
                }

                if (listItemData.Count > 0)
                {
                    loadingItemCoroutine = LoadAssetBundleItem();
                    StartCoroutine(loadingItemCoroutine);
                }
                else
                {
                    StopCoroutine(loadingItemCoroutine);
                    loadingItemCoroutine = null;
                }
            }
        }

        private GameObject SpawnObject(GameObject obj, DataItemData itemData)
        {
            ViewItemBase script = null;

            Vector3 position = new Vector3(itemData.Position.X, itemData.Position.Y, itemData.Position.Z);
            Quaternion rotation = new Quaternion(itemData.Rotation.X, itemData.Rotation.Y, itemData.Rotation.Z,
                itemData.Rotation.W);
            Vector3 scale = new Vector3(itemData.Scale.X, itemData.Scale.Y, itemData.Scale.Z);
            GameObject objSpawnedItem;

            if (null != obj)
            {
                objSpawnedItem = Instantiate(obj, position, rotation) as GameObject;
            }
            else
            {
                objSpawnedItem = new GameObject();
            }

            objSpawnedItem.transform.localScale = scale;

            switch (itemData.ItemType)
            {
                case ItemTypeCode.INTERACTIVE_ITEM:
                    {
                        script = objSpawnedItem.AddComponent<ViewItemInteractive>();
                        break;
                    }

                case ItemTypeCode.SLOT:
                    {
                        script = objSpawnedItem.AddComponent<ViewSlotItem>();

                        break;
                    }

                case ItemTypeCode.SYNCTIMER:
                    {
                        script = objSpawnedItem.AddComponent<ViewTimerItem>();
                        break;
                    }

                case ItemTypeCode.TUTORIAL_ITEM:
                    {
                        script = objSpawnedItem.AddComponent<ViewTutorialItem>();
                        break;
                    }

                case ItemTypeCode.ITEM:
                    {
                        script = objSpawnedItem.AddComponent<ViewItemBase>();
                        break;
                    }

                case ItemTypeCode.UI:
                    {
                        script = objSpawnedItem.AddComponent<ViewUIItem>();
                        break;
                    }

                case ItemTypeCode.SLOT_GROUP:
                    {
                        script = objSpawnedItem.AddComponent<ViewSlotGroup>();
                        break;
                    }
            }

            script.SetItemID(itemData.Id);
            script.itemIndex = itemData.ItemIndex;
            objSpawnedItem.transform.SetParent(transform);
            objSpawnedItem.name = itemData.Id;

            //if(itemData.ItemIndex == worldDataset.worldXmlData.SlotGroupFinishActionItemIndex)
            //{
            //    objSpawnedItem.GetComponent<ViewItemBase>().HideView();
            //}

            objSpawnedItem.GetComponent<ViewItemBase>().SetItemDefaultReturnPositions(position, rotation, scale);
            return objSpawnedItem;
        }

        public void ClearUI()
        {
            for (int i = 0; i < mediaUIControls.Count; ++i)
            {
                Destroy(mediaUIControls[i]);
            }

            for (int i = 0; i < notationUIControls.Count; ++i)
            {
                Destroy(notationUIControls[i]);
            }

            for (int i = 0; i < timerUIControls.Count; ++i)
            {
                Destroy(timerUIControls[i]);
            }

            for (int i = 0; i < tutorialUIControls.Count; ++i)
            {
                Destroy(tutorialUIControls[i]);
            }

            tutorialUIControls.Clear();
            timerUIControls.Clear();
            notationUIControls.Clear();
            mediaUIControls.Clear();

            itemDataset.mainCamera = null;
            isLocalCreated = false;

            if (mediaDropdownUI != null)
            {
                Destroy(mediaDropdownUI);
            }

            MemoryUtilities.CleanUpMemory();
        }

        public void OnWorldEntered(DataWorldData worldData)
        {
            noItemsInWorld = true;
            isDestroyedAll = false;
            isFirstTimeAnimPlayed = false;
            worldItemDictionary.Clear();

            foreach (DataItemData item in worldData.ListItems)
            {
                if (item.ItemType == ItemTypeCode.SPAWN_POINT || item.ItemType == ItemTypeCode.NOTATION_LINE ||
                    item.ItemType == ItemTypeCode.AVATAR || item.ItemType == ItemTypeCode.SLOT_GROUP)
                {
                    continue;
                }

                if (!worldDataSet.worldItemToDestroyList.Contains(item.Id))
                {
                    worldDataSet.worldItemToDestroyList.Add(item.Id);
                }

                noItemsInWorld = false;
                //if (item.ItemType == ItemTypeCode.PANORAMA || item.ItemType == ItemTypeCode.PANORAMA_VIDEO || item.ItemType == ItemTypeCode.PLANAR_VIDEO || item.ItemType == ItemTypeCode.STEREO_PANORAMA_VIDEO)
                //{
                //    continue;
                //}

                worldItemDictionary.Add(item.Id, false);
            }

            IProgressBarView progressBarView = commonModel.loadingScreen.GetComponent<IProgressBarView>();
            progressBarView.ShowProgressBar();
            progressBarView.UpdateProgressText("Loading module resources...");

            dispatcher.Dispatch(ItemManagerEvents.ITEM_LIST_STORED);
        }

        public void SlotAnimationEnded()
        {
            slotEndedAnimatingCounter++;

            int totalSlotItems = 0;

            foreach (var item in itemDataset.itemList)
            {
                if (item.itemType == ItemTypeCode.SLOT)
                {
                    totalSlotItems++;
                }
            }

            if (slotEndedAnimatingCounter >= totalSlotItems)
            {
                ViewUIItem viewUIItem = GetReplayUI();

                if (null == viewUIItem)
                {
                    return;
                }

                if (isFirstTimeAnimPlayed)
                {
                    foreach (int index in viewUIItem.responseItemIndices)
                    {
                        foreach (var item in itemDataset.itemList)
                        {
                            if (item.itemIndex == index)
                            {
                                item.GetComponent<ViewSlotGroup>().ReplayAnimEnded();
                            }
                        }
                    }
                }

                isFirstTimeAnimPlayed = true;

                viewUIItem.ShowView();

                slotEndedAnimatingCounter = 0;
            }
        }

        private ViewUIItem GetReplayUI()
        {
            foreach (var item in itemDataset.itemList)
            {
                if (item.itemType == ItemTypeCode.UI)
                {
                    if (item.GetComponent<ViewUIItem>().IsReplayButton())
                    {
                        return item.GetComponent<ViewUIItem>();
                    }
                }
            }

            return null;
        }

        private void CheckAllItemsCreated()
        {
            bool allItemsCreated = true;

            foreach (string item in worldItemDictionary.Keys)
            {
                if (worldItemDictionary[item] == false)
                {
                    allItemsCreated = false;
                    break;
                }
            }

            if (allItemsCreated)
            {
                worldItemDictionary.Clear();

                dispatcher.Dispatch(ItemManagerEvents.ALL_ITEMS_CREATED);

                if (Settings.applicationType == EApplicationType.PLAYER_CLIENT)
                {
                    foreach (var item in itemDataset.itemList)
                    {
                        if (item.itemType == ItemTypeCode.SLOT_GROUP)
                        {
                            ((ViewSlotGroup)item).NavigateNext();
                        }
                    }
                }

                IProgressBarView progressBarView = commonModel.loadingScreen.GetComponent<IProgressBarView>();
                progressBarView.UpdateProgress(1);
                progressBarCounter = 0;
                progressBarView.HideProgressBar();
                // force media dropdown on for all worlds.
                SpawnAndActivateMediaUI();
            }
        }

        private void CheckAllItemsDestroyed()
        {
            if (worldDataSet.worldItemToDestroyList.Count <= 0 && !isDestroyedAll)
            {
                isDestroyedAll = true;
                Debug.Log("[ItemManager.CheckAllItemsDestroyed] All Items are destroyed");
                dispatcher.Dispatch(ItemManagerEvents.ALL_ITEMS_DESTROYED);
            }
        }

        private void UpdateProgressBar()
        {
            progressBarCounter++;

            float val = (float)progressBarCounter / (float)worldItemDictionary.Count;

            commonModel.loadingScreen.GetComponent<IProgressBarView>().UpdateProgress(val);
        }

        private void UpdateItem(DataItemData itemData, GameObject itemObj, int listIndex)
        {
            Destroy(itemObj.GetComponent<ViewItemBase>());
            Destroy(itemObj.GetComponent<MediatorItemBase>());

            ViewItemBase itemScript = itemObj.AddComponent<ViewItemBase>();

            Vector3 position = new Vector3(itemData.Position.X, itemData.Position.Y, itemData.Position.Z);
            Quaternion rotation = new Quaternion(itemData.Rotation.X, itemData.Rotation.Y, itemData.Rotation.Z,
                itemData.Rotation.W);
            Vector3 scale = new Vector3(itemData.Scale.X, itemData.Scale.Y, itemData.Scale.Z);

            itemObj.transform.position = position;
            itemObj.transform.rotation = rotation;
            itemObj.transform.localScale = scale;
            itemScript.SetItemDefaultReturnPositions(position, rotation, scale);

            if (itemData.IsVisible)
            {
                itemScript.ShowView();
            }
            else
            {
                itemScript.HideView();
            }

            itemScript.itemID = itemData.Id;
            itemScript.name = itemData.Id;
            itemScript.itemIndex = itemData.ItemIndex;

            worldItemDictionary[itemData.Id] = true;

            itemDataset.itemList[listIndex] = itemScript;
        }

        private void UpdateInteractiveItem(MessageData itemData, GameObject itemObj, int listIndex)
        {
            Destroy(itemObj.GetComponent<ViewItemInteractive>());
            Destroy(itemObj.GetComponent<MediatorItemInteractive>());

            ViewItemInteractive interactiveScript = itemObj.AddComponent<ViewItemInteractive>();

            DataItemData dataItemData = (DataItemData)itemData[(byte)CommonParameterCode.DATA_ITEM_DATA];

            Vector3 position = new Vector3(dataItemData.Position.X, dataItemData.Position.Y, dataItemData.Position.Z);
            Quaternion rotation = new Quaternion(dataItemData.Rotation.X, dataItemData.Rotation.Y,
                dataItemData.Rotation.Z, dataItemData.Rotation.W);
            Vector3 scale = new Vector3(dataItemData.Scale.X, dataItemData.Scale.Y, dataItemData.Scale.Z);

            itemObj.transform.position = position;
            itemObj.transform.rotation = rotation;
            itemObj.transform.localScale = scale;
            interactiveScript.SetItemDefaultReturnPositions(position, rotation, scale);

            if (dataItemData.IsVisible)
            {
                interactiveScript.ShowView();
            }
            else
            {
                interactiveScript.HideView();
            }

            interactiveScript.itemID = dataItemData.Id;
            interactiveScript.name = dataItemData.Id;
            interactiveScript.itemIndex = dataItemData.ItemIndex;

            int[] slotIDs =
                (int[])((MessageData)itemData).Parameters[(byte)CommonParameterCode.INTERACTIVE_ITEM_SLOTS];
            EAbandonAction abandonAction =
                (EAbandonAction)((MessageData)itemData).Parameters[
                    (byte)CommonParameterCode.INTERACTIVE_ITEM_ABANDON_ACTION];
            EObtainAction obtainAction =
                (EObtainAction)((MessageData)itemData).Parameters[
                    (byte)CommonParameterCode.INTERACTIVE_ITEM_OBTAIN_ACTION];
            int attachedIndex = (int)((MessageData)itemData).Parameters[(byte)CommonParameterCode.ITEM_INDEX];
            bool isRepickable = (bool)itemData[(byte)CommonParameterCode.INTERACTIVE_ISREPICKABLE];

            interactiveScript.SetInteractiveItemSlotList(slotIDs);
            interactiveScript.SetAbandonAction(abandonAction);
            interactiveScript.SetObtainAction(obtainAction);
            interactiveScript.SetAttachedItemIndex(attachedIndex);
            interactiveScript.SetIsRepickable(isRepickable);

            worldItemDictionary[dataItemData.Id] = true;

            itemDataset.itemList[listIndex] = interactiveScript;
        }

        private void UpdateSlotItem(MessageData itemData, GameObject itemObj, int listIndex)
        {
            Destroy(itemObj.GetComponent<ViewSlotItem>());
            Destroy(itemObj.GetComponent<MediatorSlotItem>());

            ViewSlotItem slotScript = itemObj.AddComponent<ViewSlotItem>();

            DataItemData dataItemData = (DataItemData)itemData[(byte)CommonParameterCode.DATA_ITEM_DATA];

            Vector3 position = new Vector3(dataItemData.Position.X, dataItemData.Position.Y, dataItemData.Position.Z);
            Quaternion rotation = new Quaternion(dataItemData.Rotation.X, dataItemData.Rotation.Y,
                dataItemData.Rotation.Z, dataItemData.Rotation.W);
            Vector3 scale = new Vector3(dataItemData.Scale.X, dataItemData.Scale.Y, dataItemData.Scale.Z);

            itemObj.transform.position = position;
            itemObj.transform.rotation = rotation;
            itemObj.transform.localScale = scale;
            slotScript.SetItemDefaultReturnPositions(position, rotation, scale);

            if (dataItemData.IsVisible)
            {
                slotScript.ShowView();
            }
            else
            {
                slotScript.HideView();
            }

            slotScript.itemID = dataItemData.Id;
            slotScript.name = dataItemData.Id;
            slotScript.itemIndex = dataItemData.ItemIndex;

            CTVector3 localPosition = (CTVector3)itemData.Parameters[(byte)CommonParameterCode.SLOT_LOCAL_POSITION];
            CTQuaternion localRotation =
                (CTQuaternion)itemData.Parameters[(byte)CommonParameterCode.SLOT_LOCAL_ROTATION];
            CTVector3 localScale = (CTVector3)itemData.Parameters[(byte)CommonParameterCode.SLOT_LOCAL_SCALE];
            string slotID = ((int)itemData.Parameters[(byte)CommonParameterCode.SLOTID]).ToString();
            bool isIndicating = (bool)itemData.Parameters[(byte)CommonParameterCode.SLOT_IS_INDICATING];
            bool isOpen = (bool)itemData.Parameters[(byte)CommonParameterCode.SLOT_IS_OPEN];
            string emptyAnimName = (string)itemData.Parameters[(byte)CommonParameterCode.SLOT_EMPTY_ANIM_NAME];
            string correctAnimName = (string)itemData.Parameters[(byte)CommonParameterCode.SLOT_CORRECT_ANIM_NAME];
            string incorrectAnimName =
                (string)itemData.Parameters[(byte)CommonParameterCode.SLOT_INCORRECT_ANIM_NAME];
            ESlotState slotState = (ESlotState)itemData.Parameters[(byte)CommonParameterCode.SLOT_STATE];

            if (Settings.applicationType != EApplicationType.PLAYER_CLIENT)
            {
                slotScript.ReferenceMainCamera(itemCreator.GetMainCamera());
            }

            slotScript.SetDefaultDetails(localPosition, localRotation, localScale, slotID, isOpen, isIndicating,
                slotState, emptyAnimName, correctAnimName, incorrectAnimName);

            if (isIndicating)
            {
                slotScript.SlotIndicatorShown(slotState);
            }
            else
            {
                slotScript.SlotIndicatorHidden();
            }

            worldItemDictionary[dataItemData.Id] = true;

            itemDataset.itemList[listIndex] = slotScript;
        }

        private void UpdateTimerItem(MessageData itemData, GameObject itemObj, int listIndex)
        {
            Destroy(itemObj.GetComponent<ViewTimerItem>());
            Destroy(itemObj.GetComponent<MediatorTimerItem>());

            ViewTimerItem timerScript = itemObj.AddComponent<ViewTimerItem>();

            DataItemData dataItemData = (DataItemData)itemData[(byte)CommonParameterCode.DATA_ITEM_DATA];

            Vector3 position = new Vector3(dataItemData.Position.X, dataItemData.Position.Y, dataItemData.Position.Z);
            Quaternion rotation = new Quaternion(dataItemData.Rotation.X, dataItemData.Rotation.Y,
                dataItemData.Rotation.Z, dataItemData.Rotation.W);
            Vector3 scale = new Vector3(dataItemData.Scale.X, dataItemData.Scale.Y, dataItemData.Scale.Z);

            itemObj.transform.position = position;
            itemObj.transform.rotation = rotation;
            itemObj.transform.localScale = scale;
            timerScript.SetItemDefaultReturnPositions(position, rotation, scale);

            if (dataItemData.IsVisible)
            {
                timerScript.ShowView();
            }
            else
            {
                timerScript.HideView();
            }

            timerScript.itemID = dataItemData.Id;
            timerScript.name = dataItemData.Id;
            timerScript.itemIndex = dataItemData.ItemIndex;

            bool isCountdownTimer = (bool)itemData.Parameters[(byte)CommonParameterCode.COUNTDOWN_TIMER];
            float totalTime = (float)itemData.Parameters[(byte)CommonParameterCode.TOTAL_TIME];
            float interval = (float)itemData.Parameters[(byte)CommonParameterCode.INTERVAL];
            float curTime = (float)itemData.Parameters[(byte)CommonParameterCode.CURRENT_TIME];

            timerScript.SetDefaultValues(isCountdownTimer, totalTime, interval, curTime, dataItemData.IsVisible);

            if (Settings.applicationType != EApplicationType.PLAYER_CLIENT)
            {
                timerUIControls.Add(Instantiate(itemCreator.GetTimerItemUI(), uiRootTransform));
            }

            foreach (GameObject ui in timerUIControls)
            {
                ui.SetActive(false);
            }

            worldItemDictionary[dataItemData.Id] = true;

            itemDataset.itemList[listIndex] = timerScript;
        }

        private void UpdateTutorialItem(MessageData itemData, GameObject itemObj, int listIndex)
        {
            Destroy(itemObj.GetComponent<ViewTutorialItem>());
            Destroy(itemObj.GetComponent<MediatorTutorialItem>());

            ViewTutorialItem tutorialScript = itemObj.AddComponent<ViewTutorialItem>();

            DataItemData dataItemData = (DataItemData)itemData[(byte)CommonParameterCode.DATA_ITEM_DATA];

            Vector3 position = new Vector3(dataItemData.Position.X, dataItemData.Position.Y, dataItemData.Position.Z);
            Quaternion rotation = new Quaternion(dataItemData.Rotation.X, dataItemData.Rotation.Y,
                dataItemData.Rotation.Z, dataItemData.Rotation.W);
            Vector3 scale = new Vector3(dataItemData.Scale.X, dataItemData.Scale.Y, dataItemData.Scale.Z);

            itemObj.transform.position = position;
            itemObj.transform.rotation = rotation;
            itemObj.transform.localScale = scale;
            tutorialScript.SetItemDefaultReturnPositions(position, rotation, scale);

            if (dataItemData.IsVisible)
            {
                tutorialScript.ShowView();
            }
            else
            {
                tutorialScript.HideView();
            }

            tutorialScript.itemID = dataItemData.Id;
            tutorialScript.name = dataItemData.Id;
            tutorialScript.itemIndex = dataItemData.ItemIndex;

            CTVector3 localPosition =
                (CTVector3)itemData.Parameters[(byte)CommonParameterCode.OBSERVER_LOCAL_POSITION];
            CTQuaternion localRotation =
                (CTQuaternion)itemData.Parameters[(byte)CommonParameterCode.OBSERVER_LOCAL_ROTATION];
            List<string> animList = (List<string>)itemData.Parameters[(byte)CommonParameterCode.ANIMATION_LIST];

            tutorialScript.SetObserverVariables(localPosition, localRotation);
            tutorialScript.SetAnimationList(animList);

            if (Settings.applicationType != EApplicationType.PLAYER_CLIENT)
            {
                tutorialUIControls.Add(Instantiate(itemCreator.GetTutorialItemUI(), uiRootTransform));

                SpawnAndActivateMediaUI();
                mediaUIControls.Add(Instantiate(itemCreator.GetMediaUI(), uiRootTransform));

                MessageData tutData = new MessageData();
                tutData.Parameters.Add((byte)CommonParameterCode.ITEMID, dataItemData.Id);
                tutData.Parameters.Add((byte)CommonParameterCode.MEDIA_NAME, dataItemData.Name);
                tutData.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, ItemTypeCode.TUTORIAL_ITEM);
                tutData.Parameters.Add((byte)CommonParameterCode.UI_EVENT_TYPE, ECommonViewEvent.DROPDOWN_ADD_TO_LIST);

                if (null != mediaDropdownUI)
                {
                    mediaDropdownUI.SendMessage(OnMessageReceived, tutData);
                }

                worldItemDictionary[dataItemData.Id] = true;

                itemDataset.itemList[listIndex] = tutorialScript;
            }
        }

        private void UpdateSlotGroupItem(MessageData itemData, GameObject itemObj, int listIndex)
        {
            Destroy(itemObj.GetComponent<ViewSlotGroup>());
            Destroy(itemObj.GetComponent<MediatorSlotGroup>());

            ViewSlotGroup slotGroupScript = itemObj.AddComponent<ViewSlotGroup>();

            DataItemData dataItemData = (DataItemData)itemData[(byte)CommonParameterCode.DATA_ITEM_DATA];

            Vector3 position = new Vector3(dataItemData.Position.X, dataItemData.Position.Y, dataItemData.Position.Z);
            Quaternion rotation = new Quaternion(dataItemData.Rotation.X, dataItemData.Rotation.Y,
                dataItemData.Rotation.Z, dataItemData.Rotation.W);
            Vector3 scale = new Vector3(dataItemData.Scale.X, dataItemData.Scale.Y, dataItemData.Scale.Z);

            itemObj.transform.position = position;
            itemObj.transform.rotation = rotation;
            itemObj.transform.localScale = scale;
            slotGroupScript.SetItemDefaultReturnPositions(position, rotation, scale);

            if (dataItemData.IsVisible)
            {
                slotGroupScript.ShowView();
            }
            else
            {
                slotGroupScript.HideView();
            }

            slotGroupScript.itemID = dataItemData.Id;
            slotGroupScript.name = dataItemData.Id;
            slotGroupScript.itemIndex = dataItemData.ItemIndex;

            CTVector3 camPosition = (CTVector3)itemData[(byte)CommonParameterCode.SLOT_GROUP_CAM_POS];
            CTQuaternion camRotation = (CTQuaternion)itemData[(byte)CommonParameterCode.SLOT_GROUP_CAM_ROT];
            CTVector3 camScale = (CTVector3)itemData[(byte)CommonParameterCode.SLOT_GROUP_CAM_SCL];

            int slotGroupFinishAnimItemIndex =
                (int)itemData[(byte)CommonParameterCode.SLOT_GROUP_FINISH_ANIMATION_ITEM_INDEX];

            int slotGroupControlUIItemIndex =
                (int)itemData[(byte)CommonParameterCode.SLOT_GROUP_CONTROL_UI_ITEM_INDEX];

            List<string> subGroups = (List<string>)itemData[(byte)CommonParameterCode.SLOT_GROUP_SUB_GROUPS_LIST];

            slotGroupScript.SetDefaultDetails(camPosition, camRotation, camScale, slotGroupFinishAnimItemIndex,
                slotGroupControlUIItemIndex, subGroups);

            worldItemDictionary[dataItemData.Id] = true;

            itemDataset.itemList[listIndex] = slotGroupScript;
        }

        private void UpdatePanoramaImagePlayer(string id, CTPanoramaData itemData, GameObject itemObj, string itemID,
            int listIndex)
        {
            if (null == itemDataset.panoramaImagePlayer)
            {
                itemDataset.panoramaImagePlayer =
                    Resources.Load(Settings.MediaPlayerPath +
                                   itemData.ItemResourceId) as GameObject; // temp for testings
                itemDataset.panoramaImagePlayer = Instantiate(itemDataset.panoramaImagePlayer, this.transform);
            }

            ViewPanoramaImagePlayer script = itemObj.GetComponent<ViewPanoramaImagePlayer>();
            itemObj.name = itemID;
            script.SetItemID(itemID);
            script.itemIndex = itemData.ItemIndex;

            script.SetMediaID();
            script.MediaName = itemData.PanoramaId;
            script.InitializeMedia(itemDataset.panoramaImagePlayer, itemData.ListHotspot);

            SpawnAndActivateMediaUI();

            if (Settings.applicationType != EApplicationType.PLAYER_CLIENT)
            {
                mediaUIControls.Add(Instantiate(itemCreator.GetMediaUI(), uiRootTransform));
            }

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);
            data.Parameters.Add((byte)CommonParameterCode.PANORAMA_ID, itemData.PanoramaId);
            data.Parameters.Add((byte)CommonParameterCode.MEDIA_NAME, itemData.ItemName);
            data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, ItemTypeCode.PANORAMA);
            data.Parameters.Add((byte)CommonParameterCode.UI_EVENT_TYPE, ECommonViewEvent.DROPDOWN_ADD_TO_LIST);

            if (null != mediaDropdownUI)
            {
                mediaDropdownUI.SendMessage(OnMessageReceived, data);
            }

            itemDataset.itemList[listIndex] = script;

            worldItemDictionary[id] = true;
        }

        private void UpdatePanoramaPlayer(string id, CTPanoramaVideoData itemData, GameObject itemObj, string itemID,
            int listIndex)
        {
            if (null == itemDataset.panoramaPlayer)
            {
                itemDataset.panoramaPlayer =
                    Resources.Load(Settings.MediaPlayerPath +
                                   itemData.ItemResourceId) as GameObject; // temp for testings
                itemDataset.panoramaPlayer = Instantiate(itemDataset.panoramaPlayer, this.transform);
            }

            ViewPanoramaVideoPlayer script = itemObj.GetComponent<ViewPanoramaVideoPlayer>();
            script.SetItemID(itemID);
            script.itemIndex = itemData.ItemIndex;
            script.EndBehaviourCode = itemData.EndBehavior;
            script.IsMultiplayer = itemData.IsMultiplayer;
            itemObj.name = itemID;
            script.MediaName = itemData.PanoramaVideoId;

            script.InitializeMedia(itemDataset.panoramaPlayer, itemData.ListHotspot);

            SpawnAndActivateMediaUI();

            if (Settings.applicationType != EApplicationType.PLAYER_CLIENT)
            {
                mediaUIControls.Add(Instantiate(itemCreator.GetMediaUI(), uiRootTransform));
            }

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);
            data.Parameters.Add((byte)CommonParameterCode.PANORAMA_VIDEO_ID, itemData.PanoramaVideoId);
            data.Parameters.Add((byte)CommonParameterCode.MEDIA_NAME, itemData.ItemName);
            data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, ItemTypeCode.PANORAMA_VIDEO);
            data.Parameters.Add((byte)CommonParameterCode.UI_EVENT_TYPE, ECommonViewEvent.DROPDOWN_ADD_TO_LIST);

            if (null != mediaDropdownUI)
            {
                mediaDropdownUI.SendMessage(OnMessageReceived, data);
            }

            itemDataset.itemList[listIndex] = script;

            worldItemDictionary[id] = true;
        }

        private void UpdateStereoPanoramaPlayer(string id, CTStereoPanoramaVideoData itemData, GameObject itemObj,
            string itemID, int listIndex)
        {
            if (null == itemDataset.stereoPanoramaPlayer)
            {
                itemDataset.stereoPanoramaPlayer =
                    Resources.Load(Settings.MediaPlayerPath +
                                   itemData.ItemResourceId) as GameObject; // temp for testings
                itemDataset.stereoPanoramaPlayer = Instantiate(itemDataset.stereoPanoramaPlayer, this.transform);
            }

            ViewStereoPanoramaVideoPlayer script = itemObj.GetComponent<ViewStereoPanoramaVideoPlayer>();
            script.SetItemID(itemID);
            script.itemIndex = itemData.ItemIndex;
            itemObj.name = itemID;
            script.MediaName = itemData.StereoPanoramaVideoId;
            script.InitializeMedia(itemDataset.stereoPanoramaPlayer, itemData.ListHotspot);
            script.SetStereoType(itemData.StereoPanoramaVideoType);
            script.EndBehaviourCode = itemData.EndBehavior;
            script.IsMultiplayer = itemData.IsMultiplayer;

            SpawnAndActivateMediaUI();

            if (Settings.applicationType != EApplicationType.PLAYER_CLIENT)
            {
                mediaUIControls.Add(Instantiate(itemCreator.GetMediaUI(), uiRootTransform));
            }

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);
            data.Parameters.Add((byte)CommonParameterCode.STEREO_PANORAMA_VIDEO_ID, itemData.StereoPanoramaVideoId);
            data.Parameters.Add((byte)CommonParameterCode.MEDIA_NAME, itemData.ItemName);
            data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, ItemTypeCode.STEREO_PANORAMA_VIDEO);
            data.Parameters.Add((byte)CommonParameterCode.UI_EVENT_TYPE, ECommonViewEvent.DROPDOWN_ADD_TO_LIST);

            if (null != mediaDropdownUI)
            {
                mediaDropdownUI.SendMessage(OnMessageReceived, data);
            }

            itemDataset.itemList[listIndex] = script;

            worldItemDictionary[id] = true;
        }

        private void UpdatePlanarPlayer(DataPlanarVideo itemData, GameObject itemObj, int listIndex)
        {
            if (null == itemDataset.planarPlayer)
            {
                itemDataset.planarPlayer =
                    Resources.Load(Settings.MediaPlayerPath +
                                   itemData.ItemResourceId) as GameObject; // temp for testings
                itemDataset.planarPlayer = Instantiate(itemDataset.planarPlayer, this.transform);
            }

            ViewPlanarVideoPlayer script = itemObj.GetComponent<ViewPlanarVideoPlayer>();
            script.SetItemID(itemData.ItemId);
            script.itemIndex = itemData.ItemIndex;
            itemObj.name = itemData.ItemId;

            script.MediaName = itemData.PlanarVideoId;
            script.InitializeMedia(itemDataset.planarPlayer);

            SpawnAndActivateMediaUI();

            if (Settings.applicationType != EApplicationType.PLAYER_CLIENT)
            {
                mediaUIControls.Add(Instantiate(itemCreator.GetMediaUI(), uiRootTransform));
            }

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEMID, itemData.ItemId);
            data.Parameters.Add((byte)CommonParameterCode.PLANAR_VIDEO_ID, itemData.PlanarVideoId);
            data.Parameters.Add((byte)CommonParameterCode.MEDIA_NAME, itemData.ItemName);
            data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, ItemTypeCode.PLANAR_VIDEO);
            data.Parameters.Add((byte)CommonParameterCode.UI_EVENT_TYPE, ECommonViewEvent.DROPDOWN_ADD_TO_LIST);

            if (null != mediaDropdownUI)
            {
                mediaDropdownUI.SendMessage(OnMessageReceived, data);
            }

            itemDataset.itemList[listIndex] = script;

            worldItemDictionary[itemData.ItemId] = true;
        }

        private void UpdateLineNotation(DataLineNotation itemData, GameObject itemObj, int listIndex)
        {
            ViewItemLineNotation script = itemObj.GetComponent<ViewItemLineNotation>();
            script.SetItemID(itemData.ItemId);
            script.itemIndex = itemData.ItemIndex;
            itemObj.name = itemData.ItemId;

            if (Settings.applicationType == EApplicationType.CONTROL_CLIENT)
            {
                if (null == itemDataset.mainCamera)
                {
                    itemDataset.mainCamera = itemCreator.GetMainCamera();
                }
            }

            if (null == itemDataset.mainCamera)
            {
                itemDataset.mainCamera = itemCreator.GetMainCamera();
            }

            if (null != itemDataset.mainCamera || Settings.applicationType != EApplicationType.PLAYER_CLIENT)
            {
                script.SetMainCamera(itemDataset.mainCamera.GetComponent<IMainCamera>().ReturnMainCamera());
            }

            if (Settings.applicationType != EApplicationType.PLAYER_CLIENT)
            {
                if (null != uiURootTransform)
                {
                    notationUIControls.Add(Instantiate(itemCreator.GetNotationItemUI(), uiURootTransform));
                }
                else
                {
                    Debug.Log("No Unity Canvas!!!!!!!!!!!!!!");
                }
            }

            itemDataset.itemList[listIndex] = script;

            worldItemDictionary[itemData.ItemId] = true;

            if (noItemsInWorld)
            {
                CheckAllItemsCreated();
                noItemsInWorld = false;
            }
        }

        private void UpdateUIItem(MessageData data, GameObject itemObj, int listIndex)
        {
            EUIType uiType = (EUIType)((MessageData)data).Parameters[(byte)CommonParameterCode.UI_TYPE];
            List<int> itemIndices =
                (List<int>)((MessageData)data).Parameters[(byte)CommonParameterCode.ITEM_INDICES];
            string itemid = itemObj.GetComponent<ViewUIItem>().itemID;

            Destroy(itemObj.GetComponent<ViewUIItem>());
            Destroy(itemObj.GetComponent<MediatorUIItem>());

            ViewUIItem script = itemObj.AddComponent<ViewUIItem>();

            script.SaveUIType(uiType);
            script.SetCorrespondingItemIndexes(itemIndices);
            script.itemType = ItemTypeCode.UI;


            DataItemData itemData = (DataItemData)data[(byte)CommonParameterCode.DATA_ITEM_DATA];

            if (itemData.IsVisible)
            {
                script.ShowView();
            }
            else
            {
                script.HideView();
            }

            if (worldItemDictionary.ContainsKey(itemData.Id))
            {
                worldItemDictionary[itemData.Id] = true;
            }
            else
            {
                worldItemDictionary.Add(itemData.Id, true);
            }

            if (worldItemDictionary.ContainsKey(itemid))
            {
                worldItemDictionary.Remove(itemid);
            }

            script.SetItemID(itemData.Id);
            script.itemIndex = itemData.ItemIndex;
            itemDataset.itemList[listIndex] = script;
        }
    }
}