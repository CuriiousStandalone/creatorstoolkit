﻿using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

using PulseIQ.Local.Client.Unity3DLib.Model;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.World;
using PulseIQ.Local.Common;

using PulseIQ.Local.ClientCommon.UIItem;
using System.Collections.Generic;
using PulseIQ.Local.Common.Model.StereoPanoramaVideo;
using PulseIQ.Local.Common.Model.PanoramaVideo;
using PulseIQ.Local.Common.Model.Panorama;

//NEED TO CHANGE

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public class MediatorItemManager : EventMediator
    {
        [Inject]
        public ViewItemManager view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        private void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewItemManager.ItemManagerEvents.NOTATION_FOUND, OnNotationFound);
            view.dispatcher.UpdateListener(value, ViewItemManager.ItemManagerEvents.ALL_ITEMS_REMOVED, OnAllItemsRemoved);
            view.dispatcher.UpdateListener(value, ViewItemManager.ItemManagerEvents.WORLD_NOTATE_END, OnWorldNotateEnd);
            view.dispatcher.UpdateListener(value, ViewItemManager.ItemManagerEvents.START_TIMER, OnStartTimer);
            view.dispatcher.UpdateListener(value, ViewItemManager.ItemManagerEvents.ITEM_LIST_STORED, OnItemListStored);
            view.dispatcher.UpdateListener(value, ViewItemManager.ItemManagerEvents.ALL_ITEMS_CREATED, OnAllWorldItemsCreated);
            view.dispatcher.UpdateListener(value, ViewItemManager.ItemManagerEvents.INTERACTIVE_ITEM_CREATED, OnInteractiveItemCreated);
            view.dispatcher.UpdateListener(value, ViewItemManager.ItemManagerEvents.ALL_ITEMS_DESTROYED, OnAllItemsDestoryed);
            view.dispatcher.UpdateListener(value, ViewItemManager.ItemManagerEvents.OWN_AVATAR_CREATED, OnOwnAvatarCreated);

            //NEW
            dispatcher.UpdateListener(value, IntCmdCodeItemManager.CAMERA_MESSAGE, OnCameraMessage);
            dispatcher.UpdateListener(value, IntCmdCodeItemManager.TUTORIAL_DATA, OnTutorialDataReceived);
            dispatcher.UpdateListener(value, IntCmdCodeItemManager.TIMER_DATA, OnTimerDataReceived);
            dispatcher.UpdateListener(value, IntCmdCodeItemManager.CREATE_ITEM, OnItemCreated);
            dispatcher.UpdateListener(value, IntCmdCodeItemManager.RELAY_MESSAGE, OnItemDataReceived);
            dispatcher.UpdateListener(value, IntCmdCodeItemManager.RELAY_MESSAGE_ITEM_INDEX, OnItemByIndexDataReceived);
            dispatcher.UpdateListener(value, IntCmdCodeItemManager.UI_RELAY_MESSAGE, OnDataReceivedFromUI);
            dispatcher.UpdateListener(value, IntCmdCodeItemManager.RELAY_MESSAGE_TO_UI, OnRelayMessageToUI);
            dispatcher.UpdateListener(value, IntCmdCodeItemManager.DESTROY_ITEM, OnItemDestroyed);
            dispatcher.UpdateListener(value, IntCmdCodeItemManager.MEDIA_DATA, OnMediaDataReceived);
            dispatcher.UpdateListener(value, IntCmdCodeItemManager.NOTATION_DATA, OnNotationDataReceived);
            dispatcher.UpdateListener(value, IntCmdCodeItemManager.RAYCAST_DATA, OnRaycasterDataReceived);
            dispatcher.UpdateListener(value, IntCmdCodeItemManager.FIND_NOTATION_ITEM, OnFindNotationItem);
            dispatcher.UpdateListener(value, IntCmdCodeItemManager.DESTROY_UI, OnRemoveAllUI);
            dispatcher.UpdateListener(value, IntCmdCodeItemManager.NOTATION_MEDIA_DATA, OnNotationMediaDataReceived);
            dispatcher.UpdateListener(value, IntCmdCodeItemManager.WORLD_ENTERED, OnWorldEntered);
            dispatcher.UpdateListener(value, IntCmdCodeItemManager.MEDIA_DROPDOWN_DATA, OnMediaDropdownDataReceived);
            dispatcher.UpdateListener(value, IntCmdCodeItemManager.MEDIA_UI_CONTROL_DATA, OnMediaUIControlDataReceived);
            dispatcher.UpdateListener(value, IntCmdCodeItemManager.SLOT_ANIMATION_ENDED, OnSlotAnimationEnded);
            dispatcher.UpdateListener(value, IntCmdCodeItemManager.DESTROY_EVERYTHING, OnDestroyAllItems);
            dispatcher.UpdateListener(value, IntCmdCodeItemManager.DESTROY_EVERYTHING_ON_DISCONNECT, OnDestroyAllItemsOnDisconnect);

            dispatcher.UpdateListener(value, IntCmdCodeMultimedia.RELAY_MESSAGE_MULTIMEDIA, OnMultimediaDataReceived);

            // Remove for when syncing items!
            // dispatcher.UpdateListener(value, EventCode.Teleported, OnDestroyAllItems);
        }



        private void OnItemCreated(IEvent evt)
        {
            MessageData itemData = (MessageData)evt.data;

            ItemTypeCode type = (ItemTypeCode)itemData.Parameters[(byte)CommonParameterCode.ITEM_TYPE];

            switch (type)
            {
                case ItemTypeCode.INTERACTIVE_ITEM:
                    {
                        OnCreateInteractiveItem(evt);
                        break;
                    }

                case ItemTypeCode.ITEM:
                    {
                        view.SpawnItem(itemData);
                        break;
                    }

                case ItemTypeCode.UI:
                    {
                        view.SpawnUIItem(itemData);
                        break;
                    }

                case ItemTypeCode.NOTATION_LINE:
                    {
                        OnNotationCreate(evt);
                        break;
                    }

                case ItemTypeCode.AVATAR:
                    {
                        OnAvatarCreated(evt);
                        break;
                    }

                case ItemTypeCode.PANORAMA:
                    {
                        OnPanoramaImageCreated(evt);
                        break;
                    }

                case ItemTypeCode.PANORAMA_VIDEO:
                    {
                        OnPanoramaVideoCreated(evt);
                        break;
                    }

                case ItemTypeCode.PLANAR_VIDEO:
                    {
                        OnPlanarVideoCreated(evt);
                        break;
                    }

                case ItemTypeCode.SLOT:
                    {
                        OnCreateSlotItem(evt);
                        break;
                    }
                case ItemTypeCode.SYNCTIMER:
                    {
                        OnCreateTimerItem(evt);
                        break;
                    }
                case ItemTypeCode.TUTORIAL_ITEM:
                    {
                        OnCreateTutorialItem(evt);
                        break;
                    }
                case ItemTypeCode.SLOT_GROUP:
                    {
                        OnCreateSlotGroup(evt);
                        break;
                    }
                case ItemTypeCode.STEREO_PANORAMA_VIDEO:
                    {
                        OnStereoPanoramaVideoCreated(evt);
                        break;
                    }
                default:
                    {
                        view.SpawnItem(itemData);
                        break;
                    }
            }      
        }

        #region new

        private void OnStartTimer(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.StartSyncTimer, evt.data);
        }

        private void OnCameraMessage(IEvent evt)
        {
            view.SendMainCameraMessage(evt.data);
        }

        private void OnMediaDataReceived(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;
            view.MediaDataReceived(data);            
        }

        private void OnNotationDataReceived(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;
            view.NotationDataReceived(data);
        }

        private void OnNotationMediaDataReceived(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;
            view.NotationMediaDataReceived(data);
        }

        private void OnTutorialDataReceived(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;
            view.TutorialDataReceived(data);
        }

        private void OnTimerDataReceived(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;
            view.TimerDataReceived(data);
        }

        private void OnRaycasterDataReceived(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;
            view.RaycasterDataReceived(data);
        }

        private void OnMediaDropdownDataReceived(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;
            view.DropdownDataReceived(data);
        }

        private void OnMediaUIControlDataReceived(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;
            view.UIControlDataReceived(data);
        }

        #endregion

        private void OnAvatarCreated(IEvent evt)
        {
            MessageData avatarData = (MessageData)evt.data;

            DataAvatar itemData = (DataAvatar)avatarData[(byte)CommonParameterCode.DATA_AVATAR];

            StartCoroutine(view.SpawnAvatar(itemData));
        }

        private void OnPanoramaImageCreated(IEvent evt)
        {
            MessageData panoramaImageData = (MessageData)evt.data;
            string itemId = (string)panoramaImageData[(byte)CommonParameterCode.ITEMID];
            string worldId = (string)panoramaImageData[(byte)CommonParameterCode.WORLDID];
            ItemTypeCode itemTypeCode = (ItemTypeCode)panoramaImageData[(byte)CommonParameterCode.ITEM_TYPE];
            CTPanoramaData itemData = (CTPanoramaData)panoramaImageData[(byte)CommonParameterCode.DATA_PANORAMA];

            StartCoroutine(view.SpawnPanoramaImagePlayer(itemId, worldId, itemTypeCode, itemData));
        }

        private void OnPanoramaVideoCreated(IEvent evt)
        {
            MessageData panoramaVideoData = (MessageData)evt.data;
            string itemId = (string)panoramaVideoData[(byte)CommonParameterCode.ITEMID];
            string worldId = (string)panoramaVideoData[(byte)CommonParameterCode.WORLDID];
            ItemTypeCode itemTypeCode = (ItemTypeCode)panoramaVideoData[(byte)CommonParameterCode.ITEM_TYPE];
            CTPanoramaVideoData itemData = (CTPanoramaVideoData)panoramaVideoData[(byte)CommonParameterCode.DATA_PANORAMA_VIDEO];

            StartCoroutine(view.SpawnPanoramaVideoPlayer(itemId, worldId, itemTypeCode, itemData));
        }

        private void OnPlanarVideoCreated(IEvent evt)
        {
            MessageData planarData = (MessageData)evt.data;
            DataPlanarVideo itemData = (DataPlanarVideo)planarData[(byte)CommonParameterCode.DATA_PLANAR_VIDEO];

            StartCoroutine(view.SpawnPlanarVideoPlayer(itemData));
        }

        private void OnStereoPanoramaVideoCreated(IEvent evt)
        {
            MessageData stereoPanoramaVideoData = (MessageData)evt.data;

            string itemId = (string)stereoPanoramaVideoData[(byte)CommonParameterCode.ITEMID];
            string worldId = (string)stereoPanoramaVideoData[(byte)CommonParameterCode.WORLDID];
            ItemTypeCode itemTypeCode = (ItemTypeCode)stereoPanoramaVideoData[(byte)CommonParameterCode.ITEM_TYPE];
            CTStereoPanoramaVideoData itemData = (CTStereoPanoramaVideoData)stereoPanoramaVideoData[(byte)CommonParameterCode.STEREO_PANORAMA_VIDEO_DATA];

            StartCoroutine(view.SpawnStereoPanoramaVideoPlayer(itemId, worldId, itemTypeCode, itemData));
        }

        private void OnNotationCreate(IEvent evt)
        {
            MessageData notationData = (MessageData)evt.data;
            DataLineNotation itemData = (DataLineNotation)notationData[(byte)CommonParameterCode.DATA_LINE_NOTATION];

            StartCoroutine(view.SpawnNotation(itemData));
        }

        private void OnCreateInteractiveItem(IEvent evt)
        {
            MessageData itemData = (MessageData)evt.data;

            view.SpawnInteractiveItem(itemData);
        }

        protected void OnCreateSlotItem(IEvent evt)
        {
            MessageData itemData = (MessageData)evt.data;

            view.SpawnSlotItem(itemData);
        }

        protected void OnCreateUIItem(IEvent evt)
        {
            MessageData itemData = (MessageData)evt.data;

            view.SpawnUIItem(itemData);
        }

        protected void OnCreateSlotGroup(IEvent evt)
        {
            MessageData messageData = (MessageData)evt.data;

            view.SpawnSlotGroup(messageData);
        }

        protected void OnCreateTimerItem(IEvent evt)
        {
            MessageData itemData = (MessageData)evt.data;

            view.SpawnTimerItem(itemData);
        }

        protected void OnCreateTutorialItem(IEvent evt)
        {
            MessageData itemData = (MessageData)evt.data;

            view.SpawnTutorialItem(itemData);
        }

        private void OnItemDataReceived(IEvent evt)
        {
            view.SendDataToItem((MessageData)evt.data);
        }

        private void OnItemByIndexDataReceived(IEvent evt)
        {
            view.SendDataToItemByIndex((MessageData)evt.data);
        }

        private void OnMultimediaDataReceived(IEvent evt)
        {
            view.SendDataToMultimediaItems((MessageData)evt.data);
        }

        private void OnDataReceivedFromUI(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;

            UIActionCode uIAction = (UIActionCode)data[(byte)CommonParameterCode.UI_ACTION];

            List<int> itemIndices = (List<int>)data[(byte)CommonParameterCode.ITEM_INDICES];

            foreach (int itemIndex in itemIndices)
            {
                MessageData message = new MessageData();
                message.Parameters.Add((byte)CommonParameterCode.ITEM_INDEX, itemIndex);
                message.Parameters.Add((byte)CommonParameterCode.UI_ACTION, uIAction);

                view.SendUIDataToItemByItemIndex(message);
            }
        }

        private void OnRelayMessageToUI(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;

            view.DropdownDataReceived(data);
        }

        private void OnItemDestroyed(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;
            view.DestroyItem((string)data.Parameters[(byte)CommonParameterCode.ITEMID]);
        }
        
        private void OnDestroyAllItemsOnDisconnect(IEvent payload)
        {
            view.DestroyAllItems(true);
        }

        private void OnDestroyAllItems(IEvent evt)
        {
            view.DestroyAllItems(false);
        }

        private void OnFindNotationItem(IEvent evt)
        {
            MessageData itemData = (MessageData)evt.data;

            view.FindNotationItem(itemData);
        }

        private void OnNotationFound(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.ShowLineNotation, evt.data);
        }

        private void OnShowMediaUI(IEvent evt)
        {
            MessageData itemData = (MessageData)evt.data;

            view.ActivateMediaUI(itemData);
        }

        private void OnRemoveAllUI(IEvent evt)
        {
            view.ClearUI();
        }

        private void OnAllItemsRemoved(IEvent evt)
        {
            dispatcher.Dispatch(IntCmdCodeItemManager.ALL_ITEMS_REMOVED, evt.data);
        }

        private void OnAllItemsDestoryed(IEvent evt)
        {
            dispatcher.Dispatch(IntCmdCodeItemManager.ALL_ITEMS_DESTROYED);
            dispatcher.Dispatch(IntCmdCodeWorld.ALL_ITEM_DESTROYED);
        }

        private void OnOwnAvatarCreated(IEvent evt)
        {
            dispatcher.Dispatch(SubCode.AvatarCreated, evt.data);
        }

        private void OnWorldNotateEnd(IEvent evt)
        {
            dispatcher.Dispatch(IntCmdCodeItemManager.WORLD_NOTATE_END);
        }

        private void OnWorldEntered(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;

            DataWorldData worldData = (DataWorldData)data[(byte)CommonParameterCode.DATA_WORLD_DATA];
            view.OnWorldEntered(worldData);
        }
        
        private void OnItemListStored(IEvent evt)
        {
            dispatcher.Dispatch(IntCmdCodeItemManager.WORLD_DATA_STORED);
        }
        
        private void OnAllWorldItemsCreated()
        {
            dispatcher.Dispatch(IntCmdCodeItemManager.WORLD_ITEMS_CREATED);
        }

        private void OnInteractiveItemCreated(IEvent evt)
        {
            dispatcher.Dispatch(IntCmdCodeItemManager.INTERACTIVE_ITEM_CREATED, evt.data);
        }

        private void OnSlotAnimationEnded()
        {
            view.SlotAnimationEnded();
        }
    }
}
