﻿using UnityEngine;
using System.Collections.Generic;
using PulseIQ.Local.ClientCommon.BaseItem;

namespace PulseIQ.Local.ClientCommon.Components.Common
{
    public interface IItemDataset
    {
        List<ViewItemBase> itemList { get; set; }

        GameObject mainCamera { get; set; }

        GameObject panoramaPlayer { get; set; }

        GameObject stereoPanoramaPlayer { get; set; }

        GameObject planarPlayer { get; set; }

        GameObject panoramaImagePlayer { get; set; }

        void Clear();

		ViewItemBase FindItemByItemIndex (int itemIndex);

		ViewItemBase FindItemByItemId (string itemId);
    }
}