﻿using System.Collections.Generic;
using UnityEngine;

using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.BaseItem;

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public class ModeItemDataset : IItemDataset
    {
        public List<ViewItemBase> itemList { get; set; }

        public GameObject mainCamera { get; set; }

        public GameObject panoramaPlayer { get; set; }

        public GameObject stereoPanoramaPlayer { get; set; }

        public GameObject planarPlayer { get; set; }

        public GameObject panoramaImagePlayer { get; set; }

        public ModeItemDataset()
        {
            itemList = new List<ViewItemBase>();
            mainCamera = null;
        }

        public void Clear()
        {
            itemList.Clear();
        }

		public ViewItemBase FindItemByItemIndex (int itemIndex)
		{
			foreach (ViewItemBase viewItemBase in itemList) 
			{
				if (itemIndex == viewItemBase.itemIndex) 
				{
					return viewItemBase;
				}
			}

			return null;
		}

		public ViewItemBase FindItemByItemId (string itemId)
		{
			foreach (ViewItemBase viewItemBase in itemList) 
			{
				if (itemId == viewItemBase.itemID) 
				{
					return viewItemBase;
				}
			}

			return null;
		}
    }
}
