﻿using UnityEngine;
using System.Collections.Generic;
using PulseIQ.Local.ClientCommon.BaseItem;
using PulseIQ.Local.Common.Model.World;

namespace PulseIQ.Local.ClientCommon.Components.Common
{
    public interface iWorldXmlDataset
    {
        WorldData worldXmlData { get; set; }

        List<string> interactiveItemList { get; set; }
    }
}