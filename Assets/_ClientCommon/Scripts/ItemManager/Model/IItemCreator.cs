﻿using UnityEngine;
using PulseIQ.Local.Client.Unity3DLib.Model;

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public interface IItemCreator
    {
        GameObject GetMainCamera();

        GameObject GetMainCamera(DataAvatar avatarData);

        GameObject GetMediaDropdownUI();

        GameObject GetTutorialItemUI();

        GameObject GetTimerItemUI();

        GameObject GetNotationItemUI();

        GameObject GetMediaUI();
    }
}