﻿using System.Collections.Generic;
using UnityEngine;

using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.BaseItem;
using PulseIQ.Local.Common.Model.World;

namespace PulseIQ.Local.ClientCommon.ItemManager
{
    public class ModelXmlDataset : iWorldXmlDataset
    {
        public WorldData worldXmlData { get; set; }

        public List<string> interactiveItemList { get; set; }
    }
}