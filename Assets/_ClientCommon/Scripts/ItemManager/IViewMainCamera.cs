﻿using PulseIQ.Local.Common.CustomType;

public interface IViewMainCamera 
{
    void TutorialActivated(CTVector3 position, CTQuaternion rotation);

    void TutorialDeactivated();

    void WorldNotationStarted();

    void WorldNotationEnded();

    void MaskCameraForVR();

    void UnmaskCameraFromVR();

    void ShowCamera();

    void HideCamera();
}
