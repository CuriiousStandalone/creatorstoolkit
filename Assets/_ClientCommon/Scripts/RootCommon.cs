﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using strange.extensions.context.api;
using strange.extensions.context.impl;

namespace PulseIQ.Local.ClientCommon
{
    public class RootCommon : ContextView
    {
        private void Awake()
        {
            context = new ContextCommon(this);
        }
    }
}

