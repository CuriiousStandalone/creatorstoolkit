﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.Custom
{
	public class ScaleUpdate : MonoBehaviour {

		public float MaxScale;
		public float MinScale;
		public float Speed;

		private bool isScalingUp = true;

		// Use this for initialization
		void Start () {
			
		}
		
		// Update is called once per frame
		void Update () {
			
			Vector3 curLocalScale = transform.localScale;
			curLocalScale = Speed * Time.deltaTime * (isScalingUp ? new Vector3(1.0f, 1.0f, 1.0f) : new Vector3(-1.0f, -1.0f, -1.0f)) + curLocalScale;

			if (isScalingUp && curLocalScale.x > MaxScale) 
			{
				isScalingUp = false;
			}

			if (!isScalingUp && curLocalScale.x < MinScale) 
			{
				isScalingUp = true;
			}

			transform.localScale = curLocalScale;
		}
	}
}
