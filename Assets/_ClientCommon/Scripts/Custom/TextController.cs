﻿using UnityEngine;
using UnityEngine.UI;

namespace PulseIQ.Local.ClientCommon.Custom
{
    public class TextController : MonoBehaviour
    {
        public void SetText(string txt)
        {
            if (null != gameObject.GetComponent<TextMesh>())
            {
                gameObject.GetComponent<TextMesh>().text = txt;
            }
            else if (null != gameObject.GetComponent<Text>())
            {
                gameObject.GetComponent<Text>().text = txt;
            }
        }

        public void ShowUI()
        {
            gameObject.SetActive(true);
            if (null != gameObject.transform.parent.Find("BackgroundImage"))
            {
                gameObject.transform.parent.Find("BackgroundImage").gameObject.SetActive(true);
            }
        }

        public void HideUI()
        {
            gameObject.SetActive(false);
            if (null != gameObject.transform.parent.Find("BackgroundImage"))
            {
                gameObject.transform.parent.Find("BackgroundImage").gameObject.SetActive(false);
            }
        }
    }
}