﻿using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.UIItem;
using PulseIQ.Local.ClientCommon.BaseItem;

namespace PulseIQ.Local.ClientCommon.Custom
{
    public class GeneralButtonController : MonoBehaviour
    {
        private bool isPlayer = false;
        private bool isToolkit = false;

        private void Awake()
        {
            isPlayer = (Settings.applicationType == EApplicationType.PLAYER_CLIENT) ? true : false;
            isToolkit = (Settings.applicationType == EApplicationType.CREATOR_TOOL_KIT) ? true : false;
        }

        public void Evaluate()
        {
            List<int> itemIndices = gameObject.GetComponent<ViewUIItem>().responseItemIndices;

            MessageData messageData = new MessageData();
            messageData.Parameters.Add((byte)CommonParameterCode.ITEM_INDICES, itemIndices);
            messageData.Parameters.Add((byte)CommonParameterCode.UI_ACTION, UIActionCode.EVALUATE);

            gameObject.GetComponent<ViewUIItem>().ButtonPressed(messageData);
        }

        public void NavigateNext()
        {
            List<int> itemIndices = gameObject.GetComponent<ViewUIItem>().responseItemIndices;

            MessageData messageData = new MessageData();
            messageData.Parameters.Add((byte)CommonParameterCode.ITEM_INDICES, itemIndices);
            messageData.Parameters.Add((byte)CommonParameterCode.UI_ACTION, UIActionCode.NAVIGATE_NEXT);

            gameObject.GetComponent<ViewUIItem>().ButtonPressed(messageData);
        }

        public void NavigatePrevious()
        {
            List<int> itemIndices = gameObject.GetComponent<ViewUIItem>().responseItemIndices;

            MessageData messageData = new MessageData();
            messageData.Parameters.Add((byte)CommonParameterCode.ITEM_INDICES, itemIndices);
            messageData.Parameters.Add((byte)CommonParameterCode.UI_ACTION, UIActionCode.NAVIGATE_PREV);

            gameObject.GetComponent<ViewUIItem>().ButtonPressed(messageData);
        }

		public void HideUI()
		{
			gameObject.GetComponent<ViewItemBase>().HideView();
		}

		public void ShowUI()
		{
			gameObject.GetComponent<ViewItemBase>().ShowView();
		}

        public void ShowEvaluateButton()
        {
            if (null != transform.Find("BtnEvaluate"))
            {
                transform.Find("BtnEvaluate").gameObject.SetActive(true);
            }
        }

        public void HideEvaluateButton()
        {
            if (null != transform.Find("BtnEvaluate"))
            {
                transform.Find("BtnEvaluate").gameObject.SetActive(false);
            }
        }

        public void ReplayAnimation_ButtonClicked()
        {
            List<int> itemIndices = gameObject.GetComponent<ViewUIItem>().responseItemIndices;

            MessageData messageData = new MessageData();
            messageData.Parameters.Add((byte)CommonParameterCode.ITEM_INDICES, itemIndices);
            messageData.Parameters.Add((byte)CommonParameterCode.UI_ACTION, UIActionCode.REPLAY_ANIM);

            gameObject.GetComponent<ViewUIItem>().ButtonPressed(messageData);
        }
    }
}