﻿using PulseIQ.Local.ClientCommon.BaseItem;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.Custom
{
    public class ShowHideController : MonoBehaviour
    {
        public void HideUI()
        {
            gameObject.GetComponent<ViewItemBase>().HideView();
        }

        public void ShowUI()
        {
            gameObject.GetComponent<ViewItemBase>().ShowView();
        }
    }
}