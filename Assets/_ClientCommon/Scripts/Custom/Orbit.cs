﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.Custom
{
	public class Orbit : MonoBehaviour 
	{
		public Vector3 OrbitPoint;
		public Vector3 OrbitAxis;
		public float Speed;
		// Use this for initialization
		void Start () 
		{
			
		}
		
		// Update is called once per frame
		void Update () 
		{
			transform.RotateAround (OrbitPoint, OrbitAxis, Speed * Time.deltaTime);
		}
	}
}