﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.Custom
{
    public class Billboard : MonoBehaviour
    {
        [SerializeField]
        List<GameObject> billboardObjects;
        Transform lookatTransform = null;

        // Update is called once per frame
        void Update()
        {
            if (null != lookatTransform)
            {
                foreach (GameObject obj in billboardObjects)
                {
                    obj.transform.LookAt(lookatTransform);
                }
            }
        }

        public void SetLookatCamera(Transform targetTransform)
        {
            lookatTransform = targetTransform;
        }
    }
}