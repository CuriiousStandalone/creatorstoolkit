﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.Custom
{
	public class AutoRotation : MonoBehaviour {

		public Vector3 FrameRotateAngle;

		// Use this for initialization
		void Start () 
		{
			
		}
		
		// Update is called once per frame
		void Update () 
		{
			Vector3 autoRotation = new Vector3 (FrameRotateAngle.x, FrameRotateAngle.y, FrameRotateAngle.z);
			transform.Rotate (autoRotation);
		}
	}
}
