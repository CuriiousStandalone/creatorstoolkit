﻿using System.Collections.Generic;

using UnityEngine;

using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Custom;
using PulseIQ.Local.ClientCommon.BaseItem;
using PulseIQ.Local.Common.Model.UI;
using PulseIQ.Local.Common.Model.Item;

namespace PulseIQ.Local.ClientCommon.UIItem
{
    public class ViewUIItem : ViewItemBase
    {
        public List<int> responseItemIndices;

        private EUIType uiType;

        internal enum ViewUIEvents
        {
            NEXT_PRESSED,
            PREVIOUS_PRESSED,
            EVALUATE,
            REPLAY_ANIM,
        };

        protected internal override void init()
        {
            itemType = ItemTypeCode.UI;

            CheckUIType();
        }

        public override void HideView()
        {
            gameObject.SetActive(false);
        }

        public override void ShowView()
        {
            gameObject.SetActive(true);
        }

        public void ButtonPressed(MessageData data)
        {
            UIActionCode uIAction = (UIActionCode)data[(byte)CommonParameterCode.UI_ACTION];

            switch(uIAction)
            {
                case UIActionCode.EVALUATE:
                    {
                        dispatcher.Dispatch(ViewUIEvents.EVALUATE, data);
                        break;
                    }

                case UIActionCode.NAVIGATE_NEXT:
                    {
                        dispatcher.Dispatch(ViewUIEvents.NEXT_PRESSED, data);
                        break;
                    }

                case UIActionCode.NAVIGATE_PREV:
                    {
                        dispatcher.Dispatch(ViewUIEvents.PREVIOUS_PRESSED, data);
                        break;
                    }
                case UIActionCode.REPLAY_ANIM:
                    {
                        dispatcher.Dispatch(ViewUIEvents.REPLAY_ANIM, data);
                        HideView();
                        break;
                    }
            }
        }

        public void SetCorrespondingItemIndexes(List<int> itemIndexes)
        {
            responseItemIndices = itemIndexes;
        }

        public void UpdateName(MessageData messageData)
        {
            string txt = (string)messageData[(byte)CommonParameterCode.GENERAL_TEXT];

            foreach (TextController item in gameObject.GetComponentsInChildren<TextController>())
            {
                item.SetText(txt);
                item.ShowUI();
            }
        }
        
        public void SaveUIType(EUIType uiType)
        {
            this.uiType = uiType;
        }

        public void CheckUIType()
        {
            switch(uiType)
            {
                case EUIType.CONTROL:
                    {
                        if (Settings.applicationType == EApplicationType.PLAYER_CLIENT)
                        {
                            HideView();
                        }
                        else
                        {
                            ShowView();
                        }
                        break;
                    }
                case EUIType.PLAYER:
                    {
                        if (Settings.applicationType != EApplicationType.PLAYER_CLIENT)
                        {
                            HideView();
                        }
                        else
                        {
                            ShowView();
                        }
                        break;
                    }
                case EUIType.CONTROL_PLAYER:
                    {
                        ShowView();
                        break;
                    }
            }
        }

        public void ShowEvaluationButton()
        {
            gameObject.GetComponent<GeneralButtonController>().ShowEvaluateButton();
        }

        public void HideEvaluationButton()
        {
            gameObject.GetComponent<GeneralButtonController>().HideEvaluateButton();
        }

        public bool IsReplayButton()
        {
            if(null != transform.Find("BtnReplay"))
            {
                return true;
            }

            return false;
        }

        public void ShowReplayButton()
        {
            ShowView();
        }

        public void HideReplayButton()
        {
            HideView();
        }

        public override void SyncItemData(CTItemData data)
        {
            base.SyncItemData(data);
        }
    }
}
