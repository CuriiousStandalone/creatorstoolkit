﻿using strange.extensions.mediation.impl;
using strange.extensions.dispatcher.eventdispatcher.api;

using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common.Model.SlotGroup;
using PulseIQ.Local.Common;
using PulseIQ.Local.Common.Model.Item;

namespace PulseIQ.Local.ClientCommon.UIItem
{
    public class MediatorUIItem : EventMediator
    {
        [Inject]
        public ViewUIItem view { get; set; }

        public override void OnRegister()
        {
            UpdateListeners(true);
            view.init();
        }

        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        protected void UpdateListeners(bool value)
        {
            view.dispatcher.UpdateListener(value, ViewUIItem.ViewUIEvents.NEXT_PRESSED, CommunicateToItemManager);
            view.dispatcher.UpdateListener(value, ViewUIItem.ViewUIEvents.PREVIOUS_PRESSED, CommunicateToItemManager);
            view.dispatcher.UpdateListener(value, ViewUIItem.ViewUIEvents.EVALUATE, CommunicateToItemManager);
            view.dispatcher.UpdateListener(value, ViewUIItem.ViewUIEvents.REPLAY_ANIM, CommunicateToItemManager);
        }

        private void OnMessageReceived(System.Object data)
        {
            MessageData itemData = (MessageData)data;

            ServerCmdCode Code = (ServerCmdCode)itemData[(byte)CommonParameterCode.OPERATION_CODE];

            switch (Code)
            {
                case ServerCmdCode.SLOT_GROUP_SUBSLOTGROUP_ACTIVATED:
                    {
                        view.UpdateName(itemData);
                        break;
                    }

                case ServerCmdCode.ITEM_SHOWN:
                    {
                        view.ShowView();
                        break;
                    }

                case ServerCmdCode.ITEM_HIDDEN:
                    {
                        view.HideView();
                        break;
                    }

                case ServerCmdCode.SLOT_GROUP_EVALUATION_ENABLED:
                    {
                        view.ShowEvaluationButton();
                        break;
                    }

                case ServerCmdCode.SLOT_GROUP_EVALUATION_DISABLED:
                    {
                        view.HideEvaluationButton();
                        break;
                    }

                case ServerCmdCode.ITEM_STATUS_SYNCED:
                    {
                        CTItemData syncData = (CTItemData)(itemData[(byte)CommonParameterCode.DATA_CT_ITEM_DATA]);
                        view.SyncItemData(syncData);
                        break;
                    }
            }
        }

        private void CommunicateToItemManager(IEvent evt)
        {
            MessageData data = (MessageData)evt.data;

            dispatcher.Dispatch(IntCmdCodeUIItem.COMMUNICATE_ITEM_MANAGER, data);
        }

        private void HideView()
        {
            view.HideView();
        }

        private void ShowView()
        {
            view.ShowView();
        }
    }
}
