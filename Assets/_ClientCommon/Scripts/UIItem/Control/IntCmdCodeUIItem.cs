﻿namespace PulseIQ.Local.ClientCommon.UIItem
{
    public enum IntCmdCodeUIItem
    {
        COMMUNICATE_ITEM_MANAGER,
        REPLAY_ANIM_CLICKED,
        CREATED,
    }
}
