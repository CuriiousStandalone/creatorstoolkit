﻿using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.UIItem
{
    public class OpCmdCreatedUIItem : EventCommand
    {
        public override void Execute()
        {
            dispatcher.Dispatch(IntCmdCodeUIItem.CREATED, evt.data);
        }
    }
}