﻿namespace PulseIQ.Local.ClientCommon.UIItem
{
    public enum UIActionCode
    {
        NAVIGATE_NEXT,
        NAVIGATE_PREV,
        EVALUATE,
        REPLAY_ANIM,
    }
}
