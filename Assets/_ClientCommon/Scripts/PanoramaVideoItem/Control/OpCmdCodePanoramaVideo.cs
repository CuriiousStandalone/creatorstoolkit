﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.PanoramaVideoItem
{
    public enum OpCmdCodePanoramaVideo : byte
    {
        CREATE,
        START,
        PAUSE,
        RESET,
        STOP,
        SEEK,
        SHOW,
        HIDE,
    }
}