﻿using UnityEngine;
using strange.extensions.command.impl;

using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace PulseIQ.Local.ClientCommon.PanoramaVideoItem
{
    public class OpCmdShowPanoramaVideo : EventCommand
    {
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            OperationsPanoramaVideo.ShowPanoramaVideo(ClientBridge.Instance, ClientBridge.Instance.CurUserID, (string)data[(byte)CommonParameterCode.ITEMID]);
        }
    }
}