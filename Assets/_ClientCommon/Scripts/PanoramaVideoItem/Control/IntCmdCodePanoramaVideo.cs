﻿namespace PulseIQ.Local.ClientCommon.PanoramaVideoItem
{
    public enum IntCmdCodePanoramaVideo : byte
    {
        CREATED,
        STARTED,
        PAUSED,
        RESTARTED,
        STOPPED,
        SOUGHT,
        SHOWN,
        HIDDEN,
        LOADED,
        ROTATE_CAMERA,
        RETURN_CAMERA_ROT,
        STATUS_SYNCED,
    }
}