﻿using strange.extensions.command.impl;

using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using PulseIQ.Local.ClientCommon.Components.Common;

namespace PulseIQ.Local.ClientCommon.PanoramaVideoItem
{
    public class OpCmdResetPanoramaVideo : EventCommand
    {
        public override void Execute()
        {
            MessageData data = (MessageData)evt.data;

            OperationsPanoramaVideo.ReplayPanoramaVideo(ClientBridge.Instance, ClientBridge.Instance.CurUserID, (string)data[(byte)CommonParameterCode.ITEMID]);
        }
    }
}