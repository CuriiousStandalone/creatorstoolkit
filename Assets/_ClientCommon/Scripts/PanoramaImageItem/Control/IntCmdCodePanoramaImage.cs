﻿namespace PulseIQ.Local.ClientCommon.PanoramaImageItem
{
    public enum IntCmdCodePanoramaImage : byte
    {
        CREATED,
        HIDDEN,
        SHOWN,
        LOADED,
        ROTATE_CAMERA,
        RETURN_CAMERA_ROT,
        STATUS_SYNCED,
    }
}