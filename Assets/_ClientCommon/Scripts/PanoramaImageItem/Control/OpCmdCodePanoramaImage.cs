﻿namespace PulseIQ.Local.ClientCommon.PanoramaImageItem
{
    public enum OpCmdCodePanoramaImage : byte
    {
        CREATE,
        HIDE,
        SHOW,
    }
}