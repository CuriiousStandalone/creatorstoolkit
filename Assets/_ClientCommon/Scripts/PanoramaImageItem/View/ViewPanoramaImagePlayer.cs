﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using PulseIQ.Local.ClientCommon.BaseItem;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Hotspot;
using PulseIQ.Local.Common;
using PulseIQ.Local.Common.Model.Hotspot;
using PulseIQ.Local.Common.Model.Multimedia;

namespace PulseIQ.Local.ClientCommon.PanoramaImageItem
{
    public class ViewPanoramaImagePlayer : ViewItemBase, IMediaBasic
    {
        internal const string LOADED = "LOADED";
        internal const string SHOWN = "SHOWN";
        internal const string HIDDEN = "HIDDEN";
        internal const string ROTATE_CAMERA = "ROTATE_CAMERA";
        internal const string END_BEHAVIOUR_TELEPORT = "END_BEHAVIOUR_TELEPORT";
        internal const string PLAYING_SYNC = "PLAYING_SYNC";

        private GameObject renderObject;
        private Texture2D imageTexture = null;
        public Renderer imageRenderer = null;

        [SerializeField]
        private GameObject imageJoystick = null;

        private string mediaName;

        private bool isLoaded = false;
        private bool isLoading = false;
        public bool isPlayer = false;
        public bool isShowing = false;
        private bool isMultiplayer = false;
        private bool EndBehaviourTriggered;

        private float panoramaTeleportTime = -1;

        private Coroutine coroutineTeleport = null;

        private EMultimediaEndBehaviorCode endBehaviourCode;

        List<GameObject> hotspotItems = new List<GameObject>();

        protected internal override void init()
        {
            itemType = Local.Common.ItemTypeCode.PANORAMA;

            if (Settings.applicationType == EApplicationType.PLAYER_CLIENT)
            {
                isPlayer = true;
            }
            else
            {
                isPlayer = false;
            }
        }

        public void InitializeMedia(GameObject prefab, List<CTHotspotData> hotspotData)
        {
            renderObject = prefab;
            imageRenderer = renderObject.transform.GetChild(0).GetComponent<Renderer>();
            EndBehaviourTriggered = false;
            Vector3 rightPos = transform.position + ((Vector3.forward + Vector3.right) / 2f).normalized * imageRenderer.bounds.extents.x;
            Vector3 leftPos = transform.position + ((Vector3.forward - Vector3.right) / 2f).normalized * imageRenderer.bounds.extents.x;
            float drawDistance = Vector3.Distance(transform.position, (rightPos + leftPos) / 2f);

            foreach (CTHotspotData hotspot in hotspotData)
            {
                GameObject hotspotObj = Instantiate((Resources.Load(PrefabPathSettings.hotspot) as GameObject), this.transform);
                ViewHotspot hotspotScript = hotspotObj.GetComponent<ViewHotspot>();
                hotspotScript.SetHotspotData(transform.position, itemID, hotspot, drawDistance, itemType);
                hotspotItems.Add(hotspotObj);
                hotspotObj.SetActive(false);
            }
        }

        protected override void OnDestroy()
        {
            UnloadMedia();
        }

        public string MediaName
        {
            get { return mediaName; }

            set { mediaName = value; }
        }

        public bool IsMultiplayer
        {
            get { return isMultiplayer; }
            set { isMultiplayer = value; }
        }

        public void ActivateUI()
        {
        }

        public void SetMediaID()
        {
        }

        public void HideMedia()
        {
            foreach (GameObject obj in hotspotItems)
            {
                obj.SetActive(false);
            }

            if (coroutineTeleport != null)
            {
                StopCoroutine(coroutineTeleport);
                coroutineTeleport = null;
            }

            renderObject.transform.GetChild(0).gameObject.SetActive(false);
            UnloadMedia();

            MessageData data = new MessageData();
            data.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);

            dispatcher.Dispatch(HIDDEN, data);

            isShowing = false;
        }


        public IEnumerator IShowMedia(Vector3 cameraPos)
        {
            yield return new WaitForSeconds(1f);

            LoadMedia(Settings.PanoramaFilePath + MediaName);

            renderObject.transform.GetChild(0).gameObject.SetActive(true);
            renderObject.transform.position = cameraPos;

            isShowing = true;
            foreach (GameObject obj in hotspotItems)
            {
                obj.GetComponent<ViewHotspot>().PositionHotspot(cameraPos);
            }

            if (!EndBehaviourTriggered &&  endBehaviourCode == EMultimediaEndBehaviorCode.TELEPORT && panoramaTeleportTime > 0)
            {
                EndBehaviourTriggered = true;
                coroutineTeleport = StartCoroutine(EndBehaviourTeleportToMedia());
            }
        }

        public void ShowMedia(Vector3 cameraPos)
        {
            LoadMedia(Settings.PanoramaFilePath + MediaName);

            renderObject.transform.GetChild(0).gameObject.SetActive(true);
            renderObject.transform.position = cameraPos;

            isShowing = true;
            foreach (GameObject obj in hotspotItems)
            {
                obj.GetComponent<ViewHotspot>().PositionHotspot(cameraPos);
            }

            if (!EndBehaviourTriggered && endBehaviourCode == EMultimediaEndBehaviorCode.TELEPORT && panoramaTeleportTime > 0)
            {
                EndBehaviourTriggered = true;
                coroutineTeleport = StartCoroutine(EndBehaviourTeleportToMedia());
            }
        }

        private bool isSync = false;

        public void ShowMedia(Vector3 cameraPos, float timeElapsed)
        {
            isSync = true;

            LoadMedia(Settings.PanoramaFilePath + MediaName);

            renderObject.transform.GetChild(0).gameObject.SetActive(true);
            renderObject.transform.position = cameraPos;

            isShowing = true;
            foreach (GameObject obj in hotspotItems)
            {
                obj.GetComponent<ViewHotspot>().PositionHotspot(cameraPos);
            }

            if (!EndBehaviourTriggered && endBehaviourCode == EMultimediaEndBehaviorCode.TELEPORT && panoramaTeleportTime > 0)
            {
                EndBehaviourTriggered = true;
                coroutineTeleport = StartCoroutine(EndBehaviourTeleportToMedia(timeElapsed));
            }
        }

        IEnumerator EndBehaviourTeleportToMedia(float timeElapsed = 0)
        {
            yield return new WaitForSeconds(panoramaTeleportTime - timeElapsed);

            MessageData msgData = new MessageData();

            msgData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);

            dispatcher.Dispatch(END_BEHAVIOUR_TELEPORT, msgData);
        }

        public void ShowMedia(Vector3 cameraPos, Vector3 cameraRotation)
        {
            LoadMedia(Settings.PanoramaFilePath + MediaName);

            renderObject.transform.GetChild(0).gameObject.SetActive(true);
            renderObject.transform.eulerAngles = new Vector3(renderObject.transform.eulerAngles.x, cameraRotation.y, renderObject.transform.eulerAngles.z);
            renderObject.transform.position = cameraPos;

            isShowing = true;
            foreach (GameObject obj in hotspotItems)
            {
                obj.GetComponent<ViewHotspot>().PositionHotspot(cameraPos, renderObject.transform.rotation);
            }
        }

        public void LoadMedia(string path)
        {
            if (!isLoaded && !isLoading)
            {
                EndBehaviourTriggered = false;
                isLoading = true;
                isLoaded = false;
                StartCoroutine(LoadMediaImage(path));
            }
        }

        IEnumerator LoadMediaImage(string path)
        {
            using (WWW www = new WWW(path))
            {
                yield return www;

                if (null != www.error)
                {
                    Debug.Log("WWW Error. CODE:  " + www.error);
                    isLoading = false;
                }

                imageTexture = new Texture2D(2, 2);

                www.LoadImageIntoTexture(imageTexture);
                imageRenderer.material.mainTexture = imageTexture;
                imageTexture = null; //no need to hold on to this.

                www.Dispose();

                if (!isLoaded)
                {
                    MessageData loadData = new MessageData();
                    loadData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);

                    dispatcher.Dispatch(LOADED, loadData);
                    isLoaded = true;
                }

                foreach (GameObject obj in hotspotItems)
                {
                    if (obj.GetComponent<ViewHotspot>().IsEnabled)
                    {
                        obj.GetComponent<ViewHotspot>().EnableHotspot();
                    }

                    obj.SetActive(true);
                }

                MessageData data = new MessageData();
                data.Parameters.Add((byte)CommonParameterCode.ITEM_REFERENCE_ID, itemID);
                data.Parameters.Add((byte)CommonParameterCode.MEDIA_NAME, mediaName);
                data.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, itemType);
                data.Parameters.Add((byte)CommonParameterCode.HOTSPOT_ID_LIST, GetHotspotIDs());
                data.Parameters.Add((byte)CommonParameterCode.HOTSPOT_NAME_LIST, GetHotspotNames());
                data.Parameters.Add((byte)CommonParameterCode.IS_MULTIPLAYER, isMultiplayer);

                dispatcher.Dispatch(SHOWN, data);

                if (isPlayer)
                {
                    dispatcher.Dispatch(ROTATE_CAMERA, data);
                }

                if (isSync)
                {
                    isSync = false;

                    MessageData mData = new MessageData();
                    mData.Parameters.Add((byte)CommonParameterCode.ITEMID, itemID);
                    mData.Parameters.Add((byte)CommonParameterCode.ITEM_TYPE, itemType);
                    dispatcher.Dispatch(PLAYING_SYNC, mData);
                }

                isLoading = false;
            }
        }

        public void UnloadMedia()
        {
            imageRenderer.material.mainTexture = null;
            Resources.UnloadUnusedAssets();
            GC.Collect();
            isLoaded = false;
        }

        public void ShowMedia(Vector3 cameraPos, Quaternion cameraRot)
        {
            foreach (GameObject obj in hotspotItems)
            {
                obj.GetComponent<ViewHotspot>().PositionHotspot(cameraPos, cameraRot);

                if (obj.GetComponent<ViewHotspot>().IsEnabled)
                {
                    obj.GetComponent<ViewHotspot>().EnableHotspot();
                }

                obj.SetActive(true);
            }

        }

        public void DestroyPanoramaMediaPlayer()
        {
            if (null != renderObject)
            {
                imageRenderer.material.mainTexture = null;
                Destroy(imageRenderer.material);
                imageTexture = null;
                Destroy(renderObject);
            }
        }

        private void UpdateHotspotTime(float currentTime)
        {
            foreach (GameObject obj in hotspotItems)
            {
                obj.GetComponent<ViewHotspot>().VideoTimeReceived(currentTime);
            }
        }

        private List<string> GetHotspotIDs()
        {
            List<string> ids = new List<string>();

            foreach (GameObject obj in hotspotItems)
            {
                ids.Add(obj.GetComponent<ViewHotspot>().ReturnHotspotID());
            }

            return ids;
        }

        private List<string> GetHotspotNames()
        {
            List<string> ids = new List<string>();

            foreach (GameObject obj in hotspotItems)
            {
                ids.Add(obj.GetComponent<ViewHotspot>().GetHotspotName);
            }

            return ids;
        }

        public void DisableHotspot(MessageData data)
        {
            string hotspotID = (string)data[(byte)CommonParameterCode.HOTSPOT_ID];

            foreach (GameObject obj in hotspotItems)
            {
                if (obj.GetComponent<ViewHotspot>().ReturnHotspotID() == hotspotID)
                {
                    obj.GetComponent<ViewHotspot>().DisableHotspot();
                }
            }
        }

        public void EnableHotspot(MessageData data)
        {
            string hotspotID = (string)data[(byte)CommonParameterCode.HOTSPOT_ID];

            foreach (GameObject obj in hotspotItems)
            {
                if (obj.GetComponent<ViewHotspot>().ReturnHotspotID() == hotspotID)
                {
                    obj.GetComponent<ViewHotspot>().EnableHotspot();
                }
            }
        }

        public void EnableHotspotStatus(MessageData data)
        {
            string hotspotID = (string)data[(byte)CommonParameterCode.HOTSPOT_ID];
            string hotspotStatusID = (string)data[(byte)CommonParameterCode.HOTSPOT_STATUS_ID];

            foreach (GameObject obj in hotspotItems)
            {
                if (obj.GetComponent<ViewHotspot>().ReturnHotspotID() == hotspotID)
                {
                    obj.GetComponent<ViewHotspot>().EnableHotspotStatus(hotspotStatusID);
                }
            }
        }

        public void TeleportMedia(string hotspotId, int targetItemIndex)
        {
            foreach (var hotspot in hotspotItems)
            {
                ViewHotspot viewHotspot = hotspot.GetComponent<ViewHotspot>();

                if (viewHotspot.ReturnHotspotID() == hotspotId)
                {
                    viewHotspot.ShowTeleportMedia();
                }
            }
        }

        public EMultimediaEndBehaviorCode EndBehaviourCode
        {
            get { return endBehaviourCode; }

            set { endBehaviourCode = value; }
        }

        public float PanoramaTeleportTime
        {
            get { return panoramaTeleportTime; }

            set { panoramaTeleportTime = value; }
        }
    }
}