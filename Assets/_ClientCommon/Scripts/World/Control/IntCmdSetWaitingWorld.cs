using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;
using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.World
{
    public class IntCmdSetWaitingWorld : EventCommand
    {
        public override void Execute()
        {
            MessageData msg = (MessageData)evt.data;
            string WaitingWorldID = (string)msg.Parameters[(byte)CommonParameterCode.WORLDID];
            GlobalSettings.WaitingWorldID = WaitingWorldID;
        }
    }
}