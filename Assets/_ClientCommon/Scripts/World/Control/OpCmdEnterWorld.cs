﻿using PulseIQ.Local.Client.Unity3DLib.Operations;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.ClientCommon.Components.DllBridge;
using PulseIQ.Local.Common;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PulseIQ.Local.ClientCommon.World
{
    public class OpCmdEnterWorld : EventCommand
    {
        [Inject]
        public IWorldDataSet worldDataSet { get; set; }

        private string worldID;

        public override void Execute()
        {
            MessageData messageData = (MessageData)evt.data;

            worldID = (string)messageData[(byte)CommonParameterCode.WORLDID];

            if(null != worldDataSet.worldItemToDestroyList && worldDataSet.worldItemToDestroyList.Count <= 0)
            {
                MemoryUtilities.CleanUpMemory();

                OperationsWorld.EnterWorld(ClientBridge.Instance, worldID);
            }
            else
            {
                Retain();

                UpdateListeners(true);
            }
        }

        private void UpdateListeners(bool val)
        {
            dispatcher.UpdateListener(val, IntCmdCodeWorld.ALL_ITEM_DESTROYED, AllItemsDestroyed);
        }

        private void AllItemsDestroyed(IEvent evt)
        {
            if(worldDataSet.worldItemToDestroyList.Count <= 0)
            {
                MemoryUtilities.CleanUpMemory();

                OperationsWorld.EnterWorld(ClientBridge.Instance, worldID);

                worldDataSet.worldItemToDestroyList.Clear();

                UpdateListeners(false);

                Release();
            }
        }
    }
}
