﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PulseIQ.Local.ClientCommon.World
{
    public interface IWorldDataSet
    {
        HashSet<string> worldItemToDestroyList { get; set; }
    }
}