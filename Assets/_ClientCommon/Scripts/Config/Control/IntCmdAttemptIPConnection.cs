using System;
using System.Collections;
using System.Net.Sockets;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.Config
{
    public class IntCmdAttemptIPConnection : EventCommand
    {
        [Inject(ContextKeys.CONTEXT_VIEW)] public GameObject contextView { get; set; }

        public override void Execute()
        {
            Retain();
            UpdateListeners(true);
            object val;
            string ip = "";
            int port = 0;

            MessageData msg = evt.data as MessageData;
            if (msg.Parameters.TryGetValue((byte) CommonParameterCode.LAUNCHER_IP, out val))
            {
                ip = val.ToString();
            }

            if (msg.Parameters.TryGetValue((byte) CommonParameterCode.LAUNCHER_PORT, out val))
            {
                port = (int) val;
            }

            contextView.GetComponent<MonoBehaviour>().StartCoroutine(AttemptConnectionOnIP(ip, port));
        }

        private void UpdateListeners(bool value)
        {
        }


        /// <summary>
        ///     Returns true if a connection can be established on the IP. false for all other cases.
        /// </summary>
        /// <param name="ip">ip to check.</param>
        /// <param name="port">port to check.</param>
        /// <returns>if ip can be connected to</returns>
        private IEnumerator AttemptConnectionOnIP(string ip, int port)
        {
            using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                IAsyncResult result = socket.BeginConnect(ip, port, null, null);

                //force 30ms timeout.
                yield return new WaitForSeconds(0.03f);


                MessageData msg = new MessageData();
                msg.Parameters.Add((byte) CommonParameterCode.LAUNCHER_IP, ip);
                msg.Parameters.Add((byte) CommonParameterCode.LAUNCHER_PORT, port);
                if (socket.Connected)
                {
                    socket.EndConnect(result);
//                    Debug.Log("Connection Success on " + ip);
                    msg.Parameters.Add((byte) CommonParameterCode.LAUNCHER_CONNECTION_RESULT, true);
                }
                else
                {
                    socket.Close();
                    msg.Parameters.Add((byte) CommonParameterCode.LAUNCHER_CONNECTION_RESULT, false);
                }

                dispatcher.Dispatch(IntCmdCodeConfig.ATTEMPT_CONNECTION_FINISHED, msg);

                Release();
            }
        }
    }
}