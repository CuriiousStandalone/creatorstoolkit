using System.Collections;
using System.Collections.Generic;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using strange.extensions.dispatcher.eventdispatcher.api;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.Config
{
    public class OpCmdScanIPs : EventCommand
    {
        private const int SERVER_PORT = 4530; //tcp port for server.
        private const int CMS_PORT = 8083; //tcp port for server.
        private bool ConnectionAttemptComplete;
        private bool ConnectionAttemptResult;
        private string currentIP;
        private int currentPort;

        private List<string> PotentialIPs = new List<string>();
        [Inject(ContextKeys.CONTEXT_VIEW)] public GameObject contextView { get; set; }


        private string ParsedCMSURL => GlobalSettings.CMSURL.Split(':')[1].Replace("//", "");

        
        public override void Execute()
        {
            Retain();
            UpdateListeners(true);
            currentPort = (int) evt.data;
            contextView.GetComponent<MonoBehaviour>()
                .StartCoroutine(ScanIPs(currentPort));
        }

        private void UpdateListeners(bool value)
        {
            dispatcher.UpdateListener(value, IntCmdCodeConfig.ATTEMPT_CONNECTION_FINISHED, OnConnectionAttempted);
        }


        private void OnConnectionAttempted(IEvent payload)
        {
            MessageData msg = payload.data as MessageData;
            object val;
            msg.Parameters.TryGetValue((byte) CommonParameterCode.LAUNCHER_IP, out val);
            if (!val.ToString().Equals(currentIP))
            {
                return;
            }

            msg.Parameters.TryGetValue((byte) CommonParameterCode.LAUNCHER_PORT, out val);
            if (!val.Equals(currentPort))
            {
                return;
            }

            msg.Parameters.TryGetValue((byte) CommonParameterCode.LAUNCHER_CONNECTION_RESULT, out val);
            ConnectionAttemptResult = (bool) val;
            ConnectionAttemptComplete = true;
        }

        private void AttemptConnection(string ip, int port)
        {
            currentIP = ip;
            MessageData msg = new MessageData();
            msg.Parameters.Add((byte) CommonParameterCode.LAUNCHER_IP, ip);
            msg.Parameters.Add((byte) CommonParameterCode.LAUNCHER_PORT, port);
            dispatcher.Dispatch(IntCmdCodeConfig.ATTEMPT_CONNECTION_ON_IP, msg);
            ConnectionAttemptComplete = false;
        }


        /// <summary>
        ///     Scans ip's from 0>255 of specific subnet.
        /// </summary>
        /// <param name="baseSubnet"></param>
        private IEnumerator ScanIPs(int port)
        {
            PotentialIPs = new List<string>();
            string baseSubnet;
            if (port == SERVER_PORT)
            {
                baseSubnet = string.Join(".", GlobalSettings.ServerIP.Split('.'), 0, 3) + ".";
            }
            else
            {
                baseSubnet = string.Join(".", ParsedCMSURL.Split('.'), 0, 3) + ".";

            }

            for (int i = 0; i < 256; i++)
            {
                AttemptConnection(baseSubnet + i, port);
                while (!ConnectionAttemptComplete)
                {
                    yield return null;
                }

                if (ConnectionAttemptResult && !PotentialIPs.Contains(baseSubnet + i))
                {
                    PotentialIPs.Add(baseSubnet + i);
                }
            }


            MessageData msg = new MessageData();
            msg.Parameters.Add((byte) CommonParameterCode.LAUNCHER_POTENTIAL_IPS, PotentialIPs);
            msg.Parameters.Add((byte) CommonParameterCode.LAUNCHER_PORT, port);
            dispatcher.Dispatch(ECommonCrossContextEvents.SCAN_IPS_FINSIHED, msg);
            Release();
        }
    }
}