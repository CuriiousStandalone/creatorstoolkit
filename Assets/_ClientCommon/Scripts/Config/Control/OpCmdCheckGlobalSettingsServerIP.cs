using System.Collections;
using System.Collections.Generic;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using strange.extensions.dispatcher.eventdispatcher.api;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.Config
{
    public class OpCmdCheckGlobalSettingsServerIP : EventCommand
    {
        private const int SERVER_PORT = 4530; //tcp port for server.
        private const int CMS_PORT = 8083; //tcp port for server.
        private List<string> PotentialIPs = new List<string>();
        private bool ReadyToLaunchCMS;
        private bool ReadyToLaunchServer;
        [Inject(ContextKeys.CONTEXT_VIEW)] public GameObject contextView { get; set; }


        private string ParsedCMSURL => GlobalSettings.CMSURL.Split(':')[1].Replace("//", "");

        public override void Execute()
        {
            Retain();
            GlobalSettings.ReadData(Settings.ConfigFilePath);
            dispatcher.UpdateListener(true, ECommonCrossContextEvents.SCAN_IPS_FINSIHED, OnScanIPsFinished);

            contextView.GetComponent<MonoBehaviour>().StartCoroutine(LaunchAppWhenReady());

            dispatcher.UpdateListener(true, IntCmdCodeConfig.ATTEMPT_CONNECTION_FINISHED,
                OnConfigConnectionAttemptedServer);
            MessageData msg = new MessageData();
            msg.Parameters.Add((byte) CommonParameterCode.LAUNCHER_IP, GlobalSettings.ServerIP);
            msg.Parameters.Add((byte) CommonParameterCode.LAUNCHER_PORT, SERVER_PORT);
            dispatcher.Dispatch(IntCmdCodeConfig.ATTEMPT_CONNECTION_ON_IP, msg);
        }


        private void OnConfigConnectionAttemptedServer(IEvent payload)
        {
            MessageData msg = payload.data as MessageData;
            object val;
            msg.Parameters.TryGetValue((byte) CommonParameterCode.LAUNCHER_IP, out val);
            if (!val.ToString().Equals(GlobalSettings.ServerIP))
            {
                return;
            }

            msg.Parameters.TryGetValue((byte) CommonParameterCode.LAUNCHER_CONNECTION_RESULT, out val);

            dispatcher.UpdateListener(false, IntCmdCodeConfig.ATTEMPT_CONNECTION_FINISHED,
                OnConfigConnectionAttemptedServer);
            if ((bool) val)
            {
                //xml based IP connection was established continue. Launch app.
                ReadyToLaunchServer = true;
            }
            else
            {
                dispatcher.Dispatch(ECommonCrossContextEvents.SCAN_IP_CONNECTIONS, SERVER_PORT);
                dispatcher.Dispatch(ECommonCrossContextEvents.BAD_GLOABL_IP_SETTINGS);
            }

            CheckCMSIP();
        }

        private void CheckCMSIP()
        {
            dispatcher.UpdateListener(true, IntCmdCodeConfig.ATTEMPT_CONNECTION_FINISHED,
                OnConfigConnectionAttemptedCMS);
            MessageData msgData = new MessageData();
            msgData.Parameters.Add((byte) CommonParameterCode.LAUNCHER_IP, ParsedCMSURL);
            msgData.Parameters.Add((byte) CommonParameterCode.LAUNCHER_PORT, CMS_PORT);
            dispatcher.Dispatch(IntCmdCodeConfig.ATTEMPT_CONNECTION_ON_IP, msgData);
        }

        private void OnConfigConnectionAttemptedCMS(IEvent payload)
        {
            MessageData msg = payload.data as MessageData;
            object val;
            msg.Parameters.TryGetValue((byte) CommonParameterCode.LAUNCHER_IP, out val);
            if (!val.ToString().Equals(ParsedCMSURL))
            {
                return;
            }

            msg.Parameters.TryGetValue((byte) CommonParameterCode.LAUNCHER_CONNECTION_RESULT, out val);

            dispatcher.UpdateListener(false, IntCmdCodeConfig.ATTEMPT_CONNECTION_FINISHED,
                OnConfigConnectionAttemptedCMS);
            if ((bool) val)
            {
                //xml based IP connection was established continue. Launch app.
                ReadyToLaunchCMS = true;
            }
            else
            {
                dispatcher.Dispatch(ECommonCrossContextEvents.SCAN_IP_CONNECTIONS, CMS_PORT);
                dispatcher.Dispatch(ECommonCrossContextEvents.BAD_GLOABL_IP_SETTINGS);
            }
        }


        private void OnScanIPsFinished(IEvent dummy)
        {
            //Display drop down to select ip here.
            dispatcher.UpdateListener(false, ECommonCrossContextEvents.SCAN_IPS_FINSIHED, OnScanIPsFinished);

            ReadyToLaunchServer = true;
            ReadyToLaunchCMS = true;
        }

        /// <summary>
        ///     Continue from launcher to Main.
        /// </summary>
        private IEnumerator LaunchAppWhenReady()
        {
            while (!ReadyToLaunchServer || !ReadyToLaunchCMS)
            {
                yield return null;
            }

            dispatcher.Dispatch(ECommonCrossContextEvents.CHECK_GLOBAL_SETTINGS_SERVER_IP_FINISHED);
            Release();
        }
    }
}