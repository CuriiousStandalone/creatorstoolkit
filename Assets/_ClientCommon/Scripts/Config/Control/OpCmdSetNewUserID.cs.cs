﻿using System;
using System.Collections;
using System.Collections.Generic;
using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;
using strange.extensions.command.impl;
using UnityEngine;

namespace PulseIQ.Local.ClientCommon.Config
{
    public class OpCmdSetNewUserID : EventCommand
    {
        /// <summary>
        /// Sets the UserID and Writes to xml.
        /// </summary>
        /// <param name="UserID as string"></param>
        public override void Execute()
        {
            GlobalSettings.UserId =  evt.data.ToString();

            //write to xml
            GlobalSettings.WriteData(Settings.ConfigFilePath);
        }
    }
}