namespace PulseIQ.Local.ClientCommon.Config
{
    public enum IntCmdCodeConfig
    {
        ATTEMPT_CONNECTION_ON_IP,
        ATTEMPT_CONNECTION_FINISHED
    }
}