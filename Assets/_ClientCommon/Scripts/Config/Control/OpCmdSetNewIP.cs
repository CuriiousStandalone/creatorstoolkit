﻿using PulseIQ.Local.ClientCommon.Components.Common;
using PulseIQ.Local.Common;
using strange.extensions.command.impl;

namespace PulseIQ.Local.ClientCommon.Config
{
    public class OpCmdSetNewIP : EventCommand
    {
        private const int SERVER_PORT = 4530; //tcp port for server.
        private const int CMS_PORT = 8083; //tcp port for server.

        /// <summary>
        ///     Sets the IP and Writes to xml.
        /// </summary>
        /// <param name="newip"></param>
        public override void Execute()
        {
            MessageData msg = (MessageData) evt.data;
            object val;
            if (!msg.Parameters.TryGetValue((byte) CommonParameterCode.LAUNCHER_PORT, out val))
            {
                return;
            }

            if ((int) val == SERVER_PORT)
            {
                if (!msg.Parameters.TryGetValue((byte) CommonParameterCode.LAUNCHER_IP, out val))
                {
                    return;
                }

                GlobalSettings.ServerIP = val.ToString();
            }
            else
            {
                if (!msg.Parameters.TryGetValue((byte) CommonParameterCode.LAUNCHER_IP, out val))
                {
                    return;
                }

                GlobalSettings.CMSURL = "http://" + val + ":" + CMS_PORT + "/";
            }

            //write to xml?
            GlobalSettings.WriteData(Settings.ConfigFilePath);
        }
    }
}